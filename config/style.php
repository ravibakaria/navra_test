<?php

    require_once '../config/config.php'; 
    require_once '../config/dbconnect.php'; 
    require_once '../config/functions.php'; 
    header('Content-type: text/css');
  $style = null;    

  $primary_color = getPrimaryColor();
  $primary_font_color = getPrimaryFontColor();

  $sidebar_color = getSidebarColor();
  $sidebar_font_color = getSidebarFontColor();
?>


/*******   Main Header  *********/
.header {
  background-color: <?php echo $primary_color; ?>; 
  border-bottom: 1px solid <?php echo $primary_color; ?>;
}

@media (max-width: 768px){
    .header-right {
    background-color: <?php echo $primary_color; ?> !important; 
    border-bottom: 1px solid <?php echo $primary_color; ?> !important;
  }
}

/*******   Secondary Header  *********/
.secondary-navigation {
  background: <?php echo $sidebar_color; ?>;
  color: <?php echo $sidebar_font_color; ?> !important;
  padding-left: 2%;
}

.navbar-default .navbar-nav > li > a {
  color: <?php echo $sidebar_font_color; ?> !important;
}

.navbar-default .navbar-nav > li > a:hover {
  background: <?php echo $primary_color; ?> ;
  color: <?php echo $primary_font_color; ?> !important;
  height: 49px;
}

.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
  background: <?php echo $primary_color; ?> ;
  color: <?php echo $primary_font_color; ?> !important;
}

.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
  background: <?php echo $primary_color; ?> ;
  color: <?php echo $primary_font_color; ?> !important;
}

.dropdown-menu {
  background: <?php echo $sidebar_color; ?>;
  color: <?php echo $sidebar_font_color; ?> !important;
}

.dropdown-menu > li > a {
  color: <?php echo $sidebar_font_color; ?> !important;
}

.navbar-default .navbar-toggle .icon-bar {
    background-color: <?php echo $sidebar_font_color; ?>;
    color : <?php echo $sidebar_font_color; ?>;
}

.profile-id-search {
  color : <?php echo $sidebar_font_color; ?>;
}

/*******   Homepage search row  *********/
.home-search {
  background-color: <?php echo $sidebar_color; ?>; 
  color: <?php echo $sidebar_font_color; ?>;
}

/*******   Buttons in website  *********/
.website-button {
  background-color: <?php echo $primary_color; ?> !important; 
  color: <?php echo $primary_font_color; ?> !important;
  border:1px solid #000000 !important;
  font-weight: bolder;
  margin-top:10px;
}

.profile-view-button {
  background-color: <?php echo $primary_color; ?> !important; 
  color: <?php echo $primary_font_color; ?> !important;
  border-color: <?php echo $primary_color; ?> !important; 
  font-weight: bolder;
  height: 50px;
  text-align: center;
  border-radius: 0px !important;
}

.website-button:hover {
    background-color: <?php echo $sidebar_color; ?> !important;
    color: <?php echo $sidebar_font_color; ?> !important;
    border:1px solid #000000 !important;
}

.profile-view-button:hover {
  background-color: <?php echo $sidebar_color; ?> !important;
  color: <?php echo $sidebar_font_color; ?> !important;
  border-color: <?php echo $sidebar_color; ?> !important;
  height: 50px;
  text-align: center;
  border-radius: 0px !important;
}


/********    Panel heads(Login,Register)   **********/
.body-sign .panel-sign .panel-title-sign .title{
    background-color: <?php echo $primary_color; ?> !important;
    color: <?php echo $primary_font_color; ?> !important;
    font-weight: bolder;
}

.body-sign .panel-sign .panel-body{
  border-top-color: <?php echo $primary_color; ?> !important;
  border-left: 1px solid <?php echo $primary_color; ?>;
  border-right: 1px solid <?php echo $primary_color; ?>;
  border-bottom: 1px solid <?php echo $primary_color; ?>;
}


/*******   Homepage header site description   ******/
.homepage-description {
  color: <?php echo $primary_font_color; ?>;
  font-weight: 600;
  text-align: center;
  padding-bottom: 5px;
  letter-spacing: 0.5px;
}

.homepage-heading {
  color: <?php echo $primary_font_color; ?>;
  font-weight: 600;
  text-align: center;
  padding-bottom: 5px;
  letter-spacing: 0.5px;
  font-size: x-large;
}

.toggle-on {
  background-color: <?php echo $primary_color; ?> !important;
  color: <?php echo $primary_font_color; ?> !important;
}

.toggle-on:hover {
    background-color: <?php echo $sidebar_color; ?> !important;
    color: <?php echo $sidebar_font_color; ?> !important;
}

.toggle-off {
  background-color: <?php echo $primary_color; ?> !important;
  color: <?php echo $primary_font_color; ?> !important;
}

.toggle-off:hover {
    background-color: <?php echo $sidebar_color; ?> !important;
    color: <?php echo $sidebar_font_color; ?> !important;
}


.switch-field input:checked + label {
    background-color: <?php echo $primary_color; ?> !important;
    color: <?php echo $primary_font_color; ?> !important;
}

/*******   User option popup  *********/
.userbox .name {
  color: <?php echo $primary_font_color; ?>;
}

/******    Top navigation links  ******/
.top_nav_links {
    color: <?php echo $primary_font_color; ?>;
}

.homepage-nav > li > a:hover, .homepage-nav > li > a:focus {
  color: <?php echo $primary_font_color; ?>;
}

/********  User profile  *********/
.home_profile_menu {
    background-color: <?php echo $sidebar_color; ?>;
    color: <?php echo $sidebar_font_color; ?>;
}

.userbox.open .dropdown-menu a{
  color: <?php echo $primary_font_color; ?>;     
}

.userbox.open .dropdown-menu a{
  color: <?php echo $primary_font_color; ?> !important;
}

.userbox.open .dropdown-menu a:hover{
  background: <?php echo $primary_color; ?>;
}


/******    Sidebar in user/admin section  ******/
.sidebar-left {secondary-navigation
  background: <?php echo $sidebar_color; ?>;
}

/*******    Sidebar ********/
ul.nav-main li a {
    color: <?php echo $sidebar_font_color; ?>;
}

ul.nav-main > li > a:hover, ul.nav-main > li > a:focus {
    background-color: <?php echo $primary_color; ?>;
    width:102%;
}

.nav-main > li > a:hover, .nav-main > li > a:focus {
    color: <?php echo $sidebar_font_color; ?>;
}

.sidebar-title {
  color: <?php echo $sidebar_font_color; ?>;
}

html.sidebar-left-collapsed.fixed .sidebar-left .nano {
    background: <?php echo $sidebar_color; ?>;
}

.sidebar-left .sidebar-header .sidebar-toggle {
  background-color: <?php echo $primary_color; ?>;
}

.nav-active {
  background-color: <?php echo $primary_color; ?>;
  color: <?php echo $primary_font_color; ?>;
  font-weight: 600;
  font-size: 15px;
  height: 49px;
}

/*******    Page Footer ********/
.footer-div {
  background-color: <?php echo $primary_color; ?>;
    color: <?php echo $primary_font_color; ?>;
}

.footer-white-text {
  color: <?php echo $primary_font_color; ?> !important;
}

/******    Top navigation toggle start  ******/
.navbar-toggle {
  color: <?php echo $primary_font_color; ?>;
}

@media (max-width: 768px){
    .nav.navbar-nav{
      background-color: <?php echo $primary_color; ?>;
    }

    .header .logo-container {
      background-color: <?php echo $primary_color; ?>;
    }

    .page-header{
      background: <?php echo $sidebar_color; ?>;
      margin: -40px -295px 40px -40px;
    }
}

/******   Page Header   ********/

.page-header{
    background: <?php echo $sidebar_color; ?>;
    color: <?php echo $sidebar_font_color; ?>;
  }

.page-header .breadcrumbs a, .page-header .breadcrumbs span{
  color: <?php echo $sidebar_font_color; ?> !important;
}

/******   Home page Find-my-partner button   ********/
#hero-search{
    width: 100%;
    height: 35px;
    background-color: <?php echo $primary_color; ?>;
    border: 2px solid <?php echo $primary_font_color; ?>;
    color: <?php echo $primary_font_color; ?>;
}

#hero-search:hover {
    background-color: <?php echo $sidebar_color; ?>;
    color: <?php echo $sidebar_font_color; ?> !important;
}

/***********  Chat messages   ************/
.user_text {
  color: <?php echo $primary_color; ?>;
}

.self_text {
  color: <?php echo $sidebar_color; ?>;
}

.message_chat_div {
  border: 1px solid #dddddd;
}

/**********   Back to top button   *************/
.back-to-top {
  background-color: <?php echo $sidebar_color; ?> !important;
  color: <?php echo $sidebar_font_color; ?> !important;
  border-color: <?php echo $sidebar_color; ?> !important;
}

.back-to-top:hover {
  border-color: <?php echo $sidebar_color; ?> !important;
}

/**********   Dashboard panels   *****************/
blockquote.info {
    border-color: <?php echo $primary_color; ?> !important;
    background-color:#f0f0f0;
}

.dashboard-panels {
    border-color: <?php echo $primary_color; ?> !important;
}

.edit-fa {
  color: <?php echo $primary_color; ?>;
}

/***********   Membership Plans    **************/
.pricingTable .pricingTable-header > .heading {
  background: <?php echo $sidebar_color; ?>;
  color: <?php echo $sidebar_font_color; ?> !important;
}

.price-head-value {
  text-transform: uppercase;
  color: <?php echo $sidebar_font_color; ?> !important;
}

.pricingTable .pricingTable-header > .price-value {
  background: <?php echo $sidebar_color; ?>;
  color: <?php echo $sidebar_font_color; ?> !important;
}

.pricingTable .pricingTable-sign-up > .btn-block {
  background: <?php echo $sidebar_color; ?>;
  color: <?php echo $sidebar_font_color; ?> !important;
  border: 1px solid <?php echo $sidebar_color; ?>;
}

.pricingTable .pricingTable-sign-up > .btn-block:hover:before,
.pricingTable .pricingTable-sign-up > .btn-block:hover:after{
    background: <?php echo $primary_color; ?>;
    color: <?php echo $primary_font_color; ?>;
    border-color: <?php echo $primary_color; ?>;
}

.pricingTable {
  border:1px solid <?php echo $sidebar_color; ?>;
}

.membership-price-value {
    font-size: 40px !important;
    color: <?php echo $sidebar_font_color; ?> !important;
}

.badge {
  background-color: <?php echo $sidebar_color; ?>;
}

.border-left-div {
  border-left:3px solid <?php echo $sidebar_color; ?>;
}


/**************   Featured listing plan   ***************/
.featured-listing-badge {
  background-color:<?php echo $sidebar_font_color; ?> !important;
  color:<?php echo $sidebar_color; ?> !important;
  font-size: 14px;
  vertical-align: middle !important;
}


/**************    Admin dashboard panels    ********************/
.panel-featured-primary {
  border-color: <?php echo $primary_color; ?> ;
}

/*************    Payment receipt print   *****************/
.payment-logo-row {
  background-color: <?php echo $primary_color; ?>;
}

@media print
{
    .payment-logo-row {
    background-color: <?php echo $primary_color; ?> !important;
  }
}



/***************    Datepicker    ****************/
  .datepicker.dropdown-menu {
      background-color: <?php echo $primary_color; ?> !important; 
      color: <?php echo $primary_font_color; ?> !important;
  }

  @media (max-width: 768px){
      .datepicker.dropdown-menu {
        background-color: <?php echo $primary_color; ?> !important; 
        color: <?php echo $primary_font_color; ?> !important;
      }
  }


/***************    Vendor work photos    *************/
  .vendor-gallary {
    border:5px solid <?php echo $sidebar_color; ?>;
  }

  .vendor-gallary:hover {
    border:5px solid <?php echo $primary_color; ?>;
  }