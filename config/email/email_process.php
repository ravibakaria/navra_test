<?php
	
	function EmailProcess($subject,$message,$mailto,$mailtoname)
	{
		//require_once "config/setup-values.php";
		require_once "class.phpmailer.php";

		global $FromName;
		global $FromEmail;
		global $BCCEmail;
		global $SMTPHost;
		global $SMTPPort;
		global $SMTPUsername;
		global $SMTPPassword;
		global $SMTPSSL;
		global $GlobalEmailSignature;
		
		$to = $mailto;
	    $name = $mailtoname;

	    $smtphost = $SMTPHost;
		$smtpport = $SMTPPort;
		$smtpdebug = 0;
		$smtpauth = true;
		$smtpsecure = $SMTPSSL;
		$smtpusername = $SMTPUsername;
		$smtppassword = $SMTPPassword;
		$smtpfromname = $FromName;
		$smtpfromid = $FromEmail;
		/*$smtpreplytoname = $FromName;
		$smtpreplytoid = $FromEmail;*/
    
		if($BCCEmail!='')
	    {
	    	$smtpbccname1 = $BCCEmail;
			$smtpbccid1 = $BCCEmail;
	    }

		//Create a new PHPMailer instance
		$mail = new PHPMailer();
		//Tell PHPMailer to use SMTP
		$mail->IsSMTP();
		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug  = $smtpdebug;

		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';

		//Set the hostname of the mail server
		$mail->Host       = $smtphost;

		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->Port       = $smtpport;

		//Whether to use SMTP authentication
		if ($smtpauth == "true")
		{
			$mail->SMTPAuth   = true;

			//Username to use for SMTP authentication
			$mail->Username   = $smtpusername;

			//Password to use for SMTP authentication
			$mail->Password   = $smtppassword;
		}

		$mail->SMTPKeepAlive = true;

		if ($smtpsecure == "ssl")
		{
			$mail->SMTPSecure = "ssl";
		}
		else if ($smtpsecure == "tls")
		{
			$mail->SMTPOptions = array(
				'tls' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);				
		}

		//Set who the message is to be sent from
		if ($smtpfromid != '' && $smtpfromname != '')
			$mail->SetFrom($smtpfromid, $smtpfromname);

		//Set an alternative reply-to address
		/*if ($smtpreplytoid != '' && $smtpreplytoname != '')
			$mail->AddReplyTo($smtpreplytoid, $smtpreplytoname);*/

		if ($BCCEmail != '' && $BCCEmail != '')
			$mail->AddBCC($smtpbccid1, $smtpbccname1);

		$mail->AddAddress($to, $name);
        
        
        
		//Set the subject line
		$mail->Subject = $subject;

		//Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
		$mail->MsgHTML($message);
	    
	    if(!$mail->Send())
	    {
	    	return "danger";
	    	exit;
	    }   
	    else
	    {
	    	return "success";
	    	exit;
	    }
	}
	

	function EmailProcessAttachment($subject,$message,$mailto,$mailtoname,$document_path,$document)
	{
		//require_once "config/setup-values.php";
		require_once "class.phpmailer.php";

		global $FromName;
		global $FromEmail;
		global $BCCEmail;
		global $SMTPHost;
		global $SMTPPort;
		global $SMTPUsername;
		global $SMTPPassword;
		global $SMTPSSL;
		global $GlobalEmailSignature;

		//return $message;exit;
		
		$to = $mailto;
	    $name = $mailtoname;
	    
	    $smtphost = $SMTPHost;
		$smtpport = $SMTPPort;
		$smtpdebug = 0;
		$smtpauth = true;
		$smtpsecure = $SMTPSSL;
		$smtpusername = $SMTPUsername;
		$smtppassword = $SMTPPassword;
		$smtpfromname = $FromName;
		$smtpfromid = $FromEmail;
		/*$smtpreplytoname = $FromName;
		$smtpreplytoid = $FromEmail;*/
    
		if($BCCEmail!='')
	    {
	    	$smtpbccname1 = $BCCEmail;
			$smtpbccid1 = $BCCEmail;
	    }

		//Create a new PHPMailer instance
		$mail = new PHPMailer();
		//Tell PHPMailer to use SMTP
		$mail->IsSMTP();
		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug  = $smtpdebug;

		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';

		//Set the hostname of the mail server
		$mail->Host       = $smtphost;

		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->Port       = $smtpport;

		//Whether to use SMTP authentication
		if ($smtpauth == "true")
		{
			$mail->SMTPAuth   = true;

			//Username to use for SMTP authentication
			$mail->Username   = $smtpusername;

			//Password to use for SMTP authentication
			$mail->Password   = $smtppassword;
		}

		$mail->SMTPKeepAlive = true;

		if ($smtpsecure == "ssl")
		{
			$mail->SMTPSecure = "ssl";
		}
		else if ($smtpsecure == "tls")
		{
			$mail->SMTPOptions = array(
				'tls' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);				
		}
	   
	    // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";  
        $headers .= 'Content-type: application/octet-stream; charset=UTF-8' . "\r\n";     
	     
	    //Set who the message is to be sent from
		if ($smtpfromid != '' && $smtpfromname != '')
			$mail->SetFrom($smtpfromid, $smtpfromname);

		//Set an alternative reply-to address
		/*if ($smtpreplytoid != '' && $smtpreplytoname != '')
			$mail->AddReplyTo($smtpreplytoid, $smtpreplytoname);*/

		if ($BCCEmail != '' && $BCCEmail != '')
			$mail->AddBCC($smtpbccid1, $smtpbccname1);

		$mail->AddAddress($to, $name);
	         
	    $mail->Subject = $subject;
	    $mail->MsgHTML($message);
	    
	    $mail->AddStringAttachment(file_get_contents($document_path), $document);

	    if(!$mail->Send())
	    {
	        return "danger";
	    	exit;
	    }   
	    else
	    {
	        return "success";
	    	exit;
	    }
	}
?>