<?php
	/***** Fetch template for verification email start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='1'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$EmailVerificationTemplateName = removeBrackets($result1['emailTemplateName']);
	$EmailVerificationSubject = removeBrackets($result1['emailTemplateSubject']);
    $EmailVerificationMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for verification email end  *****/


    /***** Fetch template for forgot password email start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='2'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$ForgotPasswordTemplateName = removeBrackets($result1['emailTemplateName']);
	$ForgotPasswordSubject = removeBrackets($result1['emailTemplateSubject']);
    $ForgotPasswordMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for forgot password email end  *****/


    /***** Fetch template for login credentials start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='3'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$NewRegistrationTemplateName = removeBrackets($result1['emailTemplateName']);
	$NewRegistrationSubject = removeBrackets($result1['emailTemplateSubject']);
    $NewRegistrationMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for login credentials end  *****/


    /***** Fetch template for Interest Received start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='4'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$InterestRecievedTemplateName = removeBrackets($result1['emailTemplateName']);
	$InterestRecievedSubject = removeBrackets($result1['emailTemplateSubject']);
    $InterestRecievedMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for Interest Received end  *****/

    /***** Fetch template for Message chat received start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='5'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$MessageReceivedTemplateName = removeBrackets($result1['emailTemplateName']);
	$MessageReceivedSubject = removeBrackets($result1['emailTemplateSubject']);
    $MessageReceivedMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for Request photo end  *****/

    /***** Fetch template for photo request start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='6'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$RequestPhotoTemplateName = removeBrackets($result1['emailTemplateName']);
	$RequestPhotoSubject = removeBrackets($result1['emailTemplateSubject']);
    $RequestPhotoMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for Request photo end  *****/

    /***** Fetch template for Invite friends start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='7'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$InviteFriendsTemplateName = removeBrackets($result1['emailTemplateName']);
	$InviteFriendsSubject = removeBrackets($result1['emailTemplateSubject']);
    $InviteFriendsMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for Invite Friends end  *****/


    /***** Fetch template for Document reject start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='8'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$DocumentRejectTemplateName = removeBrackets($result1['emailTemplateName']);
	$DocumentRejectSubject = removeBrackets($result1['emailTemplateSubject']);
    $DocumentRejectMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for Document reject end  *****/

    /***** Fetch template for Photo reject start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='9'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$PhotoRejectTemplateName = removeBrackets($result1['emailTemplateName']);
	$PhotoRejectSubject = removeBrackets($result1['emailTemplateSubject']);
    $PhotoRejectMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for Photo reject end  *****/

    /***** Fetch template for Enquiry Email start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='10'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$EnquiryEmailTemplateName = removeBrackets($result1['emailTemplateName']);
	$EnquiryEmailSubject = removeBrackets($result1['emailTemplateSubject']);
    $EnquiryEmailMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for Enquiry Email end  *****/

    /***** Fetch template for like Received start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='11'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$LikeRecievedTemplateName = removeBrackets($result1['emailTemplateName']);
	$LikeRecievedSubject = removeBrackets($result1['emailTemplateSubject']);
    $LikeRecievedMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for like Received end  *****/

    /***** Fetch template for New Admin Registration start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='12'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$NewAdminRegistrationTemplateName = removeBrackets($result1['emailTemplateName']);
	$NewAdminRegistrationSubject = removeBrackets($result1['emailTemplateSubject']);
    $NewAdminRegistrationMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for New Admin Registration end  *****/

    /***** Fetch template for pending verification email start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='13'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$ReminderEmailVerificationTemplateName = removeBrackets($result1['emailTemplateName']);
	$ReminderEmailVerificationSubject = removeBrackets($result1['emailTemplateSubject']);
    $ReminderEmailVerificationMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for verification email end  *****/

    /***** Fetch template for pending verification email start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='14'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$ReminderProfileCompletenessTemplateName = removeBrackets($result1['emailTemplateName']);
	$ReminderProfileCompletenessSubject = removeBrackets($result1['emailTemplateSubject']);
    $ReminderProfileCompletenessMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for verification email end  *****/

    /***** Fetch template for birthday wish email start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='15'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$BirtdayWishTemplateName = removeBrackets($result1['emailTemplateName']);
	$BirtdayWishSubject = removeBrackets($result1['emailTemplateSubject']);
    $BirtdayWishMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for birthday wish email end  *****/

    /***** Fetch template for new profile notification email start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='16'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$NewProfileNotificationTemplateName = removeBrackets($result1['emailTemplateName']);
	$NewProfileNotificationSubject = removeBrackets($result1['emailTemplateSubject']);
    $NewProfileNotificationMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for new profile notification email end  *****/

    /***** Fetch template for new messages notification email start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='17'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$NewMessageNotificationTemplateName = removeBrackets($result1['emailTemplateName']);
	$NewMessageNotificationSubject = removeBrackets($result1['emailTemplateSubject']);
    $NewMessageNotificationMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for new messages notification email end  *****/

    /***** Fetch template for payment status update email start  *****/
	$stmt1   = $link->prepare("SELECT * FROM `emailtemplates` WHERE `id`='18'");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	//$$WebSiteTitle = $WebSiteTitle;
	$PaymentStatusUpdateTemplateName = removeBrackets($result1['emailTemplateName']);
	$PaymentStatusUpdateSubject = removeBrackets($result1['emailTemplateSubject']);
    $PaymentStatusUpdateMessage = removeBrackets($result1['emailTemplateMessage']);
    /***** Fetch template for payment status update email end  *****/
?>