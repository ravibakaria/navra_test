<?php

	/***** Fetch style/css, header & footer start  *****/

	$stmt1   = $link->prepare("SELECT * FROM `emailtemplatestyle`");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();

	$EmailCSS = $result1['EmailCSS'];
	$EmailGlobalHeader = removeBrackets($result1['EmailGlobalHeader']);
    $EmailGlobalFooter = removeBrackets($result1['EmailGlobalFooter']);

    /***** Fetch style/css, header & footer end  *****/
?>