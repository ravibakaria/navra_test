<?php
	require_once 'config.php'; 
	$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
	if(mysqli_connect_errno())
	{ 
		echo "Failed to connect to MySQL: " . mysqli_connect_error(); 
	}

	// Open connection
	try 
	{
		$link = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'', ''.DB_USERNAME.'', ''.DB_PASSWORD.'');

	}
	catch (PDOException $e) 
	{
	    echo 'Error: ' . $e->getMessage();
	    exit();
	}
?>