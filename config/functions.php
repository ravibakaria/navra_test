<?php
    
    if(!isset($_SESSION))
    {
        session_start();
    }
    
    /************   Set default timezone   **********/
    $DefaultTimezone = getDefaultTimeZone();
    if($DefaultTimezone!='' || $DefaultTimezone!=null)
    {
        date_default_timezone_set("$DefaultTimezone");
    }
    else
    {
        date_default_timezone_set("Asia/Calcutta");
    }

    $today = date('Y-m-d H:i:s');

    /************   IP Address   *******************/
    $IP_Address = $_SERVER['REMOTE_ADDR'];


    // Quote variable to make safe
    function quote_smart($value)
    {
        global $link;
        // Strip HTML & PHP tags & convert all applicable characters to HTML entities
        $value = trim(htmlentities(strip_tags($value)));    

        // Stripslashes
        if ( get_magic_quotes_gpc() )
        {
            $value = stripslashes( $value );
        }
        return $value;
    }

    /*   Get Primary Color  */
    function getPrimaryColor()
    {
        global $link;
        $sql = "SELECT `primary_color` FROM `theme_color_setting`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $primary_color = $result['primary_color'];

        return $primary_color;
    }

    /*   Get Primary Font Color  */
    function getPrimaryFontColor()
    {
        global $link;
        $sql = "SELECT `primary_font_color` FROM `theme_color_setting`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $primary_font_color = $result['primary_font_color'];

        return $primary_font_color;
    }

    /*   Get Sidebar Color  */
    function getSidebarColor()
    {
        global $link;
        $sql = "SELECT `sidebar_color` FROM `theme_color_setting`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $sidebar_color = $result['sidebar_color'];

        return $sidebar_color;
    }

    /*   Get Sidebar Font Color  */
    function getSidebarFontColor()
    {
        global $link;
        $sql = "SELECT `sidebar_font_color` FROM `theme_color_setting`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $sidebar_font_color = $result['sidebar_font_color'];

        return $sidebar_font_color;
    }

    /*   Get Website basepath  */
    function getWebsiteBasePath()
    {
        global $link;
        $sql = "SELECT `WebSiteBasePath` FROM `generalsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $WebSiteBasePath = $result['WebSiteBasePath'];

        return $WebSiteBasePath;
    }


    /*   Get Website Title  */
    function getWebsiteTitle()
    {
        global $link;
        $sql = "SELECT `WebSiteTitle` FROM `generalsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $WebSiteTitle = $result['WebSiteTitle'];

        return $WebSiteTitle;
    }

    /*   Get Website Tagline  */
    function getWebSiteTagline()
    {
        global $link;
        $sql = "SELECT `WebSiteTagline` FROM `generalsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $WebSiteTagline = $result['WebSiteTagline'];

        return $WebSiteTagline;
    }

    /*   Get Website Email  */
    function getWebSiteEmail()
    {
        global $link;
        $sql = "SELECT `EmailAddress` FROM `generalsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $EmailAddress = $result['EmailAddress'];

        return $EmailAddress;
    }

    /*   Get Website enquiry email  */
    function getEnquiryEmail()
    {
        global $link;
        $sql = "SELECT `enquiry_email` FROM `contact_us`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $enquiry_email = $result['enquiry_email'];

        return $enquiry_email;
    }

    /*   Get Website LogoURL  */
    function getLogoURL()
    {
        global $link;
        $sql = "SELECT `LogoURL` FROM `generalsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $LogoURL = $result['LogoURL'];

        return $LogoURL;
    }

    /*   Get Website FaviconURL  */
    function getFaviconURL()
    {
        global $link;
        $sql = "SELECT `FaviconURL` FROM `generalsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $FaviconURL = $result['FaviconURL'];

        return $FaviconURL;
    }

    /*   Get Website TermOfServiceURL  */
    function getTermOfServiceURL()
    {
        global $link;
        $sql = "SELECT `TermOfServiceURL` FROM `generalsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $TermOfServiceURL = $result['TermOfServiceURL'];

        return $TermOfServiceURL;
    }

    /*   Get Website PrivacyPolicyURL  */
    function getPrivacyPolicyURL()
    {
        global $link;
        $sql = "SELECT `PrivacyPolicyURL` FROM `generalsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $PrivacyPolicyURL = $result['PrivacyPolicyURL'];

        return $PrivacyPolicyURL;
    }

    /*   Get Website AllowedFileAttachmentTypes  */
    function getAllowedFileAttachmentTypesString()
    {
        global $link;
        $sql = "SELECT `AllowedFileAttachmentTypes` FROM `generalsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $AllowedFileAttachmentTypes = $result['AllowedFileAttachmentTypes'];

        return $AllowedFileAttachmentTypes;
    }

    /*   Get Website AllowedFileAttachmentTypes  */
    function getAllowedFileAttachmentTypes()
    {
        global $link;
        $sql = "SELECT `AllowedFileAttachmentTypes` FROM `generalsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $AllowedFileAttachmentTypes = $result['AllowedFileAttachmentTypes'];
        $AllowedFileAttachmentTypes = explode(',',$AllowedFileAttachmentTypes);

        return $AllowedFileAttachmentTypes;
    }

    /*   Get Website reCaptchaAllowed  */
    function getreCaptchaAllowed()
    {
        global $link;
        $sql = "SELECT `recaptchaAllowed` FROM `generalsecurity`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $recaptchaAllowed = $result['recaptchaAllowed'];

        return $recaptchaAllowed;
    }

    /*   Get Website reCaptchaSiteKey  */
    function getreCaptchaSiteKey()
    {
        global $link;
        $sql = "SELECT `reCaptchaSiteKey` FROM `generalsecurity`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $reCaptchaSiteKey = $result['reCaptchaSiteKey'];

        return $reCaptchaSiteKey;
    }

    /*   Get Website reCaptchaSecretKey  */
    function getreCaptchaSecretKey()
    {
        global $link;
        $sql = "SELECT `reCaptchaSecretKey` FROM `generalsecurity`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $reCaptchaSecretKey = $result['reCaptchaSecretKey'];

        return $reCaptchaSecretKey;
    }

    /*   Get Website MinimumUserPasswordStrength  */
    function getMinimumUserPasswordStrength()
    {
        global $link;
        $sql = "SELECT `MinimumUserPasswordStrength` FROM `generalsecurity`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $MinimumUserPasswordStrength = $result['MinimumUserPasswordStrength'];

        return $MinimumUserPasswordStrength;
    }

    /*   Get Website MinimumUserPasswordLength  */
    function getMinimumUserPasswordLength()
    {
        global $link;
        $sql = "SELECT `MinimumUserPasswordLength` FROM `generalsecurity`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $MinimumUserPasswordLength = $result['MinimumUserPasswordLength'];

        return $MinimumUserPasswordLength;
    }

    /*   Get Website MaximumUserPasswordLength  */
    function getMaximumUserPasswordLength()
    {
        global $link;
        $sql = "SELECT `MaximumUserPasswordLength` FROM `generalsecurity`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $MaximumUserPasswordLength = $result['MaximumUserPasswordLength'];

        return $MaximumUserPasswordLength;
    }

    /*   Get  hide Last Name Of Member or not  */
    function gethideLastNameOfMember()
    {
        global $link;
        $sql = "SELECT `hideLastNameOfMember` FROM `generalsecurity`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $hideLastNameOfMember = $result['hideLastNameOfMember'];

        return $hideLastNameOfMember;
    }

    /*   Get Default Timezone  */
    function getDefaultTimeZone()
    {
        global $link;
        $sql = "SELECT `DefaultTimeZone` FROM `localizationsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $DefaultTimeZone = $result['DefaultTimeZone'];

        return $DefaultTimeZone;
    }

    /*   Get Default currency  */
    function getDefaultCurrency()
    {
        global $link;
        $sql = "SELECT `DefaultCurrency` FROM `localizationsetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $DefaultCurrency = $result['DefaultCurrency'];

        if($DefaultCurrency=='' || $DefaultCurrency==null)
        {
            $DefaultCurrency = 'United States Dollar';
        }
        return $DefaultCurrency;
    }

    /*   Get Default currency Code  */
    function getDefaultCurrencyCode($x)
    {
        global $link;
        $sql = "SELECT `code` FROM `currency` WHERE currency='$x'";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $code = $result['code'];

        if($code=='' || $code==null)
        {
            $code = 'USD';
        }

        return $code;
    }
    
    /* Function to generate 8 digit random unique code */
    function generateRandomString()  
    {
        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $digits = '1234567890';
        $randomString = '';
        for ($i = 0; $i < 5; $i++) 
        {
            $randomString .= $letters[rand(0, strlen($letters) - 1)];
        }
        for ($i = 0; $i < 5; $i++) 
        {
            $randomString .= $digits[rand(0, strlen($digits) - 1)];
        }
        return $randomString;
    }

    
    /* User Info */
    function removeBrackets($x)
    {
        $result = str_replace("}","",str_replace("{","",$x));
        return $result;
    }

    function fetchEmailTemplateNames()
    {
        global $link;

        $sql = "SELECT * FROM `email_template_list`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /* update option of select box as selected*/
    function manageadminrolecheckboxchecked($x,$y)
    {
        if($x == $y)
        {
            echo 'selected';
        }
        //return $y;
    }

    /* function to check the radio nutton is checked or not  */
    function adminroleradiochecked($x,$y)
    {
        if($x == $y)
        {
            echo 'checked';
        }
    }

    /*  Fetch homepage banner */
    function gethomepage_bannerURL()
    {
        global $link;
        $sql = "SELECT `homepagebanner` FROM `homepagesetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $homepagebannerURL = $result['homepagebanner'];

        return $homepagebannerURL;
    }

    /*  Fetch homepage heading */
    function gethomepageHeading()
    {
        global $link;
        $sql = "SELECT `homepageHeading` FROM `homepagesetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $homepageHeading = $result['homepageHeading'];

        return $homepageHeading;
    }

    /*  Fetch Extra content strip display or not */
    function getextracontentstripshow()
    {
        global $link;
        $sql = "SELECT `extracontentstripshow` FROM `homepagesetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $extracontentstripshow = $result['extracontentstripshow'];

        return $extracontentstripshow;
    }

    /*  Fetch Extra content strip data */
    function getextracontentstripdata()
    {
        global $link;
        $sql = "SELECT `extracontentstripdata` FROM `homepagesetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $extracontentstripdata = $result['extracontentstripdata'];

        return $extracontentstripdata;
    }


    /*  Fetch filter content strip display or not */
    function getprofilefiltershow()
    {
        global $link;
        $sql = "SELECT `profilefiltershow` FROM `homepagesetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $profilefiltershow = $result['profilefiltershow'];

        return $profilefiltershow;
    }

    /*  Fetch filters for profiles content strip data */
    function getprofilefiltervalues()
    {
        global $link;
        $sql = "SELECT `profilefiltervalues` FROM `homepagesetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $profilefiltervalues = $result['profilefiltervalues'];

        return $profilefiltervalues;
    }


    /*  Fetch footer content strip display or not */
    function getfootercontentshow()
    {
        global $link;
        $sql = "SELECT `footercontentshow` FROM `homepagesetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $footercontentshow = $result['footercontentshow'];

        return $footercontentshow;
    }

    /*  Fetch footer content strip data */
    function getfootercontent()
    {
        global $link;
        $sql = "SELECT `footercontent` FROM `homepagesetup`";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $footercontent = $result['footercontent'];

        return $footercontent;
    }


    function getsettingoption($x)
    {
        global $link;
        $sql_permission = "SELECT `WebSiteTitle` FROM `generalsetup` ";
        $stmt   = $link->prepare($sql_permission);
        $stmt->execute();
        $row_role = $stmt->fetch();
        $comp_name =$row_role['WebSiteTitle'];
        return $comp_name;  
    }

    function encrypt_decrypt($action, $string) 
    {
        $output = false;
        $key = 'pujan-enterprises';
        $method = 'aes-256-cbc';

        $password = substr(hash('sha256', $key, true), 0, 32);
       
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
       if( $action == 'encrypt' ) 
       {
           $output = base64_encode(openssl_encrypt($string, $method, $password, OPENSSL_RAW_DATA, $iv));
        }
        else if( $action == 'decrypt' )
        {
            $output = openssl_decrypt(base64_decode($string), $method, $password, OPENSSL_RAW_DATA, $iv);
            $output = rtrim($output, "");
        }
        return $output;
    }

    // About us display or not
    function getAboutUsSetting()
    {
        global $link;
        $res_count  = "SELECT display FROM `about_us`";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $display = $row_data['display'];
        return $display;
    }

    // Contact us display or not
    function getContactUsSetting()
    {
        global $link;
        $res_count  = "SELECT display FROM `contact_us`";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $display = $row_data['display'];
        return $display;
    }

    // Disclaimer display or not
    function getDisclaimerSetting()
    {
        global $link;
        $res_count  = "SELECT display FROM `disclaimer`";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $display = $row_data['display'];
        return $display;
    }

    // Privacy Policy display or not
    function getPrivacyPolicySetting()
    {
        global $link;
        $res_count  = "SELECT display FROM `privacy_policy`";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $display = $row_data['display'];
        return $display;
    }

    // Terms Of Service display or not
    function getTermsOfServiceSetting()
    {
        global $link;
        $res_count  = "SELECT display FROM `terms_of_service`";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $display = $row_data['display'];
        return $display;
    }

    // FAQ's display or not
    function getFAQSetting()
    {
        global $link;
        $res_count  = "SELECT display FROM `faq_page_show`";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $display = $row_data['display'];
        return $display;
    }

    function user_occ($x)
    {
        global $link;
        $res_count  = "SELECT name FROM `employment` WHERE `id`='$x'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $occ_name = $row_data['name'];
        return $occ_name;
    }

    /***********     Client Registered On     *************/
    function getUserRegisteredOn($id)
    {
        global $link;
        $res_count  = "SELECT created_at FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $created_at = $row_data['created_at'];
        return $created_at;
    }

    /***********     Client Updated On     *************/
    function getUserUpdatedOn($id)
    {
        global $link;
        $res_count  = "SELECT updated_at FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $updated_at = $row_data['updated_at'];
        return $updated_at;
    }

    /***********     Client Email Verified Or Not     *************/
    function getUserVerifiedOrNot($id)
    {
        global $link;
        $res_count  = "SELECT isEmailVerified FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $isEmailVerified = $row_data['isEmailVerified'];
        return $isEmailVerified;
    }

    /***********     Client Email Verified On     *************/
    function getUserVerifiedOn($id)
    {
        global $link;
        $res_count  = "SELECT isEmailVerifiedOn FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $isEmailVerifiedOn = $row_data['isEmailVerifiedOn'];
        return $isEmailVerifiedOn;
    }

    function getUserFirstName($id)
    {
        global $link;
        $res_count  = "SELECT firstname FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $firstname = $row_data['firstname'];
        return $firstname;
    }

    function getUserLastName($id)
    {
        global $link;
        $res_count  = "SELECT lastname FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $lastname = $row_data['lastname'];
        return $lastname;
    }

    function getUserDOB($id)
    {
        global $link;
        $res_count  = "SELECT dob FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $dob = $row_data['dob'];
        return $dob;
    }

    function getUserGender($id)
    {
        global $link;
        $res_count  = "SELECT gender FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $gender = $row_data['gender'];
        return $gender;
    }

    function getUserLookingFor($id)
    {
        global $link;
        $res_count  = "SELECT look_for FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $look_for = $row_data['look_for'];
        return $look_for;
    }

    function getUserUniqueCode($id)
    {
        global $link;
        $res_count  = "SELECT unique_code FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $unique_code = $row_data['unique_code'];
        return $unique_code;
    }

    function getUserIdFromUniqueId($profileid)
    {
        global $link;
        $res_count  = "SELECT id FROM `clients` WHERE `unique_code`='$profileid'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $id = $row_data['id'];
        return $id;
    }

    function getUserEmail($id)
    {
        global $link;
        $res_count  = "SELECT email FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $email = $row_data['email'];
        return $email;
    }

    function getUserIdFromEmail($email)
    {
        global $link;
        $res_count  = "SELECT id FROM `clients` WHERE `email`='$email'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $id = $row_data['id'];
        return $id;
    }


    function getUserCountryPhoneCode($id)
    {
        global $link;
        $res_count  = "SELECT phonecode FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $phonecode = $row_data['phonecode'];
        return $phonecode;
    }

    function getUserMobile($id)
    {
        global $link;
        $res_count  = "SELECT phonenumber FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $phonenumber = $row_data['phonenumber'];
        return $phonenumber;
    }

    function getUserHeight($id)
    {
        global $link;
        $res_count  = "SELECT height FROM `profilebasic` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $height = $row_data['height'];
        return $height;
    }

    function getUserWeight($id)
    {
        global $link;
        $res_count  = "SELECT weight FROM `profilebasic` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $weight = $row_data['weight'];
        return $weight;
    }

    function getUserStatus($id)
    {
        global $link;
        $res_count  = "SELECT status FROM `clients` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $status = $row_data['status'];
        return $status;
    }

    /***********     User City Id     ************/
    function getUserCityId($id)
    {
        global $link;
        $res_count  = "SELECT city FROM `address` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $city = $row_data['city'];
        return $city;
    }

    function getUserCityName($id)
    {
        global $link;
        $res_count  = "SELECT name FROM `cities` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $cityName = $row_data['name'];
        return $cityName;
    }

    /**********      User State Id      *************/
    function getUserStateId($id)
    {
        global $link;
        $res_count  = "SELECT state FROM `address` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $state = $row_data['state'];
        return $state;
    }
    function getUserStateName($id)
    {
        global $link;
        $res_count  = "SELECT name FROM `states` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $StateName = $row_data['name'];
        return $StateName;
    }

    /**************   User Country Id   *************/
    function getUserCountryId($id)
    {
        global $link;
        $res_count  = "SELECT country FROM `address` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $country = $row_data['country'];
        return $country;
    }

    function getUserCountryName($id)
    {
        global $link;
        $res_count  = "SELECT name FROM `countries` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $CountryName = $row_data['name'];
        return $CountryName;
    }

    /***********     Religion     **********/
    function getUserReligionId($id)
    {
        global $link;
        $res_count  = "SELECT religion FROM `profilereligion` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $religion = $row_data['religion'];
        return $religion;
    }

    function getUserReligionName($id)
    {
        global $link;
        $res_count  = "SELECT name FROM `religion` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $ReligionName = $row_data['name'];
        return $ReligionName;
    }

    /***********     Caste     **********/
    function getUserCastId($id)
    {
        global $link;
        $res_count  = "SELECT caste FROM `profilereligion` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $caste = $row_data['caste'];
        return $caste;
    }

    function getUserCastName($id)
    {
        global $link;
        $res_count  = "SELECT name FROM `caste` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $CastName = $row_data['name'];
        return $CastName;
    }

    /***********     Mother Tongue     **********/
    function getUserMotherTongueId($id)
    {
        global $link;
        $res_count  = "SELECT mother_tongue FROM `profilereligion` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $mother_tongue = $row_data['mother_tongue'];
        return $mother_tongue;
    }
    function getUserMotherTongueName($id)
    {
        global $link;
        $res_count  = "SELECT name FROM `mothertongue` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $MotherTongueName = $row_data['name'];
        return $MotherTongueName;
    }

    /********    Education Id    ********/
    function getUserEducationId($id)
    {
        global $link;
        $res_count  = "SELECT education FROM `eduocc` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $education = $row_data['education'];
        return $education;
    }
    function getUserEducationName($id)
    {
        global $link;
        $res_count  = "SELECT name FROM `educationname` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $EducationName = $row_data['name'];
        return $EducationName;
    }

    /***********     Employment     ***************/
    function getUserEmploymentId($id)
    {
        global $link;
        $res_count  = "SELECT occupation FROM `eduocc` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $occupation = $row_data['occupation'];
        return $occupation;
    }

    function getUserEmploymentName($id)
    {
        global $link;
        $res_count  = "SELECT name FROM `employment` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $EmploymentName = $row_data['name'];
        return $EmploymentName;
    }

    /***********     Designation     ***************/
    function getUserDesignationId($id)
    {
        global $link;
        $res_count  = "SELECT designation FROM `eduocc` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $designation = $row_data['designation'];
        return $designation;
    }

    function getUserDesignationName($id)
    {
        global $link;
        $res_count  = "SELECT name FROM `designation` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $DesignationName = $row_data['name'];
        return $DesignationName;
    }

    /***********     Designation Industry     ***************/
    function getUserDesignationCategoryId($id)
    {
        global $link;
        $res_count  = "SELECT designation_category FROM `eduocc` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $designation_category = $row_data['designation_category'];
        return $designation_category;
    }
    function getUserDesignationCategoryName($id)
    {
        global $link;
        $res_count  = "SELECT name FROM `designation_category` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $DesignationCategoryName = $row_data['name'];
        return $DesignationCategoryName;
    }
    /**********    Monthly Income    **********/
    function getUserMonthlyIncome($id)
    {
        global $link;
        $res_count  = "SELECT income FROM `eduocc` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $income = $row_data['income'];
        return $income;
    }


    /*************   Currency Name    ****************/
    function getUserIncomeCurrency($id)
    {
        global $link;
        $res_count  = "SELECT income_currency FROM `eduocc` WHERE `userid`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $income_currency = $row_data['income_currency'];
        return $income_currency;
    }

    function getUserIncomeCurrencyName($id)
    {
        global $link;
        $res_count  = "SELECT currency FROM `currency` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $currency = $row_data['currency'];
        return $currency;
    }

    function getUserIncomeCurrencyCode($id)
    {
        global $link;
        $res_count  = "SELECT code FROM `currency` WHERE `id`='$id'";
        $stmt   = $link->prepare($res_count);
        $stmt->execute();
        $row_data = $stmt->fetch();
        $IncomeCurrencyCode = $row_data['code'];
        return $IncomeCurrencyCode;
    }
    
    function getUserProfilePic($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `profilepic` WHERE `userid`='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $photo = $result['photo'];
        return $photo;
    }

    function unblockbtn($x,$y)
    {
        global $link;
        $sql_permission = "SELECT * FROM `blockedusers` WHERE blocker='$x' AND blockedUser='$y' AND status='blocked'";
        $stmt   = $link->prepare($sql_permission);
        $stmt->execute();
        $count = $stmt->rowCount();
        return $count;
    }

    function shortlistbtn($x,$y)
    {
        global $link;
        $sql_permission = "SELECT COUNT(*) as total FROM `shortlisted` WHERE userid='$x' AND short_id='$y'";
        $stmt   = $link->prepare($sql_permission);
        $stmt->execute();
        $row_role = $stmt->fetch();
        $slug =$row_role['total'];
        return $slug;
    }

    function request_photo_btn($x,$y)
    {
        global $link;
        $sql_permission = "SELECT COUNT(*) as total FROM `requestphoto` WHERE userid='$x' AND requested_id='$y'";
        $stmt   = $link->prepare($sql_permission);
        $stmt->execute();
        $row_role = $stmt->fetch();
        $slug =$row_role['total'];
        return $slug;
    }

    function sendinterestbtn($x,$y)
    {
        global $link;
        $sql_permission = "SELECT COUNT(*) as total FROM `interested` WHERE userid='$x' AND inter_id='$y'";
        $stmt   = $link->prepare($sql_permission);
        $stmt->execute();
        $row_role = $stmt->fetch();
        $slug =$row_role['total'];
        return $slug;
    }

    /*   Body type Name */
    function getUserBodyTypeId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT body_type FROM `profilebasic` WHERE `userid`='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $bodytype = $result['body_type'];
        return $bodytype;
    }
    function getUserBodyTypeName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `bodytype` WHERE `id`='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*   Complexion Name */

    function getUserComplexionId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT complexion FROM `profilebasic` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $complexion = $result['complexion'];
        return $complexion;
    }

    function getUserComplexionName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `complexion` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*   Eating Habbit Name */
    function getUserEatHabbitId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT eat_habbit FROM `profilebasic` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $eat_habbit = $result['eat_habbit'];
        return $eat_habbit;
    }

    function getUserEatHabbitName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `eathabbit` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*   Marital Status Name */
    function getUserMaritalStatusId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT marital_status FROM `profilebasic` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $marital_status = $result['marital_status'];
        return $marital_status;
    }

    function getUserMaritalStatusName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `maritalstatus` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*   Smocking Name */
    function getUserSmockingHabbitId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT smoke_habbit FROM `profilebasic` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $smoke_habbit = $result['smoke_habbit'];
        return $smoke_habbit;
    }

    function getUserSmockingHabbitName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `smokehabbit` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*   Special Case Name */
    function getUserSpecialCaseId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT special_case FROM `profilebasic` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $special_case = $result['special_case'];
        return $special_case;
    }

    function getUserSpecialCaseName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `specialcases` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*   Special Case Name */
    function getUserDrinkHabbitId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT drink_habbit FROM `profilebasic` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $drink_habbit = $result['drink_habbit'];
        return $drink_habbit;
    }

    function getUserDrinkHabbitName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `drinkhabbit` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*   Industry Name */
    function getUserIndustryId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT industry FROM `eduocc` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $industry = $result['industry'];
        return $industry;
    }
    function getUserIndustryName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `industry` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*   Family Value Name */
    function getUserFamilyValueId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT fam_val FROM `family` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $fam_val = $result['fam_val'];
        return $fam_val;
    }
    function getUserFamilyValueName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `familyvalue` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*   Family Type Name */
    function getUserFamilyTypeId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT fam_type FROM `family` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $fam_type = $result['fam_type'];
        return $fam_type;
    }

    function getUserFamilyTypeName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `familytype` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*   Family Type Name */
    function getUserFamilyStateId($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT fam_stat FROM `family` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $fam_stat = $result['fam_stat'];
        return $fam_stat;
    }

    function getUserFamilyStateName($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT * FROM `familystatus` WHERE id='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /*********    User About Myself      *********/
    function getUserAboutMyself($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT short_desc FROM `profiledesc` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $short_desc = $result['short_desc'];
        return $short_desc;
    }

    /*********    User Myself      *********/
    function getUserAboutFamily($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT short_desc_family FROM `profiledesc` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $short_desc_family = $result['short_desc_family'];
        return $short_desc_family;
    }

    /*********    User About Interest      *********/
    function getUserAboutInterest($id)
    {
        global $link;
        $stmt = $link->prepare("SELECT short_desc_interest FROM `profiledesc` WHERE userid='$id'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $short_desc_interest = $result['short_desc_interest'];
        return $short_desc_interest;
    }


    /****  Check user logged in or not   ****/
    /****  Returns 0 if not logged in & returns 1 if logged in   ****/
    function CheckUserLoggedIn()
    {
        if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
        {
            $loggedIn = '0';
        }
        else
        if(isset($_SESSION['logged_in']) && ($_SESSION['client_user']!='' || $_SESSION['client_user']!=null))
        {
            $loggedIn = '1';
        }

        return $loggedIn;
    }

    /************   Get total brides   *************/
    function getBrideCount()
    {
        global $link;
        $stmt = $link->prepare("SELECT COUNT(`id`) AS bride_count FROM `clients` WHERE `gender`='2'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $bride_count = $result['bride_count'];
        return $bride_count;
    }

    /************   Get total NeverMarrideBrides   *************/
    function getNeverMarrideBrides()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='1' AND A.`gender`='2'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $never_married_bride_count = $count;
        return $never_married_bride_count;
    }

    /************   Get total AwaitingDivorceBrides   *************/
    function getAwaitingDivorceBrides()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='2' AND A.`gender`='2'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $awaiting_divorce_bride_count = $count;
        return $awaiting_divorce_bride_count;
    }

    /************   Get total DivorceBrides   *************/
    function getDivorceBrides()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='3' AND A.`gender`='2'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $divorce_bride_count = $count;
        return $divorce_bride_count;
    }

    /************   Get total WidowedBrides   *************/
    function getWidowedBrides()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='4' AND A.`gender`='2'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $widowed_bride_count = $count;
        return $widowed_bride_count;
    }

    /************   Get total AnnulledBrides   *************/
    function getAnnulledBrides()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='5' AND A.`gender`='2'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $annulled_bride_count = $count;
        return $annulled_bride_count;
    }

    /************   Get total ProfileIncompleteBrides   *************/
    function getProfileIncompleteBrides()
    {
        global $link;
        $stmt = $link->prepare("SELECT `id` FROM `clients` WHERE `gender`='2' AND `id` NOT IN (SELECT `userid` FROM `profilebasic`)"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $profileincomplete_bride_count = $count;
        return $profileincomplete_bride_count;
    }

    /************   Get total Groome   *************/
    function getGroomCount()
    {
        global $link;
        $stmt = $link->prepare("SELECT COUNT(`id`) AS groom_count FROM `clients` WHERE `gender`='1'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $groom_count = $result['groom_count'];
        return $groom_count;
    }

    /************   Get total NeverMarrideGroom   *************/
    function getNeverMarrideGroom()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='1' AND A.`gender`='1'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $never_married_groom_count = $count;
        return $never_married_groom_count;
    }

    /************   Get total AwaitingDivorceGroom   *************/
    function getAwaitingDivorceGroom()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='2' AND A.`gender`='1'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $awaiting_divorce_groom_count = $count;
        return $awaiting_divorce_groom_count;
    }

    /************   Get total DivorceGroom   *************/
    function getDivorceGroom()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='3' AND A.`gender`='1'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $divorce_groom_count = $count;
        return $divorce_groom_count;
    }

    /************   Get total WidowedGroom   *************/
    function getWidowedGroom()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='4' AND A.`gender`='1'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $widowed_groom_count = $count;
        return $widowed_groom_count;
    }

    /************   Get total AnnulledGroom   *************/
    function getAnnulledGroom()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='5' AND A.`gender`='1'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $annulled_groom_count = $count;
        return $annulled_groom_count;
    }

    /************   Get total ProfileIncompleteGroom   *************/
    function getProfileIncompleteGroom()
    {
        global $link;
        $stmt = $link->prepare("SELECT `id` FROM `clients` WHERE `gender`='1' AND `id` NOT IN (SELECT `userid` FROM `profilebasic`)"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $profileincomplete_groom_count = $count;
        return $profileincomplete_groom_count;
    }

    /************   Get total T-Gender   *************/
    function getTGenderCount()
    {
        global $link;
        $stmt = $link->prepare("SELECT COUNT(`id`) AS t_gender_count FROM `clients` WHERE `gender`='3'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $t_gender_count = $result['t_gender_count'];
        return $t_gender_count;
    }

    /************   Get total NeverMarrideTGender   *************/
    function getNeverMarrideTGender()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='1' AND A.`gender`='3'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $never_married_t_gender_count = $count;
        return $never_married_t_gender_count;
    }

    /************   Get total AwaitingDivorceTGender   *************/
    function getAwaitingDivorceTGender()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='2' AND A.`gender`='3'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $awaiting_divorce_t_gender_count = $count;
        return $awaiting_divorce_t_gender_count;
    }

    /************   Get total DivorceTGender   *************/
    function getDivorceTGender()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='3' AND A.`gender`='3'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $divorce_t_gender_count = $count;
        return $divorce_t_gender_count;
    }

    /************   Get total WidowedTGender   *************/
    function getWidowedTGender()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='4' AND A.`gender`='3'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $widowed_t_gender_count = $count;
        return $widowed_t_gender_count;
    }

    /************   Get total AnnulledTGender   *************/
    function getAnnulledTGender()
    {
        global $link;
        $stmt = $link->prepare("SELECT A.`id`,B.`userid` FROM `clients` AS A JOIN `profilebasic` AS B ON A.`id`=B.`userid` WHERE B.`marital_status`='5' AND A.`gender`='1'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $annulled_t_gender_count = $count;
        return $annulled_t_gender_count;
    }

    /************   Get total ProfileIncompleteTGender   *************/
    function getProfileIncompleteTGender()
    {
        global $link;
        $stmt = $link->prepare("SELECT `id` FROM `clients` WHERE `gender`='3' AND `id` NOT IN (SELECT `userid` FROM `profilebasic`)"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $profileincomplete_t_gender_count = $count;
        return $profileincomplete_t_gender_count;
    }

    function getAdminFirstName($x)
    {
        global $link;
        $stmt = $link->prepare("SELECT `firstname` FROM `admins` WHERE `id`='$x'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $firstname = $result['firstname'];
        return $firstname;
    }

    function getCompanyEmail()
    {
        global $link;
        $stmt = $link->prepare("SELECT `EmailAddress` FROM `generalsetup`"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $EmailAddress = $result['EmailAddress'];
        return $EmailAddress;
    }


    /***********  Get family type of user $x    ************/
    function getUserFamilyTypeUser($x)
    {
        global $link;
        $stmt = $link->prepare("SELECT A.*,B.name FROM `family` AS A JOIN familytype AS B ON A.fam_type = B.id WHERE A.`userid`='$x'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /***********  Get family value of user $x    ************/
    function getUserFamilyValueUser($x)
    {
        global $link;
        $stmt = $link->prepare("SELECT A.*,B.name FROM `family` AS A JOIN familyvalue AS B ON A.fam_val = B.id WHERE A.`userid`='$x'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /***********  Get family status of user $x    ************/
    function getUserFamilyStatusUser($x)
    {
        global $link;
        $stmt = $link->prepare("SELECT A.*,B.name FROM `family` AS A JOIN familystatus AS B ON A.fam_stat = B.id WHERE A.`userid`='$x'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }


    /************   Get User City   *************/
    function getUserCity($x)
    {
        global $link;
        $stmt = $link->prepare("SELECT A.*,B.name FROM `address` AS A JOIN cities AS B ON A.city = B.id WHERE A.`userid`='$x'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /************   Get User State   *************/
    function getUserState($x)
    {
        global $link;
        $stmt = $link->prepare("SELECT A.*,B.name FROM `address` AS A JOIN states AS B ON A.state = B.id WHERE A.`userid`='$x'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /************   Get User Country   *************/
    function getUserCountry($x)
    {
        global $link;
        $stmt = $link->prepare("SELECT A.*,B.name FROM `address` AS A JOIN countries AS B ON A.country = B.id WHERE A.`userid`='$x'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        return $name;
    }

    /************   Get User Age   *************/
    function getUserAge($x)
    {
        global $link;
        $stmt = $link->prepare("SELECT dob FROM clients WHERE `id`='$x'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $dob = $result['dob'];

        $age = (date('Y') - date('Y',strtotime($dob)));

        return $age;
    }

    /************   Get User Education   *************/
    function getUserEducation($x)
    {
        global $link;
        $stmt = $link->prepare("SELECT A.education,B.name FROM eduocc AS A JOIN educationname AS B ON A.education=B.id WHERE A.`userid`='$x'"); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $name = $result['name'];
        
        return $name;
    }


    /************   Get User Interest count   *************/
    function getUserInterestSent($x)
    {
        global $link;
        $stmt = "SELECT * FROM `interested` WHERE `userid`='$x'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        return $count;
    }

    /************   Get User Interest received count   *************/
    function getUserInterestReceived($x)
    {
        global $link;
        $stmt = "SELECT * FROM `interested` WHERE `inter_id`='$x'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        return $count;
    }

    /************   Get User shortlisted profile count   *************/
    function getUsershortlistedProfiles($x)
    {
        global $link;
        $stmt = "SELECT * FROM `shortlisted` WHERE `userid`='$x'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        return $count;
    }

    /************   Get profile count whshortlisted user profile  *************/
    function getUsershortlistedYourProfile($x)
    {
        global $link;
        $stmt = "SELECT * FROM `shortlisted` WHERE `short_id`='$x'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        return $count;
    }

    /************   Get users second last logged in time  *************/
    function getSecondLastUserLoginDate($x)
    {
        global $link;
        $stmt = "SELECT MAX(loggedOn) AS loggedOn FROM userlogs WHERE userid='$x' AND loggedOn < (SELECT MAX(loggedOn) FROM userlogs WHERE userid='$x')"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $secondlastLoginTime = $result['loggedOn'];
        return $secondlastLoginTime;
    }

    /************   Get new message count user profile  *************/
    function getUserNewMessage($x)
    {
        global $link;

        $Second_last_login = getSecondLastUserLoginDate($x);
        //$last_login = getLastUserLoginDate($x);

        $stmt = "SELECT * FROM `messagechat` WHERE `userTo`='$x' AND (messagedOn>='$Second_last_login')"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        return $count;
    }

    /************   check profile_completness starts_or not  *************/
    function getUserProfileCompleteEntry($x)
    {
        global $link;
        $stmt = "SELECT * FROM `profile_completeness` WHERE `userid`='$x'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        //echo $count;
        return $count;
    }

    /************   Get users last logged in time  *************/
    function getLastUserLoginDate($x)
    {
        global $link;
        $stmt = "SELECT * FROM (SELECT * FROM userlogs WHERE `userid`='$x' AND user_type='Client' ORDER BY Id DESC LIMIT 2) userlogs ORDER BY Id LIMIT 1";
         
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $lastLoginTime = $result['loggedOn'];
        return $lastLoginTime;
    }

    /************   Get users last logged in time  *************/
    function getLastUserLoginIP($x)
    {
        global $link;
        $stmt = "SELECT * FROM `userlogs` WHERE `userid`='$x' AND user_type='Client' ORDER BY id DESC"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $lastLoginIP = $result['IP_Address'];
        return $lastLoginIP;
    }

    /************   Get users match count  *************/
    function getUserMatchCount($x)
    {
        global $link;
        $where=null;
        $sqlRec=null;
        $sql_search_preference = "SELECT * FROM preferences WHERE userid='$x'";
        $stmt   = $link->prepare($sql_search_preference);
        $stmt->execute();
        $count = $stmt->rowCount();
        if($count>0)
        {
            $search_preference = $stmt->fetch();
            $gender = $search_preference['gender'];
            $marital_status = $search_preference['marital_status'];
            $city = $search_preference['city'];
            $state = $search_preference['state'];
            $country = $search_preference['country'];
            $religion = $search_preference['religion'];
            $mother_tongue = $search_preference['mother_tongue'];
            $caste = $search_preference['caste'];
            $education = $search_preference['education'];
            $occupation = $search_preference['occupation'];
            $currency = $search_preference['currency'];
            $monthly_income_from = $search_preference['monthly_income_from'];
            $monthly_income_to = $search_preference['monthly_income_to'];
            $from_date = $search_preference['from_date'];
            $to_date = $search_preference['to_date'];
            $body_type = $search_preference['body_type'];
            $complexion = $search_preference['complexion'];
            $height_from = $search_preference['height_from'];
            $height_to = $search_preference['height_to'];
            $fam_type = $search_preference['fam_type'];
            $smoke_habbit = $search_preference['smoke_habbit'];
            $drink_habbit = $search_preference['drink_habbit'];
            $eat_habbit = $search_preference['eat_habbit'];

            $sql = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
            clients as A 
            LEFT OUTER JOIN
            profilebasic AS B ON A.id = B.userid
            LEFT OUTER JOIN
            address AS C ON A.id = C.userid
            LEFT OUTER JOIN
            profilereligion AS D ON A.id = D.userid
            LEFT OUTER JOIN
            eduocc AS E ON A.id = E.userid
            LEFT OUTER JOIN
            profilepic AS F ON A.id = F.userid
            LEFT OUTER JOIN
            family AS G ON A.id = G.userid
            WHERE A.gender='$gender' ";

            /******  Profile Client Start *****/
                if($from_date=='0')      // Age form
                {
                    $where .="";
                }
                else
                {
                    $age_from = date('Y-m-d', strtotime('-'.$from_date.' years'));
                    $where .=" AND A.dob >='$age_from'";
                }

                if($to_date=='0')      // Age to
                {
                    $where .="";
                }
                else
                {
                    $age_from_chk = strtotime($age_from);
                    $age_to = date('Y-m-d',strtotime("+".$to_date." years",$age_from_chk));
                    $where .=" AND A.dob <='$age_to'";
                }
            /******  Profile Client End *****/

            /******  Profile Basic Start *****/
                if($marital_status=='0' || $marital_status=='' || $marital_status==null)         // Marital status
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.marital_status IN($marital_status)";
                }

                if($body_type=='0' || $body_type=='-1')         // body_type
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.body_type ='$body_type'";
                }

                if($complexion=='0' || $complexion=='-1')         // complexion
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.complexion ='$complexion'";
                }

                if($height_from=='0')         // height_from
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.height_from >='$height_from'";
                }

                if($height_to=='0')         // height_to
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.height_to <='$height_to'";
                }

                if($fam_type=='0' || $fam_type=='-1')         // fam_type
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.fam_type ='$fam_type'";
                }

                if($smoke_habbit=='0' || $smoke_habbit=='-1')         // smoke_habbit
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.smoke_habbit ='$smoke_habbit'";
                }

                if($drink_habbit=='0' || $drink_habbit=='-1')         // drink_habbit
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.drink_habbit ='$drink_habbit'";
                }

                if($eat_habbit=='0' || $eat_habbit=='-1')         // eat_habbit
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.eat_habbit ='$eat_habbit'";
                }

                if($eat_habbit=='0' || $eat_habbit=='-1')         // eat_habbit
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.eat_habbit ='$eat_habbit'";
                }
            /******  Profile Basic End *****/


            /******  Profile location Start *****/
                if($city=='0' || $city=='-1')    // City
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND C.city ='$city'";
                }

                if($state=='0' || $state=='-1')     //state
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND C.state ='$state'";
                }

                if($country=='0' || $country=='-1')     //country
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND C.country ='$country'";
                }
            /******  Profile location End *****/


            /******  Profile Education Start *****/
                if($education=='0' || $education=='-1')     //country
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND E.education ='$education'";
                }

                if($occupation=='0' || $occupation=='-1')     //occupation
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND E.occupation ='$occupation'";
                }

                if($currency=='0')     //currency
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND E.currency ='$currency'";
                }

                if($monthly_income_from=='' || $monthly_income_from==null)     //monthly_income_from
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND E.monthly_income_from >='$monthly_income_from'";
                }

                if($monthly_income_to=='' || $monthly_income_to==null)     //monthly_income_to
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND E.monthly_income_to <='$monthly_income_to'";
                }
            /******  Profile Education  *****/


            /******  Profile Relegious Start *****/
                if($religion=='0' || $religion=='-1')     //religion
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND D.country ='$religion'";
                }

                if($mother_tongue=='0' || $mother_tongue=='-1')     //mother_tongue
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND D.mother_tongue ='$mother_tongue'";
                }

                if($caste=='0' || $caste=='-1')     //caste
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND D.caste ='$caste'";
                }

            /******  Profile Relegious End *****/
            
            $where .=" ORDER BY rand()";
            
            $sqlRec .=  $sql.$where;
            //echo $sqlRec;
            $stmt_search   = $link->prepare($sqlRec);
            $stmt_search->execute();
            $sqlTot = $stmt_search->rowCount();
        }
        else
        {
            $sqlTot = '0';
        }    
        return $sqlTot;
    }

    /************  Generate chat Id   ************/
    function generateChatId($x,$y)
    {
        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $digits = '1234567890';
        $randomString = '';
        for ($i = 0; $i < 5; $i++) 
        {
            $randomString .= $letters[rand(0, strlen($letters) - 1)];
        }

        $randomString = $x.$y.$randomString.$y.$x;

        return $randomString;
    }


    /************  Header Script display or not  **************/
    function getHeaderScriptDisplay()
    {
        global $link;
        $stmt = "SELECT header_script_display FROM `custom_script`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $header_script_display = $result['header_script_display'];
        return $header_script_display;
    }

    /************  Header Script data  **************/
    function getHeaderScript()
    {
        global $link;
        $stmt = "SELECT header_script FROM `custom_script`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $header_script = $result['header_script'];
        return $header_script;
    }

    /************  Footer Script display or not  **************/
    function getFooterScriptDisplay()
    {
        global $link;
        $stmt = "SELECT footer_script_display FROM `custom_script`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $footer_script_display = $result['footer_script_display'];
        return $footer_script_display;
    }

    /************  Header Script data  **************/
    function getFooterScript()
    {
        global $link;
        $stmt = "SELECT footer_script FROM `custom_script`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $footer_script = $result['footer_script'];
        return $footer_script;
    }

    /************  check leaderboard ad display or not  **************/
    function getRandomLeaderBoardAdDisplayOrNot()
    {
        global $link;
        $stmt = "SELECT leaderBoardDisplay1,leaderBoardDisplay2,leaderBoardDisplay3 FROM `ads_management`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        if($count>0)
        {
            $leaderBoardDisplay1 = $result['leaderBoardDisplay1'];
            $leaderBoardDisplay2 = $result['leaderBoardDisplay2'];
            $leaderBoardDisplay3 = $result['leaderBoardDisplay3'];

            if($leaderBoardDisplay1!='0' || $leaderBoardDisplay2!='0' || $leaderBoardDisplay3!='0')
            {
                $RandomLeaderBoardAdDisplayOrNot = '1';
            }
            else
            if($leaderBoardDisplay1=='0' || $leaderBoardDisplay2=='0' || $leaderBoardDisplay3=='0')
            {
                $RandomLeaderBoardAdDisplayOrNot = '0';
            }
        }
        else
        {
            $RandomLeaderBoardAdDisplayOrNot = '0';
        }

        return $RandomLeaderBoardAdDisplayOrNot;
    }
    
    /************  Leaderboard random ad data  **************/
    function getRandomLeaderBoardAdData()
    {
        global $link;
        $RandomLeaderBoardAdDataArray=null;
        $stmt = "SELECT leaderBoardData1,leaderBoardData2,leaderBoardData3,leaderBoardDisplay1,leaderBoardDisplay2,leaderBoardDisplay3 FROM `ads_management`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        if($result['leaderBoardDisplay1']=='1')
        {
            $leaderBoardData1 = $result['leaderBoardData1'];
            if($leaderBoardData1!='' || $leaderBoardData1!=null)
            {
                $RandomLeaderBoardAdDataArray[] = $leaderBoardData1;
            }
        }

        if($result['leaderBoardDisplay2']=='1')
        {
            $leaderBoardData2 = $result['leaderBoardData2'];
            if($leaderBoardData2!='' || $leaderBoardData2!=null)
            {
                $RandomLeaderBoardAdDataArray[] = $leaderBoardData2;
            }
        }

        if($result['leaderBoardDisplay3']=='1')
        {
            $leaderBoardData3 = $result['leaderBoardData3'];
            if($leaderBoardData3!='' || $leaderBoardData3!=null)
            {
                $RandomLeaderBoardAdDataArray[] = $leaderBoardData3;
            }
        }

        @$k = array_rand($RandomLeaderBoardAdDataArray);
        $RandomLeaderBoardAdData = $RandomLeaderBoardAdDataArray[$k];

        return $RandomLeaderBoardAdData;
    }

    /************  check square pop-up ad display or not  **************/
    function getRandomSquarePopUpAdDisplayOrNot()
    {
        global $link;
        $stmt = "SELECT squarePopupDisplay1,squarePopupDisplay2,squarePopupDisplay3 FROM `ads_management`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        

        $squarePopupDisplay1 = $result['squarePopupDisplay1'];
        $squarePopupDisplay2 = $result['squarePopupDisplay2'];
        $squarePopupDisplay3 = $result['squarePopupDisplay3'];

        if($count>0)
        {
            $squarePopupDisplay1 = $result['squarePopupDisplay1'];
            $squarePopupDisplay2 = $result['squarePopupDisplay2'];
            $squarePopupDisplay3 = $result['squarePopupDisplay3'];
    
            if($squarePopupDisplay1!='0' || $squarePopupDisplay2!='0' || $squarePopupDisplay3!='0')
            {
                $RandomSquarePopUpAdDisplayOrNot = '1';
            }
            else
            if($squarePopupDisplay1=='0' || $squarePopupDisplay2=='0' || $squarePopupDisplay3=='0')
            {
                $RandomSquarePopUpAdDisplayOrNot = '0';
            }
        }
        else
        {
            $RandomSquarePopUpAdDisplayOrNot = '0';
        }

        return $RandomSquarePopUpAdDisplayOrNot;
    }

    /************  Square popup random ad data  **************/
    function getRandomSquarePopUPAdData()
    {
        global $link;
        $RandomSquarePopUPAdDataArray=null;
        $stmt = "SELECT squarePopupData1,squarePopupData2,squarePopupData3,squarePopupDisplay1,squarePopupDisplay2,squarePopupDisplay3 FROM `ads_management`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        if($result['squarePopupDisplay1']=='1')
        {
            $squarePopupData1 = $result['squarePopupData1'];
            if($squarePopupData1!='' || $squarePopupData1!=null)
            {
                $RandomSquarePopUPAdDataArray[] = $squarePopupData1;
            }
        }

        if($result['squarePopupDisplay2']=='1')
        {
            $squarePopupData2 = $result['squarePopupData2'];
            if($squarePopupData2!='' || $squarePopupData2!=null)
            {
                $RandomSquarePopUPAdDataArray[] = $squarePopupData2;
            }
        }

        if($result['squarePopupDisplay3']=='1')
        {
            $squarePopupData3 = $result['squarePopupData3'];
            if($squarePopupData3!='' || $squarePopupData3!=null)
            {
                $RandomSquarePopUPAdDataArray[] = $squarePopupData3;
            }
        }

        @$k = array_rand($RandomSquarePopUPAdDataArray);
        $RandomSquarePopUpData = $RandomSquarePopUPAdDataArray[$k];

        return $RandomSquarePopUpData;
    }

    /************  check skyscrapper ad display or not  **************/
    function getRandomSkyscrapperAdDisplayOrNot()
    {
        global $link;
        $stmt = "SELECT skyScrapperDisplay1,skyScrapperDisplay2,skyScrapperDisplay3 FROM `ads_management`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        if($count>0)
        {
            $skyScrapperDisplay1 = $result['skyScrapperDisplay1'];
            $skyScrapperDisplay2 = $result['skyScrapperDisplay2'];
            $skyScrapperDisplay3 = $result['skyScrapperDisplay3'];

            if($skyScrapperDisplay1!='0' || $skyScrapperDisplay2!='0' || $skyScrapperDisplay3!='0')
            {
                $RandomSkyscrapperAdDisplayOrNot = '1';
            }
            else
            if($skyScrapperDisplay1=='0' || $skyScrapperDisplay2=='0' || $skyScrapperDisplay3=='0')
            {
                $RandomSkyscrapperAdDisplayOrNot = '0';
            }
        }
        else
        {
            $RandomSkyscrapperAdDisplayOrNot = '0';
        }

        return $RandomSkyscrapperAdDisplayOrNot;
    }

    /************  Skyscrapper random ad data  **************/
    function getRandomSkyScrapperAdData()
    {
        global $link;
        $RandomSkyScrapperAdDataArray=null;
        $stmt = "SELECT skyScrapperData1,skyScrapperData2,skyScrapperData3,skyScrapperDisplay1,skyScrapperDisplay2,skyScrapperDisplay3 FROM `ads_management`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        if($result['skyScrapperDisplay1']=='1')
        {
            $skyScrapperData1 = $result['skyScrapperData1'];
            if($skyScrapperData1!='' || $skyScrapperData1!=null)
            {
                $RandomSkyScrapperAdDataArray[] = $skyScrapperData1;
            }
        }   
        
        if($result['skyScrapperDisplay2']=='1')
        {
            $skyScrapperData2 = $result['skyScrapperData2'];
            if($skyScrapperData2!='' || $skyScrapperData2!=null)
            {
                $RandomSkyScrapperAdDataArray[] = $skyScrapperData2;
            }
        } 

        if($result['skyScrapperDisplay3']=='1')
        {
            $skyScrapperData3 = $result['skyScrapperData3'];
            if($skyScrapperData3!='' || $skyScrapperData3!=null)
            {
                $RandomSkyScrapperAdDataArray[] = $skyScrapperData3;
            }
        } 

        @$k = array_rand($RandomSkyScrapperAdDataArray);
        $RandomSkyScrapperData = $RandomSkyScrapperAdDataArray[$k];

        return $RandomSkyScrapperData;
    }

    /*********    Profile Like Count    ***********/
    function getLikeProfileCount($id)
    {
        global $link;
        $sql_like_profile_count = "SELECT `id` FROM `profile_like` WHERE `liked_userid`='$id'";
        $stmt= $link->prepare($sql_like_profile_count); 
        $stmt->execute();
        $count=$stmt->rowCount();

        return $count;
    }

    /*********    User Profile Like Or Not    ***********/
    function getuserLikedProfileOrNot($x,$y)
    {
        global $link;
        $sql_like_profile_count = "SELECT * FROM `profile_like` WHERE `userid`='$x' AND `liked_userid`='$y'";
        $stmt= $link->prepare($sql_like_profile_count); 
        $stmt->execute();
        $count=$stmt->rowCount();

        return $count;
    }

    /*********   Update profile view count    ************/
    function updateProfileViewCount($id,$profileid)
    {
        global $link;
        $profile_view_count = null;
        $sql_like_profile_view = "SELECT * FROM `profile_views` WHERE `userid`='$id' AND unique_code='$profileid'";
        $stmt_profile_view = $link->prepare($sql_like_profile_view); 
        $stmt_profile_view->execute();
        $count_profile_view = $stmt_profile_view->rowCount();
        $res_profile_view = $stmt_profile_view->fetch();

        if($count_profile_view==0)
        {
            $sql_update_profile_count = "INSERT INTO `profile_views`(`userid`,`unique_code`,`view_count`) VALUES('$id','$profileid','1')";
        }
        else
        if($count_profile_view>0)
        {
            $profile_view_count = $res_profile_view['view_count']+1;
            $sql_update_profile_count = "UPDATE `profile_views` SET `view_count`='$profile_view_count' WHERE `userid`='$id' AND unique_code='$profileid'";
        }
        
        $link->exec($sql_update_profile_count); 
    }


    /*********   Update profile view count    ************/
    function getProfileViewCount($id,$profileid)
    {
        global $link;
        $profile_view_count = null;
        $sql_like_profile_view = "SELECT * FROM `profile_views` WHERE `userid`='$id' AND unique_code='$profileid'";
        $stmt_profile_view = $link->prepare($sql_like_profile_view); 
        $stmt_profile_view->execute();
        $count_profile_view = $stmt_profile_view->rowCount();
        $res_profile_view = $stmt_profile_view->fetch();

        if($count_profile_view>0)
        {
            $profile_view_count = $res_profile_view['view_count'];
        }
        else
        if($count_profile_view==0)
        {
            $profile_view_count = '0';
        }    

        return $profile_view_count;
    }

    /*********    Get social sharing data    ***********/
    function getSocialSharingLinks()
    {
        global $link;
        $primary_color = getPrimaryColor();
        $primary_font_color = getPrimaryFontColor();
        $siteBasePath = getWebsiteBasePath();
        $sql_like_profile_count = "SELECT * FROM `social_sharing`";
        $stmt= $link->prepare($sql_like_profile_count); 
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();

        $website_facebook_display = $result['website_facebook_display'];
        $website_facebook_data = $result['website_facebook_data'];
        $website_twitter_display = $result['website_twitter_display'];
        $website_twitter_data = $result['website_twitter_data'];
        $website_instagram_display = $result['website_instagram_display'];
        $website_instagram_data = $result['website_instagram_data'];
        $website_tumbler_display = $result['website_tumbler_display'];
        $website_tumbler_data = $result['website_tumbler_data'];
        $website_youtube_display = $result['website_youtube_display'];
        $website_youtube_data = $result['website_youtube_data'];
        $website_pinterest_display = $result['website_pinterest_display'];
        $website_pinterest_data = $result['website_pinterest_data'];
        $website_myspace_display = $result['website_myspace_display'];
        $website_myspace_data = $result['website_myspace_data'];
        $website_linkedin_display = $result['website_linkedin_display'];
        $website_linkedin_data = $result['website_linkedin_data'];
        $website_VKontakte_display = $result['website_VKontakte_display'];
        $website_VKontakte_data = $result['website_VKontakte_data'];
        $website_foursquare_display = $result['website_foursquare_display'];
        $website_foursquare_data = $result['website_foursquare_data'];
        $website_flicker_display = $result['website_flicker_display'];
        $website_flicker_data = $result['website_flicker_data'];
        $website_vine_display = $result['website_vine_display'];
        $website_vine_data = $result['website_vine_data'];
        $website_blogger_display = $result['website_blogger_display'];
        $website_blogger_data = $result['website_blogger_data'];
        $website_quora_display = $result['website_quora_display'];
        $website_quora_data = $result['website_quora_data'];
        $website_reddit_display = $result['website_reddit_display'];
        $website_reddit_data = $result['website_reddit_data'];

        $output = null;

        if($count>0)
        {
            $output .= "<div class='row'>";

            if($website_facebook_display=='1')
            {
                $output .=  "<a href='$website_facebook_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/facebook.png'></a>";
            }

            if($website_twitter_display=='1')
            {
                $output .=  "<a href='$website_twitter_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/twitter.png'></a>";
            }

            if($website_instagram_display=='1')
            {
                $output .=  "<a href='$website_instagram_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/instagram-logo.png'></a>";
            }

            if($website_tumbler_display=='1')
            {
                $output .=  "<a href='$website_tumbler_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/tumblr.png'></a>";
            }

            if($website_youtube_display=='1')
            {
                $output .=  "<a href='$website_youtube_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/youtube.png'></a>";
            }

            if($website_pinterest_display=='1')
            {
                $output .=  "<a href='$website_pinterest_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;'><img src='$siteBasePath/images/social-media/pinterest.png'></a>";
            }

            if($website_myspace_display=='1')
            {
                $output .=  "<a href='$website_myspace_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/myspace.png'></a>";
            }

            if($website_linkedin_display=='1')
            {
                $output .=  "<a href='$website_linkedin_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/linkedIn.png'></a>";
            }

            if($website_VKontakte_display=='1')
            {
                $output .=  "<a href='$website_VKontakte_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/vKontakte.png'></a>";
            }

            if($website_foursquare_display=='1')
            {
                $output .=  "<a href='$website_foursquare_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/foursquare.png'></a>";
            }

            if($website_flicker_display=='1')
            {
                $output .=  "<a href='$website_flicker_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/flicker.png'></a>";
            }

            if($website_vine_display=='1')
            {
                $output .=  "<a href='$website_vine_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/vine.png'></a>";
            }

            if($website_blogger_display=='1')
            {
                $output .=  "<a href='$website_blogger_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/blogger.png'></a>";
            }

            if($website_quora_display=='1')
            {
                $output .=  "<a href='$website_quora_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/quora.png'></a>";
            }

            if($website_reddit_display=='1')
            {
                $output .=  "<a href='$website_reddit_data' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;' ><img src='$siteBasePath/images/social-media/reddit.png'></a>";
            }
            $output .= "</div>";
        }
        else
        if($count==0)
        {
            $output .= "<div class='row text-left'>";
            $output .=  "<a href='$siteBasePath' target='_blank' style='padding:5px;margin:5px;color:$primary_font_color;'>$WebSiteTitle</a>";
            $output .= "</div>";
        }

        return $output;

    }


    /**********    Social sharing displayed or not    **********/
    function getSocialSharingDisplayOrNot()
    {
        global $link;
        $stmt = "SELECT social_sharing_display FROM `social_sharing`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $social_sharing_display = $result['social_sharing_display'];

        return $social_sharing_display;
    }

    /**********    Get deactivation reason    **********/
    function getDeactivationReason($x)
    {
        global $link;
        $stmt = "SELECT reason FROM `deactivated_profiles` WHERE task='Deactivate' AND userid='$x' ORDER BY updated_on DESC"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $reason = $result['reason'];

        return $reason;
    }

    /**********    Get deactivation on    **********/
    function getDeactivationOn($x)
    {
        global $link;
        $stmt = "SELECT updated_on FROM `deactivated_profiles` WHERE task='Deactivate' AND userid='$x' ORDER BY updated_on DESC"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $updated_on = $result['updated_on'];

        return $updated_on;
    }

    /**********    Get in-activ member count     **********/
    function getInActiveMemberCount()
    {
        global $link;
        $stmt = "SELECT * FROM `clients` WHERE status='0'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();

        return $count;
    }

    /**********    Get activ member count     **********/
    function getActiveMemberCount()
    {
        global $link;
        $stmt = "SELECT * FROM `clients` WHERE status='1'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();

        return $count;
    }

    /**********    Get deactivated member count     **********/
    function getDeactivatedMemberCount()
    {
        global $link;
        $stmt = "SELECT * FROM `clients` WHERE status='2'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();

        return $count;
    }

    /**********    Get suspended member count     **********/
    function getSuspendedMemberCount()
    {
        global $link;
        $stmt = "SELECT * FROM `clients` WHERE status='3'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();

        return $count;
    }

    /**********    Get membership plan count     **********/
    function getMembershipPlanCount()
    {
        global $link;
        $stmt = "SELECT * FROM `membership_plan` WHERE status='1'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count_membership_plan=$stmt->rowCount();

        return $count_membership_plan;
    }

    /**********    Get featured listing activated or not     **********/
    function getFeaturedListingActivatedOrNot()
    {
        global $link;
        $stmt = "SELECT FeaturedMembershipAddOrNot FROM `featured_listing_plan`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $FeaturedMembershipAddOrNot = $result['FeaturedMembershipAddOrNot'];
        return $FeaturedMembershipAddOrNot;
    }

    /**********    Get featured listing price     **********/
    function getFeaturedListingPrice()
    {
        global $link;
        $stmt = "SELECT FeaturdListingPrice FROM `featured_listing_plan`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $FeaturdListingPrice = $result['FeaturdListingPrice'];
        return $FeaturdListingPrice;
    }

    /**********    Get membership plan duration     **********/
    function getMembershipPlanDuaration($id)
    {
        global $link;
        $stmt = "SELECT number_of_duration_months FROM `membership_plan` WHERE id='$id'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $number_of_duration_months = $result['number_of_duration_months'];
        return $number_of_duration_months;
    }

    /**********    Get membership plan id     **********/
    function getMembershipPlanId($plan_name)
    {
        global $link;
        $stmt = "SELECT id FROM `membership_plan` WHERE LOWER(membership_plan_name)=LOWER('$plan_name')"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $id = $result['id'];
        return $id;
    }

    /**********    Get membership plan Conacts     **********/
    function getMembershipPlanContacts($plan_name)
    {
        global $link;
        $stmt = "SELECT number_of_contacts FROM `membership_plan` WHERE LOWER(membership_plan_name)=LOWER('$plan_name')"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $number_of_contacts = $result['number_of_contacts'];
        return $number_of_contacts;
    }

    /**********    Get membership plan amount     **********/
    function getMembershipPlanAmount($plan_name)
    {
        global $link;
        $stmt = "SELECT price FROM `membership_plan` WHERE LOWER(membership_plan_name)=LOWER('$plan_name')"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $price = $result['price'];
        return $price;
    }


    /**********    Get tax applied or not     **********/
    function getTaxAppliedOrNot()
    {
        global $link;
        $stmt = "SELECT taxationAddOrNot FROM `taxation_setup`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $taxationAddOrNot = $result['taxationAddOrNot'];
        return $taxationAddOrNot;
    }

    /**********    Get tax name     **********/
    function getTaxName()
    {
        global $link;
        $stmt = "SELECT taxation_Name FROM `taxation_setup`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $taxation_Name = $result['taxation_Name'];
        return $taxation_Name;
    }

    /**********    Get tax percentage     **********/
    function getTaxPercent()
    {
        global $link;
        $stmt = "SELECT taxationPercent FROM `taxation_setup`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $taxationPercent = $result['taxationPercent'];
        return $taxationPercent;
    }

    /**********    Get maintainace mode activated or not     **********/
    function getMaintainanceModeActiveOrNot()
    {
        global $link;
        $stmt = "SELECT maintainance_mode FROM `maintainance_mode_setting`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $maintainance_mode = $result['maintainance_mode'];
        return $maintainance_mode;
    }

    /**********    Get popup display or not     **********/
    function getPopupDisplayOrNot()
    {
        global $link;
        $stmt = "SELECT popup_display_or_not FROM `popup_setting`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $popup_display_or_not = $result['popup_display_or_not'];
        return $popup_display_or_not;
    }

    /**********    Get popup image     **********/
    function getPopupImagePath()
    {
        global $link;
        $stmt = "SELECT popup_image FROM `popup_setting`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $popup_image = $result['popup_image'];
        return $popup_image;
    }

    /**********    Get popup redirect url     **********/
    function getPopupDestinationURL()
    {
        global $link;
        $stmt = "SELECT destination_url FROM `popup_setting`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $destination_url = $result['destination_url'];
        return $destination_url;
    }

    /*********************************************************
                    OFFLINE SETTING START
    *********************************************************/

    /**********    Get offlive enable or not     **********/
    function getIsOfflineEnable()
    {
        global $link;
        $stmt = "SELECT off_is_enable FROM `payment_offline`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $off_is_enable = $result['off_is_enable'];
        return $off_is_enable;
    }

    /**********    Get offline display Name     **********/
    function getOfflineDisplayName()
    {
        global $link;
        $stmt = "SELECT off_display_name FROM `payment_offline`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $off_display_name = $result['off_display_name'];
        return $off_display_name;
    }

    /**********    Get offline pay to text     **********/
    function getOfflinePayToText()
    {
        global $link;
        $stmt = "SELECT off_pay_to_text FROM `payment_offline`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $off_pay_to_text = $result['off_pay_to_text'];
        return $off_pay_to_text;
    }

    /*********************************************************
                    OFFLINE SETTING END
    *********************************************************/

    /*********************************************************
                    BANK TRANSFER SETTING START
    *********************************************************/

    /**********    Get bank-transfer enable or not     **********/
    function getIsBankTransferEnable()
    {
        global $link;
        $stmt = "SELECT bt_is_enable FROM `payment_bank_transfer`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $bt_is_enable = $result['bt_is_enable'];
        return $bt_is_enable;
    }

    /**********    Get bank-transfer display Name     **********/
    function getBankTransferDisplayName()
    {
        global $link;
        $stmt = "SELECT bt_display_name FROM `payment_bank_transfer`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $bt_display_name = $result['bt_display_name'];
        return $bt_display_name;
    }

    /**********    Get bank-transfer pay to text     **********/
    function getBankTransferPayToText()
    {
        global $link;
        $stmt = "SELECT bt_pay_to_text FROM `payment_bank_transfer`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $bt_pay_to_text = $result['bt_pay_to_text'];
        return $bt_pay_to_text;
    }

    /*********************************************************
                    BANK TRANSFER SETTING END
    *********************************************************/



    /*********************************************************
                    INSTAMOJO SETTING START
    *********************************************************/

    /**********    Get instamojo enable or not     **********/
    function getIsInstamojoEnable()
    {
        global $link;
        $stmt = "SELECT im_is_enable FROM `payment_instamojo`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $im_is_enable = $result['im_is_enable'];
        return $im_is_enable;
    }

    /**********    Get instamojo display Name     **********/
    function getIsInstamojoDisplayName()
    {
        global $link;
        $stmt = "SELECT im_display_name FROM `payment_instamojo`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $im_display_name = $result['im_display_name'];
        return $im_display_name;
    }

    function getInstamojoActivatedMode()
    {
        global $link;
        $stmt = "SELECT im_activated_mode FROM `payment_instamojo`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $im_activated_mode = $result['im_activated_mode'];
        return $im_activated_mode;
    }

    /**********    Get instamojo private api key     **********/
    function getImPrivateApiKey($active_mode)
    {
        global $link;
        $stmt = "SELECT im_test_api_key,im_prod_api_key FROM `payment_instamojo`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $im_test_api_key = $result['im_test_api_key'];
        $im_prod_api_key = $result['im_prod_api_key'];

        if($active_mode=='TEST')
        {
            return $im_test_api_key;
        }
        else
        if($active_mode=='PROD')
        {
            return $im_prod_api_key;
        }
        
    }

    /**********    Get instamojo private auth key     **********/
    function getImPrivateAuthKey($active_mode)
    {
        global $link;
        $stmt = "SELECT im_test_auth_token,im_prod_auth_token FROM `payment_instamojo`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $im_test_auth_token = $result['im_test_auth_token'];
        $im_prod_auth_token = $result['im_prod_auth_token'];
        
        if($active_mode=='TEST')
        {
            return $im_test_auth_token;
        }
        else
        if($active_mode=='PROD')
        {
            return $im_prod_auth_token;
        }
    }

    /**********    Get instamojo private salt    **********/
    function getImPrivateSalt($active_mode)
    {
        global $link;
        $stmt = "SELECT im_test_salt,im_prod_salt FROM `payment_instamojo`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $im_test_salt = $result['im_test_salt'];
        $im_prod_salt = $result['im_prod_salt'];
        
        if($active_mode=='TEST')
        {
            return $im_test_salt;
        }
        else
        if($active_mode=='PROD')
        {
            return $im_prod_salt;
        }
    }

    /**********    Get instamojo email    **********/
    function getImEmail($active_mode)
    {
        global $link;
        $stmt = "SELECT im_test_email,im_prod_email FROM `payment_instamojo`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $im_test_email = $result['im_test_email'];
        $im_prod_email = $result['im_prod_email'];
        
        if($active_mode=='TEST')
        {
            return $im_test_email;
        }
        else
        if($active_mode=='PROD')
        {
            return $im_prod_email;
        }
    }

    /*********************************************************
                    INSTAMOJO SETTING END
    *********************************************************/

    /*********************************************************
                    PAYTM SETTING START
    *********************************************************/

    /**********    Get paytm enable or not     **********/
    function getIsPaytmEnable()
    {
        global $link;
        $stmt = "SELECT pt_is_enable FROM `payment_paytm`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pt_is_enable = $result['pt_is_enable'];
        return $pt_is_enable;
    }

    /**********    Get paytm display Name     **********/
    function getPaytmDisplayName()
    {
        global $link;
        $stmt = "SELECT pt_display_name FROM `payment_paytm`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pt_display_name = $result['pt_display_name'];
        return $pt_display_name;
    }

    /**********    Get paytm activated mode     **********/
    function getPaytmActivatedMode()
    {
        global $link;
        $stmt = "SELECT pt_activated_mode FROM `payment_paytm`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pt_activated_mode = $result['pt_activated_mode'];
        return $pt_activated_mode;
    }

    /**********    Get paytm merchant Id    **********/
    function getPtMerchantId($active_mode)
    {
        global $link;
        $stmt = "SELECT pt_test_merchant_id,pt_prod_merchant_id FROM `payment_paytm`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pt_test_merchant_id = $result['pt_test_merchant_id'];
        $pt_prod_merchant_id = $result['pt_prod_merchant_id'];
        
        if($active_mode=='TEST')
        {
            return $pt_test_merchant_id;
        }
        else
        if($active_mode=='PROD')
        {
            return $pt_prod_merchant_id;
        }
    }

    /**********    Get paytm merchant key     **********/
    function getPtMerchantSecreteKey($active_mode)
    {
        global $link;
        $stmt = "SELECT pt_test_merchant_key,pt_prod_merchant_key FROM `payment_paytm`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pt_test_merchant_key = $result['pt_test_merchant_key'];
        $pt_prod_merchant_key = $result['pt_prod_merchant_key'];
        
        if($active_mode=='TEST')
        {
            return $pt_test_merchant_key;
        }
        else
        if($active_mode=='PROD')
        {
            return $pt_prod_merchant_key;
        }
    }

    /**********    Get paytm Website Name     **********/
    function getPtWebsiteName($active_mode)
    {
        global $link;
        $stmt = "SELECT pt_test_website_name,pt_prod_website_name FROM `payment_paytm`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pt_test_website_name = $result['pt_test_website_name'];
        $pt_prod_website_name = $result['pt_prod_website_name'];
        
        if($active_mode=='TEST')
        {
            return $pt_test_website_name;
        }
        else
        if($active_mode=='PROD')
        {
            return $pt_prod_website_name;
        }
    }

    /**********    Get paytm Industry Type     **********/
    function getPtIndustryType($active_mode)
    {
        global $link;
        $stmt = "SELECT pt_test_industry_type,pt_prod_industry_type FROM `payment_paytm`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pt_test_industry_type = $result['pt_test_industry_type'];
        $pt_prod_industry_type = $result['pt_prod_industry_type'];
        
        if($active_mode=='TEST')
        {
            return $pt_test_industry_type;
        }
        else
        if($active_mode=='PROD')
        {
            return $pt_prod_industry_type;
        }
    }

    /*********************************************************
                    PAYTM SETTING END
    *********************************************************/


    /*********************************************************
                    PAYUMONEY SETTING START
    *********************************************************/

    /**********    Get payUmoney enable or not     **********/
    function getIsPayUmoneyEnable()
    {
        global $link;
        $stmt = "SELECT pm_is_enable FROM `payment_payumoney`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pm_is_enable = $result['pm_is_enable'];
        return $pm_is_enable;
    }

    /**********    Get payUmoney display Name     **********/
    function getPayUmoneyDisplayName()
    {
        global $link;
        $stmt = "SELECT pm_display_name FROM `payment_payumoney`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pm_display_name = $result['pm_display_name'];
        return $pm_display_name;
    }

    /**********    Get payUmoney activated mode     **********/
    function getPayUmoneyActivatedMode()
    {
        global $link;
        $stmt = "SELECT pm_activated_mode FROM `payment_payumoney`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pm_activated_mode = $result['pm_activated_mode'];
        return $pm_activated_mode;
    }

    /**********    Get payUmoney key    **********/
    function getPmMerchantKey($active_mode)
    {
        global $link;
        $stmt = "SELECT pm_test_merchant_key,pm_prod_merchant_key FROM `payment_payumoney`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pm_test_merchant_key = $result['pm_test_merchant_key'];
        $pm_prod_merchant_key = $result['pm_prod_merchant_key'];
        
        if($active_mode=='TEST')
        {
            return $pm_test_merchant_key;
        }
        else
        if($active_mode=='PROD')
        {
            return $pm_prod_merchant_key;
        }
    }

    /**********    Get payUmoney salt     **********/
    function getPmMerchantSalt($active_mode)
    {
        global $link;
        $stmt = "SELECT pm_test_merchant_salt,pm_prod_merchant_salt FROM `payment_payumoney`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pm_test_merchant_salt = $result['pm_test_merchant_salt'];
        $pm_prod_merchant_salt = $result['pm_prod_merchant_salt'];
        
        if($active_mode=='TEST')
        {
            return $pm_test_merchant_salt;
        }
        else
        if($active_mode=='PROD')
        {
            return $pm_prod_merchant_salt;
        }
    }

    /**********    Get payUmoney auth header     **********/
    function getPmAuthHeader($active_mode)
    {
        global $link;
        $stmt = "SELECT pm_test_merchant_auth_header,pm_prod_merchant_auth_header FROM `payment_payumoney`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pm_test_merchant_auth_header = $result['pm_test_merchant_auth_header'];
        $pm_prod_merchant_auth_header = $result['pm_prod_merchant_auth_header'];
        
        if($active_mode=='TEST')
        {
            return $pm_test_merchant_auth_header;
        }
        else
        if($active_mode=='PROD')
        {
            return $pm_prod_merchant_auth_header;
        }
    }

    /*********************************************************
                    PAYUMONEY SETTING END
    *********************************************************/

    /*********************************************************
                    RAZORPAY SETTING START
    *********************************************************/

    /**********    Get razorpay enable or not     **********/
    function getIsRazorpayEnable()
    {
        global $link;
        $stmt = "SELECT rp_is_enable FROM `payment_razorpay`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $rp_is_enable = $result['rp_is_enable'];
        return $rp_is_enable;
    }

    /**********    Get razorpay display Name     **********/
    function getRazorpayDisplayName()
    {
        global $link;
        $stmt = "SELECT rp_display_name FROM `payment_razorpay`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $rp_display_name = $result['rp_display_name'];
        return $rp_display_name;
    }

    /**********    Get razorpay activated mode     **********/
    function getRazorpayActivatedMode()
    {
        global $link;
        $stmt = "SELECT rp_activated_mode FROM `payment_razorpay`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $rp_activated_mode = $result['rp_activated_mode'];
        return $rp_activated_mode;
    }

    /**********    Get razorpay key id    **********/
    function getRazorpayKeyId($active_mode)
    {
        global $link;
        $stmt = "SELECT rp_test_key_id,rp_prod_key_id FROM `payment_razorpay`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $rp_test_key_id = $result['rp_test_key_id'];
        $rp_prod_key_id = $result['rp_prod_key_id'];
        
        if($active_mode=='TEST')
        {
            return $rp_test_key_id;
        }
        else
        if($active_mode=='PROD')
        {
            return $rp_prod_key_id;
        }
    }

    /**********    Get razorpay key secret     **********/
    function getRazorpayKeySecret($active_mode)
    {
        global $link;
        $stmt = "SELECT rp_test_key_secret,rp_prod_key_secret FROM `payment_razorpay`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $rp_test_key_secret = $result['rp_test_key_secret'];
        $rp_prod_key_secret = $result['rp_prod_key_secret'];
        
        if($active_mode=='TEST')
        {
            return $rp_test_key_secret;
        }
        else
        if($active_mode=='PROD')
        {
            return $rp_prod_key_secret;
        }
    }

    /*********************************************************
                    RAZORPAY SETTING END
    *********************************************************/

    /*********************************************************
                    PAYPAL SETTING START
    *********************************************************/

    /**********    Get paypal enable or not     **********/
    function getIsPaypalEnable()
    {
        global $link;
        $stmt = "SELECT pp_is_enable FROM `payment_paypal`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pp_is_enable = $result['pp_is_enable'];
        return $pp_is_enable;
    }

    /**********    Get paypal display Name     **********/
    function getPaypalDisplayName()
    {
        global $link;
        $stmt = "SELECT pp_display_name FROM `payment_paypal`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pp_display_name = $result['pp_display_name'];
        return $pp_display_name;
    }

    /**********    Get paypal activated mode     **********/
    function getPaypalActivatedMode()
    {
        global $link;
        $stmt = "SELECT pp_activated_mode FROM `payment_paypal`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pp_activated_mode = $result['pp_activated_mode'];
        return $pp_activated_mode;
    }

    /**********    Get paypal Username    **********/
    function getPaypalClientID($active_mode)
    {
        global $link;
        $stmt = "SELECT pp_test_client_id,pp_prod_client_id FROM `payment_paypal`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pp_test_client_id = $result['pp_test_client_id'];
        $pp_prod_client_id = $result['pp_prod_client_id'];
        
        if($active_mode=='TEST')
        {
            return $pp_test_client_id;
        }
        else
        if($active_mode=='PROD')
        {
            return $pp_prod_client_id;
        }
    }

    /**********    Get paypal Password     **********/
    function getPaypalSecret($active_mode)
    {
        global $link;
        $stmt = "SELECT pp_test_secret,pp_prod_secret FROM `payment_paypal`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $pp_test_secret = $result['pp_test_secret'];
        $pp_prod_secret = $result['pp_prod_secret'];
        
        if($active_mode=='TEST')
        {
            return $pp_test_secret;
        }
        else
        if($active_mode=='PROD')
        {
            return $pp_prod_secret;
        }
    }

    /*********************************************************
                    PAYPAL SETTING END
    *********************************************************/

    /*********************************************************
                    2CHECKOUT SETTING START
    *********************************************************/

    /**********    Get 2checkout enable or not     **********/
    function getIs2CheckoutEnable()
    {
        global $link;
        $stmt = "SELECT co_is_enable FROM `payment_2checkout`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $co_is_enable = $result['co_is_enable'];
        return $co_is_enable;
    }

    /**********    Get 2checkout display Name     **********/
    function get2CheckoutDisplayName()
    {
        global $link;
        $stmt = "SELECT co_display_name FROM `payment_2checkout`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $co_display_name = $result['co_display_name'];
        return $co_display_name;
    }

    /**********    Get 2checkout activated mode     **********/
    function get2CheckoutActivatedMode()
    {
        global $link;
        $stmt = "SELECT co_activated_mode FROM `payment_2checkout`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $co_activated_mode = $result['co_activated_mode'];
        return $co_activated_mode;
    }

    /**********    Get 2Checkout Account Number    **********/
    function get2CheckoutAccountNumber($active_mode)
    {
        global $link;
        $stmt = "SELECT co_test_account_number,co_prod_account_number FROM `payment_2checkout`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $co_test_account_number = $result['co_test_account_number'];
        $co_prod_account_number = $result['co_prod_account_number'];
        
        if($active_mode=='TEST')
        {
            return $co_test_account_number;
        }
        else
        if($active_mode=='PROD')
        {
            return $co_prod_account_number;
        }
    }

    /**********    Get 2Checkout Publishable Key     **********/
    function get2CheckoutPublishableKey($active_mode)
    {
        global $link;
        $stmt = "SELECT co_test_publishable_key,co_prod_publishable_key FROM `payment_2checkout`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $co_test_publishable_key = $result['co_test_publishable_key'];
        $co_prod_publishable_key = $result['co_prod_publishable_key'];
        
        if($active_mode=='TEST')
        {
            return $co_test_publishable_key;
        }
        else
        if($active_mode=='PROD')
        {
            return $co_prod_publishable_key;
        }
    }

    /**********    Get 2Checkout Private Key     **********/
    function get2CheckoutPrivateKey($active_mode)
    {
        global $link;
        $stmt = "SELECT co_test_private_key,co_prod_private_key FROM `payment_2checkout`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $co_test_private_key = $result['co_test_private_key'];
        $co_prod_private_key = $result['co_prod_private_key'];
        
        if($active_mode=='TEST')
        {
            return $co_test_private_key;
        }
        else
        if($active_mode=='PROD')
        {
            return $co_prod_private_key;
        }
    }

    /*********************************************************
                    2CHECKOUT SETTING END
    *********************************************************/

    /*********************************************************
                    Stripe SETTING START
    *********************************************************/

    /**********    Get Stripe enable or not     **********/
    function getIsStripeEnable()
    {
        global $link;
        $stmt = "SELECT sp_is_enable FROM `payment_stripe`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $sp_is_enable = $result['sp_is_enable'];
        return $sp_is_enable;
    }

    /**********    Get Stripe display Name     **********/
    function getStripeDisplayName()
    {
        global $link;
        $stmt = "SELECT sp_display_name FROM `payment_stripe`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $sp_display_name = $result['sp_display_name'];
        return $sp_display_name;
    }

    /**********    Get Stripe activated mode     **********/
    function getStripeActivatedMode()
    {
        global $link;
        $stmt = "SELECT sp_activated_mode FROM `payment_stripe`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $sp_activated_mode = $result['sp_activated_mode'];
        return $sp_activated_mode;
    }

    /**********    Get Stripe Publishable Key     **********/
    function getStripePublishableKey($active_mode)
    {
        global $link;
        $stmt = "SELECT sp_test_publishable_key,sp_prod_publishable_key FROM `payment_stripe`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $sp_test_publishable_key = $result['sp_test_publishable_key'];
        $sp_prod_publishable_key = $result['sp_prod_publishable_key'];
        
        if($active_mode=='TEST')
        {
            return $sp_test_publishable_key;
        }
        else
        if($active_mode=='PROD')
        {
            return $sp_prod_publishable_key;
        }
    }

    /**********    Get Stripe Secret Key     **********/
    function getStripeSecretKey($active_mode)
    {
        global $link;
        $stmt = "SELECT sp_test_secret_key,sp_prod_secret_key FROM `payment_stripe`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $sp_test_secret_key = $result['sp_test_secret_key'];
        $sp_prod_secret_key = $result['sp_prod_secret_key'];
        
        if($active_mode=='TEST')
        {
            return $sp_test_secret_key;
        }
        else
        if($active_mode=='PROD')
        {
            return $sp_prod_secret_key;
        }
    }

    /*********************************************************
                    Stripe SETTING END
    *********************************************************/

    /*********************************************************
                    CCAVENUE SETTING START
    *********************************************************/

    /**********    Get CCAvenue enable or not     **********/
    function getIsCCAvenueEnable()
    {
        global $link;
        $stmt = "SELECT cc_is_enable FROM `payment_ccavenue`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $cc_is_enable = $result['cc_is_enable'];
        return $cc_is_enable;
    }

    /**********    Get CCAvenue display Name     **********/
    function getCCAvenueDisplayName()
    {
        global $link;
        $stmt = "SELECT cc_display_name FROM `payment_ccavenue`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $cc_display_name = $result['cc_display_name'];
        return $cc_display_name;
    }


    /**********    Get CCAvenue Merchant ID     **********/
    function getCCAvenueMerchantID()
    {
        global $link;
        $stmt = "SELECT cc_merchant_id FROM `payment_ccavenue`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $cc_merchant_id = $result['cc_merchant_id'];
        return $cc_merchant_id;
    }

    /**********    Get CCAvenue Access Code     **********/
    function getCCAvenueAccessCode()
    {
        global $link;
        $stmt = "SELECT cc_access_code FROM `payment_ccavenue`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $cc_access_code = $result['cc_access_code'];
        return $cc_access_code;
    }

    /**********    Get CCAvenue Working Key     **********/
    function getCCAvenueWorkingKey()
    {
        global $link;
        $stmt = "SELECT cc_working_key FROM `payment_ccavenue`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result=$stmt->fetch();

        $cc_working_key = $result['cc_working_key'];
        return $cc_working_key;
    }

    /*********************************************************
                    CCAVENUE SETTING END
    *********************************************************/


    /***********     Member puchased plan or not    **************/
    function getMemberIsPremiumOrNot($x)
    {
        global $link;
        $stmt = "SELECT * FROM `payment_transactions` WHERE userid='$x' AND status='1'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count = $stmt->rowCount();
        
        return $count;
    }

    /***********     Get Membership plan total validity    **************/
    function getMembershipValidity($x)
    {
        global $link;
        $stmt = "SELECT membership_plan_expiry_date FROM `payment_transactions` WHERE userid='$x' AND membership_plan='Yes' AND status='1' ORDER BY id DESC"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result = $stmt->fetch();
        
        $membership_plan_expiry_date = $result['membership_plan_expiry_date'];

        return $membership_plan_expiry_date;
    }

    /***********     Get contacts used by member    **************/
    function getContactsUsed($x)
    {
        global $link;
        $stmt = "SELECT DISTINCT(userTo) AS userTo FROM `messagechat` WHERE `userFrom`='1'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count = $stmt->rowCount();
        
        return $count;
    }

    /***********     Member puchased featured listing or not    **************/
    function getMemberIsFeaturedOrNot($x)
    {
        global $link;
        $stmt = "SELECT * FROM `payment_transactions` WHERE featured_listing='Yes' AND status='1' AND userid='$x'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count = $stmt->rowCount();
        
        return $count;
    }

    /***********     Get Featured Listing validity    **************/
    function getFeaturedListingValidity($x)
    {
        global $link;
        $stmt = "SELECT featured_listing_expiry_date FROM `payment_transactions` WHERE userid='$x' AND featured_listing='Yes' AND status='1' ORDER BY id DESC"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result = $stmt->fetch();
        
        $featured_listing_expiry_date = $result['featured_listing_expiry_date'];

        return $featured_listing_expiry_date;
    }

    /***********     Get Remaining Days upto expiry date    **************/
    function getRemainingDaysValidity($x,$y)
    {
        $date1 = new DateTime($x);
        $date2 = new DateTime($y);
        $diff = $date1->diff($date2);

        $remaining_validity = $diff->y . " years, " . $diff->m." months, ".$diff->d." days remaining";

        return $remaining_validity;
    }

    /***********     Get Remaining validity in total Days    **************/
    function getRemainingTotalDaysValidity($x)
    {
        $endDate = strtotime($x);
        $startDate = strtotime(date('Y-m-d H:i:s'));

        $datediff = $endDate - $startDate;
        return floor($datediff /86400);
    }       

    /***********  Generate unique random 10 digit order number  ************/
    function getOrderId()
    {
        global $link;

        $sql = "select concat(substring('0123456789', rand()*10+1, 1),
              substring('0123456789', rand()*10+1, 1),
              substring('0123456789', rand()*10+1, 1),
              substring('0123456789', rand()*10+1, 1),
              substring('0123456789', rand()*10+1, 1),
              substring('0123456789', rand()*10+1, 1),
              substring('0123456789', rand()*10+1, 1),
              substring('0123456789', rand()*10+1, 1),
              substring('0123456789', rand()*10+1, 1),
              substring('0123456789', rand()*10+1, 1)
             ) as OrderNumber";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $OrderNumber = $result['OrderNumber'];

        $sql_chk = "SELECT OrderNumber FROM payment_transactions WHERE OrderNumber='$OrderNumber'";
        $stmt_chk = $link->prepare($sql_chk);
        $stmt_chk->execute();
        $count = $stmt_chk->rowCount();

        if($count>0)
        {
            getOrderId();
        }
        else
        {
            return $OrderNumber;
        }
    }

    /***********  Generate unique random 10 digit order number  ************/
    function getOfflineRequestId()
    {
        global $link;

        $year = date('Y');
        $month = date('m');
        $day = date('d');

        $time = time();

        $OfflineRequestId = 'OFFLINE'.$year.$month.$day.$time;
        

        $sql_chk = "SELECT transact_id,request_id FROM payment_transactions WHERE transact_id='$OfflineRequestId' OR request_id='$OfflineRequestId'";
        $stmt_chk = $link->prepare($sql_chk);
        $stmt_chk->execute();
        $count = $stmt_chk->rowCount();

        if($count>0)
        {
            getOfflineRequestId();
        }
        else
        {
            return $OfflineRequestId;
        }
    }

    /***********  Generate unique random payment id  ************/
    function getPaymentId()
    {
        global $link;

        $year = date('Y');
        $month = date('m');
        $day = date('d');

        $time = time();

        $OfflineRequestId = 'MANUAL_PAYMENT'.$year.$month.$day.$time;
        

        $sql_chk = "SELECT transact_id FROM payment_transactions WHERE transact_id='$OfflineRequestId'";
        $stmt_chk = $link->prepare($sql_chk);
        $stmt_chk->execute();
        $count = $stmt_chk->rowCount();

        if($count>0)
        {
            getPaymentId();
        }
        else
        {
            return $OfflineRequestId;
        }
    }

    /***********     Sitemap Enable or not    **************/
    function getSitemapGenerationEnableOrNot()
    {
        global $link;
        $stmt = "SELECT sitemap_generation_allowed FROM `sitemap_generation`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();
        
        $sitemap_generation_allowed = $result['sitemap_generation_allowed'];

        return $sitemap_generation_allowed;
    }

    /***********     Sitemap Generation time    **************/
    function getSitemapGenerationTime()
    {
        global $link;
        $stmt = "SELECT SitemapGenerationRunAt FROM `sitemap_generation`"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result = $stmt->fetch();
        
        $SitemapGenerationRunAt = $result['SitemapGenerationRunAt'];

        return $SitemapGenerationRunAt;
    }

    /**********   Category id from slug    ***********/
    function getdirectoryidfromslug($x)
    {
        global $link;
        $sql_permission = "SELECT id FROM `membercategory` WHERE slug='$x'";
        $stmt   = $link->prepare($sql_permission);
        $stmt->execute();
        $row_role = $stmt->fetch();
        $id =$row_role['id'];
        return $id;
    }

    /**********   Category name from slug    ***********/
    function getdirectorynamefromslug($x)
    {
        global $link;
        $sql_permission = "SELECT name FROM `membercategory` WHERE slug='$x'";
        $stmt   = $link->prepare($sql_permission);
        $stmt->execute();
        $row_role = $stmt->fetch();
        $name =$row_role['name'];
        return $name;
    }

    /**********   Category name from id    ***********/
    function getdirectorynamefromId($x)
    {
        global $link;
        $sql_permission = "SELECT name FROM `membercategory` WHERE id='$x'";
        $stmt   = $link->prepare($sql_permission);
        $stmt->execute();
        $row_role = $stmt->fetch();
        $name =$row_role['name'];
        return $name;
    }

    /**********   Vendor name from email id   ***********/
    function getVenderIdFromEmail($x)
    {
        global $link;
        $sql_permission = "SELECT vuserid FROM `vendors` WHERE email='$x'";
        $stmt   = $link->prepare($sql_permission);
        $stmt->execute();
        $row_role = $stmt->fetch();
        $vuserid =$row_role['vuserid'];
        return $vuserid;
    }

    /**********   Category count from category-id   ***********/
    function getCategoryCount($id)
    {
        global $link;
        $sql = "SELECT COUNT(vuserid) AS category_count FROM `vendors` WHERE category_id='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $count =$row['category_count'];
        return $count;
    }

    /**********   Vendor First name   ***********/
    function getVendorFirstName($id)
    {
        global $link;
        $sql = "SELECT firstname FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $firstname =$row['firstname'];
        return $firstname;
    }

    /**********   Vendor Last name   ***********/
    function getVendorLastName($id)
    {
        global $link;
        $sql = "SELECT lastname FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $lastname =$row['lastname'];
        return $lastname;
    }

    /**********   Vendor email id   ***********/
    function getVendorEmail($id)
    {
        global $link;
        $sql = "SELECT email FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $email =$row['email'];
        return $email;
    }

    /**********   Vendor phonecode   ***********/
    function getVendorPhoneCode($id)
    {
        global $link;
        $sql = "SELECT phonecode FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $phonecode =$row['phonecode'];
        return $phonecode;
    }

    /**********   Vendor phone   ***********/
    function getVendorPhone($id)
    {
        global $link;
        $sql = "SELECT phone FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $phone =$row['phone'];
        return $phone;
    }

    /**********   Vendor company_name   ***********/
    function getVendorCompanyName($id)
    {
        global $link;
        $sql = "SELECT company_name FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $company_name =$row['company_name'];
        return $company_name;
    }

    /**********   Vendor category_id   ***********/
    function getVendorCategoryId($id)
    {
        global $link;
        $sql = "SELECT category_id FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $category_id =$row['category_id'];
        return $category_id;
    }

    /**********   Vendor city id   ***********/
    function getVendorCityId($id)
    {
        global $link;
        $sql = "SELECT city FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $city =$row['city'];
        return $city;
    }

    /**********   Vendor state id   ***********/
    function getVendorStateId($id)
    {
        global $link;
        $sql = "SELECT state FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $state =$row['state'];
        return $state;
    }

    /**********   Vendor country id   ***********/
    function getVendorCountryId($id)
    {
        global $link;
        $sql = "SELECT country FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $country =$row['country'];
        return $country;
    }

    /**********   Vendor short_desc   ***********/
    function getVendorShortDesc($id)
    {
        global $link;
        $sql = "SELECT short_desc FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $short_desc =$row['short_desc'];
        return $short_desc;
    }

    /**********   Vendor status   ***********/
    function getVendorStatus($id)
    {
        global $link;
        $sql = "SELECT status FROM `vendors` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $status =$row['status'];
        return $status;
    }

    /************   Get vendor last logged in time  *************/
    function getLastVendorLoginDate($x)
    {
        global $link;
        $stmt = "SELECT * FROM (SELECT * FROM userlogs WHERE `userid`='$x' AND user_type='Vendor' ORDER BY Id DESC LIMIT 2) userlogs ORDER BY Id LIMIT 1";
         
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $lastLoginTime = $result['loggedOn'];
        return $lastLoginTime;
    }

    /************   Get Vendors last logged in time  *************/
    function getLastVendorLoginIP($x)
    {
        global $link;
        $stmt = "SELECT * FROM `userlogs` WHERE `userid`='$x' AND user_type='Vendor' ORDER BY id DESC"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $count=$stmt->rowCount();
        $result = $stmt->fetch();
        
        $lastLoginIP = $result['IP_Address'];
        return $lastLoginIP;
    }

    /************   Get next auto increment value from table  *************/
    function getNextInsertId($x)
    {
        global $link;
        $stmt = "SHOW TABLE STATUS LIKE '$x'"; 
        $stmt = $link->prepare($stmt);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC)['Auto_increment'];
        
        //$lastLoginIP = $result['IP_Address'];
        return $result;
    }

    /************   Vendor profile pic  *************/
    function getVendorProfilePic($id)
    {
        global $link;
        $sql = "SELECT profile_pic FROM `vendor_profile_pic` WHERE vuserid='$id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $profile_pic =$row['profile_pic'];
        return $profile_pic;
    }

    /************   Wedding directory dispaly Or Not  *************/
    function getWeddingDirectoryDisplayOrNot()
    {
        global $link;
        $sql = "SELECT wedding_directory_display_or_not FROM `wedding_directory_settings`";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $wedding_directory_display_or_not =$row['wedding_directory_display_or_not'];
        return $wedding_directory_display_or_not;
    }

    /************   Wedding directory custom content  *************/
    function getWeddingDirectoryCustomContent()
    {
        global $link;
        $sql = "SELECT custom_content FROM `wedding_directory_settings`";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $custom_content =$row['custom_content'];
        return $custom_content;
    }
?>
