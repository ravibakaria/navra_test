<?php

	/******  Fetch General Setup data start  ******/
		$stmt1   = $link->prepare("SELECT * FROM `generalsetup`");
		$stmt1->execute();
		$result1 = $stmt1->fetch();
		$count1=$stmt1->rowCount();

		$WebSiteBasePath = $result1['WebSiteBasePath'];
		$WebSiteTitle = $result1['WebSiteTitle'];
	    $WebSiteTagline = $result1['WebSiteTagline'];
	    $EmailAddress = $result1['EmailAddress'];
	    $LogoURL = $result1['LogoURL'];
	    $FaviconURL = $result1['FaviconURL'];
	    $TermOfServiceURL = $result1['TermOfServiceURL'];
	    $PrivacyPolicyURL = $result1['PrivacyPolicyURL'];
	    $AllowedFileAttachmentTypes = $result1['AllowedFileAttachmentTypes'];
    /******  Fetch General Setup data end  ******/

    /******  Fetch Localization Setup data start  ******/
	    $stmt2  = $link->prepare("SELECT * FROM `localizationsetup`");
		$stmt2->execute();
		$result2 = $stmt2->fetch();
		$count2=$stmt2->rowCount();

		$DefaultCountry = $result2['DefaultCountry'];
	    $DefaultTimeZone = $result2['DefaultTimeZone'];
	    $DefaultCurrency = $result2['DefaultCurrency'];
	/******  Fetch Localization Setup data end  ******/


	/******  Fetch Mail Setup data start  ******/
	    $stmt3   = $link->prepare("SELECT * FROM `mailrelaysetup`");
		$stmt3->execute();
		$result3 = $stmt3->fetch();
		$count=$stmt3->rowCount();

		$FromName = $result3['FromName'];
	    $FromEmail = $result3['FromEmail'];
	    $BCCEmail = $result3['BCCEmail'];
	    $SMTPHost = $result3['SMTPHost'];
	    $SMTPPort = $result3['SMTPPort'];
	    $SMTPUsername = $result3['SMTPUsername'];
	    $SMTPPassword = $result3['SMTPPassword'];
	    $SMTPSSL = $result3['SMTPSSL'];
	    $GlobalEmailSignature = $result3['GlobalEmailSignature'];
	    $emailFooterText = $result3['emailFooterText'];
	    $headerBackgroundColor = $result3['headerBackgroundColor'];
	/******  Fetch mail Setup data end  ******/

	$primary_color = getPrimaryColor();
	$primary_font_color = getPrimaryFontColor();
?>