<?php
  session_start();
  include("layout/header.php"); 
  if(isset($_SESSION['last_page_accessed']))
  {
      unset($_SESSION['last_page_accessed']);
      unset($_SESSION['membership_plan_id']);
      unset($_SESSION['featured_listing']);
      unset($_SESSION['validity']);
      unset($_SESSION['secret_key']);
  }
?>

<!-- meta info  -->

    <title><?php echo $site_title.' - '.$site_tagline;?></title>

    <meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

    <meta name="description" content="<?php echo $site_tagline;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!"/>

    <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

    <meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
    <meta property="og:url" content="<?= $site_url; ?>" />
    <meta property="og:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
    <meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
    <meta property="twitter:url" content="<?= $site_url; ?>" />
    <meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
    include("layout/styles.php"); 
    include("layout/menu.php"); 

  $homepage_bannerURL = gethomepage_bannerURL();

  $homepageHeading = gethomepageHeading();

  if($homepage_bannerURL!='' || $homepage_bannerURL!=null)
  {
    $homepage_banner=array();
    $homepage_bannerURL = explode('/',$homepage_bannerURL);

    for($i=1;$i<count($homepage_bannerURL);$i++)
    {
      $homepage_banner[] = $homepage_bannerURL[$i];
    }

    $homepage_banner = implode('/',$homepage_banner);
  }
  else
  {
    $homepage_banner = "images/home-page-banner/default/default-wedding-template.png";
  }

  $extracontentstripshow = getextracontentstripshow();
  $extracontentstripdata = getextracontentstripdata();

  $footercontentshow = getfootercontentshow();
  $footercontent = getfootercontent();

  $profilefiltershow = getprofilefiltershow();
  $profilefiltervalues = getprofilefiltervalues();
  $profilefiltervalues = explode(',',$profilefiltervalues);
?>



<div class="homepage-data">
  <!-- start: page -->
  <?php
      $popup_display_or_not = getPopupDisplayOrNot();
      if($popup_display_or_not=='1')
      {
        $popup_image = getPopupImagePath();
        $popup_image = $websiteBasePath."/".$popup_image;
        $destination_url = getPopupDestinationURL();
  ?>
      <section id="intro-home" class="intro1">
        <div class="bts-popup alert" role="alert alert-dismissible" style="z-index:1001;">
            <div class="bts-popup-container">
              <a href="#" class="close popup-close" data-dismiss="alert" aria-label="close"><i class="fa fa-close" aria-hidden="true"></i></a>
              <a href="<?php echo $destination_url; ?>">
                  <img src="<?php echo $popup_image; ?>" alt="<?php echo $site_title;?>" class="popup-image"/>
              </a>
            </div>
        </div>
      </section>
  <?php
    }
  ?>  

  <!-- Section: intro -->
  <section id="intro1" class="intro1">

    <div class="row home-hero-section common-row-div">
      <img src="<?php echo $homepage_banner;?>" alt="<?= $site_title;?>" class="img-responsive slide_img desktop-banner">
      <div class="home-search-box">
          <h1 class="homepage-description">
            <?php echo $site_title;?>
          </h1>
          <h2 class="homepage-description">
            <?php echo $site_tagline;?>
          </h2>
          
            <?php 
              if($homepageHeading!='')
              {
                echo "<p class='homepage-heading'>".$homepageHeading."</p>"; 
              }
            ?>
      </div>
    </div>

  </section>
  <!-- /Section: intro -->

  <!--  Section home-search start  -->
  <section class="about clearfix">
    <div class="row home-search common-row-div">
      <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
          <div class="">
            <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 search-div centered-data'>
                <form method="POST" action="users/advanced-search-result.php">
                  <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 homepage-search-div looking-for-gender">
                        <div class="inline-form-search">
                          <label class="searching-for"><b>Looking for: </b></label>
                          <div class="switch-field homepage-gender-search">
                              <?php
                                $sql_gender = "SELECT * FROM gender WHERE status='1'";
                                $stmt = $link->prepare($sql_gender);
                                $stmt->execute();
                                $count_gender = $stmt->rowCount();
                                $result_gender = $stmt->fetchAll();

                                if($count_gender>0)
                                {
                                  foreach ($result_gender as $row_gender) 
                                  {
                                    $id = $row_gender['id'];
                                    $name = $row_gender['name'];
                                    $description = $row_gender['description'];

                                    if($id=='1')
                                    {
                                      echo "<input class='gender' type='radio' id='$id' name='gender' value='$id' checked/>
                                      <label for='$id'><i class='fa fa-check gender-check_$id'></i> $description  </label>";
                                    }
                                    else
                                    if($id=='2')
                                    {
                                      echo "<input class='gender' type='radio' id='$id' name='gender' value='$id'/>
                                      <label for='$id'> <i class='fa fa-check gender-check_$id' style='display:none;'></i> $description </label>";
                                    }
                                    else
                                    {
                                      echo "<input class='gender' type='radio' id='$id' name='gender' value='$id'/>
                                      <label for='$id'> <i class='fa fa-check gender-check_$id' style='display:none;'></i> $description </label>";
                                    }
                                  }
                                }
                              ?>
                              <input type="hidden" class="count_gender" value="<?php echo $count_gender; ?>">
                          </div>
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 homepage-search-div search-age">
                      <div class="inline-form-search">
                        <label class="searching-for"><b>Age From: </b></label>
                      </div>
                      <div class="inline-form-search">
                        <select class="form-control from_date" id="min-age"  name="from_date">
                          <?php
                            for($i=18;$i<100;$i++)
                            {
                              echo "<option class='home-search-filter' value='".$i."'>".$i."</option>";
                            }
                          ?>
                        </select>
                      </div>

                      <div class="inline-form-search">
                        <label class="searching-for"><b>To: </b></label>
                      </div>
                      <div class="inline-form-search">
                        <select class="form-control to_date" id="max-age" name="to_date">
                          <?php
                            for($i=18;$i<100;$i++)
                            {
                              echo "<option class='home-search-filter' value='".$i."'>".$i."</option>";
                            }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 homepage-search-div home-search-country-div">
                      <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 inline-form-search">
                        <label class="searching-for searching-for-country"><b>Country: </b></label>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 inline-form-search">
                          <select class="form-control home-search-country" name="country">
                            <option value='0' selected>--Select--</option>
                              <?php 
                                $sql_country = "SELECT * FROM countries WHERE status='1'";
                                $stmt_country = $link->prepare($sql_country);
                                $stmt_country->execute();
                                $count_country = $stmt_country->rowCount();
                                $result_country = $stmt_country->fetchAll();

                                if($count_country>0)
                                {
                                  foreach ($result_country as $row_country) 
                                  {
                                    $id = $row_country['id'];
                                    $name = $row_country['name'];

                                    echo "<option value='".$id."'>".$name."</option>";
                                  }
                                }
                              ?>
                          </select>
                      </div>
                      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 inline-form-search">
                      </div>
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inline-form-search">
                        <button class="form-control" id="hero-search" name="search">Search</button>
                      </div>
              
                    </div>
                    
                  </div>
                </form>
            </div>
          </div>
      </div>
    </div>
    
  </section>
  <!--  Section home-search end   --> 

  <!-- ***** Advertisement Area Start ***** -->
  <?php
    $display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
    if($display_leaderboard_ad=='1')
    {
      $leaderboard_ad = getRandomLeaderBoardAdData();
      if($leaderboard_ad!='')
      {
  ?>
  <section class="about clearfix">
    <div class="row">
      <div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
        <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
          
        </div>
      </div>
      <div class='col-xs-12 col-sm-12 col-md-8 col-lg-8'>
        <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 leaderboard-ad'>
          <?php
            
              echo $leaderboard_ad;
            
          ?>
        </div>
      </div>
      <div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
        <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
          
        </div>
      </div>
    </div>
  </section>
  <?php
      }
    }
  ?>

  <!--  ******** Extra content data start  *******  -->  
    <?php
      if($extracontentstripshow=='1')
      {
    ?>
        <section class="about clearfix">
          <div class="row extra-content-strip common-row-div">
            <!-- Set up your HTML -->
            <div class="col-md-12 centered-data">
                <?php echo $extracontentstripdata;?>
            </div>
          </div>
        </section>
    <?php
      }
    ?>
  <!--  *********  Profile filters  data   ***********  -->
    <?php
      if($profilefiltershow=='1')
      {

    ?>
      <br/>
      <section class="about clearfix">
        <div class="row profile-filter-div common-row-div">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <h2>Search Profiles By:</h2>
              <br/>
            </div>
            <?php
              for($i=0;$i<count($profilefiltervalues);$i++)
              {
                if($profilefiltervalues[$i]=='1')
                {
                  $category='Location';
                  $icon = 'fa fa-map-marker';
                  $web_link = 'location.html';
                }
                else
                if($profilefiltervalues[$i]=='2')
                {
                  $category='Religion';
                  $icon = 'fa fa-group';
                  $web_link = "category-religion.html";
                }
                else
                if($profilefiltervalues[$i]=='3')
                {
                  $category='Caste';
                  $icon = 'fa fa-user';
                  $web_link = "category-caste.html";
                }
                else
                if($profilefiltervalues[$i]=='4')
                {
                  $category='Mother Tongue';
                  $icon = 'fa fa-language';
                  $web_link = "category-mothertongue.html";
                }
                else
                if($profilefiltervalues[$i]=='5')
                {
                  $category='Education';
                  $icon = 'fa fa-graduation-cap';
                  $web_link = "category-education.html";
                }
                else
                if($profilefiltervalues[$i]=='6')
                {
                  $category='Profession';
                  $icon = 'fa fa-briefcase';
                  $web_link = "category-profession.html";
                }
                else
                if($profilefiltervalues[$i]=='7')
                {
                  $category='Marital Status';
                  $icon = 'fa fa-history';
                  $web_link = "category-maritalstatus.html";
                }
                else
                if($profilefiltervalues[$i]=='8')
                {
                  $category='Family Value';
                  $icon = 'fa fa-users';
                  $web_link = "category-familyvalue.html";
                }
                else
                if($profilefiltervalues[$i]=='9')
                {
                  $category='Family Type';
                  $icon = 'fa fa-users';
                  $web_link = "category-familytype.html";
                }
                else
                if($profilefiltervalues[$i]=='10')
                {
                  $category='Family Status';
                  $icon = 'fa fa-users';
                  $web_link = "category-familystatus.html";
                }
                else
                if($profilefiltervalues[$i]=='11')
                {
                  $category='Body Type';
                  $icon = 'fa fa-child';
                  $web_link = "category-bodytype.html";
                }
                else
                if($profilefiltervalues[$i]=='12')
                {
                  $category='Complexion';
                  $icon = 'fa fa-user';
                  $web_link = "category-complexion.html";
                }
                else
                if($profilefiltervalues[$i]=='13')
                {
                  $category='Eating Habbit';
                  $icon = 'fa fa-glass';
                  $web_link = "category-eatinghabbit.html";
                }
                else
                if($profilefiltervalues[$i]=='14')
                {
                  $category='Smoking Habbit';
                  $icon = 'fa fa-fire';
                  $web_link = "category-smokinghabbit.html";
                }
                else
                if($profilefiltervalues[$i]=='15')
                {
                  $category='Drinking Habbit';
                  $icon = 'fa fa-beer';
                  $web_link = "category-drinkinghabbit.html";
                }
                  echo "<a href='$web_link'>";
                    echo "<div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
                          
                          <blockquote class='info' style='background-color:#f0f0f0;'>
                            <p> $category</p>
                          </blockquote>
                      </div>";
                  echo "</a>";
                
              }
            ?>
            <!-- Set up your HTML -->
          </div>
        </div>
      </section>
    <?php
      }
    ?>

    <!--   Ad area   -->
    <?php
      $display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
      if($display_leaderboard_ad=='1')
      {
        $leaderboard_ad = getRandomLeaderBoardAdData();
        if($leaderboard_ad!='')
        {
    ?>
        <section class="about clearfix">
          <div class="row">
            <div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
              <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                
              </div>
            </div>
            <div class='col-xs-12 col-sm-12 col-md-8 col-lg-8'>
              <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 leaderboard-ad'>
                <?php
                  
                    echo $leaderboard_ad;
                  
                ?>
              </div>
            </div>
            <div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
              <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                
              </div>
            </div>
          </div>
        </section>
    <?php
        }
      }
    ?>

    <!--  *********    Footer content data   ***********  -->
      <?php
        if($footercontentshow=='1')
        {
      ?>
          <section class="about clearfix" id="about">
            <div class="row">
              <!-- Set up your HTML -->
              <div class="col-md-12 centered-data">
                  <?php echo $footercontent;?>
              </div>
            </div>
          </section>
      <?php
        }
      ?>

  <!--  *********    Additional Footer content   ***********  -->
    <?php

      $sql_homepage1 = "SELECT * FROM `homepagesetup`";
      $stmt1 = $link->prepare($sql_homepage1);
      $stmt1->execute();
      $result = $stmt1->fetch();
      $count=$stmt1->rowCount();

      if($count>0)
      {
          $additional_footer_content1_display = $result['additional_footer_content1_display'];
          $additional_footer_content1 = $result['additional_footer_content1'];
          $additional_footer_content2_display = $result['additional_footer_content2_display'];
          $additional_footer_content2 = $result['additional_footer_content2'];
          $additional_footer_content3_display = $result['additional_footer_content3_display'];
          $additional_footer_content3 = $result['additional_footer_content3'];
          $additional_footer_content4_display = $result['additional_footer_content4_display'];
          $additional_footer_content4 = $result['additional_footer_content4'];

      }

      $count_display = null;
      if(@$additional_footer_content1_display=='1' || @$additional_footer_content2_display=='1' || @$additional_footer_content3_display=='1' || @$additional_footer_content4_display=='1')
      {
        if($additional_footer_content1_display=='1')
        {
          $count_display = $count_display+1;
        }

        if($additional_footer_content2_display=='1')
        {
          $count_display = $count_display+1;
        }

        if($additional_footer_content3_display=='1')
        {
          $count_display = $count_display+1;
        }

        if($additional_footer_content4_display=='1')
        {
          $count_display = $count_display+1;
        }
    ?>
    <?php
      if($count_display=='1')
      {
    ?>
        <section class="about clearfix" id="about">
          <div class="row">
            <!-- Set up your HTML -->
            <?php
              
                echo "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
                if($additional_footer_content1_display=='1')
                {
                  echo $additional_footer_content1;
                }
                
                if($additional_footer_content2_display=='1')
                {
                  echo $additional_footer_content2;
                }
               
                if($additional_footer_content3_display=='1')
                {
                  echo $additional_footer_content3;
                }
                
                if($additional_footer_content4_display=='1')
                {
                  echo $additional_footer_content4;
                }
                echo "</div>";
              }

              if($count_display=='2')
              {
                
                if($additional_footer_content1_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>";
                    echo $additional_footer_content1;
                  echo "</div>";
                }

                if($additional_footer_content2_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>";
                    echo $additional_footer_content2;
                  echo "</div>";
                }
               
                if($additional_footer_content3_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>";
                    echo $additional_footer_content3;
                  echo "</div>";
                }
               
                if($additional_footer_content4_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>";
                    echo $additional_footer_content4;
                  echo "</div>";
                }
              }

              if($count_display=='3')
              {
                
                if($additional_footer_content1_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>";
                    echo $additional_footer_content1;
                  echo "</div>";
                }

                if($additional_footer_content2_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>";
                    echo $additional_footer_content2;
                  echo "</div>";
                }
               
                if($additional_footer_content3_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>";
                    echo $additional_footer_content3;
                  echo "</div>";
                }
               
                if($additional_footer_content4_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>";
                    echo $additional_footer_content4;
                  echo "</div>";
                }
              }

              if($count_display=='4')
              {
                
                if($additional_footer_content1_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3'>";
                    echo $additional_footer_content1;
                  echo "</div>";
                }

                if($additional_footer_content2_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3'>";
                    echo $additional_footer_content2;
                  echo "</div>";
                }
               
                if($additional_footer_content3_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3'>";
                    echo $additional_footer_content3;
                  echo "</div>";
                }
               
                if($additional_footer_content4_display=='1')
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3'>";
                    echo $additional_footer_content4;
                  echo "</div>";
                }
                
              }
            ?>
            <div class="col-md-12 centered-data">
                <?php echo $footercontent;?>
            </div>
          </div>

          <div class="row">
            <div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
              <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                
              </div>
            </div>
            <div class='col-xs-12 col-sm-12 col-md-8 col-lg-8'>
              <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 leaderboard-ad'>
                <?php
                  $display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
                  if($display_leaderboard_ad=='1')
                  {
                    echo $leaderboard_ad = getRandomLeaderBoardAdData();
                  }
                ?>
              </div>
            </div>
            <div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
              <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                
              </div>
            </div>
          </div>
        </section>
   <?php
      }
    ?>


</div>
</section>
<?php
  include("layout/footer.php");
?>
<?php
    $sql_AddThisScript = "SELECT social_sharing_display,social_sharing_data FROM `social_sharing`";
    $stmt= $link->prepare($sql_AddThisScript); 
    $stmt->execute();
    $count=$stmt->rowCount();
    $result = $stmt->fetch();
    $social_sharing_display = $result['social_sharing_display'];
    $social_sharing_data = $result['social_sharing_data'];

    if($social_sharing_display=='1')
    {
        if($social_sharing_data!='' || $social_sharing_data!=null)
        {
            @$social_sharing_data = file_get_contents($websiteBasePath.'/'.$social_sharing_data);
            echo "$social_sharing_data";
        }
    }
?>

<script>
  jQuery(document).ready(function($){
    
    window.onload = function (){
      $(".bts-popup").delay(1000).addClass('is-visible');
    }
    
    //open popup
    $('.bts-popup-trigger').on('click', function(event){
      event.preventDefault();
      $('.bts-popup').addClass('is-visible');
    });
    
    //close popup
    $('.bts-popup').on('click', function(event){
      if( $(event.target).is('.popup-close') || $(event.target).is('.bts-popup') ) {
        event.preventDefault();
        $(this).removeClass('is-visible');
      }
    });
    //close popup when clicking the esc keyboard button
    $(document).keyup(function(event){
        if(event.which=='27'){
          $('.bts-popup').removeClass('is-visible');
        }
      });
  });
</script>

<script>
  $(document).ready(function(){
    $('.gender').change(function(){
      var value = $(this).val();
      var count_gender = $('.count_gender').val();
      var i;

      $('.gender-check_'+value).show();
      for(i=1;i<=count_gender;i++)
      {
        if(i!=value)
        {
          $('.gender-check_'+i).hide();
        }
      }
      
    });

    $('.from_date').change(function(){
      var from_date = $(this).val();
      var task = "Fetch_to_age_values";

      $.ajax({
        type:'post',
        url:'query/fetch-info-helper.php',
        data:'from_date='+from_date+'&task='+task,
        success:function(res)
        {
          $('.to_date').html(res);
        }
      });
    });
  });
</script>

</body>
</html>