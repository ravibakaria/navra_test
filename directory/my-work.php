<?php
	session_start();
  	if(!isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']=='' || $_SESSION['vendor_user']==null))
  	{
		header('Location: login.php');
		exit;
	}
	include("templates/header.php"); 

	$basepath = $WebSiteBasePath.'/';
	$vuserid = $_SESSION['vuserid'];
	if(isset($_POST['my_photos_upload']))
	{
		$sql_chk = $link->prepare("SELECT * FROM `vendor_gallery` WHERE `vuserid`='$vuserid' AND status <> '2'"); 
	    $sql_chk->execute();
	    $count=$sql_chk->rowCount();
	    
	    $no_files = sizeof($_FILES['my_photos']['name']);
	    $remaining_files = (6-$count);
	    if($no_files>$remaining_files)
	    {
	    	$file_upload_count = $remaining_files;
	    }
	    else
	    {
	    	$file_upload_count = $no_files;
	    }
	    
		if($remaining_files<=0)
		{
			$errorMessage = "You are already uploaded maximun number of photos.<br/>To upload new photos remove previous photo.<br/> Then reupload new photo";
		}
		else
		{
			if(isset($_FILES['my_photos']['name']) &&  $_FILES['my_photos']['name']!='')
			{

				ini_set("post_max_size", "2M");
				ini_set("upload_max_filesize", "2M");
				ini_set("memory_limit", "2M");

				$target_dir = 'uploads/'.$vuserid.'/photo/';
				if (!file_exists($target_dir)) 
				{
					try 
					{
						mkdir($target_dir, 0777, true);
					} 
					catch (Exception $ex) 
					{
						die("error");
					}
				}
				$filename = $_FILES["my_photos"]["name"];

				$all_files = array();
				for($i=0;$i<$file_upload_count;$i++)
				{
					$errorMessage    = null;
					$successMessage = null;				
					$file_type = $_FILES["my_photos"]["type"][$i];
					$file_size = $_FILES["my_photos"]["size"][$i];
					$file_tmp  = $_FILES['my_photos']['tmp_name'][$i];
					$file_name = $_FILES["my_photos"]["name"][$i];
					
					$file_name =str_replace(",","_",$file_name);
					$file_ext  = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));	

					$extensions= getAllowedFileAttachmentTypes();
					if(in_array($file_ext,$extensions)=== false)
					{
						if($errorMessage=='' || $errorMessage=null)
						{
							$errorMessage="File extension not allowed, please choose a jpg/jpeg/png file.";
						}
					}

					//! in_array( $ext, $allowed )
					if(in_array($file_ext,$extensions)=== false)
					{
						if($errorMessage=='' || $errorMessage=null)
						{
							$errorMessage="File extension not allowed, please choose a jpg/jpeg/png file.";
						}
					}
					//echo $errorMessage;exit;
					if($file_size > 2097152 || $file_size==0)
					{
						if($errorMessage=='' || $errorMessage=null)
						{
							$errorMessage='Photo size must be less than 2 MB';
						}
					}

					if (file_exists($target_dir.$file_name)) 
					{
						if($errorMessage=='' || $errorMessage=null)
						{
							$errorMessage = "You have already uploaded photo.";
						}
					}	

					if($errorMessage=='' || $errorMessage==null)
					{
						if (move_uploaded_file($file_tmp,$target_dir.$file_name)) 
					    {
					    	$sql_insert = "INSERT INTO `vendor_gallery`(`vuserid`, `photo`, `created_on`) VALUES ('$vuserid','$file_name',now())";

					    	if($link->exec($sql_insert))
							{
								$sql_member_log = "INSERT INTO vendor_activity_logs(vuserid,task,activity,IP_Address,created_On) VALUES('$vuserid','upload','photo-$file_name','$IP_Address',now())";
								$link->exec($sql_member_log);

								$successMessage = "Your photos uploaded successfully.";
							}
							else
							{
								$errorMessage = "Sorry, Something went wrong. Try after some time.";
							}
					    }
					}
				}
			}
		}
	}	
?>

<!-- meta info  -->

<title>
  <?php
  $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
  $string = str_replace("-", " ", $a);
  echo $title = ucwords($string);
  ?> -  <?php echo getWebsiteTitle(); ?>
</title>

<meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

<meta name="description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!"/>

<meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, vendor information, vendor lobin, vendor registration" />

<meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
<meta property="og:url" content="<?= $site_url; ?>" />
<meta property="og:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
<meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
<meta property="twitter:url" content="<?= $site_url; ?>" />
<meta property="twitter:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
    include("templates/styles.php"); 
    include("templates/menu.php"); 
?>

<div class="col-md-12">
  <br><br><br><br><br><br>
</div>

<div class="vendor-data">
	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    		<div class="row profile-row">
	    			<div class="text-left col-xs-12 col-sm-12 col-md-6 col-lg-6">
	    				<h1>My Work</h1>
	    			</div>
	    			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
	                    
	    			</div>
	    		</div>

	    		<br><br>
		    	<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div>
							<h2>General Instructions</h2>
							<ul style="list-style-type:circle">
								<li>
									For uploading multiple files press <b>"Ctrl" button</b> & select files.
								</li>
								<li>
									Only files with extention <b><?php echo getAllowedFileAttachmentTypesString();?> </b> is  allowed to upload.
								</li>
								<li>
									You are only allowed to upload <b>6 </b> files.
								</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<form action="<?php echo $_SERVER['PHP_SELF'];?>" method='POST'  enctype='multipart/form-data'>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top">
								<input type="file" name="my_photos[]" id="my_photos"  class='form-control my_photos file-upload' multiple>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="my_photos_status"></div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top">
								<center>
									<button type="submit" class="btn btn-primary  btn-sm btn-view-profile my_photos_upload website-button" name="my_photos_upload" style="margin-left:-10px;">
									Upload
									</button>
								</center>
							</div>
						</form>
					</div>
				</div>
				<hr>
				<div class="row">
					
					<div class=" main_photo_section_status">
						<center><img src="../images/loader/loader.gif" class='img-responsive loading_img1' id='loading_img1' style='width:80px; height:80px; display:none;'/></center>
						<?php
							if(isset($errorMessage) && ($errorMessage!='' || $errorMessage!=null))
							{
								echo "<div class='alert alert-danger my-photo-alert'><span class='fa fa-exclamation-circle'></span> ".$errorMessage."</div>";
							}

							if(isset($successMessage) && ($successMessage!='' || $successMessage!=null))
							{
								echo "<div class='alert alert-success my-photo-alert'><span class='fa fa-check'></span> ".$successMessage."</div>";
							}
						?>
					</div>
				</div>
				<div class="row">
					<div class="mg-files" data-sort-destination data-sort-id="media-gallery">
						<?php
							$stmt = $link->prepare("SELECT * FROM `vendor_gallery` WHERE `vuserid`='$vuserid' AND status <> '2'"); 
						    $stmt->execute();
						    $count=$stmt->rowCount();

						    if($count==0)
						    {
						    	echo "<h4>You have not uploaded any photos yet.</h4>";
						    }
						    else
						    if($count>0)
						    {	
						    	echo "<h3>Your uploaded photos.</h3>";
						    }

						    $result = $stmt->fetchAll();
							foreach( $result as $row )
							{
								$filename = $row['photo'];
								$photo_id =  $row['id'];
								$photo =  'uploads/'.$vuserid.'/photo/'.$filename;
								$created_at = $row['created_on']; 
								$status = $row['status']; 
						?>
						<div class="isotope-item document col-sm-6 col-md-4 col-lg-4">
							<div class="thumbnail">
								<div class="popup-gallery">
									<a class="thumb-image" href="<?php echo $photo;?>"  title="<?php echo $filename; ?>">
										<img src="<?php echo $photo;?>" class="img-responsive member-photos">
									</a>
								</div>
								<div class="mg-description">
									<small class="pull-left">Uploaded On</small>
									<small class="pull-right"><?php echo date('d-M-Y h:i A',strtotime($created_at));?></small>
								</div><br/>
								<div class="row">
									<?php
										if($status=='1')
										{
											echo "<center><a class='btn btn-success btn-sm' disabled><i class='fa fa-check'></i> Approved</a></center>";
										}
										else
										if($status=='0')
										{
											echo "<center><a class='btn btn-danger btn-sm my_photos_delete' id='$photo_id'><i class='fa fa-trash-o'></i> Delete</a></center>";
										}
									?>
									
								</div>
							</div>

						</div>

						<?php
							}
						?>
					</div>
				</div>
				<br><br>
			</div>
		</div>
	</div>
</div>

</section>
<?php
  	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		$('#my_photos').click(function(){
			$('.my_photos_status').html("");
		});

		$('.my_photos_upload').click(function(){
			var my_photos = $("#my_photos").val();
			if(my_photos=='' || my_photos==null)
			{
				$('.my_photos_status').html("<div class='alert alert-danger margin-top' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>Select photos to upload</div>");
				return false;
			}

			$('.loading_img').show();
			$('.my_photos_status').html("");
		});

		$('.my_photos_delete').click(function(){
			var my_photos_remove = $(this).attr('id');
			var task = "Delete_Photo_Gallery";
			var r = confirm("Do you really want to delete this photo");
			if(r==true)
			{
				$('.loading_img1').show();
				$.ajax({                //delete user photos
					type : 'post',
					url : 'query/my-work-helper.php',
					data : 'my_photos_remove='+my_photos_remove+'&task='+task,
					success : function(res)
					{
						$('.loading_img1').hide();
						if(res=='success')
						{
							$('.main_photo_section_status').html("<div class='alert alert-success photo_delete_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><i class='fa fa-check'></i> Success! </strong>Photo deleted successfully.</div>");
							window.location.assign('my-work.php');
						}
						else
						{
							$('.main_photo_section_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error! </strong>"+res+"</div>");
							return false;
						}
					}
				});
			}
			else
			{
				return false;
			}
		});
	});
</script>