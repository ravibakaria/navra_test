<?php
	session_start();
	
	if(isset($_SESSION['logged_in']))
	{
		unset($_SESSION['logged_in']);
	}

	if(isset($_SESSION['vendor_user']))
	{
		unset($_SESSION['vendor_user']);
	}

	if(isset($_SESSION['vuserid']))
	{
		unset($_SESSION['vuserid']);
	}

	if(isset($_SESSION['vuser_email']))
	{
		unset($_SESSION['vuser_email']);
	}

	session_destroy();
	header('Location:index.php');
	exit;
?>