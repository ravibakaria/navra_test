<?php
	session_start();
	if(isset($_SESSION['logged_in']) && @$_SESSION['vendor_user']!='')
	{
		header('Location: dashboard.php');
		exit;
	}

	if(isset($_SESSION['logged_in']) && @$_SESSION['client_user']!='')
	{
		header('Location: ../users/');
		exit;
	}

	if(isset($_SESSION['logged_in']) && @$_SESSION['admin_user']!='')
    {
        header('Location: ../admin/');
        exit;
    }

	include("templates/header.php");

	$wedding_directory_display_or_not = getWeddingDirectoryDisplayOrNot();
    $custom_content = getWeddingDirectoryCustomContent(); 

    if($wedding_directory_display_or_not=='0' || $wedding_directory_display_or_not=='' || $wedding_directory_display_or_not==null)
    {
      header("Location: ../index.php");
      exit;
    }
    
	$recaptchaAllowed = getreCaptchaAllowed();
	if($recaptchaAllowed == "1")
	{
		$recaptchaSiteKey=getreCaptchaSiteKey();
		$recaptchaSecretKey=getreCaptchaSecretKey();
	}

	$errorMessage = null;
	$successMessage = null;

	if($recaptchaAllowed == "1")
	{
		$recaptchaSiteKey=getreCaptchaSiteKey();
		$recaptchaSecretKey=getreCaptchaSecretKey();

		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		{
			//your site secret key
			$secret = $recaptchaSecretKey;
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);
			if($responseData->success)
			{
				if (isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['password']) && $_POST['password'] != '')
				{
					$email = quote_smart($_POST['email']);
	        		$password = md5(quote_smart($_POST['password']));

	        		$sql_chk = $link->prepare("SELECT * FROM `vendors` WHERE email='$email'"); 
	                $sql_chk->execute();
	                $count=$sql_chk->rowCount();
	                if($count>0)
	                {
	                	$userData = $sql_chk->fetch(PDO::FETCH_OBJ);
	                	$user_id = $userData->vuserid;
	                    $user_firstname = $userData->firstname;
	                	$user_password = $userData->password;
	                	$user_status = $userData->status;
	                	$user_isEmailVerified = $userData->isEmailVerified;
	                        $IP_Address = $_SERVER['REMOTE_ADDR'];
	                        
	                	if($user_status=='1' || $user_status=='2')
	                	{
	                		if($user_isEmailVerified=='1')
	                		{
	                			if($user_password==$password)
	                			{
	                                $sql_insert_log = "INSERT INTO `userlogs`(`userid`,`user_firstname`,`user_type`,`IP_Address`,`loggedOn`) VALUES('$user_id','$user_firstname','Vendor','$IP_Address',now())";

	                                $sql_member_log = "INSERT INTO vendor_activity_logs(vuserid,task,activity,IP_Address,created_On) VALUES('$user_id','login','to system','$IP_Address',now())";

	                                $link->exec($sql_insert_log);   //Inserting logs for login

	                                $link->exec($sql_member_log);   //Inserting activity logs

	                                $_SESSION['logged_in'] = true;
	                				$_SESSION['vendor_user'] = true;
	                				$_SESSION['vuserid'] = $user_id;
	                				$_SESSION['vuser_email'] = $email;

	                				$successMessage = "success";
	                			}
	                			else
	                			{
	                                $errorMessage = "Please enter valid email id & password.<br/>";
	                			}
	                		}
	                		else
	                		if($user_isEmailVerified=='0')
	                		{       
	                            $errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email id.<br/>
                                        <a class='btn btn-danger verify-email' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
	                		}
	                	}
	                	else
	                	if($user_status=='0')
	                	{
	                		$errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email address.<br/>
	                            <a class='btn btn-danger verify-email' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
	                	}
	                    else
	                    if($user_status=='3')
	                    {
	                        $errorMessage = "Your account is suspended.<br/>Contact administrator to activate your account.";
	                    }
	                }
	                else
	                {
	                	$errorMessage = "Please enter valid email id & password.";
	                }	
				}
			}
		}
	}
	else
	{
		if (isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['password']) && $_POST['password'] != '')
		{
			$email = quote_smart($_POST['email']);
    		$password = md5(quote_smart($_POST['password']));

    		$sql_chk = $link->prepare("SELECT * FROM `vendors` WHERE email='$email'"); 
            $sql_chk->execute();
            $count=$sql_chk->rowCount();
            if($count>0)
            {
            	$userData = $sql_chk->fetch(PDO::FETCH_OBJ);
            	$user_id = $userData->id;
                $user_firstname = $userData->firstname;
            	$user_password = $userData->password;
            	$user_status = $userData->status;
            	$user_isEmailVerified = $userData->isEmailVerified;
                    $IP_Address = $_SERVER['REMOTE_ADDR'];
                    
            	if($user_status=='1' || $user_status=='2')
            	{
            		if($user_isEmailVerified=='1')
            		{
            			if($user_password==$password)
            			{
                            $sql_insert_log = "INSERT INTO `userlogs`(`userid`,`user_firstname`,`user_type`,`IP_Address`,`loggedOn`) VALUES('$user_id','$user_firstname','Vendor','$IP_Address',now())";

                            $sql_member_log = "INSERT INTO vendor_activity_logs(vuserid,task,activity,IP_Address,created_On) VALUES('$user_id','login','to system','$IP_Address',now())";

                            $link->exec($sql_insert_log);   //Inserting logs for login

                            $link->exec($sql_member_log);   //Inserting activity logs

                            $_SESSION['logged_in'] = true;
            				$_SESSION['vendor_user'] = true;
            				$_SESSION['user_id'] = $user_id;
            				$_SESSION['user_email'] = $email;

            				$successMessage = "success";
            			}
            			else
            			{
                            $errorMessage = "Please enter valid email id & password.<br/>";
            			}
            		}
            		else
            		if($user_isEmailVerified=='0')
            		{       
                        $errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email id.<br/>
                                <a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
            		}
            	}
            	else
            	if($user_status=='0')
            	{
            		$errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email address.<br/>
                        <a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
            	}
                else
                if($user_status=='3')
                {
                    $errorMessage = "Your account is suspended.<br/>Contact administrator to activate your account.";
                }
            }
            else
            {
            	$errorMessage = "Please enter valid email id & password.";
            }	
		}
	}
	
?>

<!-- meta info  -->

  <title>
    <?php
    $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
    $string = str_replace("-", " ", $a);
    echo $title = ucwords($string);
    ?>  -  <?php echo getWebsiteTitle(); ?>
  </title>

  	<meta name="title" content="<?= $site_title.' - Login'; ?>" />

  	<meta name="description" content="<?= $site_title; ?> - Login Here To find Verified Profiles! If you are Looking For Groom or Bride – we have a perfect match for you."/>

  	<meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

  	<meta property="og:title" content="<?php echo $site_title.' - Login'; ?>" />
  	<meta property="og:url" content="<?= $site_url; ?>" />
  	<meta property="og:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  	<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
  	<meta property="twitter:title" content="<?= $site_title.' - Login'; ?>" />
  	<meta property="twitter:url" content="<?= $site_url; ?>" />
  	<meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  	<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

  	<script src="../js/jquery.js"></script>

  	<script>
		function onSubmit(token)
		{
		    document.getElementById("frmLogin").submit();
		}

		function validate(event) 
		{
		    event.preventDefault();
		    var email = $('.email').val();
			var password = $('.password').val();
			var recaptchaAllowed = $('.recaptchaAllowed').val();
			var task = "login_client_user";

			/* Email Validation */
			if(email=='' || email==null)
	        {
	        	$('.server_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty! </strong> Enter email address.</div>");
	            return false;
	        }

	        /* Password validation */
	        if(password=='' || password==null)
	        {
	        	$('.server_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty! </strong> Enter your password.</div>");
	            return false;
	        }

	        //recaptcha validation 
	        if(recaptchaAllowed=='1')
			{
				grecaptcha.execute();
			}
			else
			{
				document.getElementById("frmLogin").submit();
			}

			$('.loading_img').show();
			$('.server_data_status').html("");
		}

		function onload() 
		{
		    var element = document.getElementById('recaptcha-submit');
		    element.onclick = validate;
		}
	</script>
<?php
  include("templates/styles.php"); 
  include("templates/menu.php"); 
?>

<!-- Google recaptcha -->
<?php
	if($recaptchaAllowed == "1")
	{
		echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
	}
?>

<div class="">
	<!-- start: page -->
	<section class="body-sign member-login-box">
		<div class="center-sign login-box">

			<div class="panel panel-sign">
				<div class="panel-title-sign mt-xl text-right">
					<h1 class="title text-uppercase text-bold m-none title-login-forgot"><i class="fa fa-user mr-xs"></i>Vendor Login</h1>
				</div>
				<div class="panel-body">
					<form name="frmLogin" id="frmLogin" autocomplete="off" method="post"  action="<?php echo $_SERVER['PHP_SELF'];?>">
						<div class="form-group mb-lg">
							<label>Email Id</label>
							<div class="input-group input-group-icon">
								<input name="email" type="text" class="form-control input-lg email"  placeholder="Ex: abc@gmail.com" value="<?php echo @$email; ?>"/>
								<span class="input-group-addon">
									<span class="icon icon-lg">
										<i class="fa fa-user"></i>
									</span>
								</span>
							</div>
						</div>

						<div class="form-group mb-lg">
							<div class="clearfix">
								<label class="pull-left">Password</label>
								<a href="forgot-password.php" class="pull-right">Forgot Password?</a>
							</div>
							<div class="input-group input-group-icon">
								<input name="password" type="password" class="form-control input-lg password"  placeholder="Password"  value="<?php echo @$password; ?>"/>
								<span class="input-group-addon">
									<span class="icon icon-lg">
										<i class="fa fa-lock"></i>
									</span>
								</span>
							</div>
						</div>
						
						<div class="form-group">
				        	<?php
				        		if ($errorMessage != '' || $errorMessage != null)
								{
									echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
									echo "<strong>Error!</strong> $errorMessage";
									echo "</div>";
								}

								if ($successMessage != '' || $successMessage != null)
								{
									echo "<script>window.location.assign('dashboard.php');</script>";
								}
				        	?>
				        </div>

						<input type="hidden" class="recaptchaAllowed" name="recaptchaAllowed" value="<?php echo $recaptchaAllowed;?>">
						
						<div class="row">
							<img src="../images/loader/loader.gif" class='img-responsive loading_img centered-loading-image' id='loading_img' alt='loading' style='width:80px; height:80px; display:none;'/>
						</div>

						<div class="mb-xs text-center server_data_status">
							
						</div>

						<div class="row">
							<div class="col-sm-6 centered-data">
								<a  href="free-register.php">Free Registration!</a>
							</div>
							<div class="col-sm-6 text-right centered-data">
								<input type="submit" name="login btn"  class="btn btn-primary btn-lg btn-block login_btn website-button" id="recaptcha-submit" value="Sign In">
							</div>
						</div>
						<?php
							if($recaptchaAllowed == "1")
							{
								echo "<div class='form-group'>
									<div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
								</div>";
							}
						?>
						
						<script>onload();</script>
					</form>
				</div>
			</div>
		</div>
	</section>

</div>
</section>
<?php
	include("templates/footer.php");
?>
<script>
    $(document).ready(function(){
         $('.verify-email').click(function(){
             $(this).attr('disabled',true);
             $('.loading_img').show();
         });
    });
</script>
</body>
</html>