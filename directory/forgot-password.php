<?php
	session_start();
	if(isset($_SESSION['logged_in']) && $_SESSION['client_user']!='')
	{
		header('Location: dashboard.php');
		exit;
	}
	
	include("templates/header.php");
    include('../config/setup-values.php');   //get master setup values
    include('../config/email/email_style.php');   //get master setup values
    include('../config/email/email_templates.php');   //get master setup values
    include('../config/email/email_process.php');
    
    $WebSiteBasePath = getWebsiteBasePath();
    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }

    /********    Submit For status    **********/
	$recaptchaAllowed = getreCaptchaAllowed();

	if($recaptchaAllowed == "1")
	{
		$recaptchaSiteKey=getreCaptchaSiteKey();
		$recaptchaSecretKey=getreCaptchaSecretKey();
	}

	$errorMessage = null;
	$successMessage = null;

	if($recaptchaAllowed == "1")
	{
		$recaptchaSiteKey=getreCaptchaSiteKey();
		$recaptchaSecretKey=getreCaptchaSecretKey();

		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		{	
			//your site secret key
			$secret = $recaptchaSecretKey;
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);
			if($responseData->success)
			{
				if (isset($_POST['email']) && $_POST['email'] != '')
				{
					$email = quote_smart($_POST['email']);
			
					$sql_chk = $link->prepare("SELECT * FROM `vendors` WHERE `email`='$email'"); 
			        $sql_chk->execute();

			        $count=$sql_chk->rowCount();

			        if($count>0)
			        {
			        	$userData = $sql_chk->fetch(PDO::FETCH_OBJ);
			            $id = $userData->vuserid;
			            $firstname = $userData->firstname;
			            $email = $userData->email;
			            $status = $userData->status;

			            if($status=='0')
			            {
			                $errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email address.<br/>
			                        <a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
			            }
			            else
			            if($status=='2')
			            {
			                $errorMessage = "Your account is disabled.<br/>Contact administrator to activate your account.";
			            }
			            else
			            if($status=='3')
			            {
			                $errorMessage = "Your account is suspended.<br/>Contact administrator to activate your account.";
			            }

			            if($errorMessage == null)
			            {
			            	$Special_char_string = '[!@#$%^)*_(+=}{|:;,.<>}]'; 
				            $pos1 = rand(0,(strlen($Special_char_string)-1));
				            $pass1 = $Special_char_string[$pos1];

				            $Capital_char_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
				            $pos2 = rand(0,(strlen($Capital_char_string)-1));
				            $pass2 = $Capital_char_string[$pos2];

				            $Number_char_string = '1234567890'; 
				            $pos3 = rand(0,(strlen($Number_char_string)-1));
				            $pass3 = $Number_char_string[$pos3];

				            $salt = "abchefghjkmnpqrstuvwxyz0123456789";
				            srand((double)microtime()*1000000);

				            $i = 0;
				            while ($i <= 5) 
				            {
				                $num = rand() % 33;
				                $tmp = substr($salt, $num, 1);
				                $pass1 = $pass1 . $tmp;
				                $i++;
				            }
				                
				            $new_pass = $pass2.$pass3.$pass1;    //
				            $tmp_password = md5($new_pass);     // random 

				            $sql_update = "UPDATE `vendors` SET `password`='$tmp_password' WHERE `vuserid`='$id' AND `email`='$email'";

				            $login_link = "<a href='$WebSiteBasePath/directory/login.php' target='_blank'>$WebSiteBasePath/directory/login.php</a>";

				            if($link->exec($sql_update))
				            {
				                $SocialSharing = getSocialSharingLinks();   // social sharing links
				                
				                $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

				                $ForgotPasswordSubject = str_replace('$site_name',$WebSiteTitle,$ForgotPasswordSubject);   //replace subject variables with value

				                $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

				                $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

				                $ForgotPasswordMessage = str_replace(array('$first_name','$site_name','$login_link','$email_address','$new_password','$signature'),array($firstname,$WebSiteTitle,$login_link,$email,$new_pass,$GlobalEmailSignature),$ForgotPasswordMessage);  //replace footer variables with value


				                $subject = $ForgotPasswordSubject;
				                
				                $message  = '<!DOCTYPE html>';
				                $message .= '<html lang="en">
				                    <head>
				                    <meta charset="utf-8">
				                    <meta name="viewport" content="width=device-width">
				                    <title></title>
				                    <style type="text/css">'.$EmailCSS.'</style>
				                    </head>
				                    <body style="margin: 0; padding: 0;">';
				                //echo $message;exit;
				                $message .= $EmailGlobalHeader;

				                $message .= $ForgotPasswordMessage;                                               
				                $message .= $GlobalEmailSignature;

				                $message .= $EmailGlobalFooter;
				                
				                $mailto = $email;
				                $mailtoname = $firstname;
				                
				                $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

				                $sql_member_log = "INSERT INTO vendor_activity_logs(vuserid,task,activity,IP_Address,created_On) VALUES('$id','forgot-password','forgot-password','$IP_Address','$today')";
                                $link->exec($sql_member_log);
    
                                /*************      Email log      ***************/
                                $sql_member_email_log = "INSERT INTO vendor_email_logs(vuserid,task,activity,sent_On) VALUES('$id','forgot password','email','$today')";

				                if($emailResponse == 'success')
				                {
				                	$link->exec($sql_member_email_log);
			                        $successMessage = "success";
				                }
				                else
				                {
				                    $errorMessage = "Error in sending login details to user.";
				                }
				            }
				            else
				            {
				                $errorMessage = "Something went wrong. Try after some time.";
				            }
			        	}	            
			        }
			        else
			        {
			            $errorMessage = "Email id not exists. Please enter valid email id.";
			        }	
				}
			}
			
		}
	}
	else
	{
		if (isset($_POST['email']) && $_POST['email'] != '')
		{
			$email = quote_smart($_POST['email']);
	
			$sql_chk = $link->prepare("SELECT * FROM `vendors` WHERE `email`='$email'"); 
	        $sql_chk->execute();

	        $count=$sql_chk->rowCount();

	        if($count>0)
	        {
	        	$userData = $sql_chk->fetch(PDO::FETCH_OBJ);
	            $id = $userData->vuserid;
	            $firstname = $userData->firstname;
	            $email = $userData->email;
	            $status = $userData->status;

	            if($status=='0')
	            {
	                $errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email address.<br/>
	                        <a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
	            }
	            else
	            if($status=='2')
	            {
	                $errorMessage = "Your account is disabled.<br/>Contact administrator to activate your account.";
	            }
	            else
	            if($status=='3')
	            {
	                $errorMessage = "Your account is suspended.<br/>Contact administrator to activate your account.";
	            }

	            if($errorMessage == null)
	            {
	            	$Special_char_string = '[!@#$%^)*_(+=}{|:;,.<>}]'; 
		            $pos1 = rand(0,(strlen($Special_char_string)-1));
		            $pass1 = $Special_char_string[$pos1];

		            $Capital_char_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
		            $pos2 = rand(0,(strlen($Capital_char_string)-1));
		            $pass2 = $Capital_char_string[$pos2];

		            $Number_char_string = '1234567890'; 
		            $pos3 = rand(0,(strlen($Number_char_string)-1));
		            $pass3 = $Number_char_string[$pos3];

		            $salt = "abchefghjkmnpqrstuvwxyz0123456789";
		            srand((double)microtime()*1000000);

		            $i = 0;
		            while ($i <= 5) 
		            {
		                $num = rand() % 33;
		                $tmp = substr($salt, $num, 1);
		                $pass1 = $pass1 . $tmp;
		                $i++;
		            }
		                
		            $new_pass = $pass2.$pass3.$pass1;    //
		            $tmp_password = md5($new_pass);     // random 

		            $sql_update = "UPDATE `vendors` SET `password`='$tmp_password' WHERE `vuserid`='$id' AND `email`='$email'";

		            $login_link = "<a href='$WebSiteBasePath/directory/login.php' target='_blank'>$WebSiteBasePath/directory/login.php</a>";

		            if($link->exec($sql_update))
		            {
		                $SocialSharing = getSocialSharingLinks();   // social sharing links
		                
		                $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

		                $ForgotPasswordSubject = str_replace('$site_name',$WebSiteTitle,$ForgotPasswordSubject);   //replace subject variables with value

		                $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

		                $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

		                $ForgotPasswordMessage = str_replace(array('$first_name','$site_name','$login_link','$email_address','$new_password','$signature'),array($firstname,$WebSiteTitle,$login_link,$email,$new_pass,$GlobalEmailSignature),$ForgotPasswordMessage);  //replace footer variables with value


		                $subject = $ForgotPasswordSubject;
		                
		                $message  = '<!DOCTYPE html>';
		                $message .= '<html lang="en">
		                    <head>
		                    <meta charset="utf-8">
		                    <meta name="viewport" content="width=device-width">
		                    <title></title>
		                    <style type="text/css">'.$EmailCSS.'</style>
		                    </head>
		                    <body style="margin: 0; padding: 0;">';
		                //echo $message;exit;
		                $message .= $EmailGlobalHeader;

		                $message .= $ForgotPasswordMessage;                                               
		                $message .= $GlobalEmailSignature;

		                $message .= $EmailGlobalFooter;
		                
		                $mailto = $email;
		                $mailtoname = $firstname;
		                
		                $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);
		                
		                $sql_member_log = "INSERT INTO vendor_activity_logs(vuserid,task,activity,IP_Address,created_On) VALUES('$id','forgot-password','forgot-password','$IP_Address','$today')";
                        $link->exec($sql_member_log);
    
                        /*************      Email log      ***************/
                        $sql_member_email_log = "INSERT INTO vendor_email_logs(vuserid,task,activity,sent_On) VALUES('$id','forgot password','email','$today')";
                        
		                if($emailResponse == 'success')
		                {
		                    $link->exec($sql_member_email_log);
	                        $successMessage = "success";
		                }
		                else
		                {
		                    $errorMessage = "Error in sending login details to user.";
		                }
		            }
		            else
		            {
		                $errorMessage = "Something went wrong. Try after some time.";
		            }
	        	}	            
	        }
	        else
	        {
	            $errorMessage = "Email id not exists. Please enter valid email id.";
	        }	
		}
	}
?>

	<!-- meta info  -->

  	<title>
	    <?php
		    $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		    $string = str_replace("-", " ", $a);
		    echo $title = ucwords($string);
	    ?>  -  <?php echo getWebsiteTitle(); ?>
  	</title>

  	<meta name="title" content="<?= $site_title.' - Forgot Password'; ?>" />

  	<meta name="description" content="<?= $site_title; ?> - Forgot Password."/>

  	<meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

  	<meta property="og:title" content="<?php echo $site_title.' - Forgot Password'; ?>" />
  	<meta property="og:url" content="<?= $site_url; ?>" />
  	<meta property="og:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  	<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
  	<meta property="twitter:title" content="<?= $site_title.' - Forgot Password'; ?>" />
  	<meta property="twitter:url" content="<?= $site_url; ?>" />
  	<meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  	<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />
  	
  	<script src="../js/jquery.js"></script>

  	<script>
		function onSubmit(token)
		{
		    document.getElementById("frmLogin").submit();
		}

		function validate(event) 
		{
		    event.preventDefault();
		    var email = $('.email').val();
			var recaptchaAllowed = $('.recaptchaAllowed').val();
			var task = "Forgot-Password-User";
			
			// Email Validation 
			if(email=='' || email==null)
            {
            	$('.server_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty! </strong> Enter email address.</div>");
                return false;
            }

	        //recaptcha validation 
	        if(recaptchaAllowed=='1')
			{
				grecaptcha.execute();
			}
			else
			{
				document.getElementById("frmLogin").submit();
			}

			$('.loading_img').show();
			$('.server_data_status').html("");
		}

		function onload() 
		{
		    var element = document.getElementById('recaptcha-submit');
		    element.onclick = validate;
		}
	</script>

<?php
  	include("templates/styles.php"); 
  	include("templates/menu.php");
?>

<!-- Google recaptcha -->
<?php
	if($recaptchaAllowed == "1")
	{
		echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
	}
?>

<br/>
<div class="">
	<!-- start: page -->
	<section class="body-sign member-login-box">
		<div class="center-sign">
			<div class="panel panel-sign">
				<div class="panel-title-sign  text-right">
					<h2 class="title text-uppercase text-bold m-none title-login-forgot"><i class="fa fa-lock mr-xs"></i> Forgot Password</h2>
				</div>
				<div class="panel-body">
					<form name="frmLogin" id="frmLogin" autocomplete="off" method="post"  action="<?php echo $_SERVER['PHP_SELF'];?>">
						<div class="form-group ">
							<label>Email Id</label>
							<div class="input-group input-group-icon">
								<input name="email" type="text" class="form-control input-lg email"  placeholder="Ex: abc@gmail.com" value="<?php echo @$email;?>"/>
								<span class="input-group-addon">
									<span class="icon icon-lg">
										<i class="fa fa-user"></i>
									</span>
								</span>
							</div>
						</div>

						<div class="form-group">
				        	<?php
				        		if ($errorMessage != '' || $errorMessage != null)
								{
									echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
									echo "<strong>Error!</strong> $errorMessage";
									echo "</div>";
								}

								if ($successMessage != '' || $successMessage != null)
								{
								    echo "<div class='alert alert-success col-sm-12 success_status' style='padding: 10px; margin-bottom: 10px;'>";
									echo "<strong><i class='fa fa-check'></i> Success! </strong>Password sent successfully. Please check Your email inbox/spam folder.";
									echo "</div>";
									
									
									echo "<script>
									        $('.success_status').fadeTo(2000, 500).slideUp(500, function(){
                                            $('.success_status').slideUp(200);
                                            window.location.assign('login.php');
                                        });
                                    </script>";
								}
				        	?>
				        </div>

						<input type="hidden" class="recaptchaAllowed" value="<?php echo $recaptchaAllowed;?>">

						<div class="row">
							<img src="../images/loader/loader.gif" class='img-responsive loading_img centered-loading-image' id='loading_img' alt='loader' style='width:80px; height:80px; display:none;'/>
						</div>

						<div class=" text-center server_data_status">
							
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  forgot-btn-fg-password">
										<input type="submit" name="login btn"  class="btn btn-primary btn-block login_btn website-button" id="recaptcha-submit" value="Reset & Send Password">
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  forgot-btn-fg-password">
										<a class="btn btn-primary website-button" href="login.php">Back To Login</a>
									</div>
								</div>
							</div>
						</div>
						<?php
							if($recaptchaAllowed == "1")
							{
								echo "<div class='form-group'>
									<div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
								</div>";
							}
						?>
						
						<script>onload();</script>
						
					</form>
				</div>
			</div>
		</div>
	</section>

</div>
</section>

<?php
	include("templates/footer.php");
?>

</body>
</html>