<?php
  session_start();
  include("templates/header.php"); 

    $wedding_directory_display_or_not = getWeddingDirectoryDisplayOrNot();
    $custom_content = getWeddingDirectoryCustomContent(); 

    if($wedding_directory_display_or_not=='0' || $wedding_directory_display_or_not=='' || $wedding_directory_display_or_not==null)
    {
      header("Location: ../index.php");
      exit;
    }
?>

<!-- meta info  -->

    <title><?php echo $site_title.' - '.$site_tagline;?></title>

    <meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

    <meta name="description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!"/>

    <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, vendor information, vendor lobin, vendor registration" />

    <meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
    <meta property="og:url" content="<?= $site_url; ?>" />
    <meta property="og:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
    <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
    <meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
    <meta property="twitter:url" content="<?= $site_url; ?>" />
    <meta property="twitter:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
    <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
    include("templates/styles.php"); 
    include("templates/menu.php");
?>

<div class="col-md-12">
  <br><br><br><br>
</div>


<div class="">
  <!-- Section: intro -->
  <section id="vendor-intro1">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 vendor-intro1 centered-data">
      <h1> Wedding Directory </h1>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
      <?php
        if(isset($_SESSION['logged_in']) && ($_SESSION['client_user']!='' || $_SESSION['client_user']!=null))
        {
          
        }
        else
        if(isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']!='' || $_SESSION['vendor_user']!=null))
        {
          echo "<a href='dashboard.php' class='btn website-button'>Dashboard</a>
          <a href='logout.php' class='btn website-button'>Logout</a>";
        }
        else
        if(!isset($_SESSION['logged_in']))
        {
          echo "<a href='free-register.php' class='btn website-button'>VENDOR REGISTRATION</a>
          <a href='login.php' class='btn website-button'>VENDOR LOGIN</a>";
        }
      ?>
      
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?php
        if($wedding_directory_display_or_not=='1' && ($custom_content!='' || $custom_content!=null))
        {
            echo "<hr>";
            echo $custom_content;
        }
      ?>
      <hr class="dotted">
    </div>
    <div class="row profile-filter-div common-row-div">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <?php
            $query  = "SELECT * FROM `membercategory` WHERE status='1' ORDER BY `name` ASC";
            $stmt   = $link->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll();
            foreach( $result as $row )
            {
              $category_name =  $row['name'];
              $category_id = $row['id']; 
              $category_slug = $row['slug']; 

              $web_link = 'category-'.$category_slug;
              echo "<a href='$web_link'>";
                echo "<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
                      <blockquote class='info' style='background-color:#f0f0f0;'>
                        <p align='justify'><b> $category_name</b></p>
                      </blockquote>
                  </div>";
              echo "</a>";
            }
          ?>
      </div>
    </div>
  </section>
  <!-- /Section: intro -->
</div>

</section>
<?php
  include("templates/footer.php");
?>

</body>
</html>