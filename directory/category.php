<?php
  session_start();
  include("templates/header.php"); 

  $cat_slug = $_GET['category'];
  $category_name =  getdirectorynamefromslug($cat_slug);
  $category_id =  getdirectoryidfromslug($cat_slug);
?>
    <!-- meta info  -->

    <title><?php echo $category_name.' - '.$site_title;?></title>

    <meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

    <meta name="description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!"/>

    <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, vendor information, vendor lobin, vendor registration" />

    <meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
    <meta property="og:url" content="<?= $site_url; ?>" />
    <meta property="og:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
    <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
    <meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
    <meta property="twitter:url" content="<?= $site_url; ?>" />
    <meta property="twitter:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
    <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
    include("templates/styles.php"); 
    include("templates/menu.php"); 
?>

<div class="col-md-12">
  <br><br><br><br><br><br><br>
</div>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 centered-data">
      <h1><?php echo $category_name;?><h1>
      <center><hr class="dotted" style="width:50%;"></center>
  </div>
</div>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="panel panel-default vendor-category-list-panel">
          <div class="panel-heading"><h2>Wedding Directory Category</h2></div>
          <div class="panel-body">
            <ul>
              <?php
                $query  = "SELECT * FROM `membercategory` ORDER BY `name` ASC";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                foreach( $result as $row )
                {
                  $cat_name =  $row['name'];
                  $cat_id = $row['id']; 
                  $cat_slug = $row['slug']; 

                  $categoryCount = getCategoryCount($cat_id);

                  $web_link = 'category-'.$cat_slug;
                  echo "<a href='$web_link' class='category-links'>
                          <li>
                            <p align='justify'><b> $cat_name ($categoryCount)</b></p>
                          </li>
                        </a>";
                }
              ?>
            </ul>
          </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
      <?php
        $sql_vendor_info  = "SELECT * FROM `vendors` WHERE category_id='$category_id'";
        $stmt_vendor_info   = $link->prepare($sql_vendor_info);
        $stmt_vendor_info->execute();
        $count_vendor_info = $stmt_vendor_info->rowCount();
        $result_vendor_info = $stmt_vendor_info->fetchAll();
        if($count_vendor_info>0)
        {
          foreach( $result_vendor_info as $row_vendor_info )
          {
            $vuserid =  $row_vendor_info['vuserid'];
            $firstname =  $row_vendor_info['firstname'];
            $lastname =  $row_vendor_info['lastname'];
            $email =  $row_vendor_info['email'];
            $city =  $row_vendor_info['city'];
            $state =  $row_vendor_info['state'];
            $country =  $row_vendor_info['country'];
            $short_desc =  $row_vendor_info['short_desc'];
            $company_name =  $row_vendor_info['company_name'];

            if(strlen($short_desc)>400)
            {
              $short_desc = substr($short_desc, 0, 400)."...[read more]";
            }

            $profile_pic = getVendorProfilePic($vuserid);
            $profile_pic_path = 'uploads/'.$vuserid.'/profile/'.$profile_pic;
      ?>
        <div class="panel panel-default vendor-category">
          <div class="panel-body vendor-category-info">
              <a href="Profile-<?php echo $vuserid;?>.html"  class='category-links'>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 centered-data">
                  <?php
                    if($profile_pic=='' || $profile_pic==null)
                    {
                      echo "<img src='../images/no_profile_pic.png' class='rounded img_preview' alt='$firstname'>";
                    }
                    else
                    {
                      echo "<img src='$profile_pic_path' class='rounded img_preview' alt='$firstname' >";
                    }
                  ?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 text-left">
                  <h3><b><?php echo $company_name;?></b></h3>
                  
                  <?php echo $short_desc;?>
                  <hr/>
                  <b>Location: </b> &nbsp;&nbsp;&nbsp;
                  <?php echo getUserCityName($city).", ".getUserStateName($state).", ".getUserCountryName($country);?>
                  <br>
                  <a href="Profile-<?php echo $vuserid;?>.html" class="btn centered-data website-button">View Profile</a>
                </div>
              </a>
          </div>
        </div>

      <?php
          }
        }
        else
        if($count_vendor_info==0)
        {
          echo "<br><br><br><br>";
          echo "<h3>Ooops! No result found. Please try after some time</h3>";
        }
      ?>
        
    </div>
    
  </div>
</div>


</section>

<?php
  include("templates/footer.php");
?>

</body>
</html>