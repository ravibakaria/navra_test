<?php
	if(!isset($_SESSION))
	{
	    ob_start();
	    session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['vendor_user']=='' || $_SESSION['vendor_user']==null))
  	{
    	header("Location: ../index.php");
    	exit;
  	}

  	$vuserid = $_SESSION['vuserid'];

	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$task = quote_smart($_POST['task']);

	/*     update vendor profile     */
	if($task == "update_vendor_profile")
	{
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$company_name = $_POST['company_name'];
		$category_id = $_POST['category_id'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$city = $_POST['city'];
		$short_desc = $_POST['short_desc'];
		$phonecode = $_POST['phonecode'];
		$vuserid_request = $_POST['vuserid'];

		if($vuserid != $vuserid_request)
		{
			echo json_encode("Access forbidden! Contact administrator");
			exit;
		}

		$sql_update = "UPDATE vendors SET firstname='$firstname',lastname='$lastname',company_name='$company_name',category_id='$category_id',country='$country',state='$state',city='$city',short_desc='$short_desc',phonecode='$phonecode',updated_by_user='vendor',updated_by='$vuserid',updated_on=now() WHERE vuserid='$vuserid'";

		if($link->exec($sql_update))
		{
			$sql_member_log = "INSERT INTO vendor_activity_logs(vuserid,task,activity,IP_Address,created_On) VALUES('$vuserid','update','profile details','$IP_Address',now())";
			$link->exec($sql_member_log);

			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Try after sometime");
			exit;
		}
	}

?>