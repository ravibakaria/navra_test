<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']=='' || $_SESSION['vendor_user']==null))
  	{
		header('Location: ../login.php');
		exit;
	}

	$vuserid = $_SESSION['vuserid'];
	$vuser_email = $_SESSION['vuser_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$task = quote_smart($_POST['task']);

	if($task=="vendor_Change_Password") 
	{
		$crr_pwd = $_POST['crr_pwd'];
		$new_pwd = $_POST['new_pwd'];
		$cnf_new_pwd = $_POST['cnf_new_pwd'];

		$crr_pwd_hash = md5(trim($_POST['crr_pwd']));

		$sql  = "SELECT * FROM `vendors` WHERE vuserid='$vuserid'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $userRow = $stmt->fetch();

        $db_pass = $userRow['password'];

        $MinimumUserPasswordLength = getMinimumUserPasswordLength();
        $MaximumUserPasswordLength = getMaximumUserPasswordLength();

        if($MinimumUserPasswordLength=='0' || $MinimumUserPasswordLength=='' || $MinimumUserPasswordLength==null)
		{
			$MinimumUserPasswordLength='8';
		}

		if($MaximumUserPasswordLength=='0' || $MaximumUserPasswordLength=='' || $MaximumUserPasswordLength==null)
		{
			$MaximumUserPasswordLength='40';
		}
		
        if($crr_pwd_hash == $db_pass)
        {
        	$haveuppercase = preg_match('/[A-Z]/', $new_pwd);
			$havenumeric = preg_match('/[0-9]/', $new_pwd);
			$havespecial = preg_match('/[!@#$%^&)*_(+=}{|:;,.<>}]/', $new_pwd);

			if (strlen($new_pwd) < $MinimumUserPasswordLength)
			{
				$errorMessage = "New Password must be of minimum $MinimumUserPasswordLength characters long.";
			}
			else if (strlen($new_pwd) > $MaximumUserPasswordLength)
			{
				$errorMessage = "New Password must be of maximum $MaximumUserPasswordLength characters long.";
			}
			else if (!$haveuppercase)
			{
				$errorMessage = 'New Password must have atleast one upper case character.';
			}
			else if (!$havenumeric)
			{
				$errorMessage = 'New Password must have atleast one digit.';
			}
			else if (!$havespecial)
			{
				$errorMessage = 'New Password must have atleast one of the special characters [!@#$%^&)*_(+=}{|:;,.<>}]';
			}
			else
			{
				$updated_pass = md5(trim($new_pwd));
				$sql_update = "UPDATE vendors SET password='$updated_pass',updated_by_user='vendor',updated_by='$vuserid',updated_on=now() WHERE vuserid='$vuserid'";

				if($link->exec($sql_update))
				{
					$errorMessage = "success";
				}
				else
				{
					$errorMessage = "Something went wrong. Try after some time.";
				}
			}
        }
        else
        {
        	$errorMessage = "Current password does not match.<br>Please enter correct password.";
        }

        echo json_encode($errorMessage);
        exit;
    }
?>