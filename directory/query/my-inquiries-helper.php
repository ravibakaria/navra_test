<?php
	if(!isset($_SESSION))
	{
	    ob_start();
	    session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['vendor_user']=='' || $_SESSION['vendor_user']==null))
  	{
    	header("Location: ../index.php");
    	exit;
  	}

  	$vuserid = $_SESSION['vuserid'];

	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$task = quote_smart($_POST['task']);

	/*     update vendor profile     */
	if($task == "fetch-inquiry-details")
	{
		$output = null;
		$id = $_POST['id'];
		
		$sql_update = "SELECT * FROM vendor_enquiries WHERE id='$id' AND vuserid='$vuserid'";
		$stmt = $link->prepare($sql_update);
		$stmt->execute();
		$count = $stmt->rowCount();
		$result = $stmt->fetch();

		if($count>0)
		{
			$id = $result['id'];
			$vuserid = $result['vuserid'];
			$fullname = $result['fullname'];
			$email = $result['email'];
			$phone = $result['phone'];
			$subject = $result['subject'];
			$message = $result['message'];
			$enquiry_on = $result['enquiry_on'];

			$enquiry_on = date('d-M-Y h:i A',strtotime($enquiry_on));

			$output = "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3 inquiry-info-row'>";
				$output .= "<b>Inquiry Id  </b>";
			$output .= "</div>";
			$output .= "<div class='col-xs-6 col-sm-6 col-md-9 col-lg-9 inquiry-info-row'>";
				$output .= $id;
			$output .= "</div>";

			$output .= "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3 inquiry-info-row'>";
				$output .= "<b>Full Name  </b>";
			$output .= "</div>";
			$output .= "<div class='col-xs-6 col-sm-6 col-md-9 col-lg-9 inquiry-info-row'>";
				$output .= $fullname;
			$output .= "</div>";

			$output .= "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3 inquiry-info-row'>";
				$output .= "<b>Email Id  </b>";
			$output .= "</div>";
			$output .= "<div class='col-xs-6 col-sm-6 col-md-9 col-lg-9 inquiry-info-row'>";
				$output .= $email;
			$output .= "</div>";

			$output .= "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3 inquiry-info-row'>";
				$output .= "<b>Mobile  </b>";
			$output .= "</div>";
			$output .= "<div class='col-xs-6 col-sm-6 col-md-9 col-lg-9 inquiry-info-row'>";
				$output .= $phone;
			$output .= "</div>";

			$output .= "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3 inquiry-info-row'>";
				$output .= "<b>Subject  </b>";
			$output .= "</div>";
			$output .= "<div class='col-xs-6 col-sm-6 col-md-9 col-lg-9 inquiry-info-row'>";
				$output .= $subject;
			$output .= "</div>";

			$output .= "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3 inquiry-info-row'>";
				$output .= "<b>Message  </b>";
			$output .= "</div>";
			$output .= "<div class='col-xs-6 col-sm-6 col-md-9 col-lg-9 inquiry-info-row'>";
				$output .= $message;
			$output .= "</div>";

			$output .= "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3 inquiry-info-row'>";
				$output .= "<b>Inquiry On  </b>";
			$output .= "</div>";
			$output .= "<div class='col-xs-6 col-sm-6 col-md-9 col-lg-9 inquiry-info-row'>";
				$output .= $enquiry_on;
			$output .= "</div>";

		}
		else
		{
			$output = "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 inquiry-info-row'>";
				$output .= "<b>No record found : </b>";
			$output .= "</div>";
		}

		echo json_encode($output);
		exit;
	}

?>