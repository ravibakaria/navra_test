<?php
    session_start();
    if(isset($_SESSION['logged_in']) && @$_SESSION['vendor_user']!='')
    {
        header('Location: dashboard.php');
        exit;
    }

    if(isset($_SESSION['logged_in']) && @$_SESSION['client_user']!='')
    {
        header('Location: ../users/');
        exit;
    }

    if(isset($_SESSION['logged_in']) && @$_SESSION['admin_user']!='')
    {
        header('Location: ../admin/');
        exit;
    }
    
    include("templates/header.php");
    include('../config/setup-values.php');   //get master setup values
    include('../config/email/email_style.php');   //get master setup values
    include('../config/email/email_templates.php');   //get master setup values
    include('../config/email/email_process.php');

    $today_datetime = date('Y-m-d H:i:s');

    $WebSiteBasePath = getWebsiteBasePath();

    $MinPassLength = getMinimumUserPasswordLength();
    $MaxPassLength = getMaximumUserPasswordLength();

    $terms_of_service_display = getTermsOfServiceSetting();
    $privacy_policy_display = getPrivacyPolicySetting();


    /**********   Form submit status   ***********/
    $recaptchaAllowed = getreCaptchaAllowed();
    if($recaptchaAllowed == "1")
    {
        $recaptchaSiteKey=getreCaptchaSiteKey();
        $recaptchaSecretKey=getreCaptchaSecretKey();
    }

    $errorMessage = null;
    $successMessage = null;

    if($recaptchaAllowed == "1")
    {
        $recaptchaSiteKey=getreCaptchaSiteKey();
        $recaptchaSecretKey=getreCaptchaSecretKey();

        if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
        {
            $secret = $recaptchaSecretKey;
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            
            if($responseData->success)
            {
                if ((isset($_POST['email']) && $_POST['email'] != '') && (isset($_POST['firstname']) && $_POST['firstname'] != '') && (isset($_POST['lastname']) && $_POST['lastname'] != '') && (isset($_POST['company_name']) && $_POST['company_name'] != '') && (isset($_POST['short_desc']) && $_POST['short_desc'] != '') && (isset($_POST['password']) && $_POST['password'] != ''))
                {
                    $firstname = quote_smart($_POST['firstname']);
                    $lastname = quote_smart($_POST['lastname']);
                    $company_name = quote_smart($_POST['company_name']);
                    $category_id = quote_smart($_POST['category_id']);
                    $email = quote_smart($_POST['email']);
                    $country = quote_smart($_POST['country']);
                    $state = quote_smart($_POST['state']);
                    $city = quote_smart($_POST['city']);
                    $phonecode = quote_smart($_POST['country_phone_code']);
                    $profile_pic = quote_smart($_POST['profile_pic']);
                    $short_desc = quote_smart($_POST['short_desc']);
                    $phone = quote_smart($_POST['phone']);
                    $password = quote_smart($_POST['password']);
                    $confirm_password = quote_smart($_POST['confirm_password']);
                    
                    $MinimumUserPasswordLength = getMinimumUserPasswordLength();
                    $MaximumUserPasswordLength = getMaximumUserPasswordLength();

                    if($MinimumUserPasswordLength=='0' || $MinimumUserPasswordLength=='' || $MinimumUserPasswordLength==null)
                    {
                        $MinimumUserPasswordLength='10';
                    }

                    if($MaximumUserPasswordLength=='0' || $MaximumUserPasswordLength=='' || $MaximumUserPasswordLength==null)
                    {
                        $MaximumUserPasswordLength='100';
                    }

                    $passwordx = md5(trim($_POST['password']));

                    $subject = null;
                    $message = null;
                    $mailto = null;
                    $mailtoname = null;
                    $sql_chk = "SELECT `email` FROM `vendors` WHERE email='$email' OR  phone='$phone'";
                    $sql_chk = $link->prepare($sql_chk);
                    $sql_chk->execute();
                    $count=$sql_chk->rowCount();
                    //echo "<script>alert(".$count.");</script>";
                    if($count == '0')
                    {
                        if($password == $confirm_password)
                        {
                            $haveuppercase = preg_match('/[A-Z]/', $password);
                            $havenumeric = preg_match('/[0-9]/', $password);
                            $havespecial = preg_match('/[!@#$%^&)*_(+=}{|:;,.<>}]/', $password);

                            if (!$haveuppercase)
                            {
                                $errorMessage = 'Password must have atleast one upper case character.';
                            }
                            else if (!$havenumeric)
                            {
                                $errorMessage = 'Password must have atleast one digit.';
                            }
                            else if (!$havespecial)
                            {
                                $errorMessage = 'Password must have atleast one of the special characters [!@#$%^&)*_(+=}{|:;,.<>}]';
                            }
                            else if (strlen($password) < $MinimumUserPasswordLength)
                            {
                                $errorMessage = "Password must be of minimum ".$MinimumUserPasswordLength." characters long.";
                            }
                            else if (strlen($password) > $MaximumUserPasswordLength)
                            {
                                $errorMessage = "Password must be of maximum ".$MaximumUserPasswordLength." characters long.";
                            }
                            else
                            {

                                $sql_insert = "INSERT INTO `vendors`(firstname, lastname, email, phonecode, phone, company_name, category_id, city, state, country, short_desc, password, created_by_user, created_by, created_on, status) VALUES ('$firstname','$lastname','$email','$phonecode', '$phone', '$company_name', '$category_id', '$city', '$state', '$country', '$short_desc', '$passwordx', 'vendor', '0', now(), '0')";
                                //$insert_query = $link->exec($sql_insert);

                                $passcode = encrypt_decrypt('encrypt', $email);
                                
                                $verification_link = "<a href='$WebSiteBasePath/directory/verify-email.php?passcode=$passcode'><button>Verify</button></a>";

                                $verification_text_link = "<a href='$WebSiteBasePath/directory/verify-email.php?passcode=$passcode'>$WebSiteBasePath/directory/verify-email.php?passcode=$passcode</a>";

                                $sitetitle = getWebsiteTitle();
                                $logo_array=array();
                                $logoURL = getLogoURL();
                                if($logoURL!='' || $logoURL!=null)
                                {
                                    $logoURL = explode('/',$logoURL);

                                    for($i=1;$i<count($logoURL);$i++)
                                    {
                                        $logo_array[] = $logoURL[$i];
                                    }

                                    $logo_path = implode('/',$logo_array);
                                }

                                if($logoURL!='' || $logoURL!=null)
                                {
                                    $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
                                }
                                else
                                {
                                    $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
                                }

                                if($link->exec($sql_insert))
                                {
                                    $SocialSharing = getSocialSharingLinks();   // social sharing links
                                    $primary_color = getPrimaryColor();
                                    $primary_font_color = getPrimaryFontColor();
        
                                    $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value
        
                                    $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$sitetitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value
        
                                    $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$sitetitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value
        
                                    $EmailVerificationMessage = str_replace(array('$first_name','$site_name','$verification_link','$verification_text_link','$signature'),array($firstname,$sitetitle,$verification_link,$verification_text_link,$GlobalEmailSignature),$EmailVerificationMessage);  //replace footer variables with value
        
                                    $subject = $EmailVerificationSubject;
                                    $message  = '<!DOCTYPE html>';
                                    $message .= '<html lang="en">
                                        <head>
                                        <meta charset="utf-8">
                                        <meta name="viewport" content="width=device-width">
                                        <title></title>
                                        <style type="text/css">'.$EmailCSS.'</style>
                                        </head>
                                        <body style="margin: 0; padding: 0;">';
                                    //echo $message;exit;
                                    $message .= $EmailGlobalHeader;
        
                                    $message .= $EmailVerificationMessage;
        
                                    $message .= $EmailGlobalFooter;
        
                                    $mailto = $email;
                                    $mailtoname = $firstname;
        
                                    $registserUserId = getVenderIdFromEmail($email);
                                    $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);
        
                                    /*************      Activity log      ***************/
                                    $sql_member_log = "INSERT INTO vendor_activity_logs(vuserid,task,activity,IP_Address,created_On) VALUES('$registserUserId','registred','to system','$IP_Address',now())";
                                    $link->exec($sql_member_log);
        
                                    /*************      Email log      ***************/
                                    $sql_member_email_log = "INSERT INTO vendor_email_logs(vuserid,task,activity,sent_On) VALUES('$registserUserId','new registration','email',now())";
        
                                    if($emailResponse == 'success')
                                    {
                                        $link->exec($sql_member_email_log);
                                        $successMessage = "success";
                                    }
                                    else
                                    {
                                        $errorMessage = "Error in sending verification link";
                                        //$errorMessage = $emailResponse;
                                    }
                                }
                                else
                                {
                                    $errorMessage = $sql_insert;
                                }
                            }
                        }
                        
                    }
                    else
                    {
                        $errorMessage = "Email id or Mobile number already exists. Please use another email id & mobile number.";
                    }
                }
            }
        }
    }    
    else
    {
        if ((isset($_POST['email']) && $_POST['email'] != '') && (isset($_POST['firstname']) && $_POST['firstname'] != '') && (isset($_POST['lastname']) && $_POST['lastname'] != '') && (isset($_POST['company_name']) && $_POST['company_name'] != '') && (isset($_POST['short_desc']) && $_POST['short_desc'] != '') && (isset($_POST['password']) && $_POST['password'] != ''))
        {
            $firstname = quote_smart($_POST['firstname']);
            $lastname = quote_smart($_POST['lastname']);
            $company_name = quote_smart($_POST['company_name']);
            $category_id = quote_smart($_POST['category_id']);
            $email = quote_smart($_POST['email']);
            $country = quote_smart($_POST['country']);
            $state = quote_smart($_POST['state']);
            $city = quote_smart($_POST['city']);
            $phonecode = quote_smart($_POST['country_phone_code']);
            $short_desc = quote_smart($_POST['short_desc']);
            $phone = quote_smart($_POST['phone']);
            $password = quote_smart($_POST['password']);
            $confirm_password = quote_smart($_POST['confirm_password']);
            
            $MinimumUserPasswordLength = getMinimumUserPasswordLength();
            $MaximumUserPasswordLength = getMaximumUserPasswordLength();

            if($MinimumUserPasswordLength=='0' || $MinimumUserPasswordLength=='' || $MinimumUserPasswordLength==null)
            {
                $MinimumUserPasswordLength='10';
            }

            if($MaximumUserPasswordLength=='0' || $MaximumUserPasswordLength=='' || $MaximumUserPasswordLength==null)
            {
                $MaximumUserPasswordLength='100';
            }

            $passwordx = md5(trim($_POST['password']));

            $subject = null;
            $message = null;
            $mailto = null;
            $mailtoname = null;
            $sql_chk = "SELECT `email` FROM `vendors` WHERE email='$email' OR  phone='$phone'";
            $sql_chk = $link->prepare($sql_chk);
            $sql_chk->execute();
            $count=$sql_chk->rowCount();
            //echo "<script>alert(".$count.");</script>";
            if($count == '0')
            {
                if($password == $confirm_password)
                {
                    $haveuppercase = preg_match('/[A-Z]/', $password);
                    $havenumeric = preg_match('/[0-9]/', $password);
                    $havespecial = preg_match('/[!@#$%^&)*_(+=}{|:;,.<>}]/', $password);

                    if (!$haveuppercase)
                    {
                        $errorMessage = 'Password must have atleast one upper case character.';
                    }
                    else if (!$havenumeric)
                    {
                        $errorMessage = 'Password must have atleast one digit.';
                    }
                    else if (!$havespecial)
                    {
                        $errorMessage = 'Password must have atleast one of the special characters [!@#$%^&)*_(+=}{|:;,.<>}]';
                    }
                    else if (strlen($password) < $MinimumUserPasswordLength)
                    {
                        $errorMessage = "Password must be of minimum ".$MinimumUserPasswordLength." characters long.";
                    }
                    else if (strlen($password) > $MaximumUserPasswordLength)
                    {
                        $errorMessage = "Password must be of maximum ".$MaximumUserPasswordLength." characters long.";
                    }
                    else
                    {
                        $sql_insert = "INSERT INTO `vendors`(firstname, lastname, email, phonecode, phone, company_name, category_id, city, state, country, short_desc, password, created_by_user, created_by, created_on, status) VALUES ('$firstname','$lastname','$email','$phonecode', '$phone', '$company_name', '$category_id', '$city', '$state', '$country', '$short_desc', '$passwordx', 'vendor', '0', now(), '0')";
                        //$insert_query = $link->exec($sql_insert);

                        $passcode = encrypt_decrypt('encrypt', $email);
                        
                        $verification_link = "<a href='$WebSiteBasePath/directory/verify-email.php?passcode=$passcode'><button>Verify</button></a>";

                        $verification_text_link = "<a href='$WebSiteBasePath/directory/verify-email.php?passcode=$passcode'>$WebSiteBasePath/directory/verify-email.php?passcode=$passcode</a>";

                        $sitetitle = getWebsiteTitle();
                        $logo_array=array();
                        $logoURL = getLogoURL();
                        if($logoURL!='' || $logoURL!=null)
                        {
                            $logoURL = explode('/',$logoURL);

                            for($i=1;$i<count($logoURL);$i++)
                            {
                                $logo_array[] = $logoURL[$i];
                            }

                            $logo_path = implode('/',$logo_array);
                        }

                        if($logoURL!='' || $logoURL!=null)
                        {
                            $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
                        }
                        else
                        {
                            $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
                        }

                        if($link->exec($sql_insert))
                        {
                            $SocialSharing = getSocialSharingLinks();   // social sharing links
                            $primary_color = getPrimaryColor();
                            $primary_font_color = getPrimaryFontColor();

                            $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

                            $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$sitetitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

                            $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$sitetitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

                            $EmailVerificationMessage = str_replace(array('$first_name','$site_name','$verification_link','$verification_text_link','$signature'),array($firstname,$sitetitle,$verification_link,$verification_text_link,$GlobalEmailSignature),$EmailVerificationMessage);  //replace footer variables with value

                            $subject = $EmailVerificationSubject;
                            $message  = '<!DOCTYPE html>';
                            $message .= '<html lang="en">
                                <head>
                                <meta charset="utf-8">
                                <meta name="viewport" content="width=device-width">
                                <title></title>
                                <style type="text/css">'.$EmailCSS.'</style>
                                </head>
                                <body style="margin: 0; padding: 0;">';
                            //echo $message;exit;
                            $message .= $EmailGlobalHeader;

                            $message .= $EmailVerificationMessage;

                            $message .= $EmailGlobalFooter;

                            $mailto = $email;
                            $mailtoname = $firstname;

                            $registserUserId = getVenderIdFromEmail($email);
                            $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

                            /*************      Activity log      ***************/
                            $sql_member_log = "INSERT INTO vendor_activity_logs(vuserid,task,activity,IP_Address,created_On) VALUES('$registserUserId','registred','to system','$IP_Address',now())";
                            $link->exec($sql_member_log);

                            /*************      Email log      ***************/
                            $sql_member_email_log = "INSERT INTO vendor_email_logs(vuserid,task,activity,sent_On) VALUES('$registserUserId','new registration','email',now())";

                            if($emailResponse == 'success')
                            {
                                $link->exec($sql_member_email_log);
                                $successMessage = "success";
                            }
                            else
                            {
                                $errorMessage = "Error in sending verification link";
                                //$errorMessage = $emailResponse;
                            }
                        }
                        else
                        {
                            $errorMessage = $sql_insert;
                        }
                    }
                }
                
            }
            else
            {
                $errorMessage = "Email id or Mobile number already exists. Please use another email id & mobile number.";
            }
        }
    }
?>

    <!-- meta info  -->

    <title>
        <?php
        $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
        $string = str_replace("-", " ", $a);
        echo $title = ucwords($string);
        ?>  -  <?php echo getWebsiteTitle(); ?>
    </title>

    <meta name="title" content="<?= $site_title.' - Registration'; ?>" />

    <meta name="description" content="To find Verified Profiles, Register Free! If you are Looking For Groom or Bride – we have a perfect match for you."/>

    <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

    <meta property="og:title" content="<?php echo $site_title.' - Registration'; ?>" />
    <meta property="og:url" content="<?= $site_url; ?>" />
    <meta property="og:description" content="<?php echo $site_tagline;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
    <meta property="twitter:title" content="<?= $site_title.' - Registration'; ?>" />
    <meta property="twitter:url" content="<?= $site_url; ?>" />
    <meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

    <script src="../js/jquery.js"></script>
    
    <script>
        function onSubmit(token)
        {
            document.getElementById("frmLogin").submit();
        }

        function validate(event) 
        {
            event.preventDefault();
            var firstname = $('.firstname').val();
            var lastname = $('.lastname').val();
            var company_name = $('.company_name').val();
            var category_id = $('.category_id').val();
            var short_desc = $('.short_desc').val();
            var country = $('.country').val();
            var state = $('.state').val();
            var city = $('.city').val();
            var country_phone_code = $('.country_phone_code').val();
            var email = $('.email').val();
            var phone = $('.phone').val();
            var password = $('.password').val();
            var confirm_password = $('.confirm_password').val();
            var agree_check = $('#agree_check').is(':checked');
            var MinPassLength = $('.MinPassLength').val();
            var MaxPassLength = $('.MaxPassLength').val();
            var recaptchaAllowed = $('.recaptchaAllowed').val();
            var empty_success=0;            

            if(MinPassLength=='0' || MinPassLength=='' || MinPassLength==null)
            {
                MinPassLength='10';
            }

            if(MaxPassLength=='0' || MaxPassLength=='' || MaxPassLength==null)
            {
                MaxPassLength='100';
            }

            /* First name Validation */
            if(firstname=='' || firstname==null)
            {
                $('.firstname').addClass('danger_error');
                empty_success=empty_success+1;
            }


            /* Last name Validation */
            if(lastname=='' || lastname==null)
            {
                $('.lastname').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Company Name validation */
            if(company_name=='' || company_name==null)
            {
                $('.company_name').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Category Id validation */
            if(category_id=='0')
            {
                $('.category_id').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Short description validation */
            if(short_desc=='' || short_desc==null)
            {
                $('.short_desc').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Country validation */
            if(country=='0' || country=='' || country==null)
            {
                $('.country').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* State validation */
            if(state=='0' || state=='' || state==null)
            {
                $('.state').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* city validation */
            if(city=='0' || city=='' || city==null)
            {
                $('.city').addClass('danger_error');
                empty_success=empty_success+1;
            }


            /* Email Validation */
            if(email=='' || email==null)
            {
                $('.email').addClass('danger_error');
                empty_success=empty_success+1;
            }

            function validateEmail($email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $email );
            }

            if(email!='' && !validateEmail(email)) 
            { 
                $('.email').focus();
                $('.email_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Enter valid Email.</div>");
                $('.email').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Phone number validation */
            if(phone=='' || phone==null)
            {
                $('.phone').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(phone!='' && phone.length<4)
            {
                $('.phone').focus();
                $('.phone_error').show();
                $('.phone_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Mobile number should be of minimum 4 digits!</div>");
                $('.phone').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Password validation */
            if(password=='' || password==null)
            {
                $('.password').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(password!='' && password.length < MinPassLength)
            {
                $('.password').focus();
                $('.password_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Password should be of minimum "+MinPassLength+" characters long!</div>");
                $('.password').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(password!='' && password.length > MaxPassLength)
            {
                $('.password').focus();
                $('.password_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Password length not exceed more than "+MaxPassLength+" !</div>");
                $('.password').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Confirm Password validation */
            if(confirm_password=='' || confirm_password==null)
            {
                $('.confirm_password').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(confirm_password!='' && confirm_password != password)
            {
                $('.confirm_password').focus();
                $('.confirm_password_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Password & Confirm password not same!</div>");
                $('.confirm_password').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Agreement check validation */
            if(agree_check==false)
            {
                $('.agree_check_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Emty: </strong> Please check the terms & condtions.</div>");
                $('#agree_check').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(empty_success>0)
            {
                $('.server_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong> Empty! </strong> Please enter all required field & then click on register button.</div>");
                return false;
            }

            //recaptcha validation 
            if(recaptchaAllowed=='1')
            {
                grecaptcha.execute();
            }
            else
            {
                document.getElementById("frmLogin").submit();
            }

            $('.loading_img').show();
            $('.server_data_status').html("");
            $('.error_status').html("");
            $('.agree_check_error').html("");
        }

        function onload() 
        {
            var element = document.getElementById('recaptcha-submit');
            element.onclick = validate;
        }
    </script>

<?php
  include("templates/styles.php"); 
  include("templates/menu.php"); 
?>

<!-- Google recaptcha -->
<?php
    if($recaptchaAllowed == "1")
    {
        echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
    }
?>

<section>
  <br/><br/><br/><br/><br/><br/>
</section>

<div class="row free-register-panel common-row-div">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="row">
                <section class="panel register-panel">
                    <header class="panel-heading register-panel-heading">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 centered-data">
                                    <h1>Vendor Registration!</h1>
                                    <h2>*All fields are manadatory</h2>
                            </div>
                        </div>
                    </header>
                    <form name="frmLogin" id="frmLogin" autocomplete="off" method="post"  action="<?php echo $_SERVER['PHP_SELF'];?>">
                        <div class="panel-body register-panel-body">
                            <input type="hidden" class="MinPassLength" value="<?php echo $MinPassLength;?>">
                            <input type="hidden" class="MaxPassLength" value="<?php echo $MaxPassLength;?>">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">First Name</label>
                                        <input type="text" name="firstname" id="firstname" class="form-control firstname" placeholder="Ex: John" value="<?php echo @$firstname;?>">
                                    </div>
                                    <div class="firstname_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" name="lastname" id="lastname" class="form-control lastname" placeholder="Ex: Smith" value="<?php echo @$lastname;?>">
                                    </div>
                                    <div class="lastname_error"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">Company Name</label>
                                        <input type="text" name="company_name" id="company_name" class="form-control company_name" placeholder="Ex: John" value="<?php echo @$company_name;?>">
                                    </div>
                                    <div class="company_name_error"></div>

                                    <div class="form-group">
                                        <label class="control-label">Category</label>
                                        <select name="category_id" id="category_id" class="form-control category_id">
                                            <option value="0" >--- Select Category ---</option>
                                            <?php
                                                $sql = "SELECT * FROM membercategory ORDER BY name ASC";
                                                $stmt = $link->prepare($sql);
                                                $stmt->execute();
                                                $result = $stmt->fetchAll();
                                                $count = $stmt->rowCount();

                                                if($count>0)
                                                {
                                                    foreach ($result as $row) 
                                                    {
                                                        $id = $row['id'];
                                                        $name = $row['name'];

                                                        $id = $row['id'];
                                                        $name = $row['name'];

                                                        if(@$category_id == $id)
                                                        {
                                                            echo "<option value='".$id."' selected>".$name."</option>";
                                                        }
                                                        else
                                                        {
                                                            echo "<option value='".$id."'>".$name."</option>";
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="category_id_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">Short Description About Your Services</label>
                                        <textarea class="form-control short_desc" name="short_desc" rows="5"><?php echo @$short_desc;?></textarea>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Country</label>
                                        <select name="country" class="form-control country">
                                            <option value="0">Select Country</option>
                                            <?php
                                                $query  = "SELECT * FROM `countries` ORDER BY `name` ASC";
                                                $stmt   = $link->prepare($query);
                                                $stmt->execute();
                                                $result = $stmt->fetchAll();
                                                foreach( $result as $row )
                                                {
                                                    $country_name =  $row['name'];
                                                    $country_id = $row['id']; 

                                                    if(isset($country))
                                                    {
                                                        if($country==$country_id)
                                                        {
                                                            echo "<option value=".$country_id." selected>".$country_name."</option>";
                                                        }
                                                        else
                                                        {
                                                            echo "<option value=".$country_id.">".$country_name."</option>";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        echo "<option value=".$country_id.">".$country_name."</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="country_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">State</label>
                                        <select name="state" class="form-control state">
                                            <?php
                                                if(isset($state))
                                                {
                                                    $sql = "SELECT * FROM states WHERE country_id='$country'";
                                                    $stmt = $link->prepare($sql);
                                                    $stmt->execute();
                                                    $result = $stmt->fetchAll();

                                                    foreach ($result as $row) 
                                                    {
                                                        $state_id = $row['id'];
                                                        $state_name = $row['name'];
                                                        if($state==$state_id)
                                                        {
                                                            echo "<option value='".$state_id."' selected>".$state_name."</option>";
                                                        }
                                                        else
                                                        {
                                                            echo "<option value='".$state_id."'>".$state_name."</option>";
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="state_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <select name="city" class="form-control city">
                                            <?php
                                                if(isset($city))
                                                {
                                                    $sql = "SELECT * FROM cities WHERE state_id='$state'";
                                                    $stmt = $link->prepare($sql);
                                                    $stmt->execute();
                                                    $result = $stmt->fetchAll();

                                                    foreach ($result as $row) 
                                                    {
                                                        $city_id = $row['id'];
                                                        $city_name = $row['name'];
                                                        if($city==$city_id)
                                                        {
                                                            echo "<option value='".$city_id."' selected>".$city_name."</option>";
                                                        }
                                                        else
                                                        {
                                                            echo "<option value='".$city_id."'>".$city_name."</option>";
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="city_error"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">Email Id</label>
                                        <input type="text" name="email" id="email" class="form-control email" placeholder="Ex: John@gmail.com" value="<?php echo @$email;?>">
                                    </div>
                                    <div class="email_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">Mobile Number</label>
                                        <div class="input-group">
                                            <span class="input-group-addon country_code">
                                                <?php
                                                    echo @$phonecode;
                                                ?>
                                            </span>
                                            <input type="text" name="phone" class="form-control phone" placeholder="Mobile Number"  value="<?php echo @$phone;?>" maxlength="20">
                                        </div>
                                        <input type="hidden"  name="country_phone_code" class="country_phone_code"  value="<?php echo @$phonecode;?>"/>
                                        <div class="phone_error"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <p class="password-notification">
                                            <b>Instructions to set password:</b><br/>
                                            Password should contain atleast one uppercase character.<br/>
                                            Password should contain atleast one digit.<br/>
                                            Password should contain atleast one special character from !@#$%^*()_=+{}|;:,&lt;.><br/>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label">Password</label>
                                                <input type="password" name="password" class="form-control password" id="inputPassword" placeholder="Password" value="<?php echo @$password;?>">
                                            </div>
                                            <div class="password_error"></div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label">Confirm Password</label>
                                                <input type="password" class="form-control confirm_password" name="confirm_password" id="inputPasswordConfirm" placeholder="Confirm Password" value="<?php echo @$password;?>">
                                            </div>
                                            <div class="confirm_password_error"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <span class="button-checkbox">
                                            <input type="checkbox" class="form-inline agree_check" name="agree_check" id="agree_check" <?php if(isset($agree_check) && $agree_check=='1') { echo 'checked'; } ?>> &nbsp;&nbsp;<label for="agree_check" class="control-label">I agree</label>
                                        </span>
                                
                                        to the 

                                        <?php
                                            if($terms_of_service_display=='1')
                                            {
                                                echo "<a href='$websiteBasePath/terms-of-service.html' target='_blank'>Terms Of Services</a>";
                                            }
                                            else
                                            {
                                                echo "Terms Of Services";
                                            }

                                            echo " & ";

                                            if($privacy_policy_display=='1')
                                            {
                                                echo "<a href='$websiteBasePath/privacy-policy.html' target='_blank'>Privacy Policy.</a>";
                                            }
                                            else
                                            {
                                                echo "Privacy Policy.";
                                            }
                                        ?>
                                    </div>
                                    <div class="agree_check_error"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php
                                    if ($errorMessage != '' || $errorMessage != null)
                                    {
                                        echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
                                        echo "<strong>Error!</strong> $errorMessage";
                                        echo "</div>";
                                    }

                                    if ($successMessage != '' || $successMessage != null)
                                    {
                                        echo "<div class='alert alert-success col-sm-12 success_status' style='padding: 10px; margin-bottom: 10px;'>";
                                        echo "<strong><i class='fa fa-check'></i> Success! </strong>Registration completed successfully. Please check Your email inbox/spam folder for verification email.";
                                        echo "</div>";
                                        
                                        echo "<script>
                                                $('.success_status').fadeTo(2000, 500).slideUp(500, function(){
                                                $('.success_status').slideUp(200);
                                                window.location.assign('login.php');
                                            });
                                        </script>";
                                    }
                                ?>
                            </div>

                            <input type="hidden" class="recaptchaAllowed" value="<?php echo $recaptchaAllowed;?>">

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 centered-data">
                                            <img src="../images/loader/loader.gif" class='img-responsive loading_img centered-loading-image' id='loading_img' alt='loader' style='width:60px; height:60px; display:none;'/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 server_data_status">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer register-panel-footer centered-data">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <input type="submit" name="login_btn"  class="btn btn-primary btn_register website-button" id="recaptcha-submit" value="REGISTER">
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <a href="login.php" class="btn website-button">LOGIN</a>
                                </div>
                            </div>
                            
                        </footer>
                        <?php
                            if($recaptchaAllowed == "1")
                            {
                                echo "<div class='form-group'>
                                    <div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
                                </div>";
                            }
                        ?>
                        
                        <script>onload();</script>

                    </form>
                </section>
            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 free-registration-ad">
            <div class='skyscrapper-ad'>
                <?php
                    $display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
                    if($display_skyscrapper_ad=='1')
                    {
                        echo $skyscrapper_ad = getRandomSkyScrapperAdData();
                    }
                ?>
            </div>
        </div>
    </div>
</div>

</section>
<?php
    include("templates/footer.php");
?>

<?php
    $sql_AddThisScript = "SELECT social_sharing_display,social_sharing_data FROM `social_sharing`";
    $stmt= $link->prepare($sql_AddThisScript); 
    $stmt->execute();
    $count=$stmt->rowCount();
    $result = $stmt->fetch();
    $social_sharing_display = $result['social_sharing_display'];
    $social_sharing_data = $result['social_sharing_data'];

    if($social_sharing_display=='1')
    {
        if($social_sharing_data!='' || $social_sharing_data!=null)
        {
            $social_sharing_data = file_get_contents($websiteBasePath.'/'.$social_sharing_data);
            echo "$social_sharing_data";
        }
    }
?>

<script>
    $(document).ready(function(){

        /* empty error message  */
        $('.firstname, .lastname, .company_name, .category_id, .email, .phone, .password, .confirm_password, #agree_check, .country, .state, .city').keypress(function(){
            $('.firstname_error, .lastname_error, .company_name_error, .category_id_error, .email_error, .phone_error, .password_error, .confirm_password_error, .agree_check_error, .country_error, .state_error, .city_error').html("");
        });

        /* empty error message  */
        $('.firstname, .lastname, .company_name, .category_id, .email, .phone, .password, .confirm_password, #agree_check, .country, .state, .city').click(function(){
            $('.firstname_error, .lastname_error, .company_name_error, .category_id_error, .email_error, .phone_error, .password_error, .confirm_password_error, .agree_check_error, .country_error, .state_error, .city_error').html("");
        });

        /* empty danger class message  */
        $('.firstname').click(function(){
            $('.firstname').removeClass("danger_error");
        });

        $('.lastname').click(function(){
            $('.lastname').removeClass("danger_error");
        });

        $('.company_name').click(function(){
            $('.company_name').removeClass("danger_error");
        });

        $('.category_id').click(function(){
            $('.category_id').removeClass("danger_error");
        });

        $('.email').click(function(){
            $('.email').removeClass("danger_error");
        });

        $('.phone').click(function(){
            $('.phone').removeClass("danger_error");
        });

        $('.password').click(function(){
            $('.password').removeClass("danger_error");
        });

        $('.confirm_password').click(function(){
            $('.confirm_password').removeClass("danger_error");
        });

        $('#agree_check').click(function(){
            $('#agree_check').removeClass("danger_error");
        });

        $('.country').click(function(){
            $('.country').removeClass("danger_error");
        });

        $('.state').click(function(){
            $('.state').removeClass("danger_error");
        });

        $('.city').click(function(){
            $('.city').removeClass("danger_error");
        });

        $('.short_desc').click(function(){
            $('.short_desc').removeClass("danger_error");
        });

        /*   Prevent entering charaters in mobile & phone number   */
        $(".phone").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.phone_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Albhabets not allowed. Enter Digits only.</div>").show().fadeOut(3000);
                return false;
            }
        })

        $('#profile_pic').on('change', function(){

            var file = $(this).val();
            var ext = file.split('.').pop();
            var img_array = "<?php echo getAllowedFileAttachmentTypesString(); ?>";

            var i = img_array.indexOf(ext);
             
            if(i <= -1) 
            {
                $('.profile_pic_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 15px;'><span class='fa fa-times'></span><strong> Error! </strong> Invalid image type.</div>");
                return false;
            }
            
        });

        $('.country').change(function(){         //Fetch states
            var country_id = $(this).val();
            var task = "Fetch_state_data";  

            $.ajax({
                type:'post',
                data:'country_id='+country_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    var result = $.parseJSON(res);
                    $('.state').html(result[0]);
                    $('.country_code').html("+"+result[1]);
                    $('.country_phone_code').val(result[1]);
                }
            });         
        });

        $('.state').change(function(){         //Fetch cities
            var state_id = $(this).val();
            var task = "Fetch_city_data";  
            $.ajax({
                type:'post',
                data:'state_id='+state_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    $('.city').html(res);
                }
            });         
        });
    });
</script>

</body>
</html>