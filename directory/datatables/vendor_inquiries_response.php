<?php
	session_start();
	require_once '../../config/config.php'; 
	include('../../config/dbconnect.php');    //database connection
	include('../../config/functions.php');   //strip query string

	if(!isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']=='' || $_SESSION['vendor_user']==null))
	{
		header('Location: ../login.php');
		exit;
	}

	$vuserid = $_SESSION['vuserid'];

	// initilize all variable
	$params = $columns = $totalRecords = $data = $i = array();

	$params = $_REQUEST;
	//print_r($params);
	//define index of column
	$columns = array( 
		0 =>'id',
		1 =>'fullname',
		2 =>'email',
		3 =>'phone',
		4 =>'subject',
		5 =>'enquiry_on'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value if exist
	if( !empty($params['search']['value']) ) {   
		$where .=" AND ( id LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR fullname LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR email LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR phone LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR subject LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR enquiry_on LIKE '%".quote_smart($params['search']['value'])."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT id,fullname,email,phone,subject,enquiry_on FROM vendor_enquiries WHERE  vuserid='$vuserid' ";

	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	$sqlRec .=  " ORDER BY ". $columns[quote_smart($params['order'][0]['column'])]."   ".quote_smart($params['order'][0]['dir'])."  LIMIT ".quote_smart($params['start'])." ,".quote_smart($params['length'])." ";

 	
	$stmt1   = $link->prepare($sqlTot);
    $stmt1->execute();
    $totalRecords = $stmt1->rowCount();

	$stmt2   = $link->prepare($sqlRec);
    $stmt2->execute();
    $total_rows = $stmt2->rowCount();

    $result = $stmt2->fetchAll();

    $i = 1;

    foreach( $result as $row )
    {
    	$id = $row['0'];

    	$row['0'] = "<a data-toggle='modal' href='#view_inquiry_$id' style='color:black;text-decoration:none;' value='$id' class='view-inquiry'>".$i."</a>";
    	$row['1'] = "<a data-toggle='modal' href='#view_inquiry_$id' style='color:black;text-decoration:none;' value='$id' class='view-inquiry'>".$row['1']."</a>";
    	$row['2'] = "<a data-toggle='modal' href='#view_inquiry_$id' style='color:black;text-decoration:none;' value='$id' class='view-inquiry'>".$row['2']."</a>";
    	$row['3'] = "<a data-toggle='modal' href='#view_inquiry_$id' style='color:black;text-decoration:none;' value='$id' class='view-inquiry'>".$row['3']."</a>";
        $row['4'] = "<a data-toggle='modal' href='#view_inquiry_$id' style='color:black;text-decoration:none;' value='$id' class='view-inquiry'>".$row['4']."</a>";
        $row['5'] = "<a data-toggle='modal' href='#view_inquiry_$id' style='color:black;text-decoration:none;' value='$id' class='view-inquiry'>".$row['5']."</a>";
        $row['6'] = "<a data-toggle='modal' href='#view_inquiry_$id' style='color:black;text-decoration:none;' value='$id' class='view-inquiry'>View</a>";

        $data[] = $row;

        $i++;
    }

    $json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>