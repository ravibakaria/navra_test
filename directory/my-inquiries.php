<?php
	session_start();
  	if(!isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']=='' || $_SESSION['vendor_user']==null))
  	{
		header('Location: login.php');
		exit;
	}
	include("templates/header.php"); 
?>

<!-- meta info  -->

<title>
  <?php
  $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
  $string = str_replace("-", " ", $a);
  echo $title = ucwords($string);
  ?> -  <?php echo getWebsiteTitle(); ?>
</title>

<meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

<meta name="description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!"/>

<meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, vendor information, vendor lobin, vendor registration" />

<meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
<meta property="og:url" content="<?= $site_url; ?>" />
<meta property="og:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
<meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
<meta property="twitter:url" content="<?= $site_url; ?>" />
<meta property="twitter:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
    include("templates/styles.php"); 
    include("templates/menu.php"); 
?>

<div class="col-md-12">
  <br><br><br><br><br><br>
</div>


<div class="vendor-data">
	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    		<div class="row profile-row">
	    			<div class="text-left col-xs-12 col-sm-12 col-md-6 col-lg-6">
	    				<h1>My Inquiries</h1>
	    			</div>
	    			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
	                    
	    			</div>
	    		</div>

	    		<br><br>
		    	<div class="row">
		    		<table id='datatable-info' class='table-hover table-striped table-bordered vendor_inquiries_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Full Name</th>
                                <th>Email Id</th>
                                <th>Phone</th>
                                <th>Subject</th>
                                <th>Inquiry On</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
		    	</div>

		    	<!-- Modal -->
	            <div class="modal fade" id="View-inquiry-details" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <center><h4 class="modal-title" id="exampleModalLongTitle">Inquiry Details</h4></center>
	                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                            </button>
	                        </div>
	                        <div class="modal-body">
	                            <div class="row input-row">
	                            	<div class='div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 " inquiry_details_data'></div>
	                            </div>
	                        </div>
	                        <div class="modal-footer">
	                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                        </div>
	                    </div>
	                </div>
	            </div>

		    </div>
		</div>
	</div>
</div>

</section>
<?php
  	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		var dataTable_vendor_inquiries_list = $('.vendor_inquiries_list').DataTable({      //inquiry datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            "ajax":{
                url :"datatables/vendor_inquiries_response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".vendor_inquiries_list_processing").css("display","none");
                }
            }
        });

        $('.vendor_inquiries_list tbody').on('click', '.view-inquiry', function(){
        	$('.inquiry_details_data').html("");
        	var id = $(this).attr('value');
        	var task = "fetch-inquiry-details";
        	$('#View-inquiry-details').modal('show');

        	var data = {
        		id : id,
        		task : task
        	};

        	$.ajax({
        		type:'post',
        		dataType:'json',
        		data:data,
        		url:'query/my-inquiries-helper.php',
        		success:function(res)
        		{
        			$('.inquiry_details_data').html(res);
        			return false;
        		}
        	});
        });
	});
</script>