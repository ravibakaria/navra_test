<?php
	session_start();
	if(isset($_SESSION['logged_in']) && $_SESSION['vendor_user']!='')
	{
		header('Location: dashboard.php');
		exit;
	}
	include("templates/header.php");
?>

	<!-- meta info  -->

  	<title>
	  	<?php
	    $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
	    $string = str_replace("-", " ", $a);
	    echo $title = ucwords($string);
	    ?>  -  <?php echo getWebsiteTitle(); ?>
	</title>

<?php
	include("templates/styles.php"); 
  	include("templates/menu.php");
	include('../config/setup-values.php');   //get master setup values
    include('../config/email/email_style.php');   //get master setup values
    include('../config/email/email_templates.php');   //get master setup values
    include('../config/email/email_process.php');
?>

<br/><br/><br/><br/>


<?php
	//echo $SMTPPort;
	$successMessage = $errorMessage = null;
	$email = quote_smart($_GET['email']);
	$sql_chk = $link->prepare("SELECT * FROM `vendors` WHERE email='$email'"); 
    $sql_chk->execute();
    $count=$sql_chk->rowCount();

    if($count>0)
    {
    	$result1 = $sql_chk->fetch();
        
        $vuserid = $result1['vuserid'];
    	$firstname = $result1['firstname'];

    	$passcode = encrypt_decrypt('encrypt', $email);

    	$sitetitle = getWebsiteTitle();
		$logo_array=array();
		$logoURL = getLogoURL();
		if($logoURL!='' || $logoURL!=null)
		{
			$logoURL = explode('/',$logoURL);

			for($i=1;$i<count($logoURL);$i++)
			{
				$logo_array[] = $logoURL[$i];
			}

			$logo_path = implode('/',$logo_array);
		}

		if($logoURL!='' || $logoURL!=null)
        {
            $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
        }
        else
        {
            $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
        }

        $verification_link = "<a href='$WebSiteBasePath/directory/verify-email.php?passcode=$passcode'><button>Verify</button></a>";

    	$verification_text_link = "<a href='$WebSiteBasePath/directory/verify-email.php?passcode=$passcode'>$WebSiteBasePath/directory/verify-email.php?passcode=$passcode</a>";

        $SocialSharing = getSocialSharingLinks();   // social sharing links
		$primary_color = getPrimaryColor();
		$primary_font_color = getPrimaryFontColor();

		$EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

		$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

		$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

		$EmailVerificationMessage = str_replace(array('$first_name','$site_name','$verification_link','$verification_text_link','$signature'),array($firstname,$WebSiteTitle,$verification_link,$verification_text_link,$GlobalEmailSignature),$EmailVerificationMessage);  //replace footer variables with value

		$subject = $EmailVerificationSubject;
		$message  = '<!DOCTYPE html>';
		$message .= '<html lang="en">
			<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width">
			<title></title>
			<style type="text/css">'.$EmailCSS.'</style>
			</head>
			<body style="margin: 0; padding: 0;">';
		//echo $message;exit;
		$message .= $EmailGlobalHeader;

		$message .= $EmailVerificationMessage;

		$message .= $EmailGlobalFooter;

		$mailto = $email;
		$mailtoname = $firstname;
		
		$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

		if($emailResponse == 'success')
		{
		    $sql_member_email_log = "INSERT INTO vendor_email_logs(vuserid,task,activity,sent_On) VALUES('$vuserid','verification','email',now())";
		    $link->exec($sql_member_email_log);
			$successMessage = "success";
		}
		else
		{
			$errorMessage = "Error in sending verification link";
			//$errorMessage = $emailResponse;
		}
    }
	else
	{
		$errorMessage = "Invalid data";
	}
?>

<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<section class="panel">
				<header class="panel-heading">
					<center><h1 class="panel-title">Verify your email id.</h1></center>
				</header>
				<div class="panel-body">
					
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<br/><br/><br/>
							<?php
								if(($successMessage!='' || $successMessage!=null) || ($errorMessage!='' || $errorMessage!=null))
								{
									echo "<script>$('.loading_img').hide();</script>";
								}

								if($successMessage!='' || $successMessage!=null)
								{
									echo "<div class='alert alert-success col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
									echo "<center>";
									echo "<strong><span class='fa fa-check'></span> Success!</strong> Verification link sent successfully at ".$email."<br/>";
									echo "Please check your email inbox/spam folder for verification link.<br/>";
									echo "Click on verification link to complete verification process.";
									echo "</center>";
									echo "</div>";
								}
								else
								if($errorMessage!='' || $errorMessage!=null)
								{
									echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'><span class='fa fa-exclamation-triangle'>";
									echo "<center>";
									echo "<strong>Error!</strong>".$errorMessage;
									echo "</center>";
									echo "</div>";
								}
							?>
						</div>
						
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<?php
	include("templates/footer.php");
?>