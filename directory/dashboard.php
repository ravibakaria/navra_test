<?php
	session_start();
  	if(!isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']=='' || $_SESSION['vendor_user']==null))
  	{
		header('Location: login.php');
		exit;
	}
	include("templates/header.php"); 

	$profile_pic = getVendorProfilePic($vuserid);
	$vendor_name = getVendorFirstName($vuserid).' '.getVendorLastName($vuserid);
	$vendor_email = getVendorEmail($vuserid);

	if($profile_pic!='' || $profile_pic!=null)
	{
		$profile_pic_path = 'uploads/'.$vuserid.'/profile/'.$profile_pic;
	}

?>

<title>
	<?php
	$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
	$string = str_replace("-", " ", $a);
	echo $title = ucwords($string);
	?> -  <?php echo getWebsiteTitle(); ?>
</title>

<meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

<meta name="description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!"/>

<meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, vendor information, vendor lobin, vendor registration" />

<meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
<meta property="og:url" content="<?= $site_url; ?>" />
<meta property="og:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
<meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
<meta property="twitter:url" content="<?= $site_url; ?>" />
<meta property="twitter:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
    include("templates/styles.php"); 
    include("templates/menu.php"); 
?>

<div class="col-md-12">
  	<br><br><br><br><br><br><br><br>
</div>

<div class="row">
	<div class="vendor-data">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
			<div class="panel ">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-bookmark"></span> Welcome, <?php echo $vendor_name;?>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                        	<div class="card vendor-home-cards">
            					<div class="card-body">
            						<h5 class="card-title"></h5>
            						<br>
            						<?php
            							if($profile_pic!='' || $profile_pic!=null)
            							{
            								echo "<b style='color:green;'><i class='fa fa-check'></i> Profile picture uploaded.</b>";
            								echo "<center>
            										<a href='my-profile-picture.php' style='text-decoration:none;'>
            											<img src='$profile_pic_path' class='img img-responsive' style='width:160px;height:160px'>
            										</a>
            									</center>";
            								echo "<a href='my-profile-picture.php'><b><i class='fa fa-upload'></i>Update</b></a>";
            							}
            							else
            							{
            								echo "<b style='color:red;'><i class='fa fa-times'></i> Profile picture not yet uploaded.</b><br>";
            								echo "<a href='my-profile-picture.php' style='text-decoration:none;'><b><i class='fa fa-upload'></i>Upload</b></a>";
            							}
            						?>
            					</div>
            				</div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                        	<?php 
                        		$sql_work = "SELECT * FROM vendor_gallery WHERE vuserid='$vuserid' AND status <>'2'";
                        		$stmt_work = $link->prepare($sql_work);
                        		$stmt_work->execute();
                        		$count_work = $stmt_work->rowCount();

                        	?>
                        	<div class="card vendor-home-cards">
            					<div class="card-body">
            						<h5 class="card-title"></h5>
            						<br>
            						<?php
            							if($count_work>0)
            							{
            								$result = $stmt_work->fetchAll();
            								echo "<b style='color:green;'><i class='fa fa-check'></i> Work gallery uploaded.</b><br><br>";

            								echo "<div class='mg-files' data-sort-destination data-sort-id='media-gallery'>";
    										foreach ($result as $row) 
    										{
    											$photo = $row['photo'];
    											echo "<div class='isotope-item document col-xs-4 col-lg-4'>";
    												echo "<div class='popup-gallery'>";
    													echo "<a class='thumb-image' href='uploads/$vuserid/photo/$photo'  title='$photo'>";
														echo "<img src='uploads/$vuserid/photo/$photo' style='width:60px;height:60px;'>";
														echo "</a>";
													echo "</div>";
												echo "</div>";
    										}
    										echo "</div>";
    										echo "<br><br><br><br><br><br>";
            								echo "<a href='my-work.php' style='text-decoration:none;'><b><i class='fa fa-upload'></i>Update</b></a>";

            							}
            							else
            							{
            								echo "<b style='color:red;'><i class='fa fa-times'></i> You nor uploaded your work gallery till now.</b><br>";
            								echo "<a href='my-work.php' style='text-decoration:none;'><b><i class='fa fa-upload'></i>Upload</b></a>";
            							}
            						?>
            					</div>
            				</div>
                        </div>
                    </div>
                </div>
            </div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
			<div class="panel ">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-bookmark"></span> Recent Inquiries 
                    </h3>
                </div>
                <div class="panel-body">
                	<br>
                    <div class="row">
                    	<table id='datatable-info' class='table-hover table-striped table-bordered vendor_inquiries_list' style="width:100%">
	                        <thead>
	                            <tr>
	                                <th>Sr. No.</th>
	                                <th>Full Name</th>
	                                <th>Email Id</th>
	                                <th>Phone</th>
	                                <th>Subject</th>
	                                <th>Inquiry On</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	<?php
	                        		$i = null;
	                        		$sql_inquiries = "SELECT fullname,email,phone,subject,enquiry_on FROM vendor_enquiries WHERE  vuserid='$vuserid' ORDER BY enquiry_on DESC LIMIT 5";
	                        		$stmt_inquiries = $link->prepare($sql_inquiries);
	                        		$stmt_inquiries->execute();
	                        		$count_inquiries = $stmt_inquiries->rowCount();
	                        		$result_inquiries = $stmt_inquiries->fetchAll();
	                        		if($count_inquiries>0)
	                        		{
	                        			$i=1;
	                        			foreach ($result_inquiries as $row_inquiries) 
	                        			{
	                        				$fullname = $row_inquiries['fullname'];
	                        				$email = $row_inquiries['email'];
	                        				$phone = $row_inquiries['phone'];
	                        				$subject = $row_inquiries['subject'];
	                        				$enquiry_on = $row_inquiries['enquiry_on'];

	                        				$enquiry_on = date('d-m-Y',strtotime($enquiry_on));

	                        				echo "<tr>";
	                        					echo "<td>".$i."</td>";
	                        					echo "<td>".$fullname."</td>";
	                        					echo "<td>".$email."</td>";
	                        					echo "<td>".$phone."</td>";
	                        					echo "<td>".$subject."</td>";
	                        					echo "<td>".$enquiry_on."</td>";
	                        				echo "</tr>";

	                        				$i = $i+1;
	                        			}
	                        			
	                        		}
	                        	?>
	                        </tbody>
	                    </table>

	                    <p align="right"><a href="my-inquiries.php" style="text-decoration:none;">[View All]</p>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>

</section>
<?php
  include("templates/footer.php");
?>

</body>
</html>