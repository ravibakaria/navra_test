<?php
	$vuserid = $_SESSION['vuserid'];
?>
<!-- start: header -->
<header class="header">
	
	<div class="logo-container">
		<a href="../" class="navbar-brand logo">
			<?php 
				$logo = getLogoURL();
				$sitetitle = getWebsiteTitle();
				$siteBasePath = getWebsiteBasePath();
				if($logo!='' || $logo!=null)
				{
					echo "<img src='$logo' class='img-responsive logo-img' alt='$sitetitle'/>";
				}
				else
				{
					echo "<img src='$siteBasePath/images/logo/default-logo.jpg' class='img-responsive logo-img' />";
				}
			?>
		</a>
	</div>
	
	<?php
		if(isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']!='' || $_SESSION['vendor_user']!=null))
		{						
	?>	
		
		<!-- start: search & user box -->
			<div class="header-right text-right" style="width:50%">		

				<span class="separator"></span>
				
				<div id="userbox" class="userbox">
					<a href="my-profile.php" data-toggle="dropdown">
						<figure class="profile-picture">
							<?php
								$profile_pic = getVendorProfilePic($vuserid);
								$vendor_name = getVendorFirstName($vuserid).' '.getVendorLastName($vuserid);
								$vendor_email = getVendorEmail($vuserid);

								$profile_pic_path = 'uploads/'.$vuserid.'/profile/'.$profile_pic;
								
								if($profile_pic!='' || $profile_pic!=null)
								{
									echo "<img src='$profile_pic_path' alt='$vendor_name' class='img-circle' data-lock-picture='$profile_pic_path' alt='$vendor_name'/>";
								}
								else
								{
							?>
									<img src="<?php echo $websiteBasePath; ?>/images/no_profile_pic.png" alt="<?php echo $vendor_name?>" class="img-circle" data-lock-picture="<?php echo $websiteBasePath; ?>/images/no_profile_pic.png" />
							<?php	
								}
							?>
						</figure>
						<div class="profile-info text-right" data-lock-name="<?php echo $vendor_name?>" data-lock-email="<?php echo $vendor_email?>">
							<span class="name"><?php echo getVendorFirstName($vuserid);?> <i class="fa custom-caret"></i></span>
						</div>
		
						
					</a>
		
					<div class="dropdown-menu home_profile_menu">
						<ul class="list-unstyled">
							<li class="divider"></li>
							<li>
								<a role="menuitem" tabindex="-1" href="change-password.php"><i class="fa fa-key"></i> Change Password</a>
							</li>
							<li>
								<a role="menuitem" tabindex="-1" href="logout.php"><i class="fa fa-power-off"></i> Logout</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		<!-- end: search & user box -->

		<!--    Secondary Menu Start   -->
			<div class="navbar navbar-default secondary-navigation" role="navigation">
			    <div class="row">
			        <div class="navbar-header">
			            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			                <span class="sr-only">Toggle navigation</span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			            </button>
			        </div>
			        <div class="collapse navbar-collapse">
			            <ul class="nav navbar-nav secondary-navigation-ul">
			                <li class="<?php if($current_file=='dashboard.php') { echo 'nav-active'; } ?> ">
								<a href="dashboard.php">
									<i class="fa fa-home" aria-hidden="true"></i>
									<span>Dashboard</span>
								</a>
							</li>
							
							<li>
			                    <a href="#" class="dropdown-toggle <?php if($current_file=='profile.php' || $current_file=='my-profile.php' || $current_file=='my-profile-picture.php' || $current_file=='my-gallery.php') { echo 'nav-active'; } ?>" data-toggle="dropdown">
			                    	<i class="fa fa-users" aria-hidden="true"></i>
			                    	Profile <b class="caret"></b></a>
			                    <ul class="dropdown-menu">
			                        <li class="<?php if($current_file=='my-profile.php') { echo 'nav-active'; } ?> sidebar-links">
										<a href="my-profile.php">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>My Profile</span>
										</a>
									</li>

									<li class="<?php if($current_file=='my-profile-picture.php') { echo 'nav-active'; } ?> sidebar-links">
										<a href="my-profile-picture.php">
											<i class="fa fa-picture-o" aria-hidden="true"></i>
											<span>Profile Picture</span>
										</a>
									</li>

									<li class="<?php if($current_file=='my-work.php') { echo 'nav-active'; } ?> sidebar-links">
										<a href="my-work.php">
											<i class="fa fa-braille" aria-hidden="true"></i>
											<span>My Work</span>
										</a>
									</li>
								</ul>
							</li>

							<li class="<?php if($current_file=='my-inquiries.php') { echo 'nav-active'; } ?> ">
								<a href="my-inquiries.php">
									<i class="fa fa-envelope" aria-hidden="true"></i>
									<span>My Inquiries</span>
								</a>
							</li>

			            </ul>

			            <p align='right'>
				            <strong>You Last logged in On: 
								<?php 
									$vendor_last_logged_On = getLastVendorLoginDate($vuserid);
									echo date('d-M-Y h:i A',strtotime($vendor_last_logged_On));
								?>
							</strong>
						
							<strong class="login-ip"> | IP Address: <?php echo getLastVendorLoginIP($vuserid);?></strong>
						</p>
			        </div><!--/.nav-collapse -->
			    </div>
			</div>
			<!--    Secondary Menu End   -->
	<?php
		}
	?>
	<!-- end: search & user box -->
</header>