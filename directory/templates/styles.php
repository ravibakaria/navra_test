  <!-- CSS -->
  <link rel="stylesheet" href="../css/bootstrap.css" type='text/css' />
  <link rel="stylesheet" href="../css/font-awesome.css" type='text/css' />
  <link rel="stylesheet" href="../css/bootstrap-glyphicons.css" type='text/css' />
  <link rel="stylesheet" href="../css/magnific-popup.css" type='text/css' />
  <link rel="stylesheet" href="../css/datepicker3.css" type='text/css' />
  <link rel="stylesheet" href="../css/theme.css" type='text/css' />
  <link rel="stylesheet" href="../css/default.css" type='text/css' />
  <link rel="stylesheet" href="../css/theme-custom.css" type='text/css' />
  <link rel="stylesheet" href="../css/master.css" type='text/css' />
  <link rel="stylesheet" href="../css/custom.css" type='text/css' />
  <link rel="stylesheet" href="../config/style.php" type='text/css' />
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/bootstrap-toggle.min.css" type='text/css' />
  <link rel="stylesheet" href="../css/dataTables.bootstrap.min.css" type='text/css' />

  <script src="../js/modernizr.js"></script>
    
</head>