<header class="header">
      
      <div class="navbar-header page-scroll">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
            <i class="fa fa-bars mobile-top"></i>
        </button>
        <a class="navbar-brand logo" href="../index.php">
          <?php
            if($logoURL!='' || $logoURL!=null)
            {
                echo "<img src='$siteBasePath/$logo' class='img-responsive logo-img' alt='$sitetitle'/>";
            }
            else
            {
                echo "<img src='$siteBasePath/images/logo/default-logo.jpg' class='img-responsive logo-img' alt='$sitetitle'/>";
            }
          ?>
          
        </a>
      </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
        <ul class="nav navbar-nav homepage-nav">
          <li><a href="<?php echo $siteBasePath; ?>/users/advanced-search.php" class="top_nav_links">Advance Search</a></li>
          <?php
            if(isset($_SESSION['logged_in']) && $_SESSION['client_user']!='')
            {
              
            }
            else
            {
              echo "<li><a href='$siteBasePath/free-register.php' class='top_nav_links'>Free Registration</a></li>";
            }
          ?>

          <?php
              $membership_plan_available_count = getMembershipPlanCount();
              if($membership_plan_available_count>1)
              {
                echo "<li><a href='$siteBasePath/membership-plans.php' class='top_nav_links'>Membership Plans</a></li>";
              }
          ?>
          <?php
              $wedding_directory_display_or_not = getWeddingDirectoryDisplayOrNot();

              if($wedding_directory_display_or_not=='1')
              {
                echo "<li><a href='$siteBasePath/directory/' class='top_nav_links'>Wedding Directory</a></li>";
              }
          ?>
          
          <?php
            if(isset($_SESSION['logged_in']) && $_SESSION['client_user']!='')
            {
              echo "<li><a href='$siteBasePath/users/' class='top_nav_links'>My Account</a></li>";
              echo "<li><a href='$siteBasePath/users/logout.php'  class='top_nav_links'>Logout</a></li>";
            }
            else
            {
              echo "<li><a href='$siteBasePath/login.php' class='top_nav_links'>Login</a></li>";
            }
          ?>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
      <!-- end: search & user box -->
    </header>