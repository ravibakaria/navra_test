<body class="vendor-section">
  <?php
    $sitetitle = getWebsiteTitle();
    $siteBasePath = getWebsiteBasePath();
    $logo=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
      $logoURL = explode('/',$logoURL);

      for($i=1;$i<count($logoURL);$i++)
      {
        $logo[] = $logoURL[$i];
      }

      $logo = implode('/',$logo);
    }
    
    if($logo=='' || $logo==null)
    {
      $logo = $siteBasePath."images/logo/default-logo.jpg";
    }
  ?>
  <!-- Return to Top -->
  
    <a href="javascript:" id="return-to-top" class="return-to-top" style="display:none;"><i class="fa-caret-square-o-up"></i></a>
  
  <section>
    <?php
      if(!isset($_SESSION['logged_in']) || isset($_SESSION['client_user']) || isset($_SESSION['admin_user']) || $_SESSION['vendor_user']=='')
      {
          require_once("user-menu.php");
      }
      else
      if(isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']!='' || $_SESSION['vendor_user']!=null))
      {
          require_once("vendor-menu.php");
      }
    ?>
    <!-- end: header -->

    

