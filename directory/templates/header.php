<?php 

  if(!isset($_SESSION))
  {
    ob_start();
    session_start();
  }
  
  if(isset($_SESSION['logged_in']) && (@$_SESSION['vendor_user']!='' || @$_SESSION['vendor_user']!=null))
  {
    $vuserid = $_SESSION['vuserid'];
    $vemail = $_SESSION['vuser_email'];
  }

  if(!file_exists('../config/config.php'))
  {
      header('Location: ../website-under-maintainance.php');
      exit;
  }

  require_once '../config/config.php'; 
  include("../config/dbconnect.php"); 
  require_once '../config/functions.php'; 
  require_once '../config/setup-values.php'; 

  $maintainance_mode = getMaintainanceModeActiveOrNot();

  if($maintainance_mode=='1')
  {
      echo "<script>window.location.assign('../website-maintainance.php');</script>";
      exit;
  }
  
  if(isset($_SESSION['order_id']) && ($_SESSION['order_id']!='' || $_SESSION['order_id']!=null))
  {
    unset($_SESSION['order_id']);
  }
  
  $primary_color = getPrimaryColor();
  $primary_font_color = getPrimaryFontColor();

  $sidebar_color = getSidebarColor();
  $sidebar_font_color = getSidebarFontColor();
  
  $site_title = getWebsiteTitle();
  $site_tagline = getWebSiteTagline();
  $site_url = getWebsiteBasePath();

  $logo=array();
  $logoURL = getLogoURL();
  if($logoURL!='' || $logoURL!=null)
  {
    $logoURL = explode('/',$logoURL);

    for($i=1;$i<count($logoURL);$i++)
    {
      $logo[] = $logoURL[$i];
    }

    $logo = implode('/',$logo);
  }

  if($logo=='' || $logo==null)
  {
    $logo = "images/logo/default-logo.jpg";
  }

  $current_file = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta charset="UTF-8">
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!-- Favicon  -->
  <?php 
    $websiteBasePath = getWebsiteBasePath();
    if(getFaviconURL()!='' || getFaviconURL()!=null)
    {
        $favicon_arr=array();
        $favicon_get = getFaviconURL();
        if($favicon_get!='' || $favicon_get!=null)
        {
          $favicon_get = explode('/',$favicon_get);

          for($i=1;$i<count($favicon_get);$i++)
          {
            $favicon_arr[] = $favicon_get[$i];
          }

          $favicon = implode('/',$favicon_arr);
        }
        echo "<link rel='icon' type='image/png' href='$websiteBasePath/$favicon' sizes='32x32'>";
    }
    else
    {
        echo "<link rel='icon' type='image/png' href='$websiteBasePath/images/favicon/default-favicon.png' sizes='32x32'>";
    }
  ?>
  

  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "<?= $site_title;?>",
    "alternateName": "<?= $site_tagline;?>",
    "url": "<?= $site_url;?>",
    "logo": "<?= $site_url.'/'.$logo; ?>",
    "description": "<?= $site_title.' - '.$site_tagline;?>",
    "email": "<?= getWebSiteEmail(); ?>"
  }
  </script>
