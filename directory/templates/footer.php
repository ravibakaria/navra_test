    <?php include("../layout/footer-content.php");?>
    
    <!-- Scripts -->
    <script src="../js/jquery.js"></script>
    <script src="../js/jquery.browser.mobile.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/nanoscroller.js"></script>
    <script src="../js/bootstrap-datepicker.js"></script>
    <script src="../js/magnific-popup.js"></script>
    <script src="../js/jquery.placeholder.js"></script>
    <script src="../js/bootstrap-multiselect.js"></script>
    <script src="../js/theme.js"></script>
    <script src="../js/theme.custom.js"></script>
    <script src="../js/theme.init.js"></script>
    <script src="../js/examples.mediagallery.js"></script>
    <script src="../js/examples.lightbox.js"></script>
    <script src="../js/examples.advanced.form.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
    <script src="../js/jquery.richtext.js"></script>
    
    <!-- Footer Script  -->
    <?php 
        $include_footer_script = getFooterScriptDisplay();
        $footer_script = getFooterScript();
        if($include_footer_script!='' && $include_footer_script!=null && $include_footer_script!='0')
        {
            @$footer_script_data = file_get_contents($websiteBasePath.'/'.$footer_script);
            echo "$footer_script_data";
        }
    ?>

<script>
    $(document).ready(function(){
         $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
            
            $('#back-to-top').tooltip('show');

    });
</script>