<?php
  if(!isset($_SESSION))
  {
    session_start();
  }

  include("templates/header.php"); 
  include('../config/setup-values.php');   //get master setup values
  include('../config/email/email_style.php');   //get master setup values
  include('../config/email/email_templates.php');   //get master setup values
  include('../config/email/email_process.php');

  if(isset($_SESSION['logged_in']) && ((isset($_SESSION['client_user'])) && ($_SESSION['client_user']!='' || $_SESSION['client_user']!=null)))
  {
    $user_id = $_SESSION['user_id'];
    $user_full_name = getUserFirstName($user_id)." ".getUserLastName($user_id);
    $user_email = getUserEmail($user_id);
    $user_phone = getUserMobile($user_id);
  }

  $errorMessage=null;
  $vendor_userid = $_GET['userid'];
  $vendor_userid = explode('-',$vendor_userid);
  $vuserid = $vendor_userid[1];

  $sql_vendor = "SELECT * FROM vendors WHERE vuserid='$vuserid' AND status='1'";
  $stmt_vendor = $link->prepare($sql_vendor);
  $stmt_vendor->execute();
  $count_vendor = $stmt_vendor->rowCount();
  $result_vendor = $stmt_vendor->fetch();

  if(!is_numeric($vuserid))
  {
    $errorMessage = "ERROR! Invalid parameter value";
    $title = "Invalid parameter value";
  }

  if($errorMessage==null && $count_vendor==0)
  {
    $errorMessage = "ERROR! Invalid parameter value";
    $title = "Invalid parameter value";
  }

  if($count_vendor>0)
  {
    $title =  $result_vendor['company_name'];
  }

  /**********   Form submit status   ***********/
  $recaptchaAllowed = getreCaptchaAllowed();
  if($recaptchaAllowed == "1")
  {
      $recaptchaSiteKey=getreCaptchaSiteKey();
      $recaptchaSecretKey=getreCaptchaSecretKey();
  }

  if ((isset($_POST['fullname']) && $_POST['fullname'] != '') && (isset($_POST['email']) && $_POST['email'] != '') && (isset($_POST['phone']) && $_POST['phone'] != '') && (isset($_POST['subject']) && $_POST['subject'] != '') && (isset($_POST['message']) && $_POST['message'] != '') && (isset($_POST['vuserid']) && $_POST['vuserid'] != ''))
  {
    $fullname = $_POST['fullname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    $isRegistered = $_POST['isRegistered'];
    $userid = $_POST['userid'];
    $vuserid = $_POST['vuserid'];

    //print_r($_POST);exit;
    $sql_insert = "INSERT INTO `vendor_enquiries`(vuserid,fullname, email, phone, subject, message, isRegistered, userid, enquiry_on) VALUES ('$vuserid','$fullname','$email','$phone','$subject', '$message', '$isRegistered', '$userid', now())";

    if($link->exec($sql_insert))
    {
        $full_name = $fullname;
        $email_address = $email;
        $mobile = $phone;
        $enquiry_subject = $subject;
        $message = $message;

        $sitetitle = getWebsiteTitle();
        $logo_array=array();
        $logoURL = getLogoURL();
        if($logoURL!='' || $logoURL!=null)
        {
            $logoURL = explode('/',$logoURL);

            for($i=1;$i<count($logoURL);$i++)
            {
                $logo_array[] = $logoURL[$i];
            }

            $logo_path = implode('/',$logo_array);
        }

        if($logoURL!='' || $logoURL!=null)
        {
            $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
        }
        else
        {
            $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
        }

        $SocialSharing = getSocialSharingLinks();   // social sharing links
        $primary_color = getPrimaryColor();
        $primary_font_color = getPrimaryFontColor();

        $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$sitetitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

        $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo'),array($WebSiteBasePath,$sitetitle,$logo),$EmailGlobalHeader);  //replace header variables with value

        $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$sitetitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

        $EnquiryEmailMessage = str_replace(array('$site_name','$full_name','$email_address','$mobile','$enquiry_subject','$message','$signature'),array($sitetitle,$full_name,$email_address,$mobile,$enquiry_subject,$message,$GlobalEmailSignature),$EnquiryEmailMessage);  //replace footer variables with value

        $subject = "New Enquiry : ".$sitetitle;
          
        $message  = '<!DOCTYPE html>';
        $message .= '<html lang="en">
          <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width">
          <title></title>
          <style type="text/css">'.$EmailCSS.'</style>
          </head>
          <body style="margin: 0; padding: 0;">';
        //echo $message;exit;
        $message .= $EmailGlobalHeader;

        $message .= $EnquiryEmailMessage;

        $message .= $EmailGlobalFooter;

        $vendorEmail = getVendorEmail($vuserid);
        $vendorName = getVendorFirstName($vuserid).' '.getVendorLastName($vuserid);

        $mailto = $vendorEmail;
        $mailtoname = $vendorName;

        $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

        /*************      Email log      ***************/
        $sql_member_email_log = "INSERT INTO vendor_email_logs(vuserid,task,activity,sent_On) VALUES('$vuserid','new enquiry','$email',now())";


        if($emailResponse == 'success')
        {
            $link->exec($sql_member_email_log);
            $_SESSION['alert']='success';
            $_SESSION['alert_message'] = "success";
        }
        else
        {
          $_SESSION['alert']='danger';
          $_SESSION['alert_message'] = "Error sending email! Please try after some time.";
        }
    }
    else
    {
      $_SESSION['alert']='danger';
      $_SESSION['alert_message'] = "Something went wrong! Try after some time.";
    }

  echo "<script>window.location.assign('Profile-$vuserid.html')<script>";  
  }

  
?>
    <!-- meta info  -->

    <title><?php echo $title.' - '.$site_title;?></title>

    <meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

    <meta name="description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!"/>

    <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, vendor information, vendor lobin, vendor registration" />

    <meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
    <meta property="og:url" content="<?= $site_url; ?>" />
    <meta property="og:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
    <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
    <meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
    <meta property="twitter:url" content="<?= $site_url; ?>" />
    <meta property="twitter:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
    <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

    <script src="../js/jquery.js"></script>
    
    <script>
        function onSubmit(token)
        {
            document.getElementById("frmLogin").submit();
        }

        function validate(event) 
        {
            event.preventDefault();
            var fullname = $('.fullname').val();
            var email = $('.email').val();
            var phone = $('.phone').val();
            var subject = $('.subject').val();
            var message = $('.message').val();
            var recaptchaAllowed = $('.recaptchaAllowed').val();
            var empty_success=0; 
            var error;           

            /* fullname  Validation */
            if(fullname=='' || fullname==null)
            {
                $('.fullname').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* email Validation */
            if(email=='' || email==null)
            {
                $('.email').addClass('danger_error');
                empty_success=empty_success+1;
            }

            function validateEmail($email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $email );
            }

            if(email!='' && !validateEmail(email)) 
            { 
                $('.email').focus();
                error = "Enter valid email address";
                $('.email').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* phone validation */
            if(phone=='' || phone==null)
            {
                $('.phone').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(phone!='' && phone.length<4)
            {
                $('.phone').focus();
                if(error==null)
                {
                  error = "Mobile number should be of minimum 4 digits";
                }
                $('.phone').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* subject validation */
            if(subject=='' || subject==null)
            {
                $('.subject').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* message validation */
            if(message=='' || message==null)
            {
                $('.message').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(empty_success>0)
            {
              if(error==null)
              {
                $('.send_enquiry_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong> Empty! </strong> Please enter all required field & then click on register button.</div>");
              }
              else
              {
                $('.send_enquiry_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong> Empty! </strong>"+error+".</div>");
              }
                
                return false;
            }

            //recaptcha validation 
            if(recaptchaAllowed=='1')
            {
                grecaptcha.execute();           
            }
            else
            {
                document.getElementById("frmLogin").submit();
            }

            $('.loading_img').show();
            $('.send_enquiry_status').html("");
        }

        function onload() 
        {
            var element = document.getElementById('recaptcha-submit');
            element.onclick = validate;
        }
    </script>
<?php
    include("templates/styles.php"); 
    include("templates/menu.php"); 
?>

<!-- Google recaptcha -->
<?php
    if($recaptchaAllowed == "1")
    {
        echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
    }
?>
<div class="col-md-12">
  <br><br><br><br><br>
</div>
<?php
  if($errorMessage=='' || $errorMessage==null)
  {
    $vuserid =  $result_vendor['vuserid'];
    $firstname =  $result_vendor['firstname'];
    $lastname =  $result_vendor['lastname'];
    $email =  $result_vendor['email'];
    $city =  $result_vendor['city'];
    $state =  $result_vendor['state'];
    $country =  $result_vendor['country'];
    $short_desc =  $result_vendor['short_desc'];
    $company_name =  $result_vendor['company_name'];
    $category_id =  $result_vendor['category_id'];

    $profile_pic = getVendorProfilePic($vuserid);
    $profile_pic_path = 'uploads/'.$vuserid.'/profile/'.$profile_pic;
?>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 centered-data">
      <h1><?php echo $company_name;?><h1>
      <center><hr class="dotted" style="width:50%;"></center>
  </div>
</div>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
      
        <div class="panel panel-default vendor-category">
          <div class="panel-body">
              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <center>
                  <?php
                    if($profile_pic=='' || $profile_pic==null)
                    {
                      echo "<img src='../images/no_profile_pic.png' class='rounded img_preview' alt='$firstname'>";
                    }
                    else
                    {
                      echo "<img src='$profile_pic_path' class='rounded img_preview' alt='$firstname' >";
                    }
                  ?> 
                </center> 
              </div>
              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 text-left">
                <?php echo $short_desc;?>
                <hr/>
                <b>Location: </b> &nbsp;&nbsp;&nbsp;
                <?php echo getUserCityName($city).", ".getUserStateName($state).", ".getUserCountryName($country);?>
                <br>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-left">
                  <a class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Send_Email_<?php echo $vuserid;?>">Send Email</a>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-left">
                  <?php
                    if(isset($_SESSION['alert']) && ($_SESSION['alert']!='' || $_SESSION['alert']!=null))
                    {
                      if ($_SESSION['alert'] == 'danger')
                      {
                          echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
                          echo "<strong><i class='fa fa-times'></i> Error!</strong> ".$_SESSION['alert_message'];
                          echo "</div>";
                      }
                      else
                      if ($_SESSION['alert'] == 'success')
                      {
                          echo "<div class='alert alert-success col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
                          echo "<strong><i class='fa fa-check'></i> Success!</strong> Email sent successfully.";
                          echo "</div>";
                      }

                      unset($_SESSION['alert']);
                      unset($_SESSION['alert_message']);
                    }
                  ?>
                </div>

                <!-- Modal -->
                <div class="modal fade vendor-enquiry-model" id="Send_Email_<?php echo $vuserid;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <center><h4 class="modal-title" id="exampleModalLongTitle">Send enquiry email</h4></center>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                              <form name="frmLogin" id="frmLogin" autocomplete="off" method="post"  action="Profile-<?php echo $vuserid;?>.html">
                                <div class="row wedding-input-row">
                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                    <p class="danger-text">* All fields are mandatory.</p>
                                  </div>
                                </div>
                                <div class="row wedding-input-row">
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                        <lable class="control-label">Full Name:</lable>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                        <input type="text" class="form-control form-control-sm fullname" name="fullname" value="<?php echo @$user_full_name;?>"/>
                                    </div>
                                </div>
                                <div class="row wedding-input-row">
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                        <lable class="control-label">Email Address:</lable>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                        <input type="text" class="form-control form-control-sm email"  name="email" value="<?php echo @$user_email;?>"/>
                                    </div>
                                </div>
                                <div class="row wedding-input-row">
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                        <lable class="control-label">Mobile Number:</lable>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                        <input type="text" class="form-control form-control-sm phone" name="phone" value="<?php echo @$user_phone;?>" />
                                    </div>
                                </div>
                                <div class="row wedding-input-row">
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                        <lable class="control-label">Subject:</lable>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                        <input type="text" class="form-control subject" name="subject" />
                                    </div>
                                </div>
                                <div class="row wedding-input-row">
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                        <lable class="control-label">Message:</lable>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                      <textarea class="form-control message" name="message" rows="4"></textarea>
                                    </div>
                                </div>
                                
                                <div class="row wedding-input-row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 send_enquiry_status">
                                    </div>
                                </div>

                                <input type="hidden" class="recaptchaAllowed" value="<?php echo $recaptchaAllowed;?>">

                                <?php
                                  if(isset($_SESSION['logged_in']) && ((isset($_SESSION['client_user'])) && ($_SESSION['client_user']!='' || $_SESSION['client_user']!=null)))
                                  {
                                    echo "<input type='hidden' class='isRegistered' name='isRegistered' value='1'>";
                                    echo "<input type='hidden' class='userid' name='userid' value='$user_id'>";
                                  }
                                  else
                                  {
                                    echo "<input type='hidden' class='isRegistered' name='isRegistered' value='0'>";
                                    echo "<input type='hidden' class='userid' name='userid' value='0'>";
                                  }
                                ?>
                                <input type='hidden' class='vuserid' name='vuserid' value="<?php echo $vuserid;?>">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary website-button" data-dismiss="modal">Close</button>
                                <input type="submit" name="login btn"  class="btn btn-primary btn_send_vendor_enquiry website-button" id="recaptcha-submit" value="SEND">
                            </div>
                            <?php
                                if($recaptchaAllowed == "1")
                                {
                                    echo "<div class='form-group'>
                                        <div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomleft' ></div>
                                    </div>";
                                }
                            ?>
                            
                            <script>onload();</script>
                          </form>
                        </div>
                    </div>
                </div>
                
              </div>
              <br><br>
              
              
                
              <br>
              <?php
                $sql_gallery = "SELECT * FROM vendor_gallery WHERE vuserid='$vuserid' AND status='1'";
                $stmt_gallery = $link->prepare($sql_gallery);
                $stmt_gallery->execute();
                $count_gallery = $stmt_gallery->rowCount();
                if($count_gallery>0)
                {
                  echo "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
                  echo "<div><hr class='dotted'></div>";
                  echo "<center><h3> Work Gallery </h3></center>";
                  echo "<br>";
                  echo "<div class='row' style='margin-top:30px;'>";
                  $result_gallery = $stmt_gallery->fetchAll();
                    $i=0;
                  foreach ($result_gallery as $row_gallery) 
                  {
                      $photo = $row_gallery['photo'];
                      if($i!=0 && $i%3==0)
                      {
                          echo "<div class='row' style='margin-top:30px;'>";
                      }
                      echo "<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4' >";
                        echo "<div class='mg-files' data-sort-destination data-sort-id='media-gallery'>";
                          echo "<div class='isotope-item document col-xs-4 col-lg-4'>";
                              echo "<div class='popup-gallery'>";
                                echo "<center>";
                                  echo "<a class='thumb-image' href='uploads/$vuserid/photo/$photo'  title='$photo'>";
                                    echo "<img src='uploads/$vuserid/photo/$photo' class='vendor-gallary'>";
                                  echo "</a>";
                                echo "</center>";
                              echo "</div>";
                          echo "</div>";
                        echo "</div>";
                      echo "</div>";
                      
                      $i=$i+1;
                      if($i!=$count_gallery && $i%3==0)
                      {
                          echo "</div>";
                      }
                  }
                    echo "</div>";
                  echo "</div>";
                }
              ?>
             
          </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-left">
        <div class="panel panel-default similar-vendors-panel">
          <div class="panel-heading"><h2>Similar Business</h2></div>
          <div class="panel-body similar-vendors-panel">
            
            <?php
              $query_similar  = "SELECT * FROM `vendors` WHERE category_id='$category_id' AND vuserid!='$vuserid'  ORDER BY `company_name` ASC";
              $stmt_similar   = $link->prepare($query_similar);
              $stmt_similar->execute();
              $count_similar = $stmt_similar->rowCount();
              $result_similar = $stmt_similar->fetchAll();
              if($count_similar>0)
              {
                foreach( $result_similar as $row_similar )
                {
                  
                  $vuseridx =  $row_similar['vuserid'];
                  $firstnamex =  $row_similar['firstname'];
                  $lastnamex =  $row_similar['lastname'];
                  $emailx =  $row_similar['email'];
                  $cityx =  $row_similar['city'];
                  $statex =  $row_similar['state'];
                  $countryx =  $row_similar['country'];
                  $short_descx =  $row_similar['short_desc'];
                  $company_namex =  $row_similar['company_name'];
                  $category_idx =  $row_similar['category_id'];
                  if(strlen($short_descx)>125)
                  {
                    $short_descx = substr($short_descx, 0, 125)."...";
                  }

                  $profile_picx = getVendorProfilePic($vuseridx);
                  if($profile_picx=='' || $profile_picx==null)
                  {
                    $profile_picx = $WebSiteBasePath."/images/no_profile_pic.png";
                  }
                  else
                  {
                    $profile_picx = $WebSiteBasePath."/directory/uploads/".$vuseridx."/profile/".$profile_picx;
                  }
              ?>
                
                <a href="Profile-<?php echo $vuseridx;?>.html" class="category-links">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">
                      <br>
                      <img src="<?php echo $profile_picx; ?>" class="img img-responsive" style="width:80px;">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 text-left">
                      <h3><b><?php echo $company_namex;?></b></h3>
                      <?php echo $short_descx;?>
                      <br>
                      <b>Location: </b> &nbsp;&nbsp;&nbsp;
                      <?php echo getUserCityName($cityx).", ".getUserStateName($statex).", ".getUserCountryName($countryx);?>
                      <br>
                      <a href="Profile-<?php echo $vuseridx;?>.html">View Profile</a>

                    </div>
                  </div>
                </a>
                <div class="row">
                  <hr class="dotted">
                </div>
              <?php    
                }
              }
              else
              if($count_similar==0)  
              {
                echo "No records found...";
              }
              ?>
          </div>
        </div>
    </div>
    
  </div>
</div>

<div>
<br><br><br>
</div>
<?php
  }
  else
  {
    echo "<br><br><br><br><br><br><br><br>";
    echo "<div class='container centered-data'><h1>".$errorMessage."</h1></div>";
    echo "<br><br><br><br><br><br><br><br>";
  }
?>
</section>


<?php
  include("templates/footer.php");
?>

<script>
  $(document).ready(function(){

    /* empty error message  */
    $('.fullname, .email, .phone, .subject, .message').keypress(function(){
        $('.send_enquiry_status').html("");
    });

    /* empty error message  */
    $('.fullname, .email, .phone, .subject, .message').click(function(){
        $('.send_enquiry_status').html("");
    });

    /* empty danger class message  */
    $('.fullname').click(function(){
        $('.fullname').removeClass("danger_error");
    });

    $('.email').click(function(){
        $('.email').removeClass("danger_error");
    });

    $('.phone').click(function(){
        $('.phone').removeClass("danger_error");
    });

    $('.subject').click(function(){
        $('.subject').removeClass("danger_error");
    });

    $('.message').click(function(){
        $('.message').removeClass("danger_error");
    });

    /*   Prevent entering charaters in mobile & phone number   */
    $(".phone").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
        {
            //display error message
            return false;
        }
    })

  });
</script>

</body>
</html>