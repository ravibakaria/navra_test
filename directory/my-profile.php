<?php
	session_start();
  	if(!isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']=='' || $_SESSION['vendor_user']==null))
  	{
		header('Location: login.php');
		exit;
	}
	include("templates/header.php"); 	
?>

<!-- meta info  -->

<title>
  <?php
  $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
  $string = str_replace("-", " ", $a);
  echo $title = ucwords($string);
  ?> -  <?php echo getWebsiteTitle(); ?>
</title>

<meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

<meta name="description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!"/>

<meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, vendor information, vendor lobin, vendor registration" />

<meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
<meta property="og:url" content="<?= $site_url; ?>" />
<meta property="og:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
<meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
<meta property="twitter:url" content="<?= $site_url; ?>" />
<meta property="twitter:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
    include("templates/styles.php"); 
    include("templates/menu.php"); 
?>

<div class="col-md-12">
  <br><br><br><br><br><br>
</div>

<div class="vendor-data">
	<?php
	    $sql_profile = "SELECT * FROM vendors WHERE  vuserid='$vuserid'";
	    $stmt_profile = $link->prepare($sql_profile);
	    $stmt_profile->execute();
	    $result_profile = $stmt_profile->fetch();

	    $firstname = $result_profile['firstname'];
	    $lastname = $result_profile['lastname'];
	    $email = $result_profile['email'];
	    $phonecode = $result_profile['phonecode'];
	    $phone = $result_profile['phone'];
	    $company_name = $result_profile['company_name'];
	    $category_id = $result_profile['category_id'];
	    $city = $result_profile['city'];
	    $state = $result_profile['state'];
	    $country = $result_profile['country'];
	    $short_desc = $result_profile['short_desc'];
	    $created_on = $result_profile['created_on'];
	    $updated_on = $result_profile['updated_on'];
	    $isEmailVerifiedOn = $result_profile['isEmailVerifiedOn'];
	    $status = $result_profile['status'];

	    $category_name = getdirectorynamefromId($category_id);
	    $city_name = getUserCityName($city);
	    $state_name = getUserStateName($state);
	    $country_name = getUserCountryName($country);
  	?>

  	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    		<div class="row profile-row">
	    			<div class="text-left col-xs-12 col-sm-12 col-md-6 col-lg-6">
	    				<h1>My Profile</h1>
	    			</div>
	    			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
	                    <button class="btn btn-primary update-profile website-button" data-toggle="modal" data-target="#update-profile">Edit Profile</button>
	    			</div>
	    		</div>

	    		<br><br>

	    		<div class="row profile-row">
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 border-left-div">
	    				<b>First Name</b>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
	    				<?= $firstname; ?>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 border-left-div">
	    				<b>Last Name</b>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
	    				<?= $lastname; ?>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 border-left-div">
	    				<b>Company Name</b>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
	    				<?= $company_name; ?>
	    			</div>
	    		</div>

	    		<br><br>

	    		<div class="row profile-row">
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 border-left-div">
	    				<b>Category</b>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
	    				<?= $category_name; ?>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 border-left-div">
	    				<b>Email Id</b>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
	    				<?= $email; ?>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 border-left-div">
	    				<b>Mobile</b>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
	    				<?= '+'.$phonecode.' '.$phone; ?>
	    			</div>
	    		</div>

	    		<br><br>

	    		<div class="row profile-row">
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 border-left-div">
	    				<b>City</b>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
	    				<?= $city_name; ?>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 border-left-div">
	    				<b>State</b>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
	    				<?= $state_name; ?>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 border-left-div">
	    				<b>Country</b>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
	    				<?= $country_name; ?>
	    			</div>
	    		</div>

	    		<br><br>

	    		<div class="row profile-row border-left-div">
	    			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
	    				<b>Short Description</b>
	    			</div>
	    			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
	    				<?= $short_desc; ?>
	    			</div>
	    		</div>

	    		<br><br>

	    	</div>
	    </div>

	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<!-- Edit Profile Modal -->
	        <div class="modal fade" id="update-profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	            <div class="modal-dialog modal-dialog-centered add-item-model-template" role="document">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <center><h2 class="modal-title" id="exampleModalLongTitle">Edit Profile Information</h2></center>
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                        </button>
	                    </div>
	                    <div class="modal-body">
	                    	<div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">First Name:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm firstname" value="<?php echo $firstname;?>"/>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Last Name:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm lastname" value="<?php echo $lastname;?>" />
		                            </div>
		                        </div>		                        
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Company Name:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm company_name" value="<?php echo $company_name;?>" />
		                            </div>
		                        </div>
	                        </div>
	                        <br><br>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Category:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control category_id">
		                                	<?php
		                                		$sql_category = "SELECT * FROM membercategory ORDER BY name ASC";
		                                		$stmt_category = $link->prepare($sql_category);
		                                		$stmt_category->execute();
		                                		$result_category = $stmt_category->fetchAll();

		                                		foreach ($result_category as $row_category) {
		                                			$cat_id = $row_category['id'];
		                                			$cat_name = $row_category['name'];

		                                			if($cat_id == $category_id)
		                                			{
		                                				echo "<option value='".$cat_id."' selected>".$cat_name."</option>";
		                                			}
		                                			else
		                                			{
		                                				echo "<option value='".$cat_id."'>".$cat_name."</option>";
		                                			}
		                                		}
		                                	?>
		                                </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Email:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm email" value="<?php echo $email;?>" disabled="disabled" />
		                            </div>
		                        </div>		                        
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Mobile:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm phone" value="<?php echo $phone;?>" disabled="disabled"  />
		                            </div>
		                        </div>
	                        </div>
	                        <br><br>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Country:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control country">
		                                	<?php
		                                		$sql_country = "SELECT * FROM countries WHERE status='1'";
		                                		$stmt_country = $link->prepare($sql_country);
		                                		$stmt_country->execute();
		                                		$result_country = $stmt_country->fetchAll();

		                                		foreach ($result_country as $row_country) {
		                                			$country_id = $row_country['id'];
		                                			$country_name = $row_country['name'];

		                                			if($country_id == $country)
		                                			{
		                                				echo "<option value='".$country_id."' selected>".$country_name."</option>";
		                                			}
		                                			else
		                                			{
		                                				echo "<option value='".$country_id."'>".$country_name."</option>";
		                                			}
		                                		}
		                                	?>
		                                </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">State</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control state">
			                                <?php
		                                		$sql_state = "SELECT * FROM states WHERE country_id='$country' AND status='1'";
		                                		$stmt_state = $link->prepare($sql_state);
		                                		$stmt_state->execute();
		                                		$result_state = $stmt_state->fetchAll();

		                                		foreach ($result_state as $row_state) {
		                                			$state_id = $row_state['id'];
		                                			$state_name = $row_state['name'];

		                                			if($state_id == $state)
		                                			{
		                                				echo "<option value='".$state_id."' selected>".$state_name."</option>";
		                                			}
		                                			else
		                                			{
		                                				echo "<option value='".$state_id."'>".$state_name."</option>";
		                                			}
		                                		}
		                                	?>
		                                </select>
		                            </div>
		                        </div>		                        
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">City</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control city">
			                                <?php
		                                		$sql_city = "SELECT * FROM cities WHERE state_id='$state' AND status='1'";
		                                		$stmt_city = $link->prepare($sql_city);
		                                		$stmt_city->execute();
		                                		$result_city = $stmt_city->fetchAll();

		                                		foreach ($result_city as $row_city) {
		                                			$city_id = $row_city['id'];
		                                			$city_name = $row_city['name'];

		                                			if($city_id == $city)
		                                			{
		                                				echo "<option value='".$city_id."' selected>".$city_name."</option>";
		                                			}
		                                			else
		                                			{
		                                				echo "<option value='".$city_id."'>".$city_name."</option>";
		                                			}
		                                		}
		                                	?>
		                                </select>
		                            </div>
		                        </div>
	                        </div>
	                        <br><br>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Short Description:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <textarea class="form-control form-control-sm short_desc" rows="6"/><?php echo $short_desc;?></textarea>
		                            </div>
		                        </div>
	                        </div>

	                        <input type="hidden" class="phonecode" value="<?php echo $phonecode;?>">
	                       	<input type="hidden" class="vuserid" value="<?php echo $vuserid;?>">
	                        <div class="row">
	                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:70px; height:70px; display:none;'/></center>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 edit-user-profile-status">
	                            </div>
	                        </div>
	                    </div>
	                    <div class="modal-footer">
	                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                        <button type="button" class="btn btn-primary btn_edit_user_profile website-button">Update</button>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>

</section>
<?php
  	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){

		$('#update-profile').on('hidden.bs.modal', function () {
			location.reload();
		})

		$('.country').change(function(){         //Fetch states
            var country_id = $(this).val();
            var task = "Fetch_state_data";  

            $.ajax({
                type:'post',
                data:'country_id='+country_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    var result = $.parseJSON(res);
                    $('.state').html(result[0]);
                    //$('.country_code').html("+"+result[1]);
                    $('.phonecode').val(result[1]);
                }
            });         
        });

        $('.state').change(function(){         //Fetch cities
            var state_id = $(this).val();
            var task = "Fetch_city_data";  
            $.ajax({
                type:'post',
                data:'state_id='+state_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    $('.city').html(res);
                }
            });         
        });

        $('.btn_edit_user_profile').click(function(){
        	var firstname = $('.firstname').val();
        	var lastname = $('.lastname').val();
        	var company_name = $('.company_name').val();
        	var category_id = $('.category_id').val();
        	var email = $('.email').val();
        	var phone = $('.phone').val();
        	var country = $('.country').val();
        	var state = $('.state').val();
        	var city = $('.city').val();
        	var phonecode = $('.phonecode').val();
        	var short_desc = $('.short_desc').val();
        	var vuserid = $('.vuserid').val();
        	var task = 'update_vendor_profile';

        	if(firstname=='' || firstname==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty: </strong> Please enter firstname!</div>");
                return false;
            }

            if(lastname=='' || lastname==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty: </strong> Please enter lastname!</div>");
                return false;
            }

            if(company_name=='' || company_name==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty: </strong> Please enter company name!</div>");
                return false;
            }

            if(state=='0')
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty: </strong> Please select state!</div>");
                return false;
            }

            if(city=='0')
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty: </strong> Please select city!</div>");
                return false;
            }

            if(short_desc=='' || short_desc==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty: </strong> Please enter company short description!</div>");
                return false;
            }

            var data = {
            	firstname : firstname,
            	lastname : lastname,
            	company_name : company_name,
            	category_id : category_id,
            	email : email,
            	phone : phone,
            	phonecode : phonecode,
            	country : country,
            	state : state,
            	city : city,
            	short_desc : short_desc,
            	vuserid : vuserid,
            	task : task
            };

            $('.loading_img').show();
            $('.edit-user-profile-status').html("");

            $.ajax({
            	type:'post',
            	dataType:'json',
            	data:data,
            	url:'query/my-profile-helper.php',
            	success:function(res){
            		$('.loading_img').hide();
            		if(res=='success')
            		{
            			$('.edit-user-profile-status').html("<div class='alert alert-success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><i class='fa fa-check'></i> Success: </strong> Profile updated successfully!</div>");
            			return false;
            		}
            		else
            		{
            			$('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><i class='fa fa-times'></i> Error: </strong> "+res+"</div>");
            			return false;
            		}
            	}
            });
        });
	});
</script>
</body>
</html>