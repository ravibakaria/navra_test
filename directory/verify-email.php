<?php
	session_start();
	if(isset($_SESSION['logged_in']) && $_SESSION['vendor_user']!='')
	{
		header('Location: dashboard.php');
		exit;
	}

	include("templates/header.php");
    include('../config/setup-values.php');   //get master setup values
    include('../config/email/email_style.php');   //get master setup values
    include('../config/email/email_templates.php');   //get master setup values
    include('../config/email/email_process.php');

    $today_datetime = date('Y-m-d H:i:s');
?>

<!-- meta info  -->

<title>
	<?php
	$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
	$string = str_replace("-", " ", $a);
	echo $title = ucwords($string);
	?>  -  <?php echo getWebsiteTitle(); ?>
</title>

<?php
  	include("templates/styles.php"); 
  	include("templates/menu.php"); 

  	$sitetitle = getWebsiteTitle();
  	$WebSiteBasePath = getWebsiteBasePath();
	$logo_array=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo_array[] = $logoURL[$i];
		}

		$logo_path = implode('/',$logo_array);
	}

	if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }

?>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>

<div class="container free-register-panel">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<section class="panel register-panel">
				<header class="panel-heading register-panel-heading">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<center><h1 class="panel-title">Verify your email id.</h1></center>
						</div>
					</div>
				</header>

				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<?php
								$passcode = str_replace(" ", "+", $_GET['passcode']);
								$basepath = $WebSiteBasePath.'/';
								if($passcode=='' || $passcode==null)
								{
									echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'><span class='fa fa-exclamation-triangle'></span>";
									echo "<center>";
									echo "<h2>Error: Invalid parameter value.</h2>";
									echo "<strong>Please verify your email address again or </strong><a href='free-register.php' class='btn btn-info'>Register Here</a>";
									echo "</center>";
									echo "</div>";
								}
								else
								{
									$emailx = encrypt_decrypt('decrypt', $passcode);

									define("EMAIL", $emailx);
									
									$sql_chk = $link->prepare("SELECT * FROM `vendors` WHERE email='$emailx'"); 
							        $sql_chk->execute();
							        $count=$sql_chk->rowCount();
							        if($count>0)
							        {
							        	$userData = $sql_chk->fetch(PDO::FETCH_OBJ);
							        	$client_id = $userData->vuserid;
							        	$isEmailVerified = $userData->isEmailVerified;
							        	$curr_password = $userData->password;
							        	$firstname = $userData->firstname;
							        	$sitetitle = getWebsiteTitle();
							    	
							    		if($isEmailVerified=='1')
							    		{
							    			echo "<div class='alert alert-success col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
											echo "<center>";
											echo "<strong><span class='fa fa-check'></span> Success!</strong> You have already verified your email.<br/>";
											echo "Please Login to your account.<a href='login.php' class='btn btn-success'>Login Here</a>";
											echo "</center>";
											echo "</div>";
							    		}
							    		else
							    		if($isEmailVerified=='0')
							    		{
							    			$sql_update = "UPDATE `vendors` SET `isEmailVerified`='1',`isEmailVerifiedOn`='$today_datetime',status='1' WHERE `vuserid`='$client_id' AND `email`='$emailx'";
							    			
							    			$sql_member_log = "INSERT INTO vendor_activity_logs(vuserid,task,activity,IP_Address,created_On) VALUES('$client_id','vendor','verified email','$IP_Address',now())";
							    			$link->exec($sql_member_log);

							    			if($link->exec($sql_update))
											{
												$SocialSharing = getSocialSharingLinks();   // social sharing links

												$login_link = "<a href='$WebSiteBasePath/directory/login.php' target='_blank'>$WebSiteBasePath/directory/login.php</a>";
									            $email_address = $emailx;
									            $forgot_password_link = "<a href='$WebSiteBasePath/directory/forgot-password.php' target='_blank'>$WebSiteBasePath/directory/forgot-password.php</a>";
									            $current_password = $curr_password;
									            $first_name = $firstname;
									            $site_link = "<a href='$WebSiteBasePath' target='_blank'>$WebSiteBasePath/directory/</a>";

								                $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

								                $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

								                $NewRegistrationMessage = str_replace(array('$first_name','$site_name','$login_link','$email_address','$forgot_password_link','$site_link','$signature'),array($firstname,$WebSiteTitle,$login_link,$emailx,$forgot_password_link,$site_link,$GlobalEmailSignature),$NewRegistrationMessage);  //replace footer variables with value
								                //echo $NewRegistrationMessage;exit;
								                $subject = $NewRegistrationSubject;

								                $message  = '<!DOCTYPE html>';
								                $message .= '<html lang="en">
								                    <head>
								                    <meta charset="utf-8">
								                    <meta name="viewport" content="width=device-width">
								                    <title></title>
													<style type="text/css">'.$EmailCSS.'</style>
								                    </head>
								                    <body style="margin: 0; padding: 0;">';
								                //echo $message;exit;
								                $message .= $EmailGlobalHeader;

								                $message .= $NewRegistrationMessage;

								                $message .= $EmailGlobalFooter;

								                $mailto = $emailx;
								                $mailtoname = $firstname;
								                
								                $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

								                $sql_member_email_log = "INSERT INTO vendor_email_logs(vuserid,task,activity,sent_On) VALUES('$client_id','new registration','welcome email',now())";

								                if($emailResponse == 'success')
								                {
								                	$link->exec($sql_member_email_log);
							                        echo "<div class='alert alert-success col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
													echo "<center>";
													echo "<strong><span class='fa fa-check'></span> Success!</strong> Congratulation your account has been verified successfully.<br/>";
													echo "You will be automatically redirected to <a href='login.php'>Login</a> page in 5 seconds...<a href='login.php' style='text-decoration:none;'>Continue</a>";
													echo "<meta http-equiv=\"refresh\" content=\"5;url='login.php'>";
													echo "</center>";
													echo "</div>";
								                }
								                else
								                {
								                	$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$client_id','failed to send','welcome email','$IP_Address',now())";
							    					$link->exec($sql_member_log);

							                        echo "<div class='alert alert-danger col-sm-12' style='padding: 10px; margin-bottom: 10px;'>";
													echo "<center>";
													echo "<h3><span class='fa fa-check'></span> Failed!</h3> Your account verification process not completed successfully.<br/>";
													echo "Please try after some time...<br/>Thank you.";
													echo "</center>";
													echo "</div>";
								                }

												
											}
											else
											{
												$sql_member_log = "INSERT INTO vendor_activity_logs(vuserid,task,activity,IP_Address,created_On) VALUES('$client_id','failed','email verification','$IP_Address',now())";
							    				$link->exec($sql_member_log);

												echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'><span class='fa fa-exclamation-triangle'>";
												echo "<center>";
												echo "<h2>Ooops something went wrong.</h2>";
												echo "<strong>Try after some time Or </strong><a href='free-register.php' class='btn btn-success'>Register Here</a>";
												echo "</center>";
												echo "</div>";
											}
							    		}
							    	
							        }
							        else
							        {
							        	echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'><span class='fa fa-exclamation-triangle'>";
										echo "<center>";
										echo "<h2>Error: Email Id not exists.</h2>";
										echo "<strong>Please register </strong><a href='free-register.php' class='btn btn-info'>Register Here</a>";
										echo "</center>";
										echo "</div>";
							        }
								}
							?>
						</div>
					</div>
				</div>

				<footer class="panel-footer">
					<center>
						
					</center>
				</footer>
			</section>
		</div>
	</div>
</div>

<?php
	include('templates/footer.php')
?>