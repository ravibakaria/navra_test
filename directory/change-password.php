<?php
	session_start();
	if(!isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']=='' || $_SESSION['vendor_user']==null))
  	{
		header('Location: login.php');
		exit;
	}
	include("templates/header.php"); 
?>

<!-- meta info  -->

<title>
  <?php
  $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
  $string = str_replace("-", " ", $a);
  echo $title = ucwords($string);
  ?> -  <?php echo getWebsiteTitle(); ?>
</title>

<meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

<meta name="description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!"/>

<meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, vendor information, vendor lobin, vendor registration" />

<meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
<meta property="og:url" content="<?= $site_url; ?>" />
<meta property="og:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
<meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
<meta property="twitter:url" content="<?= $site_url; ?>" />
<meta property="twitter:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
    include("templates/styles.php"); 
    include("templates/menu.php"); 
?>

<div class="col-md-12">
  	<br><br><br><br><br><br>
</div>


<div class="vendor-data">
	<section class="body-sign admin-change-password-section">
		<div class="center-sign">
			<div class="panel panel-sign vendor-change-password">
				<div class="panel-title-sign mt-xl text-right">
					<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Change Password</h2>
				</div>
				<div class="panel-body">
					<form>
						<div class="form-group mb-lg">
							<div class="clearfix">
								<label class="pull-left">Current Password</label>
							</div>
							<div class="input-group input-group-icon">
								<input name="crr_pwd" type="password" class="form-control input-lg crr_pwd" />
								<span class="input-group-addon">
									<span class="icon icon-lg">
										<i class="fa fa-lock"></i>
									</span>
								</span>
							</div>
						</div>

						<div class="form-group mb-lg">
							<div class="clearfix">
								<label class="pull-left">New Password</label>
							</div>
							<div class="input-group input-group-icon">
								<input name="new_pwd" type="password" class="form-control input-lg new_pwd" />
								<span class="input-group-addon">
									<span class="icon icon-lg">
										<i class="fa fa-lock"></i>
									</span>
								</span>
							</div>
						</div>

						<div class="form-group mb-lg">
							<div class="clearfix">
								<label class="pull-left">Confirm New Password</label>
							</div>
							<div class="input-group input-group-icon">
								<input name="cnf_new_pwd" type="password" class="form-control input-lg cnf_new_pwd" />
								<span class="input-group-addon">
									<span class="icon icon-lg">
										<i class="fa fa-lock"></i>
									</span>
								</span>
							</div>
						</div>
						<div class="form-group mb-lg">
							<div class="input-group input-group-icon">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
							</div>
							<div class="input-group input-group-icon form_status">
								
							</div>
						</div>
						<div class="mb-xs text-center">
							<button type="button" class="btn btn-primary mb-md ml-xs mr-xs btn_change_password website-button">Change Password</button>
						</div>

					</form>
				</div>
			</div>
		</div>
	</section>
</div>

</section>
<?php
    include("templates/footer.php");
?>

<script>
	$(document).ready(function(){

		/* removing religious info error_class  */
        $('.crr_pwd, .new_pwd, .cnf_new_pwd').click(function(){
            $(this).removeClass("danger_error");
            $('.form_status').html("");
        });

		$('.btn_change_password').click(function(){
			var crr_pwd = $('.crr_pwd').val();
			var new_pwd = $('.new_pwd').val();
			var cnf_new_pwd = $('.cnf_new_pwd').val();
			var task = "vendor_Change_Password";

			if(crr_pwd=='' || crr_pwd==null)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter your current password</div>");
				$('.crr_pwd').focus();
				$(".crr_pwd").addClass("danger_error");
				return false;
			}

			if(new_pwd=='' || new_pwd==null)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter new password</div>");
				$('.new_pwd').focus();
				$(".new_pwd").addClass("danger_error");
				return false;
			}

			if(cnf_new_pwd=='' || cnf_new_pwd==null)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Confirm new password</div>");
				$('.cnf_new_pwd').focus();
				$(".cnf_new_pwd").addClass("danger_error");
				return false;
			}

			if(new_pwd!=cnf_new_pwd)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Match Error!</strong> New & Confirm password does not match.</div>");
				$(".new_pwd").addClass("danger_error");
				$(".cnf_new_pwd").addClass("danger_error");
				return false;
			}

			if(new_pwd==crr_pwd)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Password Error!</strong> <br/>Current password & new password both are same.<br/> Please enter different new password.</div>");
				$(".new_pwd").addClass("danger_error");
				$(".crr_pwd").addClass("danger_error");
				return false;
			}

			var data = {
				crr_pwd : crr_pwd,
				new_pwd : new_pwd,
				cnf_new_pwd : cnf_new_pwd,
				task : task
			};
			
			$('.form_status').html("");
			$('.loading_img').show();

			$.ajax({
				type:'post',
				dataType:'json',
            	data:data,
            	url:'query/change-password-helper.php',
            	success:function(res)
            	{
            		$('.loading_img').hide();

            		if(res=='success')
            		{
            			$('.form_status').html("<div class='alert alert-success login_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span><strong> Sucess! </strong><br/> Your password updated successfully.</br> Thank you.<center> </div>");
            			return false;
            		}
            		else
            		{
            			$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Error!</strong><br/> "+res+"</center></div>");
            			return false;
            		}
            	}
            });
            return false;
		});
	});
</script>