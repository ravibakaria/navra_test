<?php
	session_start();
  	if(!isset($_SESSION['logged_in']) && ($_SESSION['vendor_user']=='' || $_SESSION['vendor_user']==null))
  	{
		header('Location: login.php');
		exit;
	}
	include("templates/header.php"); 	
?>

<!-- meta info  -->

<title>
  <?php
  $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
  $string = str_replace("-", " ", $a);
  echo $title = ucwords($string);
  ?> -  <?php echo getWebsiteTitle(); ?>
</title>

<meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

<meta name="description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!"/>

<meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, vendor information, vendor lobin, vendor registration" />

<meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
<meta property="og:url" content="<?= $site_url; ?>" />
<meta property="og:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
<meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
<meta property="twitter:url" content="<?= $site_url; ?>" />
<meta property="twitter:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<link rel="stylesheet" href="../css/croppie.css" />

<?php
    include("templates/styles.php"); 
    include("templates/menu.php"); 
?>

<div class="col-md-12">
  <br><br><br><br><br><br>
</div>

<div class="vendor-data">
	<?php
	    $sql_profile_pic = "SELECT * FROM vendor_profile_pic WHERE  vuserid='$vuserid'";
	    $stmt_profile_pic = $link->prepare($sql_profile_pic);
	    $stmt_profile_pic->execute();
	    $count_profile_pic = $stmt_profile_pic->rowCount();
	    $result_profile_pic = $stmt_profile_pic->fetch();

	    $profile_pic = $result_profile_pic['profile_pic'];

	    $profile_pic_path = 'uploads/'.$vuserid.'/profile/'.$profile_pic;

	    $vendor_name = getVendorFirstName($vuserid).' '.getVendorLastName($vuserid);
  	?>

  	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    		<div class="row profile-row">
	    			<div class="text-left col-xs-12 col-sm-12 col-md-6 col-lg-6">
	    				<h1>My Profile Picture</h1>
	    			</div>
	    			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
	                    
	    			</div>
	    		</div>

	    		<br><br>
	    		<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<br/><br/>
							<h2>General Instructions</h2>
							<ul style="list-style-type:circle">
								<li>
									Only files with extention <b><?php echo getAllowedFileAttachmentTypesString();?> </b> is  allowed to upload.
								</li>
								<li>
									Please upload clear & visible images.
								</li>
							</ul>
							
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div class="" align="center">
			  					<div id="uploaded_image">
			  						<?php
										if($count_profile_pic>0)
										{
											echo "<img src='$profile_pic_path' class='rounded img_preview' alt='$vendor_name' >";
										}
										else
										{
											echo "<img src='../images/no_profile_pic.png' class='rounded img_preview' alt='$vendor_name'>";
										}
									?>
			  					</div>
			  				</div>
			  				<br/>
			  				<div class="" align="center">
			  					<div class="row">
				  					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				  						<center><input type="file" name="upload_image" id="upload_image" class="form-control file-upload"/></center>
				  					</div>
				  					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 status">
				  						
				  					</div>
				  				</div>
			  				</div>
						</div>
		  				
			  			

			  			<div id="uploadimageModal" class="modal" role="dialog">
							<div class="modal-dialog">
								<div class="modal-content">
						      		<div class="modal-header">
						        		<button type="button" class="close" data-dismiss="modal" style="margin-top:0px;">&times;</button>
						        		<h4 class="modal-title">Upload & Crop Profile Pic</h4>
						      		</div>
						      		<div class="modal-body">
						        		<div class="row">
						  					<div class="col-md-8 text-center">
												  <div id="image_demo" style="width:350px; margin-top:30px"></div>
						  					</div>
						  					<div class="col-md-4" style="padding-top:30px;">
						  						<br />
						  						<br />
						  						<br/>
												  <button class="btn btn-success crop_image">Crop & Save Image</button>
											</div>
										</div>
						      		</div>
						      		<div class="modal-footer">
						        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						      		</div>
						    	</div>
						    </div>
						</div>
			  		</div>
				</div>


				<br><br>
	    	</div>
	    </div>
	</div>
</div>

</section>
<?php
  	include("templates/footer.php");
?>

<script src="../js/croppie.js"></script>

<script>
	$(document).ready(function(){
		$image_crop = $('#image_demo').croppie({
			enableExif: true,
			viewport: {
				width:300,
				height:300,
				type:'square' //circle
			},
			boundary:{
			width:350,
			height:350
			}
		});

		$('#upload_image').on('change', function(){

		  	var file = $(this).val();
			var ext = file.split('.').pop();
			var img_array = "<?php echo getAllowedFileAttachmentTypesString(); ?>";

			var i = img_array.indexOf(ext);

			if(i > -1) 
			{
			    var reader = new FileReader();
			    reader.onload = function (event) {
			      $image_crop.croppie('bind', {
			        url: event.target.result
			      }).then(function(){
			        console.log('jQuery bind complete');
			      });
			    }
			    reader.readAsDataURL(this.files[0]);
			    $('#uploadimageModal').modal('show');
			} 
			else 
			{
			    $('.status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 15px;'><span class='fa fa-times'></span><strong> Error! </strong> Invalid image type.</div>");
			    return false;
			}
		    
		});

		$('.crop_image').click(function(event){
		    $image_crop.croppie('result', {
		      	type: 'canvas',
		      	size: 'viewport'
		    }).then(function(response){
		      	$.ajax({
			        url:"query/my-profile-picture-helper.php",
			        type: "POST",
			        data:{"image": response},
			        success:function(res)
			        {
			          	$('#uploadimageModal').modal('hide');
			          	$('#uploaded_image').html(res);
			          	$('.status').html("<div class='alert alert-success success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Profile Updated  Successfully.</div>");
			          	$('.success_status').fadeTo(2000, 500).slideUp(500, function(){
							location.reload();
			            });
			        }
		      	});
		    })
		});
	});
</script>