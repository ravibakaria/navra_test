<?php
  session_start();
  if(isset($_SESSION['last_page_accessed']))
  {
    unset($_SESSION['last_page_accessed']);
  }

  if(isset($_SESSION['membership_plan_id']))
  {
    unset($_SESSION['membership_plan_id']);
  }

  if(isset($_SESSION['featured_listing']))
  {
    unset($_SESSION['featured_listing']);
  }
  
  include("layout/header.php");      // Header
?>

<!-- meta info  -->

  <title>
    <?php
    $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
    $string = str_replace("-", " ", $a);
    echo $title = ucwords($string);
    ?>  -  <?php echo getWebsiteTitle(); ?>
  </title>

  <meta name="title" content="<?= $site_title.' - Membership Plans'; ?>" />

  <meta name="description" content="<?= $site_title; ?> - To find Verified Profiles, Register Free!"/>

  <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

  <meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
  <meta property="og:url" content="<?= $site_url; ?>" />
  <meta property="og:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
  <meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
  <meta property="twitter:url" content="<?= $site_url; ?>" />
  <meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

  <link rel="stylesheet" href="css/membership-plans.css" type='text/css' />
<?php
  include("layout/styles.php"); 
  include("layout/menu.php"); 
?>
<section>
  <br/><br/><br/><br/><br/><br/> 
</section>
<?php
    $DefaultCurrency = getDefaultCurrency();
    $DefaultCurrencyCode = getDefaultCurrencyCode($DefaultCurrency);

    $tax_applied_or_not = getTaxAppliedOrNot();

    $featured_listing_activated_or_not = getFeaturedListingActivatedOrNot();
    $featured_listing_price = getFeaturedListingPrice();

    $sql_get_membership_plans ="SELECT * FROM `membership_plan` WHERE id <> '1' AND status='1'";
    $stmt   = $link->prepare($sql_get_membership_plans);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $count=$stmt->rowCount();
?>
  <input type="hidden" class="tax_applied" value="<?php echo $tax_applied_or_not;?>">
  <input type="hidden" class="featured_membership_price" value="<?php echo $featured_listing_price;?>">

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
          <h1>Membership Plans</h1>
          <br>
        </div>
      </div>
      <!--   Membership Plans Start  -->
      <?php
        if($count>0)
        {
          $i = 0;
          foreach ($result as $row) 
          {
            $id = $row['id'];
            $membership_plan_name = $row['membership_plan_name'];
            $number_of_contacts = $row['number_of_contacts'];
            $number_of_duration_months = $row['number_of_duration_months'];

            if($number_of_contacts=='0')
            {
              $display_number_of_contacts = "Unlimited";
            }
            else
            {
              $display_number_of_contacts = $number_of_contacts;
            }

            $price = $row['price'];
            if($i==0 || ($i>0 && $i==4))
            {
              echo "<div class='row'>";
            }
      ?>
            <div class="col-md-3 col-sm-6">
              <div class="pricingTable">
                  <div class="pricingTable-header">
                      <span class="heading">
                          <h2 class="price-head-value"><?= $membership_plan_name ?></h2>
                      </span>
                      <span class="price-value"><span class="month"> <?= $DefaultCurrencyCode ?>:</span> <b class="membership-price-value"><?= $price ?></b></span>
                  </div>

                  <div class="pricingContent">
                      <ul>
                          <li><?= $display_number_of_contacts ?> Contacts/Chats</li>
                          <li><?= $number_of_duration_months ?> Months Validity</li>
                          <li>Unlimited Profile Views</li>
                          <li>Unlimited Expression Of Interest</li>
                          <li>Unlimited Photo Requests</li>
                          <li>Unlimited Profile Sharing</li>
                          <?php
                            if($featured_listing_activated_or_not=='1')
                            {
                          ?>
                          <li>
                            <b>Featured Listing  &nbsp;<input type='checkbox' class='featured_membership' name="<?php echo $featured_listing_price.','.$price; ?>" id="<?php echo $id; ?>"></b> <br/>INR <?php echo $featured_listing_price*$number_of_duration_months; ?> / For <?php echo $number_of_duration_months; ?> Month &nbsp;&nbsp;&nbsp;
                            <span class="badge" data-toggle="tooltip" data-placement="top" title="Featured members profile will be shown above other profiles."><span class="fa fa-exclamation"></span></span>
                          </li>
                          <li>
                            <b class="total_amt_<?php echo $id;?>">Total: <h3>INR: <?php echo $price; ?> <?php if($tax_applied_or_not=='1') { echo " +Tax "; } ?></h3></b>
                          </li>
                          <input type="hidden" class="featured_listing" id="featured_listing_<?php echo $id;?>" value="0">
                          <?php
                            }
                            else
                            {
                              echo "<input type='hidden' class='featured_listing' id='featured_listing_$id' value='0'>";
                            }

                            $featured = "0";   // by default featured is not checked
                            echo "<input type='hidden' class='months_validity_$id' value='$number_of_duration_months'>";

                            $secret_key = $id."*+".$number_of_duration_months."+*".$featured;
                            $secret_key = md5($secret_key);

                            echo "<input type='hidden' class='secret_key_$id' id='secret_key_$id' value='$secret_key'>";
                          ?>
                      </ul>
                  </div><!-- /  CONTENT BOX-->

                  <div class="pricingTable-sign-up">
                      <img src="images/loader/loader.gif" class="img-responsive loading_img_membership_<?php echo $id; ?> centered-loading-image" id='loading_img' alt='loader' style='width:80px; height:80px; display:none;'/>

                      <a href="javascript:void(0)" class="btn btn-block btn-default btn_buy_membership_plan" id='<?php echo $id; ?>'>Buy Now</a>
                  </div><!-- BUTTON BOX-->
              </div>
            </div>

      <?php
            
            if($i>0 && $i==3)
            {
              echo "</div>";
              echo "<br>";
            }
            $i++;
          }
        }
        else
        {
          echo "<div class='row'><h3>No plans available now!</h3></div>";
        }
      ?>
      <!--   Membership Plans end  -->

      <!--   Featured Listing Start  -->
      <?php
        if($featured_listing_activated_or_not=='1')
        {
      ?>
      
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
          <h2>Featured Listing Plan</h2>
          <br>
        </div>
      </div>
      <div class="row">
          <div class="col-md-4 col-sm-6"></div>
          <div class="col-md-4 col-sm-6">
              <div class="pricingTable">
                  <div class="pricingTable-header">
                      <span class="heading">
                          <h2 class="price-head-value">Featured Listing </h2> 
                      </span>
                      <span class="price-value"><span class="month"> <?= $DefaultCurrencyCode ?>:</span> <b class="membership-price-value"><?= $featured_listing_price ?></b><span class="month"> /PER MONTH</span></span>
                  </div>
                  <div class="pricingContent">
                      <ul>
                          <li><strong>Choose Validity : </strong>
                            <select class="no_of_months featured-no-of-moths-select">
                              <?php
                                for($i=1;$i<=12;$i++)
                                {
                                  echo "<option value='".$i."'>".$i." Month</option>";
                                }
                              ?>
                            </select>
                          </li>
                          <li><b class="featured_listing_validity">Featured members profile will be <br/>shown above other profiles.</b></li>
                          <li>
                            <b class="total_featured_amt">Total: <h3>INR: <?php echo $featured_listing_price; ?> <?php if($tax_applied_or_not=='1') { echo " +Tax "; } ?></h3></b>
                          </li>

                          <?php
                              $id = "0";   //No member plan only featured listing
                              $featured = "1";   //Only featured listing
                              $number_of_duration_months = "1"; //default month=1

                              $featured_secret_key = $id."*+".$number_of_duration_months."+*".$featured;
                              $featured_secret_key = md5($featured_secret_key);

                              echo "<input type='hidden' class='featured_secret_key' id='featured_secret_key' value='$featured_secret_key'>";
                          ?>
                      </ul>
                  </div><!-- /  CONTENT BOX-->

                  <div class="pricingTable-sign-up">
                      <img src="images/loader/loader.gif" class='img-responsive loading_img_featured centered-loading-image' id='loading_img' alt='loader' style='width:80px; height:80px; display:none;'/>

                      <a href="javascript:void(0)" class="btn btn-block btn-default btn_buy_featured_listing" id='<?php echo $id; ?>'>Buy Now</a>
                  </div><!-- BUTTON BOX-->
              </div>
          </div>
          <div class="col-md-4 col-sm-6"></div>
      </div>
      <?php
        }
      ?>
      <!--   Featured Listing End  -->

    </div>
   
  <br/><br/>
</section>
<?php
  include "layout/footer.php";
?>

<script>
  $(document).ready(function(){
    

    $('.featured_membership').change(function(){
      var id = $(this).attr('id');
      var membership_price = $(this).attr('name');
      var tax_applied = $('.tax_applied').val();

      $('.btn_buy_membership_plan').attr('disabled',true);
      $('.loading_img_membership_'+id).show();

      if($(this).is(':checked'))
      {
          var featured_listing = "1";
          task = "get_total_plan_price";

          $.ajax({
            type:'post',
            data:'id='+id+'&membership_price='+membership_price+'&task='+task,
            url:'query/membership-plan-helper.php',
            success:function(res)
            {
                $('#featured_listing_'+id).attr('value','1');
                $('.total_amt_'+id).html(res);
                return false;
            }
          });
          
      }
      else
      {
          var featured_listing = "0";
          membership_price = membership_price.split(',');
          $('#featured_listing_'+id).attr('value','0');
          if(tax_applied=='1')
          {
            $('.total_amt_'+id).html('Total:<h3> INR '+membership_price[1]+' +Tax</h3>');
          }
          else
          {
            $('.total_amt_'+id).html('Total:<h3> INR '+membership_price[1]+'</h3>');
          }
      }

      /**************   Secret code generator  **************/

      var task2 = "generate_secret_code_premium_order";
      $.ajax({
          type:'post',
          data:'id='+id+'&featured_listing='+featured_listing+'&task='+task2,
          url:'query/membership-plan-helper.php',
          success:function(res)
          {
              $('#secret_key_'+id).attr('value',res);
              $('.btn_buy_membership_plan').attr('disabled',false);
              $('.loading_img_membership_'+id).hide();
              return false;
          }
        });
      
    });

    $('.btn_buy_membership_plan').click(function(){
      var membership_plan_id = $(this).attr('id');
      var featured_listing = $('#featured_listing_'+membership_plan_id).val();
      var validity = $(".months_validity_"+membership_plan_id).val();
      var secret_key = $(".secret_key_"+membership_plan_id).val();
    
      var url = 'order-summary.php';
      var form = $('<form action="' + url + '" method="get">' +
        '<input type="hidden" name="membership_plan_id" value="' + membership_plan_id + '" />' +
        '<input type="hidden" name="featured_listing" value="' + featured_listing + '" />' +
        '<input type="hidden" name="validity" value="' + validity + '" />' +
        '<input type="hidden" name="secret_key" value="' + secret_key + '" />' +
        '</form>');
      $('body').append(form);
      form.submit();
    });

    $('.no_of_months').change(function(){
      var month = $(this).val();
      var tax_applied = $('.tax_applied').val();
      var featured_membership_price = $('.featured_membership_price').val();
      var total_featured_price;

      total_featured_price = featured_membership_price*month;

      if(tax_applied=='1')
      {
        $('.total_featured_amt').html("Total: <h3>INR: "+total_featured_price+".00  +Tax </h3>");
      }
      else
      {
        $('.total_featured_amt').html("Total: <h3>INR: "+total_featured_price+".00</h3>");
      }

      $('.btn_buy_featured_listing').attr('disabled',true);
      $('.loading_img_featured').show();

      /*************   fetch secret key for featured listing    *************/
        var task = "generate_secret_code_featured_order"
        $.ajax({
          type:'post',
          data:'month='+month+'&task='+task,
          url:'query/membership-plan-helper.php',
          success:function(res)
          {
              $('#featured_secret_key').attr('value',res);
              $('.btn_buy_featured_listing').attr('disabled',false);
              $('.loading_img_featured').hide();
              return false;
          }
        });
    });

      /************    Featured Listing Order    ****************/
      $('.btn_buy_featured_listing').click(function(){
        $(this).attr('disabled',true);
        $('.loading_img_featured').show();
        var membership_plan_id = '0';
        var featured_listing = '1';
        var validity = $('.no_of_months').val();
        var secret_key = $('.featured_secret_key').val();
      
        var url = 'order-summary.php';
        var form = $('<form action="' + url + '" method="get">' +
          '<input type="hidden" name="membership_plan_id" value="' + membership_plan_id + '" />' +
          '<input type="hidden" name="featured_listing" value="' + featured_listing + '" />' +
          '<input type="hidden" name="validity" value="' + validity + '" />' +
          '<input type="hidden" name="secret_key" value="' + secret_key + '" />' +
          '</form>');
        $('body').append(form);
        form.submit();
      });
  });
</script>