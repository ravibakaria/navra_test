<?php
  session_start();
  if(isset($_SESSION['last_page_accessed']))
  {
    unset($_SESSION['last_page_accessed']);
    unset($_SESSION['membership_plan_id']);
    unset($_SESSION['featured_listing']);
    unset($_SESSION['validity']);
    unset($_SESSION['secret_key']);
  }
  include("layout/header.php");      // Header
  include('config/setup-values.php');   //get master setup values
  include('config/email/email_style.php');   //get master setup values
  include('config/email/email_templates.php');   //get master setup values
  include('config/email/email_process.php');

  $WebSiteBasePath = getWebsiteBasePath();
  $sitetitle = getWebsiteTitle();
  $logo_array=array();
  $logoURL = getLogoURL();
  if($logoURL!='' || $logoURL!=null)
  {
      $logoURL = explode('/',$logoURL);

      for($i=1;$i<count($logoURL);$i++)
      {
          $logo_array[] = $logoURL[$i];
      }

      $logo_path = implode('/',$logo_array);
  }

  if($logoURL!='' || $logoURL!=null)
  {
      $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
  }
  else
  {
      $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
  }

  $stmt   = $link->prepare("SELECT * FROM `contact_us`");
  $stmt->execute();
  $result = $stmt->fetch();
  $count=$stmt->rowCount();

  $recaptchaAllowed = getreCaptchaAllowed();
  if($recaptchaAllowed == "1")
  {
    $recaptchaSiteKey=getreCaptchaSiteKey();
    $recaptchaSecretKey=getreCaptchaSecretKey();
  }

  $errorMessage = null;
  $successMessage = null;

  if($recaptchaAllowed == "1")
  {
    $recaptchaSiteKey=getreCaptchaSiteKey();
    $recaptchaSecretKey=getreCaptchaSecretKey();

    if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
    {
      //your site secret key
      $secret = $recaptchaSecretKey;
      $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
      $responseData = json_decode($verifyResponse);
      if($responseData->success)
      {
        if (isset($_POST['fullname']) && $_POST['fullname'] != '' && isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['mobile']) && $_POST['mobile'] != '' && isset($_POST['subject']) && $_POST['subject'] != '' && isset($_POST['message']) && $_POST['message'] )
        {
            $fullname = quote_smart($_POST['fullname']);
            $email = quote_smart($_POST['email']);
            $mobile = quote_smart($_POST['mobile']);
            $subject = quote_smart($_POST['subject']);
            $message = quote_smart($_POST['message']);

            $full_name = $fullname;
            $email_address = $email;
            $enquiry_subject = $subject;

            $enquiry_email = getEnquiryEmail();

            $SocialSharing = getSocialSharingLinks();   // social sharing links
            $primary_color = getPrimaryColor();
            $primary_font_color = getPrimaryFontColor();

          $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$sitetitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

          $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo'),array($WebSiteBasePath,$sitetitle,$logo),$EmailGlobalHeader);  //replace header variables with value

          $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$sitetitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

          $EnquiryEmailMessage = str_replace(array('$site_name','$full_name','$email_address','$mobile','$enquiry_subject','$message','$signature'),array($sitetitle,$full_name,$email_address,$mobile,$enquiry_subject,$message,$GlobalEmailSignature),$EnquiryEmailMessage);  //replace footer variables with value

          $subject = "New Enquiry : ".$sitetitle;
            
          $message  = '<!DOCTYPE html>';
          $message .= '<html lang="en">
            <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <title></title>
            <style type="text/css">'.$EmailCSS.'</style>
            </head>
            <body style="margin: 0; padding: 0;">';
          //echo $message;exit;
          $message .= $EmailGlobalHeader;

          $message .= $EnquiryEmailMessage;

          $message .= $EmailGlobalFooter;
          
          $mailto = $enquiry_email;
          //$mailto = $email_address;
          $mailtoname = 'user';

          $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

          if($emailResponse == 'success')
          {
            $successMessage = "success";
          }
          else
          {
            $errorMessage = "Error in sending email. Please try after some time.<br/> Thank You.";
          }
        }
      }
    }
  }
  else
  {
    if (isset($_POST['fullname']) && $_POST['fullname'] != '' && isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['mobile']) && $_POST['mobile'] != '' && isset($_POST['subject']) && $_POST['subject'] != '' && isset($_POST['message']) && $_POST['message'] )
    {
        $fullname = quote_smart($_POST['fullname']);
        $email = quote_smart($_POST['email']);
        $mobile = quote_smart($_POST['mobile']);
        $subject = quote_smart($_POST['subject']);
        $message = quote_smart($_POST['message']);
        
        $full_name = $fullname;
        $email_address = $email;
        $enquiry_subject = $subject;

        $enquiry_email = getEnquiryEmail();

        $SocialSharing = getSocialSharingLinks();   // social sharing links
        $primary_color = getPrimaryColor();
        $primary_font_color = getPrimaryFontColor();

        $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$sitetitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

        $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo'),array($WebSiteBasePath,$sitetitle,$logo),$EmailGlobalHeader);  //replace header variables with value

        $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$sitetitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

        $EnquiryEmailMessage = str_replace(array('$site_name','$full_name','$email_address','$mobile','$enquiry_subject','$message','$signature'),array($sitetitle,$full_name,$email_address,$mobile,$enquiry_subject,$message,$GlobalEmailSignature),$EnquiryEmailMessage);  //replace footer variables with value

      $subject = "New Enquiry : ".$sitetitle;
        
      $message  = '<!DOCTYPE html>';
      $message .= '<html lang="en">
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title></title>
        <style type="text/css">'.$EmailCSS.'</style>
        </head>
        <body style="margin: 0; padding: 0;">';
      //echo $message;exit;
      $message .= $EmailGlobalHeader;

      $message .= $EnquiryEmailMessage;

      $message .= $EmailGlobalFooter;
      
      $mailto = $enquiry_email;
      //$mailto = $email_address;
      $mailtoname = 'user';

      $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

      if($emailResponse == 'success')
      {
        $successMessage = "success";
      }
      else
      {
        $errorMessage = "Error in sending email. Please try after some time.<br/> Thank You.";
      }
        
    }
  }
?>

  <!-- meta info  -->

  <title>
    <?php
    $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
    $string = str_replace("-", " ", $a);
    echo $title = ucwords($string);
    ?>  -  <?php echo getWebsiteTitle(); ?>
  </title>

  <meta name="title" content="<?= $site_title.' - Contact Us'; ?>" />

  <meta name="description" content="<?= $site_title; ?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!"/>

  <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

  <meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
  <meta property="og:url" content="<?= $site_url; ?>" />
  <meta property="og:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
  <meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
  <meta property="twitter:url" content="<?= $site_url; ?>" />
  <meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

  <script src="js/jquery.js"></script>

  <script>
    function onSubmit(token)
    {
        document.getElementById("frmLogin").submit();
    }

    function validate(event) 
    {
      event.preventDefault();
      var fullname = $('.fullname').val();
      var email = $('.email').val();
      var mobile = $('.mobile').val();
      var subject = $('.subject').val();
      var message = $('.message').val();
      var recaptchaAllowed = $('.recaptchaAllowed').val();
      var user = $('.user').val();
      var task = "contact-us-request";
      var empty_success=0;
        
        if(fullname=='' || fullname==null)
        {
          $('.fullname').addClass('danger_error');
          empty_success = empty_success+1;
        }        

        /* Email Validation */
        if(email=='' || email==null)
        {
          $('.email').addClass('danger_error');
          empty_success = empty_success+1;
        }

        function validateEmail($email) {
            var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            return emailReg.test( $email );
        }

        if(email!='' && !validateEmail(email)) 
        { 
            $('.email').addClass('danger_error');
            empty_success=empty_success+1;
            $('.contact-us-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong> Invalid! </strong> Email id not valid!. Please enter valid email id.</div>");
            return false;
        }

        if(mobile=='' || mobile==null)
        {
          $('.mobile').addClass('danger_error');
          empty_success = empty_success+1;
        }
        if(subject=='' || subject==null)
        {
          $('.subject').addClass('danger_error');
          empty_success = empty_success+1;
        }
        if(message=='' || message==null)
        {
          $('.message').addClass('danger_error');
          empty_success = empty_success+1;
        }

        if(empty_success>0)
        {
          $('.contact-us-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong> Empty! </strong> Please enter all required data & then click on send.</div>");
          return false;
        }

        $('.btn-send-message').attr('disabled',true);
      //recaptcha validation 
      if(recaptchaAllowed=='1')
      {
        grecaptcha.execute();
      }
      else
      {
        document.getElementById("frmLogin").submit();
      }

      $('.loading_img').show();
      $('.contact-us-status').html("");
    }

    function onload() 
    {
        var element = document.getElementById('recaptcha-submit');
        element.onclick = validate;
    }
  </script>

<?php
  include("layout/styles.php"); 
  include("layout/menu.php"); 
?>

<!-- Google recaptcha -->
<?php
  if($recaptchaAllowed == "1")
  {
    echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
  }
?>

<section class="blank_section">
  <br/><br/><br/>
</section>
<!-- Main Page Content Shown Here  -->
  <div class="row contact-us-page">
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
      <!-- Section Titile -->
      <div class="col-md-12 wow animated fadeInLeft" data-wow-delay=".2s">
          <h1>Contact Us</h1>
          <br>
      </div>

      <div class="">

        <?php
        if($count>0)
        {
          $enquiry_email = $result['enquiry_email'];
          $companyName = $result['companyName'];
          $street = $result['street'];
          $city = $result['city'];
          $state = $result['state'];
          $country = $result['country'];
          $postalCode = $result['postalCode'];
          $phonecode = $result['phonecode'];
          $phone = $result['phone'];
          $companyEmail = $result['companyEmail'];
          $companyURL = $result['companyURL'];

          $city_name = getUserCityName($city);
          $state_name = getUserStateName($state);
          $country_name = getUserCountryName($country);
        ?>
        <br/>
        <?php
        if($companyName!='' || $companyName!=null)
          {
          ?>
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
              <b>Company Name</b>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
              <?php echo $companyName; ?>
            </div>
          </div>

          <br/>

          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
              <b>Address</b>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
              <?php echo $street ;?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">

            </div>
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
            <?php echo $city_name.", ".$state_name.", ".$postalCode." ".$country_name; ?>
            </div>
          </div>

          <br/>              

          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
              <b>Phone Number</b>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
              <?php echo "+".$phonecode." - ".$phone; ?>
            </div>
          </div>

          <br/>

          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
              <b>Email Address</b>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
              <?php echo "<a href='mailto:$companyEmail'>$companyEmail</a>"; ?>
            </div>
          </div>

          <?php
          }
        }
        ?>
        <hr/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
      <div class="row">
                  <!-- Section Titile -->
                  <div class="col-md-12 wow animated fadeInLeft" data-wow-delay=".2s">
                      <h2>Enquiry Form</h2>
                  </div>
              </div>
              <br/>
                  <form name="frmLogin" id="frmLogin" autocomplete="off" method="post"  action="<?php echo $_SERVER['PHP_SELF'];?>">
                    <div class="form-group">
                      <?php
                        if ($errorMessage != '' || $errorMessage != null)
                        {
                          echo "<div class='alert alert-danger alert-dismissible order-detail-alert-dismissible col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
                          echo "<a href='#' class='close' data-dismiss='alert' aria-label='close' style='top: 0px;right: 0px;'>&times;</a>";
                          echo "<strong>Error!</strong> $errorMessage";
                          echo "</div>";
                        }

                        if ($successMessage != '' || $successMessage != null)
                        {
                          echo "<div class='alert alert-success alert-dismissible order-detail-alert-dismissible col-sm-12' style='padding: 10px; margin-bottom: 10px;'>";
                          echo "<a href='#' class='close' data-dismiss='alert' aria-label='close' style='top: 0px;right: 0px;'>&times;</a>";
                          echo "<strong>Success!</strong> Your enquiry sent successfully.Thank You.";
                          echo "</div>";
                        }
                      ?>
                    </div>

                    <!-- Name -->
                    <div class="form-group label-floating">
                      <label class="control-label">Full Name</label>
                      <input class="form-control fullname" type="text" name="fullname">
                      <div class="fullname-error"></div>
                    </div>
                    <!-- email -->
                    <div class="form-group label-floating">
                      <label class="control-label" for="email">Email Address</label>
                      <input class="form-control email" id="email" type="email" name="email">
                      <div class="email-error"></div>
                    </div>
                    <!-- mobile -->
                    <div class="form-group label-floating">
                      <label class="control-label" for="mobile">Mobile Number</label>
                      <input class="form-control mobile" id="mobile" type="text" name="mobile">
                      <div class="mobile-error"></div>
                    </div>
                    <!-- Subject -->
                    <div class="form-group label-floating">
                      <label class="control-label">Subject</label>
                      <input class="form-control subject" id="subject" type="text" name="subject">
                      <div class="subject-error"></div>
                    </div>
                    <!-- Message -->
                    <div class="form-group label-floating">
                        <label for="message" class="control-label">Message</label>
                        <textarea class="form-control message" rows="3" id="message" name="message"></textarea>
                        <div class="message-error"></div>
                    </div>                    

                    <input type="hidden" class="recaptchaAllowed" value="<?php echo $recaptchaAllowed;?>">

                    <div class="form-group label-floating centered-data">
                        <center><img src="images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' alt='loader' style='width:60px; height:60px; display:none;'/></center>
                        <div class="contact-us-status"></div>
                        <div class="phonenumber_error"></div>
                    </div>
                    <!-- Form Submit -->
                    <div class="form-submit mt-5">
                        <button type="submit" name="contact_us_btn"  class="btn btn-primary btn-lg btn-block btn-send-message website-button" id="recaptcha-submit"><span class="fa fa-envelope"></span>&nbsp;&nbsp;Send</button>
                        <div class="clearfix"></div>
                    </div>
                    <?php
                      if($recaptchaAllowed == "1")
                      {
                        echo "<div class='form-group recaptcha-div'>
                          <div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
                        </div>";
                      }
                    ?>
                    
                    <script>onload();</script>
                </form>
            </div>
    </div>
  </div>
<br/><br/>
</body>
</section>
<?php
  include "layout/footer.php";
?>

<?php
    $sql_AddThisScript = "SELECT social_sharing_display,social_sharing_data FROM `social_sharing`";
    $stmt= $link->prepare($sql_AddThisScript); 
    $stmt->execute();
    $count=$stmt->rowCount();
    $result = $stmt->fetch();
    $social_sharing_display = $result['social_sharing_display'];
    $social_sharing_data = $result['social_sharing_data'];

    if($social_sharing_display=='1')
    {
        if($social_sharing_data!='' || $social_sharing_data!=null)
        {
            $social_sharing_data = file_get_contents($websiteBasePath.'/'.$social_sharing_data);
            echo "$social_sharing_data";
        }
    }
?>

<script>
  $(document).ready(function(){
    
    /* empty error message  */
    $('.fullname, .email, .mobile, .subject, .message').click(function(){
          $('.contact-us-status').html("");
      });

    /* empty danger class message  */
    $('.fullname').click(function(){
        $('.fullname').removeClass("danger_error");
    });

    $('.email').click(function(){
        $('.email').removeClass("danger_error");
    });

    $('.mobile').click(function(){
        $('.mobile').removeClass("danger_error");
    });

    $('.subject').click(function(){
        $('.subject').removeClass("danger_error");
    });

    $('.message').click(function(){
        $('.message').removeClass("danger_error");
    });

    /*   Prevent entering charaters in mobile & phone number   */
    $(".mobile").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
        {
            //display error message
            $('.phonenumber_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Albhabets not allowed. Enter Digits only.</div>").show().fadeOut(3000);
            return false;
        }
    });
  });
</script>

</body>
</html>