<?php
  	session_start();
  	if(isset($_SESSION['last_page_accessed']))
	{
		unset($_SESSION['last_page_accessed']);
	    unset($_SESSION['membership_plan_id']);
	    unset($_SESSION['featured_listing']);
	    unset($_SESSION['validity']);
	    unset($_SESSION['secret_key']);
	}
  	include("layout/header.php"); 
  	$siteName = getWebsiteTitle();
	$siteTagline = getWebSiteTagline();
	$site_url = getWebsiteBasePath();
	$site_email = getWebSiteEmail();

	$logo=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo[] = $logoURL[$i];
		}

		$logo = implode('/',$logo);
	}

  	$search_code = $_GET['category'];
	$search_code_val = explode('-',$search_code);
	$search_type = $search_code_val[0];
	$search_value = $search_code_val[1];
?>

 	 <!-- meta info  -->

  	<title><?php echo getWebsiteTitle().' - '.getWebSiteTagline();?></title>

 	<meta name="description" content="Matrimonial Profiles of <?= $siteName; ?> members search by <?= $search_value; ?>. If you are looking for Bride or Groom then we have a perfect match for you. Search matrimonial profiles by your desired <?= $search_value; ?>."/>

 	<meta name="keywords" content="<?php echo $siteName;?>, <?php echo $siteTagline; ?>, Matrimonial profile search by <?php echo $search_value; ?>">

 	<meta property="og:title" content="<?php echo $siteName.' - '.$siteTagline;?>" />
  	<meta property="og:url" content="<?= $site_url; ?>" />
  	<meta property="og:description" content="<?php echo $siteName;?> Profile Search by <?= $search_value; ?> helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  	<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
  	<meta property="twitter:title" content="<?= $siteName.' - '.$siteTagline; ?>" />
  	<meta property="twitter:url" content="<?= $site_url; ?>" />
  	<meta property="twitter:description" content="<?php echo $siteName;?> Profile Search by <?= $search_value; ?> helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  	<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
  include("layout/styles.php"); 
  include("layout/menu.php"); 
?>
<br/><br/><br/>
	<section class="about clearfix page-header-search">
	    <div class="row">
	    	<div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
	    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
	    			
	    		</div>
	    	</div>
	    	<div class='col-xs-12 col-sm-12 col-md-8 col-lg-8'>
	    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
	    			<div class='leaderboard-ad'>
			          	<?php
			            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
			            	if($display_leaderboard_ad=='1')
			            	{
			              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
			            	}
			          	?>
			        </div>
	    		</div>
	    	</div>
	    	<div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
	    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
	    			
	    		</div>
	    	</div>
	    </div>
	</section>

	<section class="about clearfix" id="about">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
				<div class="row">
			    	<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
						<?php
							$search_code = $_GET['category'];
							$search_code_val = explode('-',$search_code);
							$search_type = $search_code_val[0];
							$search_value = $search_code_val[1];

							if($search_value=='religion')
							{
								$sql="SELECT * FROM religion WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By  Religion </h1>";
								$search_url_val = "Religion";
							}
							else
							if($search_value=='caste')
							{
								$sql="SELECT * FROM caste WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By  Caste </h1>";
								$search_url_val = "Caste";
							}
							else
							if($search_value=='mothertongue')
							{
								$sql="SELECT * FROM mothertongue WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By  Mother Tongue </h1>";
								$search_url_val = "MotherTongue";
							}
							else
							if($search_value=='education')
							{
								$sql="SELECT * FROM educationname WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By  Education </h1>";
								$search_url_val = "Education";
							}
							else
							if($search_value=='profession')
							{
								$sql="SELECT * FROM employment WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By  Profession </h1>";
								$search_url_val = "Occupation";
							}
							else
							if($search_value=='maritalstatus')
							{
								$sql="SELECT * FROM maritalstatus WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By  Marital Status </h1>";
								$search_url_val = "MaritalStatus";
							}
							else
							if($search_value=='familyvalue')
							{
								$sql="SELECT * FROM familyvalue WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By  Family Value </h1>";
								$search_url_val = "FamilyValue";
							}
							else
							if($search_value=='familytype')
							{
								$sql="SELECT * FROM familytype WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By  Family Type </h1>";
								$search_url_val = "FamilyType";
							}
							else
							if($search_value=='familystatus')
							{
								$sql="SELECT * FROM familystatus WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By  Family Status";
								$search_url_val = "FamilyStatus";
							}
							else
							if($search_value=='bodytype')
							{
								$sql="SELECT * FROM bodytype WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By Body Type</h1>";
								$search_url_val = "BodyType";
							}
							else
							if($search_value=='complexion')
							{
								$sql="SELECT * FROM complexion WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By Complexion</h1>";
								$search_url_val = "Complexion";
							}
							else
							if($search_value=='eatinghabbit')
							{
								$sql="SELECT * FROM eathabbit WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By Eating Habbit</h1>";
								$search_url_val = "EatingHabbit";
							}
							else
							if($search_value=='smokinghabbit')
							{
								$sql="SELECT * FROM smokehabbit WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By Smoking Habbit</h1>";
								$search_url_val = "SmokingHabbit";
							}
							else
							if($search_value=='drinkinghabbit')
							{
								$sql="SELECT * FROM drinkhabbit WHERE status='1' ORDER BY name";
								$heading = "<h1>Search By Drinking Habbit</h1>";
								$search_url_val = "DrinkingHabbit";
							}

							$stmt=$link->prepare($sql);
							$stmt->execute();
							$result = $stmt->fetchAll();
							echo "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>$heading</div>";
							
							foreach ($result as $row) 
							{
								$category_id = $row['id']; 
								$category_name = $row['name']; 

								$link_url = str_replace(' ','_',$category_name);
								$link_url_search = 'users/Search-'.$search_url_val.'-'.$link_url.'.html';

								echo "<a href='$link_url_search'>";
									echo "<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4'>
							                <blockquote class='info'>
							                  <p>$category_name</p>
							                </blockquote>
							            </div>";
						        echo "</a>";
							}
							
						?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<div class='skyscrapper-ad'>
		          	<?php
	            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
		            	if($display_skyscrapper_ad=='1')
		            	{
		              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
		            	}
		          	?>
		        </div>
			</div>
		</div>
	    
	</section>
	<section class="about clearfix">
	    <div class="row">
	    	<div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
	    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
	    			
	    		</div>
	    	</div>
	    	<div class='col-xs-12 col-sm-12 col-md-8 col-lg-8'>
	    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
	    			<div class='leaderboard-ad'>
			          	<?php
			            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
			            	if($display_leaderboard_ad=='1')
			            	{
			              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
			            	}
			          	?>
			        </div>
	    		</div>
	    	</div>
	    	<div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
	    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
	    			
	    		</div>
	    	</div>
	    </div>
	</section>
</section>
<?php
  include("layout/footer.php");
?>
</body>

</html>