<?php
  session_start();
  if(isset($_SESSION['last_page_accessed']))
  {
    unset($_SESSION['last_page_accessed']);
    unset($_SESSION['membership_plan_id']);
    unset($_SESSION['featured_listing']);
    unset($_SESSION['validity']);
    unset($_SESSION['secret_key']);
  }
  include("layout/header.php");      // Header

    $recaptchaAllowed = getreCaptchaAllowed();

    if($recaptchaAllowed == "1")
    {
      $recaptchaSiteKey=getreCaptchaSiteKey();
    }

    $stmt   = $link->prepare("SELECT * FROM `contact_us`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

?>

  <!-- meta info  -->

  <title>
    <?php
    $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
    $string = str_replace("-", " ", $a);
    echo $title = ucwords($string);
    ?>  -  <?php echo getWebsiteTitle(); ?>
  </title>

  <meta name="title" content="<?= $site_title.' - Contact Us'; ?>" />

  <meta name="description" content="<?= $site_title; ?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!"/>

  <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

  <meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
  <meta property="og:url" content="<?= $site_url; ?>" />
  <meta property="og:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
  <meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
  <meta property="twitter:url" content="<?= $site_url; ?>" />
  <meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
  include("layout/styles.php"); 
  include("layout/menu.php"); 
?>
<?php
  if($recaptchaAllowed == "1")
  {
    echo "<script src='https://www.google.com/recaptcha/api.js'></script>";
  }
?>

<section>
  <br/><br/><br/> 
</section>
<!-- Main Page Content Shown Here  -->
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contact-us">
       <div class="row common-row-div">
            <!-- Section Titile -->
            <br/>
            <div class="col-md-12 wow animated fadeInLeft" data-wow-delay=".2s">
                <h1>Contact Us</h1>
            </div>
        </div>
        <div class="row common-row-div">
            <!-- Section Titile -->
            <div class="col-md-6 mt-3 contact-widget-section2 wow animated fadeInLeft" data-wow-delay=".1s">
              
              <?php
                if($count>0)
                {
                    $enquiry_email = $result['enquiry_email'];
                    $companyName = $result['companyName'];
                    $street = $result['street'];
                    $city = $result['city'];
                    $state = $result['state'];
                    $country = $result['country'];
                    $postalCode = $result['postalCode'];
                    $phonecode = $result['phonecode'];
                    $phone = $result['phone'];
                    $companyEmail = $result['companyEmail'];
                    $companyURL = $result['companyURL'];

                    $city_name = getUserCityName($city);
                    $state_name = getUserStateName($state);
                    $country_name = getUserCountryName($country);
              ?>
              <br/>
              <?php
                if($companyName!='' || $companyName!=null)
                {
              ?>
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                  <b>Company Name</b>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
                  <?php echo $companyName; ?>
                </div>
              </div>

              <br/>

              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                  <b>Address</b>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
                  <?php echo $street ;?>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                  
                </div>
                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
                  <?php echo $city_name.", ".$state_name.", ".$postalCode." ".$country_name; ?>
                </div>
              </div>

              <br/>              

              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                  <b>Phone Number</b>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
                  <?php echo "+".$phonecode." - ".$phone; ?>
                </div>
              </div>

              <br/>

              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                  <b>Email Address</b>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
                  <?php echo "<a href='mailto:$companyEmail'>$companyEmail</a>"; ?>
                </div>
              </div>

              <?php
                  }
                }
              ?>
              <hr/>
            </div>

            <!-- contact form -->
            <div class="col-md-6 wow animated fadeInRight" data-wow-delay=".2s">
              <div class="row">
                  <!-- Section Titile -->
                  <div class="col-md-12 wow animated fadeInLeft" data-wow-delay=".2s">
                      <h2>Enquiry Form</h2>
                  </div>
              </div>
              <br/>
                  <form>
                    <!-- Name -->
                    <div class="form-group label-floating">
                      <label class="control-label">Full Name</label>
                      <input class="form-control fullname" type="text" name="fullname">
                      <div class="fullname-error"></div>
                    </div>
                    <!-- email -->
                    <div class="form-group label-floating">
                      <label class="control-label" for="email">Email Address</label>
                      <input class="form-control email" id="email" type="email" name="email">
                      <div class="email-error"></div>
                    </div>
                    <!-- mobile -->
                    <div class="form-group label-floating">
                      <label class="control-label" for="mobile">Mobile Number</label>
                      <input class="form-control mobile" id="mobile" type="text" name="mobile">
                      <div class="mobile-error"></div>
                    </div>
                    <!-- Subject -->
                    <div class="form-group label-floating">
                      <label class="control-label">Subject</label>
                      <input class="form-control subject" id="subject" type="text" name="subject">
                      <div class="subject-error"></div>
                    </div>
                    <!-- Message -->
                    <div class="form-group label-floating">
                        <label for="message" class="control-label">Message</label>
                        <textarea class="form-control message" rows="3" id="message" name="message"></textarea>
                        <div class="message-error"></div>
                    </div>

                    <div class='text-div' style='display:none;'>
                        <input type='text' name='user' class='user'>
                    </div>

                    <?php
                      if($recaptchaAllowed == "1")
                      {
                        echo "<div class='row'>
                          <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                            <div class='g-recaptcha' data-sitekey='$recaptchaSiteKey'></div>
                          </div>
                        </div>";
                      }
                    ?>
                    <input type="hidden" class="recaptchaAllowed" value="<?php echo $recaptchaAllowed;?>">

                    <div class="form-group label-floating centered-data">
                        <img src="images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' alt='loader' style='width:60px; height:60px; display:none;'/>
                        <div class="contact-us-status"></div>
                        <div class="phonenumber_error"></div>
                    </div>
                    <!-- Form Submit -->
                    <div class="form-submit mt-5">
                        <button class="btn btn-common btn-send-message website-button"><span class="fa fa-envelope"></span> Send</button>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
  <br/><br/>
</section>
<?php
  include "layout/footer.php";
?>

<?php
    $sql_AddThisScript = "SELECT social_sharing_display,social_sharing_data FROM `social_sharing`";
    $stmt= $link->prepare($sql_AddThisScript); 
    $stmt->execute();
    $count=$stmt->rowCount();
    $result = $stmt->fetch();
    $social_sharing_display = $result['social_sharing_display'];
    $social_sharing_data = $result['social_sharing_data'];

    if($social_sharing_display=='1')
    {
        if($social_sharing_data!='' || $social_sharing_data!=null)
        {
            $social_sharing_data = file_get_contents($websiteBasePath.'/'.$social_sharing_data);
            echo "$social_sharing_data";
        }
    }
?>

<script>
  $(document).ready(function(){
    $(".text-div").hide();

    /* empty error message  */
    $('.fullname, .email, .mobile, .subject, .message').click(function(){
          $('.contact-us-status').html("");
      });

    /* empty danger class message  */
    $('.fullname').click(function(){
        $('.fullname').removeClass("danger_error");
    });

    $('.email').click(function(){
        $('.email').removeClass("danger_error");
    });

    $('.mobile').click(function(){
        $('.mobile').removeClass("danger_error");
    });

    $('.subject').click(function(){
        $('.subject').removeClass("danger_error");
    });

    $('.message').click(function(){
        $('.message').removeClass("danger_error");
    });

    /*   Prevent entering charaters in mobile & phone number   */
    $(".mobile").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
        {
            //display error message
            $('.phonenumber_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Albhabets not allowed. Enter Digits only.</div>").show().fadeOut(3000);
            return false;
        }
    });

    $('.btn-send-message').click(function(){
      var fullname = $('.fullname').val();
      var email = $('.email').val();
      var mobile = $('.mobile').val();
      var subject = $('.subject').val();
      var message = $('.message').val();
      var recaptchaAllowed = $('.recaptchaAllowed').val();
      var user = $('.user').val();
      var task = "contact-us-request";
      var empty_success=0;
        
        if(fullname=='' || fullname==null)
        {
          $('.fullname').addClass('danger_error');
          empty_success = empty_success+1;
        }
        if(email=='' || email==null)
        {
          $('.email').addClass('danger_error');
          empty_success = empty_success+1;
        }
        if(mobile=='' || mobile==null)
        {
          $('.mobile').addClass('danger_error');
          empty_success = empty_success+1;
        }
        if(subject=='' || subject==null)
        {
          $('.subject').addClass('danger_error');
          empty_success = empty_success+1;
        }
        if(message=='' || message==null)
        {
          $('.message').addClass('danger_error');
          empty_success = empty_success+1;
        }

        if(empty_success>0)
        {
          $('.contact-us-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong> Empty! </strong> Please enter all required data & then click on send.</div>");
          return false;
        }

          /* recaptcha validation */
          if(recaptchaAllowed=='1')
          {
            var response = grecaptcha.getResponse();
            if (!response) 
            {
              $('.contact-us-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> Please verify you are not robot..</div>");
              return false; 
            }
          }

        $('.loading_img').show();

        data = 'fullname='+fullname+'&email='+email+'&mobile='+mobile+'&subject='+subject+'&message='+message+'&user='+user+'&task='+task;

        $('.btn-send-message').attr('disabled',true);

        $.ajax({
          type:'post',
          data:data,
          url:'query/contact_us_helper.php',
          success:function(res)
          {
            $('.loading_img').hide();
            if(res=='success')
            {
              $('.btn-send-message').attr('disabled','true');
              $('.contact-us-status').html("<div class='alert alert-success centered-data' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'> Email Sent Successfully.<br/>Thank You.</div>");
            }
            else
            {
              $('.btn-send-message').attr('disabled',false);
              $('.contact-us-status').html("<div class='alert alert-danger centered-data' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error!</strong> "+res+"</div>");
              return false;
            }
          }
        });
    });
  });
</script>

</body>
</html>