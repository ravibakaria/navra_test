<?php
  session_start();
  include("layout/header.php"); 
?>

<!-- meta info  -->

    <title><?php echo $site_title.' - '.$site_tagline;?></title>

    <meta name="title" content="<?= $site_title.' - '.$site_tagline; ?>" />

    <meta name="description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!"/>

    <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, vendor information, vendor lobin, vendor registration" />

    <meta property="og:title" content="<?php echo $site_title.' - '.$site_tagline;?>" />
    <meta property="og:url" content="<?= $site_url; ?>" />
    <meta property="og:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
    <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
    <meta property="twitter:title" content="<?= $site_title.' - '.$site_tagline; ?>" />
    <meta property="twitter:url" content="<?= $site_url; ?>" />
    <meta property="twitter:description" content="<?php echo $site_title;?> Vendor information  To find Verified Vendor details for wedding process - Register Free!" />
    <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
    include("layout/styles.php"); 
    include("layout/menu.php"); 
?>

<div class="col-md-12">
  <br><br><br><br><br><br>
</div>

<div class="">
  <!-- Section: intro -->
  <section id="vendor-intro1" class="vendor-intro1 centered-data">
      <h1>* Wedding Directory *</h1>
      <center><hr class="dotted" style="width:50%;"></center>
      <div class="row profile-filter-div common-row-div">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php
              $query  = "SELECT * FROM `membercategory` WHERE status='1' ORDER BY `name` ASC";
              $stmt   = $link->prepare($query);
              $stmt->execute();
              $result = $stmt->fetchAll();
              foreach( $result as $row )
              {
                $category_name =  $row['name'];
                $category_id = $row['id']; 
                $category_slug = $row['slug']; 

                $web_link = 'directory/'.$category_slug;
                echo "<a href='$web_link'>";
                  echo "<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
                        <blockquote class='info' style='background-color:#f0f0f0;'>
                          <p align='justify'><b> $category_name</b></p>
                        </blockquote>
                    </div>";
                echo "</a>";
              }
            ?>
        </div>
      </div>
      
  </section>
  <!-- /Section: intro -->



</div>
</section>
<?php
  include("layout/footer.php");
?>

</body>
</html>