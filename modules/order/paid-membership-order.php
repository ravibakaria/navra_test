<?php
	$sql_get_membership_plans ="SELECT * FROM `membership_plan` WHERE id <> '1' AND status='1' AND id='$membership_plan_id'";
    $stmt   = $link->prepare($sql_get_membership_plans);
    $stmt->execute();
    $result = $stmt->fetch();
    
    $membership_plan_name = $result['membership_plan_name'];
    $number_of_contacts = $result['number_of_contacts'];
    if($number_of_contacts=='0')
    {
    	$number_of_contacts="Unlimited";
    }

    $number_of_duration_months = $result['number_of_duration_months'];

    $price = $result['price'];

    //Featured listing price
    if($featured_listing=='1')
    {
    	$featured_listing_price = getFeaturedListingPrice();
    	$featured_listing_price = $featured_listing_price*$number_of_duration_months;
    	$featured_listing_price = $featured_listing_price.".00";

    	//  PLAN, FEATURED, NUMBER OF MONTHS
    	$product_name = $membership_plan_name.'_featured_'.$number_of_duration_months;
    }
   	else
   	{
   		$featured_listing_price = 0;
   		$product_name = $membership_plan_name.'_nonfeatured_'.$number_of_duration_months;
   	}

   	//echo $featured_listing_price;
    $total_price = $price+$featured_listing_price;

    $tax_applied_or_not = getTaxAppliedOrNot();
    $tax_name = getTaxName();
    $tax_percent = getTaxPercent();

    if($tax_applied_or_not=='1')
    {
    	$tax = number_format(round(($total_price * $tax_percent)/100,2),2,'.','');

    	$total_price_tax_included = number_format(round($total_price+$tax,2),2,'.','');
    }
    else
    {
    	$total_price_tax_included = number_format(round($total_price,2),2,'.','');
    }
?>
<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
				<h1>ORDER SUMMARY</h1>
				<br>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="alert alert-danger alert-dismissible order-detail-alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Note:</strong><br/>
					Please Verify below details before purchasing membership plan.<br/>
					If all the details are correct then proceed for payment.<br/>
					And if below details are not correct then email us at <a href="<?php echo $adminEmail;?>" target="_top"><?php echo $adminEmail;?></a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
		</div>

		<div class="row">
	        <div class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3 order-detail-well">
	            <div class="row">
	                <div class="col-xs-12 col-sm-12 col-md-12">
	                    <address>
	                        <strong><?php echo $user_name; ?></strong>
	                        <br>
	                        Email: <?php echo $email; ?>
	                        <br>
	                        Profile Id: <?php echo $profileId; ?>
	                        <br>
	                        <abbr title="Phone">Phone:</abbr> <?php echo $phone; ?>
	                    </address>
	                </div>
	            </div>
	            <div class="row order-detail-table-row">
	                <table class="table table-hover">
	                    <thead>
	                        <tr>
	                            <th>Serial Number</th>
	                            <th>Product</th>
	                            <th>Contacts</th>
	                            <th class="text-center">Validity</th>
	                            <th class="text-right">Price</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr>
	                            <td class="col-md-1" style="text-align: center"> 1 </td>
	                            <td class="col-md-5"><em><?php echo $membership_plan_name;?> Membership Plan</em></h4></td>
	                            <td class="col-md-1 text-center"><?php echo $number_of_contacts;?></td>
	                            <td class="col-md-3 text-center"><?php echo $number_of_duration_months;?> Months</td>
	                            <td class="col-md-1 text-right"><?php echo $price;?></td>
	                        </tr>
	                        <?php
	                        	if($featured_listing=='1')
	                        	{
	                        ?>
	                        <tr>
	                            <td class="col-md-1" style="text-align: center"> 2 </td>
	                            <td class="col-md-5"><em>Featured Listing</em></h4></td>
	                            <td></td>
	                            <td class="col-md-3 text-center"><?php echo $number_of_duration_months;?> Months</td>
	                            <td class="col-md-1 text-right"><?php echo $featured_listing_price;?></td>
	                        </tr>
	                        <?php
	                        	}
	                        ?>
	                        <tr>
	                            <td>   </td>
	                            <td>   </td>
	                            <td>   </td>
	                            <td class="text-right">
	                            <p>
	                                <strong>Subtotal : </strong>
	                            </p>
	                            
                                <?php 
                                	if($tax_applied_or_not=='1')
                                	{
                                		echo "<p>";
                                			echo "<strong>".$tax_name." ".$tax_percent."% :</strong>";
                                		echo "</p>";
                                	}
                                ?>
	                                	
	                            </td>
	                            <td class="text-right">
	                            <p>
	                                <strong><?php echo $total_price;?>.00</strong>
	                            </p>
	                            <?php 
                                	if($tax_applied_or_not=='1')
                                	{
                                		echo "<p>";
                                			echo "<strong>".$tax." </strong>";
                                		echo "</p>";
                                	}
                                ?>
                            	</td>
	                        </tr>
	                        <tr>
	                            <td>   </td>
	                            <td>   </td>
	                            <td>   </td>
	                            <td class="text-right"><h4><strong>Total: </strong></h4></td>
	                            <td class="text-center text-right"><h4><strong><?php echo $total_price_tax_included;?></strong></h4></td>
	                        </tr>
	                    </tbody>
	                </table>
	                <form method="post" action="select-payment-method.php">
	                	<!--   All required values   -->
	                	<input type="hidden" name="user_name" value="<?php echo $user_name;?>">
	                	<input type="hidden" name="email" value="<?php echo $email;?>">
	                	<input type="hidden" name="phone" value="<?php echo $mobile;?>">
	                	<input type="hidden" name="product_name" value="<?php echo $product_name; ?>">
	                	<input type="hidden" name="amount" value="<?php echo $total_price_tax_included;?>">
		                <button type="submit" class="btn btn-success btn-lg btn-block website-button">
		                    Pay Now   <span class="glyphicon glyphicon-chevron-right"></span>
		                </button>
	                </form>
	            </td>
	            </div>
	        </div>
	    </div>
	</div>