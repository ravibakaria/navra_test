<?php
  session_start();
    if(isset($_SESSION['last_page_accessed']))
    {
      unset($_SESSION['last_page_accessed']);
      unset($_SESSION['membership_plan_id']);
      unset($_SESSION['featured_listing']);
      unset($_SESSION['validity']);
      unset($_SESSION['secret_key']);
    }

    include("layout/header.php");      // Header

    $primary_color = getPrimaryColor();
    $primary_font_color = getPrimaryFontColor();

    $sidebar_color = getSidebarColor();
    $sidebar_font_color = getSidebarFontColor();

  $stmt   = $link->prepare("SELECT * FROM `faq` WHERE `status`='1'");
    $stmt->execute();
    $result = $stmt->fetchAll();
    $count=$stmt->rowCount();

?>

<!-- meta info  -->

  <title>
    Frequently Asked Questions  -  <?php echo getWebsiteTitle(); ?>
  </title>

    <meta name="title" content="<?= $site_title.' - Frequently Asked Questions'; ?>" />

    <meta name="description" content="To find Verified Profiles, Register Free! If you are Looking For Groom or Bride – we have a perfect match for you."/>

    <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

    <meta property="og:title" content="<?php echo $site_title.' - Frequently Asked Questions'; ?>" />
    <meta property="og:url" content="<?= $site_url; ?>" />
    <meta property="og:description" content="<?php echo $site_tagline;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
    <meta property="twitter:title" content="<?= $site_title.' - Frequently Asked Questions'; ?>" />
    <meta property="twitter:url" content="<?= $site_url; ?>" />
    <meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
  include("layout/styles.php"); 
  include("layout/menu.php"); 
?>
<section class="blank_section">
  <br/><br/><br/><br/><br/>
</section>
<!-- Main Page Content Shown Here  -->
    
    <div class="container">
      <div class="panel-group" id="faqAccordion">
       
          <h1 class="heading-primary centered-data">Frequently Asked Questions</h1>
            <?php
                  $i = 1;
                  foreach ($result as $row) 
                  {
                    $id = $row['id'];
                    $faq_question = $row['faq_question'];
                    $faq_answer = $row['faq_answer'];
              ?>
              <div class="panel panel-default ">
                  <div class="panel-heading accordion-toggle question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question-<?php echo $id;?>" area-expanded="true">
                       <h4 class="panel-title">
                          <a class="ing"><b>Q: <?php echo $faq_question;?>?</b></b></a>
                    </h4>

                  </div>
                  <div id="question-<?php echo $id;?>" class="panel-collapse <?php if($i!=1) { echo 'collapse in'; } else { echo 'collapse';}?>" style="height: 0px;">
                      <div class="panel-body">
                           <h5><span class="label label-success">Answer</span></h5>

                          <p><?php echo $faq_answer;?></p>
                      </div>
                  </div>
              </div>
              <br>
            <?php
                  $i = $i+1;
                  }
              ?>
      </div>
    </div>
  </div>
  <!-- // Accordion -->
      </div>      
    </div>
</body>
<?php
  include "layout/footer.php";
?>
<script>
  $('.collapse').collapse({parent: '#accordion'});
</script>