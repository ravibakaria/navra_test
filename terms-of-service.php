<?php
	session_start();
  if(isset($_SESSION['last_page_accessed']))
  {
    unset($_SESSION['last_page_accessed']);
    unset($_SESSION['membership_plan_id']);
    unset($_SESSION['featured_listing']);
    unset($_SESSION['validity']);
    unset($_SESSION['secret_key']);
  }
  include("layout/header.php");      // Header

	$stmt   = $link->prepare("SELECT * FROM `terms_of_service`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    if($count>0)
    {
    	$content = $result['content'];
      $content = str_replace("../", '', $content);
    }
?>

<!-- meta info  -->

  <title>
    <?php
    $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
    $string = str_replace("-", " ", $a);
    echo $title = ucwords($string);
    ?>  -  <?php echo getWebsiteTitle(); ?>
  </title>

    <meta name="title" content="<?= $site_title.' - Privacy Policy'; ?>" />

    <meta name="description" content="To find Verified Profiles, Register Free! If you are Looking For Groom or Bride – we have a perfect match for you."/>

    <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

    <meta property="og:title" content="<?php echo $site_title.' - Frequently Asked Questions'; ?>" />
    <meta property="og:url" content="<?= $site_url; ?>" />
    <meta property="og:description" content="<?php echo $site_tagline;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
    <meta property="twitter:title" content="<?= $site_title.' - Frequently Asked Questions'; ?>" />
    <meta property="twitter:url" content="<?= $site_url; ?>" />
    <meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
  include("layout/styles.php"); 
  include("layout/menu.php"); 
?>

<section class="blank_section">
  <br/><br/><br/><br/><br/><br/> 
</section>
<!-- Main Page Content Shown Here  -->
	<div class="container">
		<div class="row terms-of-services-data">
      <h1 class="text-center">Terms of Services</h1>
			<?php echo $content;?>
		</div>
	</div>
  <br/><br/>
</body>
<?php
	include "layout/footer.php";
?>