<?php
	session_start();
	include("layout/header.php");      // Header
?>

<!-- meta info  -->

	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?>  -  <?php echo getWebsiteTitle(); ?>
	</title>
<?php
  include("layout/styles.php"); 
  include("layout/menu.php"); 
?>

<section>
  <br/><br/><br/><br/><br/>
</section>

<?php
	
	if(!isset($_GET['membership_plan_id']) && !isset($_GET['featured_listing']) && !isset($_GET['validity']) && !isset($_GET['secret_key']) )
	{
		echo "<script>window.location.assign('membership-plans.php');</script>";
		exit;
	}

	$membership_plan_id = $_GET['membership_plan_id'];
	$featured_listing = $_GET['featured_listing'];
	$validity = $_GET['validity'];
	$secret_key = $_GET['secret_key'];

	if(!is_numeric($membership_plan_id) || !is_numeric($featured_listing) || !is_numeric($validity))
	{
		echo "<br/><br/><br/><br/><br/><br/>";
		echo "<div class='row'><center><h1>Error! Invalid parameter value.</h1></center></div>";
		echo "<br/><br/><br/><br/><br/><br/><br/><br/>";
		include("layout/footer.php");      // footer
		exit;
	}

	$secret_key_chk = $membership_plan_id."*+".$validity."+*".$featured_listing;
	$secret_key_chk = md5($secret_key_chk);

	if($secret_key!=$secret_key_chk)
	{
		echo "<br/><br/><br/><br/><br/><br/>";
		echo "<div class='row'><center><h1>Error! Invalid parameter value.</h1></center></div>";
		echo "<br/><br/><br/><br/><br/><br/><br/><br/>";
		include("layout/footer.php");      // footer
		exit;
	}

	$logged_in = CheckUserLoggedIn();
	$adminEmail = getCompanyEmail();
	if($logged_in=='0')
	{
		$_SESSION['last_page_accessed']="membership-plans.php";
		$_SESSION['membership_plan_id']=$membership_plan_id;
		$_SESSION['featured_listing']=$featured_listing;
		$_SESSION['validity']=$validity;
		$_SESSION['secret_key']=$secret_key;
		
		echo "<script>window.location.assign('login.php');</script>";
		exit;
	}


	if(isset($_SESSION['last_page_accessed']))
	{
		unset($_SESSION['last_page_accessed']);
	}
	

	$user_id = $_SESSION['user_id'];
	
	/************   User info Details start   **************/
		$first_name = getUserFirstName($user_id);
		$last_name = getUserLastName($user_id);
		$user_name = $first_name." ".$last_name;
		$email = getUserEmail($user_id);
		$mobile = getUserMobile($user_id);
		$phone = '+'.getUserCountryPhoneCode($user_id).'-'.getUserMobile($user_id);
		$profileId = getUserUniqueCode($user_id);
	/************   User info Details End   **************/

	$DefaultCurrency = getDefaultCurrency();
    $DefaultCurrencyCode = getDefaultCurrencyCode($DefaultCurrency);


	/************   Premium plan order start   **************/
    if($membership_plan_id!=0)
    {
    	include("modules/order/paid-membership-order.php");
    }
    /************   Premium plan order end   **************/


    /************   Featured listing order start   **************/
    if($membership_plan_id==0)
    {
    	include("modules/order/featured-listing-order.php");
    }
    /************   Featured listing order end   **************/
?>

	
	<br/><br/>
</section>
<?php
  include "layout/footer.php";
?>