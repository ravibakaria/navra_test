<?php
	if(!isset($_SESSION))
	{
    	session_start();
	}

	require_once '../../config/config.php'; 
	include("../../config/dbconnect.php"); 
	require_once '../../config/functions.php'; 
	require_once '../../config/setup-values.php'; 


	$logged_in = CheckUserLoggedIn();
	if($logged_in=='0')
	{
	    echo "<script>window.location.assign('../../login.php');</script>";
	    exit;
	}

	$userid = $_SESSION['user_id'];
	
	$WebsiteBasePath = getWebsiteBasePath();
	$siteTitle = getWebsiteTitle();
	$siteDescription = getWebSiteTagline();

	$co_activated_mode = get2CheckoutActivatedMode();
	$AccountNumber = get2CheckoutAccountNumber($co_activated_mode);
	$PublishableKey = get2CheckoutPublishableKey($co_activated_mode);
	$PrivateKey = get2CheckoutPrivateKey($co_activated_mode);

	$environment = null;
  
  if($co_activated_mode=='TEST')
  {
    $environment = 'sandbox';
  }
  else
  if($co_activated_mode=='PROD')
  {
    $environment = 'production';
  }

	$DefaultCurrency = getDefaultCurrency();
  $CURRENCY = getDefaultCurrencyCode($DefaultCurrency);

  $username = getUserFirstName($userid).' '.getUserLastName($userid);
  $userEmail = getUserEmail($userid);
  $userContact = getUserMobile($userid);

  $city_id = getUserCityId($userid);
  $user_city = getUserCityName($city_id);
  $state_id = getUserStateId($userid);
  $user_state = getUserStateName($state_id);
  $country_id = getUserCountryId($userid);
  $user_country = getUserCountryName($country_id);
?>