<?php
require_once("2checkout-config.php");

require_once("lib/Twocheckout.php");

Twocheckout::privateKey($PrivateKey); //Private Key
Twocheckout::sellerId($AccountNumber); // 2Checkout Account Number

if($co_activated_mode=='TEST')
{
    Twocheckout::sandbox(true); // Set to false for production accounts.
}
else
if($co_activated_mode=='PROD')
{
    Twocheckout::sandbox(false); // Set to false for production accounts.
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        <?php
            $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
            $string = str_replace("-", " ", $a);
            echo $title = ucwords($string);
        ?>  -  <?php echo getWebsiteTitle(); ?>
    </title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="css/cards.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <style>
        .error_input{
            border:1px solid red !important;
        }
    </style>
</head>
<body>
    <div class="container">
    <br/><br/><br/><br/>
    <div class="col-lg-12">
        <div class="row">           
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">    
<?php
try {
        $charge = Twocheckout_Charge::auth(array(
            "merchantOrderId" => $_POST['order_id'],
            "token"      => $_POST['token'],
            "currency"   => $_POST['currency_code'],
            "total"      => $_POST['product_price'],
            "billingAddr" => array(
                "name" => $_POST['username'],
                "addrLine1" => '-',
                "city" => $_POST['user_city'],
                "state" => $_POST['user_state'],
                "zipCode" => '-',
                "country" => $_POST['user_country'],
                "email" => $_POST['userEmail'],
                "phoneNumber" => $_POST['userContact']
            )
        ));

        if ($charge['response']['responseCode'] == 'APPROVED') 
        {
            $order_id = $charge['response']['merchantOrderId'];
            $transaction_id = $charge['response']['transactionId'];
            $payment_gateway = '2CheckOut';
            $payment_method = 'CARD';
            $request_id = 'CO'.$charge['response']['orderNumber'].'CO';
            $amount_paid = $charge['response']['total'];
            $create_date = date('Y-m-d H:i:s');
            $today = date('Y-m-d');
     
            $sql = "SELECT * FROM payment_transactions WHERE OrderNumber='$order_id'";
            $stmt = $link->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetch();

            $membership_plan = $result['membership_plan'];
            $membership_plan_name = $result['membership_plan_name'];
            $featured_listing = $result['featured_listing'];
            $package_month = $result['tenure'];
            $status = $result['status'];

            if($status=='0')
            {
                if($membership_plan=='Yes')
                {
                    $sql_chk_expiry = "SELECT * FROM payment_transactions where userid = '$userid' AND LOWER(membership_plan_name)=LOWER('$membership_plan_name') AND membership_plan_expiry_date>'$today' AND status='1' ORDER BY id DESC";
                    $stmt_chk_expiry = $link->prepare($sql_chk_expiry);
                    $stmt_chk_expiry->execute();
                    $count_chk_expiry = $stmt_chk_expiry->rowCount();
                    
                    if($count_chk_expiry>0)
                    {
                        $result_chk_expiry = $stmt_chk_expiry->fetch();
                        $plan_date = $result_chk_expiry['membership_plan_expiry_date'];
                    }
                    else
                    {
                        $plan_date = $today;
                    }
                    
                    $membership_plan_expiry_date = date('Y-m-d', strtotime("$plan_date +".$package_month." month"));                    
                }
                else
                {
                    $membership_plan_expiry_date = '0000-00-00';
                }
                
                if($featured_listing=='Yes')
                {
                    $sql_chk_expiry_fp = "SELECT * FROM payment_transactions where userid = '$userid' AND featured_listing_expiry_date>'$today' AND status='1' ORDER BY id DESC";
                    $stmt_chk_expiry_fp = $link->prepare($sql_chk_expiry_fp);
                    $stmt_chk_expiry_fp->execute();
                    $count_chk_expiry_fp = $stmt_chk_expiry_fp->rowCount();
                    
                    if($count_chk_expiry_fp>0)
                    {
                        $result_chk_expiry_fp = $stmt_chk_expiry_fp->fetch();
                        $plan_date_fp = $result_chk_expiry_fp['featured_listing_expiry_date'];
                    }
                    else
                    {
                        $plan_date_fp = $today;
                    }
                    
                    $featured_listing_expiry_date = date('Y-m-d', strtotime("$plan_date_fp +".$package_month." month"));
                }
                else
                {
                    $featured_listing_expiry_date = '0000-00-00';
                }

                
                $sql_chk_pp = "SELECT * FROM payment_transactions where userid = '$userid' AND `request_id` = '$request_id' AND OrderNumber='$order_id'";
                $stmt_chk_pp = $link->prepare($sql_chk_pp);
                $stmt_chk_pp->execute();
                $count_pp = $stmt_chk_pp->rowCount();
                
                if($count_pp==0)
                {
                    
                    $sql_insert = "UPDATE payment_transactions SET membership_plan_expiry_date='$membership_plan_expiry_date',featured_listing_expiry_date='$featured_listing_expiry_date',transact_id='$transaction_id',request_id='$request_id',payment_gateway='$payment_gateway',payment_method='$payment_method',updated_at=now(),status='1' WHERE userid = '$userid' AND OrderNumber='$order_id'";
                    $link->exec($sql_insert);
                }
         
                echo "<script>window.location.assign('$WebsiteBasePath/users/payment-order-receipt.php?request_id=$request_id');</script>";
                exit; 
            }

        }
    } 
    catch (Twocheckout_Error $e) 
    {
        $error_message = $e->getMessage();
        
        $order_id = $_POST['order_id'];

        $sql_insert = "UPDATE payment_transactions SET updated_at='$today',status='2' WHERE userid = '$userid' AND OrderNumber='$order_id'";
        $link->exec($sql_insert);

        echo "<center><h2><b style='color:red;'>".$error_message."</b></h2></center><br/>";
        echo "<center>&nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/'>Home</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/users/my-orders.php'>My Orders</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/membership-plans.php'>Purchase New Plan</a></center>";
        exit;
    }
?>

                </div>
            </div>
        </div>
    </div>
</body>
</html>