<?php
	include_once("2checkout-config.php");

	if(empty($_POST))
	{
	    echo "<script>window.location.assign('../../membership-plans.php');</script>";
	    exit;
	}

	$order_id_NB = $_POST['order_id'];
	$full_name = $_POST['full_name'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$product_name = $_POST['product_name'];
	$product_price = $_POST['product_price'];

 	$today = date('Y-m-d H:i:s');
	$ORDER_ID = $_POST["order_id"];
	$_SESSION['order_id']=$ORDER_ID;
	$product = $_POST["product_name"];

	$product_arr = explode('_',$product);
    $premium_plan = $product_arr[0];
    $featured_plan = $product_arr[1];
    $package_month = $product_arr[2];

    if($premium_plan!='NONPREMIUM')
    {
    	$membership_plan = 'Yes';
    	$membership_plan_name = $premium_plan;
    	$membership_plan_id = getMembershipPlanId($premium_plan);
    	$membership_contacts = getMembershipPlanContacts($premium_plan);
    	$membership_plan_amount = getMembershipPlanAmount($premium_plan);
    }
    else
    {
    	$membership_plan = 'No';
    	$membership_plan_name = NULL;
    	$membership_plan_id = 0;
    	$membership_contacts = 0;
    	$membership_plan_amount = '0.00';
    }

    if($featured_plan!='NONFEATURED')
    {
    	$featured_listing = "Yes";
    	$featured_listing_amount = $package_month * getFeaturedListingPrice();
    }
    else
    {
    	$featured_listing = "No";
    	$featured_listing_amount = '0.00';
    }

    $tax_applied = getTaxAppliedOrNot();  // check tax applied or not
    if($tax_applied=='' || $tax_applied==null)
    {
    	$tax_applied='0';
    }

    if($tax_applied=='1')
    {
    	$tax_name = getTaxName();
    	$tax_percent = getTaxPercent();
    	$tax_amount = number_format(round((($membership_plan_amount+$featured_listing_amount) * $tax_percent)/100,2),2,'.','');
    	$total_amount = number_format(round(($membership_plan_amount+$featured_listing_amount)+$tax_amount,2),2,'.','');
    }
    else
    {
    	$tax_applied='0';
    	$tax_name = 'NA';
    	$tax_percent = 0;
    	$tax_amount = '0.00';
    	$total_amount = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');
    }

    $sql_chk = "SELECT * FROM payment_transactions WHERE OrderNumber='$ORDER_ID'";
    $stmt_chk = $link->prepare($sql_chk);
    $stmt_chk->execute();
    $count_chk = $stmt_chk->rowCount();

    if($count_chk==0)
    {
	    $sql_insert_order = "INSERT INTO payment_transactions(`userid`,`OrderNumber`,`membership_plan`,`membership_plan_name`,`membership_plan_id`,`membership_contacts`,`membership_plan_amount`,`featured_listing`,`featured_listing_amount`,`tax_applied`,`tax_name`,`tax_percent`,`tax_amount`,`total_amount`,`tenure`,`created_at`,`status`) VALUES('$userid','$ORDER_ID','$membership_plan','$membership_plan_name','$membership_plan_id','$membership_contacts','$membership_plan_amount','$featured_listing','$featured_listing_amount','$tax_applied','$tax_name','$tax_percent','$tax_amount','$total_amount','$package_month','$today','0')";
	    $link->exec($sql_insert_order);
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>
	    <?php
	        $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
	        $string = str_replace("-", " ", $a);
	        echo $title = ucwords($string);
	    ?>  -  <?php echo getWebsiteTitle(); ?>
	</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="css/cards.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<style>
		.error_input{
			border:1px solid red !important;
		}
	</style>
</head>
<body>
	<div class="container">
	<br/><br/><br/><br/>
	<div class="col-lg-12">
		<div class="row">			
			<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
				<center>
					<h1>Please verify details below.<br></h1>
				</center>
				
				<table border="1px" class="table table-responsive table-bordered">
			        <tr>
			          	<th>Full Name</th>
			          	<td><?php echo $full_name;?></td>
			        </tr>
			        <tr>
			          	<th>Email</th>
			          	<td><?php echo $email;?></td>
			        </tr>
			        <tr>
			          	<th>Mobile</th>
			          	<td><?php echo $phone;?></td>
			        </tr>
			        <tr>
			          	<th>Product Description</th>
			          	<td>
			            <?php 
			                if($premium_plan!='NONPREMIUM')
			                {
			                  echo "<strong>Product Name:</strong> ".$premium_plan." plan<br>";
			                  echo "<strong>Validity in months:</strong> ".$package_month." month<br>";
			                }
			                else
			                if($featured_plan!='NONFEATURED')
			                {
			                  echo "<strong>Product Name:</strong> Featured Listing<br>";
			                  echo "<strong>Validity in months:</strong> ".$package_month." month<br>";
			                }
			            ?>
			              
			          	</td>
			        </tr>
			        <tr>
			          	<th>Product Price</th>
			          	<td><?php echo $CURRENCY.' '.$product_price;?></td>
			        </tr>
			        <tr>
			          	<td colspan='2'><center><a href="<?php echo $WebsiteBasePath?>/membership-plans.php" class="btn btn-default">Cancel</a></center></td>							
			        </tr>
			 	</table>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
				<center>
					<h2>Buy Now</h2>
				</center>
				<div class="panel panel-default credit-card-box">
	                <div class="panel-heading display-table" >
	                    <div class="row display-tr" >
	                        <h3 class="panel-title display-td" >Payment Details</h3>
	                        <div class="display-td" >                            
	                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
	                        </div>
	                    </div> 

	                </div>
	                <div class="panel-body">
				    	<form id="myCCForm" action="2checkout-thank-you.php" method="post">
				    		<p align="right" style="color:red;"><b>* All fields are mandatory</b></p>
				            <input id="token" name="token" type="hidden" value="">
				            <div>
				                <label>
				                    <span>Card Number</span>
				                </label>
				                <div class="input-group">
                                    <input id="ccNo" type="text" class="form-control" size="20" maxlength="20" value="" autocomplete="off"  placeholder="123456789012345" required />
                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                </div>
				            </div>
				            <br/>
				            <div class="row">
					            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					                <label>
					                    <span>Expiration Date (MM/YYYY)</span>
					                </label>
					                <input type="text" maxlength="2" size="2" id="expMonth" placeholder="MM" required />
					                <span> / </span>
					                <input type="text" maxlength="4" size="4" id="expYear" placeholder="YYYY" required />
					            </div>
					            
					            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					                <label>
					                    <span>CVV</span>
					                </label>
					                <input id="cvv" maxlength="4" size="4" type="text" value="" autocomplete="off" placeholder="123" required />
					            </div>
					        </div>
				            <input type="hidden" name="product_name" value="<?php echo $product_name; ?>">
							<input type="hidden" name="order_id" value="<?php echo $order_id_NB; ?>">
							<input type="hidden" name="product_price" value="<?php echo $product_price; ?>">
							<input type="hidden" name="currency_code" value="<?php echo $CURRENCY; ?>">	
							<input type="hidden" name="username" value="<?php echo $username; ?>">
							<input type="hidden" name="userEmail" value="<?php echo $userEmail; ?>">
							<input type="hidden" name="userContact" value="<?php echo $userContact; ?>">	
							<input type="hidden" name="user_city" value="<?php echo $user_city; ?>">
							<input type="hidden" name="user_state" value="<?php echo $user_state; ?>">
							<input type="hidden" name="user_country" value="<?php echo $user_country; ?>">	
							<br/>
							<div class="row">
	                            <div class="col-xs-12 payment_data_status">
	                            	
	                            </div>
	                        </div>

	                        <div class="row">
	                            <div class="col-xs-12">
	                            	<center><img src="<?php echo $WebsiteBasePath;?>/images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
	                            </div>
	                        </div>

				            <input type="submit" class="btn_buynow btn btn-success btn-lg btn-block" name="submit-button" id="submit-button" value="Submit Payment">
				        </form>
				    </div>
		    </div>
		</div>		
	</div>
</div>

<script type="text/javascript" src="https://www.2checkout.com/checkout/api/2co.min.js"></script>

<script>
    // Called when token created successfully.
    var successCallback = function(data) {
        var myForm = document.getElementById('myCCForm');

        // Set the token as the value for the token input
        myForm.token.value = data.response.token.token;

        // IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.
        myForm.submit();
    };

    // Called when token creation fails.
    var errorCallback = function(data) {
        if (data.errorCode === 200) {
            tokenRequest();
        } else {
        	$('.loading_img').hide();
    		$('.btn_buynow').attr('disabled',false);
            alert(data.errorMsg);
        }
    };


    var tokenRequest = function() {

    	var ccNo = $('#ccNo').val();
    	var expMonth = $('#expMonth').val();
    	var expYear = $('#expYear').val();
    	var cvv = $('#cvv').val();

    	if(ccNo.length<16)
    	{
    		$('#ccNo').addClass('error_input');
    		$('.payment_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong> Invalid! </strong> Credit/Debit card number should be atleast 16 digits!</div>");
    		return false;
    	}

    	if(expYear.length<3)
    	{
    		$('#expYear').addClass('error_input');
    		$('.payment_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong> Invalid! </strong> Credit/Debit card expiry year should be 4 digits (YYYY) format!</div>");
    		return false;
    	}

    	if(cvv.length<3)
    	{
    		$('#cvv').addClass('error_input');
    		$('.payment_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong> Invalid! </strong> CVV number should be atleast 3 digits!</div>");
    		return false;
    	}

    	$('.loading_img').show();
    	$('.btn_buynow').attr('disabled',true);
    	// Setup token request arguments
        var args = {
            sellerId: "<?php echo $AccountNumber?>",
            publishableKey: "<?php echo $PublishableKey?>",
            ccNo: $("#ccNo").val(),
            cvv: $("#cvv").val(),
            expMonth: $("#expMonth").val(),
            expYear: $("#expYear").val()
        };

        // Make the token request
        TCO.requestToken(successCallback, errorCallback, args);
    };

    $(function() {
        // Pull in the public encryption key for our environment
        TCO.loadPubKey('<?php echo $environment?>');

        $("#myCCForm").submit(function(e) {
            // Call our token request function
            tokenRequest();

            // Prevent form from submitting
            return false;
        });
    });
</script>

<script>
	$(document).ready(function(){
		/*   Prevent entering charaters in credit card, month,year,cvv number   */
        $("#ccNo").keypress(function (e) {
        	$(this).removeClass('error_input');
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                return false;
            }
        });

        $("#expMonth").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                return false;
            }
        });

        $("#expYear").keypress(function (e) {
        	$(this).removeClass('error_input');
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                return false;
            }
        });

        $("#cvv").keypress(function (e) {
        	$(this).removeClass('error_input');
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                return false;
            }
        });
		
	});
</script>

</body>
</html>