<?php
    if(!isset($_SESSION))
    {
        session_start();
    }

    require_once '../../config/config.php'; 
    include("../../config/dbconnect.php"); 
    require_once '../../config/functions.php'; 
    require_once '../../config/setup-values.php'; 

    $im_activated_mode = getInstamojoActivatedMode();
    
    $im_salt = getImPrivateSalt($im_activated_mode);
    $im_email = getImEmail($im_activated_mode);

    $data = $_POST;
    $mac_provided = $data['mac'];  // Get the MAC from the POST data
    unset($data['mac']);  // Remove the MAC key from the data.

    $ver = explode('.', phpversion());
    $major = (int) $ver[0];
    $minor = (int) $ver[1];

    if($major >= 5 and $minor >= 4){
         ksort($data, SORT_STRING | SORT_FLAG_CASE);
    }
    else{
         uksort($data, 'strcasecmp');
    }

    // You can get the 'salt' from Instamojo's developers page(make sure to log in first): https://www.instamojo.com/developers
    // Pass the 'salt' without the <>.
    $mac_calculated = hash_hmac("sha1", implode("|", $data), $im_salt);

    if($mac_provided == $mac_calculated){
        echo "MAC is fine";
        // Do something here
        if($data['status'] == "Credit"){
           // Payment was successful, mark it as completed in your database  
                    $OrderNumber = $data['purpose'];
                    $sql_get_pp_details = "SELECT * FROM `payment_transactions` WHERE `OrderNumber` LIKE '$OrderNumber'";
                    $stmt_get_pp_details = $link->prepare($sql_get_pp_details);
                    $stmt_get_pp_details->execute();

                    $result_get_pp_details = $stmt_get_pp_details->fetch();

                    $receipt_number = $result_get_pp_details['id'];
                    $userid = $result_get_pp_details['userid'];
                    $membership_plan = $result_get_pp_details['membership_plan'];
                    $membership_plan_name = $result_get_pp_details['membership_plan_name'];
                    $membership_plan_id = $result_get_pp_details['membership_plan_id'];
                    $membership_contacts = $result_get_pp_details['membership_contacts'];
                    $membership_plan_amount = $result_get_pp_details['membership_plan_amount'];
                    $membership_plan_expiry_date = $result_get_pp_details['membership_plan_expiry_date'];
                    $featured_listing = $result_get_pp_details['featured_listing'];
                    $featured_listing_amount = $result_get_pp_details['featured_listing_amount'];
                    $featured_listing_expiry_date = $result_get_pp_details['featured_listing_expiry_date'];
                    $tax_applied = $result_get_pp_details['tax_applied'];
                    $tax_name = $result_get_pp_details['tax_name'];
                    $tax_percent = number_format(round($result_get_pp_details['tax_percent'],2),2,'.','');
                    $tax_amount = $result_get_pp_details['tax_amount'];
                    $total_amount = $result_get_pp_details['total_amount'];
                    $tenure = $result_get_pp_details['tenure'];
                    $transact_id = $result_get_pp_details['transact_id'];
                    $request_id = $result_get_pp_details['request_id'];
                    $payment_gateway = $result_get_pp_details['payment_gateway'];
                    $payment_method = $result_get_pp_details['payment_method'];
                    $created_at = $result_get_pp_details['created_at'];
                    $OrderNumber = $result_get_pp_details['OrderNumber'];

                    if($membership_contacts=='0')
                    {
                        $membership_contacts="Unlimited";
                    }

                    /************   User info Details start   **************/
                    $first_name = getUserFirstName($userid);
                    $last_name = getUserLastName($userid);
                    $user_name = $first_name." ".$last_name;
                    $email = getUserEmail($userid);
                    $mobile = getUserMobile($userid);
                    $phone = '+'.getUserCountryPhoneCode($userid).'-'.getUserMobile($userid);
                    $profileId = getUserUniqueCode($userid);

                    /************   User info Details End   **************/

                    $DefaultCurrency = getDefaultCurrency();
                    $DefaultCurrencyCode = getDefaultCurrencyCode($DefaultCurrency);

                    $purchase_date = date('d F Y',strtotime($created_at));

                    $subtotal = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');

                    $total_tax = number_format(round($tax_amount,2),2,'.','');
                    $total_amount_paid = number_format(round($total_amount,2),2,'.','');

                    $to = $im_email;
                    $subject = 'Website Payment Request ' .$data['buyer_name'].'';
                    
                    $message = "<center><h2>Payment Receipt Copy</h2></center>";
                    
                    $message = "<h1>Payment Details</h1>";
                    $message .= '<p><b>Order ID:</b> '.$data['purpose'].'</p>';
                    $message .= '<p><b>ID:</b> '.$data['payment_id'].'</p>';
                    $message .= '<p><b>Amount:</b> '.$data['amount'].'</p>';
                    $message .= '<p><b>Transaction Date:</b> '.$purchase_date.'</p>';
                    $message .= "<hr>";
                    $message = "<h1>Member  Details</h1>";
                    $message .= '<p><b>Full Name:</b> '.$user_name.'</p>';
                    $message .= '<p><b>Email:</b> '.$email.'</p>';
                    $message .= '<p><b>Phone:</b> '.$phone.'</p>';
                    $message .= '<p><b>Profile ID:</b> '.$profileId.'</p>';

                    $message .= "<h1>Purchased Plan  Details</h1>";

                    if($membership_plan=='Yes')
                    {
                        $membership_plan_description = $membership_plan_name." Membership Plan - ".$membership_contacts." Contacts - Validity ".$tenure." Months";
                        $message .= "<p><b>Membership Plan :</b> ".$membership_plan_description."</p>";
                        $message .= "<p><b>Amount : </b>".$DefaultCurrencyCode.":".$membership_plan_amount."</p>";
                        $message .= "<hr>";
                    }


                    if($featured_listing=='Yes')
                    {
                        $featured_plan_description = "  Validity ".$tenure." Months";
                        $message .= "<p><b>Featured Listing - </b> ".$featured_plan_description."</p>";
                        $message .= "<p><b>Amount : </b>".$DefaultCurrencyCode.":".$featured_listing_amount."</p>";
                        $message .= "<hr>";
                    }
                    

                    $message .= '<p><b>Sub Total:</b> '.$DefaultCurrencyCode.":".$subtotal.'</p>';

                    $message .= "<hr>";

                    if($tax_applied=='1')
                    {
                        $message .= "<p".$tax_percent."% ".$tax_name."</p>";
                        $message .= "<p>".$DefaultCurrencyCode.":".$tax_amount."</p>";
                        $message .= "<hr>";
                    }


                    $message .= '<p><b>Total: </b> '.$DefaultCurrencyCode.":".$total_amount.'</p>';

                    $message .= "<hr>";

                    $message .= "<p><b>Transaction Date:</b> ".$purchase_date."</p>";
                    $message .= "<p><b>Gateway: </b> Instamojo </p>";
                    $message .= "<p><b>Transaction ID: </b> ".$data['payment_id']."</p>";
                    $message .= "<p><b>Amount: </b> ".$DefaultCurrencyCode.":".$total_amount."</p>";

                    $message .= "<hr>";

                  
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    // send email
                    mail($to, $subject, $message, $headers);




        }
        else{
           // Payment was unsuccessful, mark it as failed in your database
        }
    }
    else{
        echo "Invalid MAC passed";
    }
?>
