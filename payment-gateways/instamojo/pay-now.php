<?php
  	if(!isset($_SESSION))
	{
	    session_start();
	}

	require_once '../../config/config.php'; 
	include("../../config/dbconnect.php"); 
	require_once '../../config/functions.php'; 
	require_once '../../config/setup-values.php'; 

	$logged_in = CheckUserLoggedIn();
	if($logged_in=='0')
	{
		echo "<script>window.location.assign('../../login.php');</script>";
		exit;
	}

	if(empty($_POST))
	{
		echo "<script>window.location.assign('../../membership-plans.php');</script>";
		exit;
	}

	$userid = $_SESSION['user_id'];

	$product_arr = array();

	$order_id = $_POST["order_id"];
	$product = $_POST["product_name"];
	$price = $_POST["product_price"];
	$name = $_POST["full_name"];
	$phone = $_POST["phone"];
	$email = $_POST["email"];

	$im_activated_mode = getInstamojoActivatedMode();
	$private_api_key = getImPrivateApiKey($im_activated_mode);
	$private_auth_key = getImPrivateAuthKey($im_activated_mode);

	$WebsiteBasePath = getWebsiteBasePath();


	if($im_activated_mode=='TEST')
	{
		include 'src/instamojo-test.php';
		$api = new Instamojo\Instamojo($private_api_key, $private_auth_key,'https://test.instamojo.com/api/1.1/');
	}
	else
	if($im_activated_mode=='PROD')
	{
		include 'src/instamojo.php';
		$api = new Instamojo\Instamojo($private_api_key, $private_auth_key,'https://www.instamojo.com/api/1.1/');
	}

	try {
	    $response = $api->paymentRequestCreate(array(
	        "purpose" => $order_id,
	        "amount" => $price,
	        "buyer_name" => $name,
	        "phone" => $phone,
	        "send_email" => true,
	        "send_sms" => true,
	        "email" => $email,
	        'allow_repeated_payments' => false,
	        "redirect_url" => $WebsiteBasePath."/payment-gateways/instamojo/instamojo-thank-you.php",
	        "webhook" => $WebsiteBasePath."/payment-gateways/instamojo/webhook.php"
	        ));
    	//print_r($response);

	    $product_arr = explode('_',$product);
        $premium_plan = $product_arr[0];
        $featured_plan = $product_arr[1];
        $package_month = $product_arr[2];

        if($premium_plan!='NONPREMIUM')
        {
        	$membership_plan = 'Yes';
        	$membership_plan_name = $premium_plan;
        	$membership_plan_id = getMembershipPlanId($premium_plan);
        	$membership_contacts = getMembershipPlanContacts($premium_plan);
        	$membership_plan_amount = getMembershipPlanAmount($premium_plan);
        }
        else
        {
        	$membership_plan = 'No';
        	$membership_plan_name = NULL;
        	$membership_plan_id = 0;
        	$membership_contacts = 0;
        	$membership_plan_amount = '0.00';
        }

        if($featured_plan!='NONFEATURED')
        {
        	$featured_listing = "Yes";
        	$featured_listing_amount = $package_month * getFeaturedListingPrice();
        }
        else
        {
        	$featured_listing = "No";
        	$featured_listing_amount = '0.00';
        }

        $tax_applied = getTaxAppliedOrNot();  // check tax applied or not
        if($tax_applied=='' || $tax_applied==null)
        {
        	$tax_applied='0';
        }

        if($tax_applied=='1')
        {
        	$tax_name = getTaxName();
        	$tax_percent = getTaxPercent();
        	$tax_amount = number_format(round((($membership_plan_amount+$featured_listing_amount) * $tax_percent)/100,2),2,'.','');
        	$total_amount = number_format(round(($membership_plan_amount+$featured_listing_amount)+$tax_amount,2),2,'.','');
        }
        else
        {
        	$tax_applied='0';
        	$tax_name = 'NA';
        	$tax_percent = 0;
        	$tax_amount = '0.00';
        	$total_amount = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');
        }

        $sql_insert_order = "INSERT INTO payment_transactions(`userid`,`OrderNumber`,`membership_plan`,`membership_plan_name`,`membership_plan_id`,`membership_contacts`,`membership_plan_amount`,`featured_listing`,`featured_listing_amount`,`tax_applied`,`tax_name`,`tax_percent`,`tax_amount`,`total_amount`,`tenure`,`created_at`,`status`) VALUES('$userid','$order_id','$membership_plan','$membership_plan_name','$membership_plan_id','$membership_contacts','$membership_plan_amount','$featured_listing','$featured_listing_amount','$tax_applied','$tax_name','$tax_percent','$tax_amount','$total_amount','$package_month',now(),'0')";
        $link->exec($sql_insert_order);

	    $pay_ulr = $response['longurl'];
	    
	    //Redirect($response['longurl'],302); //Go to Payment page
	    
	     echo ("<SCRIPT LANGUAGE='JavaScript'>window.location.href='".$pay_ulr."';</SCRIPT>"); exit;
	    // header("Location: $pay_ulr");
	    // exit();

	}
	catch (Exception $e) {
	    print('Error: ' . $e->getMessage());
	} 
?>