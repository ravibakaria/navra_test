<?php
	session_start();
	
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include "../../config/functions.php";
	include "../../config/setup-values.php";
	
	$logged_in = CheckUserLoggedIn();
	if($logged_in=='0')
	{
		echo "<script>window.location.assign('../../login.php');</script>";
		exit;
	}

	$userid = $_SESSION['user_id'];

	$WebsiteBasePath = getWebsiteBasePath();
	$SiteTitle = getWebsiteTitle();
	$adminEmail = getCompanyEmail();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>
		<?php
			$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
			$string = str_replace("-", " ", $a);
			echo $title = ucwords($string);
		?>  -  <?php echo getWebsiteTitle(); ?>
	</title>
</head>
<body>
<section>
  <br/><br/><br/><br/><br/>
</section>
<?php
if(isset($_GET["payment_request_id"]))
{
?>	
<div class="container">
	<div class="row text-center">
        <div class="col-sm-6 col-sm-offset-3">
			<?php
				$im_activated_mode = getInstamojoActivatedMode();
				$private_api_key = getImPrivateApiKey($im_activated_mode);
				$private_auth_key = getImPrivateAuthKey($im_activated_mode);

				

				if($im_activated_mode=='TEST')
				{
					include 'src/instamojo-test.php';     //Instamojo
					$api = new Instamojo\Instamojo($private_api_key, $private_auth_key,'https://test.instamojo.com/api/1.1/');
				}
				else
				if($im_activated_mode=='PROD')
				{
					include 'src/instamojo.php';     //Instamojo
					$api = new Instamojo\Instamojo($private_api_key, $private_auth_key,'https://www.instamojo.com/api/1.1/');
				}

			    $payid = $_GET["payment_request_id"];

			    try 
			    {
			      	$response = $api->paymentRequestStatus($payid);

			      	$order_id = $response['purpose'];
			      	
			      	$transaction_id = $response['payments'][0]['payment_id'];
			      	$payment_gateway = 'Instamojo';
			      	$payment_method = $response['payments'][0]['instrument_type'].' - '.$response['payments'][0]['billing_instrument'];
			      	$request_id = $response['id'];

			      	$amount_paid = $response['amount'];
			      	$create_date = date('Y-m-d H:i:s');
			      	$today = date('Y-m-d');
	        
	                $sql = "SELECT * FROM payment_transactions WHERE OrderNumber='$order_id'";
	                $stmt = $link->prepare($sql);
	                $stmt->execute();
	                $result = $stmt->fetch();

	                $membership_plan = $result['membership_plan'];
	                $membership_plan_name = $result['membership_plan_name'];
	                $featured_listing = $result['featured_listing'];
	                $package_month = $result['tenure'];
	                $status = $result['status'];

	                if($status=='0')
	                {
	                	if($membership_plan=='Yes')
		                {
		                	$sql_chk_expiry = "SELECT * FROM payment_transactions where userid = '$userid' AND LOWER(membership_plan_name)=LOWER('$membership_plan_name') AND membership_plan_expiry_date>'$today' AND status='1' ORDER BY id DESC";
	                        $stmt_chk_expiry = $link->prepare($sql_chk_expiry);
		                    $stmt_chk_expiry->execute();
		                    $count_chk_expiry = $stmt_chk_expiry->rowCount();
		                    
		                    if($count_chk_expiry>0)
		                    {
		                        $result_chk_expiry = $stmt_chk_expiry->fetch();
		                        $plan_date = $result_chk_expiry['membership_plan_expiry_date'];
		                    }
		                    else
	        		      	{
	        		      		$plan_date = $today;
	        		      	}
	        		      	
	        		      	$membership_plan_expiry_date = date('Y-m-d', strtotime("$plan_date +".$package_month." month"));                    
		                }
		                else
		                {
		                	$membership_plan_expiry_date = '0000-00-00';
		                }
		                
		                if($featured_listing=='Yes')
		                {
		                	$sql_chk_expiry_fp = "SELECT * FROM payment_transactions where userid = '$userid' AND featured_listing_expiry_date>'$today' AND status='1' ORDER BY id DESC";
	                        $stmt_chk_expiry_fp = $link->prepare($sql_chk_expiry_fp);
		                    $stmt_chk_expiry_fp->execute();
		                    $count_chk_expiry_fp = $stmt_chk_expiry_fp->rowCount();
		                    
		                    if($count_chk_expiry_fp>0)
		                    {
		                        $result_chk_expiry_fp = $stmt_chk_expiry_fp->fetch();
		      		            $plan_date_fp = $result_chk_expiry_fp['featured_listing_expiry_date'];
		                    }
		                    else
	        		      	{
	        		      		$plan_date_fp = $today;
	        		      	}
	        		      	
	        		      	$featured_listing_expiry_date = date('Y-m-d', strtotime("$plan_date_fp +".$package_month." month"));
		                }
		                else
		                {
		                	$featured_listing_expiry_date = '0000-00-00';
		                }

		                
		                $sql_chk_pp = "SELECT * FROM payment_transactions where userid = '$userid' AND `request_id` = '$request_id' AND OrderNumber='$order_id'";
	                    $stmt_chk_pp = $link->prepare($sql_chk_pp);
	                    $stmt_chk_pp->execute();
	                    $count_pp = $stmt_chk_pp->rowCount();
	                    
		      		    if($count_pp==0)
	                    {
	                        
	        		      	$sql_insert = "UPDATE payment_transactions SET membership_plan_expiry_date='$membership_plan_expiry_date',featured_listing_expiry_date='$featured_listing_expiry_date',transact_id='$transaction_id',request_id='$request_id',payment_gateway='$payment_gateway',payment_method='$payment_method',updated_at='$today',status='1' WHERE userid = '$userid' AND OrderNumber='$order_id'";
	                        $link->exec($sql_insert);
	                    }
	             
		                echo "<script>window.location.assign('../../users/payment-order-receipt.php?request_id=$request_id');</script>";
		                exit;
	                }               
			    }
			    catch (Exception $e) 
			    {
			    	echo "<center><h2><b style='color:red;'>Transaction Failed - Payment was Unsuccessful</b></h2></center>
				  	<hr>";
				  	
				  	echo "<center><a href='$WebsiteBasePath/membership-plans.php'>Back to purchase plan</a></center>";
				  	
				}
			?>
		</div>  
	</div>
</div>
<?php
}
else
{
	echo "<script>window.location.assign('../../users/page-404.php');</script>";
}
?>
<br/><br/>
</section>
</body>
</html>