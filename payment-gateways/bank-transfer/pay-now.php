<?php
  	if(!isset($_SESSION))
	{
	    session_start();
	}

	require_once '../../config/config.php'; 
	include("../../config/dbconnect.php"); 
	require_once '../../config/functions.php'; 
	require_once '../../config/setup-values.php'; 

	$logged_in = CheckUserLoggedIn();
	if($logged_in=='0')
	{
		echo "<script>window.location.assign('../../login.php');</script>";
		exit;
	}

	if(empty($_POST))
	{
		echo "<script>window.location.assign('../../membership-plans.php');</script>";
		exit;
	}

	$userid = $_SESSION['user_id'];

	$product_arr = array();

	$order_id = $_POST["order_id"];
	$product = $_POST["product_name"];
	$price = $_POST["product_price"];
	$name = $_POST["full_name"];
	$phone = $_POST["phone"];
	$email = $_POST["email"];

	$WebsiteBasePath = getWebsiteBasePath();
	$request_id = getOfflineRequestId();

    $product_arr = explode('_',$product);
    $premium_plan = $product_arr[0];
    $featured_plan = $product_arr[1];
    $package_month = $product_arr[2];

    if($premium_plan!='NONPREMIUM')
    {
    	$membership_plan = 'Yes';
    	$membership_plan_name = $premium_plan;
    	$membership_plan_id = getMembershipPlanId($premium_plan);
    	$membership_contacts = getMembershipPlanContacts($premium_plan);
    	$membership_plan_amount = getMembershipPlanAmount($premium_plan);
    }
    else
    {
    	$membership_plan = 'No';
    	$membership_plan_name = NULL;
    	$membership_plan_id = 0;
    	$membership_contacts = 0;
    	$membership_plan_amount = '0.00';
    }

    if($featured_plan!='NONFEATURED')
    {
    	$featured_listing = "Yes";
    	$featured_listing_amount = $package_month * getFeaturedListingPrice();
    }
    else
    {
    	$featured_listing = "No";
    	$featured_listing_amount = '0.00';
    }

    $tax_applied = getTaxAppliedOrNot();  // check tax applied or not
    if($tax_applied=='' || $tax_applied==null)
    {
    	$tax_applied='0';
    }

    if($tax_applied=='1')
    {
    	$tax_name = getTaxName();
    	$tax_percent = getTaxPercent();
    	$tax_amount = number_format(round((($membership_plan_amount+$featured_listing_amount) * $tax_percent)/100,2),2,'.','');
    	$total_amount = number_format(round(($membership_plan_amount+$featured_listing_amount)+$tax_amount,2),2,'.','');
    }
    else
    {
    	$tax_applied='0';
    	$tax_name = 'NA';
    	$tax_percent = 0;
    	$tax_amount = '0.00';
    	$total_amount = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');
    }

    $sql_insert_order = "INSERT INTO payment_transactions(`userid`,`OrderNumber`,`membership_plan`,`membership_plan_name`,`membership_plan_id`,`membership_contacts`,`membership_plan_amount`,`featured_listing`,`featured_listing_amount`,`tax_applied`,`tax_name`,`tax_percent`,`tax_amount`,`total_amount`,`tenure`,`request_id`,`payment_gateway`,`created_at`,`status`) VALUES('$userid','$order_id','$membership_plan','$membership_plan_name','$membership_plan_id','$membership_contacts','$membership_plan_amount','$featured_listing','$featured_listing_amount','$tax_applied','$tax_name','$tax_percent','$tax_amount','$total_amount','$package_month','$request_id','Bank-Transfer',now(),'0')";
    
    if($link->exec($sql_insert_order))
    {
    ?>	<form id='redirect_payment_summary' name='redirect_payment_summary' method="post" action="bank-transfer-payment-thank-you.php">
			<input type="hidden" name="order_id" value="<?php echo $order_id;?>">
			<input type="hidden" name="request_id" value="<?php echo $request_id;?>">
			<input type="hidden" name="full_name" value="<?php echo $name;?>">
		    <input type="hidden" name="email" value="<?php echo $email;?>">
		    <input type="hidden" name="phone" value="<?php echo $phone;?>">
		    <input type="hidden" name="product_name" value="<?php echo $product;?>">
		    <input type="hidden" name="product_price" value="<?php echo $price;?>">
		</form>
	<?php
    }
    else
    {
    	echo "<br><br><br><br><br>";
    	echo "<center><h1>Something went wrong...!! Try after some time.</h1></center><br><br>";
    	echo "<center><a href='../../users/'>My Account</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href='../../membership-plans.php'>Membership Plans</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href='../../'>Home</a></center>";
    }
?>

<script src="../../js/jquery.js"></script>
<script>
	$(document).ready(function(){
		$("#redirect_payment_summary").submit();
	});
</script>