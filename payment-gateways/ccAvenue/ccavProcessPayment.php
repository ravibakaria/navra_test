<?php
	require_once "ccAvenue-config.php";
	include('Crypto.php');

	error_reporting(0);

	if(empty($_POST))
	{
	    echo "<script>window.location.assign('../../membership-plans.php');</script>";
	    exit;
	}

	$working_key = $ccAvenue['WorkingKey'];//Shared by CCAVENUES
	$access_code = $ccAvenue['AccessCode'];//Shared by CCAVENUES
	$merchant_data = '';

	foreach ($_POST as $key => $value){
		$merchant_data.=$key.'='.$value.'&';
	}
	
	$encrypted_data=encrypt($merchant_data,$working_key); // Method for encrypting the data.

	$production_url='https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
?>
<html>
<head>
<title>Process Payment - <?php echo getWebsiteTitle(); ?></title>
</head>
<body>
<center>

<form method="post" name="redirect" action="<?php echo $production_url?>"> 
<?php
echo "<input type=hidden name=encRequest value=$encrypted_data>";
echo "<input type=hidden name=access_code value=$access_code>";
?>
</form>

<script language='javascript'>document.redirect.submit();</script>

</center>
</body>
</html>