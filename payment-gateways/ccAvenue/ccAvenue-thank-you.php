<?php 
	require_once "ccAvenue-config.php";
	include('Crypto.php');
?>
<?php

	error_reporting(0);
	
	$workingKey=$ccAvenue['WorkingKey'];		//Working Key should be provided here.
	$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
	$rcvdString=decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
	$order_status="";
	$decryptValues=explode('&', $rcvdString);
	$dataSize=sizeof($decryptValues);

	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
		$res_data[$information[0]] = $information[1];
		
		if($i==3)	
		{
			$order_status=$information[1];
		}
	}

    //echo "<pre>";
    //print_r($res_data);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        <?php
            $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
            $string = str_replace("-", " ", $a);
            echo $title = ucwords($string);
        ?>  -  <?php echo getWebsiteTitle(); ?>
    </title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="css/cards.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <style>
        .error_input{
            border:1px solid red !important;
        }
    </style>
</head>
<body>
    <div class="container">
	    <br/><br/><br/><br/>
	    <div class="col-lg-12">
	        <div class="row">           
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	            	<center>
					<?php
						if($order_status==="Success")
						{
							$order_id = $res_data['order_id'];
					        $transaction_id = $res_data['bank_ref_no'];
					        $payment_gateway = 'CCAvenue';
					        $payment_method = $res_data['payment_mode'];
					        $request_id = $res_data['tracking_id'];
					        $amount_paid = $charge['response']['total'];
					        $create_date = date('Y-m-d H:i:s');
					        $today = date('Y-m-d');
					 
					        $sql = "SELECT * FROM payment_transactions WHERE OrderNumber='$order_id'";
					        $stmt = $link->prepare($sql);
					        $stmt->execute();
					        $result = $stmt->fetch();

					        $membership_plan = $result['membership_plan'];
					        $membership_plan_name = $result['membership_plan_name'];
					        $featured_listing = $result['featured_listing'];
					        $package_month = $result['tenure'];
					        $status = $result['status'];

					        if($status=='0')
					        {
					            if($membership_plan=='Yes')
					            {
					                $sql_chk_expiry = "SELECT * FROM payment_transactions where userid = '$userid' AND LOWER(membership_plan_name)=LOWER('$membership_plan_name') AND membership_plan_expiry_date>'$today' AND status='1' ORDER BY id DESC";
					                $stmt_chk_expiry = $link->prepare($sql_chk_expiry);
					                $stmt_chk_expiry->execute();
					                $count_chk_expiry = $stmt_chk_expiry->rowCount();
					                
					                if($count_chk_expiry>0)
					                {
					                    $result_chk_expiry = $stmt_chk_expiry->fetch();
					                    $plan_date = $result_chk_expiry['membership_plan_expiry_date'];
					                }
					                else
					                {
					                    $plan_date = $today;
					                }
					                
					                $membership_plan_expiry_date = date('Y-m-d', strtotime("$plan_date +".$package_month." month"));                    
					            }
					            else
					            {
					                $membership_plan_expiry_date = '0000-00-00';
					            }
					            
					            if($featured_listing=='Yes')
					            {
					                $sql_chk_expiry_fp = "SELECT * FROM payment_transactions where userid = '$userid' AND featured_listing_expiry_date>'$today' AND status='1' ORDER BY id DESC";
					                $stmt_chk_expiry_fp = $link->prepare($sql_chk_expiry_fp);
					                $stmt_chk_expiry_fp->execute();
					                $count_chk_expiry_fp = $stmt_chk_expiry_fp->rowCount();
					                
					                if($count_chk_expiry_fp>0)
					                {
					                    $result_chk_expiry_fp = $stmt_chk_expiry_fp->fetch();
					                    $plan_date_fp = $result_chk_expiry_fp['featured_listing_expiry_date'];
					                }
					                else
					                {
					                    $plan_date_fp = $today;
					                }
					                
					                $featured_listing_expiry_date = date('Y-m-d', strtotime("$plan_date_fp +".$package_month." month"));
					            }
					            else
					            {
					                $featured_listing_expiry_date = '0000-00-00';
					            }

					            
					            $sql_chk_pp = "SELECT * FROM payment_transactions where userid = '$userid' AND `request_id` = '$request_id' AND OrderNumber='$order_id'";
					            $stmt_chk_pp = $link->prepare($sql_chk_pp);
					            $stmt_chk_pp->execute();
					            $count_pp = $stmt_chk_pp->rowCount();
					            
					            if($count_pp==0)
					            {
					                
					                $sql_insert = "UPDATE payment_transactions SET membership_plan_expiry_date='$membership_plan_expiry_date',featured_listing_expiry_date='$featured_listing_expiry_date',transact_id='$transaction_id',request_id='$request_id',payment_gateway='$payment_gateway',payment_method='$payment_method',updated_at='$today',status='1' WHERE userid = '$userid' AND OrderNumber='$order_id'";
					                $link->exec($sql_insert);
					            }
					     
					            echo "<script>window.location.assign('$WebsiteBasePath/users/payment-order-receipt.php?request_id=$request_id');</script>";
					            exit; 
					        }
							
						}
						else if($order_status==="Aborted")
						{
						    echo "<center><h2><b style='color:red;'>Thank you for shopping with us. You have aborted this payment transaction.</b></h2></center><br/>";
					        echo "<center>&nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/'>Home</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/users/my-orders.php'>My Orders</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/membership-plans.php'>Purchase New Plan</a></center>";
					        exit;
						
						}
						else if($order_status==="Failure")
						{
							echo "<center><h2><b style='color:red;'>Transaction failed please try after some time.</b></h2>
					            If transaction amount was debited from your account please contact administrator for the same.</center><br/>";
					        echo "<center>&nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/'>Home</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/users/my-orders.php'>My Orders</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/membership-plans.php'>Purchase New Plan</a></center>";
					        exit;
						}
						else
						{
						    echo "<center><h2><b style='color:red;'>Security Error. Illegal access detected.</b></h2></center><br/>";
					        echo "<center>&nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/'>Home</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/users/my-orders.php'>My Orders</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href='$WebsiteBasePath/membership-plans.php'>Purchase New Plan</a></center>";
					        exit;
						
						}
					?>
					</center>
				</div>
			</div>
		</div>
	</div>
</body>
</html>