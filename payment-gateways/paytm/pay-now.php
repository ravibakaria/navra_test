<?php
	header("Pragma: no-cache");
	header("Cache-Control: no-cache");
	header("Expires: 0");
	// following files need to be included
	require_once("./lib/config_paytm.php");
	require_once("./lib/encdec_paytm.php");

	if(empty($_POST))
	{
		echo "<script>window.location.assign('../../membership-plans.php');</script>";
		exit;
	}

	$checkSum = "";
	$paramList = $product_arr = array();

	$ORDER_ID = $_POST["order_id"];
	$CUST_ID = $userid;
	$INDUSTRY_TYPE_ID = INDUSTRY_TYPE_ID;
	$CHANNEL_ID = CHANNEL_ID;
	$TXN_AMOUNT = $_POST["product_price"];
	$MSISDN = $_POST["phone"];
	$EMAIL = $_POST["email"];
	$product = $_POST["product_name"];

	// Create an array having all required parameters for creating checksum.
	$paramList["MID"] = PAYTM_MERCHANT_MID;
	$paramList["ORDER_ID"] = $ORDER_ID;
	$paramList["CUST_ID"] = $CUST_ID;
	$paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
	$paramList["CHANNEL_ID"] = $CHANNEL_ID;
	$paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
	$paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
	$paramList["CALLBACK_URL"] = $WebsiteBasePath."/payment-gateways/paytm/paytm-thank-you.php";

	$paramList["MSISDN"] = $MSISDN; //Mobile number of customer
	$paramList["EMAIL"] = $EMAIL; //Email ID of customer
	$paramList["VERIFIED_BY"] = "EMAIL"; //
	$paramList["IS_USER_VERIFIED"] = "YES"; //

	//$paramList["PAYMENT_MODE_ONLY"] = "YES";
	//$paramList["AUTH_MODE"] = "USRPWD";
	//$paramList["PAYMENT_TYPE_ID"] = "UPI"; //"CC","DC", "UPI"
	//$paramList["MERC_UNQ_REF"] = $FIRST_NAME._.$LAST_NAME; // %!#$&*() are not allow  //dynamically value pass
	//$paramList["MERC_UNQ_REF"] = $INDUSTRY_TYPE_ID;

	//Here checksum string will return by getChecksumFromArray() function.
	$checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);

	$product_arr = explode('_',$product);
    $premium_plan = $product_arr[0];
    $featured_plan = $product_arr[1];
    $package_month = $product_arr[2];

    if($premium_plan!='NONPREMIUM')
    {
    	$membership_plan = 'Yes';
    	$membership_plan_name = $premium_plan;
    	$membership_plan_id = getMembershipPlanId($premium_plan);
    	$membership_contacts = getMembershipPlanContacts($premium_plan);
    	$membership_plan_amount = getMembershipPlanAmount($premium_plan);
    }
    else
    {
    	$membership_plan = 'No';
    	$membership_plan_name = NULL;
    	$membership_plan_id = 0;
    	$membership_contacts = 0;
    	$membership_plan_amount = '0.00';
    }

    if($featured_plan!='NONFEATURED')
    {
    	$featured_listing = "Yes";
    	$featured_listing_amount = $package_month * getFeaturedListingPrice();
    }
    else
    {
    	$featured_listing = "No";
    	$featured_listing_amount = '0.00';
    }

    $tax_applied = getTaxAppliedOrNot();  // check tax applied or not
    if($tax_applied=='' || $tax_applied==null)
    {
    	$tax_applied='0';
    }

    if($tax_applied=='1')
    {
    	$tax_name = getTaxName();
    	$tax_percent = getTaxPercent();
    	$tax_amount = number_format(round((($membership_plan_amount+$featured_listing_amount) * $tax_percent)/100,2),2,'.','');
    	$total_amount = number_format(round(($membership_plan_amount+$featured_listing_amount)+$tax_amount,2),2,'.','');
    }
    else
    {
    	$tax_applied='0';
    	$tax_name = 'NA';
    	$tax_percent = 0;
    	$tax_amount = '0.00';
    	$total_amount = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');
    }

    $sql_insert_order = "INSERT INTO payment_transactions(`userid`,`OrderNumber`,`membership_plan`,`membership_plan_name`,`membership_plan_id`,`membership_contacts`,`membership_plan_amount`,`featured_listing`,`featured_listing_amount`,`tax_applied`,`tax_name`,`tax_percent`,`tax_amount`,`total_amount`,`tenure`,`created_at`,`status`) VALUES('$userid','$ORDER_ID','$membership_plan','$membership_plan_name','$membership_plan_id','$membership_contacts','$membership_plan_amount','$featured_listing','$featured_listing_amount','$tax_applied','$tax_name','$tax_percent','$tax_amount','$total_amount','$package_month',now(),'0')";
    $link->exec($sql_insert_order);
?>
<html>
<head>
<title>Merchant Check Out Page</title>
</head>
<body>
	<center><h1>Please do not refresh this page...</h1></center>
		<form method="post" action="<?php echo $PAYTM_TXN_URL; ?>" name="f1">
		<table border="1">
			<tbody>
			<?php
			foreach($paramList as $name => $value) {
				echo '<input type="hidden" name="' . $name .'" value="' . $value . '">';
			}
			?>
			<input type="hidden" name="CHECKSUMHASH" value="<?php echo $checkSum ?>">
			</tbody>
		</table>
		<script type="text/javascript">
			document.f1.submit();
		</script>
	</form>
</body>
</html>