<?php

if(!isset($_SESSION))
{
    session_start();
}

require_once '../../config/config.php'; 
include("../../config/dbconnect.php"); 
require_once '../../config/functions.php'; 
require_once '../../config/setup-values.php'; 

$logged_in = CheckUserLoggedIn();
if($logged_in=='0')
{
	echo "<script>window.location.assign('../../login.php');</script>";
	exit;
}

$userid = $_SESSION['user_id'];

$WebsiteBasePath = getWebsiteBasePath();
$adminEmail = getCompanyEmail();

$pt_activated_mode = getPaytmActivatedMode();
$PAYTM_MERCHANT_KEY = getPtMerchantSecreteKey($pt_activated_mode);
$PAYTM_MERCHANT_MID = getPtMerchantId($pt_activated_mode);
$PAYTM_WEBSITE_NAME = getPtWebsiteName($pt_activated_mode);
$PAYTM_INSUSTRY_TYPE = getPtIndustryType($pt_activated_mode);

/*

- Use PAYTM_ENVIRONMENT as 'PROD' if you wanted to do transaction in production environment else 'TEST' for doing transaction in testing environment.
- Change the value of PAYTM_MERCHANT_KEY constant with details received from Paytm.
- Change the value of PAYTM_MERCHANT_MID constant with details received from Paytm.
- Change the value of PAYTM_MERCHANT_WEBSITE constant with details received from Paytm.
- Above details will be different for testing and production environment.

*/
define('PAYTM_ENVIRONMENT', $pt_activated_mode); // TEST or PROD
define('PAYTM_MERCHANT_KEY', $PAYTM_MERCHANT_KEY); //Change this constant's value with Merchant key downloaded from portal
define('PAYTM_MERCHANT_MID', $PAYTM_MERCHANT_MID); //Change this constant's value with MID (Merchant ID) received from Paytm
define('PAYTM_MERCHANT_WEBSITE', $PAYTM_WEBSITE_NAME); //Change this constant's value with Website name received from Paytm
define('INDUSTRY_TYPE_ID', $PAYTM_INSUSTRY_TYPE); //Change this constant's value received from Paytm
define('CHANNEL_ID', 'WEB'); 

/*$PAYTM_DOMAIN = "pguat.paytm.com";
if (PAYTM_ENVIRONMENT == 'PROD') {
	$PAYTM_DOMAIN = 'secure.paytm.in';
}

define('PAYTM_REFUND_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/REFUND');
define('PAYTM_STATUS_QUERY_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/TXNSTATUS');
define('PAYTM_STATUS_QUERY_NEW_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/getTxnStatus');
define('PAYTM_TXN_URL', 'https://'.$PAYTM_DOMAIN.'/oltp-web/processTransaction');*/

$PAYTM_STATUS_QUERY_NEW_URL='https://securegw-stage.paytm.in/merchant-status/getTxnStatus';
$PAYTM_TXN_URL='https://securegw-stage.paytm.in/theia/processTransaction';
if (PAYTM_ENVIRONMENT == 'PROD') {
	$PAYTM_STATUS_QUERY_NEW_URL='https://securegw.paytm.in/merchant-status/getTxnStatus';
	$PAYTM_TXN_URL='https://securegw.paytm.in/theia/processTransaction';
}
define('PAYTM_REFUND_URL', '');
define('PAYTM_STATUS_QUERY_URL', $PAYTM_STATUS_QUERY_NEW_URL);
define('PAYTM_STATUS_QUERY_NEW_URL', $PAYTM_STATUS_QUERY_NEW_URL);
define('PAYTM_TXN_URL', $PAYTM_TXN_URL);


?>