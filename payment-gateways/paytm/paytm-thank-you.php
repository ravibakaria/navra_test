<?php
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

// following files need to be included
require_once("./lib/config_paytm.php");
require_once("./lib/encdec_paytm.php");

if(empty($_POST))
{
	echo "<script>window.location.assign('../../membership-plans.php');</script>";
	exit;
}

$paytmChecksum = "";
$paramList = array();
$isValidChecksum = "FALSE";

$paramList = $_POST;
$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>
		<?php
			$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
			$string = str_replace("-", " ", $a);
			echo $title = ucwords($string);
		?>  -  <?php echo getWebsiteTitle(); ?>
	</title>
</head>
<body>
<section>
  <br/><br/><br/><br/><br/>
</section>
<?php
if($isValidChecksum == "TRUE")
{
	if ($_POST["STATUS"] == "TXN_SUCCESS") 
	{
?>	
		<div class="container">
			<div class="row text-center">
		        <div class="col-sm-6 col-sm-offset-3">
					<?php
						try 
					    {
					    	$order_id = $_POST['ORDERID'];
						    $transaction_id = $_POST["TXNID"];
						    $payment_gateway = 'Paytm-'.$_POST["GATEWAYNAME"];
						    $payment_method = $_POST['PAYMENTMODE'];
						    $request_id = $transaction_id;
						    $amount_paid = $_POST['TXNAMOUNT'];
						    $create_date = date('Y-m-d H:i:s');
						    $today = date('Y-m-d');
					 
			                $sql = "SELECT * FROM payment_transactions WHERE OrderNumber='$order_id'";
			                $stmt = $link->prepare($sql);
			                $stmt->execute();
			                $result = $stmt->fetch();

			                $membership_plan = $result['membership_plan'];
			                $membership_plan_name = $result['membership_plan_name'];
			                $featured_listing = $result['featured_listing'];
			                $package_month = $result['tenure'];
			                $status = $result['status'];

			                if($status=='0')
			                {
			                	if($membership_plan=='Yes')
				                {
				                	$sql_chk_expiry = "SELECT * FROM payment_transactions where userid = '$userid' AND LOWER(membership_plan_name)=LOWER('$membership_plan_name') AND membership_plan_expiry_date>'$today' AND status='1' ORDER BY id DESC";
			                        $stmt_chk_expiry = $link->prepare($sql_chk_expiry);
				                    $stmt_chk_expiry->execute();
				                    $count_chk_expiry = $stmt_chk_expiry->rowCount();
				                    
				                    if($count_chk_expiry>0)
				                    {
				                        $result_chk_expiry = $stmt_chk_expiry->fetch();
				                        $plan_date = $result_chk_expiry['membership_plan_expiry_date'];
				                    }
				                    else
			        		      	{
			        		      		$plan_date = $today;
			        		      	}
			        		      	
			        		      	$membership_plan_expiry_date = date('Y-m-d', strtotime("$plan_date +".$package_month." month"));                    
				                }
				                else
				                {
				                	$membership_plan_expiry_date = '0000-00-00';
				                }
				                
				                if($featured_listing=='Yes')
				                {
				                	$sql_chk_expiry_fp = "SELECT * FROM payment_transactions where userid = '$userid' AND featured_listing_expiry_date>'$today' AND status='1' ORDER BY id DESC";
			                        $stmt_chk_expiry_fp = $link->prepare($sql_chk_expiry_fp);
				                    $stmt_chk_expiry_fp->execute();
				                    $count_chk_expiry_fp = $stmt_chk_expiry_fp->rowCount();
				                    
				                    if($count_chk_expiry_fp>0)
				                    {
				                        $result_chk_expiry_fp = $stmt_chk_expiry_fp->fetch();
				      		            $plan_date_fp = $result_chk_expiry_fp['featured_listing_expiry_date'];
				                    }
				                    else
			        		      	{
			        		      		$plan_date_fp = $today;
			        		      	}
			        		      	
			        		      	$featured_listing_expiry_date = date('Y-m-d', strtotime("$plan_date_fp +".$package_month." month"));
				                }
				                else
				                {
				                	$featured_listing_expiry_date = '0000-00-00';
				                }

				                
				                $sql_chk_pp = "SELECT * FROM payment_transactions where userid = '$userid' AND `request_id` = '$request_id' AND OrderNumber='$order_id'";
			                    $stmt_chk_pp = $link->prepare($sql_chk_pp);
			                    $stmt_chk_pp->execute();
			                    $count_pp = $stmt_chk_pp->rowCount();
			                    
				      		    if($count_pp==0)
			                    {
			                        
			        		      	$sql_insert = "UPDATE payment_transactions SET membership_plan_expiry_date='$membership_plan_expiry_date',featured_listing_expiry_date='$featured_listing_expiry_date',transact_id='$transaction_id',request_id='$request_id',payment_gateway='$payment_gateway',payment_method='$payment_method',updated_at='$today',status='1' WHERE userid = '$userid' AND OrderNumber='$order_id'";
			                        $link->exec($sql_insert);
			                    }
			             
				                echo "<script>window.location.assign('$WebsiteBasePath/users/payment-order-receipt.php?request_id=$request_id');</script>";
				                exit;   
			                }     
			                       
					    }
					    catch (Exception $e) 
					    {
					    	echo "<center><h2><b style='color:red;'>Transaction Failed - Payment unsuccessful</b></h2></center><br>
							<center><a href='$WebsiteBasePath/users/'>Back to Home  | <a href='$WebsiteBasePath/membership-plans.php'>Back to purchase plan</a></center>";
							exit;
						}
					?>
				</div>  
			</div>
		</div>
<?php
	}
	else 
	{
		$order_id = $_POST['ORDERID'];

		$sql_insert = "UPDATE payment_transactions SET status='2' WHERE userid = '$userid' AND OrderNumber='$order_id'";
        $link->exec($sql_insert);

		echo "<center><h2><b style='color:red;'>Transaction status is failure</b></h2></center><br/>";
		echo "<center><a href='$WebsiteBasePath/users/'>Back to Home </center>";
		exit;
	}
}
else
{
	echo "<center><h2><b style='color:red;'>Checksum mismatched. <br>Please email at <a href='$adminEmail'>".$adminEmail."</a></b></h2></center><br/>";

	echo "<center><a href='$WebsiteBasePath/users/'>Back to Home </center>";
	exit;
}
?>
<br/><br/>
</section>
</body>
</html>