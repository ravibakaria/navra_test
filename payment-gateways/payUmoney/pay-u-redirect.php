<?php
  if(!isset($_SESSION))
  {
      session_start();
  }

  require_once '../../config/config.php'; 
  include("../../config/dbconnect.php"); 
  require_once '../../config/functions.php'; 
  require_once '../../config/setup-values.php'; 

  $logged_in = CheckUserLoggedIn();
  if($logged_in=='0')
  {
    echo "<script>window.location.assign('../../login.php');</script>";
    exit;
  }

  if(empty($_POST))
  {
    echo "<script>window.location.assign('../../membership-plans.php');</script>";
    exit;
  }

  $userid = $_SESSION['user_id'];

  $WebsiteBasePath = getWebsiteBasePath();

  $pm_activated_mode = getPayUmoneyActivatedMode();
  $MERCHANT_KEY = getPmMerchantKey($pm_activated_mode);
  $SALT = getPmMerchantSalt($pm_activated_mode);
  // Merchant Key and Salt as provided by Payu.

  $surl = $WebsiteBasePath."/payment-gateways/payUmoney/pay-u-money-thank-you.php";
  $furl = $WebsiteBasePath."/payment-gateways/payUmoney/pay-u-money-failure.php";

  if($pm_activated_mode=='TEST')
  {
    $PAYU_BASE_URL = "https://sandboxsecure.payu.in";   // For Sandbox Mode
  }
	else
  if($pm_activated_mode=='PROD')
  {
    $PAYU_BASE_URL = "https://secure.payu.in";      // For Production Mode
  }
  
  $action = '';

  $posted = array();
  if(!empty($_POST)) 
  {
    //print_r($_POST);
    foreach($_POST as $key => $value) 
    {    
      $posted[$key] = $value; 
    }
  }

  $MERCHANT_KEY = $posted['key'];
  $price = $posted['amount'];
  $name = $posted['firstname'];
  $email = $posted['email'];
  $phone = $posted['phone'];
  $product = $posted['productinfo'];
  $txnid = $posted['txnid'];
  $surl = $posted['surl'];
  $furl = $posted['furl'];

  $formError = 0;


  $hash = '';
  // Hash Sequence
  $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
  if(empty($posted['hash']) && sizeof($posted) > 0) 
  {
    if(
    empty($posted['key'])
    || empty($posted['txnid'])
    || empty($posted['amount'])
    || empty($posted['firstname'])
    || empty($posted['email'])
    || empty($posted['phone'])
    || empty($posted['productinfo'])
    || empty($posted['surl'])
    || empty($posted['furl'])
    || empty($posted['service_provider'])
    ) 
    {
      $formError = 1;
    } 
    else 
    {
      //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
      $hashVarsSeq = explode('|', $hashSequence);
      $hash_string = '';  
      foreach($hashVarsSeq as $hash_var) 
      {
        $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
        $hash_string .= '|';
      }

      $hash_string .= $SALT;


      $hash = strtolower(hash('sha512', $hash_string));
      $action = $PAYU_BASE_URL . '/_payment';
    }
  } 
  elseif(!empty($posted['hash'])) 
  {
    $hash = $posted['hash'];
    $action = $PAYU_BASE_URL . '/_payment';
  }

    
?>

<html>
  <head>
  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  </head>
  <body onload="submitPayuForm()">
    <form action="<?php echo $action; ?>" method="post" name="payuForm">
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="amount" value="<?php echo $price ?>" />
      <input type="hidden" name="firstname" value="<?php echo $name ?>" />
      <input type="hidden" name="email" value="<?php echo $email ?>" />
      <input type="hidden" name="phone" value="<?php echo $phone ?>" />
      <input type="hidden" name="productinfo" value="<?php echo $product ?>" />
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
      <input type="hidden" name="surl" value="<?php echo $surl; ?>" />
      <input type="hidden" name="furl" value="<?php echo $furl ?>" />
      <input type="hidden" name="service_provider" value="payu_paisa" />
    </form>
  </body>
</html>