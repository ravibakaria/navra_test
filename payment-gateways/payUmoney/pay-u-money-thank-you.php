<?php
  if(!isset($_SESSION))
  {
      session_start();
  }
  
  require_once "../../config/config.php";
  require_once "../../config/dbconnect.php";
  include "../../config/functions.php";
  include "../../config/setup-values.php";
  
  $logged_in = CheckUserLoggedIn();
  if($logged_in=='0')
  {
    echo "<script>window.location.assign('../../login.php');</script>";
    exit;
  }

  $userid = $_SESSION['user_id'];

  $WebsiteBasePath = getWebsiteBasePath();

  $pm_activated_mode = getPayUmoneyActivatedMode();
  $MERCHANT_KEY = getPmMerchantKey($pm_activated_mode);
  $SALT = getPmMerchantSalt($pm_activated_mode);
  
  if(empty($_POST))
  {
    echo "<script>window.location.assign('../../membership-plans.php');</script>";
    exit;
  }

  $status=$_POST["status"];
  $firstname=$_POST["firstname"];
  $amount=$_POST["amount"];
  $txnid=$_POST["txnid"];
  $posted_hash=$_POST["hash"];
  $key=$_POST["key"];
  $productinfo=$_POST["productinfo"];
  $email=$_POST["email"];
  $salt=$SALT;
  
  // Salt should be same Post Request 

  If (isset($_POST["additionalCharges"])) 
  {
    $additionalCharges=$_POST["additionalCharges"];
    $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
  }
  else
  {
    $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
  }

  $hash = hash("sha512", $retHashSeq);
  if ($hash != $posted_hash) 
  {
    echo "Invalid Transaction. Please try again";
  } 
  else 
  {
    if($status=='success')
    {
        $order_id = $txnid;
      
        $transaction_id = $_POST['encryptedPaymentId'];
        $payment_gateway = 'PayUMoney';
        $payment_method = $_POST['mode'];
        $request_id = $_POST['encryptedPaymentId'];

        $amount_paid = $_POST['amount'];
        $create_date = $_POST['addedon'];
        $today = date('Y-m-d');

        $sql = "SELECT * FROM payment_transactions WHERE OrderNumber='$order_id'";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $membership_plan = $result['membership_plan'];
        $membership_plan_name = $result['membership_plan_name'];
        $featured_listing = $result['featured_listing'];
        $package_month = $result['tenure'];
        $status = $result['status'];

        if($status=='0')
        {
          if($membership_plan=='Yes')
          {
              $sql_chk_expiry = "SELECT * FROM payment_transactions where userid = '$userid' AND LOWER(membership_plan_name)=LOWER('$membership_plan_name') AND membership_plan_expiry_date>'$today' AND status='1' ORDER BY id DESC";
              $stmt_chk_expiry = $link->prepare($sql_chk_expiry);
              $stmt_chk_expiry->execute();
              $count_chk_expiry = $stmt_chk_expiry->rowCount();
              
              if($count_chk_expiry>0)
              {
                  $result_chk_expiry = $stmt_chk_expiry->fetch();
                  $plan_date = $result_chk_expiry['membership_plan_expiry_date'];
              }
              else
            {
              $plan_date = $today;
            }
            
            $membership_plan_expiry_date = date('Y-m-d', strtotime("$plan_date +".$package_month." month"));                    
          }
          else
          {
            $membership_plan_expiry_date = '0000-00-00';
          }
          
          if($featured_listing=='Yes')
          {
            $sql_chk_expiry_fp = "SELECT * FROM payment_transactions where userid = '$userid' AND featured_listing_expiry_date>'$today' AND status='1' ORDER BY id DESC";
                $stmt_chk_expiry_fp = $link->prepare($sql_chk_expiry_fp);
              $stmt_chk_expiry_fp->execute();
              $count_chk_expiry_fp = $stmt_chk_expiry_fp->rowCount();
              
              if($count_chk_expiry_fp>0)
              {
                  $result_chk_expiry_fp = $stmt_chk_expiry_fp->fetch();
                $plan_date_fp = $result_chk_expiry_fp['featured_listing_expiry_date'];
              }
              else
            {
              $plan_date_fp = $today;
            }
            
            $featured_listing_expiry_date = date('Y-m-d', strtotime("$plan_date_fp +".$package_month." month"));
          }
          else
          {
            $featured_listing_expiry_date = '0000-00-00';
          }

          
          $sql_chk_pp = "SELECT * FROM payment_transactions where userid = '$userid' AND `request_id` = '$request_id' AND OrderNumber='$order_id'";
            $stmt_chk_pp = $link->prepare($sql_chk_pp);
            $stmt_chk_pp->execute();
            $count_pp = $stmt_chk_pp->rowCount();
            
          if($count_pp==0)
          {
              
          $sql_insert = "UPDATE payment_transactions SET membership_plan_expiry_date='$membership_plan_expiry_date',featured_listing_expiry_date='$featured_listing_expiry_date',transact_id='$transaction_id',request_id='$request_id',payment_gateway='$payment_gateway',payment_method='$payment_method',created_at=now(),updated_at=now(),status='1' WHERE userid = '$userid' AND OrderNumber='$order_id'";
              $link->exec($sql_insert);
          }
     
          echo "<script>window.location.assign('../../users/payment-order-receipt.php?request_id=$request_id');</script>";
          exit;
        } 
      
    }
    else
    {
        echo "<h3>Thank You. Your order status is ". $status .".</h3>";
    }
  }
?>	