<?php
	if(!isset($_SESSION))
  	{
      	session_start();
  	}

	require_once '../../config/config.php'; 
	include("../../config/dbconnect.php"); 
	require_once '../../config/functions.php'; 
	require_once '../../config/setup-values.php'; 
	
	$WebsiteBasePath = getWebsiteBasePath();
	$siteTitle = getWebsiteTitle();
	$siteDescription = getWebSiteTagline();

	$logged_in = CheckUserLoggedIn();
	if($logged_in=='0')
	{
	    echo "<script>window.location.assign('$WebsiteBasePath/login.php');</script>";
	    exit;
	}

	$userid = $_SESSION['user_id'];
  
  	$pp_activated_mode = getPaypalActivatedMode();  	
  	$PayPal_CLIENT_ID = getPaypalClientID($pp_activated_mode);
  	$PayPal_SECRET = getPaypalSecret($pp_activated_mode);
  	
  	if($pp_activated_mode=='TEST')
  	{
  		$paypal_payment_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
  	}
  	else
  	if($pp_activated_mode=='PROD')
  	{
  		$paypal_payment_url = "https://www.paypal.com/cgi-bin/webscr";
  	}

  	$DefaultCurrency = getDefaultCurrency();
    $CURRENCY = getDefaultCurrencyCode($DefaultCurrency);

	//These should be commented out in production
	// This is for error reporting
	// Add it to config.php to report any errors
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
