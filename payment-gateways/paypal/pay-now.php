<?php 
	include_once("paypal-config.php");

	if(empty($_POST))
	{
	    echo "<script>window.location.assign('$WebsiteBasePath/membership-plans.php');</script>";
	    exit;
	}

	if(isset($_SESSION['order_id']) && ($_SESSION['order_id']!='' || $_SESSION['order_id']!=null))
	{
		unset($_SESSION['order_id']);
	}

	$order_id_NB = $_POST['order_id'];
	$full_name = $_POST['full_name'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$product_name = $_POST['product_name'];
	$product_price = $_POST['product_price'];

 	$today = date('Y-m-d H:i:s');
	$ORDER_ID = $_POST["order_id"];
	$_SESSION['order_id']=$ORDER_ID;
	$product = $_POST["product_name"];

	$product_arr = explode('_',$product);
    $premium_plan = $product_arr[0];
    $featured_plan = $product_arr[1];
    $package_month = $product_arr[2];

    if($premium_plan!='NONPREMIUM')
    {
    	$membership_plan = 'Yes';
    	$membership_plan_name = $premium_plan;
    	$membership_plan_id = getMembershipPlanId($premium_plan);
    	$membership_contacts = getMembershipPlanContacts($premium_plan);
    	$membership_plan_amount = getMembershipPlanAmount($premium_plan);
    }
    else
    {
    	$membership_plan = 'No';
    	$membership_plan_name = NULL;
    	$membership_plan_id = 0;
    	$membership_contacts = 0;
    	$membership_plan_amount = '0.00';
    }

    if($featured_plan!='NONFEATURED')
    {
    	$featured_listing = "Yes";
    	$featured_listing_amount = $package_month * getFeaturedListingPrice();
    }
    else
    {
    	$featured_listing = "No";
    	$featured_listing_amount = '0.00';
    }

    $tax_applied = getTaxAppliedOrNot();  // check tax applied or not
    if($tax_applied=='' || $tax_applied==null)
    {
    	$tax_applied='0';
    }

    if($tax_applied=='1')
    {
    	$tax_name = getTaxName();
    	$tax_percent = getTaxPercent();
    	$tax_amount = number_format(round((($membership_plan_amount+$featured_listing_amount) * $tax_percent)/100,2),2,'.','');
    	$total_amount = number_format(round(($membership_plan_amount+$featured_listing_amount)+$tax_amount,2),2,'.','');
    }
    else
    {
    	$tax_applied='0';
    	$tax_name = 'NA';
    	$tax_percent = 0;
    	$tax_amount = '0.00';
    	$total_amount = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');
    }

    $sql_chk = "SELECT * FROM payment_transactions WHERE OrderNumber='$ORDER_ID'";
    $stmt_chk = $link->prepare($sql_chk);
    $stmt_chk->execute();
    $count_chk = $stmt_chk->rowCount();

    if($count_chk==0)
    {
	    $sql_insert_order = "INSERT INTO payment_transactions(`userid`,`OrderNumber`,`membership_plan`,`membership_plan_name`,`membership_plan_id`,`membership_contacts`,`membership_plan_amount`,`featured_listing`,`featured_listing_amount`,`tax_applied`,`tax_name`,`tax_percent`,`tax_amount`,`total_amount`,`tenure`,`created_at`,`status`) VALUES('$userid','$ORDER_ID','$membership_plan','$membership_plan_name','$membership_plan_id','$membership_contacts','$membership_plan_amount','$featured_listing','$featured_listing_amount','$tax_applied','$tax_name','$tax_percent','$tax_amount','$total_amount','$package_month','$today','0')";
	    $link->exec($sql_insert_order);
	}
?>

<title>
    <?php
        $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
        $string = str_replace("-", " ", $a);
        echo $title = ucwords($string);
    ?>  -  <?php echo getWebsiteTitle(); ?>
</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<div class="container">
	<br/><br/><br/><br/>
	<center>
		<h3>Please verify your details below.<br>
	    Click on <b>Buy Now</b> button to proceed.</h3>
	</center>
	<div class="col-lg-12">
		<div class="row">			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<form action="<?php echo $paypal_payment_url; ?>" method="post">
					<table border="1px" class="table table-responsive table-bordered">
				        <tr>
				          	<th>Full Name</th>
				          	<td><?php echo $full_name;?></td>
				        </tr>
				        <tr>
				          	<th>Email</th>
				          	<td><?php echo $email;?></td>
				        </tr>
				        <tr>
				          	<th>Mobile</th>
				          	<td><?php echo $phone;?></td>
				        </tr>
				        <tr>
				          	<th>Product Description</th>
				          	<td>
				            <?php 
				                if($premium_plan!='NONPREMIUM')
				                {
				                  echo "<strong>Product Name:</strong> ".$premium_plan." plan<br>";
				                  echo "<strong>Validity in months:</strong> ".$package_month." month<br>";
				                }
				                else
				                if($featured_plan!='NONFEATURED')
				                {
				                  echo "<strong>Product Name:</strong> Featured Listing<br>";
				                  echo "<strong>Validity in months:</strong> ".$package_month." month<br>";
				                }
				            ?>
				              
				          	</td>
				        </tr>
				        <tr>
				          	<th>Product Price</th>
				          	<td><?php echo $CURRENCY.' '.$product_price;?></td>
				        </tr>
				        <tr>
				          	<td>
				          		<center><a href="<?php echo $WebsiteBasePath?>/membership-plans.php" class="btn btn-default">Cancel</a></center>
				          	</td>
							<td>
								<?php require 'paypalButton.php'; ?>
							</td>
				        </tr>
				 	</table>
				</form>
			</div>
		</div>		
	</div>	
</div>