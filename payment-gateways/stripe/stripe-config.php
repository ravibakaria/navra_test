<?php
	if(!isset($_SESSION))
	{
    	session_start();
	}

	require_once('vendor/autoload.php');

	require_once '../../config/config.php'; 
	include("../../config/dbconnect.php"); 
	require_once '../../config/functions.php'; 
	require_once '../../config/setup-values.php'; 


	$logged_in = CheckUserLoggedIn();
	if($logged_in=='0')
	{
	    echo "<script>window.location.assign('../../login.php');</script>";
	    exit;
	}

	$userid = $_SESSION['user_id'];
	
	$WebsiteBasePath = getWebsiteBasePath();
	$siteTitle = getWebsiteTitle();
	$siteDescription = getWebSiteTagline();

	$sp_activated_mode = getStripeActivatedMode();
	$sp_PublishableKey = getStripePublishableKey($sp_activated_mode);
	$sp_SecretKey = getStripeSecretKey($sp_activated_mode);

	$stripe = array(
		"secret_key" => $sp_SecretKey,
		"publishable_key" =>$sp_PublishableKey
	);

	\Stripe\Stripe::setApiKey($stripe['secret_key']);
	
	$DefaultCurrency = getDefaultCurrency();
	$CURRENCY = getDefaultCurrencyCode($DefaultCurrency);

	$username = getUserFirstName($userid).' '.getUserLastName($userid);
	$userEmail = getUserEmail($userid);
	$userContact = getUserMobile($userid);

	$city_id = getUserCityId($userid);
	$user_city = getUserCityName($city_id);
	$state_id = getUserStateId($userid);
	$user_state = getUserStateName($state_id);
	$country_id = getUserCountryId($userid);
	$user_country = getUserCountryName($country_id);
?>