<?php
	include_once("stripe-config.php");

	if(empty($_POST))
	{
	    echo "<script>window.location.assign('../../membership-plans.php');</script>";
	    exit;
	}

	$order_id_NB = $_POST['order_id'];
	$full_name = $_POST['full_name'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$product_name = $_POST['product_name'];
	$product_price = $_POST['product_price'];

 	$today = date('Y-m-d H:i:s');
	$ORDER_ID = $_POST["order_id"];
	$_SESSION['order_id']=$ORDER_ID;
	$product = $_POST["product_name"];

	$product_arr = explode('_',$product);
    $premium_plan = $product_arr[0];
    $featured_plan = $product_arr[1];
    $package_month = $product_arr[2];

    if($premium_plan!='NONPREMIUM')
    {
    	$membership_plan = 'Yes';
    	$membership_plan_name = $premium_plan;
    	$membership_plan_id = getMembershipPlanId($premium_plan);
    	$membership_contacts = getMembershipPlanContacts($premium_plan);
    	$membership_plan_amount = getMembershipPlanAmount($premium_plan);
    }
    else
    {
    	$membership_plan = 'No';
    	$membership_plan_name = NULL;
    	$membership_plan_id = 0;
    	$membership_contacts = 0;
    	$membership_plan_amount = '0.00';
    }

    if($featured_plan!='NONFEATURED')
    {
    	$featured_listing = "Yes";
    	$featured_listing_amount = $package_month * getFeaturedListingPrice();
    }
    else
    {
    	$featured_listing = "No";
    	$featured_listing_amount = '0.00';
    }

    $tax_applied = getTaxAppliedOrNot();  // check tax applied or not
    if($tax_applied=='' || $tax_applied==null)
    {
    	$tax_applied='0';
    }

    if($tax_applied=='1')
    {
    	$tax_name = getTaxName();
    	$tax_percent = getTaxPercent();
    	$tax_amount = number_format(round((($membership_plan_amount+$featured_listing_amount) * $tax_percent)/100,2),2,'.','');
    	$total_amount = number_format(round(($membership_plan_amount+$featured_listing_amount)+$tax_amount,2),2,'.','');
    }
    else
    {
    	$tax_applied='0';
    	$tax_name = 'NA';
    	$tax_percent = 0;
    	$tax_amount = '0.00';
    	$total_amount = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');
    }

    $sql_chk = "SELECT * FROM payment_transactions WHERE OrderNumber='$ORDER_ID'";
    $stmt_chk = $link->prepare($sql_chk);
    $stmt_chk->execute();
    $count_chk = $stmt_chk->rowCount();

    if($count_chk==0)
    {
	    $sql_insert_order = "INSERT INTO payment_transactions(`userid`,`OrderNumber`,`membership_plan`,`membership_plan_name`,`membership_plan_id`,`membership_contacts`,`membership_plan_amount`,`featured_listing`,`featured_listing_amount`,`tax_applied`,`tax_name`,`tax_percent`,`tax_amount`,`total_amount`,`tenure`,`created_at`,`status`) VALUES('$userid','$ORDER_ID','$membership_plan','$membership_plan_name','$membership_plan_id','$membership_contacts','$membership_plan_amount','$featured_listing','$featured_listing_amount','$tax_applied','$tax_name','$tax_percent','$tax_amount','$total_amount','$package_month','$today','0')";
	    $link->exec($sql_insert_order);
	}	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        <?php
            $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
            $string = str_replace("-", " ", $a);
            echo $title = ucwords($string);
        ?>  -  <?php echo getWebsiteTitle(); ?>
    </title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="css/cards.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <style>
        .error_input{
            border:1px solid red !important;
        }
    </style>
</head>
<body>
    <div class="container">
    <br/><br/><br/><br/>
    <div class="col-lg-12">
        <div class="row"> 
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>        
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center>
                    <h1>Please verify details below.<br></h1>
                </center>
                
                <table border="1px" class="table table-responsive table-bordered">
                    <tr>
                        <th>Full Name</th>
                        <td><?php echo $full_name;?></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><?php echo $email;?></td>
                    </tr>
                    <tr>
                        <th>Mobile</th>
                        <td><?php echo $phone;?></td>
                    </tr>
                    <tr>
                        <th>Product Description</th>
                        <td>
                        <?php 
                            if($premium_plan!='NONPREMIUM')
                            {
                              echo "<strong>Product Name:</strong> ".$premium_plan." plan<br>";
                              echo "<strong>Validity in months:</strong> ".$package_month." month<br>";
                            }
                            else
                            if($featured_plan!='NONFEATURED')
                            {
                              echo "<strong>Product Name:</strong> Featured Listing<br>";
                              echo "<strong>Validity in months:</strong> ".$package_month." month<br>";
                            }
                        ?>
                          
                        </td>
                    </tr>
                    <tr>
                        <th>Product Price</th>
                        <td><?php echo $CURRENCY.' '.$product_price;?></td>
                    </tr>
                    <tr>
                        <td><center><a href="<?php echo $WebsiteBasePath?>/membership-plans.php" class="btn btn-default">Cancel</a></center></td>   
                        <td>
                            <form action="stripe-thank-you.php" method="post">
                                <input type="hidden" name="product_name" value="<?php echo $product_name; ?>">
                                <input type="hidden" name="order_id" value="<?php echo $order_id_NB; ?>">
                                <input type="hidden" name="product_price" value="<?php echo $product_price; ?>">
                                <input type="hidden" name="currency_code" value="<?php echo $CURRENCY; ?>"> 
                                <input type="hidden" name="username" value="<?php echo $username; ?>">
                                <input type="hidden" name="userEmail" value="<?php echo $userEmail; ?>">
                                <input type="hidden" name="userContact" value="<?php echo $userContact; ?>">    
                                <input type="hidden" name="user_city" value="<?php echo $user_city; ?>">
                                <input type="hidden" name="user_state" value="<?php echo $user_state; ?>">
                                <input type="hidden" name="user_country" value="<?php echo $user_country; ?>">

                                <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                        data-key="<?php echo $stripe['publishable_key']; ?>"
                                        data-name="<?php echo $siteTitle; ?>"
                                        data-description="Order ID: <?php echo $order_id_NB; ?>"
                                        data-amount="<?php echo $product_price*100; ?>"
                                        data-locale="auto"
                                        data-currency="<?php echo $CURRENCY; ?>"
                                        data-zip-code="true">   
                                </script>
                            </form>
                        </td>                        
                    </tr>
                    <tr>
                        <td colspan="2"><i style="color:red;">Note: Please do not refresh page after clicking on <b>Pay with Card</b> button</i></td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>   
        </div>      
    </div>
</div>

</body>
</html>

