<?php
	
	if(!isset($_SESSION))
  	{
      	session_start();
  	}

	require_once '../../config/config.php'; 
	include("../../config/dbconnect.php"); 
	require_once '../../config/functions.php'; 
	require_once '../../config/setup-values.php'; 


	$logged_in = CheckUserLoggedIn();
	if($logged_in=='0')
	{
	    echo "<script>window.location.assign('../../login.php');</script>";
	    exit;
	}

	if(empty($_POST))
	{
	    echo "<script>window.location.assign('../../membership-plans.php');</script>";
	    exit;
	}

	$userid = $_SESSION['user_id'];
	
	$WebsiteBasePath = getWebsiteBasePath();
	$siteTitle = getWebsiteTitle();
	$siteDescription = getWebSiteTagline();
  
  	$rp_activated_mode = getRazorpayActivatedMode();
  	$KEY_ID = getRazorpayKeyId($rp_activated_mode);
  	$KEY_SECRET = getRazorpayKeySecret($rp_activated_mode);
  	
  	$DefaultCurrency = getDefaultCurrency();
    $CURRENCY = getDefaultCurrencyCode($DefaultCurrency);

	$keyId = $KEY_ID;
	$keySecret = $KEY_SECRET;
	$displayCurrency = $CURRENCY;

	//These should be commented out in production
	// This is for error reporting
	// Add it to config.php to report any errors
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
