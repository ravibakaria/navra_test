<html>
<head>
	<title>Confirm order details</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<script>
		$(document).ready(function(){
			$('.razorpay-payment-button').addClass('btn btn-success');
		});
	</script>
</head>
<body>

<?php
	require('razor-config.php');
	require('razorpay-php/Razorpay.php');

	// Create the Razorpay Order

	use Razorpay\Api\Api;

	$api = new Api($keyId, $keySecret);

	$order_id_NB = $_POST['order_id'];
	$full_name = $_POST['full_name'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$product_name = $_POST['product_name'];
	$product_price = $_POST['product_price'];

	$_SESSION['post_data'] = $_POST;
 	$today = date('Y-m-d H:i:s');
	$ORDER_ID = $_POST["order_id"];
	$product = $_POST["product_name"];

	$product_arr = explode('_',$product);
    $premium_plan = $product_arr[0];
    $featured_plan = $product_arr[1];
    $package_month = $product_arr[2];

    if($premium_plan!='NONPREMIUM')
    {
    	$membership_plan = 'Yes';
    	$membership_plan_name = $premium_plan;
    	$membership_plan_id = getMembershipPlanId($premium_plan);
    	$membership_contacts = getMembershipPlanContacts($premium_plan);
    	$membership_plan_amount = getMembershipPlanAmount($premium_plan);
    }
    else
    {
    	$membership_plan = 'No';
    	$membership_plan_name = NULL;
    	$membership_plan_id = 0;
    	$membership_contacts = 0;
    	$membership_plan_amount = '0.00';
    }

    if($featured_plan!='NONFEATURED')
    {
    	$featured_listing = "Yes";
    	$featured_listing_amount = $package_month * getFeaturedListingPrice();
    }
    else
    {
    	$featured_listing = "No";
    	$featured_listing_amount = '0.00';
    }

    $tax_applied = getTaxAppliedOrNot();  // check tax applied or not
    if($tax_applied=='' || $tax_applied==null)
    {
    	$tax_applied='0';
    }

    if($tax_applied=='1')
    {
    	$tax_name = getTaxName();
    	$tax_percent = getTaxPercent();
    	$tax_amount = number_format(round((($membership_plan_amount+$featured_listing_amount) * $tax_percent)/100,2),2,'.','');
    	$total_amount = number_format(round(($membership_plan_amount+$featured_listing_amount)+$tax_amount,2),2,'.','');
    }
    else
    {
    	$tax_applied='0';
    	$tax_name = 'NA';
    	$tax_percent = 0;
    	$tax_amount = '0.00';
    	$total_amount = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');
    }

    $sql_chk = "SELECT * FROM payment_transactions WHERE OrderNumber='$ORDER_ID'";
    $stmt_chk = $link->prepare($sql_chk);
    $stmt_chk->execute();
    $count_chk = $stmt_chk->rowCount();

    if($count_chk==0)
    {
	    $sql_insert_order = "INSERT INTO payment_transactions(`userid`,`OrderNumber`,`membership_plan`,`membership_plan_name`,`membership_plan_id`,`membership_contacts`,`membership_plan_amount`,`featured_listing`,`featured_listing_amount`,`tax_applied`,`tax_name`,`tax_percent`,`tax_amount`,`total_amount`,`tenure`,`created_at`,`status`) VALUES('$userid','$ORDER_ID','$membership_plan','$membership_plan_name','$membership_plan_id','$membership_contacts','$membership_plan_amount','$featured_listing','$featured_listing_amount','$tax_applied','$tax_name','$tax_percent','$tax_amount','$total_amount','$package_month','$today','0')";
	    $link->exec($sql_insert_order);
	}

	//
	// We create an razorpay order using orders api
	// Docs: https://docs.razorpay.com/docs/orders
	//
	$orderData = [
	    'receipt'         => $order_id_NB,
	    'amount'          => $product_price * 100, // 2000 rupees in paise
	    'currency'        => $displayCurrency,
	    'payment_capture' => 1 // auto capture
	];

	$razorpayOrder = $api->order->create($orderData);

	$razorpayOrderId = $razorpayOrder['id'];

	$_SESSION['razorpay_order_id'] = $razorpayOrderId;

	$displayAmount = $amount = $orderData['amount'];

	if ($displayCurrency !== 'INR')
	{
	    $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
	    $exchange = json_decode(file_get_contents($url), true);

	    $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
	}

	$data = [
	    "key"               => $keyId,
	    "amount"            => $amount,
	    "name"              => $siteTitle,
	    "description"       => $siteDescription,
	    "image"             => "https://www.navrabayko.com/images/favicon/NB-Favicon-1556602190.png",
	    "prefill"           => [
	    "name"              => $full_name,
	    "email"             => $email,
	    "contact"           => $phone,
	    ],
	    "notes"             => [
	    "address"           => "--",
	    "merchant_order_id" => $order_id_NB,
	    ],
	    "theme"             => [
	    "color"             => "#F37254"
	    ],
	    "order_id"          => $razorpayOrderId,
	];

	if ($displayCurrency !== 'INR')
	{
	    $data['display_currency']  = $displayCurrency;
	    $data['display_amount']    = $displayAmount;
	}

	$json = json_encode($data);

	require("checkout/checkout.php");