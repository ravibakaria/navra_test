<div class="container">
    <br/><br/>
    <center><h3>Please verify your details below.<br>
    Click on <b>Pay Now</b> button to proceed.</h3>

      <table border="1px" class="table table-responsive table-bordered">
        <tr>
          <th>Full Name</th>
          <td><?php echo $full_name;?></td>
        </tr>
        <tr>
          <th>Email</th>
          <td><?php echo $email;?></td>
        </tr>
        <tr>
          <th>Mobile</th>
          <td><?php echo $phone;?></td>
        </tr>
        <tr>
          <th>Product Description</th>
          <td>
            <?php 
                if($premium_plan!='NONPREMIUM')
                {
                  echo "<strong>Product Name:</strong> ".$premium_plan." plan<br>";
                  echo "<strong>Validity in months:</strong> ".$package_month." month<br>";
                }
                else
                if($featured_plan!='NONFEATURED')
                {
                  echo "<strong>Product Name:</strong> Featured Listing<br>";
                  echo "<strong>Validity in months:</strong> ".$package_month." month<br>";
                }
            ?>
              
          </td>
        </tr>
        <tr>
          <th>Product Price</th>
          <td><?php echo $displayCurrency.' '.$product_price;?></td>
        </tr>
        <tr>
          <td><center><a href="<?php echo $WebsiteBasePath?>/membership-plans.php" class="btn btn-default">Cancel</a></center></td>
          <td>
              <center>
                <form action="razorpay-thank-you.php" method="POST">
                  <script
                    src="https://checkout.razorpay.com/v1/checkout.js"
                    data-key="<?php echo $data['key']?>"
                    data-amount="<?php echo $data['amount']?>"
                    data-currency="INR"
                    data-name="<?php echo $data['name']?>"
                    data-image="<?php echo $data['image']?>"
                    data-description="<?php echo $data['description']?>"
                    data-prefill.name="<?php echo $data['prefill']['name']?>"
                    data-prefill.email="<?php echo $data['prefill']['email']?>"
                    data-prefill.contact="<?php echo $data['prefill']['contact']?>"
                    data-notes.shopping_order_id="<?php echo $order_id_NB; ?>"
                    data-order_id="<?php echo $data['order_id']; ?>"
                    <?php if ($displayCurrency !== 'INR') { ?> data-display_amount="<?php echo $data['display_amount']?>" <?php } ?>
                    <?php if ($displayCurrency !== 'INR') { ?> data-display_currency="<?php echo $data['display_currency']?>" <?php } ?>
                  >
                  </script>
                  <!-- Any extra fields to be submitted with the form but not sent to Razorpay -->
                  <input type="hidden" name="shopping_order_id" value="<?php echo $order_id_NB; ?>">
                </form>
              </center>
          </td>
        </tr>
      </table>
    <br/><br/>
    
  </div>

<!--  The entire list of Checkout fields is available at
 https://docs.razorpay.com/docs/checkout-form#checkout-fields -->


