<?php
	if(!isset($_SESSION))
	{
	    session_start();
	}

	require_once 'config/config.php'; 
	include("config/dbconnect.php"); 
	require_once 'config/functions.php'; 
	require_once 'config/setup-values.php'; 

	$logged_in = CheckUserLoggedIn();
	if($logged_in=='0')
	{
		echo "<script>window.location.assign('login.php');</script>";
		exit;
	}

	if(empty($_POST))
	{
		echo "<script>window.location.assign('membership-plans.php');</script>";
		exit;
	}

	$payment_method = $_POST['payment_method'];
	$order_id = getOrderId();	
	$full_name = $_POST['user_name'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$product_name = $_POST['product_name'];
	$product_price = $_POST['amount'];

	if($payment_method=='1')
	{
		$form_action = "payment-gateways/offline/pay-now.php";
	}

	if($payment_method=='2')
	{
		$form_action = "payment-gateways/bank-transfer/pay-now.php";
	}

	if($payment_method=='3')
	{
		$form_action = "payment-gateways/instamojo/pay-now.php";
	}

	if($payment_method=='4')
	{
		$form_action = "payment-gateways/paytm/pay-now.php";
	}

	if($payment_method=='5')
	{
		$form_action = "payment-gateways/payUmoney/pay-now.php";
	}

	if($payment_method=='6')
	{
		$form_action = "payment-gateways/razorpay/pay-now.php";
	}

	if($payment_method=='7')
	{
		$form_action = "payment-gateways/paypal/pay-now.php";
	}

	if($payment_method=='8')
	{
		$form_action = "payment-gateways/2checkout/pay-now.php";
	}

	if($payment_method=='9')
	{
		$form_action = "payment-gateways/stripe/pay-now.php";
	}

	if($payment_method=='10')
	{
		$form_action = "payment-gateways/ccAvenue/pay-now.php";
	}
?>

<form id='redirect_payment_gateway' name='redirect_payment_gateway' method="post" action="<?php echo $form_action;?>">
	<input type="hidden" name="order_id" value="<?php echo $order_id;?>">
	<input type="hidden" name="full_name" value="<?php echo $full_name;?>">
    <input type="hidden" name="email" value="<?php echo $email;?>">
    <input type="hidden" name="phone" value="<?php echo $phone;?>">
    <input type="hidden" name="product_name" value="<?php echo $product_name;?>">
    <input type="hidden" name="product_price" value="<?php echo $product_price;?>">
</form>

<script src="js/jquery.js"></script>
<script>
	$(document).ready(function(){
		$("#redirect_payment_gateway").submit();
	});
</script>