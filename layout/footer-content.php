<?php
    $site_url = getWebsiteBasePath();


    $about_us_display = getAboutUsSetting();
    $disclaimer_display = getDisclaimerSetting();
    $privacy_policy_display = getPrivacyPolicySetting();
    $terms_of_service_display = getTermsOfServiceSetting();
    $faq_display = getFAQSetting();

    $SitemapGenerationEnable = getSitemapGenerationEnableOrNot();
    $wedding_directory_display_or_not = getWeddingDirectoryDisplayOrNot();
    $custom_content = getWeddingDirectoryCustomContent();
?>
<footer class="">
    
    <div class="row footer-div common-row-div">
        <div class="col-md-12">
            <br/>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left  important-links margin-top">
                <b class="footer-white-text">
                    <?php 
                        echo "<h3 class='important-links footer-white-text'>Important Links</h3>";
                        echo "<ul>";

                            if($about_us_display=='1')
                            {
                                echo "<li><a href='$site_url/about-us.html' class='footer-white-text'>About Us</a></li>";
                            }

                            echo "<li><a href='$site_url/contact-us.html' class='footer-white-text'>Contact Us</a></li>";

                            if($disclaimer_display=='1')
                            {
                                echo "<li><a href='$site_url/disclaimer.html' class='footer-white-text'>Disclaimer</a></li>";
                            }

                            if($privacy_policy_display=='1')
                            {
                                echo "<li><a href='$site_url/privacy-policy.html' class='footer-white-text'>Privacy Policy</a></li>";
                            }

                            if($terms_of_service_display=='1')
                            {
                                echo "<li><a href='$site_url/terms-of-service.html' class='footer-white-text'>Terms Of Service</a></li>";
                            }

                            if($faq_display=='1')
                            {
                                echo "<li><a href='$site_url/faq.html' class='footer-white-text'>FAQ's</a></li>";
                            }

                            if($SitemapGenerationEnable=='1')
                            {
                                echo "<li><a href='$site_url/sitemap.xml' class='footer-white-text' target='_blank'>Sitemap</a></li>";
                            }

                            if($wedding_directory_display_or_not=='1')
                            {
                                echo "<li><a href='$site_url/directory/' class='footer-white-text'>Wedding Directory</a></li>";
                            }
                        echo "</ul>";
                    ?> 
                </b>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-left margin-top">
                <div class="row important-links centered-data">
                    <?php
                        $sql_social = "SELECT * FROM `social_sharing`";
                        $stmt_social= $link->prepare($sql_social); 
                        $stmt_social->execute();
                        $count_social=$stmt_social->rowCount();
                        $result_social = $stmt_social->fetch();
                        
                        $website_facebook_display = $result_social['website_facebook_display'];
                        $website_facebook_data = $result_social['website_facebook_data'];
                        $website_twitter_display = $result_social['website_twitter_display'];
                        $website_twitter_data = $result_social['website_twitter_data'];
                        $website_instagram_display = $result_social['website_instagram_display'];
                        $website_instagram_data = $result_social['website_instagram_data'];
                        $website_tumbler_display = $result_social['website_tumbler_display'];
                        $website_tumbler_data = $result_social['website_tumbler_data'];
                        $website_youtube_display = $result_social['website_youtube_display'];
                        $website_youtube_data = $result_social['website_youtube_data'];
                        $website_pinterest_display = $result_social['website_pinterest_display'];
                        $website_pinterest_data = $result_social['website_pinterest_data'];
                        $website_myspace_display = $result_social['website_myspace_display'];
                        $website_myspace_data = $result_social['website_myspace_data'];
                        $website_linkedin_display = $result_social['website_linkedin_display'];
                        $website_linkedin_data = $result_social['website_linkedin_data'];
                        $website_VKontakte_display = $result_social['website_VKontakte_display'];
                        $website_VKontakte_data = $result_social['website_VKontakte_data'];
                        $website_foursquare_display = $result_social['website_foursquare_display'];
                        $website_foursquare_data = $result_social['website_foursquare_data'];
                        $website_flicker_display = $result_social['website_flicker_display'];
                        $website_flicker_data = $result_social['website_flicker_data'];
                        $website_vine_display = $result_social['website_vine_display'];
                        $website_vine_data = $result_social['website_vine_data'];
                        $website_blogger_display = $result_social['website_blogger_display'];
                        $website_blogger_data = $result_social['website_blogger_data'];
                        $website_quora_display = $result_social['website_quora_display'];
                        $website_quora_data = $result_social['website_quora_data'];
                        $website_reddit_display = $result_social['website_reddit_display'];
                        $website_reddit_data = $result_social['website_reddit_data'];

                        if($count_social>0 && ($website_facebook_display!='0' || $website_twitter_display!='0' || $website_instagram_display!='0' || $website_tumbler_display!='0' || $website_youtube_display!='0' || $website_pinterest_display!='0' || $website_myspace_display!='0' || $website_linkedin_display!='0' || $website_VKontakte_display!='0' || $website_foursquare_display!='0' || $website_flicker_display!='0' || $website_vine_display!='0' || $website_blogger_display!='0' || $website_quora_display!='0' || $website_reddit_display!='0'))
                        {
                            echo "<h3 class='footer-white-text important-links text-left'>Follow Us</h3>";
                        }

                        if($website_facebook_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_facebook_data' target='_blank'><i class='fa fa-facebook-square fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_twitter_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_twitter_data' target='_blank'><i class='fa fa-twitter-square fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_instagram_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_instagram_data' target='_blank'><i class='fa fa-instagram fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_tumbler_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_tumbler_data' target='_blank'><i class='fa fa-tumblr-square fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_youtube_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_youtube_data' target='_blank'><i class='fa fa-youtube-play fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_pinterest_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_pinterest_data' target='_blank'><i class='fa fa-pinterest-square fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_myspace_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_myspace_data' target='_blank'><i class='fa fa-users fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_linkedin_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_linkedin_data' target='_blank'><i class='fa fa-linkedin-square fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_VKontakte_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_VKontakte_data' target='_blank'><i class='fa fa-vk fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_foursquare_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_foursquare_data' target='_blank'><i class='fa fa-foursquare fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_flicker_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_flicker_data' target='_blank'><i class='fa fa-flickr fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_vine_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_vine_data' target='_blank'><i class='fa fa-vine fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_blogger_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_blogger_data' target='_blank'><i class='fa fa-edge fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_quora_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_quora_data' target='_blank'><i class='fa fa-quora fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }

                        if($website_reddit_display=='1')
                        {
                            echo "<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1 social-sharing-div'>";
                                echo "<a href='$website_reddit_data' target='_blank'><i class='fa fa-reddit fa-2x footer-white-text' aria-hidden='true'></i></a>";
                            echo "</div>";
                        }
                    ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left  important-links margin-top">
                <?php
                    $site_url = getWebsiteBasePath();
                    $logo=array();
                    $logoURL = getLogoURL();
                    if($logoURL!='' || $logoURL!=null)
                    {
                        $logoURL = explode('/',$logoURL);
                        for($i=1;$i<count($logoURL);$i++)
                        {
                            $logo[] = $logoURL[$i];
                        }
                        $logo = implode('/',$logo);
                    }

                    if($logo=='' || $logo==null)
                    {
                        $logo = "images/logo/default-logo.jpg";
                    }

                    $stmt   = $link->prepare("SELECT * FROM `contact_us`");
                    $stmt->execute();
                    $result = $stmt->fetch();
                    $count=$stmt->rowCount();

                    if($count>0 && $result['companyName']!='')
                    {
                        $enquiry_email = $result['enquiry_email'];
                        $companyName = $result['companyName'];
                        $street = $result['street'];
                        $city = $result['city'];
                        $state = $result['state'];
                        $country = $result['country'];
                        $postalCode = $result['postalCode'];
                        $phonecode = $result['phonecode'];
                        $phone = $result['phone'];
                        $companyEmail = $result['companyEmail'];
                        $companyURL = $result['companyURL'];

                        $city_name = getUserCityName($city);
                        $state_name = getUserStateName($state);
                        $country_name = getUserCountryName($country);

                        echo "<div id='hcard-Navra-Bayko' class='vcard important-links'>
                                  <img style='float:left; margin-right:4px;height: 38px;' src='$site_url/$logo' alt='photo of $companyName' class='photo'/>
                                  <br/><br/>
                                  <a class='url fn n ' href='$companyURL'>  
                                        <span class='given-name footer-white-text'>$companyName</span>
                                  </a>
                                  <br/>
                                  <div class='adr'>
                                        <div class='street-address footer-white-text'><b>Address: </b>$street</div>
                                        <span class='locality footer-white-text'>$city_name</span>,  
                                        <span class='region footer-white-text'>$state_name</span>, 
                                        <span class='postal-code footer-white-text'>$postalCode</span>

                                        <span class='country-name footer-white-text'>$country_name</span>

                                  </div>

                                  <div class='tel footer-white-text'><b>Phone Number: </b>+$phonecode - $phone</div>
                                  
                                  <b>Email Address: </b>$companyEmail";

                            echo "</div>";        
                    }
                ?>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
            </div>
        </div>
    </div>
    <div class="row footer-div common-row-div">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <div class="row centered-data">
                <p class="footer-white-text">Copyright &copy; <?php echo date('Y'); ?> <?php echo getWebsiteTitle(); ?></p>
                <p class="footer-white-text">Designed & Developed By <a href="http://www.hiya.digital"  class="footer-white-text">Hiya Digital</a></p>
            </div>
        </div>
    </div>
</footer>

<div class="click-to-top">
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
</div>
<!-- Go to www.addthis.com/dashboard to customize your tools --> 
