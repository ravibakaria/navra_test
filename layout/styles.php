
  <!-- CSS -->
  <link rel="stylesheet" href="css/bootstrap.css" type='text/css' />
  <link rel="stylesheet" href="css/font-awesome.css" type='text/css' />
  <link rel="stylesheet" href="css/bootstrap-glyphicons.css" type='text/css' />
  <link rel="stylesheet" href="css/magnific-popup.css" type='text/css' />
  <link rel="stylesheet" href="css/datepicker3.css" type='text/css' />
  <link rel="stylesheet" href="css/theme.css" type='text/css' />
  <link rel="stylesheet" href="css/default.css" type='text/css' />
  <link rel="stylesheet" href="css/theme-custom.css" type='text/css' />
  <link rel="stylesheet" href="css/master.css" type='text/css' />
  <link rel="stylesheet" href="css/custom.css" type='text/css' />
  <link rel="stylesheet" href="config/style.php" type='text/css' />
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/bootstrap-toggle.min.css" type='text/css' />
  <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type='text/css' />
  
  <?php
    $sql_custom_meta = "SELECT * FROM `custom_meta_tags`";
    $stmt_custom_meta = $link->prepare($sql_custom_meta);
    $stmt_custom_meta->execute();
    $count_custom_meta = $stmt_custom_meta->rowCount();

    if($count_custom_meta>0)
    {
      $result_custom_meta = $stmt_custom_meta->fetchAll();
      foreach ($result_custom_meta as $row_custom_meta) 
      {
          $meta_tag = $row_custom_meta['meta_tag'];
          $meta_tag = str_replace("--lt","<",$meta_tag);

          echo $meta_tag;
          echo "\n";
      }
    }
  ?>


  <?php 
    /*********        Header Script          ************/
      $include_header_script = getHeaderScriptDisplay();
      $header_script = getHeaderScript();
      if($include_header_script!='' && $include_header_script!=null && $include_header_script!=0)
      {
        @$header_script_data = file_get_contents($websiteBasePath.'/'.$header_script);
        echo "$header_script_data";
      }
    ?>

    <script src="js/modernizr.js"></script>
    
</head>