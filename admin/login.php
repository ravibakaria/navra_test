<?php
	session_start();

	$install_file_name = 'install.php';
	$config_file_name = '../config/config.php';
	if(!file_exists($config_file_name)) 
	{
		header("Location:install.php");
		exit;
	}
	else
	if(file_exists($config_file_name) && file_exists($install_file_name)) 
	{
		//unlink($install_file_name);
		//echo "Deleting install file";
		echo "<script>alert('Deleting install file');</script>";
	}

	if(isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']!='' || $_SESSION['admin_user']!=null))
	{
		header('Location: index.php');
		exit;
	}

	include('../config/config.php');
	include('../config/dbconnect.php');
	include('../config/functions.php');

	$recaptchaAllowed = getreCaptchaAllowed();
	if($recaptchaAllowed == "1")
	{
		$recaptchaSiteKey=getreCaptchaSiteKey();
		$recaptchaSecretKey=getreCaptchaSecretKey();
	}

	$errorMessage = null;
	$successMessage = null;

	if($recaptchaAllowed == "1")
	{
		$recaptchaSiteKey=getreCaptchaSiteKey();
		$recaptchaSecretKey=getreCaptchaSecretKey();

		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		{
			//your site secret key
			$secret = $recaptchaSecretKey;
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);
			if($responseData->success)
			{
				if (isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['password']) && $_POST['password'] != '')
				{
					$email = quote_smart($_POST['email']);
			        $password = md5($_POST['password']);

			        $sql_chk = $link->prepare("SELECT * FROM `admins` WHERE (email='$email' OR username='$email') AND password='$password'"); 
			        $sql_chk->execute();
			        $count=$sql_chk->rowCount();
			        if($count>0)
			        {
			            $adminData = $sql_chk->fetch(PDO::FETCH_OBJ);
			            $admin_id = $adminData->id;
			            $admin_password = $adminData->password;
			            $admin_status = $adminData->status;
			            $admin_firstname = $adminData->firstname;
			            $IP_Address = $_SERVER['REMOTE_ADDR'];
			            if($admin_status=='0')
			            {
			                $errorMessage = "Your account is deactivate. Please contact administrator to activate your account.";
			            }
			            else
			            {
			                $sql_insert_log = "INSERT INTO `userlogs`(`userid`,`user_firstname`,`user_type`,`IP_Address`,`loggedOn`) VALUES('$admin_id','$admin_firstname','Admin','$IP_Address','$today')";
			                $link->exec($sql_insert_log);   //Inserting logis for login


			                $_SESSION['admin_logged_in'] = true;
			                $_SESSION['admin_user'] = true;
			                $_SESSION['admin_id'] = $admin_id;
			                $_SESSION['admin_firstname'] = $admin_firstname;

			                $successMessage = "success";
			            }
			        }
			        else
			        {
			            $errorMessage = "Please enter correct email id & password";
			        }
				}
			}
		}
	}	
	else
	{
		if (isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['password']) && $_POST['password'] != '')
		{
			$email = quote_smart($_POST['email']);
	        $password = md5($_POST['password']);

	        $sql_chk = $link->prepare("SELECT * FROM `admins` WHERE (email='$email' OR username='$email') AND password='$password'"); 
	        $sql_chk->execute();
	        $count=$sql_chk->rowCount();
	        if($count>0)
	        {
	            $adminData = $sql_chk->fetch(PDO::FETCH_OBJ);
	            $admin_id = $adminData->id;
	            $admin_password = $adminData->password;
	            $admin_status = $adminData->status;
	            $admin_firstname = $adminData->firstname;
	            $IP_Address = $_SERVER['REMOTE_ADDR'];
	            if($admin_status=='0')
	            {
	                $errorMessage = "Your account is deactivate. Please contact administrator to activate your account.";
	            }
	            else
	            {
	                $sql_insert_log = "INSERT INTO `userlogs`(`userid`,`user_firstname`,`user_type`,`IP_Address`,`loggedOn`) VALUES('$admin_id','$admin_firstname','Admin','$IP_Address','$today')";
	                $link->exec($sql_insert_log);   //Inserting logis for login


	                $_SESSION['admin_logged_in'] = true;
	                $_SESSION['admin_user'] = true;
	                $_SESSION['admin_id'] = $admin_id;
	                $_SESSION['admin_firstname'] = $admin_firstname;

	                $successMessage = "success";
	            }
	        }
	        else
	        {
	            $errorMessage = "Please enter correct email id & password";
	        }
		}
	}
?>
<!doctype html>
<html class="fixed">
	<head>
		<title>Login -  <?php echo getWebsiteTitle(); ?></title>
		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Admin - Responsive HTML5 Template">
		<meta name="author" content="navrabayko.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- CSS  -->
		<link rel="stylesheet" href="../css/google-fonts.css" type="text/css">
		<link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
		<link rel="stylesheet" href="../css/font-awesome.css" type="text/css">
		<link rel="stylesheet" href="../css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="../css/datepicker3.css" type="text/css">
		<link rel="stylesheet" href="../css/dataTables.bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="../css/bootstrap-toggle.min.css" type="text/css">
		<link rel="stylesheet" href="../css/theme.css" type="text/css">
		<link rel="stylesheet" href="../css/default.css" type="text/css">
		<link rel="stylesheet" href="../css/theme-custom.css" type="text/css">
		<link rel="stylesheet" href="../css/master.css" type="text/css">
		<link rel="stylesheet" href="../css/custom.css" type="text/css">
		<link rel="stylesheet" href="../css/admin.css" type="text/css">
		<link rel="stylesheet" href="../config/style.php" type='text/css'>
		<link rel="stylesheet" href="../css/bootstrap-glyphicons.css" type="text/css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
	    <link rel="stylesheet" href="../css/richtext-editor.css" type="text/css">
	    <link rel="stylesheet" href="../css/richtext.min.css" type="text/css">
	    <link rel="stylesheet" href="../css/bootstrap-multiselect.css" type="text/css">

	    <script>
			function onSubmit(token)
			{
			    document.getElementById("frmLogin").submit();
			}

			function validate(event) 
			{
			    event.preventDefault();
			    var email = $('.email').val();
				var password = $('.password').val();
				var recaptchaAllowed = $('.recaptchaAllowed').val();
				var task = "admin_login";

				if(email=='' || email==null)
				{
					$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter your email id.</div>");
					$(".email").addClass("danger_error");
					return false;
				}

				if(password=='' || password==null)
				{
					$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter password.</div>");
					$('.password').focus();
					$(".password").addClass("danger_error");
					return false;
				}

		        //recaptcha validation 
		        if(recaptchaAllowed=='1')
				{
					grecaptcha.execute();
				}
				else
				{
					document.getElementById("frmLogin").submit();
				}

				$('.loading_img').show();
				$('.form_status').html("");
			}

			function onload() 
			{
			    var element = document.getElementById('recaptcha-submit');
			    element.onclick = validate;
			}
		</script>

		<!-- Favicon  -->
	    <?php 
		    $websiteBasePath = getWebsiteBasePath();
		    if(getFaviconURL()!='' || getFaviconURL()!=null)
		    {
		      $favicon_arr=array();
		      $favicon_get = getFaviconURL();
		      if($favicon_get!='' || $favicon_get!=null)
		      {
		        $favicon_get = explode('/',$favicon_get);

		        for($i=1;$i<count($favicon_get);$i++)
		        {
		          $favicon_arr[] = $favicon_get[$i];
		        }

		        $favicon = implode('/',$favicon_arr);
		      }
		      echo "<link rel='icon' type='image/png' href='$websiteBasePath/$favicon' sizes='32x32'>";
		    }
		    else
		    {
		      echo "<link rel='icon' type='image/png' href='$websiteBasePath/images/favicon/default-favicon.png' sizes='32x32'>";
		    }
		?>

		<!-- Head Libs -->
		<script src="../js/modernizr.js"></script>
	</head>

<!-- Google recaptcha -->
<?php
	if($recaptchaAllowed == "1")
	{
		echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
	}
?>
	<body class="login-body">
		<section class="body">
			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="index.php" class="navbar-brand logo">
						<?php 
							$logo = getLogoURL();
							$sitetitle = getWebsiteTitle();
							$siteBasePath = getWebsiteBasePath();
							if($logo!='' || $logo!=null)
							{
								echo "<img src='$logo' class='img-responsive logo-img logo-img-admin'  />";
							}
							else
							{
								echo "<img src='$siteBasePath/images/logo/default-logo.jpg' class='img-responsive logo-img logo-img-admin' />";
							}
						?>
					</a>
				</div>
			
				<!-- start: search & user box -->
				
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->
		</section>			
		<!-- start: page -->
		<section class="body-sign admin-body-sign">

			<div class="center-sign admin-center-sign">

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none title-login-forgot"><i class="fa fa-user mr-xs"></i> Login</h2>
					</div>
					<div class="panel-body">
						<form name="frmLogin" id="frmLogin" autocomplete="off" method="post"  action="<?php echo $_SERVER['PHP_SELF'];?>">
							<div class="form-group mb-lg">
								<label>Email Id  -or-  User Name</label>
								<div class="input-group input-group-icon">
									<input name="email" type="text" class="form-control input-lg email" value="<?php echo @$email; ?>"/>
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Password</label>
									<a href="forgot-password.php" class="pull-right">Forgot Password?</a>
								</div>
								<div class="input-group input-group-icon">
									<input name="password" type="password" class="form-control input-lg password"  value="<?php echo @$password; ?>" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group">
					        	<?php
					        		if ($errorMessage != '' || $errorMessage != null)
									{
										echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
										echo "<strong>Error!</strong> $errorMessage";
										echo "</div>";
									}

									if ($successMessage != '' || $successMessage != null)
									{
										echo "<script>window.location.assign('index.php');</script>";
									}
					        	?>
					        </div>

					        <input type="hidden" class="recaptchaAllowed" name="recaptchaAllowed" value="<?php echo $recaptchaAllowed;?>">

							<div class="row">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:45px; height:45px; display:none;'/></center>
							</div>

							<div class="mb-xs text-center form_status">
								
							</div>

							<div class="row">
								<div class="col-sm-8">
									
								</div>
								<div class="col-sm-4 text-right">
									<input type="submit" name="login btn"  class="btn btn-primary  btn-block login_btn website-button" id="recaptcha-submit" value="Sign In">
								</div>
							</div>
							<?php
								if($recaptchaAllowed == "1")
								{
									echo "<div class='form-group'>
										<div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
									</div>";
								}
							?>
							<script>onload();</script>							
						</form>
					</div>
				</div>
			</div>
		</section>
		<!-- end: page -->
<?php
	include("templates/footer.php");
?>
		<!-- Vendor -->
		<script src="../js/jquery.js"></script>
		<script src="../js/jquery.browser.mobile.js"></script>
		<script src="../js/bootstrap.js"></script>
		<script src="../js/nanoscroller.js"></script>
		<script src="../js/bootstrap-datepicker.js"></script>
		<script src="../js/magnific-popup.js"></script>
		<script src="../js/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../js/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../js/theme.init.js"></script>

	</body>
</html>

<script>
	$(document).ready(function(){
		/*$(".robot-div").hide();
		
		$('.btn_login').click(function(){
			//e.preventDefault();
			var email = $('.email').val();
			var password = $('.password').val();
			var robot = $('.robot').val();
			var task = "admin_login";

			if(email=='' || email==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter your email id.</div>");
				$(".email").addClass("danger_error");
				return false;
			}

			if(password=='' || password==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter password.</div>");
				$('.password').focus();
				$(".password").addClass("danger_error");
				return false;
			}

			$('.form_status').html("");
			$('.loading_img').show();

			var data = 'email='+email+'&password='+password+'&robot='+robot+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/login_helper.php',
            	success:function(res)
            	{
            		$('.loading_img').hide();
            		if(res=='success')
            		{
            			window.location.assign("index.php");
            		}
            		else
            		{
            			$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}
            	}
            });
            return false;
		});
*/
	});
</script>