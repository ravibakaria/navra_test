<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   
	include('../../../config/setup-values.php');   //get master setup values
    include('../../../config/email/email_style.php');   //get master setup values
    include('../../../config/email/email_templates.php');   //get master setup values
    include('../../../config/email/email_process.php');
	
	$admin_id = $_SESSION['admin_id'];

	$today_datetime = date('Y-m-d H:i:s');
	
    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }


    $task = quote_smart($_POST['task']);

    if($task == 'Update_Member_Document_Status')     // approve-reject member document
	{
		$user_id = $_POST['user_id'];
		$doc_id = $_POST['doc_id'];
		$status = $_POST['status'];
		$document_reject_reason = $_POST['reason'];

		if($status=='1')
		{
			$sql_update = "UPDATE documents SET status='1',approved_reject_by='$admin_id',approved_reject_on='$today_datetime',reject_comment='Approved' WHERE userid='$user_id' AND id='$doc_id'";
		}
		else
		if($status=='0')
		{
			$sql_get_document_info = "SELECT * FROM documents  WHERE userid='$user_id' AND id='$doc_id'";
			$stmt = $link->prepare($sql_get_document_info);
			$stmt->execute();
			$result = $stmt->fetch();

			$first_name = getUserFirstName($user_id);
			$email = getUserEmail($user_id);
			$document = $result['photo'];
			$document_type = $result['photo_type'];
			$document_number = $result['photo_number'];
			$document_upload_on = $result['created_at'];

			$document_path = $WebSiteBasePath.'/users/uploads/'.$user_id.'/documents/'.$document;
			
			$delete_doc_path = '../../../users/uploads/'.$user_id.'/documents/'.$document;

			$SocialSharing = getSocialSharingLinks();   // social sharing links
            
            $DocumentRejectSubject = str_replace('$site_name',$WebSiteTitle,$DocumentRejectSubject);   //replace subject variables with value

            $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

            $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

            $DocumentRejectMessage = str_replace(array('$first_name','$document_reject_reason','$document_type','$document_number','$document_upload_on','$signature'),array($first_name,$document_reject_reason,$document_type,$document_number,$document_upload_on,$GlobalEmailSignature),$DocumentRejectMessage);  //replace footer variables with value

            $subject = $DocumentRejectSubject;
                
            $message  = '<!DOCTYPE html>';
            $message .= '<html lang="en">
                <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width">
                <title></title>
                <style type="text/css">'.$EmailCSS.'</style>
                </head>
                <body style="margin: 0; padding: 0;">';
            //echo $message;exit;
            $message .= $EmailGlobalHeader;

            $message .= $DocumentRejectMessage;

            $message .= $EmailGlobalFooter;
            
            $mailto = $email;
            $mailtoname = $first_name;

            $emailResponse = EmailProcessAttachment($subject,$message,$mailto,$mailtoname,$document_path,$document);

            $sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$user_id','admin reject','document & sent rejection email','$today')";

            if($emailResponse == 'success')
            {
            	unlink($delete_doc_path);
                $link->exec($sql_member_email_log);
            	$sql_update = "UPDATE documents SET status='2',approved_reject_by='$admin_id',approved_reject_on='$today_datetime',reject_comment='$document_reject_reason' WHERE userid='$user_id' AND id='$doc_id'";
            }
            else
            {
                echo "Error in sending rejection email to member.";
            }
		}

		if($link->exec($sql_update))
		{
			echo 'success';
			exit;
		}
	}
?>