<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   
	include('../../../config/setup-values.php');   //get master setup values
    include('../../../config/email/email_style.php');   //get master setup values
    include('../../../config/email/email_templates.php');   //get master setup values
    include('../../../config/email/email_process.php');
	
	$admin_id = $_SESSION['admin_id'];

	$today_datetime = date('Y-m-d H:i:s');
	$ip_address = $_SERVER['REMOTE_ADDR'];
    $basepath = $WebSiteBasePath.'/';

    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }


    $task = quote_smart($_POST['task']);

    /***************    Delete member personal photos    ****************/
    if($task == 'Update-Member-Photo-Status')     // approve-reject member document
	{
		$user_id = $_POST['user_id'];
		$photo_id = $_POST['photo_id'];
		$photo_reject_reason = $_POST['reason'];
		
		$sql_get_document_info = "SELECT * FROM usergallery  WHERE userid='$user_id' AND id='$photo_id'";
		$stmt = $link->prepare($sql_get_document_info);
		$stmt->execute();
		$result = $stmt->fetch();

		$first_name = getUserFirstName($user_id);
		$email = getUserEmail($user_id);
		$Photo = $result['photo'];
		$photo_upload_on = $result['created_at'];

		$photo_path = $WebSiteBasePath.'/users/uploads/'.$user_id.'/photo/'.$Photo;
		
		$delete_photo_path = '../../../users/uploads/'.$user_id.'/photo/'.$Photo;

        $SocialSharing = getSocialSharingLinks();   // social sharing links
        
		$PhotoRejectSubject = str_replace('$site_name',$WebSiteTitle,$PhotoRejectSubject);   //replace subject variables with value

        $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

        $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

        $PhotoRejectMessage = str_replace(array('$first_name','$photo_reject_reason','$photo_upload_on','$signature'),array($first_name,$photo_reject_reason,$photo_upload_on,$GlobalEmailSignature),$PhotoRejectMessage);  //replace footer variables with value

        $subject = $PhotoRejectSubject;
            
        $message  = '<!DOCTYPE html>';
        $message .= '<html lang="en">
            <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <title></title>
            <style type="text/css">'.$EmailCSS.'</style>
            </head>
            <body style="margin: 0; padding: 0;">';
        //echo $message;exit;
        $message .= $EmailGlobalHeader;

        $message .= $PhotoRejectMessage;

        $message .= $EmailGlobalFooter;
        
        $mailto = $email;
        $mailtoname = $first_name;

        $emailResponse = EmailProcessAttachment($subject,$message,$mailto,$mailtoname,$photo_path,$Photo);

        $sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$user_id','admin reject & delete','personal photo','$today')";

        if($emailResponse == 'success')
        {
        	unlink($delete_photo_path);
            $link->exec($sql_member_email_log);
        	$sql_update = "UPDATE usergallery SET status='2',rejected_by='$admin_id',rejected_on='$today_datetime',rejected_reason='$photo_reject_reason' WHERE userid='$user_id' AND id='$photo_id'";
            $sql_log = "INSERT INTO member_photo_delete_admin_logs(userid,photo_type,photo_reject_reason,deleted_by,deleted_on,ip_address) VALUES('$user_id','personal','$photo_reject_reason','$admin_id','$today','$ip_address')";
        }
        else
        {
                echo "Error in sending rejection email to member.";
        }
	
		if($link->exec($sql_update) && $link->exec($sql_log))
		{
			echo 'success';
			exit;
		}
	}

    /***************    Delete member profile photos    ****************/
    if($task == 'delete-profile-photo')     // approve-reject member document
    {
        $user_id = $_POST['user_id'];
        $photo_reject_reason = $_POST['reason_delete_profile_pic'];

        $sql_get_document_info = "SELECT * FROM profilepic  WHERE userid='$user_id'";
        $stmt = $link->prepare($sql_get_document_info);
        $stmt->execute();
        $result = $stmt->fetch();

        $first_name = getUserFirstName($user_id);
        $email = getUserEmail($user_id);
        $Photo = $result['photo'];
        $photo_upload_on = $result['created_at'];

        $photo_path = $WebSiteBasePath.'/users/uploads/'.$user_id.'/profile/'.$Photo;
        
        $delete_photo_path = '../../../users/uploads/'.$user_id.'/profile/'.$Photo;

        $SocialSharing = getSocialSharingLinks();   // social sharing links
        
        $PhotoRejectSubject = str_replace('$site_name',$WebSiteTitle,$PhotoRejectSubject);   //replace subject variables with value

        $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

        $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

        $PhotoRejectMessage = str_replace(array('$first_name','$photo_reject_reason','$photo_upload_on','$signature'),array($first_name,$photo_reject_reason,$photo_upload_on,$GlobalEmailSignature),$PhotoRejectMessage);  //replace footer variables with value

        $subject = $PhotoRejectSubject." (Profile Photo)";
            
        $message  = '<!DOCTYPE html>';
        $message .= '<html lang="en">
            <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <title></title>
            <style type="text/css">'.$EmailCSS.'</style>
            </head>
            <body style="margin: 0; padding: 0;">';
        //echo $message;exit;
        $message .= $EmailGlobalHeader;

        $message .= $PhotoRejectMessage;

        $message .= $EmailGlobalFooter;
        
        $mailto = $email;
        $mailtoname = $first_name;

        $emailResponse = EmailProcessAttachment($subject,$message,$mailto,$mailtoname,$photo_path,$Photo);

        $sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$user_id','admin reject & delete','profile photo','$today')";

        //$emailResponse = 'success';
        if($emailResponse == 'success')
        {
            unlink($delete_photo_path);
            $link->exec($sql_member_email_log);
            $sql_update = "DELETE FROM profilepic WHERE userid='$user_id' ";
            $sql_log = "INSERT INTO member_photo_delete_admin_logs(userid,photo_type,photo_reject_reason,deleted_by,deleted_on,ip_address) VALUES('$user_id','profile','$photo_reject_reason','$admin_id','$today','$ip_address')";
        }
        else
        {
                echo "Error in sending rejection email to member.";
        }
        
        if($link->exec($sql_update) && $link->exec($sql_log))
        {
            echo 'success';
            exit;
        }
    }
?>