<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   
	include('../../../config/setup-values.php');   //get master setup values
	
	$today_datetime = date('Y-m-d H:i:s');
	
    $basepath = $WebSiteBasePath.'/';

    $task = quote_smart($_POST['task']);

    if($task == 'Fetch_States_Of_Country')     // Featch states
	{
		$output = null;
		$country_id = $_POST['country_id'];

		$sql_fetch_states = "SELECT * FROM states WHERE country_id='$country_id' AND status='1'";
		$stmt = $link->prepare($sql_fetch_states); 
		$stmt->execute();
		$result = $stmt->fetchAll();

		$output = "<option value='0' selected>Select State</option>";
		foreach ($result as $row) 
		{
			$id = $row['id'];
			$name = $row['name'];
			$output .= "<option value='".$id."'>".$name."</option>";
		}

		echo $output;
	}

	if($task == 'Fetch_Cities_Of_State')     // Featch cities
	{
		$output = null;
		$state_id = $_POST['state_id'];

		$sql_fetch_cities = "SELECT * FROM cities WHERE state_id='$state_id' AND status='1'";
		$stmt = $link->prepare($sql_fetch_cities); 
		$stmt->execute();
		$result = $stmt->fetchAll();

		$output = "<option value='0' selected>Select City</option>";
		foreach ($result as $row) 
		{
			$id = $row['id'];
			$name = $row['name'];
			$output .= "<option value='".$id."'>".$name."</option>";
		}

		echo $output;
	}

	if($task == 'update_member_profile_information')
	{
		$user_id = $_POST['user_id'];
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$unique_code = $_POST['unique_code'];
		$email = $_POST['email'];
		$gender = $_POST['gender'];
		$dob = $_POST['dob'];
		$phonenumber = $_POST['phonenumber'];
		$status = $_POST['status'];
		$height = $_POST['height'];
		$body_type = $_POST['body_type'];
		$weight = $_POST['weight'];
		$complexion = $_POST['complexion'];
		$marital_status = $_POST['marital_status'];
		$special_case = $_POST['special_case'];
		$eat_habbit = $_POST['eat_habbit'];
		$smoke_habbit = $_POST['smoke_habbit'];
		$drink_habbit = $_POST['drink_habbit'];
		$religion = $_POST['religion'];
		$caste = $_POST['caste'];
		$mother_tongue = $_POST['mother_tongue'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$city = $_POST['city'];
		$education = $_POST['education'];
		$occupation = $_POST['occupation'];
		$designation_category = $_POST['designation_category'];
		$designation = $_POST['designation'];
		$industry = $_POST['industry'];
		$income_currency = $_POST['income_currency'];
		$income = $_POST['income'];
		$fam_val = $_POST['fam_val'];
		$fam_type = $_POST['fam_type'];
		$fam_stat = $_POST['fam_stat'];
		$short_desc = $_POST['short_desc'];
		$short_desc_family = $_POST['short_desc_family'];
		$short_desc_interest = $_POST['short_desc_interest'];

		$sql_chk_email = "SELECT email FROM clients WHERE email='$email' AND id<>'$user_id'";
		$stmt_email = $link->prepare($sql_chk_email);
		$stmt_email->execute();
		$count_email = $stmt_email->rowCount();

		$sql_chk_unique_code = "SELECT unique_code FROM clients WHERE unique_code='$unique_code' AND id<>'$user_id'";
		$stmt_unique_code = $link->prepare($sql_chk_unique_code);
		$stmt_unique_code->execute();
		$count_unique_code = $stmt_unique_code->rowCount();

		if($count_email==0 && $count_unique_code==0)
		{
			$update_personal_info = "UPDATE clients SET firstname='$firstname',lastname='$lastname',dob='$dob',gender='$gender',unique_code='$unique_code',email='$email',phonenumber='$phonenumber',status='$status',updated_at='$today_datetime'  WHERE id='$user_id'";
			$link->exec($update_personal_info);
		}
		else
		{
			if($count_email>0)
			{
				echo "Email Id already exist in database. You cannot enter duplicate emeil id";
				exit;
			}

			if($count_unique_code>0)
			{
				echo "Profile Id already exist in database. You cannot enter duplicate profile id";
				exit;
			}
		}
		

		$update_basic_info = "UPDATE profilebasic SET height='$height',weight='$weight',eat_habbit='$eat_habbit',smoke_habbit='$smoke_habbit',body_type='$body_type',complexion='$complexion',marital_status='$marital_status',special_case='$special_case',drink_habbit='$drink_habbit',updated_at='$today_datetime'  WHERE userid='$user_id'";
		$link->exec($update_basic_info);

		$update_religious_info = "UPDATE profilereligion SET religion='$religion',caste='$caste',mother_tongue='$mother_tongue',updated_at='$today_datetime' WHERE userid='$user_id'";
		$link->exec($update_religious_info);


		$update_location_info = "UPDATE address SET city='$city',state='$state',country='$country',updated_at='$today_datetime' WHERE userid='$user_id'";
		$link->exec($update_location_info);


		$update_education_occ_info = "UPDATE eduocc SET education='$education',occupation='$occupation',designation_category='$designation_category',designation='$designation',industry='$industry',income_currency='$income_currency',income='$income',updated_at='$today_datetime' WHERE userid='$user_id'";
		$link->exec($update_education_occ_info);


		$update_family_info = "UPDATE family SET fam_val='$fam_val',fam_type='$fam_type',fam_stat='$fam_stat',updated_at='$today_datetime' WHERE userid='$user_id'";
		$link->exec($update_family_info);


		$update_short_desc_info = "UPDATE profiledesc SET short_desc='$short_desc',short_desc_family='$short_desc_family',short_desc_interest='$short_desc_interest',updated_at='$today_datetime'";
		$link->exec($update_short_desc_info);
		

		echo "success";
	}
?>