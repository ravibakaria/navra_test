<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   
	include('../../../config/setup-values.php');   //get master setup values

	$task = quote_smart($_POST['task']);

	if($task == 'Get_Blocked_Profile')
	{
		$value = $_POST['value'];
		$user_id_x = $_POST['user_id_x'];
		$output = null;

		if($value=='Blocked_By_Me')
		{
			$sql = "SELECT * FROM blockedusers WHERE blocker='$user_id_x' AND status='blocked' ORDER BY blockedOn DESC";
		}
		else
		if($value=='Blocked_By_Others')
		{
			$sql = "SELECT * FROM blockedusers WHERE blockedUser='$user_id_x' AND status='blocked' ORDER BY blockedOn DESC";
		}

		$stmt = $link->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count>0)
		{
			$result = $stmt->fetchAll();

			$output = "<table  id='datatable-info' class='table-hover table-striped table-bordered' style='width:100%'>";
				$output .= "<thead>
	                            <tr>
	                                <th>ID</th>
	                                <th>First Name</th>
	                                <th>Last Name</th>
	                                <th>Email Id</th>
	                                <th>Profile ID</th>
	                                <th>Blocked Date</th>
	                                <th>Blocked Comment</th>
	                                <th>Status</th>
	                            </tr>
	                        </thead>";
	            $output .= "<tbody>";
	            	foreach ($result as $row) 
	            	{
	            		if($value=='Blocked_By_Me')
						{
							$blocked_id = $row['blockedUser'];
						}
						else
						if($value=='Blocked_By_Others')
						{
							$blocked_id = $row['blocker'];
						}
	            		
	            		$blocked_on = $row['blockedOn'];
	            		$blocked_comment = $row['blockComment'];

	            		$sql_userinfo = "SELECT * FROM clients WHERE id='$blocked_id'";
	            		$stmt_userinfo = $link->prepare($sql_userinfo);
	            		$stmt_userinfo->execute();
	            		$result_userinfo = $stmt_userinfo->fetch();

	            		$firstname = $result_userinfo['firstname'];
	            		$lastname = $result_userinfo['lastname'];
	            		$email = $result_userinfo['email'];
	            		$unique_code = $result_userinfo['unique_code'];
	            		$status = $result_userinfo['status'];

	            		if($status=='0')
				        {
				        	$status_display = "<b style='color:red;'>In-Active</b>";
				        }
				        else
				        if($status=='1')
				        {
				        	$status_display = "<b style='color:green;'>Active</b>";
				        }
				        else
				        if($status=='2')
				        {
				        	$status_display = "<b style='color:blue;'>Deactivate</b>";
				        }
				        else
				        if($status=='3')
				        {
				        	$status_display = "<b style='color:red;'>Suspend</b>";
				        }

	            		$output .= "<tr>";
	            			$output .= "<td><a href=member-profile.php?id=".$blocked_id." style='color:#000000;text-decoration:none;'>".$blocked_id."</a></td>";
	            			$output .= "<td><a href=member-profile.php?id=".$blocked_id." style='color:#000000;text-decoration:none;'>".$firstname."</a></td>";
	            			$output .= "<td><a href=member-profile.php?id=".$blocked_id." style='color:#000000;text-decoration:none;'>".$lastname."</a></td>";
	            			$output .= "<td><a href=member-profile.php?id=".$blocked_id." style='color:#000000;text-decoration:none;'>".$email."</a></td>";
	            			$output .= "<td><a href=member-profile.php?id=".$blocked_id." style='color:#000000;text-decoration:none;'>".$unique_code."</a></td>";
	            			$output .= "<td><a href=member-profile.php?id=".$blocked_id." style='color:#000000;text-decoration:none;'>".$blocked_on."</a></td>";
	            			$output .= "<td><a href=member-profile.php?id=".$blocked_id." style='color:#000000;text-decoration:none;'>".$blocked_comment."</a></td>";
	            			$output .= "<td><a href=member-profile.php?id=".$blocked_id." style='color:#000000;text-decoration:none;'>".$status_display."</a></td>";
	            		$output .= "<tr>";
	            	}
	            $output .= "</tbody>";
			$output .= "</table>";
		}
		else
		{
			$output = "<table  id='datatable-info' class='table-hover table-striped table-bordered' style='width:100%'>";
				$output .= "<thead>
	                            <tr>
	                                <th>ID</th>
	                                <th>First Name</th>
	                                <th>Last Name</th>
	                                <th>Email Id</th>
	                                <th>Profile ID</th>
	                                <th>Blocked Date</th>
	                                <th>Blocked Comment</th>
	                                <th>Status</th>
	                            </tr>
	                        </thead>";
	            $output .= "<tbody>";
	            	$output .= "<tr>";
	            		$output .= "<td colspan='8'>No result found.</td>";
	            	$output .= "</tr>";
	            $output .= "</tbody>";
	        $output .= "</table>";
		}

		echo $output;
		exit;
	}
?>