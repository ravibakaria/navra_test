<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']=='' || $_SESSION['admin_user']==null))
	{
		header('Location: login.php');
		exit;
	}

	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   
	include('../../../config/setup-values.php');   //get master setup values

	$task = $_POST['task'];

	if($task=='fetch_chat_list')
	{
		$user_id =$_POST['userid_chat_history'];
		$WebSiteBasePath = getWebsiteBasePath();
		$output = null;
		$fetch_chat_list = "SELECT messagechat.* FROM (SELECT chatid, MAX(messagedOn) AS messagedOn FROM messagechat  WHERE userFrom='$user_id' OR userTo='$user_id' GROUP BY chatid) AS latest_chat INNER JOIN messagechat ON messagechat.chatid = latest_chat.chatid AND messagechat.messagedOn = latest_chat.messagedOn ORDER BY latest_chat.messagedOn DESC";
		$stmt = $link->prepare($fetch_chat_list);
		$stmt->execute();
		$count_chat_list = $stmt->rowCount();
		$result = $stmt->fetchAll();

		if($count_chat_list=='0')
		{
			$output = "<div class='chat_list active_chat'>
				<div class='chat_people'>
					No chat history available...
				</div>
			</div>";
		}
		else
		{
			foreach ($result as $row) 
			{
				$userFrom = $row['userFrom'];
				$userTo = $row['userTo'];
				$message = $row['message'];
				$messagedOn = $row['messagedOn'];
				if($userFrom==$user_id)
				{
					$display_profile_id = $userTo;
					$chat_user_id = $userFrom;
					$display_from = "You sent: ";
				}
				else
				if($userTo==$user_id)
				{
					$display_profile_id = $userFrom;
					$chat_user_id = $userTo;
					$display_from = "User replies: ";
				}

				$display_profile_img = getUserProfilePic($display_profile_id);

				if($display_profile_img=='' || $display_profile_img==null)
				{
					$display_profile_img = $WebSiteBasePath.'/images/no_profile_pic.png';
				}
				else
				{
					$display_profile_img = $WebSiteBasePath.'/users/uploads/'.$display_profile_id.'/profile/'.$display_profile_img;
				}

				$display_profile_name = getUserFirstName($display_profile_id);

				$messagedOn = date('d-M-Y h:i A',strtotime($messagedOn));

				$output .= "<a href=message.php?id=$display_profile_id&chat_user_id=$chat_user_id><div class='chat_list'>
				<div class='chat_people'>
					<div class='chat_img'> 
						<img src='$display_profile_img'> 
					</div>
					<div class='chat_ib'>
						<h5>$display_profile_name <span class='chat_date'>$messagedOn</span></h5>
						<p>$display_from $message.</p>
					</div>
				</div>
			</div></a>";
			}
		}

		echo $output;
		exit;
	}

	if($task=='fetch_chat_list_userName')
	{
		$user_id =$_POST['userid_chat_history'];
		$output = null;
		$user_name = $_POST['user_name'];
		$fetch_chat_list = "SELECT messagechat.* FROM (SELECT chatid, MAX(messagedOn) AS messagedOn FROM messagechat  WHERE userFrom='$user_id' OR userTo='$user_id' GROUP BY chatid) AS latest_chat INNER JOIN messagechat ON messagechat.chatid = latest_chat.chatid AND messagechat.messagedOn = latest_chat.messagedOn ORDER BY latest_chat.messagedOn DESC";
		$stmt = $link->prepare($fetch_chat_list);
		$stmt->execute();
		$count_chat_list = $stmt->rowCount();
		$result = $stmt->fetchAll();

		if($count_chat_list=='0')
		{
			$output = "<div class='chat_list active_chat'>
				<div class='chat_people'>
					No chat history available...
				</div>
			</div>";
		}
		else
		{
			foreach ($result as $row) 
			{
				$userFrom = $row['userFrom'];
				$userTo = $row['userTo'];
				$message = $row['message'];
				$messagedOn = $row['messagedOn'];
				if($userFrom==$user_id)
				{
					$display_profile_id = $userTo;
					$chat_user_id = $userFrom;
					$display_from = "You send: ";
				}
				else
				if($userTo==$user_id)
				{
					$display_profile_id = $userFrom;
					$chat_user_id = $userTo;
					$display_from = "User send: ";
				}


				$display_profile_img = getUserProfilePic($display_profile_id);

				if($display_profile_img=='' || $display_profile_img==null)
				{
					$display_profile_img = $WebSiteBasePath.'/images/no_profile_pic.png';
				}
				else
				{
					$display_profile_img = $WebSiteBasePath.'/users/uploads/'.$display_profile_id.'/profile/'.$display_profile_img;
				}

				$display_profile_name = getUserFirstName($display_profile_id);

				if (strpos(strtolower($display_profile_name), strtolower($user_name)) === false && $user_name!='0') {
				    continue;
				}

				$messagedOn = date('d-M-Y H:i',strtotime($messagedOn));

				$output .= "<a href=message.php?id=$display_profile_id&chat_user_id=$chat_user_id><div class='chat_list'>
				<div class='chat_people'>
					<div class='chat_img'> 
						<img src='$display_profile_img'> 
					</div>
					<div class='chat_ib'>
						<h5>$display_profile_name <span class='chat_date'>$messagedOn</span></h5>
						<p>$display_from $message.</p>
					</div>
				</div>
			</div></a>";
			}
		}

		echo $output;
		exit;
	}
?>