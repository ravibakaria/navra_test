<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   
	include('../../../config/setup-values.php');   //get master setup values
    include('../../../config/email/email_style.php');   //get master setup values
    include('../../../config/email/email_templates.php');   //get master setup values
    include('../../../config/email/email_process.php');
	
	$today_datetime = date('Y-m-d H:i:s');
	
    $WebSiteBasePath = getWebsiteBasePath();
    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }

    $task = quote_smart($_POST['task']);

	if($task == 'Add_New_Member')
	{
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$city = $_POST['city'];
		$email = $_POST['email'];
		$phonecode = $_POST['phonecode'];
		$phonenumber = $_POST['phonenumber'];
		$dob = $_POST['dob'];
		$gender = $_POST['gender'];
		$status = $_POST['status'];

		$sql_chk = "SELECT * FROM `clients` WHERE LOWER(`email`)='".strtolower($email)."'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();

        if($count>0)
        {
        	echo "Member with <strong>$email</strong> email id already exisits in records. Please use another email id.";
			exit;
        }

        $age = (date('Y') - date('Y',strtotime($dob)));

		if($age<18)
		{
			echo "Its seems that member is below 18 years old. Please enter valid date.";
			exit;
		}

        $uniquecode = generateRandomString();

		$sql_chk_unique_code = $link->prepare("SELECT `unique_code` FROM `clients` WHERE unique_code='$uniquecode'"); 
        $sql_chk_unique_code->execute();
        $count_unique_code=$sql_chk_unique_code->rowCount();
        if($count_unique_code>0)
        {
        	$uniquecode = generateRandomString();
        }

        $Special_char_string = '[!@#$%^)*_(+=}{|:;,.<>}]'; 
		$pos1 = rand(0,(strlen($Special_char_string)-1));
        $pass1 = $Special_char_string[$pos1];

        $Capital_char_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
		$pos2 = rand(0,(strlen($Capital_char_string)-1));
        $pass2 = $Capital_char_string[$pos2];

        $Number_char_string = '1234567890'; 
		$pos3 = rand(0,(strlen($Number_char_string)-1));
        $pass3 = $Number_char_string[$pos3];

        $salt = "abchefghjkmnpqrstuvwxyz0123456789";
	    srand((double)microtime()*1000000);
      	$i = 0;
      	while ($i <= 5) 
      	{
            $num = rand() % 33;
            $tmp = substr($salt, $num, 1);
            $pass1 = $pass1 . $tmp;
            $i++;
      	}
	    
	    $new_pass = $pass2.$pass3.$pass1;    //
	    $tmp_password = md5($new_pass);     // random 

	    $sql_insert = "INSERT INTO `clients`(`firstname`, `lastname`, `gender`,`unique_code`, `email`, `dob`, `phonecode`, `phonenumber`, `password`, `status`, `created_at`) VALUES ('$firstname','$lastname','$gender','$uniquecode','$email','$dob','$phonecode','$phonenumber','$tmp_password','1','$today_datetime')";
		//$insert_query = $link->exec($sql_insert);
		if($link->exec($sql_insert))
		{
			$sql = "SELECT MAX(id) as id FROM clients";
			$stmt   = $link->prepare($sql);
	        $stmt->execute();
	        $sql_id = $stmt->fetch();

	        $id = $sql_id['id'];

			$sql_insert_location = "INSERT INTO `address`(`userid`, `city`, `state`, `country`, `created_at`) VALUES('$id','$city','$state','$country','$today')";

			$login_link = $WebSiteBasePath.'/login.php';
			$forgot_password_link = $WebSiteBasePath.'/forgot-password.php';
			if($link->exec($sql_insert_location))
			{
				$SocialSharing = getSocialSharingLinks();   // social sharing links

				$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

	            $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

	            $NewRegistrationMessage = str_replace(array('$first_name','$site_name','$login_link','$email_address','$forgot_password_link','$signature'),array($firstname,$WebSiteTitle,$login_link,$email,$forgot_password_link,$GlobalEmailSignature),$NewRegistrationMessage);  //replace footer variables with value


	            $subject = $NewRegistrationSubject;
	            
	            $message  = '<!DOCTYPE html>';
	            $message .= '<html lang="en">
	                <head>
	                <meta charset="utf-8">
	                <meta name="viewport" content="width=device-width">
	                <title></title>
	                <style type="text/css">'.$EmailCSS.'</style>
	                </head>
	                <body style="margin: 0; padding: 0;">';
	            //echo $message;exit;
	            $message .= $EmailGlobalHeader;

	            $message .= $NewRegistrationMessage;                                               
	            $message .= $EmailGlobalFooter;
	            
	            $mailto = $email;
	            $mailtoname = $firstname;
				
				$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

				if($emailResponse == 'success')
				{
					$errorMessage = "success";
				}
				else
				{
					$errorMessage = "Error in sending login details to member.";
				}
			}
		}
		else
		{
			$errorMessage = "Error in updating data.";
		}

		echo $errorMessage;
    }

    if($task == 'Change_Member_status')
    {
    	$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `clients` SET `status`='$updated_status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
    }

    if($task == 'Update_Member_Attributes')
	{
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];
		$dob = $_POST['dob'];
		$gender = $_POST['gender'];
		$status = $_POST['status'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `clients` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_firstname = $result['firstname'];
        $db_lastname = $result['lastname'];
        $db_email = $result['email'];
        $db_dob = $result['dob'];
        $db_gender = $result['gender'];
        $db_status = $result['status'];

        if($db_firstname==$firstname && $db_lastname==$lastname && $db_email==$email && $db_dob==$dob && $db_gender==$gender && $db_status==$status)
        {
        	echo "Data already updated.";
			exit;
        }

        $sql = "UPDATE `clients` SET `firstname`='$firstname',`lastname`='$lastname',`email`='$email',`dob`='$dob',`gender`='$gender',`status`='$status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}

	if($task == 'Reset_Member_Password')
	{
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `clients` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $firstname = $result['firstname'];
        $lastname = $result['lastname'];
        $email = $result['email'];
        $status = $result['status'];

        if($status=='0')
        {
        	echo "Member email not verified.";
        	exit;
        }
        else
        if($status=='2')
        {
        	echo "Member deactivated his/her account.";
        	exit;
        }
        else
        if($status=='3')
        {
        	echo "Member account suspended.";
        	exit;
        }

        $Special_char_string = '[!@#$%^)*_(+=}{|:;,.<>}]'; 
		$pos1 = rand(0,(strlen($Special_char_string)-1));
        $pass1 = $Special_char_string[$pos1];

        $Capital_char_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
		$pos2 = rand(0,(strlen($Capital_char_string)-1));
        $pass2 = $Capital_char_string[$pos2];

        $Number_char_string = '1234567890'; 
		$pos3 = rand(0,(strlen($Number_char_string)-1));
        $pass3 = $Number_char_string[$pos3];

        $salt = "abchefghjkmnpqrstuvwxyz0123456789";
	    srand((double)microtime()*1000000);
      	$i = 0;
      	while ($i <= 5) 
      	{
            $num = rand() % 33;
            $tmp = substr($salt, $num, 1);
            $pass1 = $pass1 . $tmp;
            $i++;
      	}
	    
	    $new_pass = $pass2.$pass3.$pass1;    //
	    $tmp_password = md5($new_pass);     // random

        $sql_update = "UPDATE `clients` SET `password`='$tmp_password' WHERE `id`='$id'";

        if($link->exec($sql_update))
        {
        	$first_name = $firstname;
        	$site_name = $WebSiteTitle;
        	$login_link = $WebSiteBasePath.'/login.php';
        	$email_address = $email;
        	$new_password = $tmp_password;

        	$SocialSharing = getSocialSharingLinks();   // social sharing links

        	$ForgotPasswordSubject = str_replace('$site_name',$site_name,$ForgotPasswordSubject);   //replace subject variables with value

            $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

            $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

            $ForgotPasswordMessage = str_replace(array('$first_name','$site_name','$login_link','$email_address','$new_password','$signature'),array($first_name,$site_name,$login_link,$email_address,$new_password,$GlobalEmailSignature),$ForgotPasswordMessage);  //replace footer variables with value


            $subject = $ForgotPasswordSubject;
            
            $message  = '<!DOCTYPE html>';
            $message .= '<html lang="en">
                <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width">
                <title></title>
                <style type="text/css">'.$EmailCSS.'</style>
                </head>
                <body style="margin: 0; padding: 0;">';
            //echo $message;exit;
            $message .= $EmailGlobalHeader;

            $message .= $ForgotPasswordMessage;

            $message .= $EmailGlobalFooter;
            
            $mailto = $email;
            $mailtoname = $firstname;
			
			$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

			$sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$id','password reset by admin','sent password on $email','$today')";
			$link->exec($sql_member_email_log);

			if($emailResponse == 'success')
			{
            	echo "success";
				exit;
			}
			else
			{
				echo "Error in sending login details to member.";
				exit;
			}
        }
        else
        {
        	echo "Password reset failed! Try after some time.";
        	exit;
        }
	}
?>