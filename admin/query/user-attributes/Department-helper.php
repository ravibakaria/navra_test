<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   
	include('../../../config/setup-values.php');   //setup values     

	$today_datetime = date('Y-m-d H:i:s');
	
    $basepath = $WebSiteBasePath.'/';
    $task = quote_smart($_POST['task']);

    if($task == 'Add_New_Department')
	{
		$name = $_POST['name'];
		$email = $_POST['email'];
		$status = $_POST['status'];

		$sql_chk = "SELECT * FROM `departments` WHERE `name`='$name' OR `email`='$email' ";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();

        if($count>0)
        {
        	echo "Duplicate entry for department.";
			exit;
        }


	    $sql_insert = "INSERT INTO `departments`(`name`, `email`, `status`, `created_at`) VALUES ('$name','$email','$status','$today_datetime')";

	    if($link->exec($sql_insert))
		{
			echo "success";
		}
		else
		{
			echo "Something went wrong. Try after some time.";
		}
	}

    if($task == 'Change_Department_status')
    {
    	$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `departments` SET `status`='$updated_status',`updated_at`='$today_datetime' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
    }

    if($task == 'Update_Department_Attributes')
	{
		$name = $_POST['name'];
		$email = $_POST['email'];
		$status = $_POST['status'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `departments` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_name = $result['name'];
        $db_email = $result['email'];
        $db_status = $result['status'];

        if($db_name==$name && $db_email==$email && $db_status==$status)
        {
        	echo "Data already updated.";
			exit;
        }

        $sql = "UPDATE `departments` SET `name`='$name',`email`='$email',`status`='$status',`updated_at`='$today_datetime' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>