<?php

	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

	$today_datetime = date('Y-m-d H:i:s');
	
	$userid = $_SESSION['admin_id'];

	$task = quote_smart($_POST['task']);

	if($task=='Add_New_FAQ')
	{
		$faq_question = $_POST['faq_question'];
		$faq_question = str_replace('\\', '\\\\', $faq_question);
        $faq_question = str_replace("'", "\\'", $faq_question);

		$faq_answer = $_POST['faq_answer'];
		$faq_answer = str_replace('\\', '\\\\', $faq_answer);
        $faq_answer = str_replace("'", "\\'", $faq_answer);

		$sql_chk = "SELECT * FROM `faq` WHERE `faq_question`='$faq_question'";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count_chk = $stmt->rowCount();

		if($count_chk>0)
		{
			echo json_encode("Question already present in FAQ list! Please use different question.");
			exit;
		}

		$sql_insert = "INSERT INTO `faq`(`faq_question`,`faq_answer`,`status`,`userid`,`created_at`) VALUES('$faq_question','$faq_answer','1','$userid','$today')";
		
		if($link->exec($sql_insert))
        {
        	echo json_encode("success");
        	exit;
        }
        else
        {
        	echo json_encode("Something went wrong try after some time");
        	exit;
        }
	}

	if($task == 'Change_FAQ_status')
    {
    	$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `faq` SET `status`='$updated_status',`userid`='$userid',`updated_at`='$today_datetime' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
    }

    if($task == 'Update_FAQ_Attributes')
	{
		$faq_question = $_POST['faq_question'];
		$faq_question = str_replace('\\', '\\\\', $faq_question);
        $faq_question = str_replace("'", "\\'", $faq_question);

		$faq_answer = $_POST['faq_answer'];
		$faq_answer = str_replace('\\', '\\\\', $faq_answer);
        $faq_answer = str_replace("'", "\\'", $faq_answer);

		$status = $_POST['status'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `faq` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_faq_question = $result['faq_question'];
        $db_faq_answer = $result['faq_answer'];
        $db_status = $result['status'];

        if($db_faq_question==$faq_question && $db_faq_answer==$faq_answer && $db_status==$status)
        {
        	echo json_encode("Same values as before.");
			exit;
        }

        $sql = "UPDATE `faq` SET `faq_question`='$faq_question',`faq_answer`='$faq_answer',`status`='$status',`userid`='$userid',`updated_at`='$today_datetime' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong. Try after some time.");
			exit;
		}
	}

	if($task=='Update_FAQ_website_display_Page')
	{
		$faq_chk_show = $_POST['faq_chk_show'];
		$userid = $_SESSION['admin_id'];

		$sql_chk = "SELECT * FROM `faq_page_show`";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count_chk = $stmt->rowCount();

		if($count_chk==0)
		{
			$sql_insert = "INSERT INTO `faq_page_show`(`display`,`userid`,`created_at`) VALUES('$faq_chk_show','$userid','$today_datetime')";
		}
		else
		if($count_chk>0)
		{
			$sql_insert = "UPDATE `faq_page_show` SET display='$faq_chk_show',userid='$userid',updated_at='$today_datetime'";
		}
		
		if($link->exec($sql_insert))
        {
        	echo "success";
        	exit;
        }
        else
        {
        	echo "Something went wrong try after some time";
        	exit;
        }
	}
?>