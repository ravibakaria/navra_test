<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$basepath = $WebSiteBasePath.'/';
    $task = quote_smart($_POST['task']);

	if($task == 'update_color_setting')
	{
		$primary_color = $_POST['primary_color'];
		$primary_font_color = $_POST['primary_font_color'];
		$sidebar_color = $_POST['sidebar_color'];
		$sidebar_font_color = $_POST['sidebar_font_color'];

		$sql_chk = "SELECT * FROM `theme_color_setting`";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();
        
       	if($count>0)
        {
        	$primary_color_db = $result['primary_color'];
			$primary_font_color_db = $result['primary_font_color'];
			$sidebar_color_db = $result['sidebar_color'];
			$sidebar_font_color_db = $result['sidebar_font_color'];

			if($primary_color_db==$primary_color && $primary_font_color_db==$primary_font_color && $sidebar_color_db==$sidebar_color && $sidebar_font_color_db==$sidebar_font_color)
        	{
        		echo "Same values as before.";
				exit;
        	}
        	else
			{
				$sql_insert = "UPDATE theme_color_setting SET primary_color='$primary_color',primary_font_color='$primary_font_color',sidebar_color='$sidebar_color',sidebar_font_color='$sidebar_font_color'";
			
				if($link->exec($sql_insert))
				{
					echo "success";
					exit;
				}
				else
				{
					echo "Something went wrong. Try after some time.";
					exit;
				}   
			} 
        }
	}
?>