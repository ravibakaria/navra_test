<?php

	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

        $task = quote_smart($_POST['task']);

	if($task=='homepagesetup_update')
	{
                $homepageHeading = $_POST['homepageHeading'];
                $footercontentshow = $_POST['footercontentshow'];
                $footercontent = $_POST['footercontent'];
                $footercontent = str_replace('\\', '\\\\', $footercontent);
                $footercontent = str_replace("'", "\\'", $footercontent);

		$extracontentstripshow = $_POST['extracontentstripshow'];
		$extracontentstripdata = $_POST['extracontentstripdata'];
                $extracontentstripdata = str_replace('\\', '\\\\', $extracontentstripdata);
                $extracontentstripdata = str_replace("'", "\\'", $extracontentstripdata);

		$featuredprofilesshow = $_POST['featuredprofilesshow'];
		$featuredprofilescount = $_POST['featuredprofilescount'];
		$premiumprofilesshow = $_POST['premiumprofilesshow'];
		$premiumprofilescount = $_POST['premiumprofilescount'];
		$recentlyaddedprofilesshow = $_POST['recentlyaddedprofilesshow'];
		$recentlyaddedprofilescount = $_POST['recentlyaddedprofilescount'];
                $profilefiltershow = $_POST['profilefiltershow'];
                $profilefiltervalues = implode(',',$_POST['profilefiltervalues']);

                //echo json_encode($profilefiltervalues);exit;

                $additional_footer_content1_display = $_POST['additional_footer_content1_display'];
                $additional_footer_content1 = $_POST['additional_footer_content1'];
                $additional_footer_content1 = str_replace('\\', '\\\\', $additional_footer_content1);
                $additional_footer_content1 = str_replace("'", "\\'", $additional_footer_content1);

                $additional_footer_content2_display = $_POST['additional_footer_content2_display'];
                $additional_footer_content2 = $_POST['additional_footer_content2'];
                $additional_footer_content2 = str_replace('\\', '\\\\', $additional_footer_content2);
                $additional_footer_content2 = str_replace("'", "\\'", $additional_footer_content2);

                $additional_footer_content3_display = $_POST['additional_footer_content3_display'];
                $additional_footer_content3 = $_POST['additional_footer_content3'];
                $additional_footer_content3 = str_replace('\\', '\\\\', $additional_footer_content3);
                $additional_footer_content3 = str_replace("'", "\\'", $additional_footer_content3);

                $additional_footer_content4_display = $_POST['additional_footer_content4_display'];
                $additional_footer_content4 = $_POST['additional_footer_content4'];
                $additional_footer_content4 = str_replace('\\', '\\\\', $additional_footer_content4);
                $additional_footer_content4 = str_replace("'", "\\'", $additional_footer_content4);

		$sql_chk = "SELECT * FROM homepagesetup";
        	$sql_chk= $link->prepare($sql_chk);
        	$sql_chk->execute();
                $result_chk = $sql_chk->fetch();
                $count=$sql_chk->rowCount();

                $homepageHeading_db = $result_chk['homepageHeading'];
                $footercontentshow_db = $result_chk['footercontentshow'];
                $footercontent_db = $result_chk['footercontent'];
                $extracontentstripshow_db = $result_chk['extracontentstripshow'];
                $extracontentstripdata_db = $result_chk['extracontentstripdata'];
                $featuredprofilesshow_db = $result_chk['featuredprofilesshow'];
                $featuredprofilescount_db = $result_chk['featuredprofilescount'];
                $premiumprofilesshow_db = $result_chk['premiumprofilesshow'];
                $premiumprofilescount_db = $result_chk['premiumprofilescount'];
                $recentlyaddedprofilesshow_db = $result_chk['recentlyaddedprofilesshow'];
                $recentlyaddedprofilescount_db = $result_chk['recentlyaddedprofilescount'];
                $profilefiltershow_db = $result_chk['profilefiltershow'];
                $profilefiltervalues_db = $result_chk['profilefiltervalues'];

                $additional_footer_content1_display_db = $result_chk['additional_footer_content1_display'];
                $additional_footer_content1_db = $result_chk['additional_footer_content1'];
                $additional_footer_content2_display_db = $result_chk['additional_footer_content2_display'];
                $additional_footer_content2_db = $result_chk['additional_footer_content2'];
                $additional_footer_content3_display_db = $result_chk['additional_footer_content3_display'];
                $additional_footer_content3_db = $result_chk['additional_footer_content3'];
                $additional_footer_content4_display_db = $result_chk['additional_footer_content4_display'];
                $additional_footer_content4_db = $result_chk['additional_footer_content4'];

                if($count>0)
                {
                        if($extracontentstripshow_db==$extracontentstripshow && $extracontentstripdata_db==$extracontentstripdata && $featuredprofilesshow_db==$featuredprofilesshow && $featuredprofilescount_db==$featuredprofilescount && $premiumprofilesshow_db==$premiumprofilesshow && $premiumprofilescount_db==$premiumprofilescount && $recentlyaddedprofilesshow_db==$recentlyaddedprofilesshow && $recentlyaddedprofilescount_db==$recentlyaddedprofilescount && $profilefiltershow_db==$profilefiltershow && $profilefiltervalues_db==$profilefiltervalues && $additional_footer_content1_display_db==$additional_footer_content1_display && $additional_footer_content1_db==$additional_footer_content1 && $additional_footer_content2_display_db==$additional_footer_content2_display && $additional_footer_content2_db==$additional_footer_content2 && $additional_footer_content3_display_db==$additional_footer_content3_display && $additional_footer_content3_db==$additional_footer_content3 && $additional_footer_content4_display_db==$additional_footer_content4_display && $additional_footer_content4_db==$additional_footer_content4 )
                        {
                                echo "Same values as before";
                                exit;
                        }

        	       $sql_update = "UPDATE homepagesetup SET homepageHeading='$homepageHeading',footercontentshow='$footercontentshow',footercontent='$footercontent',extracontentstripshow='$extracontentstripshow',extracontentstripdata='$extracontentstripdata',featuredprofilesshow='$featuredprofilesshow',featuredprofilescount='$featuredprofilescount',premiumprofilesshow='$premiumprofilesshow',premiumprofilescount='$premiumprofilescount',recentlyaddedprofilesshow='$recentlyaddedprofilesshow',recentlyaddedprofilescount='$recentlyaddedprofilescount',profilefiltershow='$profilefiltershow',profilefiltervalues='$profilefiltervalues',additional_footer_content1_display='$additional_footer_content1_display',additional_footer_content1='$additional_footer_content1',additional_footer_content2_display='$additional_footer_content2_display',additional_footer_content2='$additional_footer_content2',additional_footer_content3_display='$additional_footer_content3_display',additional_footer_content3='$additional_footer_content3',additional_footer_content4_display='$additional_footer_content4_display',additional_footer_content4='$additional_footer_content4'";
        }
        else
        if($count==0)
        {
        	$sql_update = "INSERT INTO homepagesetup(homepageHeading,footercontentshow,footercontent,extracontentstripshow,extracontentstripdata,featuredprofilesshow,featuredprofilescount,premiumprofilesshow,premiumprofilescount,recentlyaddedprofilesshow,recentlyaddedprofilescount,profilefiltershow,profilefiltervalues,additional_footer_content1_display,additional_footer_content1,additional_footer_content2_display,additional_footer_content2,additional_footer_content3_display,additional_footer_content3,additional_footer_content4_display,additional_footer_content4) VALUES('$homepageHeading','$footercontentshow','$footercontent','$extracontentstripshow','$extracontentstripdata','$featuredprofilesshow','$featuredprofilescount','$premiumprofilesshow','$premiumprofilescount','$recentlyaddedprofilesshow','$recentlyaddedprofilescount','$profilefiltershow','$profilefiltervalues','$additional_footer_content1_display','$additional_footer_content1','$additional_footer_content2_display','$additional_footer_content2','$additional_footer_content3_display','$additional_footer_content3','$additional_footer_content4_display','$additional_footer_content4')";
        }

        //echo $sql_update;exit;
        if($link->exec($sql_update))
        {
        	echo json_encode("success");
        	exit;
        }
        else
        {
        	echo json_encode("Something went wrong try after some time");
        	exit;
        }
	}
?>