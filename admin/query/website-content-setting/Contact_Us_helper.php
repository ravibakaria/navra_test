<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

        $today_datetime = date('Y-m-d H:i:s');
        
	$task = quote_smart($_POST['task']);

        $admin_id = $_SESSION['admin_id'];

        /*************    Fetch states of country   ****************/
        if($task=='Fetch_state_data')
        {
                $response = null;
                $output_data = null;
                $country_id = quote_smart($_POST['country_id']);

                $query  = "SELECT * FROM `states` WHERE `country_id`='$country_id' ORDER BY `name` ASC";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $response = "<option value='0' selected>Select State</option>";
                foreach( $result as $row )
                {
                        $state_name =  $row['name'];
                        $state_id = $row['id']; 

                        $response .= "<option value='".$state_id."'>".$state_name."</option>";
                }

                $stmt1 = $link->prepare("SELECT * FROM countries WHERE id='$country_id'");
                $stmt1->execute();
                $row1 = $stmt1->fetch(PDO::FETCH_ASSOC);

                $phonecode = $row1['phonecode'];

                $output_data = json_encode(array($response,$phonecode));
                echo $output_data;
                exit;
        }

        /*************    Fetch cities of state   ****************/
        if($task == "Fetch_city_data")
        {
                $response = null;
                $state_id = quote_smart($_POST['state_id']);

                $query  = "SELECT * FROM `cities` WHERE `state_id`='$state_id' ORDER BY `name` ASC";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $response = "<option value='0' selected>Select City</option>";
                foreach( $result as $row )
                {
                        $city_name =  $row['name'];
                        $city_id = $row['id']; 

                        $response .= "<option value='".$city_id."'>".$city_name."</option>";
                }

                echo $response;
                exit;
        }

	if($task=='Update-Contact-Us-Page')
	{
        	$enquiry_email = $_POST['enquiry_email'];
                $companyName = $_POST['companyName'];
                $street = $_POST['street'];
                $city = $_POST['city'];
                $state = $_POST['state'];
                $country = $_POST['country'];
                $postalCode = $_POST['postalCode'];
                $phonecode = $_POST['phonecode'];
                $phone = $_POST['phone'];
                $companyEmail = $_POST['companyEmail'];
                $companyURL = $_POST['companyURL'];
                
        	$sql_chk = "SELECT * FROM contact_us";
        	$sql_chk= $link->prepare($sql_chk);
        	$sql_chk->execute();
                $result_chk = $sql_chk->fetch();
                $count=$sql_chk->rowCount();
                if($count>0)
                {
                        $enquiry_email_db = $result_chk['enquiry_email'];
                        $companyName_db = $result_chk['companyName'];
                        $street_db = $result_chk['street'];
                        $city_db = $result_chk['city'];
                        $state_db = $result_chk['state'];
                        $country_db = $result_chk['country'];
                        $postalCode_db = $result_chk['postalCode'];
                        $phonecode_db = $result_chk['phonecode'];
                        $phone_db = $result_chk['phone'];
                        $companyEmail_db = $result_chk['companyEmail'];
                        $companyURL_db = $result_chk['companyURL'];

                        if($enquiry_email_db==$enquiry_email && $companyName_db==$companyName && $street_db==$street && $city_db==$city && $state_db==$state && $country_db==$country && $postalCode_db==$postalCode && $phonecode_db==$phonecode && $phone_db==$phone && $companyEmail_db==$companyEmail && $companyURL_db==$companyURL)
                        {
                                echo "Same values as before";
                                exit;
                        }
                        else
                        {
                                $sql_update = "UPDATE contact_us SET enquiry_email='$enquiry_email',companyName='$companyName',street='$street',city='$city',state='$state',country='$country',postalCode='$postalCode',phonecode='$phonecode',phone='$phone',companyEmail='$companyEmail',companyURL='$companyURL',userid='$admin_id',updated_at='$today_datetime'";
                        }
                	
                }
                else
                if($count==0)
                {
                	$sql_update = "INSERT INTO contact_us(enquiry_email,companyName,street,city,state,country,postalCode,phonecode,phone,companyEmail,companyURL,userid,created_at) VALUES('$enquiry_email','$companyName','$street','$city','$state','$country','$postalCode','$phonecode','$phone','$companyEmail','$companyURL','$admin_id','$today_datetime')";
                }

                //echo $sql_update;exit;

                if($link->exec($sql_update))
                {
                	echo "success";
                	exit;
                }
                else
                {
                	echo "Something went wrong try after some time";
                	exit;
                }
	}
?>