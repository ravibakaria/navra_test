<?php
        session_start();
        require_once '../../../config/config.php'; 
        include('../../../config/dbconnect.php');    //database connection
        include('../../../config/functions.php');   //strip query string

        $admin_id = $_SESSION['admin_id'];

        $today_datetime = date('Y-m-d H:i:s');

        $task = quote_smart($_POST['task']);

        if($task="update_ads_mgmt_setup")
        {
                $leaderBoardDisplay1 = $_POST['leaderBoardDisplay1'];
                $leaderBoardData1 = $_POST['leaderBoardData1'];
                $leaderBoardData1 = str_replace('\\', '\\\\', $leaderBoardData1);
                $leaderBoardData1 = str_replace("'", "\\'", $leaderBoardData1);

                $leaderBoardDisplay2 = $_POST['leaderBoardDisplay2'];
                $leaderBoardData2 = $_POST['leaderBoardData2'];
                $leaderBoardData2 = str_replace('\\', '\\\\', $leaderBoardData2);
                $leaderBoardData2 = str_replace("'", "\\'", $leaderBoardData2);

                $leaderBoardDisplay3 = $_POST['leaderBoardDisplay3'];
                $leaderBoardData3 = $_POST['leaderBoardData3'];
                $leaderBoardData3 = str_replace('\\', '\\\\', $leaderBoardData3);
                $leaderBoardData3 = str_replace("'", "\\'", $leaderBoardData3);

                $squarePopupDisplay1 = $_POST['squarePopupDisplay1'];
                $squarePopupData1 = $_POST['squarePopupData1'];
                $squarePopupData1 = str_replace('\\', '\\\\', $squarePopupData1);
                $squarePopupData1 = str_replace("'", "\\'", $squarePopupData1);

                $squarePopupDisplay2 = $_POST['squarePopupDisplay2'];
                $squarePopupData2 = $_POST['squarePopupData2'];
                $squarePopupData2 = str_replace('\\', '\\\\', $squarePopupData2);
                $squarePopupData2 = str_replace("'", "\\'", $squarePopupData2);

                $squarePopupDisplay3 = $_POST['squarePopupDisplay3'];
                $squarePopupData3 = $_POST['squarePopupData3'];
                $squarePopupData3 = str_replace('\\', '\\\\', $squarePopupData3);
                $squarePopupData3 = str_replace("'", "\\'", $squarePopupData3);

                $skyScrapperDisplay1 = $_POST['skyScrapperDisplay1'];
                $skyScrapperData1 = $_POST['skyScrapperData1'];
                $skyScrapperData1 = str_replace('\\', '\\\\', $skyScrapperData1);
                $skyScrapperData1 = str_replace("'", "\\'", $skyScrapperData1);

                $skyScrapperDisplay2 = $_POST['skyScrapperDisplay2'];
                $skyScrapperData2 = $_POST['skyScrapperData2'];
                $skyScrapperData2 = str_replace('\\', '\\\\', $skyScrapperData2);
                $skyScrapperData2 = str_replace("'", "\\'", $skyScrapperData2);

                $skyScrapperDisplay3 = $_POST['skyScrapperDisplay3'];
                $skyScrapperData3 = $_POST['skyScrapperData3'];
                $skyScrapperData3 = str_replace('\\', '\\\\', $skyScrapperData3);
                $skyScrapperData3 = str_replace("'", "\\'", $skyScrapperData3);


                $sql_chk = "SELECT * FROM ads_management";
                $stmt = $link->prepare($sql_chk);
                $stmt->execute();
                $result_chk = $stmt->fetch();
                $count=$stmt->rowCount();

                if($count==0)
                {
                        $sql_insert = "INSERT INTO ads_management(leaderBoardDisplay1,leaderBoardData1,leaderBoardDisplay2,leaderBoardData2,leaderBoardDisplay3,leaderBoardData3,squarePopupDisplay1,squarePopupData1,squarePopupDisplay2,squarePopupData2,squarePopupDisplay3,squarePopupData3,skyScrapperDisplay1,skyScrapperData1,skyScrapperDisplay2,skyScrapperData2,skyScrapperDisplay3,skyScrapperData3,updatedBy,updatedOn) VALUES('$leaderBoardDisplay1','$leaderBoardData1','$leaderBoardDisplay2','$leaderBoardData2','$leaderBoardDisplay3','$leaderBoardData3','$squarePopupDisplay1','$squarePopupData1','$squarePopupDisplay2','$squarePopupData2','$squarePopupDisplay3','$squarePopupData3','$skyScrapperDisplay1','$skyScrapperData1','$skyScrapperDisplay2','$skyScrapperData2','$skyScrapperDisplay3','$skyScrapperData3','$admin_id','$today_datetime')";
                }
                else
                {

                        $leaderBoardDisplay1_db = $result_chk['leaderBoardDisplay1'];
                        $leaderBoardData1_db = $result_chk['leaderBoardData1'];
                        $leaderBoardDisplay2_db = $result_chk['leaderBoardDisplay2'];
                        $leaderBoardData2_db = $result_chk['leaderBoardData2'];
                        $leaderBoardDisplay3_db = $result_chk['leaderBoardDisplay3'];
                        $leaderBoardData3_db = $result_chk['leaderBoardData3'];
                        $squarePopupDisplay1_db = $result_chk['squarePopupDisplay1'];
                        $squarePopupData1_db = $result_chk['squarePopupData1'];
                        $squarePopupDisplay2_db = $result_chk['squarePopupDisplay2'];
                        $squarePopupData2_db = $result_chk['squarePopupData2'];
                        $squarePopupDisplay3_db = $result_chk['squarePopupDisplay3'];
                        $squarePopupData3_db = $result_chk['squarePopupData3'];
                        $skyScrapperDisplay1_db = $result_chk['skyScrapperDisplay1'];
                        $skyScrapperData1_db = $result_chk['skyScrapperData1'];
                        $skyScrapperDisplay2_db = $result_chk['skyScrapperDisplay2'];
                        $skyScrapperData2_db = $result_chk['skyScrapperData2'];
                        $skyScrapperDisplay3_db = $result_chk['skyScrapperDisplay3'];
                        $skyScrapperData3_db = $result_chk['skyScrapperData3'];

                        if($leaderBoardDisplay1_db==$leaderBoardDisplay1 && $leaderBoardData1_db==$leaderBoardData1 && $leaderBoardDisplay2_db==$leaderBoardDisplay2 && $leaderBoardData2_db==$leaderBoardData2 && $leaderBoardDisplay3_db==$leaderBoardDisplay3 && $leaderBoardData3_db==$leaderBoardData3 && $squarePopupDisplay1_db==$squarePopupDisplay1 && $squarePopupData1_db==$squarePopupData1 && $squarePopupDisplay2_db==$squarePopupDisplay2 && $squarePopupData2_db==$squarePopupData2 && $squarePopupDisplay3_db==$squarePopupDisplay3 && $squarePopupData3_db==$squarePopupData3 && $skyScrapperDisplay1_db==$skyScrapperDisplay1 && $skyScrapperData1_db==$skyScrapperData1 && $skyScrapperDisplay2_db==$skyScrapperDisplay2 && $skyScrapperData2_db==$skyScrapperData2 && $skyScrapperDisplay3_db==$skyScrapperDisplay3 && $skyScrapperData3_db==$skyScrapperData3 )
                        {
                                echo json_encode("Same values as before.");
                                exit;
                        }

                        $sql_insert = "UPDATE ads_management SET leaderBoardDisplay1='$leaderBoardDisplay1',leaderBoardData1='$leaderBoardData1',leaderBoardDisplay2='$leaderBoardDisplay2',leaderBoardData2='$leaderBoardData2',leaderBoardDisplay3='$leaderBoardDisplay3',leaderBoardData3='$leaderBoardData3',squarePopupDisplay1='$squarePopupDisplay1',squarePopupData1='$squarePopupData1',squarePopupDisplay2='$squarePopupDisplay2',squarePopupData2='$squarePopupData2',squarePopupDisplay3='$squarePopupDisplay3',squarePopupData3='$squarePopupData3',skyScrapperDisplay1='$skyScrapperDisplay1',skyScrapperData1='$skyScrapperData1',skyScrapperDisplay2='$skyScrapperDisplay2',skyScrapperData2='$skyScrapperData2',skyScrapperDisplay3='$skyScrapperDisplay3',skyScrapperData3='$skyScrapperData3',updatedBy='$admin_id',updatedOn='$today_datetime'"; 
                }

                //echo $sql_insert;exit;
                if($link->exec($sql_insert))
                {
                        echo json_encode("success");
                        exit;
                }
                else
                {
                        echo json_encode("Something went wrong try after some time");
                        exit;
                }
        }
?>