<?php

	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];

	if($_POST['prev_image_available']=='1' && ($_FILES['popup_image']['name']=='' || $_FILES['popup_image']['name']==null))
	{
		if(isset($_POST['popup_display_or_not']))
		{
			$popup_display_or_not = '1';
		}
		else
		{
			$popup_display_or_not = '0';
		}
		
	    $destination_url = $_POST['destination_url'];
	    $target = $_POST['target'];

		$sql_update = "UPDATE popup_setting SET popup_display_or_not='$popup_display_or_not',destination_url='$destination_url',target='$target',updated_by='$admin_id',updated_at='$today'";

		if($link->exec($sql_update))
	    {
	    	echo "ok";
	    	exit;
	    }
	    else
	    {
	    	echo "Something went wrong. Try after some time";
	    	exit;
	    }
	}

	if(!isset($_POST['popup_display_or_not']) || empty($_POST['popup_display_or_not']))
	{
		$sql_update = "UPDATE popup_setting SET popup_display_or_not='0',updated_by='$admin_id',updated_at='$today'";

		if($link->exec($sql_update))
	    {
	    	echo "ok";
	    	exit;
	    }
	    else
	    {
	    	echo "Something went wrong. Try after some time";
	    	exit;
	    }
	}
	else
	if((isset($_POST['popup_display_or_not']) && !empty($_POST['popup_display_or_not'])) || (isset($_POST['destination_url']) && !empty($_POST['destination_url'])) || !empty($_FILES['popup_image']['name']))
	{
		$uploadedFile = '';
	    $fileName = time().'_'.$_FILES['popup_image']['name'];
	    if(!empty($_FILES["popup_image"]["type"])){
	        $fileName = time().'_'.$_FILES['popup_image']['name'];
	        $valid_extensions = array("jpeg", "jpg", "png");
	        $temporary = explode(".", $_FILES["popup_image"]["name"]);
	        $file_extension = end($temporary);
	        if((($_FILES["popup_image"]["type"] == "image/png") || ($_FILES["popup_image"]["type"] == "image/jpg") || ($_FILES["popup_image"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions))
	        {
	            $sourcePath = $_FILES['popup_image']['tmp_name'];
	            $targetPath = "../../../images/popup/".$fileName;
	            $db_path = "images/popup/".$fileName;
	            if(move_uploaded_file($sourcePath,$targetPath)){
	                $uploadedFile = $fileName;
	            }
	        }
	    }
	    
	    $popup_display_or_not = $_POST['popup_display_or_not'];
	    $destination_url = $_POST['destination_url'];
	    $target = $_POST['target'];

	    $sql_chk = "SELECT * FROM popup_setting";
	    $stmt = $link->prepare($sql_chk);
	    $stmt->execute();
	    $count = $stmt->rowCount();
	    $result = $stmt->fetch();

	    if($count==0)
	    {
	    	$sql_update = "INSERT INTO popup_setting(popup_display_or_not,popup_image,destination_url,target,created_by,created_at) VALUES('$popup_display_or_not','$db_path','$destination_url','$target','$admin_id','$today')";
	    }
	    else
	    if($count>0)
	    {
	    	$previous_popup = $result['popup_image'];

	    	unlink("../../../".$previous_popup);

	    	$sql_update = "UPDATE popup_setting SET popup_display_or_not='$popup_display_or_not',popup_image='$db_path',destination_url='$destination_url',target='$target',updated_by='$admin_id',updated_at='$today'";
	    }
	    
	    if($link->exec($sql_update))
	    {
	    	echo "ok";
	    	exit;
	    }
	    else
	    {
	    	echo "Something went wrong. Try after some time";
	    	exit;
	    }
	    
	}
	
?>