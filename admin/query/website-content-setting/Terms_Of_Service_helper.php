<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

        $today_datetime = date('Y-m-d H:i:s');
        
	$task = quote_smart($_POST['task']);

	if($task=='Update_Terms_Of_Service_Page')
	{
        	$content = $_POST['content'];
                $content = str_replace('\\', '\\\\', $content);
                $content = str_replace("'", "\\'", $content);
                $terms_chk_show = $_POST['terms_chk_show'];
        	$userid = $_SESSION['admin_id'];

        	$sql_chk = "SELECT * FROM terms_of_service";
        	$sql_chk= $link->prepare($sql_chk);
        	$sql_chk->execute();
                $result_chk = $sql_chk->fetch();
                $count=$sql_chk->rowCount();
                if($count>0)
                {
                	$sql_update = "UPDATE terms_of_service SET content='$content',display='$terms_chk_show',userid='$userid',updated_at='$today_datetime'";
                }
                else
                if($count==0)
                {
                	$sql_update = "INSERT INTO terms_of_service(content,display,userid,created_at) VALUES('$content','$terms_chk_show','$userid','$today_datetime')";
                }

                if($link->exec($sql_update))
                {
                	echo json_encode("success");
                	exit;
                }
                else
                {
                	echo json_encode("Something went wrong try after some time");
                	exit;
                }
	}
?>