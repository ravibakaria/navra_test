<?php
    session_start();
    require_once '../../config/config.php'; 
    include('../../config/dbconnect.php');    //database connection
    include('../../config/functions.php');   //strip query string
    include('../../config/setup-values.php');   //strip query string
    require_once "../../config/email/class.phpmailer.php";
    //require_once "../../config/email/class.smtp.php";
    $admin_id = $_SESSION['admin_id'];

    $today_datetime = date('Y-m-d H:i:s');
    
    $task = quote_smart($_POST['task']);

    
    /************   General Setting   *****************/
    if($task == "Update_General_Setup")
    {
        $WebSiteBasePath = $_POST['WebSiteBasePath'];
        $WebSiteTitle = $_POST['WebSiteTitle'];
        $WebSiteTagline = $_POST['WebSiteTagline'];
        $EmailAddress = $_POST['EmailAddress'];
        $TermOfServiceURL = $_POST['TermOfServiceURL'];
        $PrivacyPolicyURL = $_POST['PrivacyPolicyURL'];
        $AllowedFileAttachmentTypes = $_POST['AllowedFileAttachmentTypes'];

        $sql_chk = $link->prepare("SELECT * FROM `generalsetup`"); 
        $sql_chk->execute();
        $result_chk = $sql_chk->fetch();
        $count=$sql_chk->rowCount();
        if($count==0)
        {
            $sql_general_setup = "INSERT INTO `generalsetup`(`WebSiteBasePath`,`WebSiteTitle`, `WebSiteTagline`, `EmailAddress`, `TermOfServiceURL`,`PrivacyPolicyURL`, `AllowedFileAttachmentTypes`) VALUES ('$WebSiteBasePath','$WebSiteTitle','$WebSiteTagline','$EmailAddress','$TermOfServiceURL','$PrivacyPolicyURL','$AllowedFileAttachmentTypes')";
        }
        else
        {
            $WebSiteBasePath_db = $result_chk['WebSiteBasePath'];
            $WebSiteTitle_db = $result_chk['WebSiteTitle'];
            $WebSiteTagline_db = $result_chk['WebSiteTagline'];
            $EmailAddress_db = $result_chk['EmailAddress'];
            $TermOfServiceURL_db = $result_chk['TermOfServiceURL'];
            $PrivacyPolicyURL_db = $result_chk['PrivacyPolicyURL'];
            $AllowedFileAttachmentTypes_db = $result_chk['AllowedFileAttachmentTypes'];

            if(($WebSiteBasePath_db==$WebSiteBasePath) && ($WebSiteTitle_db==$WebSiteTitle) && ($WebSiteTagline_db==$WebSiteTagline) && ($EmailAddress_db==$EmailAddress) && ($TermOfServiceURL_db==$TermOfServiceURL) && ($PrivacyPolicyURL_db==$PrivacyPolicyURL) && ($AllowedFileAttachmentTypes_db==$AllowedFileAttachmentTypes))
            {
                echo "Same values as before.";
                exit;
            }

            $sql_general_setup = "UPDATE `generalsetup` set `WebSiteBasePath`='$WebSiteBasePath',`WebSiteTitle`='$WebSiteTitle',`WebSiteTagline`='$WebSiteTagline',`EmailAddress`='$EmailAddress',`TermOfServiceURL`='$TermOfServiceURL',`PrivacyPolicyURL`='$PrivacyPolicyURL',`AllowedFileAttachmentTypes`='$AllowedFileAttachmentTypes'";
        }

        if($link->exec($sql_general_setup))
        {
            echo "success";
            exit;
        }
        else
        {
            echo "Something went wrong try after some time";
            exit;
        }
    }

    /************   General Security Setting   *****************/
    if($task == "Update_General_Security_Setup")
    {
        $recaptchaAllowed = $_POST['recaptchaAllowed'];
        $reCaptchaSiteKey = $_POST['reCaptchaSiteKey'];
        $reCaptchaSecretKey = $_POST['reCaptchaSecretKey'];
        $MinimumUserPasswordLength = $_POST['MinimumUserPasswordLength'];
        $MaximumUserPasswordLength = $_POST['MaximumUserPasswordLength'];
        $hideLastNameOfMember = $_POST['hideLastNameOfMember'];

        $sql_chk = $link->prepare("SELECT * FROM `generalsecurity`"); 
        $sql_chk->execute();
        $result_chk = $sql_chk->fetch();
        $count=$sql_chk->rowCount();
        if($count==0)
        {
            $sql_general_setup = "INSERT INTO `generalsecurity`(`recaptchaAllowed`, `reCaptchaSiteKey`, `reCaptchaSecretKey`, `MinimumUserPasswordLength`, `MaximumUserPasswordLength`,`hideLastNameOfMember`) VALUES ('$recaptchaAllowed','$reCaptchaSiteKey','$reCaptchaSecretKey','$MinimumUserPasswordLength','$MaximumUserPasswordLength','$hideLastNameOfMember')";
        }
        else
        {
            $recaptchaAllowed_db = $result_chk['recaptchaAllowed'];
            $reCaptchaSiteKey_db = $result_chk['reCaptchaSiteKey'];
            $reCaptchaSecretKey_db = $result_chk['reCaptchaSecretKey'];
            $MinimumUserPasswordLength_db = $result_chk['MinimumUserPasswordLength'];
            $MaximumUserPasswordLength_db = $result_chk['MaximumUserPasswordLength'];
            $hideLastNameOfMember_db = $result_chk['hideLastNameOfMember'];

            if(($recaptchaAllowed_db==$recaptchaAllowed) && ($reCaptchaSiteKey_db==$reCaptchaSiteKey) && ($reCaptchaSecretKey_db==$reCaptchaSecretKey) && ($MinimumUserPasswordLength_db==$MinimumUserPasswordLength) && ($MaximumUserPasswordLength_db==$MaximumUserPasswordLength) && ($hideLastNameOfMember_db==$hideLastNameOfMember))
            {
                echo "Same values as before.";
                exit;
            }
            
            $sql_general_setup = "UPDATE `generalsecurity` set `recaptchaAllowed`='$recaptchaAllowed',`reCaptchaSiteKey`='$reCaptchaSiteKey',`reCaptchaSecretKey`='$reCaptchaSecretKey',`MinimumUserPasswordLength`='$MinimumUserPasswordLength',`MaximumUserPasswordLength`='$MaximumUserPasswordLength',`hideLastNameOfMember`='$hideLastNameOfMember'";
        }

        if($link->exec($sql_general_setup))
        {
            echo "success";
            exit;
        }
        else
        {
            echo "Something went wrong try after some time";
            exit;
        }
    }
    

    /*******************   Localization Setting   ********************/
    if($task == "Update_Localization_Setup")
    {
        $DefaultCountry = $_POST['DefaultCountry'];
        $DefaultTimeZone = $_POST['DefaultTimeZone'];
        $DefaultCurrency = $_POST['DefaultCurrency'];

        $sql_chk = $link->prepare("SELECT * FROM `localizationsetup`"); 
        $sql_chk->execute();
        $result_chk = $sql_chk->fetch();
        $count=$sql_chk->rowCount();
        if($count==0)
        {
            $sql_general_setup = "INSERT INTO `localizationsetup`(`DefaultCountry`, `DefaultTimeZone`, `DefaultCurrency`) VALUES ('$DefaultCountry','$DefaultTimeZone','$DefaultCurrency')";
        }
        else
        {
            $DefaultCountry_db = $result_chk['DefaultCountry'];
            $DefaultTimeZone_db = $result_chk['DefaultTimeZone'];
            $DefaultCurrency_db = $result_chk['DefaultCurrency'];

            if(($DefaultCountry_db==$DefaultCountry) && ($DefaultTimeZone_db==$DefaultTimeZone) && ($DefaultCurrency_db==$DefaultCurrency))
            {
                echo "Same values as before.";
                exit;
            }
            
            $sql_general_setup = "UPDATE `localizationsetup` set `DefaultCountry`='$DefaultCountry',`DefaultTimeZone`='$DefaultTimeZone',`DefaultCurrency`='$DefaultCurrency'";
        }

        if($link->exec($sql_general_setup))
        {
            echo "success";
            exit;
        }
        else
        {
            echo "Something went wrong try after some time";
            exit;
        }
    }

    /************   Test email validation   *****************/
    if($task == "Validate_mail_Relay_Setting")
    {
                
        $FromName = $_POST['FromName'];
        $FromEmail = $_POST['FromEmail'];
        $BCCEmail = $_POST['BCCEmail'];
        $SMTPHost = $_POST['SMTPHost'];
        $SMTPPort = $_POST['SMTPPort'];
        $SMTPUsername = $_POST['SMTPUsername'];
        $SMTPPassword = $_POST['SMTPPassword'];
        $SMTPSSL = $_POST['SMTPSSL'];

        $to = $FromEmail;
        $name = $FromName;

        $smtphost = $SMTPHost;
        $smtpport = $SMTPPort;
        $smtpdebug = 0;
        $smtpauth = true;
        $smtpsecure = $SMTPSSL;
        $smtpusername = $SMTPUsername;
        $smtppassword = $SMTPPassword;
        $smtpfromname = $FromName;
        $smtpfromid = $FromEmail;
        /*$smtpreplytoname = $FromName;
        $smtpreplytoid = $FromEmail;*/
    
        if($BCCEmail!='')
        {
            $smtpbccname1 = $BCCEmail;
            $smtpbccid1 = $BCCEmail;
        }

        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        //Tell PHPMailer to use SMTP
        $mail->IsSMTP();
        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug  = $smtpdebug;

        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';

        //Set the hostname of the mail server
        $mail->Host       = $smtphost;

        //Set the SMTP port number - likely to be 25, 465 or 587
        $mail->Port       = $smtpport;

        //Whether to use SMTP authentication
        if ($smtpauth == "true")
        {
            $mail->SMTPAuth   = true;

            //Username to use for SMTP authentication
            $mail->Username   = $smtpusername;

            //Password to use for SMTP authentication
            $mail->Password   = $smtppassword;
        }

        $mail->SMTPKeepAlive = true;

        if ($smtpsecure == "ssl")
        {
            $mail->SMTPSecure = "ssl";
        }
        else if ($smtpsecure == "tls")
        {
            $mail->SMTPOptions = array(
                'tls' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );              
        }

        //Set who the message is to be sent from
        if ($smtpfromid != '' && $smtpfromname != '')
            $mail->SetFrom($smtpfromid, $smtpfromname);

        //Set an alternative reply-to address
        /*if ($smtpreplytoid != '' && $smtpreplytoname != '')
            $mail->AddReplyTo($smtpreplytoid, $smtpreplytoname);*/

        if ($BCCEmail != '' && $BCCEmail != '')
            $mail->AddBCC($smtpbccid1, $smtpbccname1);

        $mail->AddAddress($to, $name);
        
        
        
        //Set the subject line
        $mail->Subject = "Test Email";
        $message = "Hi,<br/><br/>
                This is a test email to validate email relay setting.<br/><br/>
                Thank You.";
                
        //Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
        $mail->MsgHTML($message);
        
        if(!$mail->Send())
        {
            echo "danger";
            exit;
        }   
        else
        {
            echo "success";
            exit;
        }
    }

    /************   Mail Relay Setting   *****************/
    if($task == "Update_Mail_Relay_Setup")
    {
        $FromName = $_POST['FromName'];
        $FromEmail = $_POST['FromEmail'];
        $BCCEmail = $_POST['BCCEmail'];
        $SMTPHost = $_POST['SMTPHost'];
        $SMTPPort = $_POST['SMTPPort'];
        $SMTPUsername = $_POST['SMTPUsername'];
        $SMTPPassword = $_POST['SMTPPassword'];
        $SMTPPassword = str_replace('\\', '\\\\', $SMTPPassword);
        $SMTPPassword = str_replace("'", "\\'", $SMTPPassword);

        $SMTPSSL = $_POST['SMTPSSL'];
        $GlobalEmailSignature = $_POST['GlobalEmailSignature'];
        $GlobalEmailSignature = str_replace('\\', '\\\\', $GlobalEmailSignature);
        $GlobalEmailSignature = str_replace("'", "\\'", $GlobalEmailSignature);

        $emailFooterText = $_POST['emailFooterText'];
        $emailFooterText = str_replace('\\', '\\\\', $emailFooterText);
        $emailFooterText = str_replace("'", "\\'", $emailFooterText);

        $sql_chk = $link->prepare("SELECT * FROM `mailrelaysetup`"); 
        $sql_chk->execute();
        $result_chk = $sql_chk->fetch();
        $count=$sql_chk->rowCount();
        if($count==0)
        {
            $sql_general_setup = "INSERT INTO `mailrelaysetup`(`FromName`, `FromEmail`, `BCCEmail`,`SMTPHost`, `SMTPPort`, `SMTPUsername`, `SMTPPassword`, `SMTPSSL`, `GlobalEmailSignature`, `emailFooterText`) VALUES ('$FromName','$FromEmail','$BCCEmail','$SMTPHost','$SMTPPort','$SMTPUsername','$SMTPPassword','$SMTPSSL','$GlobalEmailSignature','$emailFooterText')";
        }
        else
        {
            $FromName_db = $result_chk['FromName'];
            $FromEmail_db = $result_chk['FromEmail'];
            $BCCEmail_db = $result_chk['BCCEmail'];
            $SMTPHost_db = $result_chk['SMTPHost'];
            $SMTPPort_db = $result_chk['SMTPPort'];
            $SMTPUsername_db = $result_chk['SMTPUsername'];
            $SMTPPassword_db = $result_chk['SMTPPassword'];
            $SMTPSSL_db = $result_chk['SMTPSSL'];
            $GlobalEmailSignature_db = $result_chk['GlobalEmailSignature'];
            $emailFooterText_db = $result_chk['emailFooterText'];

            if(($FromName_db==$FromName) && ($FromEmail_db==$FromEmail) && ($BCCEmail_db==$BCCEmail) && ($SMTPHost_db==$SMTPHost) && ($SMTPPort_db==$SMTPPort) && ($SMTPUsername_db==$SMTPUsername) && ($SMTPPassword_db==$SMTPPassword) && ($SMTPSSL_db==$SMTPSSL) && ($GlobalEmailSignature_db==$GlobalEmailSignature) && ($emailFooterText_db==$emailFooterText))
            {
                echo json_encode("Same values as before.");
                exit;
            }

            $sql_general_setup = "UPDATE `mailrelaysetup` set `FromName`='$FromName',`FromEmail`='$FromEmail',`BCCEmail`='$BCCEmail',`SMTPHost`='$SMTPHost',`SMTPPort`='$SMTPPort',`SMTPUsername`='$SMTPUsername',`SMTPPassword`='$SMTPPassword',`SMTPSSL`='$SMTPSSL',`GlobalEmailSignature`='$GlobalEmailSignature',`emailFooterText`='$emailFooterText'";
        }

        if($link->exec($sql_general_setup))
        {
            echo json_encode("success");
            exit;
        }
        else
        {
            echo json_encode("Something went wrong try after some time");
            exit;
        }
    }

    /************   Custom Script Setting   *****************/
    if($task == "Update_Custom_Script_Data")
    {
        $header_script_display = $_POST['header_script_display'];
        $header_script = $_POST['header_script'];
        $footer_script_display = $_POST['footer_script_display'];
        $footer_script = $_POST['footer_script'];

        /***********  header file content  ************/
        $header_file_data = null;
        $header_script_file_name = "../../scripts/custom_header_script.js";
        $db_header_file_name = "scripts/custom_header_script.js";
        
        $header_script_file = fopen($header_script_file_name, "w") or die("Unable to open file!");

        $header_file_data = "$header_script";
        fwrite($header_script_file, $header_file_data);
        fclose($header_script_file);

        /***********  footer file content  ************/
        $footer_file_data = null;
        $footer_script_file_name = "../../scripts/custom_footer_script.js";
        $db_footer_file_name = "scripts/custom_footer_script.js";
        
        $footer_script_file = fopen($footer_script_file_name, "w") or die("Unable to open file!");

        $footer_file_data = "$footer_script";
        fwrite($footer_script_file, $footer_file_data);
        fclose($footer_script_file);

        

        $sql_chk = $link->prepare("SELECT * FROM `custom_script`"); 
        $sql_chk->execute();
        $result_chk = $sql_chk->fetch();
        $count=$sql_chk->rowCount();
        if($count==0)
        {
            $sql_custom_script_setup = "INSERT INTO `custom_script`(`header_script_display`,`header_script`, `footer_script_display`, `footer_script`, `updated_by`,`updated_on`) VALUES ('$header_script_display','$db_header_file_name','$footer_script_display','$db_footer_file_name','$admin_id','$today_datetime')";
        }
        else
        {
            $sql_custom_script_setup = "UPDATE `custom_script` set `header_script_display`='$header_script_display',`header_script`='$db_header_file_name',`footer_script_display`='$footer_script_display',`footer_script`='$db_footer_file_name',`updated_by`='$admin_id',`updated_on`='$today_datetime'";
        }

        //echo  $sql_custom_script_setup;exit;
        if($link->exec($sql_custom_script_setup))
        {
            echo json_encode("success");
            exit;
        }
        else
        {
            echo json_encode("Something went wrong try after some time");
            exit;
        }
    }

    /************   Cron Job Setting   *****************/
    if($task == "Update_Cron_Job_Sheduler")
    {
        $EmailVerificationReminderRunOrNot = $_POST['EmailVerificationReminderRunOrNot'];
        $EmailVerificationReminderRunAt = $_POST['EmailVerificationReminderRunAt'];
        $ProfileCompletenessReminderRunOrNot = $_POST['ProfileCompletenessReminderRunOrNot'];
        $ProfileCompletenessRunAt = $_POST['ProfileCompletenessRunAt'];
        $BirthdayReminderRunOrNot = $_POST['BirthdayReminderRunOrNot'];
        $BirthdayReminderRunAt = $_POST['BirthdayReminderRunAt'];
        $NewProfileReminderRunOrNot = $_POST['NewProfileReminderRunOrNot'];
        $NewProfileReminderFrom = $_POST['NewProfileReminderFrom'];
        $NewProfileReminderRunAt = $_POST['NewProfileReminderRunAt'];
        $UnreadMessageReminderRunOrNot = $_POST['UnreadMessageReminderRunOrNot'];
        $UnreadMessageReminderRunAt = $_POST['UnreadMessageReminderRunAt'];
        $MembershipRenewalReminderRunOrNot = $_POST['MembershipRenewalReminderRunOrNot'];
        $MembershipRenewalReminderRunAt = $_POST['MembershipRenewalReminderRunAt'];

        $sql_chk = "SELECT * FROM cron_jobs";
        $stmt_chk = $link->prepare($sql_chk);
        $stmt_chk->execute();
        $count_chk = $stmt_chk->rowCount();

        if($count_chk==0)
        {
            $sql_insert = "INSERT INTO cron_jobs(EmailVerificationReminderRunOrNot,EmailVerificationReminderRunAt,ProfileCompletenessReminderRunOrNot,ProfileCompletenessRunAt,BirthdayReminderRunOrNot,BirthdayReminderRunAt,NewProfileReminderRunOrNot,NewProfileReminderFrom,NewProfileReminderRunAt,UnreadMessageReminderRunOrNot,UnreadMessageReminderRunAt,MembershipRenewalReminderRunOrNot,MembershipRenewalReminderRunAt,CreatedBy,CreatedOn) VALUES('$EmailVerificationReminderRunOrNot','$EmailVerificationReminderRunAt','$ProfileCompletenessReminderRunOrNot','$ProfileCompletenessRunAt','$BirthdayReminderRunOrNot','$BirthdayReminderRunAt','$NewProfileReminderRunOrNot','$NewProfileReminderFrom','$NewProfileReminderRunAt','$UnreadMessageReminderRunOrNot','$UnreadMessageReminderRunAt','$MembershipRenewalReminderRunOrNot','$MembershipRenewalReminderRunAt','$admin_id','$today_datetime')";
        }
        else
        {
            $sql_insert = "UPDATE cron_jobs SET EmailVerificationReminderRunOrNot='$EmailVerificationReminderRunOrNot',EmailVerificationReminderRunAt='$EmailVerificationReminderRunAt',ProfileCompletenessReminderRunOrNot='$ProfileCompletenessReminderRunOrNot',ProfileCompletenessRunAt='$ProfileCompletenessRunAt',BirthdayReminderRunOrNot='$BirthdayReminderRunOrNot',BirthdayReminderRunAt='$BirthdayReminderRunAt',NewProfileReminderRunOrNot='$NewProfileReminderRunOrNot',NewProfileReminderFrom='$NewProfileReminderFrom',NewProfileReminderRunAt='$NewProfileReminderRunAt',UnreadMessageReminderRunOrNot='$UnreadMessageReminderRunOrNot',UnreadMessageReminderRunAt='$UnreadMessageReminderRunAt',MembershipRenewalReminderRunOrNot='$MembershipRenewalReminderRunOrNot',MembershipRenewalReminderRunAt='$MembershipRenewalReminderRunAt',UpdatedBy='$admin_id',UpdatedOn='$today_datetime'";
        }

        //echo $sql_insert;exit;
        if($link->exec($sql_insert))
        {
            echo "success";
            exit;
        }
        else
        {
            echo "Something went wrong. Please try after some time";
            exit;
        }
    }

    /************   Add Custome meta tag Setting   *****************/
    if($task == "add_custom_meta_tag")
    {
        $meta_tag = $_POST['meta_tag'];
        
        $sql_check = "SELECT * FROM `custom_meta_tags` WHERE meta_tag='$meta_tag'";
        $stmt = $link->prepare($sql_check);
        $stmt->execute();
        $count = $stmt->rowCount();

        if($count>0)
        {
            echo "Meta tag already present in records.";
            exit;
        }
        else
        if($count==0)
        {
            $sql_insert = "INSERT INTO `custom_meta_tags`(meta_tag,created_by,created_at) VALUES('$meta_tag','$admin_id','$today')";
            
            if($link->exec($sql_insert))
            {
                echo "success";
                exit;
            }
            else
            {
                echo "Something went wrong. Try after some time.";
                exit;
            }
        }
    }

    /************   Delete Custome meta tag   *****************/
    if($task == "delete_custom_meta_tag")
    {
        $id = $_POST['id'];

        $sql_check = "SELECT * FROM `custom_meta_tags` WHERE id='$id'";
        $stmt = $link->prepare($sql_check);
        $stmt->execute();
        $count = $stmt->rowCount();

        if($count==0)
        {
            echo "Meta tag id not present in database. Try after some time.";
            exit;
        }
        else
        if($count>0)
        {
            $sql_delete = "DELETE FROM `custom_meta_tags` WHERE id='$id'";

            if($link->exec($sql_delete))
            {
                echo "success";
                exit;
            }
            else
            {
                echo "Something went wrong. Try after some time.";
                exit;
            }
        }
    }

    /************   Update meta tag Setting   *****************/
    if($task == "update_custom_meta_tag")
    {
        $id = $_POST['meta_tag_id'];
        $meta_tag = $_POST['meta_tag'];
        $meta_tag = str_replace('\\', '\\\\', $meta_tag);
        $meta_tag = str_replace("'", "\\'", $meta_tag);
        
        $sql_check = "SELECT * FROM `custom_meta_tags` WHERE meta_tag='$meta_tag'";
        $stmt = $link->prepare($sql_check);
        $stmt->execute();
        $count = $stmt->rowCount();

        if($count>0)
        {
            echo json_encode("Meta tag already present records.");
            exit;
        }
        else
        if($count==0)
        {
            $sql_update = "UPDATE `custom_meta_tags` SET meta_tag='$meta_tag',updated_by='$admin_id',updated_at='$today' WHERE id='$id'";
            
            if($link->exec($sql_update))
            {
                echo json_encode("success");
                exit;
            }
            else
            {
                echo json_encode("Something went wrong. Try after some time.");
                exit;
            }
        }
    }

    /************   Update maintainance mode Setting   *****************/
    if($task == "update-maintainance-mode")
    {
        $maintainance_mode = $_POST['maintainance_mode'];

        $sql_chk = "SELECT * FROM maintainance_mode_setting";
        $stmt = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();

        if($count==0)
        {
            $sql_update = "INSERT INTO maintainance_mode_setting(maintainance_mode,updated_by,updated_on) VALUES('$maintainance_mode','$admin_id','$today')";
        }
        else
        if($count>0)
        {
            $sql_update = "UPDATE maintainance_mode_setting SET maintainance_mode='$maintainance_mode',updated_by='$admin_id',updated_on='$today'";
        }

        if($link->exec($sql_update))
        {
            echo "success";
            exit;
        }
        else
        {
            echo "Something went wrong. Please try after some time";
            exit;
        }
    }

    /************   Update sitemap generation Setting   *****************/
    if($task == "update-sitemap-generation")
    {
        $sitemap_generation_allowed = $_POST['sitemap_generation_allowed'];
        $SitemapGenerationRunAt = $_POST['SitemapGenerationRunAt'];

        $sql_chk = "SELECT * FROM sitemap_generation";
        $stmt = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        if($count==0)
        {
            $sql_update = "INSERT INTO sitemap_generation(sitemap_generation_allowed,SitemapGenerationRunAt,created_by,created_on) VALUES('$sitemap_generation_allowed','$SitemapGenerationRunAt','$admin_id','$today')";
        }
        else
        if($count>0)
        {
            $sitemap_generation_allowed_db = $result['sitemap_generation_allowed'];
            $SitemapGenerationRunAt_db = $result['SitemapGenerationRunAt'];

            if(($sitemap_generation_allowed_db==$sitemap_generation_allowed) && ($SitemapGenerationRunAt_db==$SitemapGenerationRunAt))
            {
                echo json_encode("Same values as before");
                exit;
            }

            $sql_update = "UPDATE sitemap_generation SET sitemap_generation_allowed='$sitemap_generation_allowed',SitemapGenerationRunAt='$SitemapGenerationRunAt',updated_by='$admin_id',updated_on='$today'";
        }

        if($link->exec($sql_update))
        {
            echo json_encode("success");
            exit;
        }
        else
        {
            echo json_encode("Something went wrong. Please try after some time");
            exit;
        }
    }

    /************   Update robots.txt generation Setting   *****************/
    if($task == "update-robots-txt-generation")
    {
        $robots_txt_allowed = $_POST['robots_txt_allowed'];
        $robots_txt = $_POST['robots_txt'];

        $sql_chk = "SELECT * FROM robots_txt";
        $stmt = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $sql_update = "UPDATE robots_txt SET robots_txt_allowed='$robots_txt_allowed',updated_by='$admin_id',updated_on='$today'";
        
        if($robots_txt_allowed=='1')
        {
            $robots_txt_file_name = "../../robots.txt";
            
            $robots_txt_file = fopen($robots_txt_file_name, "w") or die("Unable to open file!");

            $robots_txt_file_data = "$robots_txt";
            fwrite($robots_txt_file, $robots_txt_file_data);
            fclose($robots_txt_file);
        }
        else
        if($robots_txt_allowed=='0')
        {
            $robots_txt_file_name = "../../robots.txt";
            
            if(file_exists($robots_txt_file_name))
            {
                unlink($robots_txt_file_name);
            }
        }

        if($link->exec($sql_update))
        {
            echo json_encode("success");
            exit;
        }
        else
        {
            echo json_encode("Something went wrong. Please try after some time");
            exit;
        }
    }

    /************   reset robots.txt default   *****************/
    if($task == "reset-robots-txt-default")
    {
        $sql_chk = "SELECT * FROM robots_txt";
        $stmt = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $default_content = $result['default_content'];

        $robots_txt_file_name = "../../robots.txt";
            
        $robots_txt_file = fopen($robots_txt_file_name, "w") or die("Unable to open file!");

        $robots_txt_file_data = "$default_content";
        fwrite($robots_txt_file, $robots_txt_file_data);
        fclose($robots_txt_file);

        $sql_update = "UPDATE robots_txt SET robots_txt_allowed='1',updated_by='$admin_id',updated_on='$today'";

        if($link->exec($sql_update))
        {
            echo json_encode("success");
            exit;
        }
        else
        {
            echo json_encode("Something went wrong. Please try after some time");
            exit;
        }
    }
?>