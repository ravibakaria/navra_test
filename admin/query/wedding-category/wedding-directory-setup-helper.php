<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
    
    $admin_id = $_SESSION['admin_id'];

	$task = quote_smart($_POST['task']);

	if($task=='update-wedding-directory-setup')
	{
        $wedding_directory_display_or_not = $_POST['wedding_directory_display_or_not'];
        $custom_content = $_POST['custom_content'];

        $sql_chk = "SELECT * FROM wedding_directory_settings";
        $stmt_chk = $link->prepare($sql_chk);
        $stmt_chk->execute();
        $count_chk = $stmt_chk->rowCount();

        if($count_chk==0)
        {
            $sql_insert = "INSERT INTO wedding_directory_settings(wedding_directory_display_or_not,custom_content,created_by,created_on) VALUES('$wedding_directory_display_or_not','$custom_content','$admin_id','$today')";
        }
        else
        if($count_chk>0)
        {
            $sql_insert = "UPDATE wedding_directory_settings SET wedding_directory_display_or_not='$wedding_directory_display_or_not',custom_content='$custom_content',updated_by='$admin_id',updated_on='$today'";
        }
        
        if($link->exec($sql_insert))
        {
            echo json_encode("success");
            exit;
        }
        else
        {
            echo json_encode("Something went wrong! Try after some time.");
            exit;
        }
    }
?>