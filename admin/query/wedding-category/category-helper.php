<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
    
    $admin_id = $_SESSION['admin_id'];

	$task = quote_smart($_POST['task']);

	if($task=='add-new-category')
	{
        $name = $_POST['name'];
        $slug = $_POST['slug'];
        $status = $_POST['status'];

        $sql_chk = "SELECT * FROM membercategory WHERE LOWER(name)=LOWER('$name')";
        $stmt_chk = $link->prepare($sql_chk);
        $stmt_chk->execute();
        $count_chk = $stmt_chk->rowCount();

        if($count_chk>0)
        {
        	echo json_encode("Category name already exists");
        	exit;
        }

        $sql_insert = "INSERT INTO membercategory(name,slug,status,created_by,created_on) VALUES('$name','$slug','$status','$admin_id','$today')";

        if($link->exec($sql_insert))
        {
        	echo json_encode("success");
        	exit;
        }
        else
        {
        	echo json_encode("Something went wrong! Try after some time.");
        	exit;
        }
    }

    if($task=='Update_Category_Attributes')
    {
    	$id = $_POST['id'];
    	$name = $_POST['name'];
        $slug = $_POST['slug'];
        $status = $_POST['status'];

        $sql_db_data = "SELECT * FROM membercategory WHERE id='$id'";
        $stmt_db_data = $link->prepare($sql_db_data);
        $stmt_db_data->execute();
        $count_db_data = $stmt_db_data->rowCount();

        if($count_db_data==0)
        {
        	echo json_encode("Invalid parameter value.");
        	exit;
        }

        $result_db_data = $stmt_db_data->fetch();

        $name_db = $result_db_data['name'];
        $slug_db = $result_db_data['slug'];
        $status_db = $result_db_data['status'];

        if($name_db==$name && $slug_db==$slug && $status_db==$status)
        {
        	echo json_encode("Same values as before!");
        	exit;
        }

        $sql_update = "UPDATE membercategory SET name='$name',slug='$slug',status='$status' WHERE id='$id'";

        if($link->exec($sql_update))
        {
        	echo json_encode("success");
        	exit;
        }
        else
        {
        	echo json_encode("Something went wrong! Try after some time.");
        	exit;
        }
    }

    if($task = 'Change_Category_status')
    {
    	$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `membercategory` SET `status`='$updated_status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong. Try after some time.");
			exit;
		}
    }
?>