<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //get master setup values
    include('../../../config/email/email_style.php');   //get master setup values
    include('../../../config/email/email_templates.php');   //get master setup values
    include('../../../config/email/email_process.php');
    
    $admin_id = $_SESSION['admin_id'];

    $WebSiteBasePath = getWebsiteBasePath();
    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }

	$task = quote_smart($_POST['task']);

	if($task=='add-new-vendor')
	{
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $phonecode = $_POST['phonecode'];
        $phone = $_POST['phone'];
        $company_name = $_POST['company_name'];
        $category_id = $_POST['category_id'];
        $city = $_POST['city'];
        $state = $_POST['state'];
        $country = $_POST['country'];
        $short_desc = $_POST['short_desc'];

        $sql_chk = "SELECT `email` FROM `vendors` WHERE email='$email' OR  phone='$phone'";
        $sql_chk = $link->prepare($sql_chk);
        $sql_chk->execute();
        $count=$sql_chk->rowCount();
        //echo "<script>alert(".$count.");</script>";
        if($count == '0')
        {
        	$Special_char_string = '[!@#$%^)*_(+=}{|:;,.<>}]'; 
			$pos1 = rand(0,(strlen($Special_char_string)-1));
	        $pass1 = $Special_char_string[$pos1];

	        $Capital_char_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
			$pos2 = rand(0,(strlen($Capital_char_string)-1));
	        $pass2 = $Capital_char_string[$pos2];

	        $Number_char_string = '1234567890'; 
			$pos3 = rand(0,(strlen($Number_char_string)-1));
	        $pass3 = $Number_char_string[$pos3];

	        $salt = "abchefghjkmnpqrstuvwxyz0123456789";
		    srand((double)microtime()*1000000);
	      	$i = 0;
	      	while ($i <= 5) 
	      	{
	            $num = rand() % 33;
	            $tmp = substr($salt, $num, 1);
	            $pass1 = $pass1 . $tmp;
	            $i++;
	      	}
		    
		    $new_pass = $pass2.$pass3.$pass1;    //
		    $tmp_password = md5($new_pass);     // random 

        	$sql_insert = "INSERT INTO `vendors`(firstname,lastname,email,phonecode,phone,company_name,category_id,city,state,country,short_desc,password,created_by_user,created_by,created_on,status) VALUES('$firstname','$lastname','$email','$phonecode','$phone','$company_name','$category_id','$city','$state','$country','$short_desc','$tmp_password','admin','$admin_id','$today','1')";
        	if($link->exec($sql_insert))
        	{
        		$login_link = $WebSiteBasePath.'/directory/login.php';
				$forgot_password_link = $WebSiteBasePath.'/directory/forgot-password.php';

				$SocialSharing = getSocialSharingLinks();   // social sharing links

				$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

	            $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

	            $NewRegistrationMessage = str_replace(array('$first_name','$site_name','$login_link','$email_address','$forgot_password_link','$signature'),array($firstname,$WebSiteTitle,$login_link,$email,$forgot_password_link,$GlobalEmailSignature),$NewRegistrationMessage);  //replace footer variables with value


	            $subject = $NewRegistrationSubject;
	            
	            $message  = '<!DOCTYPE html>';
	            $message .= '<html lang="en">
	                <head>
	                <meta charset="utf-8">
	                <meta name="viewport" content="width=device-width">
	                <title></title>
	                <style type="text/css">'.$EmailCSS.'</style>
	                </head>
	                <body style="margin: 0; padding: 0;">';
	            //echo $message;exit;
	            $message .= $EmailGlobalHeader;

	            $message .= $NewRegistrationMessage;                                               
	            $message .= $EmailGlobalFooter;
	            
	            $mailto = $email;
	            $mailtoname = $firstname;
				
				$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

				if($emailResponse == 'success')
				{
					echo json_encode("success");
				}
				else
				{
					echo json_encode("Error in sending login details to member.");
				}
        	}
        	else
        	{
        		echo json_encode("Something went wrong! Try after some time.");
        		exit;
        	}
        }
        else
        {
        	echo json_encode("Email id or mobile number already exists.");
        	exit;
        }
    }

    if($task=='Update_Vendor_Attributes')
	{
		$vuserid = $_POST['vuserid'];
		$firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $company_name = $_POST['company_name'];
        $category_id = $_POST['category_id'];
        $short_desc = $_POST['short_desc'];
        $country = $_POST['country'];
        $state = $_POST['state'];
        $city = $_POST['city'];
        $email = $_POST['email'];
        $phonecode = $_POST['phonecode'];
        $phone = $_POST['phone'];
        $status = $_POST['status'];

        $sql_update = "UPDATE vendors SET firstname='$firstname',lastname='$lastname',company_name='$company_name',category_id='$category_id',short_desc='$short_desc',country='$country',state='$state',city='$city',email='$email',phonecode='$phonecode',phone='$phone',status='$status',updated_by_user='admin',updated_by='$admin_id',updated_on='$today' WHERE vuserid='$vuserid'";

        if($link->exec($sql_update))
        {
        	echo json_encode("success");
        	exit;
        }
        else
        {
        	echo json_encode("Something went wrong! Try after some time");
        	exit;
        }
	}

	if($task=='Reset-Send-Password')
	{
		$vuserid = $_POST['vuserid'];

		$firstname = getVendorFirstName($vuserid);
		$email = getVendorEmail($vuserid);

		$Special_char_string = '[!@#$%^)*_(+=}{|:;,.<>}]'; 
        $pos1 = rand(0,(strlen($Special_char_string)-1));
        $pass1 = $Special_char_string[$pos1];

        $Capital_char_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $pos2 = rand(0,(strlen($Capital_char_string)-1));
        $pass2 = $Capital_char_string[$pos2];

        $Number_char_string = '1234567890'; 
        $pos3 = rand(0,(strlen($Number_char_string)-1));
        $pass3 = $Number_char_string[$pos3];

        $salt = "abchefghjkmnpqrstuvwxyz0123456789";
        srand((double)microtime()*1000000);

        $i = 0;
        while ($i <= 5) 
        {
            $num = rand() % 33;
            $tmp = substr($salt, $num, 1);
            $pass1 = $pass1 . $tmp;
            $i++;
        }
            
        $new_pass = $pass2.$pass3.$pass1;    //
        $tmp_password = md5($new_pass);     // random 

        $sql_update = "UPDATE `vendors` SET `password`='$tmp_password' WHERE `vuserid`='$vuserid'";

        $login_link = "<a href='$WebSiteBasePath/directory/login.php' target='_blank'>$WebSiteBasePath/directory/login.php</a>";

        if($link->exec($sql_update))
        {
        	$SocialSharing = getSocialSharingLinks();   // social sharing links
				                
            $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

            $ForgotPasswordSubject = str_replace('$site_name',$WebSiteTitle,$ForgotPasswordSubject);   //replace subject variables with value

            $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

            $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

            $ForgotPasswordMessage = str_replace(array('$first_name','$site_name','$login_link','$email_address','$new_password','$signature'),array($firstname,$WebSiteTitle,$login_link,$email,$new_pass,$GlobalEmailSignature),$ForgotPasswordMessage);  //replace footer variables with value


            $subject = $ForgotPasswordSubject;
            
            $message  = '<!DOCTYPE html>';
            $message .= '<html lang="en">
                <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width">
                <title></title>
                <style type="text/css">'.$EmailCSS.'</style>
                </head>
                <body style="margin: 0; padding: 0;">';
            //echo $message;exit;
            $message .= $EmailGlobalHeader;

            $message .= $ForgotPasswordMessage;                                               
            $message .= $GlobalEmailSignature;

            $message .= $EmailGlobalFooter;
            
            $mailto = $email;
            $mailtoname = $firstname;
            
            $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);
            if($emailResponse == 'success')
            {
                echo json_encode("success");
                exit;
            }
            else
            {
                echo json_encode("Error in sending login details to vendor.");
                exit;
            }
        }
        else
        {
        	echo json_encode("Something went wrong! Try after some time");
        	exit;
        }
	}
?>