<?php
    session_start();
    require_once '../../../config/config.php'; 
    include('../../../config/dbconnect.php');    //database connection
    include('../../../config/functions.php');   
    include('../../../config/setup-values.php');   //get master setup values
    include('../../../config/email/email_style.php');   //get master setup values
    include('../../../config/email/email_templates.php');   //get master setup values
    include('../../../config/email/email_process.php');
    
    $admin_id = $_SESSION['admin_id'];

    $today = date('Y-m-d H:i:s');
    $ip_address = $_SERVER['REMOTE_ADDR'];
    $basepath = $WebSiteBasePath.'/';

    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }


    $task = quote_smart($_POST['task']);

    /***************    Delete member personal photos    ****************/
    if($task == 'Update-vendor-Photo-Status')     // approve-reject member document
    {
        $vuserid = $_POST['vuserid'];
        $photo_id = $_POST['photo_id'];
        $photo_reject_reason = $_POST['reason'];
        
        $sql_get_document_info = "SELECT * FROM vendor_gallery  WHERE vuserid='$vuserid' AND id='$photo_id'";
        $stmt = $link->prepare($sql_get_document_info);
        $stmt->execute();
        $result = $stmt->fetch();

        $first_name = getVendorFirstName($vuserid);
        $email = getVendorEmail($vuserid);
        $Photo = $result['photo'];
        $photo_upload_on = $result['created_on'];

        $photo_path = $WebSiteBasePath.'/directory/uploads/'.$vuserid.'/photo/'.$Photo;
        
        $delete_photo_path = '../../../directory/uploads/'.$vuserid.'/photo/'.$Photo;

        $SocialSharing = getSocialSharingLinks();   // social sharing links
        
        $PhotoRejectSubject = str_replace('$site_name',$WebSiteTitle,$PhotoRejectSubject);   //replace subject variables with value

        $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

        $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

        $PhotoRejectMessage = str_replace(array('$first_name','$photo_reject_reason','$photo_upload_on','$signature'),array($first_name,$photo_reject_reason,$photo_upload_on,$GlobalEmailSignature),$PhotoRejectMessage);  //replace footer variables with value

        $subject = $PhotoRejectSubject;
            
        $message  = '<!DOCTYPE html>';
        $message .= '<html lang="en">
            <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <title></title>
            <style type="text/css">'.$EmailCSS.'</style>
            </head>
            <body style="margin: 0; padding: 0;">';
        //echo $message;exit;
        $message .= $EmailGlobalHeader;

        $message .= $PhotoRejectMessage;

        $message .= $EmailGlobalFooter;
        
        $mailto = $email;
        $mailtoname = $first_name;

        $emailResponse = EmailProcessAttachment($subject,$message,$mailto,$mailtoname,$photo_path,$Photo);

        $sql_vendor_email_log = "INSERT INTO vendor_email_logs(vuserid,task,activity,sent_On) VALUES('$vuserid','admin reject & delete','work photo','$today')";

        if($emailResponse == 'success')
        {
            $link->exec($sql_vendor_email_log);
            $sql_update = "DELETE FROM vendor_gallery WHERE vuserid='$vuserid' AND id='$photo_id'";
            $sql_log = "INSERT INTO vendor_photo_delete_admin_logs(vuserid,photo_type,photo_reject_reason,deleted_by,deleted_on,ip_address) VALUES('$vuserid','work photo','$photo_reject_reason','$admin_id','$today','$ip_address')";

            unlink($delete_photo_path);
        }
        else
        {
                echo json_encode("Error in sending rejection email to member.");
                exit;
        }
    
        if($link->exec($sql_update) && $link->exec($sql_log))
        {
            echo json_encode('success');
            exit;
        }
    }

    /***************    Delete member profile photos    ****************/
    if($task == 'delete-profile-photo')     // approve-reject member document
    {
        $vuserid = $_POST['vuserid'];
        $photo_reject_reason = $_POST['reason_delete_profile_pic'];

        $sql_get_document_info = "SELECT * FROM vendor_profile_pic  WHERE vuserid='$vuserid'";
        $stmt = $link->prepare($sql_get_document_info);
        $stmt->execute();
        $result = $stmt->fetch();

        $first_name = getVendorFirstName($vuserid);
        $email = getVendorEmail($vuserid);
        $Photo = $result['profile_pic'];
        $photo_upload_on = $result['uploaded_on'];

        $photo_path = $WebSiteBasePath.'/directory/uploads/'.$vuserid.'/profile/'.$Photo;
        
        $delete_photo_path = '../../../directory/uploads/'.$vuserid.'/profile/'.$Photo;

        $SocialSharing = getSocialSharingLinks();   // social sharing links
        
        $PhotoRejectSubject = str_replace('$site_name',$WebSiteTitle,$PhotoRejectSubject);   //replace subject variables with value

        $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

        $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

        $PhotoRejectMessage = str_replace(array('$first_name','$photo_reject_reason','$photo_upload_on','$signature'),array($first_name,$photo_reject_reason,$photo_upload_on,$GlobalEmailSignature),$PhotoRejectMessage);  //replace footer variables with value

        $subject = $PhotoRejectSubject." (Profile Photo)";
            
        $message  = '<!DOCTYPE html>';
        $message .= '<html lang="en">
            <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <title></title>
            <style type="text/css">'.$EmailCSS.'</style>
            </head>
            <body style="margin: 0; padding: 0;">';
        //echo $message;exit;
        $message .= $EmailGlobalHeader;

        $message .= $PhotoRejectMessage;

        $message .= $EmailGlobalFooter;
        
        $mailto = $email;
        $mailtoname = $first_name;

        $emailResponse = EmailProcessAttachment($subject,$message,$mailto,$mailtoname,$photo_path,$Photo);

        $sql_vendor_email_log = "INSERT INTO vendor_email_logs(vuserid,task,activity,sent_On) VALUES('$vuserid','admin reject & delete','profile photo','$today')";

        //$emailResponse = 'success';
        if($emailResponse == 'success')
        {
            unlink($delete_photo_path);
            $link->exec($sql_vendor_email_log);
            $sql_update = "DELETE FROM vendor_profile_pic WHERE vuserid='$vuserid' ";
            $sql_log = "INSERT INTO vendor_photo_delete_admin_logs(vuserid,photo_type,photo_reject_reason,deleted_by,deleted_on,ip_address) VALUES('$vuserid','profile','$photo_reject_reason','$admin_id','$today','$ip_address')";
        }
        else
        {
                echo json_encode("Error in sending rejection email to member.");
        }
        
        if($link->exec($sql_update) && $link->exec($sql_log))
        {
            echo json_encode('success');
            exit;
        }
    }

    /***************    Approve work photos    ****************/
    if($task == 'Approve-vendor-Photo-Status')     
    {
        $vuserid = $_POST['vuserid'];
        $photo_id = $_POST['photo_id'];
        
        $sql_update = "UPDATE vendor_gallery SET status='1' WHERE vuserid='$vuserid' AND id='$photo_id'";

        if($link->exec($sql_update))
        {
            echo json_encode("success");
            exit;
        }
        else
        {
            echo json_encode("Something went wrong! Try after some time.");
            exit;
        }
    }
?>