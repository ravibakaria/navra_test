<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];
 
	$task = quote_smart($_POST['task']);

	if($task == 'Stripe_payment_gateway')
	{

		$sp_is_enable = $_POST['sp_is_enable'];
		$sp_display_name = $_POST['sp_display_name'];
		$sp_activated_mode = $_POST['sp_activated_mode'];
		$sp_test_publishable_key = $_POST['sp_test_publishable_key'];
		$sp_test_secret_key = $_POST['sp_test_secret_key'];
		$sp_prod_publishable_key = $_POST["sp_prod_publishable_key"];
		$sp_prod_secret_key = $_POST['sp_prod_secret_key'];

		$sql_chk = "SELECT * FROM payment_stripe";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count==0)
		{
			$result = $stmt->fetch();

			$sp_is_enable_db = $result['sp_is_enable'];
			$sp_display_name_db = $result['sp_display_name'];
			$sp_activated_mode_db = $result['sp_activated_mode'];
			$sp_test_publishable_key_db = $result['sp_test_publishable_key'];
			$sp_test_secret_key_db = $result['sp_test_secret_key'];
			$sp_prod_publishable_key_db = $result['sp_prod_publishable_key'];
			$sp_prod_secret_key_db = $result['sp_prod_secret_key'];

			if($sp_is_enable_db==$sp_is_enable && $sp_display_name_db==$sp_display_name &&  $sp_activated_mode_db==$sp_activated_mode && $sp_test_publishable_key_db ==$sp_test_publishable_key && $sp_test_secret_key_db ==$sp_test_secret_key && $sp_prod_publishable_key_db==$sp_prod_publishable_key && $sp_prod_secret_key_db==$sp_prod_secret_key)
			{
				echo json_encode("Same values as before.");
				exit;
			}
			else
			{
				$sql_update = "INSERT INTO payment_stripe(sp_is_enable,sp_display_name,sp_activated_mode,sp_test_publishable_key,sp_test_secret_key,sp_prod_publishable_key,sp_prod_secret_key,sp_created_by,sp_created_on) VALUES('$sp_is_enable','$sp_display_name','$sp_activated_mode','$sp_test_publishable_key','$sp_test_secret_key','$sp_prod_publishable_key','$sp_prod_secret_key','$admin_id','$today')";
			}
		}
		else
		if($count>0)
		{
			$sql_update = "UPDATE payment_stripe SET sp_is_enable='$sp_is_enable',sp_display_name='$sp_display_name',sp_activated_mode='$sp_activated_mode',sp_test_publishable_key='$sp_test_publishable_key',sp_test_secret_key='$sp_test_secret_key',sp_prod_publishable_key='$sp_prod_publishable_key',sp_prod_secret_key='$sp_prod_secret_key',sp_updated_by='$admin_id',sp_updated_on='$today'";
		}

		if($link->exec($sql_update))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Please try after some time.");
		}
	}
?>