<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];
 
	$task = quote_smart($_POST['task']);

	if($task == 'CCAvenue_payment_gateway')
	{

		$cc_is_enable = $_POST['cc_is_enable'];
		$cc_display_name = $_POST['cc_display_name'];
		$cc_merchant_id = $_POST['cc_merchant_id'];
		$cc_access_code = $_POST['cc_access_code'];
		$cc_working_key = $_POST['cc_working_key'];

		$sql_chk = "SELECT * FROM payment_ccavenue";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count==0)
		{
			$result = $stmt->fetch();

			$cc_is_enable_db = $result['cc_is_enable'];
			$cc_display_name_db = $result['cc_display_name'];
			$cc_merchant_id_db = $result['cc_merchant_id'];
			$cc_access_code_db = $result['cc_access_code'];
			$cc_working_key_db = $result['cc_working_key'];

			if($cc_is_enable_db==$cc_is_enable && $cc_display_name_db==$cc_display_name && $cc_merchant_id_db==$cc_merchant_id && $cc_access_code_db==$cc_access_code && $cc_working_key_db ==$cc_working_key)
			{
				echo json_encode("Same values as before.");
				exit;
			}
			else
			{
				$sql_update = "INSERT INTO payment_ccavenue(cc_is_enable,cc_display_name,cc_merchant_id,cc_access_code,cc_working_key,cc_created_by,cc_created_on) VALUES('$cc_is_enable','$cc_display_name','$cc_merchant_id','$cc_access_code','$cc_working_key','$admin_id','$today')";
			}
		}
		else
		if($count>0)
		{
			$sql_update = "UPDATE payment_ccavenue SET cc_is_enable='$cc_is_enable',cc_display_name='$cc_display_name',cc_merchant_id='$cc_merchant_id',cc_access_code='$cc_access_code',cc_working_key='$cc_working_key',cc_updated_by='$admin_id',cc_updated_on='$today'";
		}

		if($link->exec($sql_update))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Please try after some time.");
		}
	}
?>