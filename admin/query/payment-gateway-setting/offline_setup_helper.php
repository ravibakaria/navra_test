<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];

    $task = quote_smart($_POST['task']);

	if($task == 'offline_payment_gateway')
	{
		$off_is_enable = $_POST['off_is_enable'];
		$off_display_name = $_POST['off_display_name'];
		$off_pay_to_text = $_POST['off_pay_to_text'];

		$sql_chk = "SELECT * FROM payment_offline";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count==0)
		{
			$result = $stmt->fetch();

			$off_is_enable_db = $result['off_is_enable'];
			$off_display_name_db = $result['off_display_name'];
			$off_pay_to_text_db = $result['off_pay_to_text'];

			if($off_is_enable_db==$off_is_enable && $off_display_name_db==$off_display_name && $off_pay_to_text_db==$off_pay_to_text)
			{
				echo json_encode("Setting already updated.");
				exit;
			}
			else
			{
				$sql_update = "INSERT INTO payment_offline(off_is_enable,off_display_name,off_pay_to_text,off_created_by,off_created_on) VALUES('$off_is_enable','$off_display_name','$off_pay_to_text','$admin_id','$today')";
			}
		}
		else
		if($count>0)
		{
			$sql_update = "UPDATE payment_offline SET off_is_enable='$off_is_enable',off_display_name='$off_display_name',off_pay_to_text='$off_pay_to_text',off_updated_by='$admin_id',off_updated_on='$today'";
		}

		if($link->exec($sql_update))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Please try after some time.");
		}
	}
?>