<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];
 
	$task = quote_smart($_POST['task']);

	if($task == 'razorpay_payment_gateway')
	{

		$rp_is_enable = $_POST['rp_is_enable'];
		$rp_display_name = $_POST['rp_display_name'];
		$rp_activated_mode = $_POST['rp_activated_mode'];
		$rp_test_key_id = $_POST['rp_test_key_id'];
		$rp_test_key_secret = $_POST['rp_test_key_secret'];
		$rp_prod_key_id = $_POST['rp_prod_key_id'];
		$rp_prod_key_secret = $_POST['rp_prod_key_secret'];

		$sql_chk = "SELECT * FROM payment_razorpay";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count==0)
		{
			$result = $stmt->fetch();

			$rp_is_enable_db = $result['rp_is_enable'];
			$rp_display_name_db = $result['rp_display_name'];
			$rp_activated_mode_db = $result['rp_activated_mode'];
			$rp_test_key_id_db = $result['rp_test_key_id'];
			$rp_test_key_secret_db = $result['rp_test_key_secret'];
			$rp_prod_key_id_db = $result['rp_prod_key_id'];
			$rp_prod_key_secret_db = $result['rp_prod_key_secret'];

			if($rp_is_enable_db==$rp_is_enable && $rp_display_name_db==$rp_display_name && $rp_activated_mode_db==$rp_activated_mode && $rp_test_key_id_db==$rp_test_key_id && $rp_test_key_secret_db ==$rp_test_key_secret && $rp_prod_key_id_db ==$rp_prod_key_id  && $rp_prod_key_secret_db==$rp_prod_key_secret)
			{
				echo json_encode("Same values as before.");
				exit;
			}
			else
			{
				$sql_update = "INSERT INTO payment_razorpay(rp_is_enable,rp_display_name,rp_activated_mode,rp_test_key_id,rp_test_key_secret,rp_prod_key_id,rp_prod_key_secret,rp_created_by,rp_created_on) VALUES('$rp_is_enable','$rp_display_name','$rp_activated_mode','$rp_test_key_id','$rp_test_key_secret','$rp_prod_key_id','$rp_prod_key_secret','$admin_id','$today')";
			}
		}
		else
		if($count>0)
		{
			$sql_update = "UPDATE payment_razorpay SET rp_is_enable='$rp_is_enable',rp_display_name='$rp_display_name',rp_activated_mode='$rp_activated_mode',rp_test_key_id='$rp_test_key_id',rp_test_key_secret='$rp_test_key_secret',rp_prod_key_id='$rp_prod_key_id',rp_prod_key_secret='$rp_prod_key_secret',rp_updated_by='$admin_id',rp_updated_on='$today'";
		}

		if($link->exec($sql_update))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Please try after some time.");
			exit;
		}
	}
?>