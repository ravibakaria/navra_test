<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];
 
	$task = quote_smart($_POST['task']);

	if($task == 'paytm_payment_gateway')
	{

		$pt_is_enable = $_POST['pt_is_enable'];
		$pt_display_name = $_POST['pt_display_name'];
		$pt_activated_mode = $_POST['pt_activated_mode'];
		$pt_test_merchant_id = $_POST['pt_test_merchant_id'];
		$pt_test_merchant_key = $_POST['pt_test_merchant_key'];
		$pt_test_website_name = $_POST['pt_test_website_name'];
		$pt_test_industry_type = $_POST['pt_test_industry_type'];
		$pt_prod_merchant_id = $_POST['pt_prod_merchant_id'];
		$pt_prod_merchant_key = $_POST["pt_prod_merchant_key"];
		$pt_prod_website_name = $_POST['pt_prod_website_name'];
		$pt_prod_industry_type = $_POST['pt_prod_industry_type'];

		$sql_chk = "SELECT * FROM payment_paytm";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count==0)
		{
			$result = $stmt->fetch();

			$pt_is_enable_db = $result['pt_is_enable'];
			$pt_display_name_db = $result['pt_display_name'];
			$pt_activated_mode_db = $result['pt_activated_mode'];
			$pt_test_merchant_id_db = $result['pt_test_merchant_id'];
			$pt_test_merchant_key_db = $result['pt_test_merchant_key'];
			$pt_test_website_name_db = $result['pt_test_website_name'];
			$pt_test_industry_type_db = $result['pt_test_industry_type'];
			$pt_prod_merchant_id_db = $result['pt_prod_merchant_id'];
			$pt_prod_merchant_key_db = $result['pt_prod_merchant_key'];
			$pt_prod_website_name_db = $result['pt_prod_website_name'];
			$pt_prod_industry_type_db = $result['pt_prod_industry_type'];

			if($pt_is_enable_db==$pt_is_enable && $pt_display_name_db==$pt_display_name && $pt_activated_mode_db==$pt_activated_mode && $pt_test_merchant_id_db==$pt_test_merchant_id && $pt_test_merchant_key_db ==$pt_test_merchant_key && $pt_test_website_name_db ==$pt_test_website_name && $pt_test_industry_type_db==$pt_test_industry_type && $pt_prod_merchant_id_db==$pt_prod_merchant_id && $pt_prod_merchant_key_db==$pt_prod_merchant_key && $pt_prod_website_name_db==$pt_prod_website_name && $pt_prod_industry_type_db==$pt_prod_industry_type)
			{
				echo json_encode("Setting already updated.");
				exit;
			}
			else
			{
				$sql_update = "INSERT INTO payment_paytm(pt_is_enable,pt_display_name,pt_activated_mode,pt_test_merchant_id,pt_test_merchant_key,pt_test_website_name,pt_test_industry_type,pt_prod_merchant_id,pt_prod_merchant_key,pt_prod_website_name,pt_prod_industry_type,pt_created_by,pt_created_on) VALUES('$pt_is_enable','$pt_display_name','$pt_activated_mode','$pt_test_merchant_id','$pt_test_merchant_key','$pt_test_website_name','$pt_test_industry_type','$pt_prod_merchant_id','$pt_prod_merchant_key','$pt_prod_website_name','$pt_prod_industry_type','$admin_id','$today')";
			}
		}
		else
		if($count>0)
		{
			$sql_update = "UPDATE payment_paytm SET pt_is_enable='$pt_is_enable',pt_display_name='$pt_display_name',pt_activated_mode='$pt_activated_mode',pt_test_merchant_id='$pt_test_merchant_id',pt_test_merchant_key='$pt_test_merchant_key',pt_test_website_name='$pt_test_website_name',pt_test_industry_type='$pt_test_industry_type',pt_prod_merchant_id='$pt_prod_merchant_id',pt_prod_merchant_key='$pt_prod_merchant_key',pt_prod_website_name='$pt_prod_website_name',pt_prod_industry_type='$pt_prod_industry_type',pt_updated_by='$admin_id',pt_updated_on='$today'";
		}

		if($link->exec($sql_update))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Please try after some time.");
		}
	}
?>