<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];

    $task = quote_smart($_POST['task']);

	if($task == 'bank_transfer_payment_gateway')
	{
		$bt_is_enable = $_POST['bt_is_enable'];
		$bt_display_name = $_POST['bt_display_name'];
		$bt_pay_to_text = $_POST['bt_pay_to_text'];

		$sql_chk = "SELECT * FROM payment_bank_transfer";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count==0)
		{
			$result = $stmt->fetch();

			$bt_is_enable_db = $result['bt_is_enable'];
			$bt_display_name_db = $result['bt_display_name'];
			$bt_pay_to_text_db = $result['bt_pay_to_text'];

			if($bt_is_enable_db==$bt_is_enable && $bt_display_name_db==$bt_display_name && $bt_pay_to_text_db==$bt_pay_to_text)
			{
				echo json_encode("Setting already updated.");
				exit;
			}
			else
			{
				$sql_update = "INSERT INTO payment_bank_transfer(bt_is_enable,bt_display_name,bt_pay_to_text,off_created_by,off_created_on) VALUES('$bt_is_enable','$bt_display_name','$bt_pay_to_text','$admin_id','$today')";
			}
		}
		else
		if($count>0)
		{
			$sql_update = "UPDATE payment_bank_transfer SET bt_is_enable='$bt_is_enable',bt_display_name='$bt_display_name',bt_pay_to_text='$bt_pay_to_text',off_updated_by='$admin_id',off_updated_on='$today'";
		}

		if($link->exec($sql_update))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Please try after some time.");
		}
	}
?>