<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];
 
	$task = quote_smart($_POST['task']);

	if($task == 'paypal_payment_gateway')
	{

		$pp_is_enable = $_POST['pp_is_enable'];
		$pp_display_name = $_POST['pp_display_name'];
		$pp_activated_mode = $_POST['pp_activated_mode'];
		$pp_test_client_id = $_POST['pp_test_client_id'];
		$pp_test_secret = $_POST['pp_test_secret'];
		$pp_prod_client_id = $_POST['pp_prod_client_id'];
		$pp_prod_secret = $_POST["pp_prod_secret"];

		$sql_chk = "SELECT * FROM payment_paypal";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count==0)
		{
			$result = $stmt->fetch();

			$pp_is_enable_db = $result['pp_is_enable'];
			$pp_display_name_db = $result['pp_display_name'];
			$pp_activated_mode_db = $result['pp_activated_mode'];
			$pp_test_client_id_db = $result['pp_test_client_id'];
			$pp_test_secret_db = $result['pp_test_secret'];
			$pp_prod_client_id_db = $result['pp_prod_client_id'];
			$pp_prod_secret_db = $result['pp_prod_secret'];

			if($pp_is_enable_db==$pp_is_enable && $pp_display_name_db==$pp_display_name && $pp_activated_mode_db==$pp_activated_mode && $pp_test_client_id_db==$pp_test_client_id && $pp_test_secret_db ==$pp_test_secret && $pp_prod_client_id_db==$pp_prod_client_id && $pp_prod_secret_db==$pp_prod_secret)
			{
				echo json_encode("Same values as before.");
				exit;
			}
			else
			{
				$sql_update = "INSERT INTO payment_paypal(pp_is_enable,pp_display_name,pp_activated_mode,pp_test_client_id,pp_test_secret,pp_prod_client_id,pp_prod_secret,pp_created_by,pp_created_on) VALUES('$pp_is_enable','$pp_display_name','$pp_activated_mode','$pp_test_client_id','$pp_test_secret','$pp_prod_client_id','$pp_prod_secret','$admin_id','$today')";
			}
		}
		else
		if($count>0)
		{
			$sql_update = "UPDATE payment_paypal SET pp_is_enable='$pp_is_enable',pp_display_name='$pp_display_name',pp_activated_mode='$pp_activated_mode',pp_test_client_id='$pp_test_client_id',pp_test_secret='$pp_test_secret',pp_prod_client_id='$pp_prod_client_id',pp_prod_secret='$pp_prod_secret',pp_updated_by='$admin_id',pp_updated_on='$today'";
		}

		if($link->exec($sql_update))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Please try after some time.");
		}
	}
?>