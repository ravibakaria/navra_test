<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];
 
	$task = quote_smart($_POST['task']);

	if($task == '2checkout_payment_gateway')
	{

		$co_is_enable = $_POST['co_is_enable'];
		$co_display_name = $_POST['co_display_name'];
		$co_activated_mode = $_POST['co_activated_mode'];
		$co_test_account_number = $_POST['co_test_account_number'];
		$co_test_publishable_key = $_POST['co_test_publishable_key'];
		$co_test_private_key = $_POST['co_test_private_key'];
		$co_prod_account_number = $_POST['co_prod_account_number'];
		$co_prod_publishable_key = $_POST["co_prod_publishable_key"];
		$co_prod_private_key = $_POST['co_prod_private_key'];

		$sql_chk = "SELECT * FROM payment_2checkout";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count==0)
		{
			$result = $stmt->fetch();

			$co_is_enable_db = $result['co_is_enable'];
			$co_display_name_db = $result['co_display_name'];
			$co_activated_mode_db = $result['co_activated_mode'];
			$co_test_account_number_db = $result['co_test_account_number'];
			$co_test_publishable_key_db = $result['co_test_publishable_key'];
			$co_test_private_key_db = $result['co_test_private_key'];
			$co_prod_account_number_db = $result['co_prod_account_number'];
			$co_prod_publishable_key_db = $result['co_prod_publishable_key'];
			$co_prod_private_key_db = $result['co_prod_private_key'];

			if($co_is_enable_db==$co_is_enable && $co_display_name_db==$co_display_name &&  $co_activated_mode_db==$co_activated_mode && $co_test_account_number_db==$co_test_account_number && $co_test_publishable_key_db ==$co_test_publishable_key && $co_test_private_key_db ==$co_test_private_key && $co_prod_account_number_db==$co_prod_account_number && $co_prod_publishable_key_db==$co_prod_publishable_key && $co_prod_private_key_db==$co_prod_private_key)
			{
				echo json_encode("Same values as before.");
				exit;
			}
			else
			{
				$sql_update = "INSERT INTO payment_2checkout(co_is_enable,co_display_name,co_activated_mode,co_test_account_number,co_test_publishable_key,co_test_private_key,co_prod_account_number,co_prod_publishable_key,co_prod_private_key,co_created_by,co_created_on) VALUES('$co_is_enable','$co_display_name','$co_activated_mode','$co_test_account_number','$co_test_publishable_key','$co_test_private_key','$co_prod_account_number','$co_prod_publishable_key','$co_prod_private_key','$admin_id','$today')";
			}
		}
		else
		if($count>0)
		{
			$sql_update = "UPDATE payment_2checkout SET co_is_enable='$co_is_enable',co_display_name='$co_display_name',co_activated_mode='$co_activated_mode',co_test_account_number='$co_test_account_number',co_test_publishable_key='$co_test_publishable_key',co_test_private_key='$co_test_private_key',co_prod_account_number='$co_prod_account_number',co_prod_publishable_key='$co_prod_publishable_key',co_prod_private_key='$co_prod_private_key',co_updated_by='$admin_id',co_updated_on='$today'";
		}

		if($link->exec($sql_update))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Please try after some time.");
		}
	}
?>