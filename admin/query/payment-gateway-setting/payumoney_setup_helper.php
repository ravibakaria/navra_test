<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];
 
	$task = quote_smart($_POST['task']);

	if($task == 'payumoney_payment_gateway')
	{

		$pm_is_enable = $_POST['pm_is_enable'];
		$pm_display_name = $_POST['pm_display_name'];
		$pm_activated_mode = $_POST['pm_activated_mode'];
		$pm_test_merchant_key = $_POST['pm_test_merchant_key'];
		$pm_test_merchant_salt = $_POST['pm_test_merchant_salt'];
		$pm_test_merchant_auth_header = $_POST['pm_test_merchant_auth_header'];
		$pm_prod_merchant_key = $_POST['pm_prod_merchant_key'];
		$pm_prod_merchant_salt = $_POST["pm_prod_merchant_salt"];
		$pm_prod_merchant_auth_header = $_POST['pm_prod_merchant_auth_header'];

		$sql_chk = "SELECT * FROM payment_payumoney";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count==0)
		{
			$result = $stmt->fetch();

			$pm_is_enable_db = $result['pm_is_enable'];
			$pm_display_name_db = $result['pm_display_name'];
			$pm_activated_mode_db = $result['pm_activated_mode'];
			$pm_test_merchant_key_db = $result['pm_test_merchant_key'];
			$pm_test_merchant_salt_db = $result['pm_test_merchant_salt'];
			$pm_test_merchant_auth_header_db = $result['pm_test_merchant_auth_header'];
			$pm_prod_merchant_key_db = $result['pm_prod_merchant_key'];
			$pm_prod_merchant_salt_db = $result['pm_prod_merchant_salt'];
			$pm_prod_merchant_auth_header_db = $result['pm_prod_merchant_auth_header'];

			if($pm_is_enable_db==$pm_is_enable && $pm_display_name_db==$pm_display_name && $pm_activated_mode_db==$pm_activated_mode && $pm_test_merchant_key_db==$pm_test_merchant_key && $pm_test_merchant_salt_db ==$pm_test_merchant_salt && $pm_test_merchant_auth_header_db ==$pm_test_merchant_auth_header  && $pm_prod_merchant_key_db==$pm_prod_merchant_key && $pm_prod_merchant_salt_db==$pm_prod_merchant_salt && $pm_prod_merchant_auth_header_db==$pm_prod_merchant_auth_header)
			{
				echo json_encode("Setting already updated.");
				exit;
			}
			else
			{
				$sql_update = "INSERT INTO payment_payumoney(pm_is_enable,pm_display_name,pm_activated_mode,pm_test_merchant_key,pm_test_merchant_salt,pm_test_merchant_auth_header,pm_prod_merchant_key,pm_prod_merchant_salt,pm_prod_merchant_auth_header,pm_created_by,pm_created_on) VALUES('$pm_is_enable','$pm_display_name','$pm_activated_mode','$pm_test_merchant_key','$pm_test_merchant_salt','$pm_test_merchant_auth_header','$pm_prod_merchant_key','$pm_prod_merchant_salt','$pm_prod_merchant_auth_header','$admin_id','$today')";
			}
		}
		else
		if($count>0)
		{
			$sql_update = "UPDATE payment_payumoney SET pm_is_enable='$pm_is_enable',pm_display_name='$pm_display_name',pm_activated_mode='$pm_activated_mode',pm_test_merchant_key='$pm_test_merchant_key',pm_test_merchant_salt='$pm_test_merchant_salt',pm_test_merchant_auth_header='$pm_test_merchant_auth_header',pm_prod_merchant_key='$pm_prod_merchant_key',pm_prod_merchant_salt='$pm_prod_merchant_salt',pm_prod_merchant_auth_header='$pm_prod_merchant_auth_header',pm_updated_by='$admin_id',pm_updated_on='$today'";
		}

		if($link->exec($sql_update))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Please try after some time.");
		}
	}
?>