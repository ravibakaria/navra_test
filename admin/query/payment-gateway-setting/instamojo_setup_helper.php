<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];

    $task = quote_smart($_POST['task']);

	if($task == 'instamojo_payment_gateway')
	{
		$im_is_enable = $_POST['im_is_enable'];
		$im_display_name = $_POST['im_display_name'];
		$im_activated_mode = $_POST['im_activated_mode'];
		$im_test_api_key = $_POST['im_test_api_key'];
		$im_test_auth_token = $_POST['im_test_auth_token'];
		$im_test_salt = $_POST['im_test_salt'];
		$im_test_email = $_POST['im_test_email'];
		$im_prod_api_key = $_POST['im_prod_api_key'];
		$im_prod_auth_token = $_POST["im_prod_auth_token"];
		$im_prod_salt = $_POST['im_prod_salt'];
		$im_prod_email = $_POST['im_prod_email'];

		$sql_chk = "SELECT * FROM payment_instamojo";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count==0)
		{
			$result = $stmt->fetch();

			$im_is_enable_db = $result['im_is_enable'];
			$im_display_name_db = $result['im_display_name'];
			$im_activated_mode_db = $result['im_activated_mode'];
			$im_test_api_key_db = $result['im_test_api_key'];
			$im_test_auth_token_db = $result['im_test_auth_token'];
			$im_test_salt_db = $result['im_test_salt'];
			$im_test_email_db = $result['im_test_email'];
			$im_prod_api_key_db = $result['im_prod_api_key'];
			$im_prod_auth_token_db = $result['im_prod_auth_token'];
			$im_prod_salt_db = $result['im_prod_salt'];
			$im_prod_email_db = $result['im_prod_email'];

			if($im_is_enable_db==$im_is_enable && $im_display_name_db==$im_display_name && $im_activated_mode_db==$im_activated_mode && $im_test_api_key_db==$im_test_api_key && $im_test_auth_token_db==$im_test_auth_token && $im_test_salt_db==$im_test_salt && $im_test_email_db==$im_test_email && $im_prod_api_key_db==$im_prod_api_key && $im_prod_auth_token_db==$im_prod_auth_token && $im_prod_salt_db==$im_prod_salt && $im_prod_email_db==$im_prod_email)
			{
				echo json_encode("Setting already updated.");
				exit;
			}
			else
			{
				$sql_update = "INSERT INTO payment_instamojo(im_is_enable,im_display_name,im_activated_mode,im_test_api_key,im_test_auth_token,im_test_salt,im_test_email,im_prod_api_key,im_prod_auth_token,im_prod_salt,im_prod_email,im_created_by,im_created_on) VALUES('$im_is_enable','$im_display_name','$im_activated_mode','$im_test_api_key','$im_test_auth_token','$im_test_salt','$im_test_email','$im_prod_api_key','$im_prod_auth_token','$im_prod_salt','$im_prod_email','$admin_id','$today')";
			}
		}
		else
		if($count>0)
		{
			$sql_update = "UPDATE payment_instamojo SET im_is_enable='$im_is_enable',im_display_name='$im_display_name',im_activated_mode='$im_activated_mode',im_test_api_key='$im_test_api_key',im_test_auth_token='$im_test_auth_token',im_test_salt='$im_test_salt',im_test_email='$im_test_email',im_prod_api_key='$im_prod_api_key',im_prod_auth_token='$im_prod_auth_token',im_prod_salt='$im_prod_salt',im_prod_email='$im_prod_email',im_updated_by='$admin_id',im_updated_on='$today'";
		}

		if($link->exec($sql_update))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong! Please try after some time.");
		}
	}
?>