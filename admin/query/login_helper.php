<?php
	session_start();
	require_once '../../config/config.php'; 
	include('../../config/dbconnect.php');    //database connection
	include('../../config/functions.php');   //strip query string
	include "../../config/setup-values.php";

    $today_datetime = date('Y-m-d H:i:s');
    
    $recaptchaAllowed = "0";

    $basepath = $WebSiteBasePath.'/';
    $task = quote_smart($_POST['task']);

	if($task == "admin_login")
	{
        $email = quote_smart($_POST['email']);
        $password = md5($_POST['password']);
        $robot = quote_smart($_POST['robot']);

        if($robot!='' || $robot!=null)
        {
            echo "You are not human. You cannot access this page.";
            exit;
        }

        $sql_chk = $link->prepare("SELECT * FROM `admins` WHERE (email='$email' OR username='$email') AND password='$password'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();
        if($count>0)
        {
            $adminData = $sql_chk->fetch(PDO::FETCH_OBJ);
            $admin_id = $adminData->id;
            $admin_password = $adminData->password;
            $admin_status = $adminData->status;
            $admin_firstname = $adminData->firstname;
            $IP_Address = $_SERVER['REMOTE_ADDR'];
            if($admin_status=='0')
            {
                echo "Your account is deactivate. Please contact administrator to activate your account.";
            }
            else
            {
                $sql_insert_log = "INSERT INTO `userlogs`(`userid`,`user_firstname`,`user_type`,`IP_Address`,`loggedOn`) VALUES('$admin_id','$admin_firstname','Admin','$IP_Address','$today_datetime')";
                $link->exec($sql_insert_log);   //Inserting logis for login


                $_SESSION['admin_logged_in'] = true;
                $_SESSION['admin_user'] = true;
                $_SESSION['admin_id'] = $admin_id;
                $_SESSION['admin_firstname'] = $admin_firstname;

                echo "success";
            }
        }
        else
        {
            echo "Please enter correct email id & password";
        }
    }
?>