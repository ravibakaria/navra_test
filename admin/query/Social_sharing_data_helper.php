<?php
        session_start();
        require_once '../../config/config.php'; 
        include('../../config/dbconnect.php');    //database connection
        include('../../config/functions.php');   //strip query string

        $today_datetime = date('Y-m-d H:i:s');
        
        $task = quote_smart($_POST['task']);

        $admin_id = $_SESSION['admin_id'];

        if($task == 'Add_Social_sharing')
        {
                $social_sharing_display = $_POST['social_sharing_display'];
                $social_sharing_data = $_POST['social_sharing_data'];
                $website_facebook_display = $_POST['website_facebook_display'];
                $website_facebook_data = $_POST['website_facebook_data'];
                $website_twitter_display = $_POST['website_twitter_display'];
                $website_twitter_data = $_POST['website_twitter_data'];
                $website_instagram_display = $_POST['website_instagram_display'];
                $website_instagram_data = $_POST['website_instagram_data'];
                $website_tumbler_display = $_POST['website_tumbler_display'];
                $website_tumbler_data = $_POST['website_tumbler_data'];
                $website_youtube_display = $_POST['website_youtube_display'];
                $website_youtube_data = $_POST['website_youtube_data'];
                $website_pinterest_display = $_POST['website_pinterest_display'];
                $website_pinterest_data = $_POST['website_pinterest_data'];
                $website_myspace_display = $_POST['website_myspace_display'];
                $website_myspace_data = $_POST['website_myspace_data'];
                $website_linkedin_display = $_POST['website_linkedin_display'];
                $website_linkedin_data = $_POST['website_linkedin_data'];
                $website_VKontakte_display = $_POST['website_VKontakte_display'];
                $website_VKontakte_data = $_POST['website_VKontakte_data'];
                $website_foursquare_display = $_POST['website_foursquare_display'];
                $website_foursquare_data = $_POST['website_foursquare_data'];
                $website_flicker_display = $_POST['website_flicker_display'];
                $website_flicker_data = $_POST['website_flicker_data'];
                $website_vine_display = $_POST['website_vine_display'];
                $website_vine_data = $_POST['website_vine_data'];
                $website_blogger_display = $_POST['website_blogger_display'];
                $website_blogger_data = $_POST['website_blogger_data'];
                $website_quora_display = $_POST['website_quora_display'];
                $website_quora_data = $_POST['website_quora_data'];
                $website_reddit_display = $_POST['website_reddit_display'];
                $website_reddit_data = $_POST['website_reddit_data'];

                /***********  addthis/shareThis file content  ************/
                $social_share_file_data = null;
                $social_share_script_file_name = "../../scripts/social_share_script.js";
                $db_social_share_file_name = "scripts/social_share_script.js";
                if(!file_exists($social_share_script_file_name))
                {
                    $social_share_script_file = fopen($social_share_script_file_name, "w") or die("Unable to open file!");
                }
                else
                {
                    $social_share_script_file = fopen($social_share_script_file_name, "w") or die("Unable to open file!");
                }

                $social_share_file_data = "$social_sharing_data";
                fwrite($social_share_script_file, $social_share_file_data);
                fclose($social_share_script_file);

                $sql_chk = "SELECT * FROM social_sharing";
                $stmt = $link->prepare($sql_chk);
                $stmt->execute();
                $count = $stmt->rowCount();
                $result_chk = $stmt->fetch();
                if($count>0)
                {
                        $sql_update = "UPDATE social_sharing SET social_sharing_display='$social_sharing_display',social_sharing_data='$db_social_share_file_name',website_facebook_display='$website_facebook_display',website_facebook_data='$website_facebook_data',website_twitter_display='$website_twitter_display',website_twitter_data='$website_twitter_data',website_instagram_display='$website_instagram_display',website_instagram_data='$website_instagram_data',website_tumbler_display='$website_tumbler_display',website_tumbler_data='$website_tumbler_data',website_youtube_display='$website_youtube_display',website_youtube_data='$website_youtube_data',website_pinterest_display='$website_pinterest_display',website_pinterest_data='$website_pinterest_data',website_myspace_display='$website_myspace_display',website_myspace_data='$website_myspace_data',website_linkedin_display='$website_linkedin_display',website_linkedin_data='$website_linkedin_data',website_VKontakte_display='$website_VKontakte_display',website_VKontakte_data='$website_VKontakte_data',website_foursquare_display='$website_foursquare_display',website_foursquare_data='$website_foursquare_data',website_flicker_display='$website_flicker_display',website_flicker_data='$website_flicker_data',website_vine_display='$website_vine_display',website_vine_data='$website_vine_data',website_blogger_display='$website_blogger_display',website_blogger_data='$website_blogger_data',website_quora_display='$website_quora_display',website_quora_data='$website_quora_data',website_reddit_display='$website_reddit_display',website_reddit_data='$website_reddit_data',userid='$admin_id',updatedOn='$today_datetime'";
                }
                else
                if($count==0)
                {
                        $sql_update = "INSERT INTO social_sharing(social_sharing_display,social_sharing_data,website_facebook_display,website_facebook_data,website_twitter_display,website_twitter_data,website_instagram_display,website_instagram_data,website_tumbler_display,website_tumbler_data,website_youtube_display,website_youtube_data,website_pinterest_display,website_pinterest_data,website_myspace_display,website_myspace_data,website_linkedin_display,website_linkedin_data,website_VKontakte_display,website_VKontakte_data,website_foursquare_display,website_foursquare_data,website_flicker_display,website_flicker_data,website_vine_display,website_vine_data,website_blogger_display,website_blogger_data,website_quora_display,website_quora_data,website_reddit_display,website_reddit_data,userid,createdOn) VALUES('$social_sharing_display','$db_social_share_file_name','$website_facebook_display','$website_facebook_data','$website_twitter_display','$website_twitter_data','$website_instagram_display','$website_instagram_data','$website_tumbler_display','$website_tumbler_data','$website_youtube_display','$website_youtube_data','$website_pinterest_display','$website_pinterest_data','$website_myspace_display','$website_myspace_data','$website_linkedin_display','$website_linkedin_data','$website_VKontakte_display','$website_VKontakte_data','$website_foursquare_display','$website_foursquare_data','$website_flicker_display','$website_flicker_data','$website_vine_display','$website_vine_data','$website_blogger_display','$website_blogger_data','$website_quora_display','$website_quora_data','$website_reddit_display','$website_reddit_data','$admin_id','$today_datetime')"; 
                }

                if($link->exec($sql_update))
                {
                        echo "success";
                        exit;
                }
                else
                {
                        echo "Something went wrong try after some time";
                        exit;
                }
        }
?>