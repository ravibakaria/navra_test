<?php

	/****************  1   admins Info    ***************/
		$sql_create_admins = $link->prepare("CREATE TABLE `admins` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `username` varchar(255) NOT NULL,
		  `password` longtext NOT NULL,
		  `firstname` varchar(255) DEFAULT NULL,
		  `lastname` varchar(255) DEFAULT NULL,
		  `email` varchar(255) NOT NULL,
		  `isSuperAdmin` char(1) DEFAULT '0',
		  `department` varchar(100) NOT NULL,
		  `status` varchar(255) NOT NULL DEFAULT '1',
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

	/****************     admins Info  End  ***************/

	/****************  2   About Us Info    ***************/
		$sql_create_about_us = $link->prepare("CREATE TABLE `about_us` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `content` longtext NOT NULL,
		  `display` char(1) DEFAULT '0',
		  `userid` int(11) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     About Us Info End   ***************/

	/**************** 3    address Info    ***************/
		$sql_create_address = $link->prepare("CREATE TABLE `address` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `city` varchar(255) NOT NULL,
		  `state` varchar(255) NOT NULL,
		  `country` varchar(255) NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     address Info End   ***************/

	/****************  4   blocked users Info    ***************/
		$sql_create_blockedusers = $link->prepare("CREATE TABLE `blockedusers` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `blocker` int(11) NOT NULL,
		  `blockedUser` int(11) NOT NULL,
		  `blockComment` varchar(500) NOT NULL,
		  `blockedOn` datetime NOT NULL,
		  `status` varchar(10) DEFAULT NULL,
		  `unblocked_Comment` varchar(500) DEFAULT NULL,
		  `unblocked_On` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     blocked users Info End   ***************/

	/****************  5   bodytype Info    ***************/
		$sql_create_bodytype = $link->prepare("CREATE TABLE `bodytype` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(50) NOT NULL,
		  `status` char(1) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     bodytype Info End   ***************/

	/****************  6   caste Info    ***************/
		$sql_create_caste = $link->prepare("CREATE TABLE `caste` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` longtext NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     caste Info End   ***************/

	/**************** 7    cities Info    ***************/
		$sql_create_cities = $link->prepare("CREATE TABLE `cities` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(30) NOT NULL,
		  `state_id` int(11) NOT NULL,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     cities Info End   ***************/

	/****************  8   clients Info    ***************/
		$sql_create_clients = $link->prepare("CREATE TABLE `clients` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `firstname` varchar(255) NOT NULL,
		  `lastname` varchar(255) NOT NULL,
		  `dob` DATE NOT NULL,
		  `gender` int(11) NOT NULL,
		  `look_for` int(11) DEFAULT NULL,
		  `unique_code` varchar(255) NOT NULL,
		  `email` varchar(255) NOT NULL,
		  `phonecode` varchar(10)  DEFAULT '0',
		  `phonenumber` varchar(255) NOT NULL,
		  `referer` varchar(20) DEFAULT NULL,
		  `password` longtext NOT NULL,
		  `status` int(11) NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
		  `isEmailVerified` char(1) DEFAULT '0',
		  `isEmailVerifiedOn` datetime DEFAULT NULL,
		  `isProfileComplete` char(1) DEFAULT '0',
		  `isProfileCompleteOn` datetime DEFAULT NULL,
		  `securityPIN` varchar(6) DEFAULT NULL,
		  `securityPIN_sentOn` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     clients Info  End  ***************/

	/****************  9   complexion Info    ***************/
		$sql_create_complexion = $link->prepare("CREATE TABLE `complexion` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(100) NOT NULL,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     complexion Info  End  ***************/

	/****************  10   contact_us Info    ***************/
		$sql_create_contact_us = $link->prepare("CREATE TABLE `contact_us` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `enquiry_email` varchar(200) DEFAULT NULL,
		  `companyName` varchar(100) DEFAULT NULL,
		  `street` varchar(200) DEFAULT NULL,
		  `city` varchar(20) DEFAULT NULL,
		  `state` varchar(20) DEFAULT NULL,
		  `country` varchar(20) DEFAULT NULL,
		  `postalCode` varchar(20) DEFAULT NULL,
		  `phonecode` varchar(10) DEFAULT NULL,
		  `phone` varchar(20) DEFAULT NULL,
		  `companyEmail` varchar(200) DEFAULT NULL,
		  `companyURL` varchar(100) DEFAULT NULL,
		  `userid` int(11) DEFAULT NULL,
		  `created_at` datetime DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     contact_us Info  End  ***************/

	/****************  11   countries Info    ***************/
		$sql_create_countries = $link->prepare("CREATE TABLE `countries` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `sortname` varchar(3) NOT NULL,
		  `name` varchar(150) NOT NULL,
		  `phonecode` int(11) NOT NULL,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	/****************     countries Info  End  ***************/

	/****************  12   currency Info    ***************/
		$sql_create_currency = $link->prepare("CREATE TABLE `currency` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `country` varchar(50) NOT NULL,
		  `currency` varchar(100) NOT NULL,
		  `code` varchar(10) NOT NULL,
		  `symbol` varchar(100) NOT NULL,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     currency Info  End  ***************/

	/****************  13   departments Info    ***************/
		$sql_create_departments = $link->prepare("CREATE TABLE `departments` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(50) NOT NULL,
		  `email` varchar(100) NOT NULL,
		  `status` char(1) NOT NULL,
		  `created_at` datetime DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     departments Info  end  ***************/

	/****************  14   designation Info    ***************/
		$sql_create_designation = $link->prepare("CREATE TABLE `designation` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(100) NOT NULL,
		  `designation_category` int(11) NOT NULL,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     designation Info  end  ***************/

	/****************  15   designation_category Info    ***************/
		$sql_create_designation_category = $link->prepare("CREATE TABLE `designation_category` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(100) NOT NULL,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     designation_category Info  end  ***************/

	/****************  16   disclaimer Info    ***************/
		$sql_create_disclaimer = $link->prepare("CREATE TABLE `disclaimer` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `content` longtext NOT NULL,
		  `display` char(1) NOT NULL DEFAULT '0',
		  `userid` int(11) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     disclaimer Info  end  ***************/

	/****************  17   documents Info    ***************/
		$sql_create_documents = $link->prepare("CREATE TABLE `documents` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `photo_type` varchar(255) NOT NULL,
		  `photo_number` varchar(255) NOT NULL,
		  `photo` longtext NOT NULL,
		  `status` int(11) NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `approved_reject_by` int(11) DEFAULT NULL,
		  `approved_reject_on` datetime DEFAULT NULL,
		  `reject_comment` varchar(200) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     documents Info  end  ***************/

	/****************  18   drinkhabbit Info    ***************/
		$sql_create_drinkhabbit = $link->prepare("CREATE TABLE `drinkhabbit` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(50) NOT NULL,
		  `status` char(1) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     drinkhabbit Info  end  ***************/

	/****************  19   eathabbit Info    ***************/
		$sql_create_eathabbit = $link->prepare("CREATE TABLE `eathabbit` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(50) NOT NULL,
		  `status` char(1) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     eathabbit Info  end  ***************/

	/****************  20   educationname Info    ***************/
		$sql_create_educationname = $link->prepare("CREATE TABLE `educationname` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` longtext NOT NULL,
		  `created_at` datetime NOT NULL,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     educationname Info  end  ***************/

	/****************  21   eduocc Info    ***************/
		$sql_create_eduocc = $link->prepare("CREATE TABLE `eduocc` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `education` int(11) NOT NULL,
		  `occupation` int(11) NOT NULL,
		  `designation_category` int(11) NOT NULL,
		  `designation` int(11) NOT NULL,
		  `industry` int(11) NOT NULL,
		  `income_currency` int(11) DEFAULT NULL,
		  `income` varchar(15) NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     eduocc Info  end  ***************/

	/****************  23   emailtemplates Info    ***************/
		$sql_create_emailtemplates = $link->prepare("CREATE TABLE `emailtemplates` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `emailTemplateName` varchar(200) NOT NULL,
		  `emailTemplateSubject` varchar(200) NOT NULL,
		  `emailTemplateMessage` longtext NOT NULL,
		  `updatedBy` varchar(10) NOT NULL,
		  `updatedOn` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     emailtemplates Info  end  ***************/

	/****************  24   emailtemplatestyle Info    ***************/
		$sql_create_emailtemplatestyle = $link->prepare("CREATE TABLE `emailtemplatestyle` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `EmailCSS` longtext NOT NULL,
		  `EmailGlobalHeader` longtext,
		  `EmailGlobalFooter` longtext,
		  `updateBy` varchar(10) DEFAULT NULL,
		  `updateOn` datetime DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     emailtemplatestyle Info  end  ***************/

	/****************  25   emailtemplatevariables Info    ***************/
		$sql_create_emailtemplatevariables = $link->prepare("CREATE TABLE `emailtemplatevariables` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(50) NOT NULL,
		  `description` varchar(100) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     emailtemplatevariables Info  end  ***************/

	/****************  26   employment Info    ***************/
		$sql_create_employment = $link->prepare("CREATE TABLE `employment` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` longtext NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     employment Info  end  ***************/

	/****************  27   family Info    ***************/
		$sql_create_family = $link->prepare("CREATE TABLE `family` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `fam_val` int(11) NOT NULL,
		  `fam_type` int(11) NOT NULL,
		  `fam_stat` int(11) NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     family Info  end  ***************/

	/****************  28   familystatus Info    ***************/
		$sql_create_familystatus = $link->prepare("CREATE TABLE `familystatus` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(50) NOT NULL,
		  `status` char(1) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     familystatus Info  end  ***************/

	/****************  29   familytype Info    ***************/
		$sql_create_familytype = $link->prepare("CREATE TABLE `familytype` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(50) NOT NULL,
		  `status` char(1) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     familytype Info  end  ***************/

	/****************  30   familyvalue Info    ***************/
		$sql_create_familyvalue = $link->prepare("CREATE TABLE `familyvalue` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(50) NOT NULL,
		  `status` char(1) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     familyvalue Info  end  ***************/

	/****************  31   faq Info    ***************/
		$sql_create_faq = $link->prepare("CREATE TABLE `faq` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `faq_question` longtext NOT NULL,
		  `faq_answer` longtext NOT NULL,
		  `status` char(1) NOT NULL DEFAULT '1',
		  `userid` int(11) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     faq Info  end  ***************/

	/****************   32  faq_page_show Info    ***************/
		$sql_create_faq_page_show = $link->prepare("CREATE TABLE `faq_page_show` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `display` char(1) DEFAULT '0',
		  `userid` int(11) NOT NULL,
		  `created_at` datetime DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     faq_page_show Info  end  ***************/

	/****************  33   gender Info    ***************/
		$sql_create_gender = $link->prepare("CREATE TABLE `gender` (
		  `id` int(11) NOT NULL,
		  `name` varchar(20) NOT NULL,
		  `description` varchar(50) NOT NULL,
		  `status` char(1) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     gender Info  end  ***************/

	/****************  34   generalsetup Info    ***************/
		$sql_create_generalsetup = $link->prepare("CREATE TABLE `generalsetup` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `WebSiteBasePath` varchar(200) NOT NULL,
		  `WebSiteTitle` varchar(100) DEFAULT NULL,
		  `WebSiteTagline` varchar(300) DEFAULT NULL,
		  `EmailAddress` varchar(200) DEFAULT NULL,
		  `LogoURL` varchar(100) DEFAULT NULL,
		  `FaviconURL` varchar(100) DEFAULT NULL,
		  `TermOfServiceURL` varchar(100) DEFAULT NULL,
		  `PrivacyPolicyURL` varchar(100) DEFAULT NULL,
		  `AllowedFileAttachmentTypes` varchar(200) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     generalsetup Info  end  ***************/

	/****************  35   homepagesetup Info    ***************/
		$sql_create_homepagesetup = $link->prepare("CREATE TABLE `homepagesetup` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `homepagebanner` varchar(500) DEFAULT NULL,
		  `homepageHeading` varchar(200) DEFAULT NULL,
		  `footercontentshow` char(1) DEFAULT '0',
		  `footercontent` varchar(1000) DEFAULT NULL,
		  `extracontentstripshow` char(1) DEFAULT '0',
		  `extracontentstripdata` varchar(1000) DEFAULT NULL,
		  `featuredprofilesshow` char(1) DEFAULT '0',
		  `featuredprofilescount` varchar(2) DEFAULT NULL,
		  `premiumprofilesshow` char(1) DEFAULT '0',
		  `premiumprofilescount` varchar(2) DEFAULT NULL,
		  `recentlyaddedprofilesshow` char(1) DEFAULT '0',
		  `recentlyaddedprofilescount` varchar(2) DEFAULT NULL,
		  `profilefiltershow` char(1) DEFAULT '0',
		  `profilefiltervalues` varchar(100) DEFAULT NULL,
		  `additional_footer_content1_display` char(1) DEFAULT NULL,
		  `additional_footer_content1` longtext,
		  `additional_footer_content2_display` char(1) DEFAULT NULL,
		  `additional_footer_content2` longtext,
		  `additional_footer_content3_display` char(1) DEFAULT NULL,
		  `additional_footer_content3` longtext,
		  `additional_footer_content4_display` char(1) DEFAULT NULL,
		  `additional_footer_content4` longtext
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     homepagesetup Info  end  ***************/

	/****************  36   industry Info    ***************/
		$sql_create_industry = $link->prepare("CREATE TABLE `industry` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(100) NOT NULL,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     industry Info  end  ***************/

	/****************  37   interested Info    ***************/
		$sql_create_interested = $link->prepare("CREATE TABLE `interested` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `inter_id` int(11) NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     interested Info  end  ***************/

	/****************  38   localizationsetup Info    ***************/
		$sql_create_localizationsetup = $link->prepare("CREATE TABLE `localizationsetup` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `DefaultCountry` varchar(100) DEFAULT NULL,
		  `DefaultTimeZone` varchar(200) DEFAULT NULL,
		  `DefaultCurrency` varchar(100) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     localizationsetup Info  end  ***************/

	/****************  39   mailrelaysetup Info    ***************/
		$sql_create_mailrelaysetup = $link->prepare("CREATE TABLE `mailrelaysetup` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `FromName` varchar(100) DEFAULT NULL,
		  `FromEmail` varchar(100) DEFAULT NULL,
		  `BCCEmail` varchar(100) DEFAULT NULL,
		  `SMTPHost` varchar(100) DEFAULT NULL,
		  `SMTPPort` varchar(10) DEFAULT NULL,
		  `SMTPUsername` varchar(100) DEFAULT NULL,
		  `SMTPPassword` varchar(200) DEFAULT NULL,
		  `SMTPSSL` varchar(5) DEFAULT NULL,
		  `GlobalEmailSignature` varchar(1000) DEFAULT NULL,
		  `emailFooterText` longtext,
		  `headerBackgroundColor` varchar(20) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     mailrelaysetup Info  end  ***************/

	/****************  40   maritalstatus Info    ***************/
		$sql_create_maritalstatus = $link->prepare("CREATE TABLE `maritalstatus` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(50) NOT NULL,
		  `status` char(1) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     maritalstatus Info  end  ***************/

	/****************  41   messagechat Info    ***************/
		$sql_create_messagechat = $link->prepare("CREATE TABLE `messagechat` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `chatid` varchar(100) NOT NULL,
		  `userFrom` int(11) NOT NULL,
		  `userTo` int(11) NOT NULL,
		  `message` varchar(500) NOT NULL,
		  `messagedOn` datetime NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     messagechat Info  end  ***************/

	/**************** 42   mothertongue Info    ***************/
		$sql_create_mothertongue = $link->prepare("CREATE TABLE `mothertongue` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		   `name` longtext NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `status` varchar(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     mothertongue Info  end  ***************/

	/****************  43   preferences Info    ***************/
		$sql_create_preferences = $link->prepare("CREATE TABLE `preferences` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) DEFAULT NULL,
		  `gender` int(11) DEFAULT '0',
		  `marital_status` varchar(255) DEFAULT NULL,
		  `city` varchar(255) DEFAULT NULL,
		  `state` varchar(255) DEFAULT NULL,
		  `country` varchar(255) DEFAULT NULL,
		  `religion` varchar(255) DEFAULT NULL,
		  `mother_tongue` varchar(255) DEFAULT NULL,
		  `caste` varchar(255) DEFAULT '0',
		  `education` varchar(255) DEFAULT NULL,
		  `occupation` varchar(255) DEFAULT NULL,
		  `currency` varchar(255) DEFAULT '0',
		  `monthly_income_from` varchar(255) DEFAULT NULL,
		  `monthly_income_to` varchar(250) DEFAULT NULL,
		  `from_date` varchar(255) NOT NULL DEFAULT '0',
		  `to_date` varchar(255) NOT NULL DEFAULT '0',
		  `body_type` varchar(255) DEFAULT '0',
		  `complexion` varchar(250) DEFAULT '0',
		  `height_from` varchar(255) DEFAULT '0',
		  `height_to` varchar(250) DEFAULT '0',
		  `fam_type` varchar(255) DEFAULT '0',
		  `smoke_habbit` varchar(255) DEFAULT '0',
		  `drink_habbit` varchar(255) DEFAULT '0',
		  `eat_habbit` varchar(255) DEFAULT '0',
		  `manglik` varchar(255) DEFAULT '0',
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     preferences Info  end  ***************/

	/****************  44   privacy_policy Info    ***************/
		$sql_create_privacy_policy = $link->prepare("CREATE TABLE `privacy_policy` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `content` longtext NOT NULL,
		  `display` char(1) NOT NULL DEFAULT '0',
		  `userid` int(11) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     privacy_policy Info  end  ***************/

	/****************  45   profilebasic Info    ***************/
		$sql_create_profilebasic = $link->prepare("CREATE TABLE `profilebasic` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `name` varchar(255) NOT NULL,
		  `height` varchar(255) NOT NULL,
		  `weight` mediumint(255) NOT NULL,
		  `eat_habbit` varchar(255) NOT NULL,
		  `smoke_habbit` varchar(255) NOT NULL,
		  `body_type` varchar(255) NOT NULL,
		  `complexion` varchar(255) NOT NULL,
		  `marital_status` varchar(255) NOT NULL,
		  `special_case` varchar(255) NOT NULL,
		  `drink_habbit` varchar(255) NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     profilebasic Info  end  ***************/

	/****************  46   profiledesc Info    ***************/
		$sql_create_profiledesc = $link->prepare("CREATE TABLE `profiledesc` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `short_desc` longtext NOT NULL,
		  `short_desc_family` longtext,
		  `short_desc_interest` longtext,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     profiledesc Info  end  ***************/

	/****************  47   profilepic Info    ***************/
		$sql_create_profilepic = $link->prepare("CREATE TABLE `profilepic` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `photo` longtext NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     profilepic Info  end  ***************/

	/****************  48   profilereligion Info    ***************/
		$sql_create_profilereligion = $link->prepare("CREATE TABLE `profilereligion` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `religion` int(11) NOT NULL,
		  `caste` int(11) NOT NULL,
		  `mother_tongue` int(11) NOT NULL,
		  `manglik` int(11) NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     profilereligion Info  end  ***************/

	/****************  49   profileview Info    ***************/
		$sql_create_profileview = $link->prepare("CREATE TABLE `profileview` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) DEFAULT NULL,
		  `profileid` int(11) DEFAULT NULL,
		  `view_count` int(11) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     profileview Info  end  ***************/

	/****************  50   religion Info    ***************/
		$sql_create_religion = $link->prepare("CREATE TABLE `religion` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` longtext NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     religion Info  end  ***************/

	/****************  51   requestphoto Info    ***************/
		$sql_create_requestphoto = $link->prepare("CREATE TABLE `requestphoto` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `requested_id` int(11) NOT NULL,
		  `requested_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     requestphoto Info  end  ***************/

	/****************  52   shortlisted Info    ***************/
		$sql_create_shortlisted = $link->prepare("CREATE TABLE `shortlisted` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `short_id` int(11) NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     shortlisted Info  end  ***************/

	/****************  53   smokehabbit Info    ***************/
		$sql_create_smokehabbit = $link->prepare("CREATE TABLE `smokehabbit` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(50) NOT NULL,
		  `status` char(1) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     smokehabbit Info  end  ***************/

	/****************  54   specialcases Info    ***************/
		$sql_create_specialcases = $link->prepare("CREATE TABLE `specialcases` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` longtext NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     specialcases Info  end  ***************/

	/****************  55   states Info    ***************/
		$sql_create_states = $link->prepare("CREATE TABLE `states` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` varchar(30) NOT NULL,
		  `country_id` int(11) NOT NULL DEFAULT '1',
		  `status` char(1) NOT NULL DEFAULT '1'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     states Info  end  ***************/

	/****************  56   terms_of_service Info    ***************/
		$sql_create_terms_of_service = $link->prepare("CREATE TABLE `terms_of_service` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `content` longtext NOT NULL,
		  `display` char(1) NOT NULL DEFAULT '0',
		  `userid` int(11) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     terms_of_service Info  end  ***************/

	/****************  57   timezone Info    ***************/
		$sql_create_timezone = $link->prepare("CREATE TABLE `timezone` (
		  `zone_id` int(10) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`zone_id`),
		  `country_code` char(2) COLLATE utf8_bin NOT NULL,
		  `zone_name` varchar(35) COLLATE utf8_bin NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
	/****************     timezone Info  end  ***************/

	/****************  58   usergallery Info    ***************/
		$sql_create_usergallery = $link->prepare("CREATE TABLE `usergallery` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `photo` longtext NOT NULL,
		  `status` char(1) NOT NULL DEFAULT '1',
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `rejected_by` int(11) DEFAULT NULL,
		  `rejected_on` datetime DEFAULT NULL,
		  `rejected_reason` longtext
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     usergallery Info  end  ***************/

	/****************  59   userlogs Info    ***************/
		$sql_create_userlogs = $link->prepare("CREATE TABLE `userlogs` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `user_firstname` varchar(20) NOT NULL,
		  `user_type` varchar(10) NOT NULL,
		  `IP_Address` varchar(20) NOT NULL,
		  `loggedOn` datetime NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     userlogs Info  end  ***************/

	/****************  60   invite_friends Info    ***************/
		$sql_create_invite_friends = $link->prepare("CREATE TABLE `invite_friends` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `unique_code` varchar(20) NOT NULL,
		  `invited_email` varchar(100) NOT NULL,
		  `invitedOn` datetime NOT NULL,
		  `invitation_sent_status` char(1) DEFAULT '0',
		  `invitation_sent_On` datetime DEFAULT NULL,
		  `invitation_status` char(1) DEFAULT '0'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     userlogs Info  end  ***************/

	/****************  61   general security Info    ***************/
		$sql_create_general_security = $link->prepare("CREATE TABLE `generalsecurity` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `recaptchaAllowed` char(1) DEFAULT '0',
		  `reCaptchaSiteKey` varchar(200) DEFAULT NULL,
		  `reCaptchaSecretKey` varchar(200) DEFAULT NULL,
		  `MinimumUserPasswordLength` varchar(10) DEFAULT NULL,
		  `MaximumUserPasswordLength` varchar(10) DEFAULT NULL,
		  `hideLastNameOfMember` char(1) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     general security  end  ***************/

	/****************  62   profile completeness Info    ***************/
		$sql_create_profile_completeness = $link->prepare("CREATE TABLE `profile_completeness` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
		  	PRIMARY KEY (`id`),
			  `userid` int(11) NOT NULL,
			  `profile_pic` char(1) DEFAULT '0',
			  `basic_info` char(1) DEFAULT '0',
			  `religious_info` char(1) DEFAULT '0',
			  `education_info` char(1) DEFAULT '0',
			  `location_info` char(1) DEFAULT '0',
			  `family_info` char(1) DEFAULT '0',
			  `about_info` char(1) DEFAULT '0'
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     general security  end  ***************/

	/****************  63   custom Script Info    ***************/
		$sql_create_custom_script = $link->prepare("CREATE TABLE `custom_script` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  PRIMARY KEY (`id`),
			  `header_script_display` char(1) DEFAULT NULL,
			  `header_script` varchar(500) DEFAULT NULL,
			  `footer_script_display` int(11) DEFAULT NULL,
			  `footer_script` varchar(500) DEFAULT NULL,
			  `updated_by` int(11) NOT NULL,
			  `updated_on` datetime DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     custom Script  end  ***************/

	/****************  64   Ads Management Info    ***************/
		$sql_create_ads_management = $link->prepare("CREATE TABLE `ads_management` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  PRIMARY KEY (`id`),
			  `leaderBoardDisplay1` char(1) DEFAULT NULL,
			  `leaderBoardData1` longtext,
			  `leaderBoardDisplay2` char(1) DEFAULT NULL,
			  `leaderBoardData2` longtext,
			  `leaderBoardDisplay3` char(1) DEFAULT NULL,
			  `leaderBoardData3` longtext,
			  `squarePopupDisplay1` char(1) DEFAULT NULL,
			  `squarePopupData1` longtext,
			  `squarePopupDisplay2` char(1) DEFAULT NULL,
			  `squarePopupData2` longtext,
			  `squarePopupDisplay3` char(1) DEFAULT NULL,
			  `squarePopupData3` longtext,
			  `skyScrapperDisplay1` char(1) DEFAULT NULL,
			  `skyScrapperData1` longtext,
			  `skyScrapperDisplay2` char(1) DEFAULT NULL,
			  `skyScrapperData2` longtext,
			  `skyScrapperDisplay3` char(1) DEFAULT NULL,
			  `skyScrapperData3` longtext,
			  `updatedBy` int(11) NOT NULL,
			  `updatedOn` datetime DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     custom Script  end  ***************/

	/****************  65   enquiry form Info  start  ***************/
		$sql_create_enquiry = $link->prepare("CREATE TABLE `custom_script` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  PRIMARY KEY (`id`),
			  `header_script_display` char(1) DEFAULT NULL,
			  `header_script` varchar(500) DEFAULT NULL,
			  `footer_script_display` int(11) DEFAULT NULL,
			  `footer_script` varchar(500) DEFAULT NULL,
			  `updated_by` int(11) NOT NULL,
			  `updated_on` datetime DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     enquiry form  end  ***************/

	/****************  66   Identity document types Info  start  ***************/
		$sql_create_identity_document_type = $link->prepare("CREATE TABLE `identity_document_types` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  PRIMARY KEY (`id`),
			  `name` varchar(100) NOT NULL,
			  `status` char(1) NOT NULL,
			  `createdUpdatedBy` int(11) NOT NULL,
			  `createdOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  `updatedOn` datetime DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     Identity document types  end  ***************/

	/****************  67   Profile Like Info  start  ***************/
		$sql_create_profile_like = $link->prepare("CREATE TABLE `profile_like` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  PRIMARY KEY (`id`),
			  `userid` int(11) NOT NULL,
			  `liked_userid` int(11) NOT NULL,
			  `liked_on` datetime NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     Profile Like  end  ***************/

	/****************  68   Social Sharing Info  start  ***************/
		$sql_create_social_sharing = $link->prepare("CREATE TABLE `social_sharing` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  PRIMARY KEY (`id`),
			  `social_sharing_display` char(1) DEFAULT '0',
			  `social_sharing_data` longtext,
			  `website_facebook_display` char(1) DEFAULT '0',
			  `website_facebook_data` longtext,
			  `website_twitter_display` char(1) DEFAULT '0',
			  `website_twitter_data` longtext,
			  `website_instagram_display` char(1) DEFAULT '0',
			  `website_instagram_data` longtext,
			  `website_tumbler_display` char(1) DEFAULT '0',
			  `website_tumbler_data` longtext,
			  `website_youtube_display` char(1) DEFAULT '0',
			  `website_youtube_data` longtext,
			  `website_pinterest_display` char(1) DEFAULT '0',
			  `website_pinterest_data` longtext,
			  `website_myspace_display` char(1) DEFAULT '0',
			  `website_myspace_data` longtext,
			  `website_linkedin_display` char(1) DEFAULT '0',
			  `website_linkedin_data` longtext,
			  `website_VKontakte_display` char(1) DEFAULT '0',
			  `website_VKontakte_data` longtext,
			  `website_foursquare_display` char(1) DEFAULT '0',
			  `website_foursquare_data` longtext,
			  `website_flicker_display` char(1) DEFAULT '0',
			  `website_flicker_data` longtext,
			  `website_vine_display` char(1) DEFAULT '0',
			  `website_vine_data` longtext,
			  `website_blogger_display` char(1) DEFAULT '0',
			  `website_blogger_data` longtext,
			  `website_quora_display` char(1) DEFAULT '0',
			  `website_quora_data` longtext,
			  `website_reddit_display` char(1) DEFAULT '0',
			  `website_reddit_data` longtext,
			  `userid` int(11) NOT NULL,
			  `createdOn` datetime DEFAULT NULL,
			  `updatedOn` datetime DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     Social Sharing Info  end  ***************/
	
	/****************  70   Theme color setting  start  ***************/
		$sql_create_theme_color_setting = $link->prepare("CREATE TABLE `theme_color_setting` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `primary_color` varchar(100) NOT NULL,
		  `primary_font_color` varchar(100) NOT NULL,
		  `sidebar_color` varchar(100) NOT NULL,
		  `sidebar_font_color` varchar(100) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     Theme color setting  end  ***************/

	/****************  71   report abuse start  ***************/
		$sql_create_report_abuse = $link->prepare("CREATE TABLE `report_abuse` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `abused_by` int(11) NOT NULL,
		  `abused_user_id` int(11) NOT NULL,
		  `abused_user_profile_id` varchar(20) NOT NULL,
		  `abused_comment` longtext NOT NULL,
		  `abused_on` datetime NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     report abuse end  ***************/

	/****************  72   profile views start  ***************/
		$sql_create_profile_views = $link->prepare("CREATE TABLE `profile_views` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `unique_code` varchar(20) NOT NULL,
		  `view_count` int(11) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     profile views end  ***************/

	/****************  73   Deactivate profiles  ***************/
		$sql_create_deactivate_profiles = $link->prepare("CREATE TABLE `deactivated_profiles` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `task` varchar(10) NOT NULL,
		  `reason` longtext NOT NULL,
		  `updated_on` datetime NOT NULL,
		  `IP` varchar(25) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     Deactivate profiles  ***************/

	/****************  74   Cron job  ***************/
		$sql_create_cron_jobs = $link->prepare("CREATE TABLE `cron_jobs` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `EmailVerificationReminderRunOrNot` char(1) DEFAULT NULL,
		  `EmailVerificationReminderRunAt` time DEFAULT NULL,
		  `ProfileCompletenessReminderRunOrNot` char(1) DEFAULT NULL,
		  `ProfileCompletenessRunAt` time DEFAULT NULL,
		  `BirthdayReminderRunOrNot` char(1) DEFAULT NULL,
		  `BirthdayReminderRunAt` time DEFAULT NULL,
		  `NewProfileReminderRunOrNot` int(1) DEFAULT NULL,
		  `NewProfileReminderFrom` varchar(10) DEFAULT NULL,
		  `NewProfileReminderRunAt` time DEFAULT NULL,
		  `UnreadMessageReminderRunOrNot` char(1) DEFAULT NULL,
		  `UnreadMessageReminderRunAt` time DEFAULT NULL,
		  `MembershipRenewalReminderRunOrNot` char(1) DEFAULT NULL,
		  `MembershipRenewalReminderRunAt` time DEFAULT NULL,
		  `CreatedBy` int(11) DEFAULT NULL,
		  `CreatedOn` datetime DEFAULT NULL,
		  `UpdatedBy` int(11) DEFAULT NULL,
		  `UpdatedOn` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     Deactivate profiles  ***************/

	/****************  75   Custome meta tags  ***************/
		$sql_create_custom_meta_tags = $link->prepare("CREATE TABLE `custom_meta_tags` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `meta_tag` longtext NOT NULL,
		  `created_by` int(11) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `updated_by` int(11) DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     Custome meta tags  ***************/

	/****************  76   Membership Plans  ***************/
		$sql_create_membership_plans = $link->prepare("CREATE TABLE `membership_plan` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `membership_plan_name` varchar(100) NOT NULL,
		  `number_of_contacts` int(11) NOT NULL,
		  `number_of_duration_months` int(11) NOT NULL,
		  `price` decimal(10,2) NOT NULL,
		  `status` char(1) NOT NULL,
		  `created_by` int(11) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `Updated_by` int(11) DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     Membership Plans  ***************/

	/****************  77   Featured Listing Plans  ***************/
		$sql_create_featured_listing_plans = $link->prepare("CREATE TABLE `featured_listing_plan` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `FeaturedMembershipAddOrNot` char(1) NOT NULL,
		  `FeaturdListingPrice` decimal(10,2) NOT NULL,
		  `created_by` int(11) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `updated_by` int(11) DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     Featured Listing Plans  ***************/

	/****************  78   Taxation Setup  ***************/
		$sql_create_taxation = $link->prepare("CREATE TABLE `taxation_setup` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `taxationAddOrNot` char(1) NOT NULL,
		  `taxation_Name` varchar(50) NOT NULL,
		  `taxationPercent` int(11) NOT NULL,
		  `created_by` int(11) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `updated_by` int(11) DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     Taxation Setup  ***************/

	/****************  79   maintainance_mode_setting   ***************/
		$sql_create_maintainance_mode = $link->prepare("CREATE TABLE `maintainance_mode_setting` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `maintainance_mode` char(1) NOT NULL,
		  `updated_by` int(11) NOT NULL,
		  `updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     maintainance_mode_setting  ***************/

	/****************  80   popup_setting   ***************/
		$sql_create_popup_setting = $link->prepare("CREATE TABLE `popup_setting` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `popup_display_or_not` char(1) NOT NULL,
		  `popup_image` varchar(250) NOT NULL,
		  `destination_url` varchar(500) NOT NULL,
		  `target` char(1) NOT NULL,
		  `created_by` int(11) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `updated_by` int(11) DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     popup_setting  ***************/

	/****************  81   member_photo_delete_admin_logs   ***************/
		$sql_create_member_photo_delete_admin_logs = $link->prepare("CREATE TABLE `member_photo_delete_admin_logs` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `photo_type` varchar(15) NOT NULL COMMENT 'profile_photo OR personal_photo',
		  `photo_reject_reason` varchar(500) NOT NULL,
		  `deleted_by` int(11) NOT NULL,
		  `deleted_on` datetime NOT NULL,
		  `ip_address` varchar(20) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     member_photo_delete_admin_logs  ***************/

	/****************  82   member_activity_logs   ***************/
		$sql_create_member_activity_logs = $link->prepare("CREATE TABLE `member_activity_logs` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `task` varchar(50) NOT NULL,
		  `activity` varchar(100) NOT NULL,
		  `IP_Address` varchar(100) NOT NULL,
		  `created_On` datetime NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     member_activity_logs  ***************/

	/****************  83   member_email_logs   ***************/
		$sql_create_member_email_logs = $link->prepare("CREATE TABLE `member_email_logs` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `task` varchar(100) NOT NULL,
		  `activity` varchar(100) NOT NULL,
		  `sent_On` datetime NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     member_email_logs  ***************/

	/****************  84   payment_instamojo   ***************/
		$sql_create_payment_instamojo = $link->prepare("CREATE TABLE `payment_instamojo` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `im_is_enable` char(1) NOT NULL,
		  `im_display_name` varchar(100) NOT NULL,
		  `im_activated_mode` varchar(10) DEFAULT NULL,
		  `im_test_api_key` varchar(200) DEFAULT NULL,
		  `im_test_auth_token` varchar(200) DEFAULT NULL,
		  `im_test_salt` varchar(200) DEFAULT NULL,
		  `im_test_email` varchar(200) DEFAULT NULL,
		  `im_prod_api_key` varchar(200) DEFAULT NULL,
		  `im_prod_auth_token` varchar(200) DEFAULT NULL,
		  `im_prod_salt` varchar(200) DEFAULT NULL,
		  `im_prod_email` varchar(200) DEFAULT NULL,
		  `im_created_by` int(11) NOT NULL,
		  `im_created_on` datetime NOT NULL,
		  `im_updated_by` int(11) DEFAULT NULL,
		  `im_updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     payment_instamojo  ***************/

	/****************  85   payment_transactions   ***************/
		$sql_create_payment_transactions = $link->prepare("CREATE TABLE `payment_transactions` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `OrderNumber` varchar(100) NOT NULL,
		  `membership_plan` varchar(10) NOT NULL,
		  `membership_plan_name` varchar(255) DEFAULT NULL,
		  `membership_plan_id` int(11) DEFAULT NULL,
		  `membership_contacts` int(11) DEFAULT NULL,
		  `membership_plan_amount` decimal(10,2) DEFAULT NULL,
		  `membership_plan_expiry_date` date DEFAULT NULL,
		  `featured_listing` varchar(10) NOT NULL,
		  `featured_listing_amount` decimal(10,2) DEFAULT NULL,
		  `featured_listing_expiry_date` date DEFAULT NULL,
		  `tax_applied` char(1) NOT NULL,
		  `tax_name` varchar(100) DEFAULT NULL,
		  `tax_percent` int(11) DEFAULT NULL,
		  `tax_amount` decimal(10,2) DEFAULT NULL,
		  `total_amount` decimal(10,2) NOT NULL,
		  `tenure` int(11) NOT NULL,
		  `transact_id` varchar(255) DEFAULT NULL,
		  `request_id` varchar(255) DEFAULT NULL,
		  `payment_gateway` varchar(255) DEFAULT NULL,
		  `payment_method` longtext,
		  `created_at` datetime NOT NULL,
		  `updated_at` datetime DEFAULT NULL,
		  `status` varchar(255) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     payment_transactions  ***************/

	/****************  86   payment_paytm   ***************/
		$sql_create_payment_paytm = $link->prepare("CREATE TABLE `payment_paytm` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `pt_is_enable` char(1) NOT NULL,
		  `pt_display_name` varchar(255) NOT NULL,
		  `pt_activated_mode` varchar(10) DEFAULT NULL,
		  `pt_test_merchant_id` varchar(255) DEFAULT NULL,
		  `pt_test_merchant_key` varchar(255) DEFAULT NULL,
		  `pt_test_website_name` varchar(100) DEFAULT NULL,
		  `pt_test_industry_type` varchar(100) DEFAULT NULL,
		  `pt_prod_merchant_id` varchar(255) DEFAULT NULL,
		  `pt_prod_merchant_key` varchar(255) DEFAULT NULL,
		  `pt_prod_website_name` varchar(100) DEFAULT NULL,
		  `pt_prod_industry_type` varchar(100) DEFAULT NULL,
		  `pt_created_by` int(11) NOT NULL,
		  `pt_created_on` datetime NOT NULL,
		  `pt_updated_by` int(11) DEFAULT NULL,
		  `pt_updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     payment_paytm  ***************/

	/****************  87   payment_payumoney   ***************/
		$sql_create_payment_payumoney = $link->prepare("CREATE TABLE `payment_payumoney` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `pm_is_enable` char(1) NOT NULL,
		  `pm_display_name` varchar(255) NOT NULL,
		  `pm_activated_mode` varchar(10) DEFAULT NULL,
		  `pm_test_merchant_key` varchar(255) DEFAULT NULL,
		  `pm_test_merchant_salt` varchar(255) DEFAULT NULL,
		  `pm_test_merchant_auth_header` longtext,
		  `pm_prod_merchant_key` varchar(255) DEFAULT NULL,
		  `pm_prod_merchant_salt` varchar(255) DEFAULT NULL,
		  `pm_prod_merchant_auth_header` longtext,
		  `pm_created_by` int(11) NOT NULL,
		  `pm_created_on` datetime NOT NULL,
		  `pm_updated_by` int(11) DEFAULT NULL,
		  `pm_updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     payment_payumoney  ***************/

	/****************  88   payment_offline   ***************/
		$sql_create_payment_offline = $link->prepare("CREATE TABLE `payment_offline` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `off_is_enable` char(1) NOT NULL,
		  `off_display_name` varchar(255) NOT NULL,
		  `off_pay_to_text` longtext NOT NULL,
		  `off_created_by` int(11) NOT NULL,
		  `off_created_on` datetime NOT NULL,
		  `off_updated_by` int(11) DEFAULT NULL,
		  `off_updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     payment_offline  ***************/

	/****************  89   payment_bank_transfer   ***************/
		$sql_create_payment_bank_transfer = $link->prepare("CREATE TABLE `payment_bank_transfer` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `bt_is_enable` char(1) NOT NULL,
		  `bt_display_name` varchar(255) NOT NULL,
		  `bt_pay_to_text` longtext NOT NULL,
		  `off_created_by` int(11) NOT NULL,
		  `off_created_on` datetime NOT NULL,
		  `off_updated_by` int(11) DEFAULT NULL,
		  `off_updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     payment_bank_transfer  ***************/

	/****************  90   sitemap_generation   ***************/
		$sql_create_sitemap_generation = $link->prepare("CREATE TABLE `sitemap_generation` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `sitemap_generation_allowed` char(1) NOT NULL,
		  `created_by` int(11) NOT NULL,
		  `created_on` datetime NOT NULL,
		  `updated_by` int(11) DEFAULT NULL,
		  `updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     sitemap_generation  ***************/

	/****************  91   robots_txt   ***************/
		$sql_create_robots_txt = $link->prepare("CREATE TABLE `robots_txt` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `robots_txt_allowed` char(1) NOT NULL,
		  `default_content` longtext NOT NULL,
		  `created_by` int(11) NOT NULL,
		  `created_on` datetime NOT NULL,
		  `updated_by` int(11) DEFAULT NULL,
		  `updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     robots_txt  ***************/

	/****************  92   membercategory   ***************/
		$sql_create_membercategory = $link->prepare("CREATE TABLE `membercategory` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `name` longtext NOT NULL,
		  `slug` longtext NOT NULL,
		  `status` char(1) NOT NULL,
		  `created_by` int(11) NOT NULL,
		  `created_on` datetime NOT NULL,
		  `updated_by` int(11) DEFAULT NULL,
		  `updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     membercategory  ***************/

	/****************  93   vendors   ***************/
		$sql_create_vendors = $link->prepare("CREATE TABLE `vendors` (
		  `vuserid` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`vuserid`),
		  `firstname` varchar(100) NOT NULL,
		  `lastname` varchar(100) NOT NULL,
		  `email` varchar(255) NOT NULL,
		  `phonecode` varchar(20) DEFAULT NULL,
		  `phone` varchar(20) NOT NULL,
		  `company_name` varchar(255) NOT NULL,
		  `category_id` int(11) NOT NULL,
		  `city` int(11) NOT NULL,
		  `state` int(11) NOT NULL,
		  `country` int(11) NOT NULL,
		  `short_desc` longtext NOT NULL,
		  `password` varchar(255) NOT NULL,
		  `created_by_user` varchar(10) NOT NULL,
		  `created_by` int(11) DEFAULT NULL,
		  `created_on` datetime NOT NULL,
		  `updated_by_user` varchar(10) DEFAULT NULL,
		  `updated_by` int(11) DEFAULT NULL,
		  `updated_on` datetime DEFAULT NULL,
		  `isEmailVerified` char(1) DEFAULT '0',
		  `isEmailVerifiedOn` datetime DEFAULT NULL,
		  `status` char(1) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     membercategory  ***************/

	/****************  94   vendor_gallery   ***************/
		$sql_create_vendor_gallery = $link->prepare("CREATE TABLE `vendor_gallery` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `vuserid` int(11) NOT NULL,
		  `photo` varchar(255) NOT NULL,
		  `created_on` datetime NOT NULL,
		  `status` char(1) DEFAULT '0',
		  `rejected_on` datetime DEFAULT NULL,
		  `rejected_reason` varchar(255) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     vendor_gallery  ***************/

	/****************  95   vendor_activity_logs   ***************/
		$sql_create_vendor_activity_logs = $link->prepare("CREATE TABLE `vendor_activity_logs` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `vuserid` int(11) NOT NULL,
		  `task` varchar(50) NOT NULL,
		  `activity` varchar(100) NOT NULL,
		  `IP_Address` varchar(100) NOT NULL,
		  `created_On` datetime NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     vendor_activity_logs  ***************/

	/****************  96   vendor_email_logs   ***************/
		$sql_create_vendor_email_logs = $link->prepare("CREATE TABLE `vendor_email_logs` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `vuserid` int(11) NOT NULL,
		  `task` varchar(100) NOT NULL,
		  `activity` varchar(100) NOT NULL,
		  `sent_On` datetime NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     vendor_email_logs  ***************/

	/****************  97   vendor_enquiries   ***************/
		$sql_create_vendor_enquiries = $link->prepare("CREATE TABLE `vendor_enquiries` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `vuserid` int(11) NOT NULL,
		  `fullname` varchar(255) NOT NULL,
		  `email` varchar(255) NOT NULL,
		  `phone` varchar(20) NOT NULL,
		  `subject` varchar(255) NOT NULL,
		  `message` longtext NOT NULL,
		  `isRegistered` char(1) NOT NULL,
		  `userid` int(11) NOT NULL,
		  `enquiry_on` datetime NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     vendor_enquiries  ***************/

	/****************  98   vendor_profile_pic   ***************/
		$sql_create_vendor_profile_pic = $link->prepare("CREATE TABLE `vendor_profile_pic` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `vuserid` int(11) NOT NULL,
		  `profile_pic` varchar(255) NOT NULL,
		  `uploaded_on` datetime NOT NULL,
		  `updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     vendor_profile_pic  ***************/

	/****************  99   wedding_directory_settings   ***************/
		$sql_create_wedding_directory_settings = $link->prepare("CREATE TABLE `wedding_directory_settings` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `wedding_directory_display_or_not` char(1) NOT NULL,
		  `custom_content` longtext,
		  `created_by` int(11) NOT NULL,
		  `created_on` datetime NOT NULL,
		  `updated_by` int(11) DEFAULT NULL,
		  `updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     wedding_directory_settings  ***************/

	/****************  100   vendor_photo_delete_admin_logs   ***************/
		$sql_create_vendor_photo_delete_admin_logs = $link->prepare("CREATE TABLE `vendor_photo_delete_admin_logs` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `vuserid` int(11) NOT NULL,
		  `photo_type` varchar(15) NOT NULL COMMENT 'profile_photo OR personal_photo',
		  `photo_reject_reason` varchar(500) NOT NULL,
		  `deleted_by` int(11) NOT NULL,
		  `deleted_on` datetime NOT NULL,
		  `ip_address` varchar(20) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     vendor_photo_delete_admin_logs  ***************/

	/****************  101   payment_razorpay   ***************/
		$sql_create_payment_razorpay = $link->prepare("CREATE TABLE `payment_razorpay` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `rp_is_enable` char(1) NOT NULL,
		  `rp_display_name` varchar(100) NOT NULL,
		  `rp_activated_mode` varchar(10) NOT NULL,
		  `rp_test_key_id` varchar(255) DEFAULT NULL,
		  `rp_test_key_secret` varchar(255) DEFAULT NULL,
		  `rp_prod_key_id` varchar(255) DEFAULT NULL,
		  `rp_prod_key_secret` varchar(255) DEFAULT NULL,
		  `rp_created_by` int(11) NOT NULL,
		  `rp_created_on` datetime NOT NULL,
		  `rp_updated_by` int(11) DEFAULT NULL,
		  `rp_updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     payment_razorpay  ***************/

	/****************  102   payment_paypal   ***************/
		$sql_create_payment_paypal = $link->prepare("CREATE TABLE `payment_paypal` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `pp_is_enable` char(1) NOT NULL,
		  `pp_display_name` varchar(100) NOT NULL,
		  `pp_activated_mode` varchar(10) NOT NULL,
		  `pp_test_client_id` varchar(255) DEFAULT NULL,
		  `pp_test_secret` varchar(255) DEFAULT NULL,
		  `pp_prod_client_id` varchar(255) DEFAULT NULL,
		  `pp_prod_secret` varchar(255) DEFAULT NULL,
		  `pp_created_by` int(11) NOT NULL,
		  `pp_created_on` datetime NOT NULL,
		  `pp_updated_by` int(11) DEFAULT NULL,
		  `pp_updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     vendor_photo_delete_admin_logs  ***************/

	/****************  103   payment_paypal   ***************/
		$sql_create_payment_2checkout = $link->prepare("CREATE TABLE `payment_2checkout` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `co_is_enable` char(1) NOT NULL,
		  `co_display_name` varchar(100) NOT NULL,
		  `co_activated_mode` varchar(10) NOT NULL,
		  `co_test_account_number` varchar(255) DEFAULT NULL,
		  `co_test_publishable_key` varchar(255) DEFAULT NULL,
		  `co_test_private_key` varchar(255) DEFAULT NULL,
		  `co_prod_account_number` varchar(255) DEFAULT NULL,
		  `co_prod_publishable_key` varchar(255) DEFAULT NULL,
		  `co_prod_private_key` varchar(255) DEFAULT NULL,
		  `co_created_by` int(11) NOT NULL,
		  `co_created_on` datetime NOT NULL,
		  `co_updated_by` int(11) DEFAULT NULL,
		  `co_updated_on` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     payment_2checkout  ***************/

	/****************  104   payment_transaction_status_update   ***************/
		$sql_create_payment_transaction_status_update = $link->prepare("CREATE TABLE `payment_transactions_status_update` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY (`id`),
		  `userid` int(11) NOT NULL,
		  `OrderNumber` varchar(255) NOT NULL,
		  `Order_Date` datetime NOT NULL,
		  `previous_status` char(1) NOT NULL,
		  `previous_amount_payable` decimal(10,2) NOT NULL,
		  `Order_update_date` datetime NOT NULL,
		  `updated_amount_payable` decimal(10,2) NOT NULL,
		  `updated_payment_method` varchar(50) NOT NULL,
		  `transaction_id` varchar(255) NOT NULL,
		  `updated_status` char(1) NOT NULL,
		  `confirmation_email_sent` char(1) NOT NULL,
		  `payment_remarks` longtext NOT NULL,
		  `admin_id` int(11) NOT NULL,
		  `updated_on` datetime NOT NULL,
		  `IP_ADDRESS` varchar(20) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
	/****************     payment_2checkout  ***************/
?>