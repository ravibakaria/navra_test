<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
    include('../../../config/setup-values.php');   //strip query string
    require_once "../../../config/email/class.phpmailer.php";
    //require_once "../../config/email/class.smtp.php";
	$admin_id = $_SESSION['admin_id'];

    $today_datetime = date('Y-m-d H:i:s');
    
    $task = quote_smart($_POST['task']);

    /************   Update membership plan Setting   *****************/
	if($task == "Change_membership_plan_status")
	{
		$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($id=='1')
	    {
	    	echo "You cannot deactivate/disable this plan.You can update its attributes.";
	    	exit;
	    }

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `membership_plan` SET `status`='$updated_status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
    
    /************   Add new membership plan   *****************/
	if($task == "Add-New-Membership-Plan")
	{
        $membership_plan_name = $_POST['membership_plan_name'];
		$number_of_contacts = $_POST['number_of_contacts'];
	    $number_of_duration_months = $_POST['number_of_duration_months'];
	    $price = $_POST['price'];
	    $status = $_POST['status'];

	    $sql_check = "SELECT * FROM `membership_plan` WHERE membership_plan_name='$membership_plan_name',number_of_contacts='$number_of_contacts',number_of_duration_months='$number_of_duration_months',price='$price',status='$status'";
	    $stmt_check = $link->prepare($sql_check);
	    $stmt_check->execute();
	    $count = $stmt_check->rowCount();

	    if($count>0)
	    {
	    	echo "Membership plan already exists.";
	    	exit;
	    }
	    else
	    if($count==0)
	    {
	    	$sql_insert = "INSERT INTO `membership_plan`(membership_plan_name,number_of_contacts,number_of_duration_months,price,status,created_by,created_at) VALUES('$membership_plan_name','$number_of_contacts','$number_of_duration_months','$price','$status','$admin_id','$today')";

	    	if($link->exec($sql_insert))
	        {
	        	echo "success";
	        	exit;
	        }
	        else
	        {
	        	echo "Something went wrong try after some time";
	        	exit;
	        }
	    }
	}

	/************   Update existing membership plan   *****************/
	if($task == "Update-Membership-Plan")
	{
		$membership_plan_id = $_POST['membership_plan_id'];
        $membership_plan_name = $_POST['membership_plan_name'];
		$number_of_contacts = $_POST['number_of_contacts'];
	    $number_of_duration_months = $_POST['number_of_duration_months'];
	    $price = $_POST['price'];
	    $status = $_POST['status'];

	    if($membership_plan_id=='1' && $status=='0')
	    {
	    	echo "You cannot deactivate/disable this plan.You can update its attributes.";
	    	exit;
	    }
	    
	    $sql_check = "SELECT * FROM `membership_plan` WHERE id='$membership_plan_id'";
	    $stmt_check = $link->prepare($sql_check);
	    $stmt_check->execute();
	    $count = $stmt_check->rowCount();

	    if($count>0)
	    {
	    	$result = $stmt_check->fetch();

	    	$membership_plan_name_db = $result['membership_plan_name'];
			$number_of_contacts_db = $result['number_of_contacts'];
		    $number_of_duration_months_db = $result['number_of_duration_months'];
		    $price_db = $result['price'];
		    $status_db = $result['status'];

		    if($membership_plan_name_db==$membership_plan_name && $number_of_contacts_db==$number_of_contacts && $number_of_duration_months_db==$number_of_duration_months && $price_db==$price && $status_db==$status)
		    {
		    	echo "Membership plan data already updated.";
	    		exit;
		    }
		    else
		    {
		    	$sql_update = "UPDATE `membership_plan` SET membership_plan_name='$membership_plan_name',number_of_contacts='$number_of_contacts',number_of_duration_months='$number_of_duration_months',price='$price',status='$status',Updated_by='$admin_id',updated_at='$today' WHERE id='$membership_plan_id'";

		    	if($link->exec($sql_update))
		        {
		        	echo "success";
		        	exit;
		        }
		        else
		        {
		        	echo "Something went wrong try after some time";
		        	exit;
		        }
		    }
	    }
	    else
	    if($count==0)
	    {
	    	echo "Membership plan does not exists.";
	    	exit;
	    }
	}

	/************   Add/Update featured membership plan   *****************/
	if($task == "update-featured-listing-plan")
	{
		$FeaturedMembershipAddOrNot = $_POST['FeaturedMembershipAddOrNot'];
        $FeaturdListingPrice = $_POST['FeaturdListingPrice'];

	    $sql_chk = "SELECT * FROM featured_listing_plan";
	    $stmt = $link->prepare($sql_chk);
	    $stmt->execute();
	    $count = $stmt->rowCount();

	    if($count==0)
	    {
	    	$sql_update = "INSERT INTO featured_listing_plan(FeaturedMembershipAddOrNot,FeaturdListingPrice,created_by,created_at) VALUES('$FeaturedMembershipAddOrNot','$FeaturdListingPrice','$admin_id','$today')";
	    }
	    else
	    if($count>0)
	    {
	    	$result = $stmt->fetch();

	    	$FeaturedMembershipAddOrNot_db = $result['FeaturedMembershipAddOrNot'];
        	$FeaturdListingPrice_db = $result['FeaturdListingPrice'];

        	if($FeaturedMembershipAddOrNot_db==$FeaturedMembershipAddOrNot && $FeaturdListingPrice_db==$FeaturdListingPrice)
        	{
        		echo "Featured listing plan already updated.";
        		exit;
        	}
        	else
        	{
        		$sql_update = "UPDATE featured_listing_plan SET FeaturedMembershipAddOrNot='$FeaturedMembershipAddOrNot',FeaturdListingPrice='$FeaturdListingPrice',updated_by='$admin_id',updated_at='$today'";
        	}
	    }

	    if($link->exec($sql_update))
	    {
	    	echo "success";
	    	exit;
	    }
	    else
	    {
	    	echo "Something went wrong! Please try after some time.";
	    	exit;
	    }
	}

	/************   Add/Update Taxation   *****************/
	if($task == "Update-Taxation-Setup")
	{
		$taxationAddOrNot = $_POST['taxationAddOrNot'];
        $taxation_Name = $_POST['taxation_Name'];
        $taxationPercent = $_POST['taxationPercent'];

	    $sql_chk = "SELECT * FROM taxation_setup";
	    $stmt = $link->prepare($sql_chk);
	    $stmt->execute();
	    $count = $stmt->rowCount();

	    if($count==0)
	    {
	    	$sql_update = "INSERT INTO taxation_setup(taxationAddOrNot,taxation_Name,taxationPercent,created_by,created_at) VALUES('$taxationAddOrNot','$taxation_Name','$taxationPercent','$admin_id','$today')";
	    }
	    else
	    if($count>0)
	    {
	    	$result = $stmt->fetch();

	    	$taxationAddOrNot_db = $result['taxationAddOrNot'];
        	$taxation_Name_db = $result['taxation_Name'];
        	$taxationPercent_db = $result['taxationPercent'];

        	if($taxationAddOrNot_db==$taxationAddOrNot && $taxation_Name_db==$taxation_Name && $taxationPercent_db==$taxationPercent)
        	{
        		echo "Featured listing plan already updated.";
        		exit;
        	}
        	else
        	{
        		$sql_update = "UPDATE taxation_setup SET taxationAddOrNot='$taxationAddOrNot',taxation_Name='$taxation_Name',taxationPercent='$taxationPercent',updated_by='$admin_id',updated_at='$today'";
        	}
	    }

	    if($link->exec($sql_update))
	    {
	    	echo "success";
	    	exit;
	    }
	    else
	    {
	    	echo "Something went wrong! Please try after some time.";
	    	exit;
	    }
	}
?>