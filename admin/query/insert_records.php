<?php

	/****************  0   WesiteTitle & Tagline    ***************/
		$sql_insert_website_content = "INSERT INTO `generalsetup`(`WebSiteBasePath`,`WebSiteTitle`,`WebSiteTagline`,`EmailAddress`,`LogoURL`,`AllowedFileAttachmentTypes`) VALUES('$base_path','$WebSiteTitle','$WebSiteTagline','$admin_email','../images/logo/default-logo.jpg','jpg,jpeg,png')";
	/****************     admins Info  End  ***************/

	/****************  1   admins Info    ***************/
		$sql_insert_admin = "INSERT INTO `admins`(`username`,`password`,`email`,`isSuperAdmin`,`department`,`created_at`) VALUES('$admin_user_name','$admin_password_x','$admin_email','1','All','$today')";
	/****************     admins Info  End  ***************/

	
	/****************   5   bodytype Info    ***************/
		$sql_insert_bodytype = file_get_contents('sql/bodytype.sql');
	
	/****************     bodytype Info  End  ***************/

	/****************  6   caste Info    ***************/
		$sql_insert_caste = file_get_contents('sql/caste.sql');
	/****************     caste Info  End  ***************/

	/****************  7    cities Info    ***************/
		$sql_insert_cities = file_get_contents('sql/cities.sql');
		$sql_insert_cities1 = file_get_contents('sql/cities1.sql');
		$sql_insert_cities2 = file_get_contents('sql/cities2.sql');
		$sql_insert_cities3 = file_get_contents('sql/cities3.sql');
		$sql_insert_cities4 = file_get_contents('sql/cities4.sql');
		$sql_insert_cities5 = file_get_contents('sql/cities5.sql');
		$sql_insert_cities6 = file_get_contents('sql/cities6.sql');
		$sql_insert_cities7 = file_get_contents('sql/cities7.sql');
		$sql_insert_cities8 = file_get_contents('sql/cities8.sql');
		$sql_insert_cities9 = file_get_contents('sql/cities9.sql');
		$sql_insert_cities10 = file_get_contents('sql/cities10.sql');

	/****************     cities Info  End  ***************/

	/****************   9   complexion Info    ***************/
		$sql_insert_complexion = file_get_contents('sql/complexion.sql');
	/****************     complexion Info  End  ***************/

	/****************  10   Contact Us Info    ***************/
		$sql_insert_contact_us = "INSERT INTO `contact_us` (`id`, `enquiry_email`, `companyName`, `street`, `city`, `state`, `country`, `postalCode`, `phonecode`, `phone`, `companyEmail`, `companyURL`, `userid`, `created_at`, `updated_at`) VALUES
(1, '$admin_email', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '$today', NULL)";
	/****************     contact_us Info  End  ***************/

	/****************   11   countries Info    ***************/
		$sql_insert_countries = file_get_contents('sql/countries.sql');
	/****************     countries Info  End  ***************/

	/****************   12   currency Info    ***************/
		$sql_insert_currency = file_get_contents('sql/currency.sql');
	/****************     currency Info  End  ***************/

	/****************   14   designation Info    ***************/
		$sql_insert_designation = file_get_contents('sql/designation.sql');
	/****************     designation Info  End  ***************/

	/****************   15   designation_category Info    ***************/
		$sql_insert_designation_category = file_get_contents('sql/designation_category.sql');
	/****************     designation_category Info  End  ***************/

	/****************   18   drinkhabbit Info    ***************/
		$sql_insert_drinkhabbit = file_get_contents('sql/drinkhabbit.sql');
	/****************     drinkhabbit Info  End  ***************/

	/****************   19   eathabbit Info    ***************/
		$sql_insert_eathabbit = file_get_contents('sql/eathabbit.sql');
	/****************     eathabbit Info  End  ***************/

	/****************   20   educationname Info    ***************/
		$sql_insert_educationname = file_get_contents('sql/educationname.sql');
	/****************     educationname Info  End  ***************/

	/****************   23   emailtemplates Info    ***************/
		$sql_insert_emailtemplates = file_get_contents('sql/emailtemplates.sql');
	/****************     emailtemplates Info  End  ***************/

	/****************   24   emailtemplatestyle Info    ***************/
		$sql_insert_emailtemplatestyle = file_get_contents('sql/emailtemplatestyle.sql');
	/****************     emailtemplatestyle Info  End  ***************/

	/****************   25   emailtemplatevariables Info    ***************/
		$sql_insert_emailtemplatevariables = file_get_contents('sql/emailtemplatevariables.sql');
	/****************     emailtemplatevariables Info  End  ***************/

	/****************   26   employment Info    ***************/
		$sql_insert_employment = file_get_contents('sql/employment.sql');
	/****************     employment Info  End  ***************/

	/****************   28   familystatus Info    ***************/
		$sql_insert_familystatus = file_get_contents('sql/familystatus.sql');
	/****************     familystatus Info  End  ***************/

	/****************   29   familytype Info    ***************/
		$sql_insert_familytype = file_get_contents('sql/familytype.sql');
	/****************     familytype Info  End  ***************/

	/****************   30   familyvalue Info    ***************/
		$sql_insert_familyvalue = file_get_contents('sql/familyvalue.sql');
	/****************     familyvalue Info  End  ***************/

	/****************   33   gender Info    ***************/
		$sql_insert_gender = file_get_contents('sql/gender.sql');
	/****************     gender Info  End  ***************/

	/****************   36   industry Info    ***************/
		$sql_insert_industry = file_get_contents('sql/industry.sql');
	/****************     industry Info  End  ***************/

	/****************   40   maritalstatus Info    ***************/
		$sql_insert_maritalstatus = file_get_contents('sql/maritalstatus.sql');
	/****************     maritalstatus Info  End  ***************/

	/****************   42   mothertongue Info    ***************/
		$sql_insert_mothertongue = file_get_contents('sql/mothertongue.sql');
	/****************     mothertongue Info  End  ***************/

	/****************   50   religion Info    ***************/
		$sql_insert_religion = file_get_contents('sql/religion.sql');
	/****************     religion Info  End  ***************/

	/****************   53   smokehabbit Info    ***************/
		$sql_insert_smokehabbit = file_get_contents('sql/smokehabbit.sql');
	/****************     smokehabbit Info  End  ***************/

	/****************   54   specialcases Info    ***************/
		$sql_insert_specialcases = file_get_contents('sql/specialcases.sql');
	/****************     specialcases Info  End  ***************/

	/****************   55   states Info    ***************/
		$sql_insert_states = file_get_contents('sql/states.sql');
	/****************     states Info  End  ***************/

	/****************   57   timezone Info    ***************/
		$sql_insert_timezone = file_get_contents('sql/timezone.sql');
	/****************     timezone Info  End  ***************/

	/****************   13   departments Info    ***************/
		$sql_insert_departments = "INSERT INTO `departments` (`id`, `name`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '$admin_email', '1', '$today', NULL);";
	/****************     timezone Info  End  ***************/

	/****************   66   identity_document_type Info    ***************/
		$sql_insert_identity_document_type = file_get_contents('sql/identity_document_types.sql');
	/****************     identity_document_type Info  End  ***************/

	/****************   70   theme color setting Info    ***************/
		$sql_insert_theme_color_setting = "INSERT INTO `theme_color_setting` (`id`, `primary_color`, `primary_font_color`, `sidebar_color`, `sidebar_font_color`) VALUES(1, '#ab47bc', '#ffffff', '#5e35b1', '#ffffff');";
	/****************     theme color setting Info  End  ***************/


	/****************   76   membership plan    ***************/
		$sql_insert_membership_plan = "INSERT INTO `membership_plan` (`id`, `membership_plan_name`, `number_of_contacts`, `number_of_duration_days`, `price`, `status`, `created_by`, `created_at`, `Updated_by`, `updated_at`) VALUES
		(1, 'FREE', 50, '1', '0.00', '1', 1, '$today', NULL, NULL);";
	/****************     membership plan  End  ***************/

	/****************   79   maintainance_mode    ***************/
		$sql_insert_maintainance_mode = "INSERT INTO `maintainance_mode_setting` (`id`, `maintainance_mode`, `updated_by`, `updated_on`) VALUES
		(1, '1', 1, '$today');";
	/****************     maintainance_mode  End  ***************/

	/****************   91   robots_txt    ***************/
		$sql_insert_robots_txt = "INSERT INTO `robots_txt` (`id`, `robots_txt_allowed`, `default_content`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
	(1, '1', 'User-agent: * \r\nDisallow: /admin/\r\nDisallow: /config/\r\nDisallow: /css/\r\nDisallow: /fonts/\r\nDisallow: /js/\r\nAllow: /', 1, '$today', NULL, NULL);";
	/****************     robots_txt  End  ***************/

	/****************   92   membercategory    ***************/
		$sql_insert_membercategory = file_get_contents('sql/membercategory.sql');
	/****************     membercategory  End  ***************/
?>