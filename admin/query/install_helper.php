<?php
	ini_set('max_execution_time', 7200); //300 seconds = 5 minutes
	include('../../config/functions.php');   //strip query string
	
    $task = quote_smart($_POST['task']);

    if($task == "Check_db_connectivity")
	{
		$host_name = quote_smart($_POST['host_name']);
    	$db_name = quote_smart($_POST['db_name']);
    	$db_user_name = quote_smart($_POST['db_user_name']);
    	$db_password = $_POST['db_password'];

		// Open connection
		$dsn = "mysql:host=$host_name;dbname=$db_name";
		try 
		{
			// create database connection
			$link = new PDO($dsn, $db_user_name, $db_password);

		} 

		catch (PDOException $e) 
		{
			echo $e->getMessage();
		}

		if($link)
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Error connecting to database";
			exit;
		}

    }

	if($task == "install_data_in")
	{
		$base_path = quote_smart($_POST['base_path']);
		$host_name = quote_smart($_POST['host_name']);
    	$db_name = quote_smart($_POST['db_name']);
    	$db_user_name = quote_smart($_POST['db_user_name']);
    	$db_password = $_POST['db_password'];
    	$WebSiteTitle = quote_smart($_POST['WebSiteTitle']);
    	$WebSiteTagline = quote_smart($_POST['WebSiteTagline']);
    	$admin_user_name = quote_smart($_POST['admin_user_name']);
    	$admin_email = quote_smart($_POST['admin_email']);
    	$admin_password = $_POST['admin_password'];
    	$admin_confirm_password = $_POST['admin_confirm_password'];
    	$admin_password_x = md5($admin_password);

    	if($admin_password == $admin_confirm_password)
		{
			$haveuppercase = preg_match('/[A-Z]/', $admin_password);
			$havenumeric = preg_match('/[0-9]/', $admin_password);
			$havespecial = preg_match('/[!@#$%^&)*_(+=}{|:;,.<>}]/', $admin_password);

			if (!$haveuppercase)
			{
				echo 'Admin password must have atleast one upper case character.';
			}
			else if (!$havenumeric)
			{
				echo 'Admin password must have atleast one digit.';
			}
			else if(!$havespecial)
			{
				echo 'Admin password must have atleast one of the special characters [!@#$%^&)*_(+=}{|:;,.<>}]';
			}
			else if (strlen($admin_password) < 8)
			{
				echo 'Admin password must be of minimum 8 characters long.';
			}
			else if (strlen($admin_password) > 40)
			{
				echo 'Admin password must be of maximum 40 characters long.';
			}
			else
			{
				// Open connection
				$dsn = "mysql:host=$host_name;dbname=$db_name";
				try 
				{
					// create database connection
					$link = new PDO($dsn, $db_user_name, $db_password, array(PDO::MYSQL_ATTR_LOCAL_INFILE => true,PDO::ATTR_EMULATE_PREPARES => true));

				} 

				catch (PDOException $e) 
				{
					echo $e->getMessage();
				}

				if(!$link)
				{
					echo "Failed to connect to MySQL database "; 
							exit;
				}
				else
				{
					$file_data = null;
					$config_file_name = "../../config/config.php";
					if(!file_exists($config_file_name))
					{
						$config_file = fopen($config_file_name, "w") or die("Unable to open file!");
						$file_data = "<?php\n";
						$file_data .= "//site specific configuration declartions\n";
						$file_data .= "define( 'DB_HOST', '".$host_name."' );\n";
						$file_data .= "define( 'DB_USERNAME', '".$db_user_name."');\n";
						$file_data .= "define( 'DB_PASSWORD', '".$db_password."');\n";
						$file_data .= "define( 'DB_NAME', '".$db_name."');\n";
						fwrite($config_file, $file_data);
						fclose($config_file);
					}

					include("create_tables.php");
					
					$sql_create_admins->execute();      //1/admin info
					$sql_create_about_us->execute();      //2/about us info
					$sql_create_address->execute();      //3/address info
					$sql_create_blockedusers->execute();      //4/blockedusers info
					$sql_create_bodytype->execute();      //5/bodytype info
					$sql_create_caste->execute();      //6/caste info
					$sql_create_cities->execute();      //7/cities info
					$sql_create_clients->execute();      //8/clients info
					$sql_create_complexion->execute();      //9/complexion info
					$sql_create_contact_us->execute();      //10/contact_us info
					$sql_create_countries->execute();      //11/countries info
					$sql_create_currency->execute();      //12/currency info
					$sql_create_departments->execute();      //13/departments info
					$sql_create_designation->execute();      //14/designation info
					$sql_create_designation_category->execute();    //15/designation_category info
					$sql_create_disclaimer->execute();    //16/disclaimer info
					$sql_create_documents->execute();      //17/documents info
					$sql_create_drinkhabbit->execute();      //18/drink_habbit info
					$sql_create_eathabbit->execute();      //19/eat_habbit info
					$sql_create_educationname->execute();      //20/educationname info
					$sql_create_eduocc->execute();      //21/eduocc info
					$sql_create_emailtemplates->execute();      //23/emailtemplates info
					$sql_create_emailtemplatestyle->execute();      //24/emailtemplatestyle info
					$sql_create_emailtemplatevariables->execute();      //25/emailtemplatevariables info
					$sql_create_employment->execute();      //26/employment info
					$sql_create_family->execute();      //27/family info
					$sql_create_familystatus->execute();      //28/familystatus info
					$sql_create_familytype->execute();      //29/familytype info
					$sql_create_familyvalue->execute();      //30/familyvalue info
					$sql_create_faq->execute();      	//31/faq info
					$sql_create_faq_page_show->execute();      //32/faq_page_show info
					$sql_create_gender->execute();      //33/gender info
					$sql_create_generalsetup->execute();      //34/generalsetup info
					$sql_create_homepagesetup->execute();      //35/homepagesetup info
					$sql_create_industry->execute();      //36/industry info
					$sql_create_interested->execute();      //37/interested info
					$sql_create_localizationsetup->execute();      //38/localizationsetup info
					$sql_create_mailrelaysetup->execute();      //39/mailrelaysetup info
					$sql_create_maritalstatus->execute();      //40/maritalstatus info
					$sql_create_messagechat->execute();      //41/messagechat info
					$sql_create_mothertongue->execute();      //42/mothertongue info
					$sql_create_preferences->execute();      //43/preferences info
					$sql_create_privacy_policy->execute();      //44/privacy_policy info
					$sql_create_profilebasic->execute();      //45/profilebasic info
					$sql_create_profiledesc->execute();      //46/profiledesc info
					$sql_create_profilepic->execute();      //47/profilepic info
					$sql_create_profilereligion->execute();      //48/profilereligion info
					$sql_create_profileview->execute();      //49/profileview info
					$sql_create_religion->execute();      //50/religion info
					$sql_create_requestphoto->execute();      //51/requestphoto info
					$sql_create_shortlisted->execute();      //52/shortlisted info
					$sql_create_smokehabbit->execute();      //53/smokehabbit info
					$sql_create_specialcases->execute();      //54/specialcases info
					$sql_create_states->execute();      //55/states info
					$sql_create_terms_of_service->execute();      //56/terms_of_service info
					$sql_create_timezone->execute();      //57/timezone info
					$sql_create_usergallery->execute();      //58/usergallery info
					$sql_create_userlogs->execute();      //59/userlogs info
					$sql_create_general_security->execute();   //60/invite_friends info
					$sql_create_invite_friends->execute();   //61/general_security info
					$sql_create_profile_completeness->execute();   //62/profile_completeness info
					$sql_create_custom_script->execute();   //63/profile_completeness info
					$sql_create_ads_management->execute();   //64/ads management
					$sql_create_enquiry->execute();   //65/contact us enquiry
					$sql_create_identity_document_type->execute();   //66/identity document typs
					$sql_create_profile_like->execute();   //67/identity document typs
					$sql_create_social_sharing->execute();   //68/social sharing typs
					$sql_create_theme_color_setting->execute();   //70/theme color
					$sql_create_report_abuse->execute();   //71/report abuse
					$sql_create_profile_views->execute();   //72/profile views
					$sql_create_deactivate_profiles->execute();   //73/deactivate profiles
					$sql_create_cron_jobs->execute();   //74/cron jobs
					$sql_create_custom_meta_tags->execute();   //75/custome meta tags
					$sql_create_membership_plans->execute();   //76/membership plans
					$sql_create_featured_listing_plans->execute();   //77/featured_listing plan
					$sql_create_taxation->execute();   //78/taxation
					$sql_create_maintainance_mode->execute();   //79/maintainance mode
					$sql_create_popup_setting->execute();   //80/popup_setting
					$sql_create_member_photo_delete_admin_logs->execute();   //81/member_photo_delete_admin_logs
					$sql_create_member_activity_logs->execute();   //82/member_activity_logs
					$sql_create_member_email_logs->execute();   //83/member_email_logs
					$sql_create_payment_instamojo->execute();   //84/member_email_logs
					$sql_create_payment_transactions->execute();   //85/member_email_logs
					$sql_create_payment_paytm->execute();   //86/payment_paytm
					$sql_create_payment_payumoney->execute();   //87/payment_payumoney
					$sql_create_payment_offline->execute();   //88/payment_offline
					$sql_create_payment_bank_transfer->execute();   //89/payment_bank_transfer
					$sql_create_sitemap_generation->execute();   //90/sitemap_generation
					$sql_create_robots_txt->execute();   //91/robots_txt
					$sql_create_membercategory->execute();   //92/membercategory
					$sql_create_vendors->execute();   //93/vendors
					$sql_create_vendor_gallery->execute();   //94/vendor_gallery
					$sql_create_vendor_activity_logs->execute();   //95/vendor_gallery
					$sql_create_vendor_email_logs->execute();   //96/vendor_email_logs
					$sql_create_vendor_enquiries->execute();   //97/vendor_enquiries
					$sql_create_vendor_profile_pic->execute();   //98/vendor_profile_pic
					$sql_create_wedding_directory_settings->execute();   //99/wedding_directory_settings
					$sql_create_vendor_photo_delete_admin_logs->execute();   //100/vendor_photo_delete_admin_logs
					$sql_create_payment_razorpay->execute();   //101/payment_razorpay
					$sql_create_payment_paypal->execute();   //102/payment_paypal
					$sql_create_payment_2checkout->execute();   //103/payment_2checkout
					$sql_create_payment_transaction_status_update->execute();   //104/sql_create_payment_transaction_status_update

					include("insert_records.php");


					if($link->exec($sql_insert_website_content) && $link->exec($sql_insert_admin) && $link->exec($sql_insert_bodytype) && $link->exec($sql_insert_caste) && $link->exec($sql_insert_complexion) && $link->exec($sql_insert_contact_us) && $link->exec($sql_insert_countries) && $link->exec($sql_insert_currency) && $link->exec($sql_insert_designation) && $link->exec($sql_insert_departments) && $link->exec($sql_insert_designation_category) && $link->exec($sql_insert_drinkhabbit) && $link->exec($sql_insert_eathabbit) && $link->exec($sql_insert_educationname) && $link->exec($sql_insert_emailtemplates) && $link->exec($sql_insert_emailtemplatestyle) && $link->exec($sql_insert_emailtemplatevariables) && $link->exec($sql_insert_employment) && $link->exec($sql_insert_familystatus) && $link->exec($sql_insert_familytype) && $link->exec($sql_insert_familyvalue) && $link->exec($sql_insert_gender) && $link->exec($sql_insert_industry) && $link->exec($sql_insert_maritalstatus) && $link->exec($sql_insert_mothertongue) && $link->exec($sql_insert_religion) && $link->exec($sql_insert_smokehabbit) && $link->exec($sql_insert_specialcases) && $link->exec($sql_insert_states) && $link->exec($sql_insert_timezone) && $link->exec($sql_insert_cities) && $link->exec($sql_insert_cities1) && $link->exec($sql_insert_cities2) && $link->exec($sql_insert_cities3) && $link->exec($sql_insert_cities4) && $link->exec($sql_insert_cities5) && $link->exec($sql_insert_cities6) && $link->exec($sql_insert_cities7) && $link->exec($sql_insert_cities8) && $link->exec($sql_insert_cities9) && $link->exec($sql_insert_cities10) && $link->exec($sql_insert_identity_document_type) && $link->exec($sql_insert_theme_color_setting) && $link->exec($sql_insert_membership_plan) && $link->exec($sql_insert_maintainance_mode) && $link->exec($sql_insert_robots_txt) && $link->exec($sql_insert_membercategory))
					{
						echo "success";
						exit;
					}
					else
					{
						echo "Error creating admin info database";
						exit;
					}
				}

				
				
			}
		}
		else
		{
			echo "Admin password & confirm password not match. Try again";
		}

		
	}
?>