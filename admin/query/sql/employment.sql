INSERT INTO `employment` (`id`, `name`, `created_at`, `status`) VALUES
(1, 'Not Working', '2018-06-04 09:55:16', '1'),
(2, 'Search For Job', '2018-06-04 06:22:55', '1'),
(3, 'Self Employed Business', '2018-06-04 06:23:00', '1'),
(4, 'Self Employed Professional', '2018-06-04 06:23:04', '1'),
(5, 'Service in Central Government', '2018-06-04 06:23:08', '1'),
(6, 'Service In IT Company', '2018-06-04 06:23:12', '1'),
(7, 'Service in Local Corporation', '2018-06-04 06:23:15', '1'),
(8, 'Service in Multi National Company', '2018-12-27 12:03:08', '1'),
(9, 'Service in Private Sector', '2018-06-04 06:23:24', '1'),
(10, 'Service in Public Sector', '2018-06-04 06:23:33', '1'),
(11, 'Service in State Government', '2018-06-04 06:23:37', '1'),
(12, 'Working in Foreign', '2018-07-16 15:08:41', '1');