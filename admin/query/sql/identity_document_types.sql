INSERT INTO `identity_document_types` (`id`, `name`, `status`, `createdUpdatedBy`, `createdOn`, `updatedOn`) VALUES
(1, 'Aadhar Card', '1', 1, now(), NULL),
(2, 'PAN Card', '1', 1, now(), NULL),
(3, 'Passport', '1', 1, now(), NULL),
(4, 'Driving Licence', '1', 1, now(), NULL),
(5, 'Voting Card', '1', 1, now(), NULL);