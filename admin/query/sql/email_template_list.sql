INSERT INTO `email_template_list` (`id`, `name`) VALUES
(1, 'Email Verification Template'),
(2, 'Forgot Password'),
(3, 'Login Information'),
(4, 'Password Reset'),
(5, 'Request Photo');