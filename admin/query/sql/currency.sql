INSERT INTO `currency` (`id`, `country`, `currency`, `code`, `symbol`, `status`) VALUES
(1, 'Afghanistan', 'Afghanistani Afghani', 'AFN', '?', '1'),
(2, 'Armenia', 'Armenian Dram', 'AMD', '??', '1'),
(3, 'Azerbaijan', 'Azerbaijani Manat', 'AZN', '?', '1'),
(4, 'Bahrain', 'Bahraini Dinar', 'BHD', '.?.?', '1'),
(5, 'Bangladesh', 'Bangladeshi Taka', 'BDT', '?', '1'),
(6, 'Brunei', 'Brunei Dollar', 'BND', '$', '1'),
(7, 'Cambodia', 'Cambodian Riel', 'KHR', '?', '1'),
(8, 'China', 'Chinese Yuan Renminbi', 'CNY', '?', '1'),
(9, 'Cyprus', 'Cypriot Pound', 'CYP', '?', '1'),
(10, 'Georgia', 'Georgian Lari', 'GEL', '?', '1'),
(11, 'India', 'Indian Rupee', 'INR', '?', '1'),
(12, 'Indonesia', 'Indonesian Rupiah', 'IDR', 'Rp', '1'),
(13, 'Iran', 'Iranian Rial', 'IRR', '?', '1'),
(14, 'Iraq', 'Iraqi Dinar', 'IQD', '?.?', '1'),
(15, 'Israel', 'Israeli New Sheqel', 'ILS', '?', '1'),
(16, 'Jordan', 'Jordanian Dinar', 'JOD', '?.?', '1'),
(17, 'Kazakhstan', 'Kazakhstani Tenge', 'KZT', '??', '1'),
(18, 'Kuwait', 'Kuwaiti Dinar', 'KWD', '?.?', '1'),
(19, 'Kyrgyzstan', 'Kyrgyzstani Som', 'KGS', '??', '1'),
(20, 'Laos', 'Lao Kip', 'LAK', '?', '1'),
(21, 'Malaysia', 'Malaysian Ringgit', 'MYR', 'RM', '1'),
(22, 'Maldives', 'Maldives Rufiyaa', 'MVR', 'Rf', '1'),
(23, 'Mongolia', 'Mongolian Tugrik', 'MNT', '?', '1'),
(24, 'Oman', 'Omani Rial', 'OMR', '?', '1'),
(25, 'Pakistan', 'Pakistan Rupee', 'PKR', '?', '1'),
(26, 'Palestine', 'Jordanian Dinar', 'JOD', '?.?', '1'),
(27, 'Philippines', 'Philippine Peso', 'PHP', '?', '1'),
(28, 'Qatar', 'Qatari Riyal', 'QAR', '?', '1'),
(29, 'Russia', 'Russian Ruble', 'RUB', '?', '1'),
(30, 'Saudi Arabia', 'Saudi Arabian Riyal', 'SAR', '?', '1'),
(31, 'Singapore', 'Singapore Dollar', 'SGD', '$', '1'),
(32, 'Sri Lanka', 'Sri Lankan Rupee', 'LKR', '?', '1'),
(33, 'Syria', 'Syrian Pound', 'SYP', '?', '1'),
(34, 'Taiwan', 'New Taiwan Dollar', 'TWD', 'NT$', '1'),
(35, 'Tajikistan', 'Tajikistan Somoni', 'TJS', '?M', '1'),
(36, 'Thailand', 'Thai Baht', 'THB', '?', '1'),
(37, 'Timor-Leste', 'United States Dollar', 'USD', '$', '1'),
(38, 'Turkey', 'Turkish New Lira', 'TRY', '?', '1'),
(39, 'Turkmenistan', 'Turkmenistani Manat', 'TMM', 'T', '1'),
(40, 'United Arab Emirates (UAE)', 'United Arab Emirates Dirham', 'AED', '?.?', '1'),
(41, 'Uzbekistan', 'Uzbekistani Som', 'UZS', '??', '1'),
(42, 'Vietnam', 'Viet Nam Dong', 'VND', '?', '1'),
(43, 'Yemen', 'Yemeni Rial', 'YER', '?', '1'),
(44, 'Algeria', 'Algerian Dinar', 'DZD', '??', '1'),
(45, 'Benin', 'West African CFA', 'XOF', 'CFA', '1'),
(46, 'Burkina Faso', 'West African CFA', 'XOF', 'CFA', '1'),
(47, 'Burundi', 'Burundian Franc', 'BIF', 'FBu', '1'),
(48, 'Cameroon', 'Central African CFA', 'XAF', 'FCFA', '1'),
(49, 'Central African Republic (CAR)', 'Central African CFA', 'XAF', 'FCFA', '1'),
(50, 'Chad', 'Central African CFA', 'XAF', 'FCFA', '1'),
(51, 'Comoros', 'Comorian Franc', 'KMF', 'CF', '1'),
(52, 'Democratic Republic of the Congo', 'Congolese franc', 'CDF', 'FC', '1'),
(53, 'Republic of the Congo', 'Central African CFA', 'XAF', 'FCFA', '1'),
(54, 'Cote d\'Ivoire', 'West African CFA', 'XOF', 'CFA', '1'),
(55, 'Djibouti', 'Djiboutian Franc', 'DJF', 'Fdj', '1'),
(56, 'Egypt', 'Egyptian Pound', 'EGP', '?', '1'),
(57, 'Equatorial Guinea', 'Central African CFA', 'XAF', 'FCFA', '1'),
(58, 'Eritrea', 'Eritrean Nakfa', 'ERN', '???', '1'),
(59, 'Ethiopia', 'Ethiopian Birr', 'ETB', '??', '1'),
(60, 'Gabon', 'Central African CFA', 'XAF', 'FCFA', '1'),
(61, 'Gambia', 'Gambian Dalasi', 'GMD', 'D', '1'),
(62, 'Ghana', 'Ghanaian Cedi', 'GHC', 'GH?', '1'),
(63, 'Guinea', 'Guinean Franc', 'GNF', 'FG', '1'),
(64, 'Guinea-Bissau', 'West African CFA', 'XOF', 'CFA', '1'),
(65, 'Kenya', 'Kenyan Shilling', 'KES', 'KSh,', '1'),
(66, 'Lesotho', 'Lesotho Loti', 'LSL', 'L', '1'),
(67, 'Liberia', 'Liberian Dollar', 'LRD', '$', '1'),
(68, 'Libya', 'Libyan Dinar', 'LYD', '?.?', '1'),
(69, 'Madagascar', 'Malagasy Ariary', 'MGA', 'Ar', '1'),
(70, 'Malawi', 'Malawian Kwacha', 'MWK', 'MK', '1'),
(71, 'Mali', 'West African CFA', 'XOF', 'CFA', '1'),
(72, 'Mauritania', 'Mauritanian Ouguiya', 'MRO', 'UM', '1'),
(73, 'Mauritius', 'Mauritian Rupee', 'MUR', '?', '1'),
(74, 'Morocco', 'Moroccan Dirham', 'MAD', 'DH', '1'),
(75, 'Mozambique', 'Mozambican Metical', 'MZN', 'MT', '1'),
(76, 'Namibia', 'Namibian Dollar', 'NAD', '$', '1'),
(77, 'Niger', 'West African CFA', 'XOF', 'CFA', '1'),
(78, 'Nigeria', 'Nigerian Naira', 'NGN', '?', '1'),
(79, 'Sao Tome and Principe', 'Sao Tome Dobra', 'STD', 'Db', '1'),
(80, 'Senegal', 'West African CFA', 'XOF', 'CFA', '1'),
(81, 'Sierra Leone', 'Sierra Leonean Leone', 'SLL', 'Le', '1'),
(82, 'Somalia', 'Somali Shilling', 'SOS', 'S', '1'),
(83, 'South Africa', 'South African Rand', 'ZAR', 'R', '1'),
(84, 'Sudan', 'Sudanese Dinar', 'SDD', '?Sd', '1'),
(85, 'Swaziland', 'Swazi Lilangeni', 'SZL', 'E', '1'),
(86, 'Tanzania', 'Tanzanian Shilling', 'TZS', 'TSh', '1'),
(87, 'Togo', 'West African CFA', 'XOF', 'CFA', '1'),
(88, 'Tunisia', 'Tunisian Dinar', 'TND', '?.?', '1'),
(89, 'Uganda', 'Ugandan Shilling', 'UGX', 'USh', '1'),
(90, 'Zambia', 'Zambian Kwacha', 'ZMK', 'ZK', '1'),
(91, 'Zimbabwe', 'Zimbabwean Dollar', 'ZWD', '$', '1'),
(92, 'Albania', 'Albanian Lek', 'ALL', 'Lek', '1'),
(93, 'Armenia', 'Armenian Dram', 'AMD', '??.', '1'),
(94, 'Austria', 'European Euro', 'EUR', '?', '1'),
(95, 'Azerbaijan', 'Azerbaijani Manat', 'AZN', '?', '1'),
(96, 'Belarus', 'Belarusian Ruble', 'BYR', 'Br', '1'),
(97, 'Belgium', 'European Euro', 'EUR', '?', '1'),
(98, 'Bosnia and Herzegovina', 'Bosnia-Herzegovina Convertible Mark', 'BAM', 'KM', '1'),
(99, 'Bulgaria', 'Bulgarian Lev', 'BGN', '??', '1'),
(100, 'Croatia', 'Croatian Kuna', 'HRK', 'kn', '1'),
(101, 'Cyprus', 'Cypriot Pound', 'CYP', '?', '1'),
(102, 'Czech Republic', 'Czech Koruna', 'CZK', 'K?', '1'),
(103, 'Denmark', 'Danish Krone', 'DKK', 'kr.', '1'),
(104, 'Estonia', 'Estonian Kroon', 'EEK', 'EEK', '1'),
(105, 'Finland', 'European Euro', 'EUR', '?', '1'),
(106, 'Georgia', 'Georgian Lari', 'GEL', '?', '1'),
(107, 'Hungary', 'Hungarian Forint', 'HUF', 'Ft', '1'),
(108, 'Iceland', 'Icelandic Krona', 'ISK', 'kr', '1'),
(109, 'Ireland', 'European Euro', 'EUR', '?', '1'),
(110, 'Italy', 'European Euro', 'EUR', '?', '1'),
(111, 'Kazakhstan', 'Kazakhstani Tenge', 'KZT', '??', '1'),
(112, 'Latvia', 'Latvian Lats', 'LVL', 'Ls', '1'),
(113, 'Liechtenstein', 'Swiss Franc', 'CHF', 'CHF', '1'),
(114, 'Lithuania', 'Lithuanian Litas', 'LTL', 'Lt', '1'),
(115, 'Macedonia (FYROM)', 'Macedonian Denar', 'MKD', '???', '1'),
(116, 'Malta', 'Maltese Lira', 'MTL', '?', '1'),
(117, 'Norway', 'Norwegian Krone', 'NOK', 'kr', '1'),
(118, 'Poland', 'Polish Zloty', 'PLN', 'z?', '1'),
(119, 'Romania', 'Romanian Leu', 'RON', 'lei', '1'),
(120, 'Russia', 'Russian Ruble', 'RUB', '?', '1'),
(121, 'San Marino', 'European Euro', 'EUR', '?', '1'),
(122, 'Serbia', 'Serbian Dinar', 'RSD', '???.', '1'),
(123, 'Slovakia', 'Slovak Koruna', 'SKK', 'Sk', '1'),
(124, 'Slovenia', 'European Euro', 'EUR', '?', '1'),
(125, 'Spain', 'European Euro', 'EUR', '?', '1'),
(126, 'Sweden', 'Swedish Krona', 'SEK', 'kr', '1'),
(127, 'Switzerland', 'Swiss Franc', 'CHF', 'CHF', '1'),
(128, 'Turkey', 'Turkish New Lira', 'TRY', '?', '1'),
(129, 'Ukraine', 'Ukrainian Hryvnia', 'UAH', '?', '1'),
(130, 'United Kingdom (UK)', 'United Kingdom Pound Sterling', 'GBP', '?', '1'),
(131, 'Vatican City (Holy See)', 'European Euro', 'EUR', '?', '1'),
(132, 'Antigua and Barbuda', 'East Caribbean Dollar', 'XCD', '$', '1'),
(133, 'Bahamas', 'Bahamian Dollar', 'BSD', '$', '1'),
(134, 'Belize', 'Belize Dollar', 'BZD', 'BZ$', '1'),
(135, 'Canada', 'Canadian Dollar', 'CAD', '$', '1'),
(136, 'Costa Rica', 'Costa Rican Colon', 'CRC', '?', '1'),
(137, 'Cuba', 'Cuban Convertible Peso', 'CUC', '$', '1'),
(138, 'Dominica', 'East Caribbean Dollar', 'XCD', '$', '1'),
(139, 'Dominican Republic', 'Dominican Peso', 'DOP', 'RD$', '1'),
(140, 'El Salvador', 'United States Dollar', 'USD', '$', '1'),
(141, 'Grenada', 'East Caribbean Dollar', 'XCD', '$', '1'),
(142, 'Haiti', 'Haitian Gourde', 'HTG', 'G', '1'),
(143, 'Honduras', 'Honduran Lempira', 'HNL', 'L', '1'),
(144, 'Jamaica', 'Jamaican Dollar', 'JMD', 'J$', '1'),
(145, 'Mexico', 'Mexican Peso', 'MXN', '$', '1'),
(146, 'Nicaragua', 'Nicaraguan C?rdoba', 'NIO', 'C$', '1'),
(147, 'Panama', 'Panamanian Balboa', 'PAB', 'B/.', '1'),
(148, 'Saint Kitts and Nevis', 'East Caribbean Dollar', 'XCD', '$', '1'),
(149, 'Saint Lucia', 'East Caribbean Dollar', 'XCD', '$', '1'),
(150, 'Saint Vincent and the Grenadines', 'East Caribbean Dollar', 'XCD', '$', '1'),
(151, 'Trinidad and Tobago', 'Trinidad and Tobago Dollar', 'TTD', 'TT$', '1'),
(152, 'United States of America (USA)', 'United States Dollar', 'USD', '$', '1'),
(153, 'Argentina', 'Argentine Peso', 'ARS', '$', '1'),
(154, 'Bolivia', 'Bolivian Boliviano', 'BOB', '$b', '1'),
(155, 'Brazil', 'Brazilian Real', 'BRL', 'R$', '1'),
(156, 'Chile', 'Chilean Peso', 'CLP', '$', '1'),
(157, 'Colombia', 'Colombian Peso', 'COP', '$', '1'),
(158, 'Ecuador', 'United States Dollar', 'USD', '$', '1'),
(159, 'Paraguay', 'Paraguay Guarani', 'PYG', 'Gs', '1'),
(160, 'Peru', 'Peruvian Nuevo Sol', 'PEN', 'S/.', '1'),
(161, 'Suriname', 'Suriname Dollar', 'SRD', '$', '1'),
(162, 'Venezuela', 'Venezuelan Bolivar', 'VEB', 'Bs.F', '1'),
(163, 'Australia', 'Australian Dollar', 'AUD', '$', '1'),
(164, 'Fiji', 'Fiji Dollar', 'FJD', '$', '1'),
(165, 'Kiribati', 'Australian Dollar', 'AUD', '$', '1'),
(166, 'Marshall Islands', 'United States Dollar', 'USD', '$', '1'),
(167, 'Micronesia', 'United States Dollar', 'USD', '$', '1'),
(168, 'Nauru', 'Australian Dollar', 'AUD', '$', '1'),
(169, 'Palau', 'United States Dollar', 'USD', '$', '1'),
(170, 'Papua New Guinea', 'Papua New Guinea Kina', 'PGK', 'K', '1'),
(171, 'Solomon Islands', 'Solomon Islands Dollar', 'SBD', '$', '1'),
(172, 'Tuvalu', 'Australian Dollar', 'AUD', '$', '1');