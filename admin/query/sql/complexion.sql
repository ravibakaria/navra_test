INSERT INTO `complexion` (`id`, `name`, `status`) VALUES
(1, 'Light Or Pale White', '1'),
(2, 'White Or Fair', '1'),
(3, 'Medium Or Light Brown', '1'),
(4, 'Wheatish Or Moderate Brown', '1'),
(5, 'Brown Or Dark Brown', '1'),
(6, 'Very Dark Or Black', '1');