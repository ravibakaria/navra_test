INSERT INTO `specialcases` (`id`, `name`, `created_at`, `status`) VALUES
(1, 'HIV Positive', '2018-06-04 10:10:50', '1'),
(2, 'Thalassemia Major', '2018-06-04 06:40:59', '1'),
(3, 'Hearing Impaired', '2018-06-04 06:41:03', '1'),
(4, 'Speech Impaired', '2018-06-04 06:41:09', '1'),
(5, 'Visually Impaired', '2018-06-04 06:41:13', '1'),
(6, 'Handicapped', '2018-06-04 06:41:17', '1'),
(7, 'Cancer Survivor', '2018-06-04 06:41:21', '1'),
(8, 'Diabetic', '2018-06-04 06:41:25', '1'),
(9, 'Leucoderma', '2018-12-26 12:35:59', '1'),
(10, 'Manglik', '2018-12-26 12:37:30', '1'),
(11, 'Partially Manglik', '2018-12-21 08:27:18', '1'),
(12, 'None of the above', '2019-01-11 05:20:43', '1');