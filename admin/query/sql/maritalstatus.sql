INSERT INTO `maritalstatus` (`id`, `name`, `status`) VALUES
(1, 'Never Married', '1'),
(2, 'Awaiting Divorce', '1'),
(3, 'Divorced', '1'),
(4, 'Widowed', '1'),
(5, 'Annulled', '1');