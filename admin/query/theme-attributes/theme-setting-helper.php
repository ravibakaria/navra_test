<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   
	include('../../../config/setup-values.php');   //setup values    
	include('../../../config/email/email_process.php');   //email function
	
	$basepath = $WebSiteBasePath.'/';

    $task = quote_smart($_POST['task']);

	if($task == 'update_master_css')
	{
		$css_data = $_POST['css_data'];

		$file = "../../../css/master.css"; // cannot be an online resource
		file_put_contents($file, $css_data);

		echo json_encode("success");
		exit;
	}
?>