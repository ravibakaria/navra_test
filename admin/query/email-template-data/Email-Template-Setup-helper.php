<?php
	session_start();

	if(!isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']=='' || $_SESSION['admin_user']==null))
	{
		header('Location: login.php');
		exit;
	}

	$admin_id = $_SESSION['admin_id'];

	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   
	include('../../../config/setup-values.php');   //setup values    
	include('../../../config/email/email_process.php');   //email function
	
	$today_datetime = date('Y-m-d H:i:s');
	
    $basepath = $WebSiteBasePath.'/';

    $task = quote_smart($_POST['task']);

    if($task == "Add-New-Email-Template")
    {
    	$emailTemplateName = $_POST['emailTemplateName'];
    	$emailTemplateSubject = $_POST['emailTemplateSubject'];
    	$emailTemplateMessage = $_POST['emailTemplateMessage'];

    	$sql_chk = "SELECT * FROM `emailtemplates` WHERE LOWER(`emailTemplateName`)='".strtolower($emailTemplateName)."'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();

        if($count>0)
        {
        	echo json_encode("Email template with <strong>$emailTemplateName</strong> name already exisits in records.<br/> Please use another template name.");
			exit;
        }

        $sql_insert = "INSERT INTO `emailtemplates`(`emailTemplateName`, `emailTemplateSubject`, `emailTemplateMessage`,`updatedBy`, `updatedOn`) VALUES ('$emailTemplateName','$emailTemplateSubject','$emailTemplateMessage','$admin_id','$today_datetime')";

        if($link->exec($sql_insert))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo "Something went wrong! Please try after some time.";
			exit;
		}
    }

    if($task=='Update_Template_Data')
	{
		$emailTemplateName = $_POST['emailTemplateName'];
		$emailTemplateSubject = $_POST['emailTemplateSubject'];
		$emailTemplateMessage = $_POST['emailTemplateMessage'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `emailtemplates` WHERE `id`<>'$id' AND LOWER(`emailTemplateName`)='".strtolower($emailTemplateName)."' ";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();

        if($count>0)
        {
        	echo json_encode("Email template with <strong>$emailTemplateName</strong> name already exisits in records.<br/> Please use another template name.");
			exit;
        }

		$sql_chk = "SELECT * FROM `emailtemplates` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_emailTemplateName = $result['emailTemplateName'];
        $db_emailTemplateSubject = $result['emailTemplateSubject'];
        $db_emailTemplateMessage = $result['emailTemplateMessage'];

        if($db_emailTemplateName==$emailTemplateName && $db_emailTemplateSubject==$emailTemplateSubject && $db_emailTemplateMessage==$emailTemplateMessage)
        {
        	echo json_encode("Values same as before.");
			exit;
        }

		$sql = "UPDATE `emailtemplates` SET `emailTemplateName`='$emailTemplateName',`emailTemplateSubject`='$emailTemplateSubject',`emailTemplateMessage`='$emailTemplateMessage' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo json_encode("success");
			exit;
		}
		else
		{
			echo json_encode("Something went wrong. Try after some time.");
			exit;
		}
	}
?>