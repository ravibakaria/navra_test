<?php
	session_start();
	require_once '../../../../config/config.php'; 
	include('../../../../config/dbconnect.php');    //database connection
	include('../../../../config/functions.php');   //strip query string
	include('../../../../config/setup-values.php');   //setup values

	$basepath = $WebSiteBasePath.'/';
    
    $task = quote_smart($_POST['task']);

	if($task == 'Add_New_Designation')
	{
		$name = $_POST['name'];
		$designation_category = $_POST['designation_category'];
		$status = $_POST['status'];
		$sql_chk = "SELECT * FROM `designation` WHERE LOWER(`name`)='".strtolower($name)."' AND `designation_category`='$designation_category'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        
       	if($count>0)
        {
        	echo "Designation already exisits in records. Please use another Designation.";
			exit;
        }
		
		$sql_insert = "INSERT INTO `designation`(`name`,`designation_category`,`status`) VALUES('$name','$designation_category','$status')";

		 if($link->exec($sql_insert))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}       
	}

	if($task == "Change_Designation_status")
	{
		$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `designation` SET `status`='$updated_status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}

	if($task=='Update_Designation_Attributes')
	{
		$name = $_POST['name'];
		$designation_category = $_POST['designation_category'];
		$status = $_POST['status'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `designation` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_name = $result['name'];
        $db_designation_category = $result['designation_category'];
        $db_status = $result['status'];

        if($db_name==$name && $db_designation_category==$designation_category && $db_status==$status)
        {
        	echo "Data already updated.";
			exit;
        }

		$sql = "UPDATE `designation` SET `name`='$name',`designation_category`='$designation_category',`status`='$status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>