<?php
	session_start();
	require_once '../../../../config/config.php'; 
	include('../../../../config/dbconnect.php');    //database connection
	include('../../../../config/functions.php');   //strip query string
	include('../../../../config/setup-values.php');   //setup values

	$basepath = $WebSiteBasePath.'/';
    
    $task = quote_smart($_POST['task']);

	if($task == 'Add_New_Currency')
	{
		$country = $_POST['country'];
		$currency = $_POST['currency'];
		$code = $_POST['code'];
		$status = $_POST['status'];
		$sql_chk = "SELECT * FROM `currency` WHERE LOWER(`country`)='".strtolower($country)."' AND LOWER(`currency`)='".strtolower($currency)."' AND LOWER(`code`)='".strtolower($code)."'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        
       	if($count>0)
        {
        	echo "Currency already exisits in records. Please use another Currency.";
			exit;
        }
		
		$sql_insert = "INSERT INTO `currency`(`country`,`currency`,`code`,`status`) VALUES('$country','$currency','$code','$status')";

		 if($link->exec($sql_insert))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}       
	}

	if($task == "Change_Currency_status")
	{
		$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `currency` SET `status`='$updated_status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}

	if($task=='Update_Currency_Attributes')
	{
		$country = $_POST['country'];
		$currency = $_POST['currency'];
		$code = $_POST['code'];
		$status = $_POST['status'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `currency` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_country = $result['country'];
		$db_currency = $result['currency'];
		$db_code = $result['code'];
		$db_status = $result['status'];

        if($db_country==$country && $db_currency==$currency && $db_code==$code && $db_status==$status)
        {
        	echo "Data already updated.";
			exit;
        }

		$sql = "UPDATE `currency` SET `country`='$country',`currency`='$currency',`code`='$code',`status`='$status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>