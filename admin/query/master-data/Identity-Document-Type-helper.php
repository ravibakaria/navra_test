<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
	include('../../../config/setup-values.php');   //strip query string
	
	$today_datetime = date('Y-m-d H:i:s');
	
	$admin_id = $_SESSION['admin_id'];
    $basepath = $WebSiteBasePath.'/';
    $task = quote_smart($_POST['task']);

	if($task == 'Add_New_Document')
	{
		$name = $_POST['name'];
		$status = $_POST['status'];
		$sql_chk = "SELECT * FROM `identity_document_types` WHERE LOWER(`name`)='".strtolower($name)."'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        
       	if($count>0)
        {
        	echo "Document type already exisits in records. Please use another document type.";
			exit;
        }
		
		$sql_insert = "INSERT INTO `identity_document_types`(`name`,`status`,`createdUpdatedBy`,`createdOn`) VALUES('$name','$status','$admin_id','$today_datetime')";

		 if($link->exec($sql_insert))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}       
	}

	if($task == "Change_Document_Type_status")
	{
		$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `identity_document_types` SET `status`='$updated_status',`createdUpdatedBy`='$admin_id',`updatedOn`='$today_datetime' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}

	if($task=='Update_Document_Type_Attributes')
	{
		$name = $_POST['name'];
		$status = $_POST['status'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `identity_document_types` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_name = $result['name'];
        $db_status = $result['status'];

        if($db_name==$name && $db_status==$status)
        {
        	echo "Data already updated.";
			exit;
        }

		$sql = "UPDATE `identity_document_types` SET `name`='$name',`status`='$status',`createdUpdatedBy`='$admin_id',`updatedOn`='$today_datetime' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>