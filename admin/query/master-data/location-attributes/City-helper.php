<?php
	session_start();
	require_once '../../../../config/config.php'; 
	include('../../../../config/dbconnect.php');    //database connection
	include('../../../../config/functions.php');   //strip query string
	include('../../../../config/setup-values.php');   //setup values

    $basepath = $WebSiteBasePath.'/';
    
    $task = quote_smart($_POST['task']);

    if($task=='Fetch_States_Of_Country')
    {
    	$country_id=$_POST['country_id'];
    	$output = null;
    	$sql = "SELECT * FROM `states` WHERE `country_id`='$country_id' AND `status`='1'";
    	$stmt = $link->prepare($sql);
    	$stmt->execute();
    	$result = $stmt->fetchAll();

    	$output = "<option value='0'>--Select State--</option>";
    	foreach ($result as $row) 
    	{
    		$state_id = $row['id'];
    		$state_name = $row['name'];
    		$output .= "<option value='".$state_id."'>".$state_name."</option>";
    	}
    	echo $output;
    }


	if($task == 'Add_New_City')
	{
		$state_id = $_POST['state_id'];
		$name = $_POST['name'];
		
		$sql_chk = "SELECT * FROM `cities` WHERE LOWER(`state_id`)='".strtolower($state_id)."' AND LOWER(`name`)='".strtolower($name)."'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        
       	if($count>0)
        {
        	echo "City already exisits in records. Please use another City.";
			exit;
        }
		
		$sql_insert = "INSERT INTO `cities`(`name`,`state_id`) VALUES('$name','$state_id')";

		 if($link->exec($sql_insert))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}       
	}

	if($task == "Change_City_status")
	{
		$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `cities` SET `status`='$updated_status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}

	if($task=='Update_State_Attributes')
	{
		$state_id = $_POST['state_id'];
		$name = $_POST['name'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `cities` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_state_id = $result['state_id'];
        $db_name = $result['name'];
        
        if($db_state_id==$state_id && $db_name==$name)
        {
        	echo "Data already updated.";
			exit;
        }

		$sql = "UPDATE `cities` SET `name`='$name',`state_id`='$state_id' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>