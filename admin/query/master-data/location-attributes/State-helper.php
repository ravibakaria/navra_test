<?php
	session_start();
	require_once '../../../../config/config.php'; 
	include('../../../../config/dbconnect.php');    //database connection
	include('../../../../config/functions.php');   //strip query string
	include('../../../../config/setup-values.php');   //setup values

	$basepath = $WebSiteBasePath.'/';

    $task = quote_smart($_POST['task']);

	if($task == 'Add_New_State')
	{
		$country_id = $_POST['country_id'];
		$name = $_POST['name'];
		
		$sql_chk = "SELECT * FROM `contries` WHERE LOWER(`country_id`)='".strtolower($country_id)."' AND LOWER(`name`)='".strtolower($name)."'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        
       	if($count>0)
        {
        	echo "State already exisits in records. Please use another State.";
			exit;
        }
		
		$sql_insert = "INSERT INTO `states`(`name`,`country_id`) VALUES('$name','$country_id')";

		 if($link->exec($sql_insert))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}       
	}

	if($task == "Change_State_status")
	{
		$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `states` SET `status`='$updated_status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}

	if($task=='Update_State_Attributes')
	{
		$country_id = $_POST['country_id'];
		$name = $_POST['name'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `states` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_country_id = $result['country_id'];
        $db_name = $result['name'];

        if($db_country_id==$country_id && $db_name==$name)
        {
        	echo "Data already updated.";
			exit;
        }

		$sql = "UPDATE `states` SET `name`='$name',`country_id`='$country_id' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>