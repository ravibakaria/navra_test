<?php
	session_start();
	require_once '../../../../config/config.php'; 
	include('../../../../config/dbconnect.php');    //database connection
	include('../../../../config/functions.php');   //strip query string
	include('../../../../config/setup-values.php');   //setup values

	$basepath = $WebSiteBasePath.'/';
    
    $task = quote_smart($_POST['task']);

	if($task == 'Add_New_Country')
	{
		$sortname = $_POST['sortname'];
		$name = $_POST['name'];
		$phonecode = $_POST['phonecode'];

		$sql_chk = "SELECT * FROM `contries` WHERE LOWER(`sortname`)='".strtolower($sortname)."' AND LOWER(`name`)='".strtolower($name)."' AND `phonecode`='$phonecode)'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        
       	if($count>0)
        {
        	echo "Country already exisits in records. Please use another Country.";
			exit;
        }
		
		$sql_insert = "INSERT INTO `countries`(`sortname`,`name`,`phonecode`) VALUES('$sortname','$name','$phonecode')";

		 if($link->exec($sql_insert))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}       
	}

	if($task == "Change_Country_status")
	{
		$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `countries` SET `status`='$updated_status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}

	if($task=='Update_Country_Attributes')
	{
		$sortname = $_POST['sortname'];
		$name = $_POST['name'];
		$phonecode = $_POST['phonecode'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `countries` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_sortname = $result['sortname'];
        $db_name = $result['name'];
        $db_phonecode = $result['phonecode'];

        if($db_sortname==$sortname && $db_name==$name && $db_phonecode==$phonecode)
        {
        	echo "Data already updated.";
			exit;
        }

		$sql = "UPDATE `countries` SET `sortname`='$sortname',`name`='$name',`phonecode`='$phonecode' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>