<?php
	session_start();
	require_once '../../../../config/config.php'; 
	include('../../../../config/dbconnect.php');    //database connection
	include('../../../../config/functions.php');   //strip query string
	include('../../../../config/setup-values.php');   //setup values

    $basepath = $WebSiteBasePath.'/';
    $task = quote_smart($_POST['task']);

	if($task == 'Add_New_FamilyType')
	{
		$name = $_POST['name'];
		$status = $_POST['status'];
		$sql_chk = "SELECT * FROM `familytype` WHERE LOWER(`name`)='".strtolower($name)."'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        
       	if($count>0)
        {
        	echo "FamilyType already exisits in records. Please use another FamilyType.";
			exit;
        }
		
		$sql_insert = "INSERT INTO `familytype`(`name`,`status`) VALUES('$name','$status')";

		 if($link->exec($sql_insert))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}       
	}

	if($task == "Change_FamilyType_status")
	{
		$status = $_POST['status'];
		$id = $_POST['id'];
		$updated_status = null;

		if($status=='0')
		{
			$updated_status = '1';
		}
		else
		if($status=='1')
		{
			$updated_status = '0';
		}

		$sql = "UPDATE `familytype` SET `status`='$updated_status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}

	if($task=='Update_FamilyType_Attributes')
	{
		$name = $_POST['name'];
		$status = $_POST['status'];
		$id = $_POST['id'];

		$sql_chk = "SELECT * FROM `familytype` WHERE `id`='$id'";
		$stmt   = $link->prepare($sql_chk);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetch();

        $db_name = $result['name'];
        $db_status = $result['status'];

        if($db_name==$name && $db_status==$status)
        {
        	echo "Data already updated.";
			exit;
        }

		$sql = "UPDATE `familytype` SET `name`='$name',`status`='$status' WHERE `id`='$id'";

		if($link->exec($sql))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>