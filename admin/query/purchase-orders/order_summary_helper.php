<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   
	include('../../../config/setup-values.php');   //get master setup values
    include('../../../config/email/email_style.php');   //get master setup values
    include('../../../config/email/email_templates.php');   //get master setup values
    include('../../../config/email/email_process.php');

	$admin_id = $_SESSION['admin_id'];

    $today_datetime = date('Y-m-d H:i:s');
    
    $task = quote_smart($_POST['task']);

    $WebSiteBasePath = getWebsiteBasePath();
    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }

    /************   Update payment status   *****************/
	if($task == "Update-Payment-Status")
	{
		$transaction_date = $_POST['transaction_date'];
		$payment_amount = $_POST['payment_amount'];
		$payment_method = $_POST['payment_method'];
		$transaction_id = $_POST['transaction_id'];
		$payment_status = $_POST['payment_status'];
		$confirm_email = $_POST['confirm_email'];
		$payment_remarks = $_POST['payment_remarks'];
		$order_id = $_POST['order_id'];
		$payment_gateway = $_POST['payment_gateway'];
		$request_id = $_POST['request_id'];
		$userid = $_POST['userid'];

		if($request_id=='' || $request_id==null)
		{
			$request_id = $transaction_id;
		}

		$today = date('Y-m-d');

		$sql = "SELECT * FROM payment_transactions WHERE OrderNumber='$order_id' AND userid='$userid'";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        $membership_plan = $result['membership_plan'];
        $membership_plan_name = $result['membership_plan_name'];
        $featured_listing = $result['featured_listing'];
        $package_month = $result['tenure'];
        $Order_Date = $result['created_at'];
        $previous_status = $result['status'];
        $previous_amount_payable = $result['total_amount'];

		if($payment_status=='1')
		{
			if($membership_plan=='Yes')
            {
                $sql_chk_expiry = "SELECT * FROM payment_transactions where userid = '$userid' AND LOWER(membership_plan_name)=LOWER('$membership_plan_name') AND membership_plan_expiry_date>'$today' AND status='1' ORDER BY id DESC";
                $stmt_chk_expiry = $link->prepare($sql_chk_expiry);
                $stmt_chk_expiry->execute();
                $count_chk_expiry = $stmt_chk_expiry->rowCount();
                
                if($count_chk_expiry>0)
                {
                    $result_chk_expiry = $stmt_chk_expiry->fetch();
                    $plan_date = $result_chk_expiry['membership_plan_expiry_date'];
                }
                else
                {
                    $plan_date = $today;
                }
                
                $membership_plan_expiry_date = date('Y-m-d', strtotime("$plan_date +".$package_month." month"));                    
            }
            else
            {
                $membership_plan_expiry_date = '0000-00-00';
            }
            
            if($featured_listing=='Yes')
            {
                $sql_chk_expiry_fp = "SELECT * FROM payment_transactions where userid = '$userid' AND featured_listing_expiry_date>'$today' AND status='1' ORDER BY id DESC";
                $stmt_chk_expiry_fp = $link->prepare($sql_chk_expiry_fp);
                $stmt_chk_expiry_fp->execute();
                $count_chk_expiry_fp = $stmt_chk_expiry_fp->rowCount();
                
                if($count_chk_expiry_fp>0)
                {
                    $result_chk_expiry_fp = $stmt_chk_expiry_fp->fetch();
                    $plan_date_fp = $result_chk_expiry_fp['featured_listing_expiry_date'];
                }
                else
                {
                    $plan_date_fp = $today;
                }
                
                $featured_listing_expiry_date = date('Y-m-d', strtotime("$plan_date_fp +".$package_month." month"));
            }
            else
            {
                $featured_listing_expiry_date = '0000-00-00';
            }

            $sql_update_payment_status = "UPDATE payment_transactions SET membership_plan_expiry_date='$membership_plan_expiry_date',featured_listing_expiry_date='$featured_listing_expiry_date',transact_id='$transaction_id',request_id='$request_id',payment_gateway='$payment_gateway',payment_method='$payment_method',updated_at='$today_datetime',status='1' WHERE userid = '$userid' AND OrderNumber='$order_id'";
		}
		else
		if($payment_status=='2')
		{
			$sql_update_payment_status = "UPDATE payment_transactions SET membership_plan_expiry_date=NULL,featured_listing_expiry_date=NULL,updated_at='$today_datetime',status='2' WHERE userid = '$userid' AND OrderNumber='$order_id'";
		}
		else
		if($payment_status=='3')
		{
			$sql_update_payment_status = "UPDATE payment_transactions SET membership_plan_expiry_date=NULL,featured_listing_expiry_date=NULL,updated_at='$today_datetime',status='3' WHERE userid = '$userid' AND OrderNumber='$order_id'";
		}
		else
		if($payment_status=='0')
		{
			$sql_update_payment_status = "UPDATE payment_transactions SET membership_plan_expiry_date=NULL,featured_listing_expiry_date=NULL,updated_at='$today_datetime',status='0' WHERE userid = '$userid' AND OrderNumber='$order_id'";
		}
		
		if($link->exec($sql_update_payment_status))
        {
        	$sql_insert_update_status = "INSERT INTO payment_transactions_status_update(userid,OrderNumber,Order_Date,previous_status,previous_amount_payable,Order_update_date,updated_amount_payable,updated_payment_method,transaction_id,updated_status,confirmation_email_sent,payment_remarks,admin_id,updated_on,IP_ADDRESS) VALUES('$userid','$order_id','$Order_Date','$previous_status','$previous_amount_payable','$transaction_date','$payment_amount','$payment_method','$transaction_id','$payment_status','$confirm_email','$payment_remarks','$admin_id','$today_datetime','$IP_Address')";

        	$link->exec($sql_insert_update_status);

        	if($confirm_email=='1')
        	{
        		$first_name = getUserFirstName($userid);
        		$userEMail = getUserEmail($userid);

        		if($payment_status=='0')
        		{
        			$email_status = 'Unpaid';
        		}
        		else
        		if($payment_status=='1')
        		{
        			$email_status = 'Paid';
        		}
        		else
        		if($payment_status=='2')
        		{
        			$email_status = 'Canceled';
        		}
        		else
        		if($payment_status=='3')
        		{
        			$email_status = 'Refund';
        		}

        		$Order_update_date = date('d-m-Y');
        		
        		$member_orders_link = "Please visit <a href='$WebSiteBasePath/users/my-orders.php'>My Orders</a> page for more details.";

        		$SocialSharing = getSocialSharingLinks();   // social sharing links
                $primary_color = getPrimaryColor();
                $primary_font_color = getPrimaryFontColor();

                $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

                $PaymentStatusUpdateSubject = str_replace(array('$site_name','$order_id'),array($sitetitle,$order_id),$PaymentStatusUpdateSubject);  //replace header variables with value

                $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$sitetitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

                $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$sitetitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

                $PaymentStatusUpdateMessage = str_replace(array('$first_name','$order_id','$Order_update_date','$payment_amount','$payment_method','$transaction_id','$payment_status','$member_orders_link','$signature'),array($first_name,$order_id,$Order_update_date,$payment_amount,$payment_method,$transaction_id,$email_status,$member_orders_link,$GlobalEmailSignature),$PaymentStatusUpdateMessage);  //replace footer variables with value

                $subject = $PaymentStatusUpdateSubject;
                $message  = '<!DOCTYPE html>';
                $message .= '<html lang="en">
                    <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width">
                    <title></title>
                    <style type="text/css">'.$EmailCSS.'</style>
                    </head>
                    <body style="margin: 0; padding: 0;">';
                //echo $message;exit;
                $message .= $EmailGlobalHeader;

                $message .= $PaymentStatusUpdateMessage;

                $message .= $EmailGlobalFooter;

                $mailto = $userEMail;
                $mailtoname = $first_name;

                $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

                if($emailResponse == 'success')
                {
                	$sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$userid','payment status update','Order Number[$order_id] - $email_status','$today_datetime')";
                	$link->exec($sql_member_email_log);
                }
        	}

        	echo json_encode('success');
			exit;
        }
        else
        {
        	echo json_encode('Payment status updation failed');
			exit;
        }
	}
?>