<?php
	include("templates/header.php");
	if(isset($_GET['main_tab']) && isset($_GET['sub_tab']))
	{
		$main_tab=quote_smart($_GET['main_tab']);
		$sub_tab=quote_smart($_GET['sub_tab']);
	}
	else
	{
		$main_tab=null;
		$sub_tab=null;
	}
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
</head>
<body>
	<section role="main" class="content-body">
		
		<!-- start: page -->
			<div class="row admin_start_section">
				<h1>Payment Gateway</h1>
				<hr class="setting-devider"/>
				<ul class="nav data-tabs nav-tabs" role="tablist">
					<li class="<?php if(($main_tab=='PaymentGateway' || $main_tab==null) && ($sub_tab=='Offline' || $sub_tab==null)) { echo 'active';}?>"><a href="#Offline" role="tab" data-toggle="tab" class="tab-links">Offline </a></li>
					<li class="<?php if(($main_tab=='PaymentGateway') && ($sub_tab=='BankTransfer')) { echo 'active';}?>"><a href="#BankTransfer" role="tab" data-toggle="tab" class="tab-links">Bank Transfer </a></li>
				  	<li class="<?php if(($main_tab=='PaymentGateway') && ($sub_tab=='Instamojo')) { echo 'active';}?>"><a href="#Instamojo" role="tab" data-toggle="tab" class="tab-links">Instamojo </a></li>
				  	<li class="<?php if(($main_tab=='PaymentGateway') && ($sub_tab=='Paytm')) { echo 'active';}?>"><a href="#Paytm" role="tab" data-toggle="tab" class="tab-links">Paytm</a></li>
				  	<li class="<?php if(($main_tab=='PaymentGateway') && ($sub_tab=='PayUmoney')) { echo 'active';}?>"><a href="#PayUmoney" role="tab" data-toggle="tab" class="tab-links">PayU Money</a></li>
				  	<li class="<?php if(($main_tab=='PaymentGateway') && ($sub_tab=='Razorpay')) { echo 'active';}?>"><a href="#Razorpay" role="tab" data-toggle="tab" class="tab-links">Razorpay</a></li>
				  	<li class="<?php if(($main_tab=='PaymentGateway') && ($sub_tab=='PayPal')) { echo 'active';}?>"><a href="#PayPal" role="tab" data-toggle="tab" class="tab-links">PayPal</a></li>
				  	<li class="<?php if(($main_tab=='PaymentGateway') && ($sub_tab=='2CheckOut')) { echo 'active';}?>"><a href="#2CheckOut" role="tab" data-toggle="tab" class="tab-links">2CheckOut</a></li>
				  	<li class="<?php if(($main_tab=='PaymentGateway') && ($sub_tab=='Stripe')) { echo 'active';}?>"><a href="#Stripe" role="tab" data-toggle="tab" class="tab-links">Stripe</a></li>
				  	<li class="<?php if(($main_tab=='PaymentGateway') && ($sub_tab=='CCAvenue')) { echo 'active';}?>"><a href="#CCAvenue" role="tab" data-toggle="tab" class="tab-links">CCAvenue</a></li>
			  	</ul>

				<div class="tab-content">

					<!-- Offline Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='PaymentGateway' || $main_tab==null) && ($sub_tab=='Offline' || $sub_tab==null)) { echo 'active';}?>" id="Offline">
			  			<?php include("modules/payment-gateway-setting/Offline_Payment_Setup_data.php");?>
			  		</div>
			  		<!-- Instamojo Setup End-->

					<!-- BankTransfer Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='PaymentGateway') && ($sub_tab=='BankTransfer')) { echo 'active';}?>" id="BankTransfer">
			  			<?php include("modules/payment-gateway-setting/BankTransfer_Setup_data.php");?>
			  		</div>
			  		<!-- BankTransfer Setup End-->

					<!-- Instamojo Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='PaymentGateway') && ($sub_tab=='Instamojo')) { echo 'active';}?>" id="Instamojo">
			  			<?php include("modules/payment-gateway-setting/Instamojo_Setup_data.php");?>
			  		</div>
			  		<!-- Instamojo Setup End-->

			  		<!-- Paytm Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='PaymentGateway') && ($sub_tab=='Paytm')) { echo 'active';}?>" id="Paytm">
			  			<?php include("modules/payment-gateway-setting/Paytm_Setup_data.php");?>
			  		</div>
			  		<!-- Paytm Setup End-->

			  		<!-- PayUmoney Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='PaymentGateway') && ($sub_tab=='PayUmoney')) { echo 'active';}?>" id="PayUmoney">
			  			<?php include("modules/payment-gateway-setting/PayUmoney_Setup_data.php");?>
			  		</div>
			  		<!-- PayUmoney Setup End-->

			  		<!-- Razorpay Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='PaymentGateway') && ($sub_tab=='Razorpay')) { echo 'active';}?>" id="Razorpay">
			  			<?php include("modules/payment-gateway-setting/Razorpay_Setup_data.php");?>
			  		</div>
			  		<!-- Razorpay Setup End-->

			  		<!-- PayPal Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='PaymentGateway') && ($sub_tab=='PayPal')) { echo 'active';}?>" id="PayPal">
			  			<?php include("modules/payment-gateway-setting/PayPal_Setup_data.php");?>
			  		</div>
			  		<!-- PayPal Setup End-->

			  		<!-- 2CheckOut Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='PaymentGateway') && ($sub_tab=='2CheckOut')) { echo 'active';}?>" id="2CheckOut">
			  			<?php include("modules/payment-gateway-setting/2CheckOut_Setup_data.php");?>
			  		</div>
			  		<!-- 2CheckOut Setup End-->

			  		<!-- Stripe Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='PaymentGateway') && ($sub_tab=='Stripe')) { echo 'active';}?>" id="Stripe">
			  			<?php include("modules/payment-gateway-setting/Stripe_Setup_data.php");?>
			  		</div>
			  		<!-- Stripe Setup End-->

			  		<!-- CCAvenue Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='PaymentGateway') && ($sub_tab=='CCAvenue')) { echo 'active';}?>" id="CCAvenue">
			  			<?php include("modules/payment-gateway-setting/CCAvenue_Setup_data.php");?>
			  		</div>
			  		<!-- CCAvenue Setup End-->

			  	</div>
			</div>
		<!-- end: page -->
	</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>