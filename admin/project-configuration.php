<?php
	include("templates/header.php");
	if(isset($_GET['main_tab']) && isset($_GET['sub_tab']))
	{
		$main_tab=quote_smart($_GET['main_tab']);
		$sub_tab=quote_smart($_GET['sub_tab']);
	}
	else
	{
		$main_tab=null;
		$sub_tab=null;
	}
	//echo $sub_tab;
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<link rel="stylesheet" href="../css/croppie.css" />
	
</head>
<body>

		<section role="main" class="content-body">
			
			<!-- start: page -->
			<div class="row admin_start_section">
				<h1>Project Configuration</h1>
				<hr class="setting-devider"/>
				<ul class="nav data-tabs nav-tabs" role="tablist">
				  	<li class="<?php if(($main_tab=='ProjectAttributes' || $main_tab==null) && ($sub_tab=='General_Setup' || $sub_tab=='General_Security_Setup' || $sub_tab=='Localization' || $sub_tab=='Mail_Relay_Settings' || $sub_tab=='Maintainance_Mode_Settings' || $sub_tab==null)) { echo 'active';}?>"><a href="#General_Setting" role="tab" data-toggle="tab" class="tab-links">General Setting</a></li>

				  	<li class="<?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Custom_Scripts_Settings')) { echo 'active';}?>"><a href="#Custom_Scripts_Settings" role="tab" data-toggle="tab" class="tab-links">Custom Scripts</a></li>

				  	<li class="<?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Social_Sharing_Settings')) { echo 'active';}?>"><a href="#Social_Sharing_Settings" role="tab" data-toggle="tab" class="tab-links">Social Sharing</a></li>

				  	<li class="<?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Cron_Job_Settings')) { echo 'active';}?>"><a href="#Cron_Job_Settings" role="tab" data-toggle="tab" class="tab-links">Cron Job</a></li>

				  	<li class="<?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Custome_Meta_Tags_Settings')) { echo 'active';}?>"><a href="#Custome_Meta_Tags_Settings" role="tab" data-toggle="tab" class="tab-links">Custom Meta Tags</a></li>

				  	<li class="<?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Sitemap_generation_Settings')) { echo 'active';}?>"><a href="#Sitemap_generation_Settings" role="tab" data-toggle="tab" class="tab-links">Sitemap</a></li>

				  	<li class="<?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Robots_txt_Settings')) { echo 'active';}?>"><a href="#Robots_txt_Settings" role="tab" data-toggle="tab" class="tab-links">Robots.txt</a></li>
				</ul>

				<div class="tab-content">
					<!-- General Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes' || $main_tab==null) && ($sub_tab=='General_Setup' || $sub_tab=='General_Security_Setup' || $sub_tab=='Localization' || $sub_tab=='Mail_Relay_Settings' || $sub_tab=='Maintainance_Mode_Settings' || $sub_tab==null)) { echo 'active';}?>" id="General_Setting">
			  			<?php include("modules/general-setting/general-setting.php");?>
			  		</div>
			  		<!-- General Setup End-->

			  		<!-- Custom Script Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Custom_Scripts_Settings')) { echo 'active';}?>" id="Custom_Scripts_Settings">
			  			<h2>Custom Script Setting</h2><br/>
			  			<?php include("modules/general-setting/Custom_Script_data.php");?>
			  			
			  		</div>
			  		<!-- Custom Script Setup End-->

			  		<!-- Social Sharing Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Social_Sharing_Settings')) { echo 'active';}?>" id="Social_Sharing_Settings">
			  			<h2>Social Sharing Setting</h2><br/>
			  			<?php include("modules/general-setting/Social_Sharing_data.php");?>
			  			
			  		</div>
			  		<!-- Social Sharing Setup End-->

			  		<!-- Cron Job Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Cron_Job_Settings')) { echo 'active';}?>" id="Cron_Job_Settings">
			  			<h2>Cron Job Settings</h2><br/>
			  			<?php include("modules/general-setting/Cron_Job_data.php");?>
			  			
			  		</div>
			  		<!-- Cron Job Setup End-->

			  		<!-- Custom Meta Tags Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Custome_Meta_Tags_Settings')) { echo 'active';}?>" id="Custome_Meta_Tags_Settings">
			  			<h2>Custom Meta Tags Settings</h2><br/>
			  			<?php include("modules/general-setting/Custom_Meta_data.php");?>
			  			
			  		</div>
			  		<!-- Custom Meta Tags Setup End-->

			  		<!-- Sitemap Generation Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Sitemap_generation_Settings')) { echo 'active';}?>" id="Sitemap_generation_Settings">
			  			<h2>Sitemap Settings</h2><br/>
			  			<?php include("modules/general-setting/Sitemap_generation_data.php");?>
			  			
			  		</div>
			  		<!-- Sitemap Generation Setup End-->

			  		<!-- Robots.txt file Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Robots_txt_Settings')) { echo 'active';}?>" id="Robots_txt_Settings">
			  			<h2>Robots.txt Settings</h2><br/>
			  			<?php include("modules/general-setting/Robots_txt_data.php");?>
			  			
			  		</div>
			  		<!-- Robots.txt file Setup End-->
			  	</div>
			</div>
			<!-- end: page -->
		</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>
<script src="../js/croppie.js"></script>
<script>  
	$(document).ready(function(){

		/*     Logo URL crop  start  */
		$image_crop = $('#image_demo_LogoURL').croppie({
			enableExif: true,
			viewport: 
			{
				width:300,
				height:120,
				type:'square' //circle
			},
			boundary:
			{
				width:300,
				height:300
			}
		});

		$('#LogoURL').on('change', function(){
			var file = $(this).val();
	  		var ext = file.split('.').pop();
	  		var img_array = "<?php echo getAllowedFileAttachmentTypesString(); ?>";

	  		var i = img_array.indexOf(ext);

	  		if(i > -1) 
	  		{
			    var reader = new FileReader();
				reader.onload = function (event) {
					$image_crop.croppie('bind', {
						url: event.target.result
					}).then(function(){
						console.log('jQuery bind complete');
					});
				}
				reader.readAsDataURL(this.files[0]);
				$('#uploadLogoModal').modal('show');
			} 
			else 
			{
			    $('.status_LogoURL').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span><strong> Error! </strong> Invalid image type.</div>");
			    return false;
			}	
		});

		$('.crop_image').click(function(event){
			$('.crop_image').attr('disabled',true);
			$image_crop.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function(response){
			$.ajax({
				url:"upload_Logo_pic.php",
				type: "POST",
				data:{"image": response},
				success:function(res)
				{
					$('#uploadLogoModal').modal('hide');
					$('#uploaded_image').html(res);
					$('.crop_image').attr('disabled',false);
					$('.status_LogoURL').html("<div class='alert alert-success success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Logo image updated  successfully.</div>");
					$('.success_status').fadeTo(2000, 500).slideUp(500, function(){
						window.location.assign("project-configuration.php#main_tab=ProjectAttributes&sub_tab=General_Setup");
					});
				}
				});
			})
		});

		/*     Logo URL crop  end  */


		/*     Favicon URL crop  start  */
		$image_crop1 = $('#image_demo_favicon').croppie({
			enableExif: true,
			viewport: {
				width:32,
				height:32,
				type:'square' //circle
			},
			boundary:{
				width:300,
				height:100
			}
		});

		$('#FaviconURL').on('change', function(){
			var file = $(this).val();
	  		var ext = file.split('.').pop();
	  		var img_array = "<?php echo getAllowedFileAttachmentTypesString(); ?>";

	  		var i = img_array.indexOf(ext);

	  		if(i > -1) 
	  		{
			    var reader = new FileReader();
				reader.onload = function (event) {
					$image_crop1.croppie('bind', {
					url: event.target.result
					}).then(function(){
						console.log('jQuery bind complete');
					});
				}
				reader.readAsDataURL(this.files[0]);
				$('#uploadFaviconModal').modal('show');
			} 
			else 
			{
			    $('.status_favicon').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span><strong> Error! </strong> Invalid image type.</div>");
			    return false;
			}
		});

		$('.crop_image_favicon').click(function(event){
			$('.crop_image_favicon').attr('disabled',true);
			$image_crop1.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function(response){
			$.ajax({
				url:"upload_favicon_pic.php",
				type: "POST",
				data:{"image": response},
				success:function(res)
				{
					$('#uploadFaviconModal').modal('hide');
					$('#uploaded_image_favicon').html(res);
					$('.crop_image_favicon').attr('disabled',false);
					$('.status_favicon').html("<div class='alert alert-success success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Favicon updated  successfully.</div>");
					$('.success_status').fadeTo(2000, 500).slideUp(500, function(){
						window.location.assign("project-configuration.php#main_tab=ProjectAttributes&sub_tab=General_Setup");
					});
				}
			});
			})
		});
		/*     Favicon URL crop  end  */


		$('#LoaderImage').on('change', function(e){
			var file = $(this).val();
	  		var ext = file.split('.').pop();
	  		var img_array = "gif";

	  		var i = img_array.indexOf(ext);

	  		if(i > -1) 
	  		{
			    var formData = new FormData();
	            formData.append('file', $(this)[0].files[0]);
	            var loader_img = e.target.files[0].name;

	            $.ajax({
	                url : 'Loader_Image_Upload.php',
	                type : 'POST',
	                data : formData,
	                processData: false,  // tell jQuery not to process the data
	                contentType: false,  // tell jQuery not to set contentType
	                success : function(data) 
	                {
	                    if(data=='success')
	                    {
	                        
	                        $('.status_loader').html("<center><div class='alert alert-success status_loader_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Loader image updated successfully.</div></center>");
	                        
	                        $('.status_loader_success').fadeTo(1000, 500).slideUp(500, function(){
	                            window.location.assign("project-configuration.php");
	                        });
	                    }
	                    else
	                    {
	                        $('.status_loader').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+data+"</div></center>");
	                        return false;
	                    }
	                }
	            });
			} 
			else 
			{
			    $('.status_loader').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 15px;'><span class='fa fa-times'></span><strong> Error! </strong> Invalid image type.</div>");
			    return false;
			}
		});

		/*******    Website title  update     ********/
		$('.WebSiteTitle').keypress(function (e) {
			var WebSiteTitle = $('.WebSiteTitle').val();
			var WebSiteTagline = $('.WebSiteTagline').val();

			var WebSiteTitle_length = WebSiteTitle.length;
			if(WebSiteTitle_length>25)
			{
				alert("Maximum website title length exceeding.");
				$('.WebSiteTitle').val(WebSiteTitle.substring(0, 25));
				return false;
			}

			
		});

		$('.WebSiteTitle').keyup(function (e) {
			var WebSiteTitle = $('.WebSiteTitle').val();
			var WebSiteTagline = $('.WebSiteTagline').val();

			$('.preview-tagline').html($(this).val()+' | '+WebSiteTagline);
			
		});
		/*******    Website title  update End    ********/

		/*******    Website tagline  update     ********/
		$('.WebSiteTagline').keypress(function (e) {
			var WebSiteTitle = $('.WebSiteTitle').val();
			var WebSiteTagline = $('.WebSiteTagline').val();

			var WebSiteTagline_length = WebSiteTagline.length;
			if(WebSiteTagline_length>45)
			{
				alert("Maximum website tagline length exceeding.");
				$('.WebSiteTagline').val(WebSiteTagline.substring(0, 45));
				return false;
			}

			
		});

		$('.WebSiteTagline').keyup(function (e) {
			var WebSiteTitle = $('.WebSiteTitle').val();
			var WebSiteTagline = $('.WebSiteTagline').val();

			$('.preview-tagline').html(WebSiteTitle+' | '+$(this).val());
			
		});
		/*******    Website Tagline  update End    ********/

		/********    General Setup Save   *********/
		$('.btn_generalsetup').click(function(){
			var WebSiteBasePath = $('.WebSiteBasePath').val();
			var WebSiteTitle = $('.WebSiteTitle').val();
			var WebSiteTagline = $('.WebSiteTagline').val();
			var EmailAddress = $('.EmailAddress').val();
			var TermOfServiceURL = $('.TermOfServiceURL').val();
			var PrivacyPolicyURL = $('.PrivacyPolicyURL').val();
			var AllowedFileAttachmentTypes = $('.AllowedFileAttachmentTypes').val();
			var task = "Update_General_Setup";

			var data = 'WebSiteBasePath='+WebSiteBasePath+'&WebSiteTitle='+WebSiteTitle+'&WebSiteTagline='+WebSiteTagline+'&EmailAddress='+EmailAddress+'&TermOfServiceURL='+TermOfServiceURL+'&PrivacyPolicyURL='+PrivacyPolicyURL+'&AllowedFileAttachmentTypes='+AllowedFileAttachmentTypes+'&task='+task;

			$('.loading_img').show();

			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/general_setup_helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.btn_generalsetup').attr('disabled',true);
	        			$('.generalsetup_status').html("<center><div class='alert alert-success generalsetup_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
	        			$('.generalsetup_success_status').fadeTo(1000, 500).slideUp(500, function(){
	        				$('.btn_generalsetup').attr('disabled',false);
							  window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=General_Setup");
	                    });
	        		}
	        		else
	        		{
	        			$('.generalsetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
						return false;
	        		}
	        	}
	        });
		});
		
	});  
</script>