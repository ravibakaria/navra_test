<?php
	session_start();

	if(isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']!='' || $_SESSION['admin_user']!=null))
	{
		header('Location: index.php');
		exit;
	}

	include('../config/config.php');
	include('../config/dbconnect.php');
	include('../config/functions.php');
	include('../config/setup-values.php');   //get master setup values
    include('../config/email/email_style.php');   //get master setup values
    include('../config/email/email_templates.php');   //get master setup values
    include('../config/email/email_process.php');
	
	$WebSiteBasePath = getWebsiteBasePath();
    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }

    /********    Submit For status    **********/
	$recaptchaAllowed = getreCaptchaAllowed();

	if($recaptchaAllowed == "1")
	{
		$recaptchaSiteKey=getreCaptchaSiteKey();
		$recaptchaSecretKey=getreCaptchaSecretKey();
	}

	$errorMessage = null;
	$successMessage = null;

	if($recaptchaAllowed == "1")
	{
		$recaptchaSiteKey=getreCaptchaSiteKey();
		$recaptchaSecretKey=getreCaptchaSecretKey();

		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		{	
			//your site secret key
			$secret = $recaptchaSecretKey;
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);
			if($responseData->success)
			{
				if (isset($_POST['email']) && $_POST['email'] != '')
				{
					$email = quote_smart($_POST['email']);

			        $sql_chk = $link->prepare("SELECT * FROM `admins` WHERE `email`='$email'"); 
			        $sql_chk->execute();

			        $count=$sql_chk->rowCount();

			        if($count>0)
			        {
			            $userData = $sql_chk->fetch(PDO::FETCH_OBJ);
			            $id = $userData->id;
			            $firstname = $userData->firstname;
			            $email = $userData->email;
			            $status = $userData->status;

			            if($status=='0')
			            {
			                $errorMessage = "Your account is inactive";
			            }
			            else
			            if($status=='2')
			            {
			                $errorMessage = "Your account is disabled.";
			                    
			            }
			            else
			            if($status=='3')
			            {
			                $errorMessage = "Your account is suspended.";
			            }

			            if($errorMessage ==null)
			            {
			            	$Special_char_string = '[!@#$%^)*_(+=}{|:;,.<>}]'; 
				            $pos1 = rand(0,(strlen($Special_char_string)-1));
				            $pass1 = $Special_char_string[$pos1];

				            $Capital_char_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
				            $pos2 = rand(0,(strlen($Capital_char_string)-1));
				            $pass2 = $Capital_char_string[$pos2];

				            $Number_char_string = '1234567890'; 
				            $pos3 = rand(0,(strlen($Number_char_string)-1));
				            $pass3 = $Number_char_string[$pos3];

				            $salt = "abchefghjkmnpqrstuvwxyz0123456789";
				            srand((double)microtime()*1000000);

				            $i = 0;
				            while ($i <= 5) 
				            {
				                $num = rand() % 33;
				                $tmp = substr($salt, $num, 1);
				                $pass1 = $pass1 . $tmp;
				                $i++;
				            }
				                
				            $new_pass = $pass2.$pass3.$pass1;    //
				            $tmp_password = md5($new_pass);     // random 

				            $sql_update = "UPDATE `admins` SET `password`='$tmp_password' WHERE `id`='$id' AND `email`='$email'";
				            $login_link = "<a href='$WebSiteBasePath/admin/login.php' target='_blank'>$WebSiteBasePath/admin/login.php</a>";
				            if($link->exec($sql_update))
				            {
				                $SocialSharing = getSocialSharingLinks();   // social sharing links
				                
				                $ForgotPasswordSubject = str_replace('$site_name',$WebSiteTitle,$ForgotPasswordSubject);   //replace subject variables with value

				                $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

				                $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

				                $ForgotPasswordMessage = str_replace(array('$first_name','$site_name','$login_link','$email_address','$new_password','$signature'),array($firstname,$WebSiteTitle,$login_link,$email,$new_pass,$GlobalEmailSignature),$ForgotPasswordMessage);  //replace footer variables with value


				                $subject = $ForgotPasswordSubject;
				                
				                $message  = '<!DOCTYPE html>';
				                $message .= '<html lang="en">
				                    <head>
				                    <meta charset="utf-8">
				                    <meta name="viewport" content="width=device-width">
				                    <title></title>
				                    <style type="text/css">'.$EmailCSS.'</style>
				                    </head>
				                    <body style="margin: 0; padding: 0;">';
				                //echo $message;exit;
				                $message .= $EmailGlobalHeader;

				                $message .= $ForgotPasswordMessage;

				                $message .= $EmailGlobalFooter;
				                
				                $mailto = $email;
				                $mailtoname = $firstname;
				                
				                $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

				                if($emailResponse == 'success')
				                {
				                        $successMessage = "success";
				                }
				                else
				                {
				                        $errorMessage = "Error in sending login details to user.";
				                }
				            }
				            else
				            {
				                    $errorMessage = "<br/>Something went wrong. Try after some time.";
				            }
			            }			            
			        }
			        else
			        {
			            $errorMessage = "<br/>Email id not registered with system.<br/>Please enter registered email id.";
			        }
				}
			}
		}
	}
	else
	{
		if (isset($_POST['email']) && $_POST['email'] != '')
		{
			$email = quote_smart($_POST['email']);

	        $sql_chk = $link->prepare("SELECT * FROM `admins` WHERE `email`='$email'"); 
	        $sql_chk->execute();

	        $count=$sql_chk->rowCount();

	        if($count>0)
	        {
	            $userData = $sql_chk->fetch(PDO::FETCH_OBJ);
	            $id = $userData->id;
	            $firstname = $userData->firstname;
	            $email = $userData->email;
	            $status = $userData->status;

	            if($status=='0')
	            {
	                $errorMessage = "Your account is inactive";
	            }
	            else
	            if($status=='2')
	            {
	                $errorMessage = "Your account is disabled.";
	                    
	            }
	            else
	            if($status=='3')
	            {
	                $errorMessage = "Your account is suspended.";
	            }

	            if($errorMessage ==null)
	            {
	            	$Special_char_string = '[!@#$%^)*_(+=}{|:;,.<>}]'; 
		            $pos1 = rand(0,(strlen($Special_char_string)-1));
		            $pass1 = $Special_char_string[$pos1];

		            $Capital_char_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
		            $pos2 = rand(0,(strlen($Capital_char_string)-1));
		            $pass2 = $Capital_char_string[$pos2];

		            $Number_char_string = '1234567890'; 
		            $pos3 = rand(0,(strlen($Number_char_string)-1));
		            $pass3 = $Number_char_string[$pos3];

		            $salt = "abchefghjkmnpqrstuvwxyz0123456789";
		            srand((double)microtime()*1000000);

		            $i = 0;
		            while ($i <= 5) 
		            {
		                $num = rand() % 33;
		                $tmp = substr($salt, $num, 1);
		                $pass1 = $pass1 . $tmp;
		                $i++;
		            }
		                
		            $new_pass = $pass2.$pass3.$pass1;    //
		            $tmp_password = md5($new_pass);     // random 

		            $sql_update = "UPDATE `admins` SET `password`='$tmp_password' WHERE `id`='$id' AND `email`='$email'";
		            $login_link = "<a href='$WebSiteBasePath/admin/login.php' target='_blank'>$WebSiteBasePath/admin/login.php</a>";
		            if($link->exec($sql_update))
		            {
		                $SocialSharing = getSocialSharingLinks();   // social sharing links
		                
		                $ForgotPasswordSubject = str_replace('$site_name',$WebSiteTitle,$ForgotPasswordSubject);   //replace subject variables with value

		                $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

		                $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

		                $ForgotPasswordMessage = str_replace(array('$first_name','$site_name','$login_link','$email_address','$new_password','$signature'),array($firstname,$WebSiteTitle,$login_link,$email,$new_pass,$GlobalEmailSignature),$ForgotPasswordMessage);  //replace footer variables with value


		                $subject = $ForgotPasswordSubject;
		                
		                $message  = '<!DOCTYPE html>';
		                $message .= '<html lang="en">
		                    <head>
		                    <meta charset="utf-8">
		                    <meta name="viewport" content="width=device-width">
		                    <title></title>
		                    <style type="text/css">'.$EmailCSS.'</style>
		                    </head>
		                    <body style="margin: 0; padding: 0;">';
		                //echo $message;exit;
		                $message .= $EmailGlobalHeader;

		                $message .= $ForgotPasswordMessage;

		                $message .= $EmailGlobalFooter;
		                
		                $mailto = $email;
		                $mailtoname = $firstname;
		                
		                $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

		                if($emailResponse == 'success')
		                {
		                        $successMessage = "success";
		                }
		                else
		                {
		                        $errorMessage = "Error in sending login details to user.";
		                }
		            }
		            else
		            {
		                    $errorMessage = "<br/>Something went wrong. Try after some time.";
		            }
	            }			            
	        }
	        else
	        {
	            $errorMessage = "<br/>Email id not registered with system.<br/>Please enter registered email id.";
	        }
		}
	}
?>
<!doctype html>
<html class="fixed">
	<head>
		<title>Forgot Password -  <?php echo getWebsiteTitle(); ?></title>
		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Admin - Responsive HTML5 Template">
		<meta name="author" content="navrabayko.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- CSS  -->
		<link rel="stylesheet" href="../css/google-fonts.css" type="text/css">
		<link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
		<link rel="stylesheet" href="../css/font-awesome.css" type="text/css">
		<link rel="stylesheet" href="../css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="../css/datepicker3.css" type="text/css">
		<link rel="stylesheet" href="../css/dataTables.bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="../css/bootstrap-toggle.min.css" type="text/css">
		<link rel="stylesheet" href="../css/theme.css" type="text/css">
		<link rel="stylesheet" href="../css/default.css" type="text/css">
		<link rel="stylesheet" href="../css/theme-custom.css" type="text/css">
		<link rel="stylesheet" href="../css/master.css" type="text/css">
		<link rel="stylesheet" href="../css/custom.css" type="text/css">
		<link rel="stylesheet" href="../css/admin.css" type="text/css">
		<link rel="stylesheet" href="../config/style.php" type='text/css'>
		<link rel="stylesheet" href="../css/bootstrap-glyphicons.css" type="text/css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
	    <link rel="stylesheet" href="../css/richtext-editor.css" type="text/css">
	    <link rel="stylesheet" href="../css/richtext.min.css" type="text/css">
	    <link rel="stylesheet" href="../css/bootstrap-multiselect.css" type="text/css">

	    <!-- Favicon  -->
	    <?php 
		    $websiteBasePath = getWebsiteBasePath();
		    if(getFaviconURL()!='' || getFaviconURL()!=null)
		    {
		      $favicon_arr=array();
		      $favicon_get = getFaviconURL();
		      if($favicon_get!='' || $favicon_get!=null)
		      {
		        $favicon_get = explode('/',$favicon_get);

		        for($i=1;$i<count($favicon_get);$i++)
		        {
		          $favicon_arr[] = $favicon_get[$i];
		        }

		        $favicon = implode('/',$favicon_arr);
		      }
		      echo "<link rel='icon' type='image/png' href='$websiteBasePath/$favicon' sizes='32x32'>";
		    }
		    else
		    {
		      echo "<link rel='icon' type='image/png' href='$websiteBasePath/images/favicon/default-favicon.png' sizes='32x32'>";
		    }
		?>

		<script>
			function onSubmit(token)
			{
			    document.getElementById("frmLogin").submit();
			}

			function validate(event) 
			{
			    event.preventDefault();
			    var email = $('.email').val();
				var recaptchaAllowed = $('.recaptchaAllowed').val();
				var task = "Forgot-Password-User";
				
				// Email Validation 
				if(email=='' || email==null)
	            {
	            	$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty! </strong> Enter email address.</div>");
	                return false;
	            }

		        //recaptcha validation 
		        if(recaptchaAllowed=='1')
				{
					grecaptcha.execute();
				}
				else
				{
					document.getElementById("frmLogin").submit();
				}

				$('.loading_img').show();
				$('.form_status').html("");
			}

			function onload() 
			{
			    var element = document.getElementById('recaptcha-submit');
			    element.onclick = validate;
			}
		</script>

		<!-- Head Libs -->
		<script src="../js/modernizr.js"></script>
		
		<!-- Google recaptcha -->
		<?php
			if($recaptchaAllowed == "1")
			{
				echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
			}
		?>
	</head>
	<body class="login-body">
		<section class="body">
			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="index.php" class="navbar-brand logo">
						<?php 
							$logo = getLogoURL();
							$sitetitle = getWebsiteTitle();
							$siteBasePath = getWebsiteBasePath();
							if($logo!='' || $logo!=null)
							{
								echo "<img src='$logo' class='img-responsive logo-img'  />";
							}
							else
							{
								echo "<img src='$siteBasePath/images/logo/default-logo.jpg' class='img-responsive logo-img' />";
							}
						?>
					</a>
				</div>

				<!-- end: search & user box -->
			</header>
			<!-- end: header -->
		</section>

		<!-- start: page -->
		<section class="body-sign admin-body-sign">
			<div class="center-sign admin-center-sign">

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none title-login-forgot"><i class="fa fa-user mr-xs"></i> Forgot Password</h2>
					</div>
					<div class="panel-body">
						<form name="frmLogin" id="frmLogin" autocomplete="off" method="post"  action="<?php echo $_SERVER['PHP_SELF'];?>">
							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Email Id</label>
									<a href="login.php" class="pull-right">Login ?</a>
								</div>
								<div class="input-group input-group-icon">
									<input name="email" type="text" class="form-control input-lg email" value="<?php echo @$email; ?>"/>
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group">
					        	<?php
					        		if ($errorMessage != '' || $errorMessage != null)
									{
										echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
										echo "<strong>Error!</strong> $errorMessage";
										echo "</div>";
									}

									if ($successMessage != '' || $successMessage != null)
									{
										echo "<div class='alert alert-success col-sm-12 success_status' style='padding: 10px; margin-bottom: 10px;'>";
										echo "<strong><i class='fa fa-check'></i> Success! </strong>Password sent successfully. Please check Your email inbox/spam folder.";
										echo "</div>";
										
										
										echo "<script>
										        $('.success_status').fadeTo(2000, 500).slideUp(500, function(){
	                                            $('.success_status').slideUp(200);
	                                            window.location.assign('login.php');
	                                        });
	                                    </script>";
									}
					        	?>
					        </div>

					        <input type="hidden" class="recaptchaAllowed" value="<?php echo $recaptchaAllowed;?>">

							<div class="row">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:60px; height:60px; display:none;'/></center>
							</div>

							<div class="mb-xs text-center form_status">
								
							</div>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  forgot-btn-fg-password">
									<center>
										<input type="submit" name="login btn"  class="btn btn-primary btn-block login_btn website-button" id="recaptcha-submit" value="Reset & Send Password">
									</center>
								</div>
							</div>
							<?php
								if($recaptchaAllowed == "1")
								{
									echo "<div class='form-group'>
										<div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
									</div>";
								}
							?>
							
							<script>onload();</script>

							
						</form>
					</div>
				</div>
			</div>
		</section>
		<!-- end: page -->
<?php
	include("templates/footer.php");
?>
		<!-- Vendor -->
		<script src="../js/jquery.js"></script>
		<script src="../js/jquery.browser.mobile.js"></script>
		<script src="../js/bootstrap.js"></script>
		<script src="../js/nanoscroller.js"></script>
		<script src="../js/bootstrap-datepicker.js"></script>
		<script src="../js/magnific-popup.js"></script>
		<script src="../js/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../js/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../js/theme.init.js"></script>

	</body>
</html>

<script>
	$(document).ready(function(){
		/*$(".robot-div").hide();
		
		$(".email").click(function(){
			$(this).removeClass("danger_error");
		});

		$('.btn_login').click(function(){
			var email = $('.email').val();
			var robot = $('.robot').val();
			
			var task = "forgot_password-admin";

			if(email=='' || email==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter your email id.</div>");
				$(".email").addClass("danger_error");
				return false;
			}

			$('.form_status').html("");
			$('.loading_img').show();
			var data = 'email='+email+'&robot='+robot+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/forgot_password_helper.php',
            	success:function(res)
            	{
            		$('.loading_img').hide();
            			
            		if(res=='success')
            		{
            			$('.btn_login').attr('disabled',true);
            			$('.form_status').html("<div class='alert alert-success login_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span><strong> Success! </strong><br/> Your password request processed successfully.<br/> Please check your email inbox/spam folder for updated password.<center> </div>");
            		}
            		else
            		{
            			$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</center></div>");
						return false;
            		}
            	}
            });
            return false;
		});*/
	});
</script>