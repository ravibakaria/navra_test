<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body update-section">
		<a href="membership-plan-setup.php" id="portletReset" type="button" class="mb-xs mt-xs mr-xs btn btn-default" style="float:right;"><i class="fa fa-arrow-left"></i> Back</a>

		<?php
			$id = quote_smart($_GET['id']);

			if ( !is_numeric( $id ) )
		    {
		        echo 'Error! Invalid Data';
		        exit;
		    }

		    $DefaultCurrency = getDefaultCurrency();
			$DefaultCurrencyCode = getDefaultCurrencyCode($DefaultCurrency);
			if($DefaultCurrencyCode=='' || $DefaultCurrencyCode==null)
			{
				$DefaultCurrencyCode='INR';
			}

		    $sql = "SELECT * FROM `membership_plan` WHERE `id`='$id'";
		    $stmt1   = $link->prepare($sql);
            $stmt1->execute();
            $queryTot = $stmt1->rowCount();
            $result = $stmt1->fetch();

           	if($queryTot>0)
           	{
           		$id = $result['id'];
           		$membership_plan_name = $result['membership_plan_name'];
           		$number_of_contacts = $result['number_of_contacts'];
           		$number_of_duration_months = $result['number_of_duration_months'];
           		$price = $result['price'];
           		$status = $result['status'];
		?>
		<br/><br/>
		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-md-1 col-lg-1">
					</div>
					<div class="col-md-10 col-lg-10 edit-master-div">
						<div class="row"><br>
							<center><h4>Update <strong><?php echo $membership_plan_name;?></strong> Membership Plan</h4></center>
						</div>
						<hr/>
						<div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Plan Name:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <input type="text" class="form-control form-control-sm membership_plan_name" value="<?php echo $membership_plan_name?>" />
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Number Of Contacts:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <input type="number" class="form-control form-control-sm number_of_contacts" value="<?php echo $number_of_contacts?>" />
                            </div>
							<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
								<div class="alert alert-info"><span class='fa fa-exclamation-circle'></span> Put 0(zero) for unlimited contacts.</div>
							</div>
                        </div>

                        <div class="row input-row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								Price: <?php echo "[".$DefaultCurrencyCode."]";?>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<input type='text' class="form-control price" value="<?php echo $price?>">
							</div>
						</div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Number Of Days:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <select class="form-control number_of_duration_months">
									<option value="0">--Select--</option>
									<?php
										for($i=1;$i<=12;$i++)
										{
											if($i==$number_of_duration_months)
											{
												echo "<option value='".$i."' selected>".$i." Month</option>";
											}
											else
											{
												echo "<option value='".$i."'>".$i." Month</option>";
											}
										}
									?>
								</select>
                            </div>
                        </div>
                        <div class="row input-row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Status:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
								<?php
									if($status=='0')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='status' name='status' value='".$status."'>";
									}
									else
									if($status=='1')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='status' name='status' checked value='".$status."'>";
									}
								?>
							</div>
						</div>
						<br/>
						<input type="hidden" class="membership_plan_id" value="<?php echo $id;?>">

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
							</div>	
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 update_membsership_plan_status">
								
							</div>	
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><button class="btn btn-success btn_update_membership_plan website-button">Update</button></center><br/>	
							</div>
						</div>
						
					</div>
					<div class="col-md-1 col-lg-1">
					</div>
				</div>
				<div class="row">
					<br/>
				</div>
			</div>
			
		</div>
		<?php
			}
			else
			{
				echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
			}
		?>
</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		/*   Prevent entering charaters in number of contacts text box   */
        $(".number_of_contacts").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.update_membsership_plan_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Only numbers allowed.</div>").show().fadeOut(3000);
                return false;
            }
        })

        /*   Prevent entering charaters in price text box   */
        $(".price").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which != 46 && (e.which < 48 || e.which > 57))) 
            {
                //display error message
                $('.update_membsership_plan_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Only numbers allowed.</div>").show().fadeOut(3000);
                return false;
            }
        })

		$('.btn_update_membership_plan').click(function(){
			var membership_plan_id = $('.membership_plan_id').val();
			var membership_plan_name = $('.membership_plan_name').val();
    		var number_of_contacts = $('.number_of_contacts').val();
    		var number_of_duration_months = $('.number_of_duration_months').val();
    		var price = $('.price').val();
    		var status = $('.status').is(":checked");
    		var task = "Update-Membership-Plan";

    		if(status==true)
            {
                status='1';
            }
            else
            if(status==false)
            {
                status='0';
            }

            if(membership_plan_name=='' || membership_plan_name==null)
            {
            	$('.update_membsership_plan_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong> Please enter membership plan.</div></center>");
                return false;
            }

            if(number_of_contacts=='' || number_of_contacts==null)
            {
            	$('.update_membsership_plan_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong> Please enter number of contacts.</div></center>");
                return false;
            }

            if(number_of_duration_months=='0')
            {
            	$('.update_membsership_plan_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong> Please select number of months.</div></center>");
                return false;
            }

            if(price=='' || price==null)
            {
            	$('.update_membsership_plan_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong> Please enter price for this membership plan.</div></center>");
                return false;
            }

            $('.update_membsership_plan_status').html("");

            var data = 'membership_plan_id='+membership_plan_id+'&membership_plan_name='+membership_plan_name+'&number_of_contacts='+number_of_contacts+'&number_of_duration_months='+number_of_duration_months+'&price='+price+'&status='+status+'&task='+task;

            $('.loading_img').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/membership-plan/membership-plan-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.update_membsership_plan_status').html("<center><div class='alert alert-success update_membsership_plan_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Membership Plan Updated Successfully.</div></center>");
                    }
                    else
                    {
                        $('.update_membsership_plan_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
		});
	});
</script>