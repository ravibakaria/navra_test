<?php
    include("templates/header.php");
    if(isset($_GET['main_tab']) && isset($_GET['sub_tab']))
    {
        $main_tab=quote_smart($_GET['main_tab']);
        $sub_tab=quote_smart($_GET['sub_tab']);
    }
    else
    {
        $main_tab=null;
        $sub_tab=null;
    }
?>

<html>
<head>
    <title>
        <?php
        $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
        $string = str_replace("-", " ", $a);
        echo $title = ucwords($string);
        ?> -  <?php echo getWebsiteTitle(); ?>
    </title>
    
</head>
<body>
    <section role="main" class="content-body">
        
        <!-- start: page -->
            <div class="row admin_start_section">
                <h1>Email Template Setup</h1>
                <hr class="setting-devider"/>

                <ul class="nav data-tabs nav-tabs" role="tablist">
                    <li class="<?php if(($main_tab=='EmailTemplateAttributes' || $main_tab==null) && ($sub_tab=='EmailTemplate_Setup' || $sub_tab==null)) { echo 'active';}?>"><a href="#EmailTemplate_Setup" role="tab" data-toggle="tab" class="tab-links">Email Template Setup </a></li>
                </ul>

                <div class="tab-content">
                    <!-- General Setup Start-->
                    <div class="tab-pane  <?php if(($main_tab=='EmailTemplateAttributes' || $main_tab==null) && ($sub_tab=='EmailTemplate_Setup' || $sub_tab==null)) { echo 'active';}?>" id="EmailTemplate_Setup">
                        <?php include("modules/Email-Template-setting/Email-Template_Setup_data.php");?>
                    </div>
                    <!-- General Setup End-->

                    
                </div>
            </div>
        <!-- end: page -->
    </section>
    </div>

</section>
</body>
<?php
    include("templates/footer.php");
?>