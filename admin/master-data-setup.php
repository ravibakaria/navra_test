<?php
	include("templates/header.php");
	if(isset($_GET['main_tab']) && isset($_GET['sub_tab']))
	{
		$main_tab=quote_smart($_GET['main_tab']);
		$sub_tab=quote_smart($_GET['sub_tab']);
	}
	else
	{
		$main_tab=null;
		$sub_tab=null;
	}
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<link rel="stylesheet" href="../css/croppie.css" />
	
</head>
<body>

		<section role="main" class="content-body">

			<div class="row admin_start_section">
				<h1>Master Data Setup</h1>
				<hr class="setting-devider"/>

				<ul class="nav data-tabs nav-tabs" role="tablist">
				  	<li class="<?php if($main_tab==null || $main_tab=='PersonalAttributes') { echo 'active';}?>"><a href="#PersonalAttributes" role="tab" data-toggle="tab" class="tab-links personal_attribute_tab">Personal Attributes </a></li>

				  	<li class="<?php if($main_tab=='ProfessionalAttributes') { echo 'active';}?>"><a href="#ProfessionalAttributes" role="tab" data-toggle="tab" class="tab-links">Professional Attributes</a></li>

				  	<li class="<?php if($main_tab=='ReligiousAttributes') { echo 'active';}?>"><a href="#ReligiousAttributes" role="tab" data-toggle="tab" class="tab-links">Religious Attributes</a></li>

				  	<li class="<?php if($main_tab=='LocationAttributes') { echo 'active';}?>"><a href="#LocationAttributes" role="tab" data-toggle="tab" class="tab-links">Location Attributes</a></li>
				  	
				  	<li class="<?php if($main_tab=='FamilyAttributes') { echo 'active';}?>"><a href="#FamilyAttributes" role="tab" data-toggle="tab" class="tab-links">Family Attributes</a></li>

				  	<li class="<?php if($main_tab=='IdentityDocuments') { echo 'active';}?>"><a href="#IdentityDocuments" role="tab" data-toggle="tab" class="tab-links">Identity Documents</a></li>
				</ul>

				<div class="tab-content">
					<!-- PersonalAttributes Setup Start-->
			  		<div class="tab-pane <?php if($main_tab==null || $main_tab=='PersonalAttributes') { echo 'active';}?>" id="PersonalAttributes">
			  			<h2>Personal Attributes Setting</h2>
			  			<?php include("modules/master-setting/personal_attributes_setup_data.php");?>
			  		</div>
			  		<!-- PersonalAttributes Setup End-->

			  		<!-- ProfessionalAttributes Setup Start-->
			  		<div class="tab-pane <?php if($main_tab=='ProfessionalAttributes') { echo 'active';}?>" id="ProfessionalAttributes">
			  			<h2>Professional Attributes Setting</h2>
			  			<?php include("modules/master-setting/professional_attributes_setup_data.php");?>
			  		</div>
			  		<!-- ProfessionalAttributes Setup End-->

			  		<!-- ReligiousAttributes Setup Start-->
			  		<div class="tab-pane <?php if($main_tab=='ReligiousAttributes') { echo 'active';}?>" id="ReligiousAttributes">
			  			<h2>Religious Attributes Setting</h2>
			  			<?php include("modules/master-setting/religious_attributes_setup_data.php");?>
			  		</div>
			  		<!-- ReligiousAttributes Setup End-->

			  		<!-- LocationAttributes Setup Start-->
			  		<div class="tab-pane <?php if($main_tab=='LocationAttributes') { echo 'active';}?>" id="LocationAttributes">
			  			<h2>Location Attributes Setting</h2>
			  			<div class="row">
			  				<?php include("modules/master-setting/location_attributes_setup_data.php");?>
			  			</div>
			  		</div>
			  		<!-- LocationAttributes Setup End-->

			  		<!-- FamilyAttributes Setup Start-->
			  		<div class="tab-pane <?php if($main_tab=='FamilyAttributes') { echo 'active';}?>" id="FamilyAttributes">
			  			<h2>Family Attributes Setting</h2>
			  			<?php include("modules/master-setting/family_attributes_setup_data.php");?>
			  		</div>
			  		<!-- FamilyAttributes Setup End-->

			  		<!-- IdentityDocuments Setup Start-->
			  		<div class="tab-pane <?php if($main_tab=='IdentityDocuments') { echo 'active';}?>" id="IdentityDocuments">
			  			<h2>Identity Document Types</h2>
			  			<?php include("modules/master-setting/identity_document_type_data.php");?>
			  		</div>
			  		<!-- IdentityDocuments Setup End-->
			  	</div>
			</div>
			<!-- end: page -->
		</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>
<script>
$(document).ready(function(){
	$('.personal_attribute_tab').click(function(){
		//$('#BodyType').addClass('active');
	});
});
</script>