<?php
	session_start();
	
	if(isset($_SESSION['logged_in']))
	{
		unset($_SESSION['logged_in']);
	}

	if(isset($_SESSION['admin_user']))
	{
		unset($_SESSION['admin_user']);
	}

	if(isset($_SESSION['admin_id']))
	{
		unset($_SESSION['admin_id']);
	}

	if(isset($_SESSION['admin_email']))
	{
		unset($_SESSION['admin_email']);
	}

	if(isset($_SESSION['admin_firstname']))
	{
		unset($_SESSION['admin_firstname']);
	}

	session_destroy();
	header('Location:index.php');
	exit;
?>