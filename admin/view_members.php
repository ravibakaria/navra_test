
<?php
	include("templates/header.php");
	$gender = quote_smart($_GET['gender']);
	$marital_status = quote_smart($_GET['marital_status']);

	if(!is_numeric($gender) || !is_numeric($marital_status))
	{
		echo "<script>alert('ERROR: Invalid Parameters. Please provide valid parameters.');</script>";
		exit;
	}

	$_SESSION['view_member_gender']=$gender;
	$_SESSION['view_member_marital_status']=$marital_status;
	
	if($gender=='1')
	{
		$display_gender = "Grooms";
	}
	else
	if($gender=='2')
	{
		$display_gender = "Brides";
	}
	else
	if($gender=='3')
	{
		$display_gender = "Others";
	}
	else
	{
		$display_gender = "All genders";
	}


	if($marital_status=='1')
	{
		$display_marital_status = "Never Married";
	}
	else
	if($marital_status=='2')
	{
		$display_marital_status = "Awaiting Divorce";
	}
	else
	if($marital_status=='3')
	{
		$display_marital_status = "Divorced";
	}
	else
	if($marital_status=='4')
	{
		$display_marital_status = "Widowed";
	}
	else
	if($marital_status=='5')
	{
		$display_marital_status = "Annulled";
	}
	else
	{
		$display_marital_status = "All marital status";
	}
?>

<title>
	View Members -  <?php echo getWebsiteTitle(); ?>
</title>

	<section role="main" class="content-body update-section">

		<div class="row admin-start-section">
			<div class="col-md-12 col-lg-12 col-xl-12">
				<h2><?php echo $display_gender.' With '.$display_marital_status.' Status.';?> </h2>
			</div>

			<input type="hidden" class="gender" value="<?php echo $gender;?>" data-column="4">
			<input type="hidden" class="marital_status" value="<?php echo $marital_status;?>" data-column="7">

			<div class="col-md-12 col-lg-12 col-xl-12">
				<table id='datatable-info' class="table-hover table-striped table-bordered datatable view_member_list">
					<thead>
						<tr>
							<th>Id</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Profile Id</th>
							<th>Gender</th>
							<th>Email</th>
							<th>Mobile</th>
							<th>Marital Status</th>
							<th>Status</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</section>
</div>
</section>

<?php
	include("templates/footer.php");
?>

<script>
$(document).ready(function(){
	var dataTable = $('.view_member_list').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"datatables/member-attributes/view_members_response.php", // json datasource
            type: "post",  // type of method  ,GET/POST/DELETE
            error: function(){
                $(".view_member_list_processing").css("display","none");
            }
        }
    });
});
</script>