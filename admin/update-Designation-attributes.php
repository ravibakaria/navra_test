<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body update-section">
		<a href="master-data-setup.php?main_tab=ProfessionalAttributes&sub_tab=Designation" id="portletReset" type="button" class="mb-xs mt-xs mr-xs btn btn-default" style="float:right;"><i class="fa fa-arrow-left"></i> Back</a>

		<?php
			$id = quote_smart($_GET['id']);

			if ( !is_numeric( $id ) )
		    {
		        echo 'Error! Invalid Data';
		        exit;
		    }

		    $sql = "SELECT * FROM `designation` WHERE `id`='$id'";
		    $stmt1   = $link->prepare($sql);
            $stmt1->execute();
            $queryTot = $stmt1->rowCount();
            $result = $stmt1->fetch();

           	if($queryTot>0)
           	{
           		$name = $result['name'];
           		$designation_category = $result['designation_category'];
           		$status = $result['status'];
		?>
		<br/><br/>
		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-md-3 col-lg-3">
					</div>
					<div class="col-md-6 col-lg-6 edit-master-div">
						<div class="row"><br>
							<center><h4>Update <strong><?php echo $name;?></strong> Designation Information</h4></center>
						</div>
							<hr/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Designation Category:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<select class='form-control designation_category'>
									<option value="0">--Designation Category--</option>option>
									<?php
										$sql2 = "SELECT * FROM `designation_category`";
									    $stmt2   = $link->prepare($sql2);
							            $stmt2->execute();
							            $queryTot2 = $stmt1->rowCount();
							            $result2 = $stmt2->fetchAll();

							            foreach ($result2 as $row) 
							            {
							            	$designation_category_id = $row['id'];
							            	$designation_category_name = $row['name'];

							            	if($designation_category == $designation_category_id)
							            	{
							            		echo "<option value='".$designation_category_id."' selected>".$designation_category_name."</option>";
							            	}
							            	else
							            	{
							            		echo "<option value='".$designation_category_id."'>".$designation_category_name."</option>";
							            	}
							            }
									?>
								</select>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Designation:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control form-control-sm Designation_name" value="<?php echo $name; ?>" />
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Status:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<center>
								<?php
									if($status=='0')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Designation_current_status' name='Designation_current_status' value='".$status."'>";
									}
									else
									if($status=='1')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Designation_current_status' name='Designation_current_status' checked value='".$status."'>";
									}
								?>
							</center>
							</div>
						</div>
						<br/>
						<input type="hidden" class="Designation_id" value="<?php echo $id;?>">

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 update_status">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
							</div>	
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><button class="btn btn-success btn_update_Designation website-button">Update</button></center><br/>	
							</div>
						</div>
						
					</div>
					<div class="col-md-3 col-lg-3">
					</div>
				</div>
				<div class="row">
					<br/>
				</div>
			</div>
			
		</div>
		<?php
			}
			else
			{
				echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
			}
		?>
</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		$('.btn_update_Designation').click(function(){
			var name = $('.Designation_name').val();
			var designation_category = $('.designation_category').val();
			var status = $('.Designation_current_status').is(":checked");
			if(status==true)
			{
				status='1';
			}
			else
			if(status==false)
			{
				status='0';
			}

			var id = $('.Designation_id').val();
			var task = "Update_Designation_Attributes";

			if(name=='' || name==null)
			{
				$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter Designation.</div></center>");
        			return false;
			}

			var data = 'name='+name+'&designation_category='+designation_category+'&status='+status+'&id='+id+'&task='+task;
			$('.loading_img').show();
			
			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/master-data/professional-attributes/Designation-helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data updated successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});
	});
</script>