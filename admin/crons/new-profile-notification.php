<?php
	$sql=$count=$result=$row=$incr=$newly_added_profiles=null;

	$sql = "SELECT * FROM clients WHERE status='1'";
	$stmt = $link->prepare($sql);
	$stmt->execute();
	$count = $stmt->rowCount();

	if($count>0)
	{
		$result = $stmt->fetchAll();

		foreach ($result as $row) 
		{
			$userid = $row['id'];
    		$unique_code = $row['unique_code'];
    		$first_name = $row['firstname'];
			$email_address = $row['email'];
			$gender = $row['gender'];

			if($NewProfileReminderFrom=='daily')
			{
				$current_date = date('Y-m-d H:i:s');
				$last_date = strtotime("-24 hours", strtotime($current_date));
		    	$last_date = date('Y-m-d H:i:s', $last_date);				
			}
			else
			if($NewProfileReminderFrom=='weekly')
			{
				$current_date = date('Y-m-d H:i:s');
				$last_date = strtotime("-7 days", strtotime($current_date));
		    	$last_date = date('Y-m-d H:i:s', $last_date);
			}
			else
			if($NewProfileReminderFrom=='monthly')
			{
				$current_date = date('Y-m-d H:i:s');
				$last_date = strtotime("-30 days", strtotime($current_date));
		    	$last_date = date('Y-m-d H:i:s', $last_date);
			}

			if($gender=='1')
	    	{
	    		$search_gender = '2';
	    		$sql_new_profiles = "SELECT * FROM clients WHERE status='1' AND gender='2' AND id<>'$userid' AND created_at BETWEEN '$last_date' AND '$current_date' ORDER BY created_at DESC LIMIT 0,10";
	    	}
	    	else
	    	if($gender=='2')
	    	{
	    		$search_gender = '1';
	    		$sql_new_profiles = "SELECT * FROM clients WHERE status='1' AND gender='1' AND id<>'$userid' AND created_at BETWEEN '$last_date' AND '$current_date' ORDER BY created_at DESC LIMIT 0,10";
	    	}
	    	else
	    	if($gender=='3')
	    	{
	    		$search_gender = '3';
	    		$sql_new_profiles = "SELECT * FROM clients WHERE status='1' AND gender='3' AND id<>'$userid' AND created_at BETWEEN '$last_date' AND '$current_date' ORDER BY created_at DESC LIMIT 0,10";
	    	}
    
	    	$stmt_new_profiles = $link->prepare($sql_new_profiles);
			$stmt_new_profiles->execute();
			$count_new_profiles = $stmt_new_profiles->rowCount();

			if($count_new_profiles>0)
			{
				$result_new_profiles = $stmt_new_profiles->fetchAll();
				$incr=0;

				$newly_added_profiles .= "<tr>
        									<td style='padding: 10px; background-color: #ffffff;''>
        										<table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'>";
				foreach ($result_new_profiles as $row_new_profiles) 
				{
					$new_user_id = $row_new_profiles['id'];

					$firstname = getUserFirstName($new_user_id);
					$profilePic = getUserProfilePic($new_user_id);
					$ProfileId = getUserUniqueCode($new_user_id);
					$City = getUserCity($new_user_id);
					$State = getUserState($new_user_id);
					$Country = getUserCountry($new_user_id);
					$Age = getUserAge($new_user_id);
					$Education = getUserEducation($new_user_id);

					if($profilePic!='' || $profilePic!=null)
					{
						$profilePic_show = $WebSiteBasePath.'/users/uploads/'.$new_user_id.'/profile/'.$profilePic;
					}
					else
					{
						$profilePic_show = $WebSiteBasePath.'/images/no_profile_pic.png';
					}

					$profile_link = $WebSiteBasePath.'/users/show-profile.php?search=Profile-'.$ProfileId;

					if($incr==0 ||$incr%2==0)
					{
						$newly_added_profiles .= "<tr>";
					}

					$newly_added_profiles .= "<td valign='top' width='50%' class='stack-column-center'>
							<center>
								<div class='card-my-matches'>
									<a href='$profile_link' target='_blank' style='color:black;text-decoration:none;'>
										<img src='$profilePic_show' class='profile_img member-profile-image' alt='$firstname' title='$firstname' style='width:100%;height:auto;'/>
										<h4><b><strong>".ucwords($firstname)."</strong></b></h4>
										<p class='title'>
											Profile ID: $ProfileId <br/>
										</p>
										City: $City<br/>
										State: $State<br/>
										Country: $Country<br/>
										Age: $Age Years<br/>
										Education: $Education<br/>
									</a>
									<br/>
									<a href='profile_link'><button  style='background-color:$primary_color;color:$primary_font_color;padding:5px;'><b>View Profile</b></button>
									</a>
								</div>
							</center>
						</td>";
					$incr++;
					if($incr>0 && $incr%2==0)
					{
						$newly_added_profiles .= "</tr>";
					}
				}

				if($count_new_profiles%2!=0)
		        {
		        	$newly_added_profiles .= "</tr>";
		        }

		        $newly_added_profiles .= "</table></td></tr>";

		        $newly_added_profiles .= "<br/><br/>";

				$newly_added_profiles .= "
				<center><a href='$WebSiteBasePath/users/my_chat_messages.php' target='_blank'><button style='background-color:$primary_color;color:$primary_font_color;padding:5px;font-size: x-large;'>View All</button></a></center>";
			}
			else
			{
				continue;
			}
            
            
			$my_preference_page_link = "<a href='$WebSiteBasePath/users/my-matches.php' target='_blank'>Click Here</a> to view your matched profiles.";

			$SocialSharing = getSocialSharingLinks();   // social sharing links

			$NewProfileNotificationSubject = str_replace('$site_name',$WebSiteTitle,$NewProfileNotificationSubject);   //replace subject variables with value

        	$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

			$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

			$NewProfileNotificationMessage = str_replace(array('$first_name','$site_name','$newly_added_profiles','$my_preference_page_link','$signature'),array($first_name,$WebSiteTitle,$newly_added_profiles,$my_preference_page_link,$GlobalEmailSignature),$NewProfileNotificationMessage);  //replace footer variables with value

			$subject = $NewProfileNotificationSubject;
                
            $message  = '<!DOCTYPE html>';
            $message .= '<html lang="en">
                <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width">
                <title></title>
                <style type="text/css">'.$EmailCSS.'</style>
                </head>
                <body style="margin: 0; padding: 0;">';
			
			$message .= $EmailGlobalHeader;

			$message .= $NewProfileNotificationMessage;

			$message .= $EmailGlobalFooter;
			
			$mailto = $email_address;
			$mailtoname = $first_name;
			
			
			$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

			if($emailResponse == 'success')
			{
				$info = "success";
			}
			else
			{
				$info = "Error in sending email";
			}

			$NewProfile_log_data = date('Y-m-d H:i:s') . " ~  User_id ~  " . $userid .  " ~  Unique_id ~  " . $unique_code . " ~ " . $first_name . " ~ " . $email_address . " ~ " ;
			$NewProfile_log_data .= $info . " ~ " ;
			$NewProfile_log_data .= str_pad('',4096)."\n";

			$NewProfileFileName='/home/navrabayko/public_html/admin/crons/logs/New-Profile-Notification-'.date('M-Y').'.log';  
            //open the file append mode,dats the log file will create day wise  
            $NewProfileHandler=fopen($NewProfileFileName,'a+');  
            //write the info into the file  
            fwrite($NewProfileHandler,$NewProfile_log_data);  
            //close handler  
            fclose($NewProfileHandler);

        break;	
		}
	}
?>