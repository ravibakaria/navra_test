<?php
	$stmt   = $link->prepare("SELECT * FROM `cron_jobs`");
	$stmt->execute();
	$result = $stmt->fetch();
	$count=$stmt->rowCount();

	$EmailVerificationReminderRunOrNot = $result['EmailVerificationReminderRunOrNot'];
    $EmailVerificationReminderRunAt = $result['EmailVerificationReminderRunAt'];
    $ProfileCompletenessReminderRunOrNot = $result['ProfileCompletenessReminderRunOrNot'];
    $ProfileCompletenessRunAt = $result['ProfileCompletenessRunAt'];
    $BirthdayReminderRunOrNot = $result['BirthdayReminderRunOrNot'];
    $BirthdayReminderRunAt = $result['BirthdayReminderRunAt'];
    $NewProfileReminderRunOrNot = $result['NewProfileReminderRunOrNot'];
    $NewProfileReminderFrom = $result['NewProfileReminderFrom'];
    $NewProfileReminderRunAt = $result['NewProfileReminderRunAt'];
    $UnreadMessageReminderRunOrNot = $result['UnreadMessageReminderRunOrNot'];
    $UnreadMessageReminderRunAt = $result['UnreadMessageReminderRunAt'];
    $MembershipRenewalReminderRunOrNot = $result['MembershipRenewalReminderRunOrNot'];
    $MembershipRenewalReminderRunAt = $result['MembershipRenewalReminderRunAt'];

    /*************   Email verificatio reminder end at   ***********/
    $EmailVerificationReminderEndAt = strtotime("+5 minutes", strtotime($EmailVerificationReminderRunAt));
    $EmailVerificationReminderEndAt = date('H:i:s', $EmailVerificationReminderEndAt);

    /*************   Profile Completeness reminder end at   ***********/
    $ProfileCompletenessReminderEndAt = strtotime("+5 minutes", strtotime($ProfileCompletenessRunAt));
    $ProfileCompletenessReminderEndAt = date('H:i:s', $ProfileCompletenessReminderEndAt);

    /*************   Birthday reminder end at   ***********/
    $BirthdayReminderEndAt = strtotime("+5 minutes", strtotime($BirthdayReminderRunAt));
    $BirthdayReminderEndAt = date('H:i:s', $BirthdayReminderEndAt);

    /*************   New Profile Notification end at   ***********/
    $NewProfileReminderEndAt = strtotime("+5 minutes", strtotime($NewProfileReminderRunAt));
    $NewProfileReminderEndAt = date('H:i:s', $NewProfileReminderEndAt);

    /*************   New Message Received Notification end at   ***********/
    $UnreadMessageReminderEndAt = strtotime("+5 minutes", strtotime($UnreadMessageReminderRunAt));
    $UnreadMessageReminderEndAt = date('H:i:s', $UnreadMessageReminderEndAt);
?>