<?php
	$sql=$count=$result=$row=null;

    $sql = "SELECT * FROM clients WHERE status='0'";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    $count = $stmt->rowCount();

    if($count>0)
    {
    	$result = $stmt->fetchAll();

    	foreach ($result as $row) 
    	{
    	    $info = null;
    		$id = $row['id'];
    		$unique_code = $row['unique_code'];
    		$created_at = $row['created_at'];
    		$registration_date = strtotime($created_at);
    		$today = time();
			$datediff = $today - $registration_date;

			$datediff = round($datediff / (60 * 60 * 24));

    		if($datediff<=7)
    		{
    			$first_name = $row['firstname'];
	    		$email_address = $row['email'];
	    		$passcode = encrypt_decrypt('encrypt', $email_address);

	    		$verification_link = "<a href='$WebSiteBasePath/verify-email.php?passcode=$passcode'><button>Verify</button></a>";

	        	$verification_text_link = "<a href='$WebSiteBasePath/verify-email.php?passcode=$passcode'>$WebSiteBasePath/verify-email.php?passcode=$passcode</a>";

	        	$SocialSharing = getSocialSharingLinks();   // social sharing links

	        	$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

				$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

				$ReminderEmailVerificationMessage = str_replace(array('$first_name','$site_name','$verification_link','$verification_text_link','$signature'),array($first_name,$WebSiteTitle,$verification_link,$verification_text_link,$GlobalEmailSignature),$ReminderEmailVerificationMessage);  //replace footer variables with value

				$subject = $ReminderEmailVerificationSubject;
                
                $message  = '<!DOCTYPE html>';
                $message .= '<html lang="en">
                    <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width">
                    <title></title>
                    <style type="text/css">'.$EmailCSS.'</style>
                    </head>
                    <body style="margin: 0; padding: 0;">';
				
                $message .= $EmailGlobalHeader;

				$message .= $ReminderEmailVerificationMessage;

				$message .= $EmailGlobalFooter;
				
				$mailto = $email_address;
				$mailtoname = $first_name;
				$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

				if($emailResponse == 'success')
				{
					$info = "success";
				}
				else
				{
					$info = "Error in sending verification link";
				}
                
                $log_for = " Email Sent ";
    		}
    		else
    		if($datediff>7)
    		{
    			$sql_delete = "DELETE FROM clients WHERE id='$id'";
                $sql_delete_address = "DELETE FROM address WHERE userid='$id'";

    			if($link->exec($sql_delete) && $link->exec($sql_delete_address) )
    			{
    				$info = "Success";
    			}
    			else
    			{
    				$info = "Error in deleting record";
    			}
    
                $log_for = " Record Deleted ";
               
    		}
    		
    		$log_data = $log_for . " ~ " . date('Y-m-d H:i:s') . " ~  User_id ~  " . $id .  " ~  Unique_id ~  " . $unique_code . " ~ " . $first_name . " ~ " . $email_address . " ~ " ;
			$log_data .= $info . " ~ " ;
			$log_data .= str_pad('',4096)."\n";
		    
		    $EmailVerificationFileName='/home/navrabayko/public_html/admin/crons/logs/email-verification-reminder-'.date('M-Y').'.log';  
            //open the file append mode,dats the log file will create day wise  
            $EmailVerificationFileHandler=fopen($EmailVerificationFileName,'a+');  
            //write the info into the file  
            fwrite($EmailVerificationFileHandler,$log_data);  
            //close handler  
            fclose($EmailVerificationFileHandler);			
    	}
    }
?>