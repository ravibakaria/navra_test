<?php
	$sql=$count=$result=$row=$incr=$new_message_member_profiles=null;
	
	$sql = "SELECT * FROM clients WHERE status='1'";
	$stmt = $link->prepare($sql);
	$stmt->execute();
	$count = $stmt->rowCount();

	if($count>0)
	{
		$result = $stmt->fetchAll();

		foreach ($result as $row) 
		{
			$userid_new_message = array();
			$userid = $row['id'];
    		$unique_code = $row['unique_code'];
    		$first_name = $row['firstname'];
			$email_address = $row['email'];

			$last_login = getLastUserLoginDate($userid);

			$stmt_new_message = "SELECT * FROM `messagechat` WHERE `userTo`='$userid' AND (messagedOn>='$last_login')"; 
	        $stmt_new_message = $link->prepare($stmt_new_message);
	        $stmt_new_message->execute();
	        $count_new_message = $stmt_new_message->rowCount();
	        $result_new_message = $stmt_new_message->fetchAll();
	        if($count_new_message>0)
	        {
	        	$incr=0;
	        	foreach ($result_new_message as $row_new_message) 
		        {
		        	if (in_array($row_new_message['userFrom'], $userid_new_message))
		        	{
		        		continue;
		        	}
		        	else
		        	{
		        		$userid_new_message[] = $row_new_message['userFrom'];
		        	}
		        }

		        $new_message_member_profiles .= "<tr>
		        									<td style='padding: 10px; background-color: #ffffff;''>
		        										<table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'>";


		        for($i=0;$i<count($userid_new_message);$i++)
		        {
		        	if($i==10)
		        	{
		        		break;
		        	}

		        	$count_new_messages = $count_new_message;
		        	$count_members = count($userid_new_message);

		        	$new_user_id = $userid_new_message[$i];

		        	$firstname = getUserFirstName($new_user_id);
					$profilePic = getUserProfilePic($new_user_id);
					$ProfileId = getUserUniqueCode($new_user_id);
					$City = getUserCity($new_user_id);
					$State = getUserState($new_user_id);
					$Country = getUserCountry($new_user_id);
					$Age = getUserAge($new_user_id);
					$Education = getUserEducation($new_user_id);

					if($profilePic!='' || $profilePic!=null)
					{
						$profilePic_show = $WebSiteBasePath.'/users/uploads/'.$new_user_id.'/profile/'.$profilePic;
					}
					else
					{
						$profilePic_show = $WebSiteBasePath.'/images/no_profile_pic.png';
					}

					$profile_link = $WebSiteBasePath.'/users/show-profile.php?search=Profile-'.$ProfileId;

					if($incr==0 ||$incr%2==0)
					{
						$new_message_member_profiles .= "<tr>";
					}

					$new_message_member_profiles .= "<td valign='top' width='50%' class='stack-column-center'>
							<center>
								<div class='card-my-matches'>
									<a href='$profile_link' target='_blank' style='color:black;text-decoration:none;'>
										<img src='$profilePic_show' class='profile_img member-profile-image' alt='$firstname' title='$firstname' style='width:100%;height:auto;'/>
										<h4><b><strong>".ucwords($firstname)."</strong></b></h4>
										<p class='title'>
											Profile ID: $ProfileId <br/>
										</p>
										City: $City<br/>
										State: $State<br/>
										Country: $Country<br/>
										Age: $Age Years<br/>
										Education: $Education<br/>
									</a>
									<br/>
									<a href='profile_link'><button  style='background-color:$primary_color;color:$primary_font_color;padding:5px;'><b>View Profile</b></button>
									</a>
								</div>
							</center>
						</td>";
					$incr++;
					if($incr>0 && $incr%2==0)
					{
						$new_message_member_profiles .= "</tr>";
					}
		        }

		        if(count($userid_new_message)%2!=0)
		        {
		        	$new_message_member_profiles .= "</tr>";
		        }

		        $new_message_member_profiles .= "</table></td></tr>";

		        $new_message_member_profiles .= "<br/><br/>";

		        $new_message_member_profiles .= "
				<center><a href='$WebSiteBasePath.'/users/my_chat_messages.php' target='_blank'><button style='background-color:$primary_color;color:$primary_font_color;padding:5px;font-size: x-large;'>View All</button></a></center>";

				$SocialSharing = getSocialSharingLinks();   // social sharing links

				$NewMessageNotificationSubject = str_replace('$site_name',$WebSiteTitle,$NewMessageNotificationSubject);   //replace subject variables with value

	        	$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

				$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

				$NewMessageNotificationMessage = str_replace(array('$first_name','$count_new_messages','$count_members','$site_name','$new_message_member_profiles','$signature'),array($first_name,$count_new_messages,$count_members,$WebSiteTitle,$new_message_member_profiles,$GlobalEmailSignature),$NewMessageNotificationMessage);  //replace footer variables with value

				$subject = $NewMessageNotificationSubject;
                
                $message  = '<!DOCTYPE html>';
                $message .= '<html lang="en">
                    <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width">
                    <title></title>
                    <style type="text/css">'.$EmailCSS.'</style>
                    </head>
                    <body style="margin: 0; padding: 0;">';
				
				$message .= $EmailGlobalHeader;

				$message .= $NewMessageNotificationMessage;

				$message .= $EmailGlobalFooter;
				
				$mailto = $email_address;
				$mailtoname = $first_name;
                
				$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

				if($emailResponse == 'success')
				{
					$info = "success";
				}
				else
				{
					$info = "Error in sending email";
				}

				$NewMessage_log_data = date('Y-m-d H:i:s') . " ~  User_id ~  " . $userid .  " ~  Unique_id ~  " . $unique_code . " ~ " . $first_name . " ~ " . $email_address . " ~ " ;
				$NewMessage_log_data .= $info . " ~ " ;
				$NewMessage_log_data .= str_pad('',4096)."\n";

				$NewMessageFileName='/home/navrabayko/public_html/admin/crons/logs/New-Profile-Notification-'.date('M-Y').'.log';  
	            //open the file append mode,dats the log file will create day wise  
	            $NewMessageHandler=fopen($NewMessageFileName,'a+');  
	            //write the info into the file  
	            fwrite($NewMessageHandler,$NewMessage_log_data);  
	            //close handler  
	            fclose($NewMessageHandler);
		    }
		    else
		    {
		    	continue;
		    }

			break;
		}
	}
?>