<?php
	$sql=$count=$result=$row=null;
	
	$sql = "SELECT * FROM clients WHERE MONTH(dob) = MONTH(NOW()) AND DAY(dob) = DAY(NOW()) AND status='1'";
	$stmt = $link->prepare($sql);
	$stmt->execute();
	$count = $stmt->rowCount();

	if($count>0)
	{
		$result = $stmt->fetchAll();

		foreach ($result as $row) 
		{
			$userid = $row['id'];
    		$unique_code = $row['unique_code'];
    		$first_name = $row['firstname'];
			$email_address = $row['email'];

			$SocialSharing = getSocialSharingLinks();   // social sharing links

			$BirtdayWishSubject = str_replace('$first_name',$first_name,$BirtdayWishSubject);   //replace subject variables with value

        	$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

			$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

			$BirtdayWishMessage = str_replace(array('$first_name','$site_name','$signature'),array($first_name,$WebSiteTitle,$GlobalEmailSignature),$BirtdayWishMessage);  //replace footer variables with value

			$subject = $BirtdayWishSubject;
                
            $message  = '<!DOCTYPE html>';
            $message .= '<html lang="en">
                <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width">
                <title></title>
                <style type="text/css">'.$EmailCSS.'</style>
                </head>
                <body style="margin: 0; padding: 0;">';			

			$message .= $EmailGlobalHeader;

			$message .= $BirtdayWishMessage;

			$message .= $EmailGlobalFooter;
			
			$mailto = $email_address;
			$mailtoname = $first_name;
			
			$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

			if($emailResponse == 'success')
			{
				$info = "success";
			}
			else
			{
				$info = "Error in sending email";
			}

			$log_data = date('Y-m-d H:i:s') . " ~  User_id ~  " . $userid .  " ~  Unique_id ~  " . $unique_code . " ~ " . $first_name . " ~ " . $email_address . " ~ " ;
			$log_data .= $info . " ~ " ;
			$log_data .= str_pad('',4096)."\n";

			$BirthdayWishFileName='/home/navrabayko/public_html/admin/crons/logs/birthday-wish-'.date('M-Y').'.log';  
            //open the file append mode,dats the log file will create day wise  
            $birthdayWishHandler=fopen($BirthdayWishFileName,'a+');  
            //write the info into the file  
            fwrite($birthdayWishHandler,$log_data);  
            //close handler  
            fclose($birthdayWishHandler);
		}
	}
?>