<?php
	ob_start();
	require_once '../../config/config.php'; 
	include('../../config/dbconnect.php');    //database connection
	include('../../config/functions.php');   //strip query string
    include('../../config/setup-values.php');   //get master setup values
    include('../../config/email/email_style.php');   //get master setup values
    include('../../config/email/email_templates.php');   //get master setup values
    include('../../config/email/email_process.php');
    include('cron-job-scheduler.php');

	$DefaultTimezone = getDefaultTimeZone();
  	if($DefaultTimezone!='' || $DefaultTimezone!=null)
  	{
    	date_default_timezone_set("$DefaultTimezone");
  	}
  	else
  	{
    	date_default_timezone_set("Asia/Calcutta");
  	}

    $WebSiteBasePath = getWebsiteBasePath();
    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='logo-img'/>";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }
    
    $day = date('D', strtotime(date('Y-m-d')));

    $today = date('d');

    $curr_time = date('H:i:s');      /*****       Current time        *****/

    include("new-message-notification.php");

    exit;

    $CronLogFileName='/home/navrabayko/public_html/admin/crons/logs/Cron-Logs-'.date('Y').'.log';  
    //open the file append the log file will create day wise  
    $fHandler=fopen($CronLogFileName,'a+'); 
    
    /*****       Email Verification Reminder start        *****/
    if($EmailVerificationReminderRunOrNot=='1' && ($curr_time>$EmailVerificationReminderRunAt && $curr_time<$EmailVerificationReminderEndAt))
    {
        include("/home/navrabayko/public_html/admin/crons/email-verification-reminder.php");
        $email_verification_log = " EMail verification reminder Sent   ~  ".date('Y-m-d'). " ~ ".$curr_time . " ~ \n";
        fwrite($fHandler,$email_verification_log);
    }
    /*****       Email Verification Reminder end        *****/
    
    /*****       Email Verification Reminder start        *****/
    if($ProfileCompletenessReminderRunOrNot=='1' && ($curr_time>$ProfileCompletenessRunAt && $curr_time<$ProfileCompletenessReminderEndAt))
    {
        include("/home/navrabayko/public_html/admin/crons/profile-completeness-reminder.php");
        $profile_completeness_log = " Profile completeness reminder Sent   ~  ".date('Y-m-d'). " ~ ".$curr_time . " ~ \n";
        fwrite($fHandler,$profile_completeness_log);
    }
    /*****       Email Verification Reminder end        *****/

    /*****       Birthday Reminder start        *****/
    if($BirthdayReminderRunOrNot=='1' && ($curr_time>$BirthdayReminderRunAt && $curr_time<$BirthdayReminderEndAt))
    {
        include("/home/navrabayko/public_html/admin/crons/member-birthday-reminder.php");
        $birthday_reminder_log = " Birthday reminder Sent   ~  ".date('Y-m-d'). " ~ ".$curr_time . " ~ \n";
        fwrite($fHandler,$birthday_reminder_log);
    }
    /*****       Birthday Reminder end        *****/


    /*****       New Profile Notification start        *****/
    if($NewProfileReminderRunOrNot=='1')
    {
        if($NewProfileReminderFrom=='daily' && ($curr_time>$NewProfileReminderRunAt && $curr_time<$NewProfileReminderEndAt) )
        {
            include("/home/navrabayko/public_html/admin/crons/new-profile-notification.php");
            $new_profile_reminder_log = " New Profile reminder Sent   ~  ".date('Y-m-d'). " ~ ".$curr_time . " ~ \n";
            fwrite($fHandler,$new_profile_reminder_log);
        }
        else
        if($NewProfileReminderFrom=='weekly' && $day=='Sun' && ($curr_time>$NewProfileReminderRunAt && $curr_time<$NewProfileReminderEndAt) )
        {
            include("/home/navrabayko/public_html/admin/crons/new-profile-notification.php");
            $new_profile_reminder_log = " New Profile reminder Sent   ~  ".date('Y-m-d'). " ~ ".$curr_time . " ~ \n";
            fwrite($fHandler,$new_profile_reminder_log);
        }
        else
        if($NewProfileReminderFrom=='monthly' && $today=='1' && ($curr_time>$NewProfileReminderRunAt && $curr_time<$NewProfileReminderEndAt) )
        {
            include("/home/navrabayko/public_html/admin/crons/new-profile-notification.php");
            $new_profile_reminder_log = " New Profile reminder Sent   ~  ".date('Y-m-d'). " ~ ".$curr_time . " ~ \n";
            fwrite($fHandler,$new_profile_reminder_log);
        }
    }
    /*****       Birthday Reminder end        *****/
    
        $new_message_notification_log = " New Message Notification Sent   ~  ".date('Y-m-d'). " ~ ".$curr_time . " ~ \n";
    /*****       New Message Notification start        *****/
    if($UnreadMessageReminderRunOrNot=='1' && ($curr_time>$UnreadMessageReminderRunAt && $curr_time<$UnreadMessageReminderEndAt))
    {
        include("/home/navrabayko/public_html/admin/crons/new-message-notification.php");
        $new_message_notification_log = " New Message Notification Sent   ~  ".date('Y-m-d'). " ~ ".$curr_time . " ~ \n";
        fwrite($fHandler,$new_message_notification_log);
    }
    /*****       Birthday Reminder end        *****/
    
    ob_end_flush();
?>