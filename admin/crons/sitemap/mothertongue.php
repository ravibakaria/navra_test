<?php
    /*****       MotherTongue Sitemap generator start        *****/
        $file_mothertongue = "../../../mother-tongue-sitemap.xml";
        
        $query_mothertongue  = "SELECT A.*,B.* FROM profilereligion AS A JOIN mothertongue AS B ON A.mother_tongue=B.id GROUP BY A.mother_tongue";
        $stmt_mothertongue   = $link->prepare($query_mothertongue);
        $stmt_mothertongue->execute();
        $result_mothertongue = $stmt_mothertongue->fetchAll();
        $count_mothertongue = $stmt_mothertongue->rowCount();

        if($count_mothertongue>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_mothertongue as $row_mothertongue)
            {
                $mother_tongue = $row_mothertongue['id'];                 
                $mother_tongue_name_search = $row_mothertongue['name'];

                $mother_tongue_name = str_replace(' ','_',$mother_tongue_name_search);

                $query  = "SELECT A.*,B.* FROM profilereligion AS A JOIN clients AS B ON A.userid=B.id WHERE A.mother_tongue='$mother_tongue' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-MotherTongue-'.$mother_tongue_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_mothertongue = fopen($file_mothertongue, "w");      

            fwrite($file_mothertongue, $xml);
            
            fclose($file_mothertongue);
        }
        else
        if($count_mothertongue==0)
        {
            if (file_exists($file_mothertongue)) 
            {
                unlink($file_mothertongue);
            }
        }
        
    /*****       MotherTongue Sitemap generator end        *****/
?>