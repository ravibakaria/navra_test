<?php
    /*****       Education Sitemap generator start        *****/
        $file_education = "../../../education-sitemap.xml";
        
        $query_education  = "SELECT A.*,B.* FROM eduocc AS A JOIN educationname AS B ON A.education=B.id GROUP BY A.education";
        $stmt_education   = $link->prepare($query_education);
        $stmt_education->execute();
        $result_education = $stmt_education->fetchAll();
        $count_education = $stmt_education->rowCount();

        if($count_education>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_education as $row_education)
            {
                $education = $row_education['id'];                 
                $education_name_search = $row_education['name'];

                $education_name = str_replace(' ','_',$education_name_search);

                $query  = "SELECT A.*,B.* FROM eduocc AS A JOIN clients AS B ON A.userid=B.id WHERE A.education='$education' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-Education-'.$education_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_education = fopen($file_education, "w");      

            fwrite($file_education, $xml);
            
            fclose($file_education);
        }
        else
        if($count_education==0)
        {
            if (file_exists($file_education)) 
            {
                unlink($file_education);
            }
        }
        
    /*****       Education Sitemap generator end        *****/
?>