<?php
	ob_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string
    include('../../../config/setup-values.php');   //get master setup values

    $i = null; 
    $day = date('D', strtotime(date('Y-m-d')));

    $today = date('d');

    $curr_time = date('H:i:s');      /*****       Current time        *****/

    $date = date('d-m-Y');

    $websiteBasePath = getWebsiteBasePath();

    $files = glob('../../../*.xml');


    $SitemapLogFileName='logs/Sitemap-Logs-'.date('M-Y').'.log';  
    //open the file append the log file will create day wise  
    $fHandler=fopen($SitemapLogFileName,'a+'); 

    $SitemapGenerationEnableOrNot = getSitemapGenerationEnableOrNot();
    $SitemapGenerationRunAt = getSitemapGenerationTime();

    /*************   Email verificatio reminder end at   ***********/
    $SitemapGenerationEndAt = strtotime("+115 minutes", strtotime($SitemapGenerationRunAt));
    $SitemapGenerationEndAt = date('H:i:s', $SitemapGenerationEndAt);

    /***********   Wedding directory display or not   ***********/
    $wedding_directory_display_or_not = getWeddingDirectoryDisplayOrNot();

    /*****       Sitemap generator start        *****/
    if($SitemapGenerationEnableOrNot=='1' && ($curr_time>$SitemapGenerationRunAt && $curr_time<$SitemapGenerationEndAt))
    {
        include "pages.php";
        include "grooms.php";
        include "brides.php";
        include "other-genders.php";
        include "city.php";
        include "state.php";
        include "country.php";
        include "religion.php";
        include "caste.php";
        include "mothertongue.php";
        include "education.php";
        include "occupation.php";
        include "body-type.php";
        include "complexion.php";
        include "marital-status.php";
        include "eating-habit.php";
        include "smoking-habit.php";
        include "drinking-habit.php";

        if($wedding_directory_display_or_not=='1')
        {
            include "wedding-directory.php";
            include "vendors.php";
        }

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
        <?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            <sitemap>
            <loc>'.$websiteBasePath.'/pages-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>';

        if($count_grooms>0)
        {
            $xml .= '<sitemap>
            <loc>'.$websiteBasePath.'/grooms-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>';
        }
        
        if($count_brides>0)
        {
            $xml .= '<sitemap>
            <loc>'.$websiteBasePath.'/brides-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>';
        }   
        
        if($count_other_genders>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/other-gender-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        }

        if($count_city>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/city-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        } 

        if($count_state>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/state-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        } 

        if($count_country>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/country-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        } 

        if($count_religion>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/religion-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        } 

        if($count_caste>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/caste-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        }    

        if($count_mothertongue>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/mother-tongue-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        }   

        if($count_education>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/education-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        }      

        if($count_occupation>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/occupation-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        }       

        if($count_bodytype>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/body-type-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        }        

        if($count_complexion>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/complexion-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        } 

        if($count_maritalstatus>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/marital-status-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        }           

        if($count_eatinghabbit>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/eating-habbit-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        }         

        if($count_smokinghabit>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/smoking-habit-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        }        

        if($count_drinkinghabit>0)
        {
            $xml .='<sitemap>
            <loc>'.$websiteBasePath.'/drinking-habit-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';
        }  

        if($wedding_directory_display_or_not=='1' && $count_wedding_directory>0)
        {
            $xml .= '<sitemap>
            <loc>'.$websiteBasePath.'/wedding-directory-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>';
        }

        if($wedding_directory_display_or_not=='1' && $count_vendor>0)
        {
            $xml .= '<sitemap>
            <loc>'.$websiteBasePath.'/vendors-sitemap.xml</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>';
        }   

		$xml .= '</sitemapindex>'; 

        $file= fopen("../../../sitemap.xml", "w");

        fwrite($file, $xml);
        
        fclose($file);

        $sitemap_generator_log = " Sitemap generated at   ~  ".date('Y-m-d'). " ~ ".$curr_time . " ~ \n";
        fwrite($fHandler,$sitemap_generator_log);
    }
    else
    if($SitemapGenerationEnableOrNot!='1') 
    {
        for($i=0;$i<count($files);$i++)
        {
            if (file_exists($files[$i])) 
            {
                unlink($files[$i]);
            }
        }
        
        $sitemap_generator_log = " Sitemap deleted at   ~  ".date('Y-m-d'). " ~ ".$curr_time . " ~ \n";
        fwrite($fHandler,$sitemap_generator_log);
    }   
    /*****       Sitemap generator end        *****/
?>