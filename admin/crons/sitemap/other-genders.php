<?php
	/*****       Other Genders Sitemap generator start        *****/

        $file_other_genders = "../../../other-gender-sitemap.xml";

        $query_other_genders  = "SELECT * FROM `clients` WHERE gender NOT IN('1','2') AND status='1' ORDER BY firstname";
        $stmt_other_genders   = $link->prepare($query_other_genders);
        $stmt_other_genders->execute();
        $result_other_genders = $stmt_other_genders->fetchAll();
        $count_other_genders = $stmt_other_genders->rowCount();

        if($count_other_genders>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            '; 

            foreach($result_other_genders as $row_other_genders)
            {
                $unique_code = $row_other_genders['unique_code'];
                $xml .= '<sitemap>
                <loc>'.$websiteBasePath.'/users/Profile-'.$unique_code.'.html</loc>
                <lastmod>'.$date.'</lastmod>
                </sitemap>'; 
            }

            $xml .= '</sitemapindex>';             

            $file_other_genders = fopen($file_other_genders, "w");
            
            fwrite($file_other_genders, $xml);
            
            fclose($file_other_genders);
        }
        else
        if($count_other_genders==0)
        {
            if (file_exists($file_other_genders)) 
            {
                unlink($file_other_genders);
            }
        }
    /*****       Other Genders Sitemap generator end        *****/
?>