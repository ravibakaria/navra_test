<?php
    /*****       Occupation Sitemap generator start        *****/
        $file_occupation = "../../../occupation-sitemap.xml";
        
        $query_occupation  = "SELECT A.*,B.* FROM eduocc AS A JOIN employment AS B ON A.occupation=B.id GROUP BY A.occupation";
        $stmt_occupation   = $link->prepare($query_occupation);
        $stmt_occupation->execute();
        $result_occupation = $stmt_occupation->fetchAll();
        $count_occupation = $stmt_occupation->rowCount();

        if($count_occupation>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_occupation as $row_occupation)
            {
                $occupation = $row_occupation['id'];                 
                $occupation_name_search = $row_occupation['name'];

                $occupation_name = str_replace(' ','_',$occupation_name_search);

                $query  = "SELECT A.*,B.* FROM eduocc AS A JOIN clients AS B ON A.userid=B.id WHERE A.occupation='$occupation' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-Occupation-'.$occupation_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_occupation = fopen($file_occupation, "w");      

            fwrite($file_occupation, $xml);
            
            fclose($file_occupation);
        }
        else
        if($count_occupation==0)
        {
            if (file_exists($file_occupation)) 
            {
                unlink($file_occupation);
            }
        }
        
    /*****       Occupation Sitemap generator end        *****/
?>