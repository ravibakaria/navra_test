<?php
    /*****       SmokingHabit Sitemap generator start        *****/
        $file_smokinghabit = "../../../smoking-habit-sitemap.xml";
        
        $query_smokinghabit  = "SELECT A.*,B.* FROM profilebasic AS A JOIN smokehabbit AS B ON A.smoke_habbit=B.id GROUP BY A.smoke_habbit";
        $stmt_smokinghabit   = $link->prepare($query_smokinghabit);
        $stmt_smokinghabit->execute();
        $result_smokinghabit = $stmt_smokinghabit->fetchAll();
        $count_smokinghabit = $stmt_smokinghabit->rowCount();

        if($count_smokinghabit>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_smokinghabit as $row_smokinghabit)
            {
                $smoke_habbit = $row_smokinghabit['id'];                 
                $smoke_habbit_name_search = $row_smokinghabit['name'];

                $smoke_habbit_name = str_replace(' ','_',$smoke_habbit_name_search);

                $query  = "SELECT A.*,B.* FROM profilebasic AS A JOIN clients AS B ON A.userid=B.id WHERE A.smoke_habbit='$smoke_habbit' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-SmokingHabit-'.$smoke_habbit_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_smokinghabit = fopen($file_smokinghabit, "w");      

            fwrite($file_smokinghabit, $xml);
            
            fclose($file_smokinghabit);
        }
        else
        if($count_smokinghabit==0)
        {
            if (file_exists($file_smokinghabit)) 
            {
                unlink($file_smokinghabit);
            }
        }
        
    /*****       SmokingHabit Sitemap generator end        *****/
?>