<?php
	/*****       brides Sitemap generator start        *****/
        
        $file_brides = "../../../brides-sitemap.xml";

        $query_brides  = "SELECT * FROM `clients` WHERE gender='2' AND status='1' ORDER BY firstname";
        $stmt_brides   = $link->prepare($query_brides);
        $stmt_brides->execute();
        $result_brides = $stmt_brides->fetchAll();
        $count_brides = $stmt_brides->rowCount();

        if($count_brides>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_brides as $row_brides)
            {
                $unique_code = $row_brides['unique_code'];
                $xml .= '<sitemap>
                <loc>'.$websiteBasePath.'/users/Profile-'.$unique_code.'.html</loc>
                <lastmod>'.$date.'</lastmod>
                </sitemap>'; 
            }

            $xml .= '</sitemapindex>';       

            $file_brides = fopen($file_brides, "w");      

            fwrite($file_brides, $xml);
            
            fclose($file_brides);
        }
        else
        if($count_brides==0)
        {
            if (file_exists($file_brides)) 
            {
                unlink($file_brides);
            }
        }
    /*****       brides Sitemap generator end        *****/
?>