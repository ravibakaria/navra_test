<?php
    /*****       Complexion Sitemap generator start        *****/
        $file_complexion = "../../../complexion-sitemap.xml";
        
        $query_complexion  = "SELECT A.*,B.* FROM profilebasic AS A JOIN complexion AS B ON A.complexion=B.id GROUP BY A.complexion";
        $stmt_complexion   = $link->prepare($query_complexion);
        $stmt_complexion->execute();
        $result_complexion = $stmt_complexion->fetchAll();
        $count_complexion = $stmt_complexion->rowCount();

        if($count_complexion>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_complexion as $row_complexion)
            {
                $complexion = $row_complexion['id'];                 
                $complexion_name_search = $row_complexion['name'];

                $complexion_name = str_replace(' ','_',$complexion_name_search);

                $query  = "SELECT A.*,B.* FROM profilebasic AS A JOIN clients AS B ON A.userid=B.id WHERE A.complexion='$complexion' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-Complexion-'.$complexion_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_complexion = fopen($file_complexion, "w");      

            fwrite($file_complexion, $xml);
            
            fclose($file_complexion);
        }
        else
        if($count_complexion==0)
        {
            if (file_exists($file_complexion)) 
            {
                unlink($file_complexion);
            }
        }
        
    /*****       Complexion Sitemap generator end        *****/
?>