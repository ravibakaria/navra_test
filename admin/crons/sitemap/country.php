<?php
    /*****       Country Sitemap generator start        *****/
        $file_country = "../../../country-sitemap.xml";
        
        $query_country  = "SELECT A.*,B.* FROM address AS A JOIN countries AS B ON A.country=B.id GROUP BY A.country";
        $stmt_country   = $link->prepare($query_country);
        $stmt_country->execute();
        $result_country = $stmt_country->fetchAll();
        $count_country = $stmt_country->rowCount();

        if($count_country>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_country as $row_country)
            {
                $country = $row_country['id'];                 
                $country_name_search = $row_country['name'];

                $country_name = str_replace(' ','_',$country_name_search);

                $query  = "SELECT A.*,B.* FROM address AS A JOIN clients AS B ON A.userid=B.id WHERE A.country='$country' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-Country-'.$country_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_country = fopen($file_country, "w");      

            fwrite($file_country, $xml);
            
            fclose($file_country);
        }
        else
        if($count_country==0)
        {
            if (file_exists($file_country)) 
            {
                unlink($file_country);
            }
        }
        
    /*****       Country Sitemap generator end        *****/
?>