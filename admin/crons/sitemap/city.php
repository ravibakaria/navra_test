<?php
    /*****       City Sitemap generator start        *****/
        $file_city = "../../../city-sitemap.xml";
        
        $query_city  = "SELECT A.*,B.* FROM address AS A JOIN cities AS B ON A.city=B.id GROUP BY A.city";
        $stmt_city   = $link->prepare($query_city);
        $stmt_city->execute();
        $result_city = $stmt_city->fetchAll();
        $count_city = $stmt_city->rowCount();

        if($count_city>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_city as $row_city)
            {
                $city = $row_city['id'];                 
                $city_name_search = $row_city['name'];

                $city_name = str_replace(' ','_',$city_name_search);

                $query  = "SELECT A.*,B.* FROM address AS A JOIN clients AS B ON A.userid=B.id WHERE A.city='$city' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-City-'.$city_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_city = fopen($file_city, "w");      

            fwrite($file_city, $xml);
            
            fclose($file_city);
        }
        else
        if($count_city==0)
        {
            if (file_exists($file_city)) 
            {
                unlink($file_city);
            }
        }
        
    /*****       City Sitemap generator end        *****/
?>