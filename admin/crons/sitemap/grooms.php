<?php
	/*****       Grooms Sitemap generator start        *****/

        $file_grooms = "../../../grooms-sitemap.xml";

        $query_grooms  = "SELECT * FROM `clients` WHERE gender='1' AND status='1' ORDER BY firstname";
        $stmt_grooms   = $link->prepare($query_grooms);
        $stmt_grooms->execute();
        $result_grooms = $stmt_grooms->fetchAll();
        $count_grooms = $stmt_grooms->rowCount();

        if($count_grooms>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            '; 

            foreach($result_grooms as $row_grooms)
            {
                $unique_code = $row_grooms['unique_code'];
                $xml .= '<sitemap>
                <loc>'.$websiteBasePath.'/users/Profile-'.$unique_code.'.html</loc>
                <lastmod>'.$date.'</lastmod>
                </sitemap>'; 
            }

            $xml .= '</sitemapindex>';  

            $file_grooms = fopen($file_grooms, "w");           

            fwrite($file_grooms, $xml);
            
            fclose($file_grooms);
        }
        else
        if($count_grooms==0)
        {
            if (file_exists($file_grooms)) 
            {
                unlink($file_grooms);
            }
        }
        
    /*****       Grooms Sitemap generator end        *****/
?>