<?php
    /*****       Wedding directory Sitemap generator start        *****/
        $file_wedding_directory = "../../../wedding-directory-sitemap.xml";
        
        $query_wedding_directory  = "SELECT A.*,B.* FROM membercategory AS A JOIN vendors AS B ON A.id=B.category_id WHERE B.status='1'";
        $stmt_wedding_directory   = $link->prepare($query_wedding_directory);
        $stmt_wedding_directory->execute();
        $result_wedding_directory = $stmt_wedding_directory->fetchAll();
        $count_wedding_directory = $stmt_wedding_directory->rowCount();

        if($count_wedding_directory>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';
            
            foreach($result_wedding_directory as $row_wedding_directory)
            {
                $id = $row_wedding_directory['id'];                 
                $name = $row_wedding_directory['name'];
                $slug = $row_wedding_directory['slug'];

                $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/directory/category-'.$slug.'</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';                
            }    

            $xml .= '</sitemapindex>';     

            $file_wedding_directory = fopen($file_wedding_directory, "w");      

            fwrite($file_wedding_directory, $xml);
            
            fclose($file_wedding_directory);
        }
        else
        if($count_wedding_directory==0)
        {
            if (file_exists($file_wedding_directory)) 
            {
                unlink($file_wedding_directory);
            }
        }
        
    /*****       City Sitemap generator end        *****/
?>