<?php
	/*****       Pages Sitemap generator start        *****/
    $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            <sitemap>
            <loc>'.$websiteBasePath.'/index.php</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            <sitemap>
            <loc>'.$websiteBasePath.'/users/advanced-search.php</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            <sitemap>
            <loc>'.$websiteBasePath.'/free-register.php</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            <sitemap>
            <loc>'.$websiteBasePath.'/login.php</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            <sitemap>
            <loc>'.$websiteBasePath.'/membership_plan.php</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            <sitemap>
            <loc>'.$websiteBasePath.'/contact-us.html</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            <sitemap>
            <loc>'.$websiteBasePath.'/about-us.html</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            <sitemap>
            <loc>'.$websiteBasePath.'/disclaimer.html</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            <sitemap>
            <loc>'.$websiteBasePath.'/privacy-policy.html</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            <sitemap>
            <loc>'.$websiteBasePath.'/terms-of-service.html</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            <sitemap>
            <loc>'.$websiteBasePath.'/faq.html</loc>
            <lastmod>'.$date.'</lastmod>
            </sitemap>
            ';

            if($wedding_directory_display_or_not=='1')
            {
                $xml .='<sitemap>
                <loc>'.$websiteBasePath.'/directory/</loc>
                <lastmod>'.$date.'</lastmod>
                </sitemap>
                <sitemap>
                <loc>'.$websiteBasePath.'/directory/free-register.php</loc>
                <lastmod>'.$date.'</lastmod>
                </sitemap>
                <sitemap>
                <loc>'.$websiteBasePath.'/directory/login.php</loc>
                <lastmod>'.$date.'</lastmod>
                </sitemap>';
            }
        
        $xml .= '</sitemapindex>'; 

        $file_pages = fopen("../../../pages-sitemap.xml", "w");

        fwrite($file_pages, $xml);
        
        fclose($file_pages);
    /*****       Sitemap generator end        *****/
?>