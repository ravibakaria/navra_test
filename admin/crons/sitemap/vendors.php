<?php
    /*****       Wedding directory Sitemap generator start        *****/
        $file_vendor = "../../../vendors-sitemap.xml";
        
        $query_vendor  = "SELECT * FROM vendors WHERE status='1'";
        $stmt_vendor   = $link->prepare($query_vendor);
        $stmt_vendor->execute();
        $result_vendor = $stmt_vendor->fetchAll();
        $count_vendor = $stmt_vendor->rowCount();

        if($count_vendor>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';
            
            foreach($result_vendor as $row_vendor)
            {
                $vuserid = $row_vendor['vuserid'];

                $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/directory/Profile-'.$vuserid.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';                
            }    

            $xml .= '</sitemapindex>';     

            $file_vendor = fopen($file_vendor, "w");      

            fwrite($file_vendor, $xml);
            
            fclose($file_vendor);
        }
        else
        if($count_vendor==0)
        {
            if (file_exists($file_vendor)) 
            {
                unlink($file_vendor);
            }
        }
        
    /*****       City Sitemap generator end        *****/
?>