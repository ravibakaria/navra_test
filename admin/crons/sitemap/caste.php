<?php
    /*****       Caste Sitemap generator start        *****/
        $file_caste = "../../../caste-sitemap.xml";
        
        $query_caste  = "SELECT A.*,B.* FROM profilereligion AS A JOIN caste AS B ON A.caste=B.id GROUP BY A.caste";
        $stmt_caste   = $link->prepare($query_caste);
        $stmt_caste->execute();
        $result_caste = $stmt_caste->fetchAll();
        $count_caste = $stmt_caste->rowCount();

        if($count_caste>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_caste as $row_caste)
            {
                $caste = $row_caste['id'];                 
                $caste_name_serach = $row_caste['name'];

                $caste_name = str_replace(' ','_',$caste_name_serach);

                $query  = "SELECT A.*,B.* FROM profilereligion AS A JOIN clients AS B ON A.userid=B.id WHERE A.caste='$caste' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-Caste-'.$caste_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_caste = fopen($file_caste, "w");      

            fwrite($file_caste, $xml);
            
            fclose($file_caste);
        }
        else
        if($count_caste==0)
        {
            if (file_exists($file_caste)) 
            {
                unlink($file_caste);
            }
        }
        
    /*****       Caste Sitemap generator end        *****/
?>