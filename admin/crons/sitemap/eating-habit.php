<?php
    /*****       EatingHabit Sitemap generator start        *****/
        $file_eatinghabbit = "../../../eating-habbit-sitemap.xml";
        
        $query_eatinghabbit  = "SELECT A.*,B.* FROM profilebasic AS A JOIN eathabbit AS B ON A.eat_habbit=B.id GROUP BY A.eat_habbit";
        $stmt_eatinghabbit   = $link->prepare($query_eatinghabbit);
        $stmt_eatinghabbit->execute();
        $result_eatinghabbit = $stmt_eatinghabbit->fetchAll();
        $count_eatinghabbit = $stmt_eatinghabbit->rowCount();

        if($count_eatinghabbit>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_eatinghabbit as $row_eatinghabbit)
            {
                $eat_habbit = $row_eatinghabbit['id'];                 
                $eat_habbit_name_search = $row_eatinghabbit['name'];

                $eat_habbit_name = str_replace(' ','_',$eat_habbit_name_search);

                $query  = "SELECT A.*,B.* FROM profilebasic AS A JOIN clients AS B ON A.userid=B.id WHERE A.eat_habbit='$eat_habbit' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-EatingHabit-'.$eat_habbit_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_eatinghabbit = fopen($file_eatinghabbit, "w");      

            fwrite($file_eatinghabbit, $xml);
            
            fclose($file_eatinghabbit);
        }
        else
        if($count_eatinghabbit==0)
        {
            if (file_exists($file_eatinghabbit)) 
            {
                unlink($file_eatinghabbit);
            }
        }
        
    /*****       EatingHabit Sitemap generator end        *****/
?>