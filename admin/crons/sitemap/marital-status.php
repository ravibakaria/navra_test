<?php
    /*****       MaritalStatus Sitemap generator start        *****/
        $file_maritalstatus = "../../../marital-status-sitemap.xml";
        
        $query_maritalstatus  = "SELECT A.*,B.* FROM profilebasic AS A JOIN maritalstatus AS B ON A.marital_status=B.id GROUP BY A.marital_status";
        $stmt_maritalstatus   = $link->prepare($query_maritalstatus);
        $stmt_maritalstatus->execute();
        $result_maritalstatus = $stmt_maritalstatus->fetchAll();
        $count_maritalstatus = $stmt_maritalstatus->rowCount();

        if($count_maritalstatus>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_maritalstatus as $row_maritalstatus)
            {
                $marital_status = $row_maritalstatus['id'];                 
                $marital_status_name_search = $row_maritalstatus['name'];

                $marital_status_name = str_replace(' ','_',$marital_status_name_search);

                $query  = "SELECT A.*,B.* FROM profilebasic AS A JOIN clients AS B ON A.userid=B.id WHERE A.marital_status='$marital_status' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-MaritalStatus-'.$marital_status_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_maritalstatus = fopen($file_maritalstatus, "w");      

            fwrite($file_maritalstatus, $xml);
            
            fclose($file_maritalstatus);
        }
        else
        if($count_maritalstatus==0)
        {
            if (file_exists($file_maritalstatus)) 
            {
                unlink($file_maritalstatus);
            }
        }
        
    /*****       MaritalStatus Sitemap generator end        *****/
?>