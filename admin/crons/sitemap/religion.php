<?php
	/*****       Religion Sitemap generator start        *****/
        $file_religion = "../../../religion-sitemap.xml";
        
        $query_religion  = "SELECT A.*,B.* FROM profilereligion AS A JOIN religion AS B ON A.religion=B.id GROUP BY A.religion";
        $stmt_religion   = $link->prepare($query_religion);
        $stmt_religion->execute();
        $result_religion = $stmt_religion->fetchAll();
        $count_religion = $stmt_religion->rowCount();

        if($count_religion>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_religion as $row_religion)
            {
                $religion = $row_religion['id'];                 
                $religion_name_search = $row_religion['name'];

                $religion_name = str_replace(' ','_',$religion_name_search);
                
                $query  = "SELECT A.*,B.* FROM profilereligion AS A JOIN clients AS B ON A.userid=B.id WHERE A.religion='$religion' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-Religion-'.$religion_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                } 
            }    

            $xml .= '</sitemapindex>';     

            $file_religion = fopen($file_religion, "w");      

            fwrite($file_religion, $xml);
            
            fclose($file_religion);
        }
        else
        if($count_religion==0)
        {
            if (file_exists($file_religion)) 
            {
                unlink($file_religion);
            }
        }
        
    /*****       Religion Sitemap generator end        *****/
?>