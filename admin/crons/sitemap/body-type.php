<?php
    /*****       BodyType Sitemap generator start        *****/
        $file_bodytype = "../../../body-type-sitemap.xml";
        
        $query_bodytype  = "SELECT A.*,B.* FROM profilebasic AS A JOIN bodytype AS B ON A.body_type=B.id GROUP BY A.body_type";
        $stmt_bodytype   = $link->prepare($query_bodytype);
        $stmt_bodytype->execute();
        $result_bodytype = $stmt_bodytype->fetchAll();
        $count_bodytype = $stmt_bodytype->rowCount();

        if($count_bodytype>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_bodytype as $row_bodytype)
            {
                $bodytype = $row_bodytype['id'];                 
                $bodytype_name_search = $row_bodytype['name'];

                $bodytype_name = str_replace(' ','_',$bodytype_name_search);

                $query  = "SELECT A.*,B.* FROM profilebasic AS A JOIN clients AS B ON A.userid=B.id WHERE A.body_type='$bodytype' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-BodyType-'.$bodytype_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_bodytype = fopen($file_bodytype, "w");      

            fwrite($file_bodytype, $xml);
            
            fclose($file_bodytype);
        }
        else
        if($count_bodytype==0)
        {
            if (file_exists($file_bodytype)) 
            {
                unlink($file_bodytype);
            }
        }
        
    /*****       BodyType Sitemap generator end        *****/
?>