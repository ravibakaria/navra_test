<?php
    /*****       DrinkingHabit Sitemap generator start        *****/
        $file_drinkinghabit = "../../../drinking-habit-sitemap.xml";
        
        $query_drinkinghabit  = "SELECT A.*,B.* FROM profilebasic AS A JOIN drinkhabbit AS B ON A.drink_habbit=B.id GROUP BY A.drink_habbit";
        $stmt_drinkinghabit   = $link->prepare($query_drinkinghabit);
        $stmt_drinkinghabit->execute();
        $result_drinkinghabit = $stmt_drinkinghabit->fetchAll();
        $count_drinkinghabit = $stmt_drinkinghabit->rowCount();

        if($count_drinkinghabit>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_drinkinghabit as $row_drinkinghabit)
            {
                $drink_habbit = $row_drinkinghabit['id'];                 
                $drink_habbit_name_search = $row_drinkinghabit['name'];

                $drink_habbit_name = str_replace(' ','_',$drink_habbit_name_search);

                $query  = "SELECT A.*,B.* FROM profilebasic AS A JOIN clients AS B ON A.userid=B.id WHERE A.drink_habbit='$drink_habbit' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-DrinkingHabit-'.$drink_habbit_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_drinkinghabit = fopen($file_drinkinghabit, "w");      

            fwrite($file_drinkinghabit, $xml);
            
            fclose($file_drinkinghabit);
        }
        else
        if($count_drinkinghabit==0)
        {
            if (file_exists($file_drinkinghabit)) 
            {
                unlink($file_drinkinghabit);
            }
        }
        
    /*****       DrinkingHabit Sitemap generator end        *****/
?>