<?php
    /*****       State Sitemap generator start        *****/
        $file_state = "../../../state-sitemap.xml";
        
        $query_state  = "SELECT A.*,B.* FROM address AS A JOIN states AS B ON A.state=B.id GROUP BY A.state";
        $stmt_state   = $link->prepare($query_state);
        $stmt_state->execute();
        $result_state = $stmt_state->fetchAll();
        $count_state = $stmt_state->rowCount();

        if($count_state>0)
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.$websiteBasePath.'/sitemap/sitemap.xsl"?>
            <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ';

            foreach($result_state as $row_state)
            {
                $state = $row_state['id'];                 
                $state_name_search = $row_state['name'];

                $state_name = str_replace(' ','_',$state_name_search);

                $query  = "SELECT A.*,B.* FROM address AS A JOIN clients AS B ON A.userid=B.id WHERE A.state='$state' AND B.status='1'";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $count = $stmt->rowCount();
                if($count>0)
                {
                    $xml .= '<sitemap>
                    <loc>'.$websiteBasePath.'/users/Search-State-'.$state_name.'.html</loc>
                    <lastmod>'.$date.'</lastmod>
                    </sitemap>';
                }  
            }    

            $xml .= '</sitemapindex>';     

            $file_state = fopen($file_state, "w");      

            fwrite($file_state, $xml);
            
            fclose($file_state);
        }
        else
        if($count_state==0)
        {
            if (file_exists($file_state)) 
            {
                unlink($file_state);
            }
        }
        
    /*****       State Sitemap generator end        *****/
?>