<?php
	$sql=$count=$result=$row=null;
	
	$sql = "SELECT * FROM clients WHERE status='1'";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    $count = $stmt->rowCount();

    if($count>0)
    {
    	$result = $stmt->fetchAll();

    	foreach ($result as $row) 
    	{
    		$profile_completeness_data=$profile_percent=$info=null;

    		$userid = $row['id'];
    		$unique_code = $row['unique_code'];
    		$first_name = $row['firstname'];
			$email_address = $row['email'];

    		$sql_get_profile_completeness_entry = "SELECT * FROM profile_completeness WHERE userid='$userid'";
    		$stmt_get_profile_completeness_entry = $link->prepare($sql_get_profile_completeness_entry);
    		$stmt_get_profile_completeness_entry->execute();
    		$count_incomplete_profile_clients = $stmt_get_profile_completeness_entry->rowCount();
    		if($count_incomplete_profile_clients>0)
    		{
    			$result_profile_completness = $stmt_get_profile_completeness_entry->fetch();
    			$chk_profile_pic = $result_profile_completness['profile_pic'];
    			$chk_basic_info = $result_profile_completness['basic_info'];
    			$chk_religious_info = $result_profile_completness['religious_info'];
    			$chk_education_info = $result_profile_completness['education_info'];
    			$chk_location_info = $result_profile_completness['location_info'];
    			$chk_family_info = $result_profile_completness['family_info'];
    			$chk_about_info = $result_profile_completness['about_info'];

    			//profile pic
    			if($chk_profile_pic=='1')
    			{
    				$profile_pic_text = "<p style='color:green' align='justify'><b> Uploaded</b></p>";
					$profile_percent = $profile_percent+10;
    			}
    			else
    			{
    				$profile_pic_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			}

    			//basic Info
				if($chk_basic_info=='1')
				{
					$basic_info_text =  "<p style='color:green' align='justify'><b>Updated</b></p>";
					$profile_percent = $profile_percent+15;
				}
    			else
    			{
    				$basic_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			}

				//Religious info
				if($chk_religious_info=='1')
				{
					$religious_info_text = "<p style='color:green' align='justify'><b> Updated</b></p>";
					$profile_percent = $profile_percent+15;
				}
    			else
    			{
    				$religious_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			}

				//Location info
				if($chk_location_info=='1')
				{
					$location_info_text = "<p style='color:green' align='justify'><b> Updated</b></p>";
					$profile_percent = $profile_percent+15;
				}
    			else
    			{
    				$location_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			}

				//Education info
				if($chk_education_info=='1')
				{
					$education_info_text = "<p style='color:green' align='justify'><b> Updated</b></p>";
					$profile_percent = $profile_percent+15;
				}
    			else
    			{
    				$education_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			}

				//Family info
				if($chk_family_info=='1')
				{
					$family_info_text = "<p style='color:green' align='justify'><b> Updated</b></p>";
					$profile_percent = $profile_percent+15;
				}
    			else
    			{
    				$family_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			}

				//About Yourself info
				if($chk_about_info=='1')
				{
					$about_info_text = "<p style='color:green' align='justify'><b> Updated</b></p>";
					$profile_percent = $profile_percent+15;
				}
    			else
    			{
    				$about_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			}

				if($profile_percent==100)
				{
					continue;
				}
				else
				{
					if($profile_percent==0 || $profile_percent<65)
					{
						$profile_percent = "<p style='color:red;' align='justify'><b> $profile_percent %</b></p>";
					}
					else
					if($profile_percent>65 || $profile_percent<80)
					{
						$profile_percent = "<p style='color:#ed9c28;' align='justify'><b> $profile_percent %</b></p>";
					}
					else
					if($profile_percent>80 || $profile_percent<100)
					{
						$profile_percent = "<p style='color:green;' align='justify'><b> $profile_percent %</b></p>";
					}

					$profile_completeness_data = "<table border='1' cellpadding='5' cellspacing='0'>";
					$profile_completeness_data .="<tr><th colspan='9' align='center' bgcolor='#d9e6f0'>Profile Completeness Details</th></tr>";
					$profile_completeness_data .="<tr><th align='left'>Profile Score</th><td colspan='8'>".$profile_percent."</td></tr>";
					$profile_completeness_data .="<tr><th align='left'>Profile Picture</th><td colspan='8'>".$profile_pic_text."</td></tr>";
					$profile_completeness_data .="<tr><th align='left'>Personal Information</th><td colspan='8'>".$basic_info_text."</td></tr>";
					$profile_completeness_data .="<tr><th align='left'>Religious Information</th><td colspan='8'>".$religious_info_text."</td></tr>";
					$profile_completeness_data .="<tr><th align='left'>Location Information</th><td colspan='8'>".$location_info_text."</td></tr>";
					$profile_completeness_data .="<tr><th align='left'>Education Information</th><td colspan='8'>".$education_info_text."</td></tr>";
					$profile_completeness_data .="<tr><th align='left'>Family Information</th><td colspan='8'>".$family_info_text."</td></tr>";
					$profile_completeness_data .="<tr><th align='left'>About Yourself</th><td colspan='8'>".$about_info_text."</td></tr>";
					$profile_completeness_data .= "</table>";

					$my_profile_link = "<a href='$WebSiteBasePath/users/my-profile.php' target='_blank'>Click Here</a>";

					$SocialSharing = getSocialSharingLinks();   // social sharing links

		        	$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

					$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

					$ReminderProfileCompletenessMessage = str_replace(array('$first_name','$site_name','$profile_completeness_data','$my_profile_link','$signature'),array($first_name,$WebSiteTitle,$profile_completeness_data,$my_profile_link,$GlobalEmailSignature),$ReminderProfileCompletenessMessage);  //replace footer variables with value
				}
    		}
    		else
    		{
    			$profile_percent = "<p style='color:red;' align='justify'><b> 0 %</b></p>";
    			$profile_pic_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			$basic_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			$religious_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			$location_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			$education_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			$family_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";
    			$about_info_text = "<p style='color:red' align='justify'><b> Pending</b></p>";

    			$profile_completeness_data = "<table border='1' cellpadding='2' cellspacing='0'>";
				$profile_completeness_data .="<tr><th colspan='9' align='center' bgcolor='#d9e6f0'>Profile Completeness Details</th></tr>";
				$profile_completeness_data .="<tr><th align='left'>Profile Score</th><td colspan='8'>".$profile_percent."</td></tr>";
				$profile_completeness_data .="<tr><th align='left'>Profile Picture</th><td colspan='8'>".$profile_pic_text."</td></tr>";
				$profile_completeness_data .="<tr><th align='left'>Personal Information</th><td colspan='8'>".$basic_info_text."</td></tr>";
				$profile_completeness_data .="<tr><th align='left'>Religious Information</th><td colspan='8'>".$religious_info_text."</td></tr>";
				$profile_completeness_data .="<tr><th align='left'>Location Information</th><td colspan='8'>".$location_info_text."</td></tr>";
				$profile_completeness_data .="<tr><th align='left'>Education Information</th><td colspan='8'>".$education_info_text."</td></tr>";
				$profile_completeness_data .="<tr><th align='left'>Family Information</th><td colspan='8'>".$family_info_text."</td></tr>";
				$profile_completeness_data .="<tr><th align='left'>About Yourself</th><td colspan='8'>".$about_info_text."</td></tr>";
				$profile_completeness_data .= "</table>";
                
                $profile_completeness_data;
				$my_profile_link = "<a href='$WebSiteBasePath/users/my-profile.php' target='_blank'>Click Here</a>";

				$SocialSharing = getSocialSharingLinks();   // social sharing links

	        	$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

				$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

				$ReminderProfileCompletenessMessage = str_replace(array('$first_name','$site_name','$profile_completeness_data','$my_profile_link','$signature'),array($first_name,$WebSiteTitle,$profile_completeness_data,$my_profile_link,$GlobalEmailSignature),$ReminderProfileCompletenessMessage);  //replace footer variables with value
    		}

    		$subject = $ReminderProfileCompletenessSubject;
                
            $message  = '<!DOCTYPE html>';
            $message .= '<html lang="en">
                <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width">
                <title></title>
                <style type="text/css">'.$EmailCSS.'</style>
                </head>
                <body style="margin: 0; padding: 0;">';
			
			$message .= $EmailGlobalHeader;

			$message .= $ReminderProfileCompletenessMessage;

			$message .= $EmailGlobalFooter;
			
			$mailto = $email_address;
			$mailtoname = $first_name;
			
			$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

			if($emailResponse == 'success')
			{
				$info = "success";
			}
			else
			{
				$info = "Error in sending email";
			}

			$log_data = date('Y-m-d H:i:s') . " ~  User_id ~  " . $userid .  " ~  Unique_id ~  " . $unique_code . " ~ " . $first_name . " ~ " . $email_address . " ~ " ;
			$log_data .= $info . " ~ " ;
			$log_data .= str_pad('',4096)."\n";

			$ProfileIncompletenessFileName='/home/navrabayko/public_html/admin/crons/logs/profile-completeness-reminder-'.date('M-Y').'.log';  
            //open the file append mode,dats the log file will create day wise  
            $ProfileIncompletenessFileHandler=fopen($ProfileIncompletenessFileName,'a+');  
            //write the info into the file  
            fwrite($ProfileIncompletenessFileHandler,$log_data);  
            //close handler  
            fclose($ProfileIncompletenessFileHandler);
    	    break;
    	}
    }
?>