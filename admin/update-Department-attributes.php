<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body update-section">
		<a href="user-setup.php?main_tab=UserAttributes&sub_tab=Department_Setup" id="portletReset" type="button" class="mb-xs mt-xs mr-xs btn btn-default" style="float:right;"><i class="fa fa-arrow-left"></i> Back</a>

		<?php
			$id = quote_smart($_GET['id']);

			if ( !is_numeric( $id ) )
		    {
		        echo 'Error! Invalid Data';
		        exit;
		    }

		    $sql = "SELECT * FROM `departments` WHERE `id`='$id'";
		    $stmt1   = $link->prepare($sql);
            $stmt1->execute();
            $queryTot = $stmt1->rowCount();
            $result = $stmt1->fetch();

           	if($queryTot>0)
           	{
           		$name = $result['name'];
           		$email = $result['email'];
           		$status = $result['status'];
		?>
		<br/><br/>
		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-md-3 col-lg-3">
					</div>
					<div class="col-md-6 col-lg-6 edit-master-div">
						<div class="row"><br>
							<center><h4>Update <strong><?php echo $name;?></strong> Department Information</h4></center>
						</div>
						<hr/>

						<div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Department Name:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <input type="text" class="form-control form-control-sm Department_name" value="<?php echo $name;?>" />
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label"> Email Id:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <input type="text" class="form-control form-control-sm Department_email" value="<?php echo $email;?>" />
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Status:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <?php
									if($status=='0')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Department_current_status' name='Department_current_status' value='".$status."'>";
									}
									else
									if($status=='1')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Department_current_status' name='Department_current_status' checked value='".$status."'>";
									}
								?>
                            </div>
                        </div>

                        <input type="hidden" class="Department_id" value="<?php echo $id;?>">

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 update_status">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
							</div>	
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><button class="btn btn-success btn_update_Department website-button">Update</button></center><br/>	
							</div>
						</div>
						
					</div>
					<div class="col-md-3 col-lg-3">
					</div>
				</div>
				<div class="row">
					<br/>
				</div>
			</div>
			
		</div>
		<?php
			}
			else
			{
				echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
			}
		?>
</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		$('.btn_update_Department').click(function(){
			var name = $('.Department_name').val();
			var email = $('.Department_email').val();
            var status = $('.Department_current_status').is(":checked");
            var id = $('.Department_id').val();
			var task = "Update_Department_Attributes";

			if(status==true)
			{
				status='1';
			}
			else
			if(status==false)
			{
				status='0';
			}

			if(name=='' || name==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter department name.</div></center>");
                return false;
            }

            if(email=='' || email==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter department email id.</div></center>");
                return false;
            }

            function validateEmail($email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $email );
            }

            if( !validateEmail(email)) 
            { 
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Please Enter valid email.</div></center>");
                return false;
            }

			var data = 'name='+name+'&email='+email+'&status='+status+'&id='+id+'&task='+task;
			
			$('.loading_img').show();
			
			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/user-attributes/Department-helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data updated successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});
	});
</script>