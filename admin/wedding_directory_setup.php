<?php
	include("templates/header.php");

	$sql_wedding_directory = "SELECT * FROM wedding_directory_settings";
	$stmt_wedding_directory = $link->prepare($sql_wedding_directory);
	$stmt_wedding_directory->execute();
	$count = $stmt_wedding_directory->rowCount();
	$result = $stmt_wedding_directory->fetch();

	$wedding_directory_display_or_not = $result['wedding_directory_display_or_not'];
	$custom_content = $result['custom_content'];
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>
	<section role="main" class="content-body">
		
		<!-- start: page -->
			<div class="row admin_start_section">
				<div class="row">
				    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				    	<h1>Wedding Directory Setting</h1>
				    </div>
				</div>

				<div class="row">
				    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				        <div class="row input-row">
				            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				                Display Wedding Directory:
				            </div>
				            <div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				                <input type="checkbox" class="wedding_directory_display_or_not" <?php if($count>0 && ($wedding_directory_display_or_not=='1')) { echo 'checked';}?>>
				            </div>
				            <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
				                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
				                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong>If you enable this property then <b>Wedding Directory</b> link will be visible on website.<br>
				                </div>
				            </div>
				        </div>
				        <div class="row input-row custom_content_data" <?php if($wedding_directory_display_or_not!='1') { echo 'style=display:none;';}?>>
				            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				                Custom Content:
				            </div>
				            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
				                <textarea class='form-control custom_content' rows="8"><?php if($count>0) { echo @$custom_content;}?></textarea>
				            </div>
				        </div>
				    </div>

				    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				        <div class="row">
				            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wedding_directory_status">
				                
				            </div>
				            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
				            </div>
				        </div>
				        <div class="row">
				            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				                <center><button class="btn btn-success btn_wedding_directory website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
				            </div>
				        </div>
				    </div>
				</div>
			</div>
		<!-- end: page -->
	</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){

		$('.custom_content').richText();
		
		$('.wedding_directory_display_or_not').change(function(){

            if($(this).is(':checked'))
            {
                var wedding_directory_display_or_not = '1';
            }
            else
            {
                var wedding_directory_display_or_not = '0';
            }

            if(wedding_directory_display_or_not=='1')
            {
                $('.custom_content_data').show();
                return false;
            }
            else
            {
                $('.custom_content_data').hide();
                return false;
            }
        });

        $('.btn_wedding_directory').click(function(){
            if($('.wedding_directory_display_or_not').is(':checked'))
            {
                var wedding_directory_display_or_not = '1';
            }
            else
            {
                var wedding_directory_display_or_not = '0';
            }

            var custom_content = $('.custom_content').val();
            var task = "update-wedding-directory-setup";

            var data = {
                wedding_directory_display_or_not : wedding_directory_display_or_not,
                custom_content : custom_content,
                task : task
            }

            $.ajax({
                type : 'post',
                dataType : 'json',
                data : data,
                url : 'query/wedding-category/wedding-directory-setup-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.wedding_directory_status').html("<center><div class='alert alert-success wedding_directory_status_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
                        $('.wedding_directory_status_status').fadeTo(1000, 500).slideUp(500, function(){
                            location.reload();
                        });
                    }
                    else
                    {
                        $('.wedding_directory_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
	});
</script>