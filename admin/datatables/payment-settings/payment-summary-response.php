<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

	if(!isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']=='' || $_SESSION['admin_user']==null))
	{
		header('Location: ../../login.php');
		exit;
	}

	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;
	//print_r($params);
	//define index of column
	$columns = array( 
		0 =>'A.OrderNumber',
		1 =>'A.created_at',
		2 =>'A.updated_at',
		3 =>'B.firstname',
		4 =>'B.lastname',
		5 =>'B.email',
		6 =>'B.unique_code',
		7 =>'A.total_amount',
		8 =>'A.status',
		9 =>'A.payment_gateway'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value if exist
	if( !empty($params['search']['value']) ) {   
		$where .=" AND ( A.OrderNumber LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.created_at LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.updated_at LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR B.firstname LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR B.lastname LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR B.email LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR B.unique_code LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.total_amount LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.status LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.payment_gateway LIKE '%".quote_smart($params['search']['value'])."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT A.OrderNumber,A.created_at,A.updated_at,B.firstname,B.lastname,B.email,B.unique_code,A.total_amount,A.status,A.userid,A.payment_gateway FROM payment_transactions AS A JOIN clients AS B ON A.userid=B.id WHERE 1=1 ";

	if(!empty(quote_smart($params['columns'][1]['search']['value'])) || quote_smart($params['columns'][1]['search']['value'])=='0') 
	{   
		$payment_status = $params['columns'][1]['search']['value'];

		if($payment_status!='-1')
		{
			$where .= " AND  A.status='$payment_status'";
		}
		else
		if($payment_status=='-1')
		{
			$where .= " ";
		}
	}

	if(!empty(quote_smart($params['columns'][2]['search']['value'])) && !empty(quote_smart($params['columns'][3]['search']['value']))) 
	{   
		$from_date = $params['columns'][2]['search']['value'];
		$to_date = $params['columns'][3]['search']['value'];

		$where .= " AND (A.created_at>='$from_date 00:00:00' OR A.updated_at>='$from_date 00:00:00') AND (A.created_at<='$to_date 23:59:59' || A.updated_at<='$to_date 23:59:59') ";
	}

	if(!empty(quote_smart($params['columns'][2]['search']['value'])) && empty(quote_smart($params['columns'][3]['search']['value']))) 
	{   
		$from_date = $params['columns'][2]['search']['value'];

		$where .= " AND (A.created_at>='$from_date 00:00:00' OR A.updated_at>='$from_date 00:00:00') ";
	}

	if(empty(quote_smart($params['columns'][2]['search']['value'])) && !empty(quote_smart($params['columns'][3]['search']['value']))) 
	{   
		$to_date = $params['columns'][3]['search']['value'];

		$where .= " AND (A.created_at<='$to_date 23:59:59' || A.updated_at<='$to_date 23:59:59') ";
	}

	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}

	//echo $sqlRec;exit;
 	$sqlRec .=  " ORDER BY ". $columns[quote_smart($params['order'][0]['column'])]."   ".quote_smart($params['order'][0]['dir'])."  LIMIT ".quote_smart($params['start'])." ,".quote_smart($params['length'])." ";

 	
	$stmt1   = $link->prepare($sqlTot);
    $stmt1->execute();
    $totalRecords = $stmt1->rowCount();

	$stmt2   = $link->prepare($sqlRec);
    $stmt2->execute();
    $total_rows = $stmt2->rowCount();

    $result = $stmt2->fetchAll();

    foreach( $result as $row )
    {
    	$OrderNumber = $row['0'];
        $created_at = date('d-m-Y',strtotime($row['1']));
        if($row['2']!='' || $row['2']!=null)
        {
        	$updated_at = date('d-m-Y',strtotime($row['2']));
        }
        else
        {
        	$updated_at = '-';	
        }

        $firstname = $row['3'];
        $lastname = $row['4'];
        $email = $row['5'];
        $unique_code = $row['6'];
        $total_amount = $row['7'];
        $status = $row['8'];
        $userid = $row['9'];
       
        if($status=='0')
        {
        	$row['8'] = "<a href='' style='color:#000000;'><b class='label label-warning text-right' style='text-align:right;'>Un-Paid</b></a>";
        }
        else
        if($status=='1')
        {
        	$row['8'] = "<a href='' style='color:#000000;'><b class='label label-success text-right' style='text-align:right;'>Paid</b></a>";
        }
        else
        if($status=='2')
        {
        	$row['8'] = "<a href='' style='color:#000000;'><b class='label label-danger text-right' style='text-align:right;'>Canceled</b></a>";
        }
        else
        if($status=='3')
        {
        	$row['8'] = "<a href='' style='color:#000000;'><b class='label label-info text-right' style='text-align:right;'>Refund</b></a>";
        }

        $row['0'] = "<a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$OrderNumber."</a>";
    	$row['1'] = "<a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$created_at."</a>";
    	$row['2'] = "<a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$updated_at."</a>";
    	$row['3'] = "<a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$firstname."</a>";
    	$row['4'] = "<a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$lastname."</a>";
    	$row['5'] = "<a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$email."</a>";
    	$row['6'] = "<a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$unique_code."</a>";
    	$row['7'] = "<a href='member-profile.php?id=$userid$&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$total_amount."</a>";
    	$row['8'] = "<a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$row['8']."</a>";
    	$row['9'] = "<a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$row['10']."</a>";
    	$row['10'] = "<a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'><img src='../images/view-icon.png' style='height:30px;width:30px;'></a>";

        $data[] = $row;
    }

    $json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>