<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

	if(!isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']=='' || $_SESSION['admin_user']==null))
	{
		header('Location: ../../login.php');
		exit;
	}


	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;
	//print_r($params);
	//define index of column
	$columns = array( 
		0 =>'A.id',
		1 =>'A.firstname',
		2 =>'A.lastname',
		3 =>'A.email',
		4 =>'A.unique_code',
		5 =>'A.created_at'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value if exist
	if( !empty($params['search']['value']) ) {   
		$where .=" AND ( A.id LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.firstname LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.lastname LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.email LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.unique_code LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.created_at LIKE '%".quote_smart($params['search']['value'])."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT A.id,A.firstname,A.lastname,A.email,A.unique_code,A.created_at,A.status,B.marital_status FROM `clients` AS A LEFT JOIN `profilebasic` AS B ON A.id=B.userid WHERE 1=1 ";

	/*  membership filter   
	if( !empty($params['columns'][1]['search']['value'])) 
	{
		
	}
	*/

	if( !empty($params['columns'][4]['search']['value'])) 
	{
		if($params['columns'][4]['search']['value'] == '11')
		{
			$where .=" AND A.status='0' ";
		}
		else
		if($params['columns'][4]['search']['value'] == '1')
		{
			$where .=" AND A.status='1' ";
		}
		else
		if($params['columns'][4]['search']['value'] == '2')
		{
			$where .= " AND A.status='2' ";
		}
		else
		if($params['columns'][4]['search']['value'] == '3')
		{
			$where .= " AND A.status='3' ";
		}
		else
		if($params['columns'][4]['search']['value'] == '-1')
		{
			$where .= " ";
		}
	}

	if(!empty($params['columns'][5]['search']['value'])) 
	{
		if($params['columns'][5]['search']['value'] != '0')
		{
			$where .=" AND A.gender='".$params['columns'][5]['search']['value']."' ";
		}
		else
		if($params['columns'][5]['search']['value'] == '0')
		{
			$where .=" ";
		}
	}

	if(!empty($params['columns'][6]['search']['value'])) 
	{
		if($params['columns'][6]['search']['value'] != '0')
		{
			$where .=" AND B.marital_status='".$params['columns'][6]['search']['value']."' ";
		}
		else
		if($params['columns'][6]['search']['value'] == '0')
		{
			$where .=" ";
		}
	}


	if(!empty($params['columns'][2]['search']['value']) || !empty($params['columns'][3]['search']['value'])) 
	{
		$from_date = $params['columns'][2]['search']['value'];
		$to_date = $params['columns'][3]['search']['value'];

		$age_from = date('Y-m-d', strtotime('-'.$from_date.' years'));
		$age_to = date('Y-m-d',strtotime("-".$to_date." years"));

		if($from_date!='-1' && $to_date!='-1')
		{
			$where .=" AND A.dob BETWEEN '$age_to' AND '$age_from' ";
		}
		else
		if($from_date=='-1' && $to_date=='-1')
		{
			$where .="";
		}
		else
		if($from_date!='-1' || $to_date!='-1')
		{
			if($from_date!='-1' && $to_date=='-1')
			{
				
				$where .=" AND A.dob <='$age_from'  ";
			}

			if($to_date!='-1' && $from_date=='-1')
			{
				
				$where .=" AND A.dob >='$age_to'  ";
			}
		}
	}

	

	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}

 	$sqlRec .=  " ORDER BY ". $columns[quote_smart($params['order'][0]['column'])]."   ".quote_smart($params['order'][0]['dir'])."  LIMIT ".quote_smart($params['start'])." ,".quote_smart($params['length'])." ";
//echo $sqlRec;exit;
 	
	$stmt1   = $link->prepare($sqlTot);
    $stmt1->execute();
    $totalRecords = $stmt1->rowCount();

	$stmt2   = $link->prepare($sqlRec);
    $stmt2->execute();
    $total_rows = $stmt2->rowCount();

    $result = $stmt2->fetchAll();

    foreach( $result as $row )
    {
    	$id = $row['0'];
        $status = $row['6'];

        if($status=='0')
        {
        	$row['6'] = "<b style='color:red;'>In-Active</b>";
        }
        else
        if($status=='1')
        {
        	$row['6'] = "<b style='color:green;'>Active</b>";
        }
        else
        if($status=='2')
        {
        	$row['6'] = "<b style='color:blue;'>Deactivated</b>";
        }
        else
        if($status=='3')
        {
        	$row['6'] = "<b style='color:red;'>Suspended</b>";
        }
       
        $row['0'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['0']."</a>";

        $row['1'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['1']."</a>";

        $row['2'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['2']."</a>";

        $row['3'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['3']."</a>";

        $row['4'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['4']."</a>";

        $row['5'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['5']."</a>";

        $row['6'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['6']."</a>";

        $data[] = $row;
    }

    $json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>