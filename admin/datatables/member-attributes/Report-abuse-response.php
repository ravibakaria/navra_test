<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

	if(!isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']=='' || $_SESSION['admin_user']==null))
	{
		header('Location: ../../login.php');
		exit;
	}

	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;
	//print_r($params);
	//define index of column
	$columns = array( 
		0 =>'A.abused_user_id',
		1 =>'A.abused_user_profile_id'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value if exist
	if( !empty($params['search']['value']) ) {   
		$where .=" AND ( A.abused_user_id LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.abused_user_profile_id LIKE '%".quote_smart($params['search']['value'])."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT A.abused_user_id,A.abused_user_profile_id,B.firstname,B.email,COUNT(*),B.status,A.abused_on FROM `report_abuse` AS A JOIN clients AS B ON A.abused_user_id=B.id GROUP BY A.abused_user_id ORDER BY COUNT(*) DESC, A.abused_on DESC";

	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}

 	//$sqlRec .=  " ORDER BY ". $columns[quote_smart($params['order'][0]['column'])]."   ".quote_smart($params['order'][0]['dir'])."  LIMIT ".quote_smart($params['start'])." ,".quote_smart($params['length'])." ";

 	
	$stmt1   = $link->prepare($sqlTot);
    $stmt1->execute();
    $totalRecords = $stmt1->rowCount();

	$stmt2   = $link->prepare($sqlRec);
    $stmt2->execute();
    $total_rows = $stmt2->rowCount();

    $result = $stmt2->fetchAll();

    foreach( $result as $row )
    {
    	$id = $row['0'];
    	$profile_id = $row['1'];
    	$first_Name = $row['2'];
    	$email_id = $row['3'];
    	$abused_count = $row['4'];
    	$status = $row['5'];

        if($status=='0')
        {
        	$status = "<b  class='label label-warning text-right' style='text-align:right;'>In-Active</b>";
        }
        else
        if($status=='1')
        {
        	$status = "<b  class='label label-success text-right' style='text-align:right;'>Active</b>";
        }
        else
        if($status=='2')
        {
        	$status = "<b  class='label label-danger text-right' style='text-align:right;'>Disabled</b>";
        }
        else
        if($status=='3')
        {
        	$status = "<b  class='label label-info text-right' style='text-align:right;'>Temporary In-Active</b>";
        }
        else
        if($status=='4')
        {
        	$status = "<b  class='label label-default text-right' style='text-align:right;'>Closed</b>";
        }
        else
        if($status=='5')
        {
        	$status = "<b  class='label label-danger text-right' style='text-align:right;'>Suspended</b>";
        }
        
        $row['0'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['0']."</a>";

        $row['1'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['1']."</a>";

        $row['2'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['2']."</a>";

        $row['3'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['3']."</a>";

        $row['4'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['4']."</a>";

        $row['5'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$status."</a>";

        $data[] = $row;
    }

    $json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>