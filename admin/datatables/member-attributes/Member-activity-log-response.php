<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

	if(!isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']=='' || $_SESSION['admin_user']==null))
	{
		header('Location: ../../login.php');
		exit;
	}


	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;
	$userid = $params['userid'];
	//define index of column

	$columns = array( 
		0 =>'created_On',
		1 =>'task',
		2 =>'activity',
		3 =>'IP_Address'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value if exist
	if( !empty($params['search']['value']) ) {   
		$where .=" AND ( task LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR activity LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR IP_Address LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR created_On LIKE '%".quote_smart($params['search']['value'])."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT created_On,task,activity,IP_Address FROM member_activity_logs WHERE userid='$userid'";
	

	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}

 	$sqlRec .=  " ORDER BY ". $columns[quote_smart($params['order'][0]['column'])]."   ".quote_smart($params['order'][0]['dir'])."  LIMIT ".quote_smart($params['start'])." ,".quote_smart($params['length'])." ";
//echo $sqlRec;exit;
 	
	$stmt1   = $link->prepare($sqlTot);
    $stmt1->execute();
    $totalRecords = $stmt1->rowCount();

	$stmt2   = $link->prepare($sqlRec);
    $stmt2->execute();
    $total_rows = $stmt2->rowCount();

    $result = $stmt2->fetchAll();

    foreach( $result as $row )
    {
    	$row['1'] = $row['1']." ".$row['2'];
    	$row['2'] = $row['3'];
        $data[] = $row;
    }

    $json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>