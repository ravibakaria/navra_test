<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

	if(!isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']=='' || $_SESSION['admin_user']==null))
	{
		header('Location: ../../login.php');
		exit;
	}

	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;
	//print_r($params);
	//define index of column
	$columns = array( 
		0 =>'A.id',
		1 =>'A.firstname',
		2 =>'A.lastname',
		3 =>'A.unique_code',
		4 =>'A.gender',
		5 =>'A.email',
		6 =>'A.phonenumber',
		7 =>'B.marital_status',
		8 =>'A.status'
	);

	$where = $sqlTot = $sqlRec = "";

	$gender = $_SESSION['view_member_gender'];
	$marital_status = $_SESSION['view_member_marital_status'];

	// check search value if exist
	if( !empty($params['search']['value']) ) {   
		$where .=" AND ( A.id LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.firstname LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.lastname LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.unique_code LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.gender LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.email LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.phonenumber LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR B.marital_status LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR A.status LIKE '%".quote_smart($params['search']['value'])."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT A.id,A.firstname,A.lastname,A.unique_code,A.gender,A.email,A.phonenumber,B.marital_status,A.status FROM `clients` AS A LEFT JOIN `profilebasic` AS B ON A.id=B.userid WHERE 1=1 ";

	if($gender=='0')
	{
		$where .= "";
	}
	else
	{
		$where .= " AND A.gender='$gender'";
	}

	if($marital_status=='0')
	{
		$where .= "";
	}
	else
	if($marital_status=='-1')
	{
		$where .= " AND  A.id NOT IN(SELECT userid FROM profilebasic)";
	}
	else
	{
		$where .= " AND  B.marital_status='$marital_status'";
	}

	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}

	//echo $sqlRec;exit;

 	$sqlRec .=  " ORDER BY ". $columns[quote_smart($params['order'][0]['column'])]."   ".quote_smart($params['order'][0]['dir'])."  LIMIT ".quote_smart($params['start'])." ,".quote_smart($params['length'])." ";


	$stmt1   = $link->prepare($sqlTot);
    $stmt1->execute();
    $totalRecords = $stmt1->rowCount();

	$stmt2   = $link->prepare($sqlRec);
    $stmt2->execute();
    $total_rows = $stmt2->rowCount();

    $result = $stmt2->fetchAll();
echo $sqlRec; die;
    foreach( $result as $row )
    {
    	$id = $row['0'];

    	$gender_db = $row['4'];
    	if($gender_db=='1')
    	{
    		$row['4'] = 'Male';
    	}
    	else
    	if($gender_db=='2')
    	{
    		$row['4'] = 'FeMale';
    	}
    	else
    	if($gender_db=='3')
    	{
    		$row['4'] = 'Other';
    	}

    	$marital_status_db = $row['7'];
    	if($marital_status_db=='1')
    	{
    		$row['7']='Never Married';
    	}
    	else
    	if($marital_status_db=='2')
    	{
    		$row['7']='Awaiting Divorce';
    	}
    	else
    	if($marital_status_db=='3')
    	{
    		$row['7']='Divorced';
    	}
    	else
    	if($marital_status_db=='4')
    	{
    		$row['7']='Widowed';
    	}
    	else
    	if($marital_status_db=='5')
    	{
    		$row['7']='Annulled';
    	}

    	$status_db = $row['8'];
    	if($status_db=='0')
        {
        	$row['8'] = "<b style='color:red;'>In-Active</b>";
        }
        else
        if($status_db=='1')
        {
        	$row['8'] = "<b style='color:green;'>Active</b>";
        }
        else
        if($status_db=='2')
        {
        	$row['8'] = "<b style='color:blue;'>Deactivate</b>";
        }
        else
        if($status_db=='3')
        {
        	$row['8'] = "<b style='color:red;'>Suspend</b>";
        }	

        $row['0'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['0']."</a>";

        $row['1'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['1']."</a>";

        $row['2'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['2']."</a>";

        $row['3'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['3']."</a>";

        $row['4'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['4']."</a>";

        $row['5'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['5']."</a>";

        $row['6'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['6']."</a>";

        $row['7'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['7']."</a>";

        $row['8'] = "<a href='member-profile.php?id=$id' class='member-profile' value='$id' id='$id' style='color:black;text-decoration:none;'>".$row['8']."</a>";

        $data[] = $row;
    }

    $json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>