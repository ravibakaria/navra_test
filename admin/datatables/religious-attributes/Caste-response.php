<?php
	session_start();
	require_once '../../../config/config.php'; 
	include('../../../config/dbconnect.php');    //database connection
	include('../../../config/functions.php');   //strip query string

	if(!isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']=='' || $_SESSION['admin_user']==null))
	{
		header('Location: ../../login.php');
		exit;
	}

	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;
	//print_r($params);
	//define index of column
	$columns = array( 
		0 =>'id',
		1 =>'name'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value if exist
	if( !empty($params['search']['value']) ) {   
		$where .=" AND ( id LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR name LIKE '%".quote_smart($params['search']['value'])."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT id,name,status FROM caste WHERE 1=1";

	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	$sqlRec .=  " ORDER BY ". $columns[quote_smart($params['order'][0]['column'])]."   ".quote_smart($params['order'][0]['dir'])."  LIMIT ".quote_smart($params['start'])." ,".quote_smart($params['length'])." ";

 	
	$stmt1   = $link->prepare($sqlTot);
    $stmt1->execute();
    $totalRecords = $stmt1->rowCount();

	$stmt2   = $link->prepare($sqlRec);
    $stmt2->execute();
    $total_rows = $stmt2->rowCount();

    $result = $stmt2->fetchAll();

    foreach( $result as $row )
    {
    	$id = $row['0'];
        $name = $row['1'];
        $status = $row['2'];
       
        if($status=='0')
        {
        	$row['2'] = "<a href='javascript:void(0)' class='change_status_Caste' value='$status,$id' id='$status,$id' style='text-decoration:none;'><b class='label label-danger text-right' style='text-align:right;'>Disabled</b></a>
        		<div class='Caste_status".$id."'></div>";
        }
        else
        {
        	$row['2'] = "<a href='javascript:void(0)' class='change_status_Caste' value='$status,$id' id='$status,$id' style='text-decoration:none;'><b  class='label label-success text-right' style='text-align:right;'>Active</b></a>
        		<div class='Caste_status".$id."'></div>";
        }

        $row['0'] = "<a href='update-Caste-attributes.php?id=$id' style='color:black;text-decoration:none;'>".$row['0']."</a>";
    	$row['1'] = "<a href='update-Caste-attributes.php?id=$id' style='color:black;text-decoration:none;'>".$row['1']."</a>";
        $row['3'] = "<a href='update-Caste-attributes.php?id=$id'><i class='fa fa-pencil fa-2x edit-fa'></i></a>";

        $data[] = $row;
    }

    $json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>