<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body update-section">
		<a href="project-configuration.php?main_tab=ProjectAttributes&sub_tab=Custome_Meta_Tags_Settings" id="portletReset" type="button" class="mb-xs mt-xs mr-xs btn btn-default" style="float:right;"><i class="fa fa-arrow-left"></i> Back</a>

		<?php
			$id = quote_smart($_GET['id']);

			if ( !is_numeric( $id ) )
		    {
		        echo 'Error! Invalid Data';
		        exit;
		    }

		    $sql = "SELECT * FROM `custom_meta_tags` WHERE `id`='$id'";
		    $stmt1   = $link->prepare($sql);
            $stmt1->execute();
            $queryTot = $stmt1->rowCount();
            $result = $stmt1->fetch();

           	if($queryTot>0)
           	{
           		$id = $result['id'];
           		$meta_tag = $result['meta_tag'];
           		$meta_tag = str_replace("'","\"",$meta_tag);
           		$meta_tag = str_replace("--lt","<",$meta_tag);
		?>
		<br/><br/>

		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-md-3 col-lg-3">
					</div>
					<div class="col-md-6 col-lg-6 edit-master-div">
						<div class="row">
							<center><h4>Update Meta Tag Information</h4></center>
						</div>
							<hr/>
						<div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <lable class="control-label">Meta Tag:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                            	<textarea class="form-control meta_tag" rows='4'><?php echo $meta_tag;?></textarea>
                            </div>
                        </div>
                        <input type="hidden" class="meta_tag_id" value="<?php echo $id;?>">

						<div class="row">
							<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 update_status">
								
							</div>	
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><button class="btn btn-success btn_update_meta_tag website-button">Update</button></center><br/>	
							</div>
						</div>
						
					</div>
					<div class="col-md-3 col-lg-3">
					</div>
				</div>
				<div class="row">
					<br/>
				</div>
			</div>
			
		</div>
		<?php
			}
			else
			{
				echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
			}
		?>
</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		$('.btn_update_meta_tag').click(function(){
			var meta_tag_id = $('.meta_tag_id').val();
            var meta_tag = $('.meta_tag').val();
            var meta_tag = meta_tag.replace("<", "--lt");
    		var meta_tag = meta_tag.replace(/'/g, '"');
    		var task = "update_custom_meta_tag";

    		if(meta_tag=='' || meta_tag==null)
    		{
    			$('.update_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> Please enter meta tag.</div>");
                        return false;
    		}

    		var data = {
                meta_tag_id : meta_tag_id,
                meta_tag : meta_tag,
                task:task
            }

    		$('.update_status').html("");

            $('.loading_img').show();

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,                
                url:'query/general_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Meta tag Updated Successfully.</div></center>");
                       	$('.update_status_success').fadeTo(1000, 500).slideUp(500, function(){
                              window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Custome_Meta_Tags_Settings");
                              return false;
                        });
                    }
                    else
                    {
                        $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>