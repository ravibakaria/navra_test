<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body update-section">
		<a href="vendors.php?" id="portletReset" type="button" class="mb-xs mt-xs mr-xs btn btn-default" style="float:right;"><i class="fa fa-arrow-left"></i> Back</a>

		<?php
			$vuserid = quote_smart($_GET['id']);

			if ( !is_numeric( $vuserid ) )
		    {
		        echo 'Error! Invalid Data';
		        exit;
		    }

		    $sql = "SELECT * FROM `vendors` WHERE `vuserid`='$vuserid'";
		    $stmt1   = $link->prepare($sql);
            $stmt1->execute();
            $queryTot = $stmt1->rowCount();
            $result = $stmt1->fetch();

           	if($queryTot>0)
           	{
           		$firstname = $result['firstname'];
           		$lastname = $result['lastname'];
           		$email = $result['email'];
           		$phonecode = $result['phonecode'];
           		$phone = $result['phone'];
           		$company_name = $result['company_name'];
           		$category_id = $result['category_id'];
           		$city = $result['city'];
           		$state = $result['state'];
           		$country = $result['country'];
           		$short_desc = $result['short_desc'];
           		$status = $result['status'];
		?>
		<br/><br/>
		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-md-3 col-lg-3">
					</div>
					<div class="col-md-6 col-lg-6 edit-master-div">
						<div class="row"><br/>
							<center><h4>Update <strong><?php echo $firstname.' '.$lastname;?></strong> Vendor Information</h4></center>
						</div>
						<div class="row text-right">
							<button class="btn reset-send-password website-button">Reset & Send Password</button>
							
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 send-password-status">
								
							</div>
							
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img1' id='loading_img1' style='width:40px; height:40px; display:none;'/></center>
							</div>

						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">First Name:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control form-control-sm firstname" value="<?php echo $firstname; ?>" />
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Last Name:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control form-control-sm lastname" value="<?php echo $lastname; ?>" />
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Company Name:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control form-control-sm company_name" value="<?php echo $company_name; ?>" />
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Category:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<select class="form-control category_id">
									<?php
										$sql_category = "SELECT * FROM membercategory WHERE status='1' ORDER BY name ASC";
										$stmt_category = $link->prepare($sql_category);
										$stmt_category->execute();
										$result_category = $stmt_category->fetchAll();

										foreach ($result_category as $row_category) 
										{
											$cat_id = $row_category['id'];
											$cat_name1 = $row_category['name'];

											if($category_id==$cat_id)
											{
												echo "<option value='".$cat_id."' selected>".$cat_name1."</option>";
											}
											else
											{
												echo "<option value='".$cat_id."'>".$cat_name1."</option>";
											}
										}
									?>
								</select>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Short Description:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<textarea class="form-control form-control-sm short_desc" rows="5"><?php echo $short_desc; ?></textarea>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Country:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<select class="form-control country">
									<?php
										$sql_country = "SELECT * FROM countries WHERE status='1'";
										$stmt_country = $link->prepare($sql_country);
										$stmt_country->execute();
										$result_country = $stmt_country->fetchAll();

										foreach ($result_country as $row_country) 
										{
											$country_id = $row_country['id'];
											$country_name1 = $row_country['name'];

											if($country==$country_id)
											{
												echo "<option value='".$country_id."' selected>".$country_name1."</option>";
											}
											else
											{
												echo "<option value='".$country_id."'>".$country_name1."</option>";
											}
										}
									?>
								</select>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">State:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<select class="form-control state">
									<?php
										$sql_state = "SELECT * FROM states WHERE status='1' AND country_id='$country'";
										$stmt_state = $link->prepare($sql_state);
										$stmt_state->execute();
										$result_state = $stmt_state->fetchAll();

										foreach ($result_state as $row_state) 
										{
											$state_id = $row_state['id'];
											$state_name1 = $row_state['name'];

											if($state==$state_id)
											{
												echo "<option value='".$state_id."' selected>".$state_name1."</option>";
											}
											else
											{
												echo "<option value='".$state_id."'>".$state_name1."</option>";
											}
										}
									?>
								</select>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">City:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<select class="form-control city">
									<?php
										$sql_city = "SELECT * FROM cities WHERE status='1' AND state_id='$state'";
										$stmt_city = $link->prepare($sql_city);
										$stmt_city->execute();
										$result_city = $stmt_city->fetchAll();

										foreach ($result_city as $row_city) 
										{
											$city_id = $row_city['id'];
											$city_name1 = $row_city['name'];

											if($city==$city_id)
											{
												echo "<option value='".$city_id."' selected>".$city_name1."</option>";
											}
											else
											{
												echo "<option value='".$city_id."'>".$city_name1."</option>";
											}
										}
									?>
								</select>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Email Id:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control form-control-sm email" value="<?php echo $email; ?>" />
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Mobile:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<div class="input-group">
                                    <span class="input-group-addon country_code">
                                        + <?php echo $phonecode;?>
                                    </span>
                                    <input type="text" name="phone" class="form-control phone" placeholder="Mobile Number" maxlength="20" value="<?php echo $phone; ?>">
                                </div>
                                <input type="hidden"  name="phonecode" class="phonecode" value="<?php echo $phonecode; ?>" />
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label">Status:</label>
							</div>

							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<center>
								<?php
									if($status=='0')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Category_current_status' name='Category_current_status' value='".$status."'>";
									}
									else
									if($status=='1')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Category_current_status' name='Category_current_status' checked value='".$status."'>";
									}
								?>
							</center>
							</div>
						</div>
						<br/>
						<input type="hidden" class="vuserid" value="<?php echo $vuserid;?>">

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 update_status">

							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
							</div>	
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><button class="btn btn-success btn_update_Category website-button">Update</button></center><br/>	
							</div>
						</div>
						
					</div>
					<div class="col-md-3 col-lg-3">
					</div>
				</div>
				<div class="row">
					<br/>
				</div>
			</div>
			
		</div>
		<?php
			}
			else
			{
				echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
			}
		?>
</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){

		/*   Prevent entering charaters in mobile & phone number   */
        $(".phone").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.add_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Albhabets not allowed. Enter Digits only.</div>").show().fadeOut(3000);
                return false;
            }
        });

        $('.country').change(function(){         //Fetch states
            var country_id = $(this).val();
            var task = "Fetch_state_data";  

            $.ajax({
                type:'post',
                data:'country_id='+country_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    var result = $.parseJSON(res);
                    $('.state').html(result[0]);
                    $('.country_code').html("+"+result[1]);
                    $('.country_phone_code').val(result[1]);
                }
            });         
        });

        $('.state').change(function(){         //Fetch cities
            var state_id = $(this).val();
            var task = "Fetch_city_data";  
            $.ajax({
                type:'post',
                data:'state_id='+state_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    $('.city').html(res);
                }
            });         
        });

        /**************   Update vendor data    ****************/
		$('.btn_update_Category').click(function(){
			var firstname = $('.firstname').val();
			var lastname = $('.lastname').val();
			var company_name = $('.company_name').val();
			var category_id = $('.category_id').val();
			var short_desc = $('.short_desc').val();
			var country = $('.country').val();
			var state = $('.state').val();
			var city = $('.city').val();
			var email = $('.email').val();
			var phonecode = $('.phonecode').val();
			var phone = $('.phone').val();
			var status = $('.Category_current_status').is(":checked");
			if(status==true)
			{
				status='1';
			}
			else
			if(status==false)
			{
				status='0';
			}

			var vuserid = $('.vuserid').val();
			var task = "Update_Vendor_Attributes";

			if(firstname=='' || firstname==null)
			{
				$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter first name.</div></center>");
        			return false;
			}

			if(lastname=='' || lastname==null)
			{
				$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter last name.</div></center>");
        			return false;
			}

			if(company_name=='' || company_name==null)
			{
				$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter company name.</div></center>");
        			return false;
			}

			if(short_desc=='' || short_desc==null)
			{
				$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter short description.</div></center>");
        			return false;
			}

			if(state=='0')
			{
				$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select state.</div></center>");
        			return false;
			}

			if(city=='0')
			{
				$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select city.</div></center>");
        			return false;
			}

			if(email=='' || email==null)
			{
				$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter email address.</div></center>");
        			return false;
			}

			if(phone=='' || phone==null)
			{
				$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter mobile number.</div></center>");
        			return false;
			}

			var data = {
				firstname : firstname,
				lastname : lastname,
				company_name : company_name,
				category_id : category_id,
				short_desc : short_desc,
				country : country,
				state : state,
				city : city,
				email : email,
				phonecode : phonecode,
				phone : phone,
				status : status,
				vuserid : vuserid,
				task : task
			};

			$('.loading_img').show();
			$('.update_status').html("");

			$.ajax({
				type:'post',
				dataType:'json',
	        	data:data,
	        	url:'query/wedding-category/vendor-helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data updated successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});

		/**************   Reset & send password   **************/
		$('.reset-send-password').click(function(){
			var vuserid = $('.vuserid').val();
			var task = "Reset-Send-Password";

			var data = {
				vuserid : vuserid,
				task : task
			};

			$('.reset-send-password').attr('disabled',true);
			$('.loading_img1').show();

			$.ajax({
				type:'post',
				dataType:'json',
				data:data,
				url:'query/wedding-category/vendor-helper.php',
				success:function(res)
				{
					$('.loading_img').hide();
					if(res=='success')
					{
						$('.send-password-status').html("<center><div class='alert alert-success alert-dismissible' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a><span class='fa fa-check'></span><strong> Success!</strong> Password reset & sent email to vendor successfully.</strong>.</div></center>");
					}
					else
					{
						$('.send-password-status').html("<div class='alert alert-danger alert-dismissible' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a><span class='fa fa-exclamation-circle'></span><strong> Error!</strong> "+res+"</strong>.</div>");
						return false;
					}
				}
			});
		});
	});
</script>