<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>
	<section role="main" class="content-body">
		
		<!-- start: page -->
			<div class="row admin_start_section">
				<div class="row">
				    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				        <div class="row div-header-row">
				            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				                <h1>Vendors Setting</h1>
				            </div>
				            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
				                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Vendor">Add New Vendor</button>
				            </div>
				        </div>

				        <div class="row">
				            <div style="">
				                <center>
				                    <table id='datatable-info' class='table-hover table-striped table-bordered vendor_list' style="width:100%;">
				                        <thead>
				                            <tr>
				                                <th>ID</th>
				                                <th>First Name</th>
				                                <th>Last Name</th>
				                                <th>Email Id</th>
				                                <th>Phone</th>
				                                <th>Company Name</th>
				                                <th>Status</th>
				                                <th>Edit</th>
				                            </tr>
				                        </thead>
				                    </table>
				                </center>
				            </div>

				            <!-- Modal -->
				            <div class="modal fade admin_add_model" id="Add-New-Vendor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
				                    <div class="modal-content">
				                        <div class="modal-header">
				                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Vendor</h4></center>
				                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                            <span aria-hidden="true">&times;</span>
				                            </button>
				                        </div>
				                        <div class="modal-body">
				                            <div class="row input-row">
				                            	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				                                    <input type="text" class="form-control firstname" placeholder="First Name"/>
					                            </div>
					                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					                                <input type="text" class="form-control lastname" placeholder="Last Name" />
					                            </div>
				                            </div>

				                            <div class="row input-row">
				                            	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				                                    <input type="text" class="form-control company_name" placeholder="Company Name"/>
				                                    <select class="form-control category_id" style="margin-top: 20px;">
				                                    	<option value="0">--Select Category--</option>
				                                    	<?php
				                                    		$sql_category = "SELECT * FROM membercategory WHERE status='1'";
				                                    		$stmt_category = $link->prepare($sql_category);
				                                    		$stmt_category->execute();
				                                    		$count_category = $stmt_category->rowCount();
				                                    		if($count_category>0)
				                                    		{
				                                    			$result_category = $stmt_category->fetchAll();
				                                    			foreach ($result_category as $row_category) 
				                                    			{
				                                    				$id = $row_category['id'];
				                                    				$name = $row_category['name'];

				                                    				echo "<option value='".$id."'>".$name."</option>";
				                                    			}
				                                    		}
				                                    	?>
				                                    </select>
					                            </div>
					                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					                                <textarea class="form-control short_desc" placeholder="Short Description" rows="4"/></textarea>
					                            </div>
				                            </div>
				                            
				                            <div class="row input-row">
				                            	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				                                    <select class="form-control country">
				                                    	<option value="0">--Select Country--</option>
				                                    	<?php
				                                    		$sql_country = "SELECT * FROM countries WHERE status='1'";
				                                    		$stmt_country = $link->prepare($sql_country);
				                                    		$stmt_country->execute();
				                                    		$result_country = $stmt_country->fetchAll();
				                                    		foreach ($result_country as $row_country) 
				                                    		{
				                                    			$country_id = $row_country['id'];
				                                    			$country_name = $row_country['name'];

				                                    			echo "<option value='".$country_id."'>".$country_name."</option>";
				                                    		}
				                                    	?>
				                                    </select>
					                            </div>
					                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				                                    <select class="form-control state">
				                                    	<option value="0">--Select State--</option>
				                                    </select>
					                            </div>
					                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				                                    <select class="form-control city">
				                                    	<option value="0">--Select City--</option>
				                                    </select>
					                            </div>
				                            </div>

				                            <div class="row input-row">
				                            	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				                                    <input type="text" class="form-control email" placeholder="Email Id"/>
					                            </div>
					                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					                                <div class="input-group">
			                                            <span class="input-group-addon country_code">
			                                                
			                                            </span>
			                                            <input type="text" name="phone" class="form-control phone" placeholder="Mobile Number" maxlength="20">
			                                        </div>
			                                        <input type="hidden"  name="country_phone_code" class="country_phone_code" />
					                            </div>
				                            </div>

				                            <div class="row input-row">
				                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status">
				                                </div>
				                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
				                                </div>
				                            </div>
				                        </div>
				                        <div class="modal-footer">
				                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				                            <button type="button" class="btn btn-primary btn_add_new_vendor website-button">Submit</button>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
			</div>
		<!-- end: page -->
	</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){

		var dataTable_vendor_list = $('.vendor_list').DataTable({          //vendor_list datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            "ajax":{
                url :"datatables/wedding-directory/vendor-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".vendor_list_processing").css("display","none");
                }
            }
        });

        /*   Prevent entering charaters in mobile & phone number   */
        $(".phone").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.add_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Albhabets not allowed. Enter Digits only.</div>").show().fadeOut(3000);
                return false;
            }
        });

        $('.country').change(function(){         //Fetch states
            var country_id = $(this).val();
            var task = "Fetch_state_data";  

            $.ajax({
                type:'post',
                data:'country_id='+country_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    var result = $.parseJSON(res);
                    $('.state').html(result[0]);
                    $('.country_code').html("+"+result[1]);
                    $('.country_phone_code').val(result[1]);
                }
            });         
        });

        $('.state').change(function(){         //Fetch cities
            var state_id = $(this).val();
            var task = "Fetch_city_data";  
            $.ajax({
                type:'post',
                data:'state_id='+state_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    $('.city').html(res);
                }
            });         
        });

		$('.btn_add_new_vendor').click(function(){
			var firstname = $('.firstname').val();
			var lastname = $('.lastname').val();
			var email = $('.email').val();
			var phonecode = $('.country_phone_code').val();
			var phone = $('.phone').val();
			var company_name = $('.company_name').val();
			var category_id = $('.category_id').val();
			var city = $('.city').val();
			var state = $('.state').val();
			var country = $('.country').val();
			var short_desc = $('.short_desc').val();
			var task = "add-new-vendor";

			if(firstname=='' || firstname==null)
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please enter first name</strong>.</div>");
				return false;
			}

			if(lastname=='' || lastname==null)
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please enter last name</strong>.</div>");
				return false;
			}

			if(company_name=='' || company_name==null)
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please enter company name</strong>.</div>");
				return false;
			}

			if(category_id=='0')
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please select vendor category.</strong>.</div>");
				return false;
			}

			if(short_desc=='' || short_desc==null)
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please enter company short description</strong>.</div>");
				return false;
			}

			if(country=='0')
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please select country.</strong>.</div>");
				return false;
			}

			if(state=='0')
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please select state.</strong>.</div>");
				return false;
			}

			if(city=='0')
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please select city.</strong>.</div>");
				return false;
			}

			if(email=='' || email==null)
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please enter email id</strong>.</div>");
				return false;
			}

			function validateEmail($email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $email );
            }

            if(email!='' && !validateEmail(email)) 
            { 
                $('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Invalid data!</strong> Please enter valid email id</strong>.</div>");
				return false;
            }

			if(phone=='' || phone==null)
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please enter mobile number</strong>.</div>");
				return false;
			}


			var data = {
				firstname : firstname,
				lastname : lastname,
				email : email,
				phonecode : phonecode,
				phone : phone,
				company_name : company_name,
				category_id : category_id,
				city : city,
				state : state,
				country : country,
				short_desc : short_desc,
				task : task
			};

			$('.loading_img').show();
			$('.add_status').html();

			$.ajax({
				type:'post',
				dataType:'json',
				data:data,
				url:'query/wedding-category/vendor-helper.php',
				success:function(res) 
				{
					$('.loading_img').hide();
					if(res=='success')
					{
						$('.add_status').html("<div class='alert alert-success add_status_success' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-check'></span><strong> Success!</strong> New vendor added successfully.</strong>.</div>");
						$('.add_status_success').fadeTo(1000, 500).slideUp(500, function(){
                              location.reload();
                        });
					}
					else
					{
						$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Error!</strong> "+res+"</strong>.</div>");
						return false;
					}
				}
			});
		});

		//Complexion change status
        $('.vendor_list tbody').on('click', '.change_status_vendor', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_Vendor_status";

            var data = {
            	status : status,
            	id : id,
            	task : task
            };

            $.ajax({
                type:'post',
                dataType:'json',
                data:data,
                url:'query/wedding-category/vendor-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.vendor_status'+id).html("<div class='alert alert-success vendor_status_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.vendor_status_status').fadeTo(1000, 500).slideUp(500, function(){
                                  location.reload();
                            });
                    }
                    else
                    {
                        $('.vendor_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });
	});
</script>