<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<link rel="stylesheet" href="../css/croppie.css" />
	
</head>
<body>
	<section role="main" class="content-body">
		
		<!-- start: page -->
			<div class="row admin_start_section">
				<h1>Payment Details</h1>
				<hr class="setting-devider"/>
				<div class="row">
		            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		            	<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
		                    <h4>Apply Filters <i class='fa fa-filter'></i>:</h4> 
		                </div>
		                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
		                    Payment Status: 
		                    <select class='form-control payment_status' data-column="1">
		                        <option value='-1'>Select/Reset--</option>
		                        <option value='0'>Un-Paid</option>
		                        <option value='1'>Paid</option>
		                        <option value='2'>Canceled/Failed</option>
		                        <option value='3'>Refund</option>
		                    </select>
		                </div>
		                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
		                    From Date: <span class="badge" data-toggle="tooltip" data-placement="top" title="To reset date filter again select the previously selected/entered date"><span class="fa fa-exclamation"></span></span>
		                    <input type="text" class="form-control from_date" placeholder="YYYY-MM-DD" name="from_date" data-column="2">
		                </div>		                
		                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
		                    To Date: <span class="badge" data-toggle="tooltip" data-placement="top" title="To reset date filter again select the previously selected/entered date"><span class="fa fa-exclamation"></span></span>
		                    <input type="text" class="form-control to_date" placeholder="YYYY-MM-DD" name="to_date" data-column="3">
		                </div>
		            </div>
		        </div>
		        <hr>
				<div class="row">
    				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 payment-summary-table">
    					<div class="row div-header-row">
    						<div>
				                <center>
				                    <table id='datatable-info' class='table-hover table-striped table-bordered payment_summary_list' style="width:100%">
				                        <thead>
				                            <tr>
				                                <th>Order#</th>
				                                <th>Order Date</th>
				                                <th>Payment Date</th>
				                                <th>First Name</th>
				                                <th>Last Name</th>
				                                <th>Email Id</th>
				                                <th>Profile ID</th>
				                                <th>Total Price</th>
				                                <th>Status</th>
				                                <th>Payment Gateway</th>
				                                <th>View</th>
				                            </tr>
				                        </thead>
				                    </table>
				                <center>
				            </div>
    					</div>
    				</div>
    			</div>
			</div>
		<!-- end: page -->
	</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>
<script>
    $(document).ready(function(){
    	/*  Datepicker setting  */
        var date_input=$('input[name="from_date"],input[name="to_date"]');
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        
        date_input.datepicker({
          format: 'yyyy-mm-dd',
          container: container,
          todayHighlight: true,
          autoclose: true,
        })

    	var dataTable = $('.payment_summary_list').DataTable({  //payment
            "order": [[ 1, "desc" ]],
            "bProcessing": true,
            "serverSide": true,            
            "ajax":{
                url :"datatables/payment-settings/payment-summary-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".payment_summary_list_processing").css("display","none");
                }
            }
        });

        $('.payment_status').change(function(){
            var i =$(this).attr('data-column');
            var v =$(this).val();
            dataTable.columns(i).search(v).draw();
        });

        $('.from_date').on("changeDate", function() {
            var i =$(this).attr('data-column');
            var v =$(this).val();
            dataTable.columns(i).search(v).draw();
        });

        $('.to_date').on("changeDate", function() {
            var i =$(this).attr('data-column');
            var v =$(this).val();
            dataTable.columns(i).search(v).draw();
        });
    });
</script>