<?php
	session_start();
	ini_set('max_execution_time', 7200); //300 seconds = 5 minutes
	$install_file_name = 'install.php';
	$config_file_name = '../config/config.php';
	
	if(file_exists($config_file_name) && file_exists($install_file_name)) 
	{
		header('Location: index.php');
		exit;
	}

	if(isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']!='' || $_SESSION['admin_user']!=null))
	{
		header('Location: index.php');
		exit;
	}
?>

<!doctype html>
<html class="fixed">
	<head>
		<title>Install Configuration</title>
		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../css/bootstrap.css" />
		<link rel="stylesheet" href="../css/font-awesome.css" />
		<link rel="stylesheet" href="../css/magnific-popup.css" />
		<link rel="stylesheet" href="../css/datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="../css/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../css/theme-custom.css">

		<!-- Custom CSS -->
		<link rel="stylesheet" href="../css/custom.css">

		<!-- Font awesome -->
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<!-- Head Libs -->
		<script src="../js/modernizr.js"></script>

	</head>
	<body class="login-body">
		<!-- start: page -->
		
		<section class="body-sign">
			<div class="center-sign">

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none title-login-forgot"><i class="fa fa-user mr-xs"></i> Install data</h2>
					</div>
					
					<div class="panel-body">
						<?php
							$domain_path = $_SERVER["SERVER_NAME"];
							$current_directory = $_SERVER['REQUEST_URI'];

							$working_directory = explode('/',$current_directory);
							$directory_count = count($working_directory);

							if($domain_path=='localhost')
							{
								$count = $directory_count-3;
							}
							else
							{
								$count = $directory_count-2;
							}
							
							$fetch_directory=null;
							for($i=1;$i<$count;$i++)
							{
								$fetch_directory .= $working_directory[$i];
								if($i!=$count-1)
								{
									$fetch_directory .= '/';
								}
							}
							
							$server_url =  $_SERVER["SERVER_NAME"].$fetch_directory;

							if($_SERVER['HTTPS']=='on')
							{
							    $server_url = 'https://'.$server_url;
							}
							else
							if($_SERVER['HTTPS']!='on')
							{
							    echo "<p style='color:red;'><b>Please install SSL certificate & then continue.</b></p>";
							}
						?>
						
						<form>
							<div class="url-path">
								<p align="center"><strong>Base Path: </strong><?php echo $server_url;?></p>
								<p align="center"><strong>Admin Path: </strong><?php echo $server_url.'/admin';?></p>
								<input type="hidden" class="base_path" value="<?php echo $server_url;?>">
							</div>

							<hr class="dotted">

							<div class="url-path">
								<div class="alert alert-info" style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Database Configuration </strong></div>
							</div>

							<div class="form-group mb-lg">
								<label>Database Host Name:</label> <span class='fa fa-exclamation-circle' data-toggle="tooltip" data-placement="top" title="Enter your database host name. For Ex: localhost"></span>

								<div class="input-group input-group-icon">
									<input name="host_name" type="text" class="form-control input-lg host_name" placeholder="Ex: localhost" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<label>Database Name:</label> <span class='fa fa-exclamation-circle' data-toggle="tooltip" data-placement="top" title="Enter your database name"></span>
								<div class="input-group input-group-icon">
									<input name="db_name" type="text" class="form-control input-lg db_name" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<label>Database User Name:</label> <span class='fa fa-exclamation-circle' data-toggle="tooltip" data-placement="top" title="Enter your database username"></span>
								<div class="input-group input-group-icon">
									<input name="db_user_name" type="text" class="form-control input-lg db_user_name" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									
								</div>
								<div class="input-group input-group-icon">
									<p style="background-color: #efefef;padding: 7px;color:red">
										<strong>Note: </strong>While setting up password for database user for strong password please use special characters from given list.<br/>
										If you receive an error while connecting to database then please check your database password contains special character other than the given list of special characters<br/>
										Special characters list  !@#$%^*()_=+{}|;:,<.><br/>
									</p>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Database User Password:</label> <span class='fa fa-exclamation-circle' data-toggle="tooltip" data-placement="top" title="Enter your database password"></span>
								</div>
								<div class="input-group input-group-icon">
									<input name="db_password" type="password" class="form-control input-lg db_password" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-7">
									<div class="mb-xs text-center db_form_status">
									</div>
								</div>
								<div class="col-sm-5 text-right">
									<a class="btn btn-primary btn_chk_database">Check Database</a>
								</div>
							</div>

							<hr class="dotted">

							<div class="url-path">
								<div class="alert alert-info" style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Website Configuration </strong></div>
							</div>
							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Site Title:</label> <span class='fa fa-exclamation-circle' data-toggle="tooltip" data-placement="top" title="Enter website title"></span>
								</div>
								<div class="input-group input-group-icon">
									<input name="WebSiteTitle" type="text" class="form-control input-lg WebSiteTitle" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-file-text-o"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Site Tagline:</label> <span class='fa fa-exclamation-circle' data-toggle="tooltip" data-placement="top" title="Enter website tagline/short description">
								</div>
								<div class="input-group input-group-icon">
									<input name="WebSiteTagline" type="text" class="form-control input-lg WebSiteTagline" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-file-text-o"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Admin User Name:</label> <span class='fa fa-exclamation-circle' data-toggle="tooltip" data-placement="top" title="Admin user name for your website">
								</div>
								<div class="input-group input-group-icon">
									<input name="admin_user_name" type="text" class="form-control input-lg admin_user_name" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Admin Email Id:</label> <span class='fa fa-exclamation-circle' data-toggle="tooltip" data-placement="top" title="Enter admin user email id">
								</div>
								<div class="input-group input-group-icon">
									<input name="admin_email" type="text" class="form-control input-lg admin_email" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-envelope"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									
								</div>
								<div class="input-group input-group-icon">
									<p style="background-color: #efefef;padding: 7px;color:red">
										<b>Instructions to set password for Admin User:</b><br/>
										Password should contain atleast one uppercase character.<br/>
										Password should contain atleast one digit.<br/>
										Password should contain atleast one special character from !@#$%^&*()_=+{}|;:,<.><br/>
									</p>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Admin Password:</label> <span class='fa fa-exclamation-circle' data-toggle="tooltip" data-placement="top" title="Enter admin user password">
								</div>
								<div class="input-group input-group-icon">
									<input name="admin_password" type="password" class="form-control input-lg admin_password" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Confirm Password:</label> <span class='fa fa-exclamation-circle' data-toggle="tooltip" data-placement="top" title="Confirm admin user password">
								</div>
								<div class="input-group input-group-icon">
									<input name="admin_confirm_password" type="password" class="form-control input-lg admin_confirm_password" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="mb-xs text-center form_status">
								
							</div>

							<div class="row">
								<div class="col-sm-8">
									<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
								</div>
								<div class="col-sm-4 text-right">
									<a class="btn btn-primary btn_install">Install</a>
								</div>
							</div>

							
						</form>
					</div>
				</div>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="../js/jquery.js"></script>
		<script src="../js/jquery.browser.mobile.js"></script>
		<script src="../js/bootstrap.js"></script>
		<script src="../js/nanoscroller.js"></script>
		<script src="../js/bootstrap-datepicker.js"></script>
		<script src="../js/magnific-popup.js"></script>
		<script src="../js/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../js/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../js/theme.init.js"></script>

	</body>
</html>

<script>
	$(document).ready(function(){

		/* empty error message  */
		$('.host_name, .db_name, .db_user_name, .db_password, .admin_user_name, .admin_password, .admin_confirm_password, .admin_email, .WebSiteTitle, .WebSiteTagline').click(function(){
            $('.host_name, .db_name, .db_user_name, .db_password, .admin_user_name, .admin_password, .admin_confirm_password, .admin_email, .WebSiteTitle, .WebSiteTagline').removeClass("danger_error");
        });

        $('.btn_chk_database').click(function(){
        	var host_name = $('.host_name').val();
			var db_name = $('.db_name').val();
			var db_user_name = $('.db_user_name').val();
			var db_password = $('.db_password').val();
			var task = "Check_db_connectivity";
			if(host_name=='' || host_name==null)
			{
				$('.db_form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter host name.</div>");
				$(".host_name").addClass("danger_error");
				return false;
			}

			if(db_name=='' || db_name==null)
			{
				$('.db_form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter database name.</div>");
				$(".db_name").addClass("danger_error");
				return false;
			}

			if(db_user_name=='' || db_user_name==null)
			{
				$('.db_form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter user name.</div>");
				$(".db_user_name").addClass("danger_error");
				return false;
			}

			if(db_password=='' || db_password==null)
			{
				$('.db_form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter database password.</div>");
				$(".db_password").addClass("danger_error");
				return false;
			}

			var data = 'host_name='+host_name+'&db_name='+db_name+'&db_user_name='+db_user_name+'&db_password='+db_password+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/install_helper.php',
            	success:function(res)
            	{
            		if(res=='success')
            		{
            			$('.db_form_status').html("<div class='alert alert-success success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Connected To Database Successfully.</div>");
            			$('.btn_chk_database').attr('disabled',true);
            			$('.host_name').attr('disabled',true);
            			$('.db_name').attr('disabled',true);
            			$('.db_user_name').attr('disabled',true);
            			$('.db_password').attr('disabled',true);
            			return false;
            		}
            		else
            		{
            			$('.db_form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}
            	}
            });

        });

		$('.btn_install').click(function(){
			var base_path = $('.base_path').val();
			var host_name = $('.host_name').val();
			var db_name = $('.db_name').val();
			var db_user_name = $('.db_user_name').val();
			var db_password = $('.db_password').val();
			var WebSiteTitle = $('.WebSiteTitle').val();
			var WebSiteTagline = $('.WebSiteTagline').val();
			var admin_user_name = $('.admin_user_name').val();
			var admin_email = $('.admin_email').val();
			var admin_password = $('.admin_password').val();
			var admin_confirm_password = $('.admin_confirm_password').val();
			var task = "install_data_in";

			if(WebSiteTitle=='' || WebSiteTitle==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter website title.</div>");
				$(".WebSiteTitle").addClass("danger_error");
				return false;
			}

			if(WebSiteTagline=='' || WebSiteTagline==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter website tagline.</div>");
				$(".WebSiteTagline").addClass("danger_error");
				return false;
			}

			if(host_name=='' || host_name==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter host name.</div>");
				$(".host_name").addClass("danger_error");
				return false;
			}

			if(db_name=='' || db_name==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter database name.</div>");
				$(".db_name").addClass("danger_error");
				return false;
			}

			if(db_user_name=='' || db_user_name==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter user name.</div>");
				$(".db_user_name").addClass("danger_error");
				return false;
			}

			if(db_password=='' || db_password==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter database password.</div>");
				$(".db_password").addClass("danger_error");
				return false;
			}

			if(admin_user_name=='' || admin_user_name==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter Admin Username.</div>");
				$(".admin_user_name").addClass("danger_error");
				return false;
			}

			if(admin_email=='' || admin_email==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter Admin email id.</div>");
				$(".admin_email").addClass("danger_error");
				return false;
			}

			function validateEmail($admin_email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $admin_email );
            }

            if( !validateEmail(admin_email)) 
            { 
            	 $('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Invalid!</strong> Email id is not valid.</div>");
				$(".admin_email").addClass("danger_error");
                return false;
            }

			if(admin_password=='' || admin_password==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter admin password.</div>");
				$(".admin_password").addClass("danger_error");
				return false;
			}

			if(admin_confirm_password=='' || admin_confirm_password==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Confirm password.</div>");
				$(".admin_confirm_password").addClass("danger_error");
				return false;
			}

			if(admin_password != admin_confirm_password==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> Admin password & confirm password not same.</div>");
				$(".admin_password").addClass("danger_error");
				$(".admin_confirm_password").addClass("danger_error");
				return false;
			}

			$('.loading_img').show();
			
			var data = 'base_path='+base_path+'&host_name='+host_name+'&db_name='+db_name+'&db_user_name='+db_user_name+'&db_password='+db_password+'&WebSiteTitle='+WebSiteTitle+'&WebSiteTagline='+WebSiteTagline+'&admin_user_name='+admin_user_name+'&admin_email='+admin_email+'&admin_password='+admin_password+'&admin_confirm_password='+admin_confirm_password+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/install_helper.php',
            	success:function(res)
            	{
            		$('.loading_img').hide();
            		if(res=='success')
            		{
            			$('.form_status').html("<div class='alert alert-success success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong> <span class='fa fa-check'></span> Installation Successfull.</strong> </div><br/><br/> Login to admin dashboard <a href='login.php'>Click Here</a>");
            			$('.btn_chk_database').attr('disabled',true);
            			$('.btn_chk_database').hide();
            			$('.btn_install').attr('disabled',true);
            			$('.btn_install').hide();
            			
            		}
            		else
            		{
            			$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}
            	}
            });
		});
	});
</script>