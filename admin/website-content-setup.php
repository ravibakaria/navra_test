<?php
	include("templates/header.php");
	if(isset($_GET['main_tab']) && isset($_GET['sub_tab']))
	{
		$main_tab=quote_smart($_GET['main_tab']);
		$sub_tab=quote_smart($_GET['sub_tab']);
	}
	else
	{
		$main_tab=null;
		$sub_tab=null;
	}
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
    
</head>
<body>
	<section role="main" class="content-body">
		
		<!-- start: page -->
			<div class="row admin_start_section">
				<h1>Website Content</h1>

				<hr class="setting-devider"/>
				<ul class="nav data-tabs nav-tabs" role="tablist">
				  	<li class="<?php if(($main_tab=='WebsiteContentAttributes' || $main_tab==null) && ($sub_tab=='Home_Page' || $sub_tab==null)) { echo 'active';}?>"><a href="#Home_Page" role="tab" data-toggle="tab" class="tab-links">Home Page </a></li>

				  	<li class="<?php if($main_tab=='WebsiteContentAttributes' && $sub_tab=='Color_Setting') { echo 'active';}?>"><a href="#Color_Setting" role="tab" data-toggle="tab" class="tab-links">Color </a></li>

				  	<li class="<?php if($main_tab=='WebsiteContentAttributes' && $sub_tab=='Theme_Setting') { echo 'active';}?>"><a href="#Theme_Setting" role="tab" data-toggle="tab" class="tab-links">CSS </a></li>

				  	<li class="<?php if($main_tab=='WebsiteContentAttributes' && $sub_tab=='About_Us') { echo 'active';}?>"><a href="#About_Us" role="tab" data-toggle="tab" class="tab-links">About Us </a></li>

				  	<li class="<?php if($main_tab=='WebsiteContentAttributes' && $sub_tab=='Contact_Us') { echo 'active';}?>"><a href="#Contact_Us" role="tab" data-toggle="tab" class="tab-links">Contact Us </a></li>

				  	<li class="<?php if($main_tab=='WebsiteContentAttributes' && $sub_tab=='Privacy_Policy') { echo 'active';}?>"><a href="#Privacy_Policy" role="tab" data-toggle="tab" class="tab-links">Privacy Policy </a></li>

				  	<li class="<?php if($main_tab=='WebsiteContentAttributes' && $sub_tab=='Terms_Of_Service') { echo 'active';}?>"><a href="#Terms_Of_Service" role="tab" data-toggle="tab" class="tab-links">Terms Of Service </a></li>

				  	<li class="<?php if($main_tab=='WebsiteContentAttributes' && $sub_tab=='Disclaimer') { echo 'active';}?>"><a href="#Disclaimer" role="tab" data-toggle="tab" class="tab-links">Disclaimer </a></li>

				  	<li class="<?php if($main_tab=='WebsiteContentAttributes' && $sub_tab=='FAQ') { echo 'active';}?>"><a href="#FAQ" role="tab" data-toggle="tab" class="tab-links">FAQ </a></li>

				  	<li class="<?php if($main_tab=='WebsiteContentAttributes' && $sub_tab=='Ads_mgmt') { echo 'active';}?>"><a href="#Ads_mgmt" role="tab" data-toggle="tab" class="tab-links">Ads </a></li>
			  	</ul>

				<div class="tab-content">
					<!-- Home Page Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='WebsiteContentAttributes' || $main_tab==null) && ($sub_tab=='Home_Page' || $sub_tab==null)) { echo 'active';}?>" id="Home_Page">
			  			<h2>Home Page</h2><br/>
			  			<?php include("modules/website-content-setting/Home_Page_Setup_data.php");?>
			  		</div>
			  		<!-- Home Page End-->

			  		<!-- Color_Setting Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='WebsiteContentAttributes') && ($sub_tab=='Color_Setting')) { echo 'active';}?>" id="Color_Setting">
			  			<h2>Color Settings</h2><br/>
			  			<?php include("modules/website-content-setting/Color_Setting_Setup_data.php");?>
			  		</div>
			  		<!-- Theme_Setting Setup End-->

			  		<!-- Theme_Setting Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='WebsiteContentAttributes') && ($sub_tab=='Theme_Setting')) { echo 'active';}?>" id="Theme_Setting">
			  			<h2>CSS Settings</h2><br/>
			  			<?php include("modules/website-content-setting/Theme_Setting_Setup_data.php");?>
			  		</div>
			  		<!-- Theme_Setting Setup End-->

			  		<!-- About_Us Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='WebsiteContentAttributes') && ($sub_tab=='About_Us')) { echo 'active';}?>" id="About_Us">
			  			<h2>About Us</h2><br/>
			  			<?php include("modules/website-content-setting/About_Us_Setup_data.php");?>
			  		</div>
			  		<!-- About_Us Setup End-->

			  		<!-- Contact_Us Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='WebsiteContentAttributes') && ($sub_tab=='Contact_Us')) { echo 'active';}?>" id="Contact_Us">
			  			<h2>Contact Us</h2><br/>
			  			<p align="right" class="all-mandatory-text">All fields are mandatory</p><br/>
			  			<?php include("modules/website-content-setting/Contact_Us_Setup_data.php");?>
			  		</div>
			  		<!-- Contact_Us Setup End-->

			  		<!-- Privacy Policy Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='WebsiteContentAttributes') && ($sub_tab=='Privacy_Policy')) { echo 'active';}?>" id="Privacy_Policy">
			  			<h2>Privacy Policy</h2><br/>
			  			<?php include("modules/website-content-setting/Privacy_Policy_Setup_data.php");?>
			  		</div>
			  		<!-- Privacy Policy Setup End-->

			  		<!-- Terms Of Service Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='WebsiteContentAttributes') && ($sub_tab=='Terms_Of_Service')) { echo 'active';}?>" id="Terms_Of_Service">
			  			<h2>Terms Of Services</h2><br/>
			  			<?php include("modules/website-content-setting/Terms_Of_Service_Setup_data.php");?>
			  		</div>
			  		<!-- Terms Of Service Setup End-->

			  		<!-- Disclaimer Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='WebsiteContentAttributes') && ($sub_tab=='Disclaimer')) { echo 'active';}?>" id="Disclaimer">
			  			<h2>Disclaimer</h2><br/>
			  			<?php include("modules/website-content-setting/Disclaimer_Setup_data.php");?>
			  		</div>
			  		<!-- Disclaimer Setup End-->

			  		<!-- FAQ Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='WebsiteContentAttributes') && ($sub_tab=='FAQ')) { echo 'active';}?>" id="FAQ">
			  			<?php include("modules/website-content-setting/FAQ_Setup_data.php");?>
			  		</div>
			  		<!-- FAQ Setup End-->

			  		<!-- Ads Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='WebsiteContentAttributes') && ($sub_tab=='Ads_mgmt')) { echo 'active';}?>" id="Ads_mgmt">
			  			<h2>Ads Management</h2><br/>
			  			<?php include("modules/website-content-setting/Ads_mgmt_Setup_data.php");?>
			  		</div>
			  		<!-- Ads_mgmt Setup End-->

			  	</div>
			</div>
		<!-- end: page -->
	</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>

