<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body update-section">
		<a href="master-data-setup.php?main_tab=LocationAttributes&sub_tab=City" id="portletReset" type="button" class="mb-xs mt-xs mr-xs btn btn-default" style="float:right;"><i class="fa fa-arrow-left"></i> Back</a>

		<?php
			$id = quote_smart($_GET['id']);

			if ( !is_numeric( $id ) )
		    {
		        echo 'Error! Invalid Data';
		        exit;
		    }

		    $sql = "SELECT A.*,B.country_id FROM `cities` AS A JOIN `states` AS B ON A.state_id=B.id WHERE A.id='$id'";
		    $stmt1   = $link->prepare($sql);
            $stmt1->execute();
            $queryTot = $stmt1->rowCount();
            $result = $stmt1->fetch();

           	if($queryTot>0)
           	{
           		$country_id = $result['country_id'];
           		$state_id = $result['state_id'];
           		$name = $result['name'];
           		$status = $result['status'];
		?>
		<br/><br/>
		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-md-3 col-lg-3">
					</div>
					<div class="col-md-6 col-lg-6 edit-master-div">
						<div class="row"><br>
							<center><h4>Update <strong><?php echo $name;?></strong> City Information</h4></center>
						</div>
							<hr/>
						<div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <lable class="control-label">Country :</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                <select class="form control City_country_id country-select-option">
                                    <option value="0">--Select Country--</option>
                                    <?php
                                        $sql="SELECT * FROM `countries` WHERE `status`='1'";
                                        $stmt = $link->prepare($sql);
                                        $stmt->execute();
                                        $resullt = $stmt->fetchAll();

                                        foreach ($resullt as $row) 
                                        {
                                            $country_id_c = $row['id'];
                                            $country_name_c = $row['name'];

                                            if($country_id == $country_id_c)
                                            {
                                            	echo "<option value='".$country_id_c."' selected>".$country_name_c."</option>";
                                            }
                                            else
                                            {
                                            	echo "<option value='".$country_id_c."'>".$country_name_c."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <lable class="control-label">State :</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                <select class="form control City_state_id country-select-option">
                                    <option value="0">--Select State--</option>
                                    <?php
                                    	$sql = "SELECT * FROM `states` WHERE `country_id`='$country_id' AND `status`='1'";
								    	$stmt = $link->prepare($sql);
								    	$stmt->execute();
								    	$result = $stmt->fetchAll();

								    	foreach ($result as $row) 
								    	{
								    		$state_id_c = $row['id'];
    										$state_name_c = $row['name'];

    										if($state_id == $state_id_c)
    										{
    											echo "<option value='".$state_id_c."' selected>".$state_name_c."</option>";
    										}
    										else
    										{
    											echo "<option value='".$state_id_c."'>".$state_name_c."</option>";
    										}
								    	}
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">City Name:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm City_name" value="<?php echo $name;?>"/>
                                </div>
                            </div>
						<br/>
						<input type="hidden" class="State_id" value="<?php echo $id;?>">

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 update_status">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
							</div>	
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><button class="btn btn-success btn_update_State website-button">Update</button></center><br/>	
							</div>
						</div>
						
					</div>
					<div class="col-md-3 col-lg-3">
					</div>
				</div>
				<div class="row">
					<br/>
				</div>
			</div>
			
		</div>
		<?php
			}
			else
			{
				echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
			}
		?>
</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		
		//fetch all states of selected country
        $('.City_country_id').change(function(){
            var country_id = $(this).val();
            var task = "Fetch_States_Of_Country";

            var data = 'country_id='+country_id+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/location-attributes/City-helper.php',
                success:function(res)
                {
                    $('.City_state_id').html(res);
                    return false;
                }
            });
        });


		$('.btn_update_State').click(function(){
			var country_id = $('.City_country_id').val();
            var state_id = $('.City_state_id').val();
            var name = $('.City_name').val();
			var id = $('.State_id').val();
			var task = "Update_State_Attributes";

            if(country_id=='0')
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select country to fetch states of the country.</div></center>");
                return false;
            }

            if(state_id=='0')
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select state to add City.</div></center>");
                return false;
            }

            if(name=='' || name==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter City name.</div></center>");
                return false;
            }

			var data = 'state_id='+state_id+'&name='+name+'&id='+id+'&task='+task;
			$('.loading_img').show();
			
			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/master-data/location-attributes/City-helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data updated successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});
	});
</script>