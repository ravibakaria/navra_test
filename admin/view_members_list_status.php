<?php
	include("templates/header.php");
	$status = quote_smart($_GET['status']);

	if(!is_numeric($status))
	{
		echo "<script>alert('ERROR: Invalid Parameters. Please provide valid parameters.');</script>";
		exit;
	}

	if($status=='0')
	{
		$display_status = "In Active";
	}
	else
	if($status=='1')
	{
		$display_status = "Active";
	}
	else
	if($status=='2')
	{
		$display_status = "Deactivate";
	}
	else
	if($status=='3')
	{
		$display_status = "Suspend";
	}
	
	$_SESSION['view_member_status']=$status;

?>
<title>
	View Members -  <?php echo getWebsiteTitle(); ?>
</title>
	<section role="main" class="content-body">
		<header class="page-header">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><a href="index.php"><span>Dashboard</span></a></li>
				<li><span>View Members</span></li>
			</ol>
			
			
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li><span><a href="index.php"><span class="fa fa-arrow-left">&nbsp;Back</span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></li>
				</ol>
			</div>
		</header>

		<div class="row admin-start-section">
			<div class="col-md-12 col-lg-12 col-xl-12">
				<h2><?php echo $display_status.' Members ';?> </h2>
			</div>

			<input type="hidden" class="status" value="<?php echo $status;?>" data-column="4">
			<div class="col-md-12 col-lg-12 col-xl-12">
				<table id='datatable-info' class="table-hover table-striped table-bordered datatable view_member_list_status">
					<thead>
						<tr>
							<th>Id</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Profile Id</th>
							<th>Gender</th>
							<th>Email</th>
							<th>Mobile</th>
							<th>Marital Status</th>
							<th>Status</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</section>
</div>
</section>

<?php
	include("templates/footer.php");
?>

<script>
$(document).ready(function(){
	var dataTable = $('.view_member_list_status').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"datatables/member-attributes/view_members_status_response.php", // json datasource
            type: "post",  // type of method  ,GET/POST/DELETE
            error: function(){
                $(".view_member_list_status_processing").css("display","none");
            }
        }
    });
});
</script>