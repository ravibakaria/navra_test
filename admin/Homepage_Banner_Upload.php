<?php
	session_start();
	require_once '../config/config.php'; 
	include('../config/dbconnect.php');    //database connection
	include('../config/functions.php');   //strip query string

	if($_FILES['file']['name'] != '')
	{
		ini_set("post_max_size", "2M");
	    ini_set("upload_max_filesize", "2M");
	    ini_set("memory_limit", "2M");

	    $target_dir = '../images/home-page-banner/';

	    if (!file_exists($target_dir)) 
		{
			try 
			{
				mkdir($target_dir, 0777, true);
				//chmod('/'.$application_id,0777);
				//chmod('upload/'.$application_id.'/',0777);
			} 
			catch (Exception $ex) 
			{
			die("error");
			}
		}

		$file_name = $_FILES["file"]["name"];

		$file_type = $_FILES["file"]["type"];
		$file_size = $_FILES["file"]["size"];
		$file_tmp  = $_FILES['file']['tmp_name'];

		$file_ext  = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		$extensions= array('jpg','jpeg','png');

		if(in_array($file_ext,$extensions)=== false)
		{
			echo "Please select 'jpg/jpeg/png' file";
			exit;
		}

		if($file_size > 2097152 || $file_size==0)
	    {
	    	echo "Image size must be less than 2 MB";
			exit;
	    }

	    $sql_chk = "SELECT homepagebanner FROM homepagesetup";
	    $sql_chk= $link->prepare($sql_chk);
	    $sql_chk->execute();
        $result_chk = $sql_chk->fetch();
        $count=$sql_chk->rowCount();
        if($count>0)
        {
        	$pre_homepage_banner = $result_chk['homepagebanner'];

        	if($pre_homepage_banner!='' || $pre_homepage_banner!=null)
        	{
        		unlink($pre_homepage_banner);
        	}
        }

	    if(move_uploaded_file($file_tmp,$target_dir.$file_name))
	    {
	    	$db_file_name = $target_dir.$file_name;
	    	if($count>0)
	    	{
	    		$sql_update = "UPDATE homepagesetup SET homepagebanner='$db_file_name'";
	    	}
	    	else
	    	{
	    		$sql_update = "INSERT INTO homepagesetup(homepagebanner) VALUES('$db_file_name')";
	    	}
	    		
	    	if($link->exec($sql_update))
	        {
	        	echo "success";
	        	exit;
	        }
	        else
	        {
	        	echo "Something went wrong try after some time";
	        	exit;
	        }
	    }
	}
?>