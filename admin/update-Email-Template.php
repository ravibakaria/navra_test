<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body update-section">
		<a href="email-template-setup.php" id="portletReset" type="button" class="mb-xs mt-xs mr-xs btn btn-default" style="float:right;"><i class="fa fa-arrow-left"></i> Back</a>

		<?php
			$id = quote_smart($_GET['id']);

			if ( !is_numeric( $id ) )
		    {
		        echo 'Error! Invalid Data';
		        exit;
		    }

		    $sql = "SELECT * FROM `emailtemplates` WHERE `id`='$id'";
		    $stmt1   = $link->prepare($sql);
            $stmt1->execute();
            $queryTot = $stmt1->rowCount();
            $result = $stmt1->fetch();

           	if($queryTot>0)
           	{
           		$id = $result['id'];
           		$emailTemplateName = $result['emailTemplateName'];
           		$emailTemplateSubject = $result['emailTemplateSubject'];
           		$emailTemplateMessage = $result['emailTemplateMessage'];
		?>

		<br/><br/>
		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 edit-master-div">
						<div class="row"><br>
							<center><h4>Update Email Template</h4></center>
						</div>
						<hr/>

						<div class="row input-row">
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <lable class="control-label">Template Name:</lable>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            	<input type="text" class="form-control form-control-sm emailTemplateName" value="<?php echo $emailTemplateName;?>">
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <lable class="control-label">Subject:</lable>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                <input type="text" class="form-control form-control-sm emailTemplateSubject" value="<?php echo $emailTemplateSubject;?>" />
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <lable class="control-label">Message:</lable>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                <textarea rows="3" class="emailTemplateMessage" id="emailTemplateMessage" name="emailTemplateMessage"><?php echo $emailTemplateMessage;?></textarea>
                            </div>
                        </div>

                        <input type="hidden" class="Template_id" value="<?php echo $id;?>">

                        <div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 update_status">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
							</div>	
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><button class="btn btn-success btn_update_FAQ website-button">Update Information</button></center><br/>	
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 edit-master-div" style="color:black;">
						<center><h4>Variables Used for template. {valiable_name}: Description</h4></center>
						<?php
							$sql = "SELECT * FROM `emailtemplatevariables`";
							$stmt = $link->prepare($sql);
							$stmt->execute();
							$result = $stmt->fetchAll();
						
							$i=1;
							foreach ($result as $row) 
							{
								if($i=='1' || ($i-1)%3==0)
								{
									echo "<div class='row'>";
								}

								echo "<div class='col-md-4 col-lg-4'>";
									echo $row['name'];
								echo "</div>";
								$i = $i+1;

								if(($i-1)%3==0)
								{
									echo "</div>";
								}
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
			}
			else
			{
				echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
			}
		?>
	</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		$('.emailTemplateMessage').richText();

		$('.btn_update_FAQ').click(function(){
			var emailTemplateName = $('.emailTemplateName').val();
			var emailTemplateSubject = $('.emailTemplateSubject').val();
			var emailTemplateMessage = $('.emailTemplateMessage').val();
            var id = $('.Template_id').val();
			var task = "Update_Template_Data";

			if(emailTemplateName=='' || emailTemplateName==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter template name.</div></center>");
                return false;
            }

			if(emailTemplateSubject=='' || emailTemplateSubject==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter subject for template.</div></center>");
                return false;
            }

			if(emailTemplateMessage=='' || emailTemplateMessage==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter message for template.</div></center>");
                return false;
            }

			var data = {
				emailTemplateName : emailTemplateName,
				emailTemplateSubject : emailTemplateSubject,
				emailTemplateMessage : emailTemplateMessage,
				id : id,
				task : task
			};

			$('.loading_img').show();
			
			$.ajax({
				type:'post',
				dataType:'json',
	        	data:data,
	        	url:'query/email-template-data/Email-Template-Setup-helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data updated successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});
	});
</script>