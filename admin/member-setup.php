<?php
	include("templates/header.php");
	if(isset($_GET['main_tab']) && isset($_GET['sub_tab']))
	{
		$main_tab=quote_smart($_GET['main_tab']);
		$sub_tab=quote_smart($_GET['sub_tab']);
	}
	else
	{
		$main_tab=null;
		$sub_tab=null;
	}
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<link rel="stylesheet" href="../css/croppie.css" />
	
</head>
<body>
	<section role="main" class="content-body">
		
		<!-- start: page -->
			<div class="row admin_start_section">
				<h1>Member Management</h1>
				<hr class="setting-devider"/>
				<ul class="nav data-tabs nav-tabs" role="tablist">
				  	<li class="<?php if(($main_tab=='MemberAttributes' || $main_tab==null) && ($sub_tab=='Member_Setup' || $sub_tab==null)) { echo 'active';}?>"><a href="#Member_Setup" role="tab" data-toggle="tab" class="tab-links">Member Setup </a></li>
				  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='Report_Abuse')) { echo 'active';}?>"><a href="#Report_Abuse" role="tab" data-toggle="tab" class="tab-links">Report Abuse </a></li>
			  	</ul>

				<div class="tab-content">
					<!-- General Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='MemberAttributes' || $main_tab==null) && ($sub_tab=='Member_Setup' || $sub_tab==null)) { echo 'active';}?>" id="Member_Setup">
			  			<?php include("modules/member-setting/Member_Setup_data.php");?>
			  		</div>
			  		<!-- General Setup End-->

			  		<!-- Report Abuse Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='Report_Abuse')) { echo 'active';}?>" id="Report_Abuse">
			  			<?php include("modules/member-setting/Report_Abuse_data.php");?>
			  		</div>
			  		<!-- Report Abuse End-->


			  	</div>
			</div>
		<!-- end: page -->
	</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>