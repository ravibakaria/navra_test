<?php
	session_start();
	require_once '../config/config.php'; 
	include('../config/dbconnect.php');    //database connection
	include('../config/functions.php');   //strip query string

	if($_FILES['file']['name'] != '')
	{
		ini_set("post_max_size", "2M");
	    ini_set("upload_max_filesize", "2M");
	    ini_set("memory_limit", "2M");

	    $target_dir = '../images/loader/';

	    if (!file_exists($target_dir)) 
		{
			try 
			{
				mkdir($target_dir, 0777, true);
				//chmod('/'.$application_id,0777);
				//chmod('upload/'.$application_id.'/',0777);
			} 
			catch (Exception $ex) 
			{
			die("error");
			}
		}

		$file_name = "loader.gif";

		$file_type = $_FILES["file"]["type"];
		$file_size = $_FILES["file"]["size"];
		$file_tmp  = $_FILES['file']['tmp_name'];

		$file_ext  = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		$extensions= array('gif');

		if(in_array($file_ext,$extensions)=== false)
		{
			echo "Please select 'gif' file";
			exit;
		}

		if($file_size > 2097152 || $file_size==0)
	    {
	    	echo "Image size must be less than 2 MB";
			exit;
	    }

	    

	    if(move_uploaded_file($file_tmp,$target_dir.$file_name))
	    {
	    	echo "success";
	        exit;
	    }
	    else
	    {
	    	echo "Something went wrong try after some time";
	        exit;
	    }
	}
?>