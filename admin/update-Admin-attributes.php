<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body update-section">
		<a href="user-setup.php" id="portletReset" type="button" class="mb-xs mt-xs mr-xs btn btn-default" style="float:right;"><i class="fa fa-arrow-left"></i> Back</a>

		<?php
			$id = quote_smart($_GET['id']);

			if ( !is_numeric( $id ) )
		    {
		        echo 'Error! Invalid Data';
		        exit;
		    }

		    $sql = "SELECT * FROM `admins` WHERE `id`='$id'";
		    $stmt1   = $link->prepare($sql);
            $stmt1->execute();
            $queryTot = $stmt1->rowCount();
            $result = $stmt1->fetch();

           	if($queryTot>0)
           	{
           		$username = $result['username'];
           		$firstname = $result['firstname'];
           		$lastname = $result['lastname'];
           		$email = $result['email'];
           		$isSuperAdmin = $result['isSuperAdmin'];
           		$department = $result['department'];
           		$status = $result['status'];
		?>
		<br/><br/>
		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-md-3 col-lg-3">
					</div>
					<div class="col-md-6 col-lg-6 edit-master-div">
						<div class="row"><br>
							<center><h4>Update <strong><?php echo $firstname.' '.$lastname;?></strong> Admin Information</h4></center>
						</div>
						<hr/>

						<div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Admin User Name:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <input type="text" class="form-control form-control-sm Admin_username" value="<?php echo $username;?>" />
                            </div>
                        </div>
						<div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">First Name:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <input type="text" class="form-control form-control-sm Admin_firstname" value="<?php echo $firstname;?>" />
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Last Name:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <input type="text" class="form-control form-control-sm Admin_lastname" value="<?php echo $lastname;?>" />
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Email Id:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <input type="text" class="form-control form-control-sm Admin_email" value="<?php echo $email;?>"/>
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Department:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                            	<?php
                            		$department = explode(',',$department);

                            		$sql_dept = "SELECT * FROM `departments` WHERE `status`='1'";
                                    $stmt = $link->prepare($sql_dept);
                                    $stmt->execute();
                                    $resullt = $stmt->fetchAll();


                                    foreach ($resullt as $row) 
                                    {
                                        $dept_id = $row['id'];
                                        $dept_name = $row['name'];
                                        echo "<div class='col-xs-12 col-sm-12'>";
                                        if(in_array($dept_id, $department))
                                        {
                                        	echo "<input type='checkbox' name='Admin_departments[]' class='Admin_departments' value='$dept_id' checked/>&nbsp;&nbsp;".$dept_name."&nbsp;&nbsp;&nbsp;";
                                        }
                                        else
                                        {
                                        	echo "<input type='checkbox' name='Admin_departments[]' class='Admin_departments' value='$dept_id'/>&nbsp;&nbsp;".$dept_name."&nbsp;&nbsp;&nbsp;";
                                        }
                                        echo "</div>";
                                    }
                            	?>
                            </div>
                        </div>

                        <?php
                        	if($isSuperAdmin!='1')
                        	{
                        ?>
                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Status:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <?php
									if($status=='0')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Admin_current_status' name='Admin_current_status' value='".$status."'>";
									}
									else
									if($status=='1')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Admin_current_status' name='Admin_current_status' checked value='".$status."'>";
									}
								?>
                            </div>
                        </div>
                        <?php
                        	}
                        	else
                        	if($isSuperAdmin=='1')
                        	{
                        		echo "<div style='display:none;'>
                        			<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Admin_current_status' name='Admin_current_status' checked value='1'>
                        			</div>";
                        	}
                        ?>
						<input type="hidden" class="Admin_id" value="<?php echo $id;?>">

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 update_status">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
							</div>	
						</div>
						
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<center><button class="btn btn-success btn_update_Admin website-button">Update Information</button></center><br/>	
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<center><button class="btn btn-success btn_reset_password_Admin website-button">Reset & Email Password</button></center><br/>	
							</div>
						</div>
						
					</div>
					<div class="col-md-3 col-lg-3">
					</div>
				</div>
				<div class="row">
					<br/>
				</div>
			</div>
			
		</div>
		<?php
			}
			else
			{
				echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
			}
		?>
</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		$('#Admin_departments').multiselect({
            includeSelectAllOption: true,
            maxHeight: 400,
            dropUp: true
        });

		$('.btn_update_Admin').click(function(){
			var username = $('.Admin_username').val();
			var firstname = $('.Admin_firstname').val();
            var lastname = $('.Admin_lastname').val();
            var email = $('.Admin_email').val();
            var department = [];
            $('input.Admin_departments:checked').each(function() {
                department.push(this.value);
            });

            var status = $('.Admin_current_status').is(":checked");
            var id = $('.Admin_id').val();
			var task = "Update_Admin_Attributes";

			if(status==true)
			{
				status='1';
			}
			else
			if(status==false)
			{
				status='0';
			}

			if(username=='' || username==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter Admin user name.</div></center>");
                return false;
            }

			if(firstname=='' || firstname==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter Admin first name.</div></center>");
                return false;
            }

            if(lastname=='' || lastname==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter user last name.</div></center>");
                return false;
            }

            if(email=='' || email==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter user email id.</div></center>");
                return false;
            }

            function validateEmail($email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $email );
            }

            if( !validateEmail(email)) 
            { 
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Please Enter valid email.</div></center>");
                return false;
            }

            if(department==null || department=='')
            { 
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty! </strong> Select department.</div></center>");
                return false;
            }

			var data = 'username='+username+'&firstname='+firstname+'&lastname='+lastname+'&email='+email+'&department='+department+'&status='+status+'&id='+id+'&task='+task;
			
			$('.loading_img').show();
			
			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/user-attributes/Admin-helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data updated successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});

		$('.btn_reset_password_Admin').click(function(){
			var id = $('.Admin_id').val();
			var task = "Reset_Admin_Password";

			var data = 'id='+id+'&task='+task;
			
			$('.loading_img').show();
			
			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/user-attributes/Admin-helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Password sent successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});
	});
</script>