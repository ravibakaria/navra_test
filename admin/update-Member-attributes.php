<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body">
		<header class="page-header">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Update Member Information</span></li>
			</ol>
			
			<ol class="breadcrumbs" style="margin-left: 775px !important;">
				<li><span><a href="<?php echo $previous_page_url; ?>"><span class="fa fa-arrow-left"></a></span>&nbsp;<a href="<?php echo $previous_page_url; ?>" style="margin-top:2px;text-decoration:none;">&nbsp;Back</a>
				</li>
			</ol>
		</header>

		<?php
			$id = quote_smart($_GET['id']);

			if ( !is_numeric( $id ) )
		    {
		        echo 'Error! Invalid Data';
		        exit;
		    }

		    $sql = "SELECT * FROM `clients` WHERE `id`='$id'";
		    $stmt1   = $link->prepare($sql);
            $stmt1->execute();
            $queryTot = $stmt1->rowCount();
            $result = $stmt1->fetch();

           	if($queryTot>0)
           	{
           		$firstname = $result['firstname'];
           		$lastname = $result['lastname'];
           		$dob = $result['dob'];
           		$gender = $result['gender'];
           		$email = $result['email'];
           		$status = $result['status'];
		?>
		<br/><br/>
		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-md-3 col-lg-3">
					</div>
					<div class="col-md-6 col-lg-6 edit-master-div">
						<div class="row">
							<center><h4>Update <strong><?php echo $firstname.' '.$lastname;?></strong> Member Information</h4></center>
						</div>
							<hr/>
						<div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Member First Name:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <input type="text" class="form-control form-control-sm Member_firstname" value="<?php echo $firstname;?>" />
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Member Last Name:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <input type="text" class="form-control form-control-sm Member_lastname" value="<?php echo $lastname;?>" />
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Member Email Id:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <input type="text" class="form-control form-control-sm Member_email" value="<?php echo $email;?>"/>
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Member DOB:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <input type="text" class="form-control form-control-sm Member_dob" name="Member_dob" value="<?php echo $dob;?>"/>
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Member Gender:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
                                <select class="form-control Member_gender">
                                    <option value="0">--Select Gender--</option>
                                    <?php
                                        $sql_gender = "SELECT * FROM `gender` WHERE `status`='1'";
                                        $stmt = $link->prepare($sql_gender);
                                        $stmt->execute();
                                        $result = $stmt->fetchAll();

                                        foreach ($result as $row) 
                                        {
                                            $gender_id_c = $row['id'];
                                            $gender_name_c = $row['name'];

                                            if($gender == $gender_id_c)
                                            {
                                            	echo "<option value='".$gender_id_c."' selected>".$gender_name_c."</option>";
                                            }
                                            else
                                            echo "<option value='".$gender_id_c."'>".$gender_name_c."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <lable class="control-label">Status:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                <?php
									if($status=='0')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Member_current_status' name='Member_current_status' value='".$status."'>";
									}
									else
									if($status=='1')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='Member_current_status' name='Member_current_status' checked value='".$status."'>";
									}
								?>
                            </div>
                        </div>
						<input type="hidden" class="Member_id" value="<?php echo $id;?>">

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 update_status">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
							</div>	
						</div>
						
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<center><button class="btn btn-success btn_update_Member website-button">Update Profile</button></center><br/>	
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<center><button class="btn btn-success btn_reset_password_Member website-button">Reset & Email Password</button></center><br/>	
							</div>
						</div>
						
					</div>
					<div class="col-md-3 col-lg-3">
					</div>
				</div>
				<div class="row">
					<br/>
				</div>
			</div>
			
		</div>
		<?php
			}
			else
			{
				echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
			}
		?>
</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		/*  Datepicker setting  */
        var date_input=$('input[name="Member_dob"]');
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        
        date_input.datepicker({
          format: 'yyyy-mm-dd',
          container: container,
          todayHighlight: true,
          autoclose: true,
        })

		$('.btn_update_Member').click(function(){
			var firstname = $('.Member_firstname').val();
            var lastname = $('.Member_lastname').val();
            var email = $('.Member_email').val();
            var dob = $('.Member_dob').val();
            var gender = $('.Member_gender').val();
            var status = $('.Member_current_status').is(":checked");
            var id = $('.Member_id').val();
			var task = "Update_Member_Attributes";

			if(status==true)
			{
				status='1';
			}
			else
			if(status==false)
			{
				status='0';
			}

			

			if(firstname=='' || firstname==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter Member first name.</div></center>");
                return false;
            }

            if(lastname=='' || lastname==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter Member last name.</div></center>");
                return false;
            }

            if(email=='' || email==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter Member email id.</div></center>");
                return false;
            }

            function validateEmail($email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $email );
            }

            if( !validateEmail(email)) 
            { 
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Please Enter valid email.</div></center>");
                return false;
            }

            if(gender=='0')
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select Member gender.</div></center>");
                return false;
            }

			var data = 'firstname='+firstname+'&lastname='+lastname+'&email='+email+'&dob='+dob+'&gender='+gender+'&status='+status+'&id='+id+'&task='+task;
			
			$('.loading_img').show();
			
			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/member-attributes/Member-helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data updated successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});

		$('.btn_reset_password_Member').click(function(){
			var id = $('.Member_id').val();
			var task = "Reset_Member_Password";

			var data = 'id='+id+'&task='+task;
			
			$('.loading_img').show();
			
			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/member-attributes/Member-helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Password updated & email sent successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});
	});
</script>