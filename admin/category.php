<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>
	<section role="main" class="content-body">
		
		<!-- start: page -->
			<div class="row admin_start_section">
				<div class="row">
				    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				        <div class="row div-header-row">
				            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				                <h1>Category Setting</h1>
				            </div>
				            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
				                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Category">Add New Category</button>
				            </div>
				        </div>

				        <div class="row">
				            <div style="">
				                <center>
				                    <table id='datatable-info' class='table-hover table-striped table-bordered category_list' style="width:100%;">
				                        <thead>
				                            <tr>
				                                <th>ID</th>
				                                <th>Category Name</th>
				                                <th>Slug</th>
				                                <th>Status</th>
				                                <th>Edit</th>
				                            </tr>
				                        </thead>
				                    </table>
				                </center>
				            </div>

				            <!-- Modal -->
				            <div class="modal fade admin_add_model" id="Add-New-Category" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
				                    <div class="modal-content">
				                        <div class="modal-header">
				                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Category</h4></center>
				                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                            <span aria-hidden="true">&times;</span>
				                            </button>
				                        </div>
				                        <div class="modal-body">
				                            <div class="row input-row">
				                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                                    <lable class="control-label">Category Name:</lable>
				                                </div>
				                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
				                                    <input type="text" class="form-control cat_name" />
				                                </div>
				                            </div>

				                            <div class="row input-row">
				                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                                    <lable class="control-label">Slug:</lable>
				                                </div>
				                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
				                                    <input type="text" class="form-control cat_slug" />
				                                </div>

				                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info note_section" style="margin-top:15px;">
				                                	<strong>Note:</strong> Used for SEO & paging.<br> Please use small character letters & hyphen sign(minus sign(-) between two keywords.) in slug<br>
				                                	For ex: if category name is "Abc Xyz" the slug will be "abc-xyz"
				                                </div>                                
				                            </div>

				                            <div class="row input-row">
				                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				                                    <lable class="control-label">Status:</lable>
				                                </div>
				                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
				                                    <input type='radio' name='cat_status' value='1' class='radio_1' id='radio_1'>  <label for='radio_1' class='control-label'>&nbsp;Enabled </label>
				                                    &nbsp;&nbsp;&nbsp;&nbsp;
				                                    <input type='radio' name='cat_status' value='0' class='radio_1' id='radio_0'>  <label for='radio_0' class='control-label'>&nbsp;Disabled </label>
				                                </div>
				                            </div>

				                            <div class="row input-row">
				                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status">
				                                </div>
				                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
				                                </div>
				                            </div>
				                        </div>
				                        <div class="modal-footer">
				                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				                            <button type="button" class="btn btn-primary btn_add_new_category website-button">Submit</button>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
			</div>
		<!-- end: page -->
	</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){

		var dataTable_category_list = $('.category_list').DataTable({          //complexion datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            "ajax":{
                url :"datatables/wedding-directory/category-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".category_list_processing").css("display","none");
                }
            }
        });

		$('.btn_add_new_category').click(function(){
			var name = $('.cat_name').val();
			var slug = $('.cat_slug').val();
			var status = $("input[name='cat_status']:checked"). val();
			var task = "add-new-category";

			if(name=='' || name==null)
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please enter category name</strong>.</div>");
				return false;
			}

			if(slug=='' || slug==null)
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please enter slug</strong>.</div>");
				return false;
			}

			if(status=='' || status==null || status=='undefined')
			{
				$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Please select status.</strong>.</div>");
				return false;
			}

			var data = {
				name : name,
				slug : slug,
				status : status,
				task : task
			};

			$('.loading_img').show();
			$('.add_status').html();

			$.ajax({
				type:'post',
				dataType:'json',
				data:data,
				url:'query/wedding-category/category-helper.php',
				success:function(res) 
				{
					$('.loading_img').hide();
					if(res=='success')
					{
						$('.add_status').html("<div class='alert alert-success add_status_success' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-check'></span><strong> Success!</strong> New category added successfully.</strong>.</div>");
						$('.add_status_success').fadeTo(1000, 500).slideUp(500, function(){
                              location.reload();
                        });
					}
					else
					{
						$('.add_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Error!</strong> "+res+"</strong>.</div>");
						return false;
					}
				}
			});
		});

		//Complexion change status
        $('.category_list tbody').on('click', '.change_status_category', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_Category_status";

            var data = {
            	status : status,
            	id : id,
            	task : task
            };

            $.ajax({
                type:'post',
                dataType:'json',
                data:data,
                url:'query/wedding-category/category-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.category_status'+id).html("<div class='alert alert-success category_status_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.category_status_status').fadeTo(1000, 500).slideUp(500, function(){
                                  location.reload();
                            });
                    }
                    else
                    {
                        $('.category_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });
	});
</script>