<?php
	include("templates/header.php");
	
	if(isset($_GET['main_tab']) && isset($_GET['sub_tab']))
	{
		$main_tab=quote_smart($_GET['main_tab']);
		$sub_tab=quote_smart($_GET['sub_tab']);
	}
	else
	{
		$main_tab=null;
		$sub_tab=null;
	}
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body">

	<?php
		$DefaultCurrency = getDefaultCurrency();
		$DefaultCurrencyCode = getDefaultCurrencyCode($DefaultCurrency);
		if($DefaultCurrencyCode=='' || $DefaultCurrencyCode==null)
		{
			$DefaultCurrencyCode='INR';
		}

		$stmt   = $link->prepare("SELECT * FROM `membership_plan`");
		$stmt->execute();
		$result = $stmt->fetchAll();
		$count=$stmt->rowCount();
	?>
		<!-- start: page -->
			<div class="row admin_start_section">
				<h1>Memberhip Plans</h1>
				<hr class="setting-devider"/>
				<ul class="nav data-tabs nav-tabs" role="tablist">
				  	<li class="<?php if(($main_tab=='MembershipPlans' || $main_tab==null) && ($sub_tab=='MembershipPlans_Setup' || $sub_tab==null)) { echo 'active';}?>"><a href="#MembershipPlans_Setup" role="tab" data-toggle="tab" class="tab-links">Membership Plans </a></li>

				  	<li class="<?php if(($main_tab=='MembershipPlans') && ($sub_tab=='FeaturedListing_Setup')) { echo 'active';}?>"><a href="#FeaturedListing_Setup" role="tab" data-toggle="tab" class="tab-links">Featured Listing </a></li>

				  	<li class="<?php if(($main_tab=='MembershipPlans') && ($sub_tab=='Taxation_Setup')) { echo 'active';}?>"><a href="#Taxation_Setup" role="tab" data-toggle="tab" class="tab-links">Taxation </a></li>
			  	</ul>

				<div class="tab-content">
					<!-- General Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='MembershipPlans' || $main_tab==null) && ($sub_tab=='MembershipPlans_Setup' || $sub_tab==null)) { echo 'active';}?>" id="MembershipPlans_Setup">
			  			<?php include("modules/membership-plans/membership-plans.php");?>
			  		</div>
			  		<!-- General Setup End-->

			  		<!-- Report Abuse Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='MembershipPlans') && ($sub_tab=='FeaturedListing_Setup')) { echo 'active';}?>" id="FeaturedListing_Setup">
			  			<?php include("modules/membership-plans/featured-listing.php");?>
			  		</div>
			  		<!-- Report Abuse End-->

			  		<!-- Report Abuse Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='MembershipPlans') && ($sub_tab=='Taxation_Setup')) { echo 'active';}?>" id="Taxation_Setup">
			  			<?php include("modules/membership-plans/taxation-setup.php");?>
			  		</div>
			  		<!-- Report Abuse End-->

			  	</div>
			</div>
		<!-- end: page -->
	
	</section>
</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>