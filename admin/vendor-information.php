<?php
	include("templates/header.php");
	if(isset($_GET['main_tab']) && isset($_GET['sub_tab']))
	{
		$main_tab=quote_smart($_GET['main_tab']);
		$sub_tab=quote_smart($_GET['sub_tab']);
	}
	else
	{
		$main_tab=null;
		$sub_tab=null;
	}

	$vuserid = quote_smart($_GET['id']);
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body">
		
		<!-- start: page -->
			<div class="row admin_start_section">
				<h1>Vendor Management</h1>
				<hr class="setting-devider"/>
				<ul class="nav data-tabs nav-tabs" role="tablist">
				  	<li class="<?php if(($main_tab=='VendorAttributes' || $main_tab==null) && ($sub_tab=='Vendor_Setup' || $sub_tab==null)) { echo 'active';}?>"><a href="#Vendor_Setup" role="tab" data-toggle="tab" class="tab-links">Vendor Information </a></li>
				  	<li class="<?php if(($main_tab=='VendorAttributes') && ($sub_tab=='Vendor_Photos')) { echo 'active';}?>"><a href="#Vendor_Photos" role="tab" data-toggle="tab" class="tab-links">Photos </a></li>
			  	</ul>

				<div class="tab-content">
					<!-- General Setup Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='VendorAttributes' || $main_tab==null) && ($sub_tab=='Vendor_Setup' || $sub_tab==null)) { echo 'active';}?>" id="Vendor_Setup">
			  			<?php include("modules/Vendor-setting/Vendor_Setup_data.php");?>
			  		</div>
			  		<!-- General Setup End-->

			  		<!-- Report Abuse Start-->
			  		<div class="tab-pane  <?php if(($main_tab=='VendorAttributes') && ($sub_tab=='Vendor_Photos')) { echo 'active';}?>" id="Vendor_Photos">
			  			<?php include("modules/Vendor-setting/Vendor_Photos_data.php");?>
			  		</div>
			  		<!-- Report Abuse End-->


			  	</div>
			</div>
		<!-- end: page -->
	</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>