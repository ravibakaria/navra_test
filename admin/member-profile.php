<?php
	include("templates/header.php");

	$user_id = $_GET['id'];

	if(isset($_GET['main_tab']) && isset($_GET['sub_tab']))
	{
		$main_tab=quote_smart($_GET['main_tab']);
		$sub_tab=quote_smart($_GET['sub_tab']);
	}
	else
	{
		$main_tab=null;
		$sub_tab=null;
	}
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<style>
		body {
			font-size: 13px;
		}
	</style>
</head>
<body>
	<section role="main" class="content-body update-section">
		<a href="member-setup.php" id="portletReset" type="button" class="mb-xs mt-xs mr-xs btn btn-default" style="float:right;"><i class="fa fa-arrow-left"></i> Back</a>

		<input type="hidden" class="$user_id" value="<?php echo $user_id;?>">

		<div class="row admin_start_section">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<?php
					$member_profile_name = getUserFirstName($user_id)." ".getUserLastName($user_id);
					$member_profile_id = getUserUniqueCode($user_id);

					echo "<h1>Profile of $member_profile_name<br> Profile ID: $member_profile_id</h1>";
				?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<center><img src="../images/loader/loader.gif" class='margin-top img-responsive loading_img_password_reset' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                <div class="margin-top password_update_status"></div>
			</div>
		</div>
		<div class="row margin-up">	
			<hr class="setting-devider"/>

			<ul class="nav data-tabs nav-tabs" role="tablist">
			  	<li class="<?php if(($main_tab=='MemberAttributes' || $main_tab==null) && ($sub_tab=='MemberInformation' || $sub_tab==null)) { echo 'active';}?>"><a href="#MemberInformation" role="tab" data-toggle="tab" class="tab-links personal_attribute_tab">Information</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberPhotos')) { echo 'active';}?>"><a href="#MemberPhotos" role="tab" data-toggle="tab" class="tab-links">Photos</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberDocuments')) { echo 'active';}?>"><a href="#MemberDocuments" role="tab" data-toggle="tab" class="tab-links">Documents</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberChatHistory')) { echo 'active';}?>"><a href="#MemberChatHistory" role="tab" data-toggle="tab" class="tab-links">Messages</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberInterests')) { echo 'active';}?>"><a href="#MemberInterests" role="tab" data-toggle="tab" class="tab-links">Interests</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberShortlistings')) { echo 'active';}?>"><a href="#MemberShortlistings" role="tab" data-toggle="tab" class="tab-links">Shortlist</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberBlocked')) { echo 'active';}?>"><a href="#MemberBlocked" role="tab" data-toggle="tab" class="tab-links">Block</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberLikedProfiles')) { echo 'active';}?>"><a href="#MemberLikedProfiles" role="tab" data-toggle="tab" class="tab-links">Likes</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberReportAbuse')) { echo 'active';}?>"><a href="#MemberReportAbuse" role="tab" data-toggle="tab" class="tab-links">Report Abuse</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberActivityLogs')) { echo 'active';}?>"><a href="#MemberActivityLogs" role="tab" data-toggle="tab" class="tab-links">Logs</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberEmailLogs')) { echo 'active';}?>"><a href="#MemberEmailLogs" role="tab" data-toggle="tab" class="tab-links">Emails</a></li>

			  	<li class="<?php if(($main_tab=='MemberAttributes') && ($sub_tab=='OrderSummary')) { echo 'active';}?>"><a href="#OrderSummary" role="tab" data-toggle="tab" class="tab-links">Orders</a></li>

			</ul>

			<div class="tab-content">
				<!-- MemberInformation Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes' || $main_tab==null) && ($sub_tab=='MemberInformation' || $sub_tab==null)) { echo 'active';}?>" id="MemberInformation">
		  			<h2>Member Detail</h2>
		  			<?php include("modules/member-setting/member_information_setup_data.php");?>
		  		</div>
		  		<!-- MemberInformation Setup End-->

		  		<!-- MemberPhotos Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberPhotos')) { echo 'active';}?>" id="MemberPhotos">
		  			<h2>Member Photos</h2>
		  			<?php include("modules/member-setting/member_photos_data.php");?>
		  		</div>
		  		<!-- ReligiousAttributes Setup End-->

		  		<!-- MemberDocuments Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberDocuments')) { echo 'active';}?>" id="MemberDocuments">
		  			<h2>Member Documents</h2>
  					<?php include("modules/member-setting/member_document_data.php");?>
		  			
		  		</div>
		  		<!-- MemberDocuments Setup End-->

		  		<!-- MemberChatHistory Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberChatHistory')) { echo 'active';}?>" id="MemberChatHistory">
		  			<h2>Member Chat Messages</h2>
  					<?php include("modules/member-setting/member_chat_data.php");?>
		  			
		  		</div>
		  		<!-- MemberChatHistory Setup End-->

		  		<!-- MemberInterests Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberInterests')) { echo 'active';}?>" id="MemberInterests">
		  			<h2>Member Profile Interests</h2>
  					<?php include("modules/member-setting/member_interests.php");?>
		  			
		  		</div>
		  		<!-- MemberInterests Setup End-->

		  		<!-- MemberShortlistings Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberShortlistings')) { echo 'active';}?>" id="MemberShortlistings">
		  			<h2>Member Profile Shortlist</h2>
  					<?php include("modules/member-setting/member_shortlistings.php");?>
		  			
		  		</div>
		  		<!-- MemberShortlistings Setup End-->

		  		<!-- MemberBlocked Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberBlocked')) { echo 'active';}?>" id="MemberBlocked">
		  			<h2>Member Profile Block</h2>
  					<?php include("modules/member-setting/member_blocked.php");?>
		  			
		  		</div>
		  		<!-- MemberBlocked Setup End-->

		  		<!-- MemberLikedProfiles Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberLikedProfiles')) { echo 'active';}?>" id="MemberLikedProfiles">
		  			<h2>Member Profile Likes</h2>
  					<?php include("modules/member-setting/member_likes_profiles.php");?>
		  			
		  		</div>
		  		<!-- MemberLikedProfiles Setup End-->

		  		<!-- MemberReportAbuse Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberReportAbuse')) { echo 'active';}?>" id="MemberReportAbuse">
		  			<h2>Member Report Abuse</h2>
  					<?php include("modules/member-setting/member_report_abuse.php");?>
		  			
		  		</div>
		  		<!-- MemberReportAbuse Setup End-->

		  		<!-- MemberActivityLogs Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberActivityLogs')) { echo 'active';}?>" id="MemberActivityLogs">
		  			<h2>Member Activity Logs</h2>
  					<?php include("modules/member-setting/member_activity_logs.php");?>
		  			
		  		</div>
		  		<!-- MemberActivityLogs Setup End-->

		  		<!-- MemberEmailLogs Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='MemberEmailLogs')) { echo 'active';}?>" id="MemberEmailLogs">
		  			<h2>Member Email Logs</h2>
  					<?php include("modules/member-setting/member_email_logs.php");?>
		  			
		  		</div>
		  		<!-- MemberEmailLogs Setup End-->

		  		<!-- OrderSummary Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='MemberAttributes') && ($sub_tab=='OrderSummary')) { echo 'active';}?>" id="OrderSummary">
		  			<?php include("modules/purchase-orders/order-summary.php");?>
		  			
		  		</div>
		  		<!-- OrderSummary Setup End-->

		  	</div>
		</div>
	</section>
</body>
<?php
	include("templates/footer.php");
?>