<?php
	$install_file_name = 'install.php';
	$config_file_name = '../config/config.php';
	if(!file_exists($config_file_name)) 
	{
		header("Location:install.php");
		exit;
	}
	else
	if(file_exists($config_file_name) && file_exists($install_file_name)) 
	{
		//unlink($install_file_name);
		//echo "Deleting install file";
		echo "<script>alert('Deleting install file');</script>";
	}
	include("templates/header.php");
	
?>
<title>Dashboard -  <?php echo getWebsiteTitle(); ?></title>

		<section role="main" class="content-body update-section">
			
			<!-- Members with genders start -->
				<div class="row admin-start-section">
					<div class="col-md-12 col-lg-12 col-xl-12">
						<h2>Total Members</h2>
					</div>
					
					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col">
										<div class="summary">
											<div class="info">
												<p>
													<strong class="amount"><?php echo getBrideCount();?></strong>
													<strong class="amount">Brides</strong>
												</p>
												<hr/>
												<p>
													<strong><?php echo getNeverMarrideBrides();?></strong>
													<?php 
														if(getNeverMarrideBrides()==0)
														{
															echo "Never Married";
														}
														else
														{
															echo '<a href="view_members.php?gender=2&marital_status=1"> 
																Never Married
															</a>';	
														}
													?>
													
												</p>
												
												<p>
													<strong><?php echo getAwaitingDivorceBrides();?></strong>
													<?php 
														if(getAwaitingDivorceBrides()==0)
														{
															echo "Awaiting Divorce";
														}
														else
														{
															echo '<a href="view_members.php?gender=2&marital_status=2"> 
																Awaiting Divorce
															</a>';	
														}
													?>
													
												</p>
												
												<p>
													<strong><?php echo getDivorceBrides();?></strong>
													<?php 
														if(getDivorceBrides()==0)
														{
															echo "Divorced";
														}
														else
														{
															echo '<a href="view_members.php?gender=2&marital_status=3"> 
																Divorced
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getWidowedBrides();?></strong>
													<?php 
														if(getWidowedBrides()==0)
														{
															echo "Widowed";
														}
														else
														{
															echo '<a href="view_members.php?gender=2&marital_status=4"> 
																Widowed
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getAnnulledBrides();?></strong>
													<?php 
														if(getAnnulledBrides()==0)
														{
															echo "Annulled";
														}
														else
														{
															echo '<a href="view_members.php?gender=2&marital_status=5"> 
																Annulled
															</a>';	
														}
													?>						
												</p>

												<p>
													<strong><?php echo getProfileIncompleteBrides();?></strong>
													<?php 
														if(getProfileIncompleteBrides()==0)
														{
															echo "Profile Incomplete";
														}
														else
														{
															echo '<a href="view_members.php?gender=2&marital_status=-1"> 
																Profile Incomplete
															</a>';	
														}
													?>
																				
												</p>
												
											</div>
										</div>
										<div class="summary-footer">
											<a href="view_members.php?gender=2&marital_status=0">(view all)</a>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>

					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col">
										<div class="summary">
											<div class="info">
												<p>
													<strong class="amount"><?php echo getGroomCount();?></strong>
													<strong class="amount">Grooms</strong>
												</p>
												<hr/>
												<p>
													<strong><?php echo getNeverMarrideGroom();?></strong>
													<?php 
														if(getNeverMarrideGroom()==0)
														{
															echo "Never Married";
														}
														else
														{
															echo '<a href="view_members.php?gender=1&marital_status=1">
																Never Married
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getAwaitingDivorceGroom();?></strong>
													<?php 
														if(getAwaitingDivorceGroom()==0)
														{
															echo "Awaiting Divorce";
														}
														else
														{
															echo '<a href="view_members.php?gender=1&marital_status=2">
																Awaiting Divorce
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getDivorceGroom();?></strong>
													<?php 
														if(getDivorceGroom()==0)
														{
															echo "Divorced";
														}
														else
														{
															echo '<a href="view_members.php?gender=1&marital_status=3">
																Divorced
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getWidowedGroom();?></strong>
													<?php 
														if(getWidowedGroom()==0)
														{
															echo "Widowed";
														}
														else
														{
															echo '<a href="view_members.php?gender=1&marital_status=4">
																Widowed
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getAnnulledGroom();?></strong>
													<?php 
														if(getAnnulledGroom()==0)
														{
															echo "Annulled";
														}
														else
														{
															echo '<a href="view_members.php?gender=1&marital_status=5">
																Annulled
															</a>';	
														}
													?>						
												</p>

												<p>
													<strong><?php echo getProfileIncompleteGroom();?></strong>
													<?php 
														if(getProfileIncompleteGroom()==0)
														{
															echo "Profile Incomplete";
														}
														else
														{
															echo '<a href="view_members.php?gender=1&marital_status=-1">
																Profile Incomplete
															</a>';	
														}
													?>						
												</p>
												
											</div>
										</div>
										<div class="summary-footer">
											<a href="view_members.php?gender=1&marital_status=0">(view all)</a>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>

					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col">
										<div class="summary">
											<div class="info">
												<p>
													<strong class="amount"><?php echo getTGenderCount();?></strong>
													<strong class="amount">Others</strong>
												</p>
												<hr/>
												<p>
													<strong><?php echo getNeverMarrideTGender();?></strong>
													<?php 
														if(getNeverMarrideTGender()==0)
														{
															echo "Never Married";
														}
														else
														{
															echo '<a href="view_members.php?gender=3&marital_status=1"> 
																Never Married
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getAwaitingDivorceTGender();?></strong>
													<?php 
														if(getAwaitingDivorceTGender()==0)
														{
															echo "Awaiting Divorce";
														}
														else
														{
															echo '<a href="view_members.php?gender=3&marital_status=2"> 
																Awaiting Divorce
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getDivorceTGender();?></strong>
													<?php 
														if(getDivorceTGender()==0)
														{
															echo "Divorced";
														}
														else
														{
															echo '<a href="view_members.php?gender=3&marital_status=3"> 
																Divorced
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getWidowedTGender();?></strong>
													<?php 
														if(getWidowedTGender()==0)
														{
															echo "Widowed";
														}
														else
														{
															echo '<a href="view_members.php?gender=3&marital_status=4"> 
																Widowed
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getAnnulledTGender();?></strong>
													<?php 
														if(getAnnulledTGender()==0)
														{
															echo "Annulled";
														}
														else
														{
															echo '<a href="view_members.php?gender=3&marital_status=5"> 
																Annulled
															</a>';	
														}
													?>						
												</p>
												
												<p>
													<strong><?php echo getProfileIncompleteTGender();?></strong>
													<?php 
														if(getProfileIncompleteTGender()==0)
														{
															echo "Profile Incomplete";
														}
														else
														{
															echo '<a href="view_members.php?gender=3&marital_status=-1"> 
																Profile Incomplete
															</a>';	
														}
													?>						
												</p>

											</div>
										</div>
										<div class="summary-footer">
											<a href="view_members.php?gender=3&marital_status=0">(view all)</a>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>

					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col">
										<div class="summary">
											<div class="info">
												<p>
													<strong class="amount"><?php echo getBrideCount()+getGroomCount()+getTGenderCount();?></strong>
													<strong class="amount">Total</strong>
												</p>
												<hr/>
												<p>
													<strong><?php echo getNeverMarrideBrides()+getNeverMarrideGroom()+getNeverMarrideTGender();?></strong>
													<?php 
														if((getNeverMarrideBrides()+getNeverMarrideGroom()+getNeverMarrideTGender())==0)
														{
															echo "Never Married";
														}
														else
														{
															echo '<a href="view_members.php?gender=0&marital_status=1"> 
																Never Married
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getAwaitingDivorceBrides()+getAwaitingDivorceGroom()+getAwaitingDivorceTGender();?></strong>
													<?php 
														if((getAwaitingDivorceBrides()+getAwaitingDivorceGroom()+getAwaitingDivorceTGender())==0)
														{
															echo "Awaiting Divorce";
														}
														else
														{
															echo '<a href="view_members.php?gender=0&marital_status=2"> 
																Awaiting Divorce
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getDivorceBrides()+getDivorceGroom()+getDivorceTGender();?></strong>
													<?php 
														if((getDivorceBrides()+getDivorceGroom()+getDivorceTGender())==0)
														{
															echo "Divorce";
														}
														else
														{
															echo '<a href="view_members.php?gender=0&marital_status=3"> 
																Divorced
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getWidowedBrides()+getWidowedGroom()+getWidowedTGender();?></strong>
													<?php 
														if((getWidowedBrides()+getWidowedGroom()+getWidowedTGender())==0)
														{
															echo "Widowed";
														}
														else
														{
															echo '<a href="view_members.php?gender=0&marital_status=4"> 
																Widowed
															</a>';	
														}
													?>
												</p>
												
												<p>
													<strong><?php echo getAnnulledBrides()+getAnnulledGroom()+getAnnulledTGender();?></strong>
													<?php 
														if((getWidowedBrides()+getWidowedGroom()+getWidowedTGender())==0)
														{
															echo "Annulled";
														}
														else
														{
															echo '<a href="view_members.php?gender=0&marital_status=5"> 
																Annulled
															</a>';	
														}
													?>						
												</p>

												<p>
													<strong><?php echo getProfileIncompleteBrides()+getProfileIncompleteGroom()+getProfileIncompleteTGender();?></strong>
													<?php 
														if((getWidowedBrides()+getWidowedGroom()+getWidowedTGender())==0)
														{
															echo "Profile Incomplete";
														}
														else
														{
															echo '<a href="view_members.php?gender=0&marital_status=-1"> 
																Profile Incomplete
															</a>';	
														}
													?>						
												</p>
												
											</div>
										</div>
										<div class="summary-footer">
											<a href="view_members.php?gender=0&marital_status=0">(view all)</a>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			<!-- Members with genders end -->

			<!-- Members with status start -->
				<div class="row">
					<div class="col-md-12 col-lg-12 col-xl-12">
						<h2>Member Status</h2>
					</div>

					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col">
										<div class="summary">
											<div class="info">
												<p>
													<strong class="amount"><?php echo getActiveMemberCount();?></strong>
													<strong class="amount">Active</strong>
												</p>
											</div>
										</div>
										<div class="summary-footer">
											<a href="view_members_list_status.php?status=1">(view all)</a>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>

					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col">
										<div class="summary">
											<div class="info">
												<p>
													<strong class="amount"><?php echo getInActiveMemberCount();?></strong>
													<strong class="amount">In-Active</strong>
												</p>
											</div>
										</div>
										<div class="summary-footer">
											<a href="view_members_list_status.php?status=0">(view all)</a>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>

					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col">
										<div class="summary">
											<div class="info">
												<p>
													<strong class="amount"><?php echo getDeactivatedMemberCount();?></strong>
													<strong class="amount">Deactivate</strong>
												</p>
											</div>
										</div>
										<div class="summary-footer">
											<a href="view_members_list_status.php?status=2">(view all)</a>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>

					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col">
										<div class="summary">
											<div class="info">
												<p>
													<strong class="amount"><?php echo getSuspendedMemberCount();?></strong>
													<strong class="amount">Suspend</strong>
												</p>
											</div>
										</div>
										<div class="summary-footer">
											<a href="view_members_list_status.php?status=3">(view all)</a>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			<!-- Members with status start -->
		</section>
	</div>
</section>
<?php
	include("templates/footer.php");
?>