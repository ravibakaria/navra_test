	</div>
</section>	
	<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>

	<?php include("../layout/footer-content.php");?>

		<!-- Scripts -->
		<script src="../js/jquery.js"></script>
		<script src="../js/jquery.browser.mobile.js"></script>
		<script src="../js/bootstrap.js"></script>
		<script src="../js/nanoscroller.js"></script>
		<script src="../js/bootstrap-datepicker.js"></script>
		<script src="../js/magnific-popup.js"></script>
		<script src="../js/jquery.placeholder.js"></script>
		<script src="../js/bootstrap-multiselect.js"></script>
		<script src="../js/theme.js"></script>
		<script src="../js/theme.custom.js"></script>
		<script src="../js/theme.init.js"></script>
		<script src="../js/examples.mediagallery.js" /></script>
		<script src="../js/examples.advanced.form.js" /></script>
		<script src="../js/jquery.dataTables.min.js"></script>
		<script src="../js/dataTables.bootstrap.min.js"></script>
		<script src="../js/dataTables.tableTools.min.js"></script>
		<script src="../js/bootstrap-toggle.min.js"></script>
		<script src="../js/jquery.richtext.js"></script>
    	
    	<!-- Footer Script  -->
	    <?php 
	        $include_footer_script = getFooterScriptDisplay();
	        $footer_script = getFooterScript();
	        if($include_footer_script!='' && $include_footer_script!=null && $include_footer_script!='0')
	        {
	            @$footer_script_data = file_get_contents($websiteBasePath.'/'.$footer_script);
	    		echo "$footer_script_data";
	        }
	    ?>

	    <script>
	    	/*************      Open menu on mouse hover     ****************/
	    	var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
	    	jQuery(document).ready(function($){
			    if(!isMobile) {
			        $(function(){
					    $('.dropdown').hover(function() {
					        $(this).addClass('open');
					    },
					    function() {
					        $(this).removeClass('open');
					    });
					});
			    }
			});
		</script>

		<script>
			$(document).ready(function(){
				$(window).scroll(function () {
		            if ($(this).scrollTop() > 50) 
		            {
		                $('#back-to-top').fadeIn();
		            } 
		            else 
		            {
		                $('#back-to-top').fadeOut();
		            }
		        });
			        // scroll body to 0px on click
			        $('#back-to-top').click(function () {
			            $('#back-to-top').tooltip('hide');
			            $('body,html').animate({
			                scrollTop: 0
			            }, 800);
			            return false;
			        });
			        
			        $('#back-to-top').tooltip('show');
			        
			});
		</script>
	</body>
</html>