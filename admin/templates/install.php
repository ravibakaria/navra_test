<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

		<!-- Custom CSS -->
		<link rel="stylesheet" href="../assets/stylesheets/custom.css">

		<!-- Head Libs -->
		<script src="../assets/vendor/modernizr/modernizr.js"></script>

	</head>
	<body class="login-body">
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Install data</h2>
					</div>
					
					<div class="panel-body">
						<form>
							<div class="url-path">
								<?php
									$server_url = $_SERVER["SERVER_NAME"]; 
									$doc_root = $_SERVER["DOCUMENT_ROOT"]; 
								?>
								<p align="center"><strong>Base Path: </strong><?php echo $server_url;?></p>
								<p align="center"><strong>Admin Path: </strong><?php echo $server_url.'/admin';?></p>
								<input type="hidden" class="base_path" value="<?php echo $server_url;?>">
							</div>

							<div class="form-group mb-lg">
								<label>Host Name:</label>
								<div class="input-group input-group-icon">
									<input name="host_name" type="text" class="form-control input-lg host_name" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<label>Database Name:</label>
								<div class="input-group input-group-icon">
									<input name="db_name" type="text" class="form-control input-lg db_name" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<label>Database User Name:</label>
								<div class="input-group input-group-icon">
									<input name="db_user_name" type="text" class="form-control input-lg db_user_name" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Database Password:</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="db_password" type="password" class="form-control input-lg db_password" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-8">
									<div class="mb-xs text-center db_form_status">
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<a class="btn btn-primary btn_chk_database">Check Database</a>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Site Title:</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="WebSiteTitle" type="text" class="form-control input-lg WebSiteTitle" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-file-text-o"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Site Tagline:</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="WebSiteTagline" type="text" class="form-control input-lg WebSiteTagline" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-file-text-o"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Admin User Name:</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="admin_user_name" type="text" class="form-control input-lg admin_user_name" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Admin Email Id:</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="admin_email" type="text" class="form-control input-lg admin_email" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-envelope"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Admin Password:</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="admin_password" type="password" class="form-control input-lg admin_password" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label>Confirm Password:</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="admin_confirm_password" type="password" class="form-control input-lg admin_confirm_password" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="mb-xs text-center form_status">
								
							</div>

							<div class="row">
								<div class="col-sm-8">
									<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
								</div>
								<div class="col-sm-4 text-right">
									<a class="btn btn-primary btn_install">Install</a>
								</div>
							</div>

							
						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2018. All rights reserved. <a href="#">NavraBayko.com</a>.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="../assets/vendor/jquery/jquery.js"></script>
		<script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../assets/javascripts/theme.init.js"></script>

	</body>
</html>

<script>
	$(document).ready(function(){

		/* empty error message  */
		$('.host_name, .db_name, .db_user_name, .db_password, .admin_user_name, .admin_password, .admin_confirm_password,admin_email').click(function(){
            $('.host_name, .db_name, .db_user_name, .db_password, .admin_user_name, .admin_password, .admin_confirm_password, .admin_email').removeClass("danger_error");
        });

        $('.btn_chk_database').click(function(){
        	var host_name = $('.host_name').val();
			var db_name = $('.db_name').val();
			var db_user_name = $('.db_user_name').val();
			var db_password = $('.db_password').val();
			var task = "Check_db_connectivity";
			if(host_name=='' || host_name==null)
			{
				$('.db_form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter host name.</div>");
				$(".host_name").addClass("danger_error");
				return false;
			}

			if(db_name=='' || db_name==null)
			{
				$('.db_form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter database name.</div>");
				$(".db_name").addClass("danger_error");
				return false;
			}

			if(db_user_name=='' || db_user_name==null)
			{
				$('.db_form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter user name.</div>");
				$(".db_user_name").addClass("danger_error");
				return false;
			}

			if(db_password=='' || db_password==null)
			{
				$('.db_form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter database password.</div>");
				$(".db_password").addClass("danger_error");
				return false;
			}

			var data = 'host_name='+host_name+'&db_name='+db_name+'&db_user_name='+db_user_name+'&db_password='+db_password+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/install_helper.php',
            	success:function(res)
            	{
            		if(res=='success')
            		{
            			$('.db_form_status').html("<div class='alert alert-success success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Connected To Database Successfully.</div>");
            			return false;
            		}
            		else
            		{
            			$('.db_form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}
            	}
            });

        });

		$('.btn_install').click(function(){
			var base_path = $('.base_path').val();
			var host_name = $('.host_name').val();
			var db_name = $('.db_name').val();
			var db_user_name = $('.db_user_name').val();
			var db_password = $('.db_password').val();
			var WebSiteTitle = $('.WebSiteTitle').val();
			var WebSiteTagline = $('.WebSiteTagline').val();
			var admin_user_name = $('.admin_user_name').val();
			var admin_email = $('.admin_email').val();
			var admin_password = $('.admin_password').val();
			var admin_confirm_password = $('.admin_confirm_password').val();
			var task = "install_data_in";

			if(WebSiteTitle=='' || WebSiteTitle==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter website title.</div>");
				$(".WebSiteTitle").addClass("danger_error");
				return false;
			}

			if(WebSiteTagline=='' || WebSiteTagline==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter website tagline.</div>");
				$(".WebSiteTagline").addClass("danger_error");
				return false;
			}

			if(host_name=='' || host_name==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter host name.</div>");
				$(".host_name").addClass("danger_error");
				return false;
			}

			if(db_name=='' || db_name==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter database name.</div>");
				$(".db_name").addClass("danger_error");
				return false;
			}

			if(db_user_name=='' || db_user_name==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter user name.</div>");
				$(".db_user_name").addClass("danger_error");
				return false;
			}

			if(db_password=='' || db_password==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter database password.</div>");
				$(".db_password").addClass("danger_error");
				return false;
			}

			if(admin_user_name=='' || admin_user_name==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter Admin Username.</div>");
				$(".admin_user_name").addClass("danger_error");
				return false;
			}

			if(admin_email=='' || admin_email==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter Admin email id.</div>");
				$(".admin_email").addClass("danger_error");
				return false;
			}

			function validateEmail($admin_email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $admin_email );
            }

            if( !validateEmail(admin_email)) 
            { 
            	 $('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Invalid!</strong> Email id is not valid.</div>");
				$(".admin_email").addClass("danger_error");
                return false;
            }

			if(admin_password=='' || admin_password==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Please enter admin password.</div>");
				$(".admin_password").addClass("danger_error");
				return false;
			}

			if(admin_confirm_password=='' || admin_confirm_password==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Confirm password.</div>");
				$(".admin_confirm_password").addClass("danger_error");
				return false;
			}

			if(admin_password != admin_confirm_password==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> Admin password & confirm password not same.</div>");
				$(".admin_password").addClass("danger_error");
				$(".admin_confirm_password").addClass("danger_error");
				return false;
			}

			$('.loading_img').show();
			
			var data = 'base_path='+base_path+'&host_name='+host_name+'&db_name='+db_name+'&db_user_name='+db_user_name+'&db_password='+db_password+'&WebSiteTitle='+WebSiteTitle+'&WebSiteTagline='+WebSiteTagline+'&admin_user_name='+admin_user_name+'&admin_email='+admin_email+'&admin_password='+admin_password+'&admin_confirm_password='+admin_confirm_password+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/install_helper.php',
            	success:function(res)
            	{
            		$('.loading_img').hide();
            		if(res=='success')
            		{
            			$('.btn_login').attr('disabled',true);
            			$('.form_status').html("<div class='alert alert-success success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong> <span class='fa fa-check'></span></strong> Database created successfully.<br/><span class='fa fa-check'></span> Data imported in tables.<br/><span class='fa fa-check'></span> Config file created successfully<br/>.</div><br/><br/> Login to admin dashboard <a href='login.php'>Click Here</a>");
            		}
            		else
            		{
            			$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}
            	}
            });
		});
	});
</script>