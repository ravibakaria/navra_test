<?php
	session_start();

	if(!isset($_SESSION['admin_logged_in']) && ($_SESSION['admin_user']=='' || $_SESSION['admin_user']==null))
	{
		header('Location: login.php');
		exit;
	}

	require_once "../config/config.php";
	require_once "../config/dbconnect.php";
	include "../config/functions.php";
	include "../config/email/email_style.php";
	include "../config/email/email_templates.php";
	
	$admin_id = $_SESSION['admin_id'];
	$admin_firstname = getAdminFirstName($admin_id);

	$current_file = basename($_SERVER['PHP_SELF']);

	if($admin_firstname=='' || $admin_firstname==null)
	{
		$admin_firstname = "Admin";
	}

	$sql_admin_info = $link->prepare("SELECT * FROM `admins` WHERE id='$admin_id'"); 
    $sql_admin_info->execute();
    $adminData = $sql_admin_info->fetch(PDO::FETCH_OBJ);
    $admin_email = $adminData->email;

    @$previous_page_url = $_SERVER['HTTP_REFERER'];
   
    $_SESSION['redirect_url'] = $previous_page_url;
?>
<!doctype html>
<html lang="en-US">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="Marriage Navra Bayko Bride Groome" />
		<meta name="description" content="Navra Bayko - Matrimonial Match">
		<meta name="author" content="navrabayko.com">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- CSS  -->
		<link rel="stylesheet" href="../css/google-fonts.css" type="text/css">
		<link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
		<link rel="stylesheet" href="../css/font-awesome.css" type="text/css">
		<link rel="stylesheet" href="../css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="../css/datepicker3.css" type="text/css">
		<link rel="stylesheet" href="../css/dataTables.bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="../css/bootstrap-toggle.min.css" type="text/css">
		<link rel="stylesheet" href="../css/theme.css" type="text/css">
		<link rel="stylesheet" href="../css/default.css" type="text/css">
		<link rel="stylesheet" href="../css/theme-custom.css" type="text/css">
		<link rel="stylesheet" href="../css/master.css" type="text/css">
		<link rel="stylesheet" href="../css/custom.css" type="text/css">
		<link rel="stylesheet" href="../css/admin.css" type="text/css">
		<link rel="stylesheet" href="../config/style.php" type='text/css'>
		<link rel="stylesheet" href="../css/bootstrap-glyphicons.css" type="text/css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
	    <link rel="stylesheet" href="../css/richtext-editor.css" type="text/css">
	    <link rel="stylesheet" href="../css/richtext.min.css" type="text/css">
	    <link rel="stylesheet" href="../css/bootstrap-multiselect.css" type="text/css">
	    <?php
		    $sql_custom_meta = "SELECT * FROM `custom_meta_tags`";
		    $stmt_custom_meta = $link->prepare($sql_custom_meta);
		    $stmt_custom_meta->execute();
		    $count_custom_meta = $stmt_custom_meta->rowCount();

		    if($count_custom_meta>0)
		    {
		      $result_custom_meta = $stmt_custom_meta->fetchAll();
		      foreach ($result_custom_meta as $row_custom_meta) 
		      {
		          $meta_tag = $row_custom_meta['meta_tag'];
		          $meta_tag = str_replace("--lt","<",$meta_tag);

		          echo $meta_tag;
		          echo "\n";
		      }
		    }
		?>
	    <!-- Favicon  -->
	    <?php 
		    $websiteBasePath = getWebsiteBasePath();
		    if(getFaviconURL()!='' || getFaviconURL()!=null)
		    {
		      $favicon_arr=array();
		      $favicon_get = getFaviconURL();
		      if($favicon_get!='' || $favicon_get!=null)
		      {
		        $favicon_get = explode('/',$favicon_get);

		        for($i=1;$i<count($favicon_get);$i++)
		        {
		          $favicon_arr[] = $favicon_get[$i];
		        }

		        $favicon = implode('/',$favicon_arr);
		      }
		      echo "<link rel='icon' type='image/png' href='$websiteBasePath/$favicon' sizes='32x32'>";
		    }
		    else
		    {
		      echo "<link rel='icon' type='image/png' href='$websiteBasePath/images/favicon/default-favicon.png' sizes='32x32'>";
		    }
		?>
	    
	    
		<!-- Head Libs -->
		<script src="../js/modernizr.js"></script>
		<script src="../js/jquery.js"></script>
		<!-- Header Script  -->
	    <?php 
	    	$include_header_script = getHeaderScriptDisplay();
	    	$header_script = getHeaderScript();
	    	if($include_header_script!='' && $include_header_script!=null && $include_header_script!=0)
	    	{
	    		@$header_script_data = file_get_contents($websiteBasePath.'/'.$header_script);
	    		echo "$header_script_data";
	    	}
	    ?>
	
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="index.php" class="navbar-brand logo">
						<?php 
							$logo = getLogoURL();
							$sitetitle = getWebsiteTitle();
							$siteBasePath = getWebsiteBasePath();
							if($logo!='' || $logo!=null)
							{
								echo "<img src='$logo' class='img-responsive logo-img'  />";
							}
							else
							{
								echo "<img src='$siteBasePath/images/logo/default-logo.jpg' class='img-responsive logo-img default-logo' />";
							}
						?>
					</a>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
			
					<span class="separator"></span>
		
					<div id="userbox" class="userbox userbox-admin">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="../images/avtar2.jpg" alt="<?php echo $admin_firstname?>" class="img-circle" data-lock-picture="../assets/images/avtar2.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="<?php echo $admin_firstname?>" data-lock-email="<?php echo $admin_email?>">
								<span class="name"><?php echo $admin_firstname?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu home_profile_menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="change-password.php"><i class="fa fa-key"></i> Change Password</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="logout.php"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->

				<!--    Secondary Menu Start   -->
				<div class="navbar navbar-default secondary-navigation" role="navigation">
				    <div class="row">
				        <div class="navbar-header">
				            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div>
				        <div class="collapse navbar-collapse">
				            <ul class="nav navbar-nav secondary-navigation-ul">
				                <li class="<?php if($current_file=='index.php') { echo 'nav-active'; } ?> ">
									<a href="index.php">
										<i class="fa fa-tachometer" aria-hidden="true"></i>
										<span>Dashboard</span>
									</a>
								</li>

								<li class="dropdown">
				                    <a href="#" class="dropdown dropdown-toggle <?php if($current_file=='project-configuration.php' || $current_file=='master-data-setup.php' || $current_file=='update-body-type-attributes.php' || $current_file=='update-complexion-attributes.php' || $current_file=='update-Eating-Habbit-attributes.php' || $current_file=='update-Marital-Status-attributes.php' || $current_file=='update-Smoking-Habbit-attributes.php' || $current_file=='update-Drinking-Habbit-attributes.php' || $current_file=='update-Special-Case-attributes.php' || $current_file=='update-gender-attributes.php' || $current_file=='update-education-attributes.php' || $current_file=='update-Employment-attributes.php' || $current_file=='update-Category-attributes.php' || $current_file=='update-Designation-attributes.php' || $current_file=='update-Industry-attributes.php' || $current_file=='update-Currency-attributes.php' || $current_file=='update-Religion-attributes.php' || $current_file=='update-Caste-attributes.php' || $current_file=='update-MotherTongue-attributes.php' || $current_file=='update-Country-attributes.php' || $current_file=='update-State-attributes.php' || $current_file=='update-City-attributes.php' || $current_file=='update-FamilyValue-attributes.php' || $current_file=='update-FamilyType-attributes.php' || $current_file=='update-FamilyStatus-attributes.php' || $current_file=='update-identity-document-types.php' || $current_file=='website-content-setup.php' || $current_file=='email-template-setup.php' || $current_file=='update-Email-Template.php' || $current_file=='membership-plan-setup.php' || $current_file=='update-membership-plan-setup.php' || $current_file=='payment-gateway-setup.php') { echo 'nav-active'; } ?>" data-toggle="dropdown">
				                    	<i class="fa fa-wrench" aria-hidden="true"></i>
				                    	Configuration <b class="caret"></b></a>
				                    <ul class="dropdown-menu">
				                        <li class="<?php if($current_file=='project-configuration.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="project-configuration.php">
												<i class="fa fa-gears"></i>
												<span>Project Configuration</span>
											</a>
										</li>
				                        
				                        <li class="<?php if($current_file=='master-data-setup.php' || $current_file=='update-body-type-attributes.php' || $current_file=='update-complexion-attributes.php' || $current_file=='update-Eating-Habbit-attributes.php' || $current_file=='update-Marital-Status-attributes.php' || $current_file=='update-Smoking-Habbit-attributes.php' || $current_file=='update-Drinking-Habbit-attributes.php' || $current_file=='update-Special-Case-attributes.php' || $current_file=='update-gender-attributes.php' || $current_file=='update-education-attributes.php' || $current_file=='update-Employment-attributes.php' || $current_file=='update-Category-attributes.php' || $current_file=='update-Designation-attributes.php' || $current_file=='update-Industry-attributes.php' || $current_file=='update-Currency-attributes.php' || $current_file=='update-Religion-attributes.php' || $current_file=='update-Caste-attributes.php' || $current_file=='update-MotherTongue-attributes.php' || $current_file=='update-Country-attributes.php' || $current_file=='update-State-attributes.php' || $current_file=='update-City-attributes.php' || $current_file=='update-FamilyValue-attributes.php' || $current_file=='update-FamilyType-attributes.php' || $current_file=='update-FamilyStatus-attributes.php' || $current_file=='update-identity-document-types.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="master-data-setup.php">
												<i class="fa fa-wrench" aria-hidden="true"></i>
												<span>Master Data Setup</span>
											</a>
										</li>

										<li class="<?php if($current_file=='website-content-setup.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="website-content-setup.php">
												<i class="fa fa-globe" aria-hidden="true"></i>
												<span>Website Content</span>
											</a>
										</li>

										<li class="<?php if($current_file=='email-template-setup.php' || $current_file=='update-Email-Template.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="email-template-setup.php">
												<i class="fa fa-envelope" aria-hidden="true"></i>
												<span>Email Template Setup</span>
											</a>
										</li>

										<li class="<?php if($current_file=='membership-plan-setup.php' || $current_file=='update-membership-plan-setup.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="membership-plan-setup.php">
												<i class="fa fa-bars" aria-hidden="true"></i>
												<span>Membership Plans</span>
											</a>
										</li>

										<li class="<?php if($current_file=='payment-gateway-setup.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="payment-gateway-setup.php">
												<i class="fa fa-money" aria-hidden="true"></i>
												<span>Payment Gateways</span>
											</a>
										</li>

				                    </ul>
				                </li>
								
								<li class="dropdown">
				                    <a href="#" class="dropdown dropdown-toggle <?php if($current_file=='user-setup.php' || $current_file=='update-Admin-attributes.php' || $current_file=='update-Department-attributes.php' || $current_file=='member-setup.php' || $current_file=='member-profile.php' || $current_file=='vendor-setup.php' || $current_file=='vendor-profile.php') { echo 'nav-active'; } ?>" data-toggle="dropdown">
				                    	<i class="fa fa-users" aria-hidden="true"></i>
				                    	Users Configuration <b class="caret"></b></a>
				                    <ul class="dropdown-menu">
				                        <li class="<?php if($current_file=='user-setup.php' || $current_file=='update-Admin-attributes.php' || $current_file=='update-Department-attributes.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="user-setup.php">
												<i class="fa fa-user" aria-hidden="true"></i>
												<span>Admin User/Department</span>
											</a>
										</li>

										<li class="<?php if($current_file=='member-setup.php' || $current_file=='member-profile.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="member-setup.php">
												<i class="fa fa-user" aria-hidden="true"></i>
												<span>Member User</span>
											</a>
										</li>
									</ul>
								</li>

								<li class="dropdown">
				                    <a href="#"  class="dropdown dropdown-toggle <?php if($current_file=='payment-summary.php') { echo 'nav-active'; } ?>" data-toggle="dropdown">
				                    	<i class="fa fa-credit-card" aria-hidden="true"></i>
				                    	Transactions <b class="caret"></b></a>
				                    <ul class="dropdown-menu">
				                        <li class="<?php if($current_file=='payment-summary.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="payment-summary.php">
												<i class="fa fa-list-alt" aria-hidden="true"></i>
												<span>Payment Summary</span>
											</a>
										</li>
									</ul>
								</li>

								<li class="dropdown">
				                    <a href="#"  class="dropdown dropdown-toggle <?php if($current_file=='category.php' || $current_file=='update-wedding-category-attributes.php' || $current_file=='vendors.php' || $current_file=='wedding_directory_setup.php' || $current_file=='vendor-information.php') { echo 'nav-active'; } ?>" data-toggle="dropdown">
				                    	<i class="fa fa-wikipedia-w" aria-hidden="true"></i>
				                    	Wedding directory <b class="caret"></b></a>
				                    <ul class="dropdown-menu">
				                    	<li class="<?php if($current_file=='wedding_directory_setup.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="wedding_directory_setup.php">
												<i class="fa fa-cog" aria-hidden="true"></i>
												<span>Setting</span>
											</a>
										</li>
				                        <li class="<?php if($current_file=='category.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="category.php">
												<i class="fa fa-th" aria-hidden="true"></i>
												<span>Category</span>
											</a>
										</li>
										<li class="<?php if($current_file=='vendors.php' || $current_file=='vendor-information.php') { echo 'nav-active'; } ?> sidebar-links">
											<a href="vendors.php">
												<i class="fa fa-user" aria-hidden="true"></i>
												<span>Vendors</span>
											</a>
										</li>
									</ul>
								</li>

				            </ul>
				        </div><!--/.nav-collapse -->
				    </div>
				</div>
				<!--    Secondary Menu End   -->
			</header>
			<!-- end: header -->

			<div class="inner-wrapper admin-start-section">
				
				