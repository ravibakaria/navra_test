<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt_im   = $link->prepare("SELECT * FROM `payment_instamojo`");
	$stmt_im->execute();
	$result_im = $stmt_im->fetch();
	$count_im=$stmt_im->rowCount();

	$im_is_enable = $result_im['im_is_enable'];
    $im_display_name = $result_im['im_display_name'];
    $im_activated_mode = $result_im['im_activated_mode'];
    $im_test_api_key = $result_im['im_test_api_key'];
    $im_test_auth_token = $result_im['im_test_auth_token'];
    $im_test_salt = $result_im['im_test_salt'];
    $im_test_email = $result_im['im_test_email'];
    $im_prod_api_key = $result_im['im_prod_api_key'];
    $im_prod_auth_token = $result_im['im_prod_auth_token'];
    $im_prod_salt = $result_im['im_prod_salt'];
    $im_prod_email = $result_im['im_prod_email'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">          
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <h2>Instamojo</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-right">
                <a href="<?php echo $websiteBasePath?>/reference-documents/Instamojo-Setup-Steps.pdf" target="_blank">
                    <span data-toggle="tooltip" data-placement="left" title="Instamojo Refrence Document.">
                        <img src="<?php echo $websiteBasePath?>/images/file-pdf.png">
                    </span>
                </a>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
                Enable Instamojo Payment Gateway:
            </div>
            <div class="col-xs-4 col-sm-4 col-md-1 col-lg-1">
                <input type="checkbox" class="im_is_enable" <?php if($count_im>0 && ($im_is_enable=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this payment gateway then Instamojo payment gateway will be available to member to make payment.
                </div>
            </div>
        </div>

        <div class="row input-row InstamojoPaymentSettingData" <?php if($im_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Display Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control im_display_name" type="text" value="<?php echo @$im_display_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This name will be displayed on page selecting payment method.
                </div>
            </div>
        </div>

        <div class="row input-row InstamojoPaymentSettingData" <?php if($im_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                Activate Mode:
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <input type='radio' name='im_activated_mode' value='TEST' class='im_activated_mode' id='IMTEST' <?php if($im_activated_mode=='TEST') { echo "checked"; } ?>>  <label for='IMTEST' class='control-label'>&nbsp;Test Mode </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='radio' name='im_activated_mode' value='PROD' class='im_activated_mode' id='IMPROD'<?php if($im_activated_mode=='PROD') { echo "checked"; } ?>>  <label for='IMPROD' class='control-label'>&nbsp;Production/Live Mode </label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> Test mode or Production mode.
                </div>
            </div>
        </div>

        <!--        Instamojo Test Mode Data STart        -->
        <div class="row input-row imTestModeSettingData" <?php if($im_is_enable!='1' || $im_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Private API Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control im_test_api_key" type="text" value="<?php echo @$im_test_api_key;?>">
            </div>
        </div>

        <div class="row input-row imTestModeSettingData" <?php if($im_is_enable!='1' || $im_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Private Auth Token:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control im_test_auth_token" type="text" value="<?php echo @$im_test_auth_token;?>">
            </div>
        </div>

        <div class="row input-row imTestModeSettingData" <?php if($im_is_enable!='1' || $im_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Instamojo Salt:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control im_test_salt" type="text" value="<?php echo @$im_test_salt;?>">
            </div>
        </div>

        <div class="row input-row imTestModeSettingData" <?php if($im_is_enable!='1' || $im_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Reponse Email:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control im_test_email" type="text" value="<?php echo @$im_test_email;?>">
            </div>
        </div>

        <!--        Instamojo Production/live Mode Data STart        -->
        <div class="row input-row imProdModeSettingData" <?php if($im_is_enable!='1' || $im_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Private API Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control im_prod_api_key" type="text" value="<?php echo @$im_prod_api_key;?>">
            </div>
        </div>

        <div class="row input-row imProdModeSettingData" <?php if($im_is_enable!='1' || $im_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Private Auth Token:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control im_prod_auth_token" type="text" value="<?php echo @$im_prod_auth_token;?>">
            </div>
        </div>

        <div class="row input-row imProdModeSettingData" <?php if($im_is_enable!='1' || $im_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Instamojo Salt:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control im_prod_salt" type="text" value="<?php echo @$im_prod_salt;?>">
            </div>
        </div>

        <div class="row input-row imProdModeSettingData" <?php if($im_is_enable!='1' || $im_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Reponse Email:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control im_prod_email" type="text" value="<?php echo @$im_prod_email;?>">
            </div>
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 instamojo_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_instamojo' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><button class="btn btn-success btn-sm btn_submit_instamojo website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        /*    Instamojo enable start   */
            $('.im_is_enable').change(function(){
                var im_activated_mode = $("input[name='im_activated_mode']:checked"). val();

                if($(this).is(':checked'))
                {
                    var im_is_enable = '1';
                }
                else
                {
                    var im_is_enable = '0';
                }

                if(im_is_enable=='1')
                {
                    $('.InstamojoPaymentSettingData').show();

                    if(im_activated_mode=='TEST')
                    {
                        $('.imTestModeSettingData').show();
                        $('.imProdModeSettingData').hide();
                    }
                    else
                    if(im_activated_mode=='PROD')
                    {
                        $('.imProdModeSettingData').show();
                        $('.imTestModeSettingData').hide();
                    }
                    return false;
                }
                else
                {
                    $('.InstamojoPaymentSettingData').hide();
                    $('.imTestModeSettingData').hide();
                    $('.imProdModeSettingData').hide();
                    return false;
                } 


            });
        /*     Instamojo enable end   */

        /*    Transaction mode activation  */
        $('.im_activated_mode').change(function(){
            var im_activated_mode = $("input[name='im_activated_mode']:checked"). val();
            if(im_activated_mode=='TEST')
            {
                $('.imTestModeSettingData').show();
                $('.imProdModeSettingData').hide();
            }
            else
            if(im_activated_mode=='PROD')
            {
                $('.imProdModeSettingData').show();
                $('.imTestModeSettingData').hide();
            }
        });

        $('.btn_submit_instamojo').click(function(){
            if($('.im_is_enable').is(':checked'))
            {
                var im_is_enable = '1';
            }
            else
            {
                var im_is_enable = '0';
            }

            var im_display_name = $('.im_display_name').val();
            var im_activated_mode = $("input[name='im_activated_mode']:checked"). val();
            if(im_activated_mode=='' || im_activated_mode==null || im_activated_mode=='undefined')
            {
                im_activated_mode = '0';
            }

            var im_test_api_key = $('.im_test_api_key').val().toString();
            var im_test_auth_token = $('.im_test_auth_token').val().toString();
            var im_test_salt = $('.im_test_salt').val().toString();
            var im_test_email = $('.im_test_email').val().toString();
            var im_prod_api_key = $('.im_prod_api_key').val().toString();
            var im_prod_auth_token = $('.im_prod_auth_token').val().toString();
            var im_prod_salt = $('.im_prod_salt').val().toString();
            var im_prod_email = $('.im_prod_email').val().toString();

            if(im_is_enable=='1' && (im_display_name=='' || im_display_name==null))
            {
                $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter display name.</div></center>");
                return false
            }

            if(im_is_enable=='1' && (im_activated_mode=='' || im_activated_mode==null || im_activated_mode=='0'))
            {
                $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please select activation mode.</div></center>");
                return false
            }

            /***********      Test mode validation      **************/
            if(im_activated_mode=='TEST' && (im_test_api_key=='' || im_test_api_key==null))
            {
                $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter instamojo test mode api key.</div></center>");
                return false
            }

            if(im_activated_mode=='TEST' && (im_test_auth_token=='' || im_test_auth_token==null))
            {
                $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter instamojo test mode auth token.</div></center>");
                return false
            }

            if(im_activated_mode=='TEST' && (im_test_salt=='' || im_test_salt==null))
            {
                $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter instamojo test mode salt.</div></center>");
                return false
            }

            if(im_activated_mode=='TEST' && (im_test_email=='' || im_test_email==null))
            {
                $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter test mode email id.</div></center>");
                return false
            }


            /***********      Production mode validation      **************/
            if(im_activated_mode=='PROD' && (im_prod_api_key=='' || im_prod_api_key==null))
            {
                $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter instamojo production mode api key.</div></center>");
                return false
            }

            if(im_activated_mode=='PROD' && (im_prod_auth_token=='' || im_prod_auth_token==null))
            {
                $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter instamojo production mode auth token.</div></center>");
                return false
            }

            if(im_activated_mode=='PROD' && (im_prod_salt=='' || im_prod_salt==null))
            {
                $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter instamojo production mode salt.</div></center>");
                return false
            }

            if(im_activated_mode=='PROD' && (im_prod_email=='' || im_prod_email==null))
            {
                $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter production mode email id.</div></center>");
                return false
            }

            var task = "instamojo_payment_gateway";

            var data = {
                im_is_enable : im_is_enable,
                im_display_name : im_display_name,
                im_activated_mode : im_activated_mode,
                im_test_api_key : im_test_api_key,
                im_test_auth_token : im_test_auth_token,
                im_test_salt : im_test_salt,
                im_test_email : im_test_email,
                im_prod_api_key : im_prod_api_key,
                im_prod_auth_token : im_prod_auth_token,
                im_prod_salt : im_prod_salt,
                im_prod_email : im_prod_email,
                task:task
            }

            $('.loading_img_instamojo').show();
            $('.instamojo_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/payment-gateway-setting/instamojo_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_instamojo').hide();
                    if(res=='success')
                    {
                        $('.instamojo_status').html("<center><div class='alert alert-success instamojo_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Instamojo Setting Data Updated Successfully.</div></center>");
                        $('.instamojo_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("payment-gateway-setup.php?main_tab=PaymentGateway&sub_tab=Instamojo");
                        });
                    }
                    else
                    {
                        $('.instamojo_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>