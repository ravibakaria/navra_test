<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt_cc   = $link->prepare("SELECT * FROM `payment_ccavenue`");
	$stmt_cc->execute();
	$result_cc = $stmt_cc->fetch();
	$count_cc = $stmt_cc->rowCount();

	$cc_is_enable = $result_cc['cc_is_enable'];
    $cc_display_name = $result_cc['cc_display_name'];
    $cc_merchant_id = $result_cc['cc_merchant_id'];
    $cc_access_code = $result_cc['cc_access_code'];
    $cc_working_key = $result_cc['cc_working_key'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">          
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <h2>CCAvenue</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-right">
                <a href="<?php echo $websiteBasePath?>/reference-documents/CCAvenue-Setup-Steps.pdf" target="_blank">
                    <span data-toggle="tooltip" data-placement="left" title="CCAvenue Refrence Document.">
                        <img src="<?php echo $websiteBasePath?>/images/file-pdf.png">
                    </span>
                </a>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
                Enable CCAvenue Payment Gateway:
            </div>
            <div class="col-xs-4 col-sm-4 col-md-1 col-lg-1">
                <input type="checkbox" class="cc_is_enable" <?php if($count_pp>0 && ($cc_is_enable=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this payment gateway then this will be available to member to make payment.
                </div>
            </div>
        </div>

        <div class="row input-row CCAvenuePaymentSettingData" <?php if($cc_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Display Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control cc_display_name" type="text" value="<?php echo @$cc_display_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This name will be displayed on page selecting payment method.
                </div>
            </div>
        </div>

        <!--        Payment gateway SETTING START        -->
        <div class="row input-row CCAvenuePaymentSettingData" <?php if($cc_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Merchant ID:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control cc_merchant_id" type="text" value="<?php echo @$cc_merchant_id;?>">
            </div>
        </div>

        <div class="row input-row CCAvenuePaymentSettingData" <?php if($cc_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Access Code:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control cc_access_code" type="text" value="<?php echo @$cc_access_code;?>">
            </div>
        </div>

        <div class="row input-row CCAvenuePaymentSettingData" <?php if($cc_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Working Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control cc_working_key" type="text" value="<?php echo @$cc_working_key;?>">
            </div>
        </div>
        <!--        SETTING END        -->
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 CCAvenue_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_CCAvenue' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><button class="btn btn-success btn-sm btn_submit_CCAvenue website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        /*    Paytm enable start   */
            $('.cc_is_enable').change(function(){
                if($(this).is(':checked'))
                {
                    var cc_is_enable = '1';
                }
                else
                {
                    var cc_is_enable = '0';
                }

                if(cc_is_enable=='1')
                {
                    $('.CCAvenuePaymentSettingData').show();
                    return false;
                }
                else
                {
                    $('.CCAvenuePaymentSettingData').hide();
                    return false;
                }    
            });
        /*     Instamojo enable end   */

        $('.btn_submit_CCAvenue').click(function(){
            if($('.cc_is_enable').is(':checked'))
            {
                var cc_is_enable = '1';
            }
            else
            {
                var cc_is_enable = '0';
            }

            var cc_display_name = $('.cc_display_name').val();
            var cc_merchant_id = $('.cc_merchant_id').val().toString();
            var cc_access_code = $('.cc_access_code').val().toString();
            var cc_working_key = $('.cc_working_key').val().toString();

            if(cc_is_enable=='1' && (cc_display_name=='' || cc_display_name==null))
            {
                $('.CCAvenue_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter display name.</div></center>");
                return false
            }

            /*********   Test Mode validations   ***********/
            if(cc_is_enable=='1' && (cc_merchant_id=='' || cc_merchant_id==null))
            {
                $('.CCAvenue_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter merchant id received from CCAvenue.</div></center>");
                return false
            }

            if(cc_is_enable=='1' && (cc_access_code=='' || cc_access_code==null))
            {
                $('.CCAvenue_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter access code received from CCAvenue.</div></center>");
                return false
            }

            if(cc_is_enable=='1' && (cc_working_key=='' || cc_working_key==null))
            {
                $('.CCAvenue_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter working key received from CCAvenue.</div></center>");
                return false
            }

            var task = "CCAvenue_payment_gateway";

            var data = {
                cc_is_enable:cc_is_enable,
                cc_display_name:cc_display_name,
                cc_merchant_id:cc_merchant_id,
                cc_access_code:cc_access_code,
                cc_working_key:cc_working_key,
                task:task
            }

            $('.loading_img_CCAvenue').show();
            $('.CCAvenue_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/payment-gateway-setting/CCAvenue_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_CCAvenue').hide();
                    if(res=='success')
                    {
                        $('.CCAvenue_status').html("<center><div class='alert alert-success CCAvenue_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> CCAvenue Setting Data Updated Successfully.</div></center>");
                        $('.CCAvenue_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("payment-gateway-setup.php?main_tab=PaymentGateway&sub_tab=CCAvenue");
                        });
                    }
                    else
                    {
                        $('.CCAvenue_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>