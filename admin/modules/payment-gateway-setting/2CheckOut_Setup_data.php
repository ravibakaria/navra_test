<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt_co   = $link->prepare("SELECT * FROM `payment_2checkout`");
	$stmt_co->execute();
	$result_co = $stmt_co->fetch();
	$count_co = $stmt_co->rowCount();

	$co_is_enable = $result_co['co_is_enable'];
    $co_display_name = $result_co['co_display_name'];
    $co_activated_mode = $result_co['co_activated_mode'];
    $co_test_account_number = $result_co['co_test_account_number'];
    $co_test_publishable_key = $result_co['co_test_publishable_key'];
    $co_test_private_key = $result_co['co_test_private_key'];
    $co_prod_account_number = $result_co['co_prod_account_number'];
    $co_prod_publishable_key = $result_co['co_prod_publishable_key'];
    $co_prod_private_key = $result_co['co_prod_private_key'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <h2>2Checkout</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-right">
                <a href="<?php echo $websiteBasePath?>/reference-documents/2Checkout-Setup-Steps.pdf" target="_blank">
                    <span data-toggle="tooltip" data-placement="left" title="2CheckOut Refrence Document.">
                        <img src="<?php echo $websiteBasePath?>/images/file-pdf.png">
                    </span>
                </a>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-9 col-sm-9 col-md-4 col-lg-4">
                Enable 2Checkout Payment Gateway:
            </div>
            <div class="col-xs-3 col-sm-3 col-md-1 col-lg-1">
                <input type="checkbox" class="co_is_enable" <?php if($count_co>0 && ($co_is_enable=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this payment gateway then this will be available to member to make payment.
                </div>
            </div>
        </div>

        <div class="row input-row CheckoutPaymentSettingData" <?php if($co_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Display Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control co_display_name" type="text" value="<?php echo @$co_display_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This name will be displayed on page selecting payment method.
                </div>
            </div>
        </div>

        <div class="row input-row CheckoutPaymentSettingData" <?php if($co_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                Activate Mode:
            </div>
            <div class="col-xs-12 col-sm-21 col-md-4 col-lg-4">
                <input type='radio' name='co_activated_mode' value='TEST' class='co_activated_mode' id='COTEST' <?php if($co_activated_mode=='TEST') { echo "checked"; } ?>>  <label for='COTEST' class='control-label'>&nbsp;Test Mode </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='radio' name='co_activated_mode' value='PROD' class='co_activated_mode' id='COPROD'<?php if($co_activated_mode=='PROD') { echo "checked"; } ?>>  <label for='COPROD' class='control-label'>&nbsp;Production/Live Mode </label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> Test mode or Production mode.
                </div>
            </div>
        </div>

        <!--        TEST MODE SETTING START        -->
        <div class="row input-row coTestModeSettingData" <?php if($co_is_enable!='1' || $co_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Account Number:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control co_test_account_number" type="text" value="<?php echo @$co_test_account_number;?>">
            </div>
        </div>

        <div class="row input-row coTestModeSettingData" <?php if($co_is_enable!='1' || $co_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Publishable Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control co_test_publishable_key" type="text" value="<?php echo @$co_test_publishable_key;?>">
            </div>
        </div>

        <div class="row input-row coTestModeSettingData" <?php if($co_is_enable!='1' || $co_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Private Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control co_test_private_key" type="text" value="<?php echo @$co_test_private_key;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->

        <!--        PRODUCTION MODE SETTING START        -->
        <div class="row input-row coProdModeSettingData" <?php if($co_is_enable!='1' || $co_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Account Number:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control co_prod_account_number" type="text" value="<?php echo @$co_prod_account_number;?>">
            </div>
        </div>

        <div class="row input-row coProdModeSettingData" <?php if($co_is_enable!='1' || $co_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Publishable Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control co_prod_publishable_key" type="text" value="<?php echo @$co_prod_publishable_key;?>">
            </div>
        </div>

        <div class="row input-row coProdModeSettingData" <?php if($co_is_enable!='1' || $co_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Private Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control co_prod_private_key" type="text" value="<?php echo @$co_prod_private_key;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 checkout_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_2checkout' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><button class="btn btn-success btn-sm btn_submit_checkout website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        /*    payumoney enable start   */
            $('.co_is_enable').change(function(){
                var co_activated_mode = $("input[name='co_activated_mode']:checked").val();
                if($(this).is(':checked'))
                {
                    var co_is_enable = '1';
                }
                else
                {
                    var co_is_enable = '0';
                }

                if(co_is_enable=='1')
                {
                    $('.CheckoutPaymentSettingData').show();

                    if(co_activated_mode=='TEST')
                    {
                        $('.coTestModeSettingData').show();
                        $('.coProdModeSettingData').hide();
                    }
                    else
                    if(co_activated_mode=='PROD')
                    {
                        $('.coProdModeSettingData').show();
                        $('.coTestModeSettingData').hide();
                    }
                    return false;
                }
                else
                {
                    $('.CheckoutPaymentSettingData').hide();
                    $('.coTestModeSettingData').hide();
                    $('.coProdModeSettingData').hide();
                    return false;
                }    
            });
        /*     Instamojo enable end   */

        /*    Transaction mode activation  */
        $('.co_activated_mode').change(function(){
            var co_activated_mode = $("input[name='co_activated_mode']:checked"). val();
            if(co_activated_mode=='TEST')
            {
                $('.coTestModeSettingData').show();
                $('.coProdModeSettingData').hide();
            }
            else
            if(co_activated_mode=='PROD')
            {
                $('.coProdModeSettingData').show();
                $('.coTestModeSettingData').hide();
            }
        });

        $('.btn_submit_checkout').click(function(){
            if($('.co_is_enable').is(':checked'))
            {
                var co_is_enable = '1';
            }
            else
            {
                var co_is_enable = '0';
            }

            var co_display_name = $('.co_display_name').val();
            var co_activated_mode = $("input[name='co_activated_mode']:checked"). val();
            if(co_activated_mode=='' || co_activated_mode==null || co_activated_mode=='undefined')
            {
                co_activated_mode = '0';
            }

            var co_test_account_number = $('.co_test_account_number').val().toString();
            var co_test_publishable_key = $('.co_test_publishable_key').val().toString();
            var co_test_private_key = $('.co_test_private_key').val().toString();
            var co_prod_account_number = $('.co_prod_account_number').val().toString();
            var co_prod_publishable_key = $('.co_prod_publishable_key').val().toString();
            var co_prod_private_key = $('.co_prod_private_key').val().toString();

            if(co_is_enable=='1' && (co_display_name=='' || co_display_name==null))
            {
                $('.checkout_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter display name.</div></center>");
                return false
            }

            if(co_is_enable=='1' && (co_activated_mode=='' || co_activated_mode==null || co_activated_mode=='0'))
            {
                $('.checkout_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please select activation mode.</div></center>");
                return false
            }

            /*********   Test Mode validations   ***********/
            if(co_activated_mode=='TEST' && (co_test_account_number=='' || co_test_account_number==null))
            {
                $('.checkout_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter 2checkout test account number .</div></center>");
                return false
            }

            if(co_activated_mode=='TEST' && (co_test_publishable_key=='' || co_test_publishable_key==null))
            {
                $('.checkout_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter 2checkout test mode publishable key.</div></center>");
                return false
            }

            if(co_activated_mode=='TEST' && (co_test_private_key=='' || co_test_private_key==null))
            {
                $('.checkout_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter 2checkout test mode private key.</div></center>");
                return false
            }

            /***************   Production mode validations   **************/
            if(co_activated_mode=='PROD' && (co_prod_account_number=='' || co_prod_account_number==null))
            {
                $('.checkout_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter 2checkout production mode account number.</div></center>");
                return false
            }

            if(co_activated_mode=='PROD' && (co_prod_publishable_key=='' || co_prod_publishable_key==null))
            {
                $('.checkout_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter 2checkout production mode publishable key.</div></center>");
                return false
            }

            if(co_activated_mode=='PROD' && (co_prod_private_key=='' || co_prod_private_key==null))
            {
                $('.checkout_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter 2checkout production mode private key.</div></center>");
                return false
            }

            var task = "2checkout_payment_gateway";

            var data = {
                co_is_enable:co_is_enable,
                co_display_name:co_display_name,
                co_activated_mode:co_activated_mode,
                co_test_account_number:co_test_account_number,
                co_test_publishable_key:co_test_publishable_key,
                co_test_private_key:co_test_private_key,
                co_prod_account_number:co_prod_account_number,
                co_prod_publishable_key:co_prod_publishable_key,
                co_prod_private_key:co_prod_private_key,
                task:task
            }

            $('.loading_img_2checkout').show();
            $('.checkout_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/payment-gateway-setting/chekout_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_2checkout').hide();
                    if(res=='success')
                    {
                        $('.checkout_status').html("<center><div class='alert alert-success checkout_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> 2CheckOut payment Setting Data Updated Successfully.</div></center>");
                        $('.checkout_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("payment-gateway-setup.php?main_tab=PaymentGateway&sub_tab=2CheckOut");
                        });
                    }
                    else
                    {
                        $('.checkout_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>