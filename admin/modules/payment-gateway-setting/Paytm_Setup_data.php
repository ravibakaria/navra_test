<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt_pt   = $link->prepare("SELECT * FROM `payment_paytm`");
	$stmt_pt->execute();
	$result_pt = $stmt_pt->fetch();
	$count_pt = $stmt_pt->rowCount();

	$pt_is_enable = $result_pt['pt_is_enable'];
    $pt_display_name = $result_pt['pt_display_name'];
    $pt_activated_mode = $result_pt['pt_activated_mode'];
    $pt_test_merchant_id = $result_pt['pt_test_merchant_id'];
    $pt_test_merchant_key = $result_pt['pt_test_merchant_key'];
    $pt_test_website_name = $result_pt['pt_test_website_name'];
    $pt_test_industry_type = $result_pt['pt_test_industry_type'];
    $pt_prod_merchant_id = $result_pt['pt_prod_merchant_id'];
    $pt_prod_merchant_key = $result_pt['pt_prod_merchant_key'];
    $pt_prod_website_name = $result_pt['pt_prod_website_name'];
    $pt_prod_industry_type = $result_pt['pt_prod_industry_type'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">           
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <h2>Paytm</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-right">
                <a href="<?php echo $websiteBasePath?>/reference-documents/Paytm-Setup-Steps.pdf" target="_blank">
                    <span data-toggle="tooltip" data-placement="left" title="Paytm Refrence Document.">
                        <img src="<?php echo $websiteBasePath?>/images/file-pdf.png">
                    </span>
                </a>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
                Enable Paytm Payment Gateway:
            </div>
            <div class="col-xs-4 col-sm-4 col-md-1 col-lg-1">
                <input type="checkbox" class="pt_is_enable" <?php if($count_pt>0 && ($pt_is_enable=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this payment gateway then this will be available to member to make payment.
                </div>
            </div>
        </div>

        <div class="row input-row PaytmPaymentSettingData" <?php if($pt_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Display Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pt_display_name" type="text" value="<?php echo @$pt_display_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This name will be displayed on page selecting payment method.
                </div>
            </div>
        </div>

        <div class="row input-row PaytmPaymentSettingData" <?php if($pt_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                Activate Mode:
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <input type='radio' name='pt_activated_mode' value='TEST' class='pt_activated_mode' id='PTTEST' <?php if($pt_activated_mode=='TEST') { echo "checked"; } ?>>  <label for='PTTEST' class='control-label'>&nbsp;Test Mode </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='radio' name='pt_activated_mode' value='PROD' class='pt_activated_mode' id='PTPROD'<?php if($pt_activated_mode=='PROD') { echo "checked"; } ?>>  <label for='PTPROD' class='control-label'>&nbsp;Production/Live Mode </label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> Test mode or Production mode.
                </div>
            </div>
        </div>

        <!--        TEST MODE SETTING START        -->
        <div class="row input-row ptTestModeSettingData" <?php if($pt_is_enable!='1' || $pt_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Merchant ID:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pt_test_merchant_id" type="text" value="<?php echo @$pt_test_merchant_id;?>">
            </div>
        </div>

        <div class="row input-row ptTestModeSettingData" <?php if($pt_is_enable!='1' || $pt_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Merchant Secret Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pt_test_merchant_key" type="text" value="<?php echo @$pt_test_merchant_key;?>">
            </div>
        </div>

        <div class="row input-row ptTestModeSettingData" <?php if($pt_is_enable!='1' || $pt_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Website Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pt_test_website_name" type="text" value="<?php echo @$pt_test_website_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>For EX: </strong> use <b>WEBSTAGING</b> for test mode.
                </div>
            </div>
        </div>

        <div class="row input-row ptTestModeSettingData" <?php if($pt_is_enable!='1' || $pt_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Industry Type:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pt_test_industry_type" type="text" value="<?php echo @$pt_test_industry_type;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>For EX: </strong> use <b>Retail</b> for test mode.
                </div>
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->

        <!--        PRODUCTION MODE SETTING START        -->
        <div class="row input-row ptProdModeSettingData" <?php if($pt_is_enable!='1' || $pt_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Merchant ID:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pt_prod_merchant_id" type="text" value="<?php echo @$pt_prod_merchant_id;?>">
            </div>
        </div>

        <div class="row input-row ptProdModeSettingData" <?php if($pt_is_enable!='1' || $pt_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Merchant Secret Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pt_prod_merchant_key" type="text" value="<?php echo @$pt_prod_merchant_key;?>">
            </div>
        </div>

        <div class="row input-row ptProdModeSettingData" <?php if($pt_is_enable!='1' || $pt_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Website Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pt_prod_website_name" type="text" value="<?php echo @$pt_prod_website_name;?>">
            </div>
        </div>

        <div class="row input-row ptProdModeSettingData" <?php if($pt_is_enable!='1' || $pt_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Industry Type:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pt_prod_industry_type" type="text" value="<?php echo @$pt_prod_industry_type;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 paytm_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_paytm' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><button class="btn btn-success btn-sm btn_submit_paytm website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        /*    Paytm enable start   */
            $('.pt_is_enable').change(function(){
                var pt_activated_mode = $("input[name='pt_activated_mode']:checked"). val();
                if($(this).is(':checked'))
                {
                    var pt_is_enable = '1';
                }
                else
                {
                    var pt_is_enable = '0';
                }

                if(pt_is_enable=='1')
                {
                    $('.PaytmPaymentSettingData').show();

                    if(pt_activated_mode=='TEST')
                    {
                        $('.ptTestModeSettingData').show();
                        $('.ptProdModeSettingData').hide();
                    }
                    else
                    if(pt_activated_mode=='PROD')
                    {
                        $('.ptProdModeSettingData').show();
                        $('.ptTestModeSettingData').hide();
                    }
                    return false;
                }
                else
                {
                    $('.PaytmPaymentSettingData').hide();
                    $('.ptTestModeSettingData').hide();
                    $('.ptProdModeSettingData').hide();
                    return false;
                }    
            });
        /*     Instamojo enable end   */

        /*    Transaction mode activation  */
        $('.pt_activated_mode').change(function(){
            var pt_activated_mode = $("input[name='pt_activated_mode']:checked"). val();
            if(pt_activated_mode=='TEST')
            {
                $('.ptTestModeSettingData').show();
                $('.ptProdModeSettingData').hide();
            }
            else
            if(pt_activated_mode=='PROD')
            {
                $('.ptProdModeSettingData').show();
                $('.ptTestModeSettingData').hide();
            }
        });

        $('.btn_submit_paytm').click(function(){
            if($('.pt_is_enable').is(':checked'))
            {
                var pt_is_enable = '1';
            }
            else
            {
                var pt_is_enable = '0';
            }

            var pt_display_name = $('.pt_display_name').val();
            var pt_activated_mode = $("input[name='pt_activated_mode']:checked"). val();
            if(pt_activated_mode=='' || pt_activated_mode==null || pt_activated_mode=='undefined')
            {
                pt_activated_mode = '0';
            }

            var pt_test_merchant_id = $('.pt_test_merchant_id').val().toString();
            var pt_test_merchant_key = $('.pt_test_merchant_key').val().toString();
            var pt_test_website_name = $('.pt_test_website_name').val().toString();
            var pt_test_industry_type = $('.pt_test_industry_type').val().toString();
            var pt_prod_merchant_id = $('.pt_prod_merchant_id').val().toString();
            var pt_prod_merchant_key = $('.pt_prod_merchant_key').val().toString();
            var pt_prod_website_name = $('.pt_prod_website_name').val().toString();
            var pt_prod_industry_type = $('.pt_prod_industry_type').val().toString();

            if(pt_is_enable=='1' && (pt_display_name=='' || pt_display_name==null))
            {
                $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter display name.</div></center>");
                return false
            }

            if(pt_is_enable=='1' && (pt_activated_mode=='' || pt_activated_mode==null || pt_activated_mode=='0'))
            {
                $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please select activation mode.</div></center>");
                return false
            }

            /*********   Test Mode validations   ***********/
            if(pt_activated_mode=='TEST' && (pt_test_merchant_id=='' || pt_test_merchant_id==null))
            {
                $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter paytm merchant id for test mode.</div></center>");
                return false
            }

            if(pt_activated_mode=='TEST' && (pt_test_merchant_key=='' || pt_test_merchant_key==null))
            {
                $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter paytm merchant key for test mode.</div></center>");
                return false
            }

            if(pt_activated_mode=='TEST' && (pt_test_website_name=='' || pt_test_website_name==null))
            {
                $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter paytm website name for test mode.</div></center>");
                return false
            }

            if(pt_activated_mode=='TEST' && (pt_test_industry_type=='' || pt_test_industry_type==null))
            {
                $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter paytm industry type for test mode.</div></center>");
                return false
            }

            /***************   Production mode validations   **************/
            if(pt_activated_mode=='PROD' && (pt_prod_merchant_id=='' || pt_prod_merchant_id==null))
            {
                $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter paytm merchant id for production mode.</div></center>");
                return false
            }

            if(pt_activated_mode=='PROD' && (pt_prod_merchant_key=='' || pt_prod_merchant_key==null))
            {
                $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter paytm merchant key for production mode.</div></center>");
                return false
            }

            if(pt_activated_mode=='PROD' && (pt_prod_website_name=='' || pt_prod_website_name==null))
            {
                $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter paytm website name for production mode.</div></center>");
                return false
            }

            if(pt_activated_mode=='PROD' && (pt_prod_industry_type=='' || pt_prod_industry_type==null))
            {
                $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter paytm industry type for production mode.</div></center>");
                return false
            }

            var task = "paytm_payment_gateway";

            var data = {
                pt_is_enable:pt_is_enable,
                pt_display_name:pt_display_name,
                pt_activated_mode:pt_activated_mode,
                pt_test_merchant_id:pt_test_merchant_id,
                pt_test_merchant_key:pt_test_merchant_key,
                pt_test_website_name:pt_test_website_name,
                pt_test_industry_type:pt_test_industry_type,
                pt_prod_merchant_id:pt_prod_merchant_id,
                pt_prod_merchant_key:pt_prod_merchant_key,
                pt_prod_website_name:pt_prod_website_name,
                pt_prod_industry_type:pt_prod_industry_type,
                task:task
            }

            $('.loading_img_paytm').show();
            $('.paytm_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/payment-gateway-setting/paytm_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_paytm').hide();
                    if(res=='success')
                    {
                        $('.paytm_status').html("<center><div class='alert alert-success paytm_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Paytm Setting Data Updated Successfully.</div></center>");
                        $('.paytm_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("payment-gateway-setup.php?main_tab=PaymentGateway&sub_tab=Paytm");
                        });
                    }
                    else
                    {
                        $('.paytm_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>