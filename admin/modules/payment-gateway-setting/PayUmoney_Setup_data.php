<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt_pt   = $link->prepare("SELECT * FROM `payment_payumoney`");
	$stmt_pt->execute();
	$result_pt = $stmt_pt->fetch();
	$count_pt = $stmt_pt->rowCount();

	$pm_is_enable = $result_pt['pm_is_enable'];
    $pm_display_name = $result_pt['pm_display_name'];
    $pm_activated_mode = $result_pt['pm_activated_mode'];
    $pm_test_merchant_key = $result_pt['pm_test_merchant_key'];
    $pm_test_merchant_salt = $result_pt['pm_test_merchant_salt'];
    $pm_test_merchant_auth_header = $result_pt['pm_test_merchant_auth_header'];
    $pm_prod_merchant_key = $result_pt['pm_prod_merchant_key'];
    $pm_prod_merchant_salt = $result_pt['pm_prod_merchant_salt'];
    $pm_prod_merchant_auth_header = $result_pt['pm_prod_merchant_auth_header'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">            
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <h2>PayUmoney</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-right">
                <a href="<?php echo $websiteBasePath?>/reference-documents/PayUMoney-Setup-Steps.pdf" target="_blank">
                    <span data-toggle="tooltip" data-placement="left" title="Pay U Money Refrence Document.">
                        <img src="<?php echo $websiteBasePath?>/images/file-pdf.png">
                    </span>
                </a>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-9 col-sm-9 col-md-4 col-lg-4">
                Enable PayU Money Payment Gateway:
            </div>
            <div class="col-xs-3 col-sm-3 col-md-1 col-lg-1">
                <input type="checkbox" class="pm_is_enable" <?php if($count_pt>0 && ($pm_is_enable=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this payment gateway then this will be available to member to make payment.
                </div>
            </div>
        </div>

        <div class="row input-row PayUmoneyPaymentSettingData" <?php if($pm_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Display Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pm_display_name" type="text" value="<?php echo @$pm_display_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This name will be displayed on page selecting payment method.
                </div>
            </div>
        </div>

        <div class="row input-row PayUmoneyPaymentSettingData" <?php if($pm_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                Activate Mode:
            </div>
            <div class="col-xs-12 col-sm-21 col-md-4 col-lg-4">
                <input type='radio' name='pm_activated_mode' value='TEST' class='pm_activated_mode' id='PUTEST' <?php if($pm_activated_mode=='TEST') { echo "checked"; } ?>>  <label for='PUTEST' class='control-label'>&nbsp;Test Mode </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='radio' name='pm_activated_mode' value='PROD' class='pm_activated_mode' id='PUPROD'<?php if($pm_activated_mode=='PROD') { echo "checked"; } ?>>  <label for='PUPROD' class='control-label'>&nbsp;Production/Live Mode </label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> Test mode or Production mode.
                </div>
            </div>
        </div>

        <!--        TEST MODE SETTING START        -->
        <div class="row input-row pmTestModeSettingData" <?php if($pm_is_enable!='1' || $pm_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pm_test_merchant_key" type="text" value="<?php echo @$pm_test_merchant_key;?>">
            </div>
        </div>

        <div class="row input-row pmTestModeSettingData" <?php if($pm_is_enable!='1' || $pm_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Salt:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pm_test_merchant_salt" type="text" value="<?php echo @$pm_test_merchant_salt;?>">
            </div>
        </div>

        <div class="row input-row pmTestModeSettingData" <?php if($pm_is_enable!='1' || $pm_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Auth Header <i>(Optional)</i>:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pm_test_merchant_auth_header" type="text" value="<?php echo @$pm_test_merchant_auth_header;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->

        <!--        PRODUCTION MODE SETTING START        -->
        <div class="row input-row pmProdModeSettingData" <?php if($pm_is_enable!='1' || $pm_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Merchant Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pm_prod_merchant_key" type="text" value="<?php echo @$pm_prod_merchant_key;?>">
            </div>
        </div>

        <div class="row input-row pmProdModeSettingData" <?php if($pm_is_enable!='1' || $pm_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Merchant Salt:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pm_prod_merchant_salt" type="text" value="<?php echo @$pm_prod_merchant_salt;?>">
            </div>
        </div>

        <div class="row input-row pmProdModeSettingData" <?php if($pm_is_enable!='1' || $pm_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Auth Header <i>(Optional)</i>:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pm_prod_merchant_auth_header" type="text" value="<?php echo @$pm_prod_merchant_auth_header;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 payumoney_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_payumoney' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><button class="btn btn-success btn-sm btn_submit_payumoney website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        /*    payumoney enable start   */
            $('.pm_is_enable').change(function(){
                var pm_activated_mode = $("input[name='pm_activated_mode']:checked"). val();
                if($(this).is(':checked'))
                {
                    var pm_is_enable = '1';
                }
                else
                {
                    var pm_is_enable = '0';
                }

                if(pm_is_enable=='1')
                {
                    $('.PayUmoneyPaymentSettingData').show();

                    if(pm_activated_mode=='TEST')
                    {
                        $('.pmTestModeSettingData').show();
                        $('.pmProdModeSettingData').hide();
                    }
                    else
                    if(pm_activated_mode=='PROD')
                    {
                        $('.pmProdModeSettingData').show();
                        $('.pmTestModeSettingData').hide();
                    }
                    return false;
                }
                else
                {
                    $('.PayUmoneyPaymentSettingData').hide();
                    $('.pmTestModeSettingData').hide();
                    $('.pmProdModeSettingData').hide();
                    return false;
                }    
            });
        /*     Instamojo enable end   */

        /*    Transaction mode activation  */
        $('.pm_activated_mode').change(function(){
            var pm_activated_mode = $("input[name='pm_activated_mode']:checked"). val();
            if(pm_activated_mode=='TEST')
            {
                $('.pmTestModeSettingData').show();
                $('.pmProdModeSettingData').hide();
            }
            else
            if(pm_activated_mode=='PROD')
            {
                $('.pmProdModeSettingData').show();
                $('.pmTestModeSettingData').hide();
            }
        });

        $('.btn_submit_payumoney').click(function(){
            if($('.pm_is_enable').is(':checked'))
            {
                var pm_is_enable = '1';
            }
            else
            {
                var pm_is_enable = '0';
            }

            var pm_display_name = $('.pm_display_name').val();
            var pm_activated_mode = $("input[name='pm_activated_mode']:checked"). val();
            if(pm_activated_mode=='' || pm_activated_mode==null || pm_activated_mode=='undefined')
            {
                pm_activated_mode = '0';
            }

            var pm_test_merchant_key = $('.pm_test_merchant_key').val().toString();
            var pm_test_merchant_salt = $('.pm_test_merchant_salt').val().toString();
            var pm_test_merchant_auth_header = $('.pm_test_merchant_auth_header').val().toString();
            var pt_test_industry_type = $('.pt_test_industry_type').val().toString();
            var pm_prod_merchant_key = $('.pm_prod_merchant_key').val().toString();
            var pm_prod_merchant_salt = $('.pm_prod_merchant_salt').val().toString();
            var pm_prod_merchant_auth_header = $('.pm_prod_merchant_auth_header').val().toString();
            var pt_prod_industry_type = $('.pt_prod_industry_type').val().toString();

            if(pm_is_enable=='1' && (pm_display_name=='' || pm_display_name==null))
            {
                $('.payumoney_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter display name.</div></center>");
                return false
            }

            if(pm_is_enable=='1' && (pm_activated_mode=='' || pm_activated_mode==null || pm_activated_mode=='0'))
            {
                $('.payumoney_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please select activation mode.</div></center>");
                return false
            }

            /*********   Test Mode validations   ***********/
            if(pm_activated_mode=='TEST' && (pm_test_merchant_key=='' || pm_test_merchant_key==null))
            {
                $('.payumoney_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter payumoney test key .</div></center>");
                return false
            }

            if(pm_activated_mode=='TEST' && (pm_test_merchant_salt=='' || pm_test_merchant_salt==null))
            {
                $('.payumoney_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter payumoney test salt.</div></center>");
                return false
            }

            /***************   Production mode validations   **************/
            if(pm_activated_mode=='PROD' && (pm_prod_merchant_key=='' || pm_prod_merchant_key==null))
            {
                $('.payumoney_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter payumoney merchant key.</div></center>");
                return false
            }

            if(pm_activated_mode=='PROD' && (pm_prod_merchant_salt=='' || pm_prod_merchant_salt==null))
            {
                $('.payumoney_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter payumoney merchant salt.</div></center>");
                return false
            }

            var task = "payumoney_payment_gateway";

            var data = {
                pm_is_enable:pm_is_enable,
                pm_display_name:pm_display_name,
                pm_activated_mode:pm_activated_mode,
                pm_test_merchant_key:pm_test_merchant_key,
                pm_test_merchant_salt:pm_test_merchant_salt,
                pm_test_merchant_auth_header:pm_test_merchant_auth_header,
                pm_prod_merchant_key:pm_prod_merchant_key,
                pm_prod_merchant_salt:pm_prod_merchant_salt,
                pm_prod_merchant_auth_header:pm_prod_merchant_auth_header,
                task:task
            }

            $('.loading_img_payumoney').show();
            $('.payumoney_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/payment-gateway-setting/payumoney_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_payumoney').hide();
                    if(res=='success')
                    {
                        $('.payumoney_status').html("<center><div class='alert alert-success payumoney_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> PayU Money Setting Data Updated Successfully.</div></center>");
                        $('.payumoney_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("payment-gateway-setup.php?main_tab=PaymentGateway&sub_tab=PayUmoney");
                        });
                    }
                    else
                    {
                        $('.payumoney_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>