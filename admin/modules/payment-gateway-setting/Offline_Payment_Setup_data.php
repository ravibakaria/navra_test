<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt_im   = $link->prepare("SELECT * FROM `payment_offline`");
	$stmt_im->execute();
	$result_im = $stmt_im->fetch();
	$count_im=$stmt_im->rowCount();

	$off_is_enable = $result_im['off_is_enable'];
    $off_display_name = $result_im['off_display_name'];
    $off_pay_to_text = $result_im['off_pay_to_text'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
                Enable Offline Payment :
            </div>
            <div class="col-xs-4 col-sm-4 col-md-1 col-lg-1">
                <input type="checkbox" class="off_is_enable" <?php if($count_im>0 && ($off_is_enable=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this payment then offline payment will be available to member to make payment.
                </div>
            </div>
        </div>

        <div class="row input-row OfflinePaymentSettingData" <?php if($off_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Display Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control off_display_name" type="text" value="<?php echo @$off_display_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This name will be displayed on page selecting payment method.
                </div>
            </div>
        </div>

        <div class="row input-row OfflinePaymentSettingData" <?php if($off_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                Pay To Text:
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <textarea class="form-control off_pay_to_text" rows="15" placeholder="Please send payment details along with this payment summary to following address................ Address details for receiving payment documents"><?php echo @$off_pay_to_text;?></textarea>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This text will be displayed on members order summary. You can add details where member have to send its payment details along with payment summary.
                </div>
                <b>Preview: </b>
                <div class='alert alert-default pay-to-text-preview' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <?php  echo @$off_pay_to_text; ?>
                </div>
            </div>
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 offline_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_offline' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><button class="btn btn-success btn-sm btn_submit_offline website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $('.off_pay_to_text').keyup(function (e) {
            $('.pay-to-text-preview').html($(this).val());
        });
        /*    Offline enable start   */
            $('.off_is_enable').change(function(){

                if($(this).is(':checked'))
                {
                    var off_is_enable = '1';
                }
                else
                {
                    var off_is_enable = '0';
                }

                if(off_is_enable=='1')
                {
                    $('.OfflinePaymentSettingData').show();
                    return false;
                }
                else
                {
                    $('.OfflinePaymentSettingData').hide();
                    return false;
                } 


            });
        /*     Offline enable end   */        

        $('.btn_submit_offline').click(function(){
            if($('.off_is_enable').is(':checked'))
            {
                var off_is_enable = '1';
            }
            else
            {
                var off_is_enable = '0';
            }

            var off_display_name = $('.off_display_name').val();
            var off_pay_to_text = $('.off_pay_to_text').val();
            var task = "offline_payment_gateway";

            if(off_is_enable=='1' && (off_display_name=='' || off_display_name==null))
            {
                $('.offline_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter display name.</div></center>");
                return false
            }

            if(off_is_enable=='1' && (off_pay_to_text=='' || off_pay_to_text==null))
            {
                $('.offline_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter pay to text.</div></center>");
                return false
            }

            
            var data = {
                off_is_enable : off_is_enable,
                off_display_name : off_display_name,
                off_pay_to_text : off_pay_to_text,
                task:task
            }

            $('.loading_img_offline').show();
            $('.offline_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/payment-gateway-setting/offline_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_offline').hide();
                    if(res=='success')
                    {
                        $('.offline_status').html("<center><div class='alert alert-success offline_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Offline Payment Setting Data Updated Successfully.</div></center>");
                        $('.offline_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("payment-gateway-setup.php?main_tab=PaymentGateway&sub_tab=Offline");
                        });
                    }
                    else
                    {
                        $('.offline_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>