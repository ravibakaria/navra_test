<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt_rp   = $link->prepare("SELECT * FROM `payment_razorpay`");
	$stmt_rp->execute();
	$result_rp = $stmt_rp->fetch();
	$count_rp = $stmt_rp->rowCount();

	$rp_is_enable = $result_rp['rp_is_enable'];
    $rp_display_name = $result_rp['rp_display_name'];
    $rp_activated_mode = $result_rp['rp_activated_mode'];
    $rp_test_key_id = $result_rp['rp_test_key_id'];
    $rp_test_key_secret = $result_rp['rp_test_key_secret'];
    $rp_prod_key_id = $result_rp['rp_prod_key_id'];
    $rp_prod_key_secret = $result_rp['rp_prod_key_secret'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <h2>Razorpay</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-right">
                <a href="<?php echo $websiteBasePath?>/reference-documents/Razorpay-Setup-Steps.pdf" target="_blank">
                    <span data-toggle="tooltip" data-placement="left" title="Razorpay Refrence Document.">
                        <img src="<?php echo $websiteBasePath?>/images/file-pdf.png">
                    </span>
                </a>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-9 col-sm-9 col-md-4 col-lg-4">
                Enable Razorpay Payment Gateway:
            </div>
            <div class="col-xs-3 col-sm-3 col-md-1 col-lg-1">
                <input type="checkbox" class="rp_is_enable" <?php if($count_pt>0 && ($rp_is_enable=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this payment gateway then this will be available to member to make payment.
                </div>
            </div>
        </div>

        <div class="row input-row RazorpayPaymentSettingData" <?php if($rp_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Display Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control rp_display_name" type="text" value="<?php echo @$rp_display_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This name will be displayed on page selecting payment method.
                </div>
            </div>
        </div>

        <div class="row input-row RazorpayPaymentSettingData" <?php if($rp_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                Activate Mode:
            </div>
            <div class="col-xs-12 col-sm-21 col-md-4 col-lg-4">
                <input type='radio' name='rp_activated_mode' value='TEST' class='rp_activated_mode' id='RPTEST' <?php if($rp_activated_mode=='TEST') { echo "checked"; } ?>>  <label for='RPTEST' class='control-label'>&nbsp;Test Mode </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='radio' name='rp_activated_mode' value='PROD' class='rp_activated_mode' id='RPPROD'<?php if($rp_activated_mode=='PROD') { echo "checked"; } ?>>  <label for='RPPROD' class='control-label'>&nbsp;Production/Live Mode </label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> Test mode or Production mode.
                </div>
            </div>
        </div>

        <!--        TEST MODE SETTING START        -->
        <div class="row input-row rpTestModeSettingData" <?php if($rp_is_enable!='1' || $rp_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Key Id:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control rp_test_key_id" type="text" value="<?php echo @$rp_test_key_id;?>">
            </div>
        </div>

        <div class="row input-row rpTestModeSettingData" <?php if($rp_is_enable!='1' || $rp_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Key Secret:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control rp_test_key_secret" type="text" value="<?php echo @$rp_test_key_secret;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->

        <!--        PRODUCTION MODE SETTING START        -->
        <div class="row input-row rpProdModeSettingData" <?php if($rp_is_enable!='1' || $rp_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Key Id:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control rp_prod_key_id" type="text" value="<?php echo @$rp_prod_key_id;?>">
            </div>
        </div>

        <div class="row input-row rpProdModeSettingData" <?php if($rp_is_enable!='1' || $rp_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Key Secret:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control rp_prod_key_secret" type="text" value="<?php echo @$rp_prod_key_secret;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 razorpay_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_razorpay' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><button class="btn btn-success btn-sm btn_submit_razorpay website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        /*    payumoney enable start   */
            $('.rp_is_enable').change(function(){
                var rp_activated_mode = $("input[name='rp_activated_mode']:checked"). val();
                if($(this).is(':checked'))
                {
                    var rp_is_enable = '1';
                }
                else
                {
                    var rp_is_enable = '0';
                }

                if(rp_is_enable=='1')
                {
                    $('.RazorpayPaymentSettingData').show();

                    if(rp_activated_mode=='TEST')
                    {
                        $('.rpTestModeSettingData').show();
                        $('.rpProdModeSettingData').hide();
                    }
                    else
                    if(rp_activated_mode=='PROD')
                    {
                        $('.rpProdModeSettingData').show();
                        $('.rpTestModeSettingData').hide();
                    }
                    return false;
                }
                else
                {
                    $('.RazorpayPaymentSettingData').hide();
                    $('.rpTestModeSettingData').hide();
                    $('.rpProdModeSettingData').hide();
                    return false;
                }    
            });
        /*     Instamojo enable end   */

        /*    Transaction mode activation  */
        $('.rp_activated_mode').change(function(){
            var rp_activated_mode = $("input[name='rp_activated_mode']:checked"). val();
            if(rp_activated_mode=='TEST')
            {
                $('.rpTestModeSettingData').show();
                $('.rpProdModeSettingData').hide();
            }
            else
            if(rp_activated_mode=='PROD')
            {
                $('.rpProdModeSettingData').show();
                $('.rpTestModeSettingData').hide();
            }
        });

        $('.btn_submit_razorpay').click(function(){
            if($('.rp_is_enable').is(':checked'))
            {
                var rp_is_enable = '1';
            }
            else
            {
                var rp_is_enable = '0';
            }

            var rp_display_name = $('.rp_display_name').val();
            var rp_activated_mode = $("input[name='rp_activated_mode']:checked"). val();
            if(rp_activated_mode=='' || rp_activated_mode==null || rp_activated_mode=='undefined')
            {
                rp_activated_mode = '0';
            }

            var rp_test_key_id = $('.rp_test_key_id').val().toString();
            var rp_test_key_secret = $('.rp_test_key_secret').val().toString();
            var rp_prod_key_id = $('.rp_prod_key_id').val().toString();
            var rp_prod_key_secret = $('.rp_prod_key_secret').val().toString();

            if(rp_is_enable=='1' && (rp_display_name=='' || rp_display_name==null))
            {
                $('.razorpay_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter display name.</div></center>");
                return false
            }

            if(rp_is_enable=='1' && (rp_activated_mode=='' || rp_activated_mode==null || rp_activated_mode=='0'))
            {
                $('.razorpay_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please choose activation mode.</div></center>");
                return false
            }

            /*********   Test Mode validations   ***********/
            if(rp_activated_mode=='TEST' && (rp_test_key_id=='' || rp_test_key_id==null))
            {
                $('.razorpay_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter razorpay test mode key id .</div></center>");
                return false
            }

            if(rp_activated_mode=='TEST' && (rp_test_key_secret=='' || rp_test_key_secret==null))
            {
                $('.razorpay_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter razorpay test mode key secret.</div></center>");
                return false
            }

            /***************   Production mode validations   **************/
            if(rp_activated_mode=='PROD' && (rp_prod_key_id=='' || rp_prod_key_id==null))
            {
                $('.razorpay_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter razorpay live mode key id.</div></center>");
                return false
            }

            if(rp_activated_mode=='PROD' && (rp_prod_key_secret=='' || rp_prod_key_secret==null))
            {
                $('.razorpay_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter razorpay live mode key secret.</div></center>");
                return false
            }

            var task = "razorpay_payment_gateway";

            var data = {
                rp_is_enable:rp_is_enable,
                rp_display_name:rp_display_name,
                rp_activated_mode:rp_activated_mode,
                rp_test_key_id:rp_test_key_id,
                rp_test_key_secret:rp_test_key_secret,
                rp_prod_key_id:rp_prod_key_id,
                rp_prod_key_secret:rp_prod_key_secret,
                task:task
            }

            $('.loading_img_razorpay').show();
            $('.razorpay_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/payment-gateway-setting/razorpay_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_razorpay').hide();
                    if(res=='success')
                    {
                        $('.razorpay_status').html("<center><div class='alert alert-success razorpay_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Razorpay Setting Data Updated Successfully.</div></center>");
                        $('.razorpay_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("payment-gateway-setup.php?main_tab=PaymentGateway&sub_tab=Razorpay");
                        });
                    }
                    else
                    {
                        $('.razorpay_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>