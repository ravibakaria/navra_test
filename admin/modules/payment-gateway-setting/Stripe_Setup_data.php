<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt_sp   = $link->prepare("SELECT * FROM `payment_stripe`");
	$stmt_sp->execute();
	$result_sp = $stmt_sp->fetch();
	$count_sp = $stmt_sp->rowCount();

	$sp_is_enable = $result_sp['sp_is_enable'];
    $sp_display_name = $result_sp['sp_display_name'];
    $sp_activated_mode = $result_sp['sp_activated_mode'];
    $sp_test_publishable_key = $result_sp['sp_test_publishable_key'];
    $sp_test_secret_key = $result_sp['sp_test_secret_key'];
    $sp_prod_publishable_key = $result_sp['sp_prod_publishable_key'];
    $sp_prod_secret_key = $result_sp['sp_prod_secret_key'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <h2>Stripe</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-right">
                <a href="<?php echo $websiteBasePath?>/reference-documents/Stripe-Setup-Steps.pdf" target="_blank">
                    <span data-toggle="tooltip" data-placement="left" title="Stripe Refrence Document.">
                        <img src="<?php echo $websiteBasePath?>/images/file-pdf.png">
                    </span>
                </a>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-9 col-sm-9 col-md-4 col-lg-4">
                Enable Stripe Payment Gateway:
            </div>
            <div class="col-xs-3 col-sm-3 col-md-1 col-lg-1">
                <input type="checkbox" class="sp_is_enable" <?php if($count_sp>0 && ($sp_is_enable=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this payment gateway then this will be available to member to make payment.
                </div>
                <div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This payment gateway will be available only when amount is greater than 100(INR).
                </div>
            </div>
        </div>

        <div class="row input-row StripePaymentSettingData" <?php if($sp_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Display Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control sp_display_name" type="text" value="<?php echo @$sp_display_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This name will be displayed on page selecting payment method.
                </div>
            </div>
        </div>

        <div class="row input-row StripePaymentSettingData" <?php if($sp_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                Activate Mode:
            </div>
            <div class="col-xs-12 col-sm-21 col-md-4 col-lg-4">
                <input type='radio' name='sp_activated_mode' value='TEST' class='sp_activated_mode' id='SPTEST' <?php if($sp_activated_mode=='TEST') { echo "checked"; } ?>>  <label for='SPTEST' class='control-label'>&nbsp;Test Mode </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='radio' name='sp_activated_mode' value='PROD' class='sp_activated_mode' id='SPPROD'<?php if($sp_activated_mode=='PROD') { echo "checked"; } ?>>  <label for='SPPROD' class='control-label'>&nbsp;Production/Live Mode </label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> Test mode or Production mode.
                </div>
            </div>
        </div>

        <!--        TEST MODE SETTING START        -->
        <div class="row input-row spTestModeSettingData" <?php if($sp_is_enable!='1' || $sp_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Publishable Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control sp_test_publishable_key" type="text" value="<?php echo @$sp_test_publishable_key;?>">
            </div>
        </div>

        <div class="row input-row spTestModeSettingData" <?php if($sp_is_enable!='1' || $sp_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Secret Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control sp_test_secret_key" type="text" value="<?php echo @$sp_test_secret_key;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->

        <!--        PRODUCTION MODE SETTING START        -->
        <div class="row input-row spProdModeSettingData" <?php if($sp_is_enable!='1' || $sp_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Publishable Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control sp_prod_publishable_key" type="text" value="<?php echo @$sp_prod_publishable_key;?>">
            </div>
        </div>

        <div class="row input-row spProdModeSettingData" <?php if($sp_is_enable!='1' || $sp_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Secret Key:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control sp_prod_secret_key" type="text" value="<?php echo @$sp_prod_secret_key;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 stripe_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_stripe' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><button class="btn btn-success btn-sm btn_submit_stripe website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        /*    payumoney enable start   */
            $('.sp_is_enable').change(function(){
                var sp_activated_mode = $("input[name='sp_activated_mode']:checked"). val();
                if($(this).is(':checked'))
                {
                    var sp_is_enable = '1';
                }
                else
                {
                    var sp_is_enable = '0';
                }

                if(sp_is_enable=='1')
                {
                    $('.StripePaymentSettingData').show();

                    if(sp_activated_mode=='TEST')
                    {
                        $('.spTestModeSettingData').show();
                        $('.spProdModeSettingData').hide();
                    }
                    else
                    if(sp_activated_mode=='PROD')
                    {
                        $('.spProdModeSettingData').show();
                        $('.spTestModeSettingData').hide();
                    }
                    return false;
                }
                else
                {
                    $('.StripePaymentSettingData').hide();
                    $('.spTestModeSettingData').hide();
                    $('.spProdModeSettingData').hide();
                    return false;
                }    
            });
        /*     Instamojo enable end   */

        /*    Transaction mode activation  */
        $('.sp_activated_mode').change(function(){
            var sp_activated_mode = $("input[name='sp_activated_mode']:checked"). val();
            if(sp_activated_mode=='TEST')
            {
                $('.spTestModeSettingData').show();
                $('.spProdModeSettingData').hide();
            }
            else
            if(sp_activated_mode=='PROD')
            {
                $('.spProdModeSettingData').show();
                $('.spTestModeSettingData').hide();
            }
        });

        $('.btn_submit_stripe').click(function(){
            if($('.sp_is_enable').is(':checked'))
            {
                var sp_is_enable = '1';
            }
            else
            {
                var sp_is_enable = '0';
            }

            var sp_display_name = $('.sp_display_name').val();
            var sp_activated_mode = $("input[name='sp_activated_mode']:checked"). val();
            if(sp_activated_mode=='' || sp_activated_mode==null || sp_activated_mode=='undefined')
            {
                sp_activated_mode = '0';
            }

            var sp_test_publishable_key = $('.sp_test_publishable_key').val().toString();
            var sp_test_secret_key = $('.sp_test_secret_key').val().toString();
            var sp_prod_publishable_key = $('.sp_prod_publishable_key').val().toString();
            var sp_prod_secret_key = $('.sp_prod_secret_key').val().toString();

            if(sp_is_enable=='1' && (sp_display_name=='' || sp_display_name==null))
            {
                $('.stripe_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter display name.</div></center>");
                return false
            }

            if(sp_is_enable=='1' && (sp_activated_mode=='' || sp_activated_mode==null || sp_activated_mode=='0'))
            {
                $('.stripe_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please select activation mode.</div></center>");
                return false
            }

            /*********   Test Mode validations   ***********/
            if(sp_activated_mode=='TEST' && (sp_test_publishable_key=='' || sp_test_publishable_key==null))
            {
                $('.stripe_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter stripe test mode publishable key.</div></center>");
                return false
            }

            if(sp_activated_mode=='TEST' && (sp_test_secret_key=='' || sp_test_secret_key==null))
            {
                $('.stripe_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter stripe test mode secret key.</div></center>");
                return false
            }

            /***************   Production mode validations   **************/
            if(sp_activated_mode=='PROD' && (sp_prod_publishable_key=='' || sp_prod_publishable_key==null))
            {
                $('.stripe_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter stripe production mode publishable key.</div></center>");
                return false
            }

            if(sp_activated_mode=='PROD' && (sp_prod_secret_key=='' || sp_prod_secret_key==null))
            {
                $('.stripe_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter stripe production mode private key.</div></center>");
                return false
            }

            var task = "Stripe_payment_gateway";

            var data = {
                sp_is_enable:sp_is_enable,
                sp_display_name:sp_display_name,
                sp_activated_mode:sp_activated_mode,
                sp_test_publishable_key:sp_test_publishable_key,
                sp_test_secret_key:sp_test_secret_key,
                sp_prod_publishable_key:sp_prod_publishable_key,
                sp_prod_secret_key:sp_prod_secret_key,
                task:task
            }

            $('.loading_img_stripe').show();
            $('.stripe_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/payment-gateway-setting/stripe_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_stripe').hide();
                    if(res=='success')
                    {
                        $('.stripe_status').html("<center><div class='alert alert-success stripe_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Stripe payment Setting Data Updated Successfully.</div></center>");
                        $('.stripe_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("payment-gateway-setup.php?main_tab=PaymentGateway&sub_tab=Stripe");
                        });
                    }
                    else
                    {
                        $('.stripe_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>