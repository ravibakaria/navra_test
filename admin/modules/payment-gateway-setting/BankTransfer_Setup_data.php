<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt_bt   = $link->prepare("SELECT * FROM `payment_bank_transfer`");
	$stmt_bt->execute();
	$result_bt = $stmt_bt->fetch();
	$count_bt=$stmt_bt->rowCount();

	$bt_is_enable = $result_bt['bt_is_enable'];
    $bt_display_name = $result_bt['bt_display_name'];
    $bt_pay_to_text = $result_bt['bt_pay_to_text'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
                Enable Bank Transfer Payment :
            </div>
            <div class="col-xs-4 col-sm-4 col-md-1 col-lg-1">
                <input type="checkbox" class="bt_is_enable" <?php if($count_bt>0 && ($bt_is_enable=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this payment then offline payment will be available to member to make payment.
                </div>
            </div>
        </div>

        <div class="row input-row BankTransferPaymentSettingData" <?php if($bt_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Display Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control bt_display_name" type="text" value="<?php echo @$bt_display_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This name will be displayed on page selecting payment method.
                </div>
            </div>
        </div>

        <div class="row input-row BankTransferPaymentSettingData" <?php if($bt_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                Pay To Text:
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <textarea class="form-control bt_pay_to_text" rows="15" placeholder="Please email bank transfer details to following email id : abc@xyz.com"><?php echo @$bt_pay_to_text;?></textarea>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This text will be displayed on members order summary. You can add complete information to indicate this on members purchase summary page.
                </div>
                <b>Preview: </b>
                <div class='alert alert-default bt-pay-to-text-preview' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <?php  echo @$bt_pay_to_text; ?>
                </div>
            </div>
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 bank_transfer_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_bank_transfer' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><button class="btn btn-success btn-sm btn_submit_bank_transfer website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $('.bt_pay_to_text').keyup(function (e) {
            $('.bt-pay-to-text-preview').html($(this).val());
        });
        /*    bank_transfer enable start   */
            $('.bt_is_enable').change(function(){

                if($(this).is(':checked'))
                {
                    var bt_is_enable = '1';
                }
                else
                {
                    var bt_is_enable = '0';
                }

                if(bt_is_enable=='1')
                {
                    $('.BankTransferPaymentSettingData').show();
                    return false;
                }
                else
                {
                    $('.BankTransferPaymentSettingData').hide();
                    return false;
                } 


            });
        /*     bank_transfer enable end   */        

        $('.btn_submit_bank_transfer').click(function(){
            if($('.bt_is_enable').is(':checked'))
            {
                var bt_is_enable = '1';
            }
            else
            {
                var bt_is_enable = '0';
            }

            var bt_display_name = $('.bt_display_name').val();
            var bt_pay_to_text = $('.bt_pay_to_text').val();
            var task = "bank_transfer_payment_gateway";

            if(bt_is_enable=='1' && (bt_display_name=='' || bt_display_name==null))
            {
                $('.bank_transfer_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter display name.</div></center>");
                return false
            }

            if(bt_is_enable=='1' && (bt_pay_to_text=='' || bt_pay_to_text==null))
            {
                $('.bank_transfer_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter pay to text.</div></center>");
                return false
            }

            
            var data = {
                bt_is_enable : bt_is_enable,
                bt_display_name : bt_display_name,
                bt_pay_to_text : bt_pay_to_text,
                task:task
            }

            $('.loading_img_bank_transfer').show();
            $('.bank_transfer_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/payment-gateway-setting/bank_transfer_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_bank_transfer').hide();
                    if(res=='success')
                    {
                        $('.bank_transfer_status').html("<center><div class='alert alert-success bank_transfer_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Bank Transfer Payment Setting Data Updated Successfully.</div></center>");
                        $('.bank_transfer_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("payment-gateway-setup.php?main_tab=PaymentGateway&sub_tab=BankTransfer");
                        });
                    }
                    else
                    {
                        $('.bank_transfer_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>