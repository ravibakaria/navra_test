<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt_pp   = $link->prepare("SELECT * FROM `payment_paypal`");
	$stmt_pp->execute();
	$result_pp = $stmt_pp->fetch();
	$count_pp = $stmt_pp->rowCount();

	$pp_is_enable = $result_pp['pp_is_enable'];
    $pp_display_name = $result_pp['pp_display_name'];
    $pp_activated_mode = $result_pp['pp_activated_mode'];
    $pp_test_client_id = $result_pp['pp_test_client_id'];
    $pp_test_secret = $result_pp['pp_test_secret'];
    $pp_prod_client_id = $result_pp['pp_prod_client_id'];
    $pp_prod_secret = $result_pp['pp_prod_secret'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">          
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <h2>PayPal</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-right">
                <a href="<?php echo $websiteBasePath?>/reference-documents/PayPal-Setup-Steps.pdf" target="_blank">
                    <span data-toggle="tooltip" data-placement="left" title="PayPal Refrence Document.">
                        <img src="<?php echo $websiteBasePath?>/images/file-pdf.png">
                    </span>
                </a>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
                Enable Paypal Payment Gateway:
            </div>
            <div class="col-xs-4 col-sm-4 col-md-1 col-lg-1">
                <input type="checkbox" class="pp_is_enable" <?php if($count_pp>0 && ($pp_is_enable=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this payment gateway then this will be available to member to make payment.
                </div>
            </div>
        </div>

        <div class="row input-row PaypalPaymentSettingData" <?php if($pp_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Display Name:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pp_display_name" type="text" value="<?php echo @$pp_display_name;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This name will be displayed on page selecting payment method.
                </div>
            </div>
        </div>

        <div class="row input-row PaypalPaymentSettingData" <?php if($pp_is_enable!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                Activate Mode:
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <input type='radio' name='pp_activated_mode' value='TEST' class='pp_activated_mode' id='PPTEST' <?php if($pp_activated_mode=='TEST') { echo "checked"; } ?>>  <label for='PPTEST' class='control-label'>&nbsp;Test Mode </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='radio' name='pp_activated_mode' value='PROD' class='pp_activated_mode' id='PPPROD'<?php if($pp_activated_mode=='PROD') { echo "checked"; } ?>>  <label for='PPPROD' class='control-label'>&nbsp;Production/Live Mode </label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> Test mode or Production mode.
                </div>
            </div>
        </div>

        <!--        TEST MODE SETTING START        -->
        <div class="row input-row ptTestModeSettingData" <?php if($pp_is_enable!='1' || $pp_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Client ID:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pp_test_client_id" type="text" value="<?php echo @$pp_test_client_id;?>">
            </div>
        </div>

        <div class="row input-row ptTestModeSettingData" <?php if($pp_is_enable!='1' || $pp_activated_mode!='TEST') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Test Mode Secret:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pp_test_secret" type="text" value="<?php echo @$pp_test_secret;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->

        <!--        PRODUCTION MODE SETTING START        -->
        <div class="row input-row ptProdModeSettingData" <?php if($pp_is_enable!='1' || $pp_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Client ID:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pp_prod_client_id" type="text" value="<?php echo @$pp_prod_client_id;?>">
            </div>
        </div>

        <div class="row input-row ptProdModeSettingData" <?php if($pp_is_enable!='1' || $pp_activated_mode!='PROD') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Production Mode Secret:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input class="form-control pp_prod_secret" type="text" value="<?php echo @$pp_prod_secret;?>">
            </div>
        </div>
        <!--        TEST MODE SETTING END        -->
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 paypal_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_paypal' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><button class="btn btn-success btn-sm btn_submit_paypal website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        /*    Paytm enable start   */
            $('.pp_is_enable').change(function(){
                var pp_activated_mode = $("input[name='pp_activated_mode']:checked"). val();
                if($(this).is(':checked'))
                {
                    var pp_is_enable = '1';
                }
                else
                {
                    var pp_is_enable = '0';
                }

                if(pp_is_enable=='1')
                {
                    $('.PaypalPaymentSettingData').show();

                    if(pp_activated_mode=='TEST')
                    {
                        $('.ptTestModeSettingData').show();
                        $('.ptProdModeSettingData').hide();
                    }
                    else
                    if(pp_activated_mode=='PROD')
                    {
                        $('.ptProdModeSettingData').show();
                        $('.ptTestModeSettingData').hide();
                    }
                    return false;
                }
                else
                {
                    $('.PaypalPaymentSettingData').hide();
                    $('.ptTestModeSettingData').hide();
                    $('.ptProdModeSettingData').hide();
                    return false;
                }    
            });
        /*     Instamojo enable end   */

        /*    Transaction mode activation  */
        $('.pp_activated_mode').change(function(){
            var pp_activated_mode = $("input[name='pp_activated_mode']:checked"). val();
            if(pp_activated_mode=='TEST')
            {
                $('.ptTestModeSettingData').show();
                $('.ptProdModeSettingData').hide();
            }
            else
            if(pp_activated_mode=='PROD')
            {
                $('.ptProdModeSettingData').show();
                $('.ptTestModeSettingData').hide();
            }
        });

        $('.btn_submit_paypal').click(function(){
            if($('.pp_is_enable').is(':checked'))
            {
                var pp_is_enable = '1';
            }
            else
            {
                var pp_is_enable = '0';
            }

            var pp_display_name = $('.pp_display_name').val();
            var pp_activated_mode = $("input[name='pp_activated_mode']:checked"). val();
            if(pp_activated_mode=='' || pp_activated_mode==null || pp_activated_mode=='undefined')
            {
                pp_activated_mode = '0';
            }

            var pp_test_client_id = $('.pp_test_client_id').val().toString();
            var pp_test_secret = $('.pp_test_secret').val().toString();
            var pp_prod_client_id = $('.pp_prod_client_id').val().toString();
            var pp_prod_secret = $('.pp_prod_secret').val().toString();

            if(pp_is_enable=='1' && (pp_display_name=='' || pp_display_name==null))
            {
                $('.paypal_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter display name.</div></center>");
                return false
            }

            if(pp_is_enable=='1' && (pp_activated_mode=='' || pp_activated_mode==null || pp_activated_mode=='0'))
            {
                $('.paypal_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please select activation mode.</div></center>");
                return false
            }

            /*********   Test Mode validations   ***********/
            if(pp_activated_mode=='TEST' && (pp_test_client_id=='' || pp_test_client_id==null))
            {
                $('.paypal_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter paypal Username for test mode.</div></center>");
                return false
            }

            if(pp_activated_mode=='TEST' && (pp_test_secret=='' || pp_test_secret==null))
            {
                $('.paypal_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter paypal password for test mode.</div></center>");
                return false
            }

            /***************   Production mode validations   **************/
            if(pp_activated_mode=='PROD' && (pp_prod_client_id=='' || pp_prod_client_id==null))
            {
                $('.paypal_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter paypal username for production mode.</div></center>");
                return false
            }

            if(pp_activated_mode=='PROD' && (pp_prod_secret=='' || pp_prod_secret==null))
            {
                $('.paypal_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please Enter paypal password for production mode.</div></center>");
                return false
            }

            var task = "paypal_payment_gateway";

            var data = {
                pp_is_enable:pp_is_enable,
                pp_display_name:pp_display_name,
                pp_activated_mode:pp_activated_mode,
                pp_test_client_id:pp_test_client_id,
                pp_test_secret:pp_test_secret,
                pp_prod_client_id:pp_prod_client_id,
                pp_prod_secret:pp_prod_secret,
                task:task
            }

            $('.loading_img_paypal').show();
            $('.paypal_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/payment-gateway-setting/PayPal_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_paypal').hide();
                    if(res=='success')
                    {
                        $('.paypal_status').html("<center><div class='alert alert-success paypal_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> PayPal Setting Data Updated Successfully.</div></center>");
                        $('.paypal_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("payment-gateway-setup.php?main_tab=PaymentGateway&sub_tab=PayPal");
                        });
                    }
                    else
                    {
                        $('.paypal_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>