<?php
	if ( !is_numeric( $vuserid ) )
    {
        echo 'Error! Invalid Data';
        exit;
    }

    $WebsiteBasePath = getWebsiteBasePath();
    $vendor_profile_pic = "SELECT * FROM vendor_profile_pic WHERE vuserid='$vuserid'";
    $stmt_profile_pic = $link->prepare($vendor_profile_pic);
    $stmt_profile_pic->execute();
    $count_profile_pic = $stmt_profile_pic->rowCount();
    $result_profile_pic = $stmt_profile_pic->fetch();
    if($count_profile_pic>0)
    {
    	$profile_pic = $result_profile_pic['profile_pic'];

    	$profile_pic_show = $WebsiteBasePath."/directory/uploads/".$vuserid."/profile/".$profile_pic;
    	$uploaded_on = $result_profile_pic['uploaded_on'];
    	$updated_on = $result_profile_pic['updated_on'];

    	if($updated_on!='' || $updated_on!=null || $updated_on!='0000-00-00 00:00:00')
    	{
    		$last_updated_on = $updated_on;
    	}
    	else
    	{
    		$last_updated_on = $uploaded_on;
    	}
    }

    $vendor_profile_pic = getVendorProfilePic($vuserid);

    $vendor_gallery = "SELECT * FROM vendor_gallery WHERE vuserid='$vuserid'";
    $stmt_gallery = $link->prepare($vendor_gallery);
    $stmt_gallery->execute();
    $count_gallery = $stmt_gallery->rowCount();
?>

<input type="hidden" class="vuserid" value="<?php echo $vuserid;?>">
<div class="row vertical-divider vertical-devider-admin">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<h4>Profile Photo</h4>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row profile-row">
				<?php
					if($count_profile_pic>0)
				    {				    	
				?>
					<div class="isotope-item document">
							<div class="thumbnail">
								<div class="thumb-preview">
									<a class="thumb-image" href="<?php echo $profile_pic_show;?>">
										<img src="<?php echo $profile_pic_show;?>" class="img-responsive member-photos">
									</a>
								</div>
								<div class="mg-description">
									<small class="pull-left">Updated On</small>
									<small class="pull-right"><?php echo date('d-M-Y H:i:A',strtotime($last_updated_on));?></small>
								</div><br/>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 vendor-profile-pic-delete-status">
									</div>
								</div>
								<div class="row">
									<center><img src="../images/loader/loader.gif" class='img-responsive loading_img_profile_pic'' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
								</div><br/>
								<div class="row">
									<center><a class="btn btn-danger btn-sm vendor_profile_photo_delete" ><i class="fa fa-trash-o"></i> Delete Profile Photo</a></center>
								</div>
							</div>
							<input type="hidden" class="vuserid" value="<?php echo $vuserid;?>">
						</div>
				<?php
					}
					else
					{
						echo "<div class='alert alert-danger'>Profile photo not uploaded by vendor</div>";
					}
				?>
			</div>
		</div>
	</div>
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
		<h4>Work Gallery</h4>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row profile-row">
				<?php
					if($count_gallery==0)
    				{
    					echo "<div class='alert alert-danger'>No work photos uploaded by member.</div>";
    				}
    				else
    				if($count_gallery>0)
    				{
    					$result_gallery = $stmt_gallery->fetchAll();
    					foreach( $result_gallery as $row_gallery )
						{
							$photo_id =  $row_gallery['id'];
							$photo =  $WebsiteBasePath.'/directory/uploads/'.$vuserid.'/photo/'.$row_gallery['photo'];
							$created_at = $row_gallery['created_on'];
							$status = $row_gallery['status'];
					?>
						<div class="isotope-item document col-sm-6 col-md-4 col-lg-4">
							<div class="thumbnail">
								<div class="thumb-preview">
									<a class="thumb-image" href="<?php echo $photo;?>">
										<img src="<?php echo $photo;?>" class="img-responsive member-photos">
									</a>
								</div>
								<div class="mg-description">
									<small class="pull-left">Uploaded On</small>
									<small class="pull-right"><?php echo date('d-M-Y H:i:A',strtotime($created_at));?></small>
								</div><br/>
								<div class="row">
									<center><img src="../images/loader/loader.gif" class='img-responsive <?php echo $photo_id;?>_loading_img'' id='<?php echo $photo_id;?>_loading_img' style='width:50px; height:50px; display:none;'/></center>
								</div><br/>
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
										<?php
											if($status!='1')
											{
												echo "<center><a class='btn btn-success btn-sm vendor_work_photos_approve' id='$photo_id'><i class='fa fa-check'></i> Approve</a></center>";
											}
											else
											if($status=='1')
											{
												echo "<center><button class='btn btn-success btn-sm' disabled><i class='fa fa-check'></i> Approved</button></center>";
											}	
										?>
										
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
										<center><a class="btn btn-danger btn-sm vendor_work_photos_delete" id="<?php echo $photo_id;?>"><i class="fa fa-trash-o"></i> Delete</a></center>
									</div>
								</div>
							</div>
							<input type="hidden" class="vuserid" value="<?php echo $vuserid;?>">
						</div>	
					<?php
						}
    				}
				?>
			</div>
			<div class="row profile-row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    				<div class="vendor-work-photo-update-status"></div>
    			</div>
    		</div>
		</div>

	</div>
</div>

<script>
	$(document).ready(function(){
		/****************    Delete profile photo    *******************/
		$('.vendor_profile_photo_delete').click(function(){
			var vuserid = $('.vuserid').val();
			var task = "delete-profile-photo";
			var reason_delete_profile_pic = prompt("Please enter reason to delete profile picture");

			if(reason_delete_profile_pic=='' || reason_delete_profile_pic==null)
			{
				$('.vendor-profile-pic-delete-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span> <strong> Empty! </strong><br/> Please enter reason for profile photo reject.</center></div>");
            	return false;
			}

			var data = {
				vuserid : vuserid,
				reason_delete_profile_pic : reason_delete_profile_pic,
				task : task
			};

			$('.loading_img_profile_pic').show();
			$('.member_profile_photo_delete').attr('disabled',true);
			$('.vendor-profile-pic-delete-status').html("");

			$.ajax({
				type:'post',
				dataType:'json',
				data:data,
				url:'query/wedding-category/Vendor-Photo-helper.php',
				success:function(res)
				{
					$('.loading_img_profile_pic').hide();
					if(res=='success')
					{
						$('.vendor-profile-pic-delete-status').html("<div class='alert alert-success vendor-profile-pic-delete-status-new' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span> <strong> Success! </strong><br/> Profile photo removed successfully .</center></div>");
						$('.vendor-profile-pic-delete-status-new').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign('vendor-information.php?id='+vuserid+"&main_tab=VendorAttributes&sub_tab=Vendor_Photos");
                        });
                		return false;
					}
					else
					{
						$('.member_profile_photo_delete').attr('disabled',false);
						$('.vendor-profile-pic-delete-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-times'></span> <strong> Empty! </strong><br/> "+res+"</center></div>");
                		return false;
					}
				}
			});
		});

		/****************    Delete personal photos    *******************/
		$('.vendor_work_photos_delete').click(function(){
			var vuserid = $('.vuserid').val();
			var photo_id = $(this).attr('id');
			var task = "Update-vendor-Photo-Status";
			
			var reason = prompt("Please enter reason to reject work photo");

			if(reason=='' || reason==null)
			{
				$('.vendor-work-photo-update-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span> <strong> Empty! </strong><br/> Please enter reason for photo reject.</center></div>");
            	return false;
			}

			$('.vendor_work_photos_delete').attr('disabled',true);
			$('.'+photo_id+'_loading_img').show();

			var data = {
				vuserid : vuserid,
				photo_id : photo_id,
				reason : reason,
				task : task
			};
			
			$('.vendor-work-photo-update-status').html("");

			$.ajax({
				type:'post',
				dataType:'json',
				data:data,
				url:'query/wedding-category/Vendor-Photo-helper.php',
				success:function(res)
				{
					$('.'+photo_id+'_loading_img').hide();
					if(res=='success')
					{
						$('.vendor-work-photo-update-status').html("<div class='alert alert-success vendor-work-photo-update-status-new' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span> <strong> Success! </strong><br/> Photo removed successfully .</center></div>");
						$('.vendor-work-photo-update-status-new').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign('vendor-information.php?id='+vuserid+"&main_tab=VendorAttributes&sub_tab=Vendor_Photos");
                        });
                		return false;
					}
					else
					{
						$('.vendor_work_photos_delete').attr('disabled',false);
						$('.vendor-work-photo-update-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-times'></span> <strong> Empty! </strong><br/> "+res+"</center></div>");
                		return false;
					}
				}
			});

		});

		/****************    Approve personal photos    *******************/
		$('.vendor_work_photos_approve').click(function(){
			var vuserid = $('.vuserid').val();
			var photo_id = $(this).attr('id');
			var task = "Approve-vendor-Photo-Status";
			
			if (confirm("Do you want to approve this work photo!")) 
			{
				$('.vendor_work_photos_approve').attr('disabled',true);
				$('.'+photo_id+'_loading_img').show();

				var data = {
					vuserid : vuserid,
					photo_id : photo_id,
					task : task
				};
				
				$('.vendor-work-photo-update-status').html("");

				$.ajax({
					type:'post',
					dataType:'json',
					data:data,
					url:'query/wedding-category/Vendor-Photo-helper.php',
					success:function(res)
					{
						$('.'+photo_id+'_loading_img').hide();
						if(res=='success')
						{
							$('.vendor-work-photo-update-status').html("<div class='alert alert-success vendor-work-photo-update-status-new' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span> <strong> Success! </strong><br/> Photo approved successfully .</center></div>");
							$('.vendor-work-photo-update-status-new').fadeTo(1000, 500).slideUp(500, function(){
	                            window.location.assign('vendor-information.php?id='+vuserid+"&main_tab=VendorAttributes&sub_tab=Vendor_Photos");
	                        });
	                		return false;
						}
						else
						{
							$('.vendor_work_photos_approve').attr('disabled',false);
							$('.vendor-work-photo-update-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-times'></span> <strong> Empty! </strong><br/> "+res+"</center></div>");
	                		return false;
						}
					}
				});
			} 
			else 
			{
				return false;
			}			

		});
	});
</script>