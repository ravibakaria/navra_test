<?php
	if ( !is_numeric( $vuserid ) )
    {
        echo 'Error! Invalid Data';
        exit;
    }

    $sql = "SELECT * FROM `vendors` WHERE `vuserid`='$vuserid'";
    $stmt1   = $link->prepare($sql);
    $stmt1->execute();
    $queryTot = $stmt1->rowCount();
    $result = $stmt1->fetch();

   	if($queryTot>0)
   	{
   		$firstname = $result['firstname'];
   		$lastname = $result['lastname'];
   		$email = $result['email'];
   		$phonecode = $result['phonecode'];
   		$phone = $result['phone'];
   		$company_name = $result['company_name'];
   		$category_id = $result['category_id'];
   		$city = $result['city'];
   		$state = $result['state'];
   		$country = $result['country'];
   		$short_desc = $result['short_desc'];
   		$status = $result['status'];
   		$created_on = $result['created_on'];
   		$updated_on = $result['updated_on'];
   		$isEmailVerifiedOn = $result['isEmailVerifiedOn'];
?>

<input type="hidden" class="vuserid" value="<?php echo $vuserid;?>">
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    		<div class="row profile-row">
    			<div class="text-left col-xs-12 col-sm-12 col-md-6 col-lg-6">
    				<h2>Profile of <?php echo $firstname.' '.$lastname;?></h2>
					<h3>Vendor ID:  <?php echo $vuserid;?></h3>
    			</div>
    			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
    				<div class="row">
	                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
	    				    <button class="btn btn-primary admin-view-profile website-button" data-toggle="modal" data-target="#EditProfileModel">Edit Profile</button>
	                    </div>
	                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
	                        <center><button class="btn btn-primary reset-send-password admin-view-profile website-button">Reset & Email Password</button></center>
	                    </div>
	                </div>
	                <div class="row">
	                	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <center><img src="../images/loader/loader.gif" class='img-responsive loading_img1' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
                        </div>
	                	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 send-password-status">
	                	</div>
	                </div>
    			</div>
    		</div>

    		<hr class="dotted">

    		<div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    				<h4>Basic Information</h4>
    				<br>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>First Name</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= $firstname; ?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Last Name</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= $lastname; ?>
    			</div>
    		</div>

    		<hr class="dotted">

    		<div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    				<h4>Contact Information</h4>
    				<br>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Email ID</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= $email; ?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Mobile Number</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= '+'.$phonecode.'-'.$phone; ?>
    			</div>
    		</div>

    		<hr class="dotted">

    		<div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    				<h4>Company Information</h4>
    				<br>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Company Name</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= $company_name; ?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Category</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= getdirectorynamefromId($category_id); ?>
    			</div>
    		</div>
    		<div class="row profile-row">
    			
    			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
    				<b>Short Description</b>
    			</div>
    			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    				<?= $short_desc; ?>
    			</div>
    		</div>

    		<hr class="dotted">

    		<div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    				<h4>Location Information</h4>
    				<br>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>City</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<?= getUserCityName($city); ?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>State</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<?= getUserStateName($state); ?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Country</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<?= getUserCountryName($country); ?>
    			</div>
    		</div>

    		<hr class="dotted">

    		<div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    				<h4>Security Information</h4>
    				<br>
    				<div class="row profile-row">
	    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<b>Registered On</b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<?= $created_on; ?>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<b>Email Verified On</b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<?php
		    					if($isEmailVerifiedOn=='' || $isEmailVerifiedOn==null)
		    					{
		    						echo "NA";
		    					}
		    					else
		    					{
		    						echo $isEmailVerifiedOn;
		    					}
		    				?>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<b>Last Password Update On</b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<?php
		    					if($updated_on=='' || $updated_on==null)
		    					{
		    						echo "Not Verified";
		    					} 
		    					else
		    					{
		    						echo $updated_on;
		    					}
		    				?>
		    			</div>
		    		</div>
    			</div>
    			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    				<h4>Last Login</h4>
    				<br>
    				<?php
    					$sql_login = "SELECT * FROM userlogs WHERE user_type='Vendor' AND userid='$vuserid' ORDER BY loggedOn DESC";
    					$stmt_login = $link->prepare($sql_login);
    					$stmt_login->execute();
    					$count_login = $stmt_login->rowCount();
    					$result_login = $stmt_login->fetch();

    					$IP_Address = $result_login['IP_Address'];
    					$loggedOn = $result_login['loggedOn'];
    				?>
    				<div class="row profile-row">
	    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<b>Last Logged in On</b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<?php
		    					if($count_login==0)
		    					{
		    						echo "NA";
		    					}
		    					else
		    					{
		    						echo $loggedOn;
		    					}
		    				?>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<b>IP Address</b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<?php
		    					if($count_login==0)
		    					{
		    						echo "NA";
		    					}
		    					else
		    					{
		    						echo $IP_Address;
		    					}
		    				?>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<b>Status</b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<?php
		    					if($status==0)
		    					{
		    						echo "In-Active";
		    					}
		    					else
		    					if($status==1)
		    					{
		    						echo "Active";
		    					}
		    					else
		    					if($status==2)
		    					{
		    						echo "Deactivated";
		    					}
		    					else
		    					if($status==3)
		    					{
		    						echo "Suspended";
		    					}
		    				?>
		    			</div>
		    		</div>
    			</div>
    		</div>

    	</div>

    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<!-- Edit Profile Modal -->
	        <div class="modal fade" id="EditProfileModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	            <div class="modal-dialog modal-dialog-centered add-item-model-template" role="document">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <center><h2 class="modal-title" id="exampleModalLongTitle">Edit Vendor Information</h2></center>
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                        </button>
	                    </div>
	                    <div class="modal-body">
	                    	<div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Basic Information</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">First Name:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm firstname" value="<?php echo $firstname;?>"/>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Last Name:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm lastname" value="<?php echo $lastname;?>" />
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Status:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control status">
		                                	<?php
		                                		if($status=='0')
		                                		{
		                                			echo "<option value='0' selected>In-Active</option>";
		                                			echo "<option value='1'>Active</option>";
		                                			echo "<option value='2'>Deactivate</option>";
                                                    echo "<option value='3'>Suspend</option>";
		                                		}
		                                		else
		                                		if($status=='1')
		                                		{
		                                			echo "<option value='0'>In-Active</option>";
		                                			echo "<option value='1' selected>Active</option>";
		                                			echo "<option value='2'>Deactivate</option>";
                                                    echo "<option value='3'>Suspended</option>";
		                                		}
		                                		else
		                                		if($status=='2')
		                                		{
		                                			echo "<option value='0'>In-Active</option>";
		                                			echo "<option value='1'>Active</option>";
		                                			echo "<option value='2' selected>Deactivate</option>";
                                                    echo "<option value='3'>Suspend</option>";
		                                		}
		                                		else
		                                		if($status=='3')
		                                		{
		                                			echo "<option value='0'>In-Active</option>";
		                                			echo "<option value='1'>Active</option>";
		                                			echo "<option value='2'>Deactivate</option>";
                                                    echo "<option value='3' selected>Suspend</option>";
		                                		}
		                                	?>
		                                </select>
		                            </div>
		                        </div>
	                        </div>

	                        <hr class="dotted">
	                    	<div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Contact Information</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Email ID:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm email" value="<?php echo $email;?>" disabled/>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Mobile:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<div class="input-group">
			                            	<span class="input-group-addon country_code">
	                                            <?php
	                                                echo $phonecode;
	                                            ?>
	                                        </span>
	                                        <input type="text" name="phone" class="form-control phone" placeholder="Mobile Number"  value="<?php echo $phone;?>" maxlength="20">
	                                        <input type="hidden"  name="phonecode" class="phonecode"  value="<?php echo $phonecode;?>"/>
	                                    </div>
		                            </div>
		                        </div>
	                        </div>

	                        <hr class="dotted">
	                    	<div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Company Information</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Company Name:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm company_name" value="<?php echo $company_name;?>"/>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Category:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control category_id">
		                            		<option value='0'>--Select Category--</option>
		                            		<?php
		                            			$sql_category = "SELECT * FROM membercategory";
		                            			$stmt_category = $link->prepare($sql_category);
		                            			$stmt_category->execute();
		                            			$result_category = $stmt_category->fetchAll();

		                            			foreach ($result_category as $row_category) 
		                            			{
		                            				$cid = $row_category['id'];
		                            				$cname = $row_category['name'];

		                            				if($cid==$category_id)
		                            				{
		                            					echo "<option value='".$cid."' selected>".$cname."</option>";
		                            				}
		                            				else
		                            				{
		                            					echo "<option value='".$cid."'>".$cname."</option>";
		                            				}
		                            			}
		                            		?>
		                            	</select>
		                            </div>
		                        </div>
	                        </div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 member-profile-edit">
		                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		                                <lable class="control-label">Short Description:</lable>
		                            </div>
		                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
		                                <textarea class="form-control form-control-sm short_desc" rows='5'><?php echo $short_desc;?></textarea>
		                            </div>
		                        </div>
	                        </div>

	                        <hr class="dotted">

	                        <div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Location Information</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Country</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control country" id="country">
							              	<?php
							              		$query  = "SELECT * FROM `countries` ORDER BY `name` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$country_name =  $row['name'];
													$country_id = $row['id']; 

													if($country_id==$country)
													{
														echo "<option value=".$country_id." selected>".$country_name."</option>";
													}
													else
													{
														echo "<option value=".$country_id.">".$country_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">State</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control state" id="state">
											<?php
												if($country!='0')
												{
													$state_sql  = "SELECT * FROM `states` WHERE `country_id`='$country' ORDER BY `name` ASC";
									                $stmt   = $link->prepare($state_sql);
									                $stmt->execute();
									                $states_userRow = $stmt->fetchAll();
									                
									                foreach( $states_userRow as $row )
													{
														$state_name =  $row['name'];
														$state_id = $row['id']; 

														if($state_id==$state)
														{
															echo "<option value=".$state_id." selected>".$state_name."</option>";
														}
														else
														{
															echo "<option value=".$state_id.">".$state_name."</option>";
														}
													}
												}
								                
								            ?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">City</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control city" id="city" required>
											<?php
												if($state!='0')
												{
													$city_sql  = "SELECT * FROM `cities` WHERE `state_id`='$state' ORDER BY `name` ASC";
									                $stmt   = $link->prepare($city_sql);
									                $stmt->execute();
									                $city_userRow = $stmt->fetchAll();
									                
									                foreach( $city_userRow as $row )
													{
														$city_name =  $row['name'];
														$city_id = $row['id']; 

														if($city_id==$city)
														{
															echo "<option value=".$city_id." selected>".$city_name."</option>";
														}
														else
														{
															echo "<option value=".$city_id.">".$city_name."</option>";
														}
													}
												}
								                
								            ?>
							            </select>
		                            </div>
		                        </div>
	                        </div>

	                        <input type="hidden" class="vuserid" value="<?php echo $vuserid;?>">
	                        <div class="row">
	                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:70px; height:70px; display:none;'/></center>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 edit-vendor-profile-status">
	                            </div>
	                        </div>
	                    </div>
	                    <div class="modal-footer">
	                        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="margin-top:10px !important;">Close</button>
	                        <button type="button" class="btn btn-primary btn_edit_vendor_profile website-button">Update</button>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    </div>
</div>


<?php
	}
	else
	{
		echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
	}
?>

<script>
	$(document).ready(function(){

		/*   Prevent entering charaters in mobile & phone number   */
        $(".phone").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.add_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Albhabets not allowed. Enter Digits only.</div>").show().fadeOut(3000);
                return false;
            }
        });

        $('.country').change(function(){         //Fetch states
            var country_id = $(this).val();
            var task = "Fetch_state_data";  

            $.ajax({
                type:'post',
                data:'country_id='+country_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    var result = $.parseJSON(res);
                    $('.state').html(result[0]);
                    $('.country_code').html("+"+result[1]);
                    $('.country_phone_code').val(result[1]);
                }
            });         
        });

        $('.state').change(function(){         //Fetch cities
            var state_id = $(this).val();
            var task = "Fetch_city_data";  
            $.ajax({
                type:'post',
                data:'state_id='+state_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    $('.city').html(res);
                }
            });         
        });

        /**************   Update vendor data    ****************/
		$('.btn_edit_vendor_profile').click(function(){
			var firstname = $('.firstname').val();
			var lastname = $('.lastname').val();
			var company_name = $('.company_name').val();
			var category_id = $('.category_id').val();
			var short_desc = $('.short_desc').val();
			var country = $('.country').val();
			var state = $('.state').val();
			var city = $('.city').val();
			var email = $('.email').val();
			var phonecode = $('.phonecode').val();
			var phone = $('.phone').val();
			var status = $('.status').val();
			
			var vuserid = $('.vuserid').val();
			var task = "Update_Vendor_Attributes";

			if(firstname=='' || firstname==null)
			{
				$('.edit-vendor-profile-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter first name.</div></center>");
        			return false;
			}

			if(lastname=='' || lastname==null)
			{
				$('.edit-vendor-profile-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter last name.</div></center>");
        			return false;
			}

			if(email=='' || email==null)
			{
				$('.edit-vendor-profile-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter email id.</div></center>");
        			return false;
			}

			if(company_name=='' || company_name==null)
			{
				$('.edit-vendor-profile-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter company name.</div></center>");
        			return false;
			}

			if(short_desc=='' || short_desc==null)
			{
				$('.edit-vendor-profile-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter short description.</div></center>");
        			return false;
			}

			if(state=='0')
			{
				$('.edit-vendor-profile-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select state.</div></center>");
        			return false;
			}

			if(city=='0')
			{
				$('.edit-vendor-profile-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select city.</div></center>");
        			return false;
			}

			if(email=='' || email==null)
			{
				$('.edit-vendor-profile-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter email address.</div></center>");
        			return false;
			}

			if(phone=='' || phone==null)
			{
				$('.edit-vendor-profile-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter mobile number.</div></center>");
        			return false;
			}

			var data = {
				firstname : firstname,
				lastname : lastname,
				company_name : company_name,
				category_id : category_id,
				short_desc : short_desc,
				country : country,
				state : state,
				city : city,
				email : email,
				phonecode : phonecode,
				phone : phone,
				status : status,
				vuserid : vuserid,
				task : task
			};

			$('.loading_img').show();
			$('.edit-vendor-profile-status').html("");

			$.ajax({
				type:'post',
				dataType:'json',
	        	data:data,
	        	url:'query/wedding-category/vendor-helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.edit-vendor-profile-status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data updated successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.edit-vendor-profile-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});

		/**************   Reset & send password   **************/
		$('.reset-send-password').click(function(){
			var vuserid = $('.vuserid').val();
			var task = "Reset-Send-Password";

			var data = {
				vuserid : vuserid,
				task : task
			};

			$('.reset-send-password').attr('disabled',true);
			$('.loading_img1').show();

			$.ajax({
				type:'post',
				dataType:'json',
				data:data,
				url:'query/wedding-category/vendor-helper.php',
				success:function(res)
				{
					$('.loading_img').hide();
					if(res=='success')
					{
						$('.send-password-status').html("<center><div class='alert alert-success alert-dismissible' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><span class='fa fa-check'></span><strong> Success!</strong> Password reset & sent email to vendor successfully.</strong>.</div></center>");
					}
					else
					{
						$('.send-password-status').html("<div class='alert alert-danger alert-dismissible' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a><span class='fa fa-exclamation-circle'></span><strong> Error!</strong> "+res+"</strong>.</div>");
						return false;
					}
				}
			});
		});
	});
</script>