<?php
    $DefaultCurrency = getDefaultCurrency();
    $CURRENCY = getDefaultCurrencyCode($DefaultCurrency);

    $userid = $_GET['id'];
    if(isset($_GET['OrderNumber']))
    {
        $OrderNumber = $_GET['OrderNumber'];
    }
    else
    {
        $OrderNumber = '0';
    }

    $sql_order = "SELECT * FROM payment_transactions WHERE userid='$userid' AND OrderNumber='$OrderNumber'";
    $stmt_order = $link->prepare($sql_order);
    $stmt_order->execute();
    $count_order = $stmt_order->rowCount();

    if($count_order>0)
    { 
        $result_order = $stmt_order->fetch();
        $id = $result_order['id'];
        $OrderNumber = $result_order['OrderNumber'];
        $membership_plan = $result_order['membership_plan'];
        $membership_plan_name = $result_order['membership_plan_name'];
        $membership_plan_id = $result_order['membership_plan_id'];
        $membership_contacts = $result_order['membership_contacts'];
        $membership_plan_amount = $result_order['membership_plan_amount'];
        $membership_plan_expiry_date = $result_order['membership_plan_expiry_date'];
        $featured_listing = $result_order['featured_listing'];
        $featured_listing_amount = $result_order['featured_listing_amount'];
        $featured_listing_expiry_date = $result_order['featured_listing_expiry_date'];
        $tax_applied = $result_order['tax_applied'];
        $tax_name = $result_order['tax_name'];
        $tax_percent = $result_order['tax_percent'];
        $tax_amount = $result_order['tax_amount'];
        $total_amount = $result_order['total_amount'];
        $tenure = $result_order['tenure'];
        $transact_id = $result_order['transact_id'];
        $request_id = $result_order['request_id'];
        $payment_gateway = $result_order['payment_gateway'];
        $payment_method = $result_order['payment_method'];
        $created_at = $result_order['created_at'];
        $updated_at = $result_order['updated_at'];
        $status = $result_order['status'];

        if($status=='0')
        {
            $status_show = "<b class='label label-warning text-right' style='text-align:right;'>Un-Paid</b>";
        }
        else
        if($status=='1')
        {
            $status_show = "<b class='label label-success text-right' style='text-align:right;'>Paid</b>";
        }
        else
        if($status=='2')
        {
            $status_show = "<b class='label label-danger text-right' style='text-align:right;'>Canceled</b>";
        }
        else
        if($status=='3')
        {
            $status_show = "<b class='label label-info text-right' style='text-align:right;'>Refund</b>";
        }
?>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Order Summary #<?= $OrderNumber?></h2>
                <table class="form" width="100%" border="0" cellspacing="2" cellpadding="3">
                    <tbody>
                        <tr>
                            <td class="fieldlabel" width="20%">Order #</td>
                            <td class="fieldarea" width="30%"><?= $OrderNumber?> <a href="" data-toggle='modal' data-target='#update_order' style='text-decoration:none;'><b>Update Order</b></a></td>
                            <td class="fieldlabel" width="20%">Registration Date</td>
                            <td class="fieldarea" width="30%"><?= $created_at?></td>
                        </tr>
                        <tr>
                            <td class="fieldlabel" width="20%">Status</td>
                            <td class="fieldarea" width="30%"><?= $status_show?></td>
                            <td class="fieldlabel" width="20%">Payment/Activation Date</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($status=='1')
                                    {
                                        echo $updated_at;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>                                    
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldlabel" width="20%">Total Payment Amount</td>
                            <td class="fieldarea" width="30%"><?= $CURRENCY." ".$total_amount?></td>
                            <td class="fieldlabel" width="20%">Tenure</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    echo $tenure." Month";
                                ?>                                    
                            </td>
                        </tr>

                        <tr><td colspan="2"></td></tr>

                        <tr>
                            <td class="fieldlabel" width="20%">Membership Plan Purchased</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($membership_plan=='Yes')
                                    {
                                        echo "<b style='color:green;'><i class='fa fa-check fa-lg'></i></b> ".$membership_plan;
                                    }
                                    else
                                    if($membership_plan=='No')
                                    {
                                        echo "<b style='color:red;'><i class='fa fa-times fa-lg'></i></b> ".$membership_plan;
                                    }
                                ?>
                            </td>
                            <td class="fieldlabel" width="20%">Membership Plan Name</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($membership_plan=='Yes')
                                    {
                                        echo $membership_plan_name;
                                    }
                                    else
                                    {
                                        echo "-"." <b style='color:red;'><i class='fa fa-times'></i></b>";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldlabel" width="20%">Membership Plan ID</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($membership_plan=='Yes')
                                    {
                                        echo $membership_plan_id;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                            <td class="fieldlabel" width="20%">Number Of Contacts</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($membership_plan=='Yes')
                                    {
                                        echo $membership_contacts;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldlabel" width="20%">Membership Plan Amount</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($membership_plan=='Yes')
                                    {
                                        echo $CURRENCY." ".$membership_plan_amount;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                            <td class="fieldlabel" width="20%">Expiry Date</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($membership_plan_expiry_date!='')
                                    {
                                        echo $membership_plan_expiry_date;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr><td colspan="2"></td></tr>

                        <tr>
                            <td class="fieldlabel" width="20%">Featured Listing Purchased</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($featured_listing=='Yes')
                                    {
                                        echo "<b style='color:green;'><i class='fa fa-check fa-lg'></i></b> ".$featured_listing;
                                    }
                                    else
                                    if($featured_listing=='No')
                                    {
                                        echo "<b style='color:red;'><i class='fa fa-times fa-lg'></i></b> ".$featured_listing;
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldlabel" width="20%">Featured Listing Amount</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($featured_listing=='Yes')
                                    {
                                        echo $CURRENCY." ".$featured_listing_amount;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                            <td class="fieldlabel" width="20%">Expiry Date</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($featured_listing=='Yes' && $featured_listing_expiry_date!='')
                                    {
                                        echo $featured_listing_expiry_date;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr><td colspan="2"></td></tr>

                        <tr>
                            <td class="fieldlabel" width="20%">Tax Applied</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($tax_applied=='1')
                                    {
                                        echo "<b style='color:green;'><i class='fa fa-check fa-lg'></i></b> Yes";
                                    }
                                    else
                                    if($tax_applied=='0')
                                    {
                                        echo "<b style='color:red;'><i class='fa fa-times fa-lg'></i></b> No";
                                    }
                                ?>
                            </td>
                            <td class="fieldlabel" width="20%">Tax Name</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($tax_applied=='1')
                                    {
                                        echo $tax_name;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldlabel" width="20%">Tax Percent</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($tax_applied=='1')
                                    {
                                        echo $tax_percent;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                            <td class="fieldlabel" width="20%">Tax Amount</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($tax_applied=='1')
                                    {
                                        echo $CURRENCY." ".$tax_amount;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr><td colspan="2"></td></tr>

                        <tr>
                            <td class="fieldlabel" width="20%">Transaction ID</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($transact_id!='')
                                    {
                                        echo $transact_id;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                            <td class="fieldlabel" width="20%">Request ID</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($request_id!='')
                                    {
                                        echo $request_id;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldlabel" width="20%">Payment Gateway</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($payment_gateway!='')
                                    {
                                        echo $payment_gateway;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                            <td class="fieldlabel" width="20%">Payment Method</td>
                            <td class="fieldarea" width="30%">
                                <?php
                                    if($payment_method!='')
                                    {
                                        echo $payment_method;
                                    }
                                    else
                                    {
                                        echo "-";
                                    }
                                ?>
                            </td>
                        </tr>

                        

                    </tbody>
                </table>
            </div>
            <!-- Modal update order details -->
            <div class="modal fade payment-summary-model" id="update_order" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered membership-plan-model" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLongTitle">Receipt ID #<?php echo $id;?> / Order #<?php echo $OrderNumber;?></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="row input-row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <h4>Order Summary</h4>
                                    </div>
                                    <br>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <!--       Order ID        -->
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <p align="right">Order#</p> 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-details">
                                            <?php echo $OrderNumber?>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <!--       Order Date        -->
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <p align="right">Order Date</p> 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-details">
                                            <?php echo $created_at?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row input-row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <!--       Status        -->
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <p align="right">Status</p> 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <?php
                                                echo $status_show;
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <!--       Total Amount        -->
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <p align="right">Total Amount Payable</p> 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-details">
                                            <?php echo $CURRENCY.' '.$total_amount?>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="row input-row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <h4>Update Order</h4>
                                    </div>
                                    <br>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <!--       Order ID        -->
                                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                            <p align="right">Date</p> 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                            <input type="text" class="form-control transaction_date" name="transaction_date" value="<?php echo date('Y-m-d');?>">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <!--       Registration Date        -->
                                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                            <p align="right">Amount</p> 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                            <input type="text" class="form-control payment_amount" value="<?php echo $total_amount;?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row input-row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <!--       Order ID        -->
                                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                            <p align="right">Payment Method</p> 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                            <select class="form-control payment_method">
                                                <option value="None">None</option>
                                                <option value="Bank Transfer">Bank Transfer</option>
                                                <option value="Credit/Debit Card <?= $CURRENCY?>">Credit/Debit Card (<?= $CURRENCY?>)</option>
                                                <option value="Cheque Payment">Cheque Payment</option>
                                                <option value="Cash Payment">Cash Payment</option>
                                                <option value="Payment Gateway">Payment Gateway</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <!--       Registration Date        -->
                                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                            <p align="right">Transaction Id</p> 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                            <input type="text" class="form-control transaction_id">
                                        </div>
                                    </div>
                                </div>

                                <div class="row input-row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <!--       Status        -->
                                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                            <p align="right">Status</p> 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                            <select class='form-control payment_status'>
                                                <?php
                                                    $status_array = array(
                                                        "0" => "Unpaid",
                                                        "1" => "Paid",
                                                        "2" => "Canceled",
                                                        "3" => "Refund"
                                                    );

                                                    foreach ($status_array as $key => $value)
                                                    {
                                                        if($status==$key)
                                                        {
                                                            echo "<option value='".$key."' selected>".$value."</option>";
                                                        }
                                                        else
                                                        {
                                                            echo "<option value='".$key."'>".$value."</option>";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <!--       Registration Date        -->
                                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                            <p align="right">Send Confirmation Email</p> 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                            <input type="checkbox" class="confirm_email" name="confirm_email">
                                        </div>
                                    </div>
                                </div>

                                <div class="row input-row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <!--       Remarks        -->
                                        <textarea class="form-control payment_remarks" rows="3" MAXLENGTH="500" placeholder="Payment Remarks(If any)  Maximum 500 Characters..."></textarea>
                                    </div>
                                </div>

                                <input type="hidden" class="order_id" value="<?php echo $OrderNumber?>">
                                <input type="hidden" class="payment_gateway" value="Manual">
                                <input type="hidden" class="request_id" value="<?php echo $request_id?>">
                                <input type="hidden" class="userid" value="<?php echo $userid?>">

                                <div class="row input-row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 payment_response_status">
                                        
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_payment_order' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="reset" class="btn btn-sm btn-default">
                                    <input type="button" class="btn btn-sm website-button btn_update_order" value="Update" style="margin-top:2px;">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php
    }
    else
    if($count_order==0)
    {
?>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 alert alert-danger">
                    <br><br>
                    <center>No records found.</center>
                    <br><br>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
            </div>
        </div>
<?php
    }
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <br><br>
        <h2>All Orders By <?= $member_profile_name?></h2>
        <table id='myorder-table' class='table-hover table-striped table-bordered mb-none my-orders' style="width:100%;">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th colspan="3"><center>Membership</center></th>
                    <th colspan="2"><center>Featured Listing</center></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <th>Receipt#</th>
                    <th style="width:70px;">Activation<br/> Date</th>
                    <th>Order#</th>
                    <th>Plan Name</th>
                    <th>Status</th>
                    <th>Expiry Date</th>
                    <th>Status</th>
                    <th>Expiry Date</th>
                    <th>Tenure</th>
                    <th>Total Price</th>
                    <th>Transaction Status</th>
                    <th>Payment Gateway</th>
                    <th><center>View</center></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $sql_order_list = "SELECT * FROM payment_transactions WHERE userid='$userid' ORDER BY created_at DESC";
                    $stmt_order_list = $link->prepare($sql_order_list);
                    $stmt_order_list->execute();
                    $count_order_list = $stmt_order_list->rowCount();
                    if($count_order_list>0)
                    {
                        $result_order_list = $stmt_order_list->fetchAll();

                        foreach ($result_order_list as $row_order_list) 
                        {
                            $id = $row_order_list['id'];
                            $OrderNumber = $row_order_list['OrderNumber'];
                            $membership_plan = $row_order_list['membership_plan'];
                            $membership_plan_name = $row_order_list['membership_plan_name'];
                            $membership_contacts = $row_order_list['membership_contacts'];
                            $membership_plan_amount = $row_order_list['membership_plan_amount'];
                            $membership_plan_expiry_date = $row_order_list['membership_plan_expiry_date'];
                            $featured_listing = $row_order_list['featured_listing'];
                            $featured_listing_amount = $row_order_list['featured_listing_amount'];
                            $featured_listing_expiry_date = $row_order_list['featured_listing_expiry_date'];
                            $tax_applied = $row_order_list['tax_applied'];
                            $tax_name = $row_order_list['tax_name'];
                            $tax_percent = $row_order_list['tax_percent'];
                            $tax_amount = $row_order_list['tax_amount'];
                            $total_amount = $row_order_list['total_amount'];
                            $tenure = $row_order_list['tenure'];
                            $transact_id = $row_order_list['transact_id'];
                            $request_id = $row_order_list['request_id'];
                            $payment_gateway = $row_order_list['payment_gateway'];
                            $payment_method = $row_order_list['payment_method'];
                            $created_at = $row_order_list['created_at'];
                            $status = $row_order_list['status'];

                            if($status=='0')
                            {
                                $status_display = "<center><b class='label label-warning'>Unpaid</b></center>";
                            }
                            else
                            if($status=='1')
                            {
                                $status_display = "<center><b class='label label-success'>Paid</b></center>";
                            }
                            else
                            if($status=='2')
                            {
                                $status_display = "<center><b class='label label-danger'>Canceled</b></center>";
                            }
                            else
                            if($status=='3')
                            {
                                $status_display = "<center><b class='label label-info'>Refund</b></center>";
                            }

                            echo "<tr>";

                            echo "<td><a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$id."</a></td>";

                            echo "<td><a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".date('d-F-Y',strtotime($created_at))."</a></td>";

                            echo "<td><a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='color:black;'>".$OrderNumber."</a></td>"; 


                                
                            if($membership_plan=='Yes' && $status=='1')
                            {
                                echo "<td>".$membership_plan_name."</td>";
                                echo "<td><center><strong style='color:green;'><i class='fa fa-check'></strong></center></td>";
                                if($membership_plan_expiry_date<$today)
                                {
                                    echo "<td><b style='color:red;'>".date('d-M-Y',strtotime($membership_plan_expiry_date))."</b></td>";
                                }
                                else
                                if($membership_plan_expiry_date>=$today)
                                {
                                    echo "<td><b style='color:#000000;'>".date('d-M-Y',strtotime($membership_plan_expiry_date))."</b></td>";
                                }
                                
                            }
                            else
                            {
                                echo "<td>".$membership_plan_name."</td>";
                                echo "<td><center><strong>-</strong></center></td>";
                                echo "<td><center><strong>-</strong></center></td>";
                            }

                            if($featured_listing=='Yes' && $status=='1')
                            {
                                echo "<td><b style='color:green;'><center><i class='fa fa-check'></center></b></td>";
                                if($featured_listing_expiry_date<$today)
                                {
                                    echo "<td><b style='color:red;'>".date('d-M-Y',strtotime($featured_listing_expiry_date))."</b></td>";
                                }
                                else
                                if($featured_listing_expiry_date>=$today)
                                {
                                    echo "<td><b style='color:#000000;'>".date('d-M-Y',strtotime($featured_listing_expiry_date))."</b></td>";
                                }
                            }
                            else
                            {
                                echo "<td><center><strong>-</strong></center></td>";
                                echo "<td><center><strong>-</strong></center></td>";
                            }

                            if($status=='1')
                            {
                                echo "<td><b style='color:#000000;'>".$tenure." Months</b></td>";
                                echo "<td><b style='color:#000000;'>".$CURRENCY.":".$total_amount."</b></td>";
                                echo "<td>".$status_display."</td>";
                                echo "<td><center><strong>".$payment_gateway."</strong></center></td>";
                            }
                            else
                            {
                                echo "<td>".$tenure." Months</td>";
                                echo "<td>".$CURRENCY.":".$total_amount."</td>";
                                echo "<td>".$status_display."</td>";
                                echo "<td><center><strong>".$payment_gateway."</strong></center></td>";
                            }

                            echo "<td><a href='member-profile.php?id=$userid&OrderNumber=$OrderNumber&main_tab=MemberAttributes&sub_tab=OrderSummary' style='background-color:Transparent;color:black;'><img src='../images/view-icon.png' style='height:25px;width:25px;'></a></td>";
                                
                            echo "</tr>";
                        }
                        
                    }
                    else
                    {
                        echo "<tr>";
                            echo "<td colspan='12'>No orders found!</td>";
                        echo "</tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>  
</div>

<script>
    $(document).ready(function(){
        /*  Datepicker setting  */
        var date_input=$('input[name="transaction_date"]');
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        
        date_input.datepicker({
          format: 'yyyy-mm-dd',
          container: container,
          todayHighlight: true,
          autoclose: true,
          orientation: "auto top",
        });

        $('.btn_update_order').click(function(){
            var transaction_date = $('.transaction_date').val();
            var payment_amount = $('.payment_amount').val();
            var payment_method = $('.payment_method').val();
            var transaction_id = $('.transaction_id').val();
            var payment_status = $('.payment_status').val();

            if($('.confirm_email').is(':checked'))
            {
                var confirm_email = '1';
            }
            else
            {
                var confirm_email = '0';
            }

            var payment_remarks = $('.payment_remarks').val();
            var order_id = $('.order_id').val();
            var payment_gateway = $('.payment_gateway').val();
            var request_id = $('.request_id').val();
            var userid = $('.userid').val();
            var task = "Update-Payment-Status";

            var data = {
                transaction_date : transaction_date,
                payment_amount : payment_amount,
                payment_method : payment_method,
                transaction_id : transaction_id,
                payment_status : payment_status,
                confirm_email : confirm_email,
                payment_remarks : payment_remarks,
                order_id : order_id,
                payment_gateway : payment_gateway,
                request_id : request_id,
                userid : userid,
                task : task
            };
            //console.log(data);return false;

            if(payment_amount=='' || payment_amount==null)
            {
                $('.payment_response_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please enter received amount.</div></center>");
                return false
            }

            if(payment_method=='' || payment_method==null)
            {
                $('.payment_response_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please select payment method.</div></center>");
                return false
            }

            if(transaction_id=='' || transaction_id==null)
            {
                $('.payment_response_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><span class='fa fa-times'></span> Empty! </strong> Please select transaction id.</div></center>");
                return false
            }

            $('.btn_update_order').attr('disabled',true);
            $('.loading_img_payment_order').show();
            $('.payment_response_status').html("");

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/purchase-orders/order_summary_helper.php',
                success:function(res)
                {
                    $('.loading_img_payment_order').hide();
                    if(res=='success')
                    {
                        $('.payment_response_status').html("<center><div class='alert alert-success payment_response_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Payment Status Data Updated Successfully.</div></center>");
                        $('.payment_response_status_success').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.reload();
                        });
                    }
                    else
                    {
                        $('.btn_update_order').attr('disabled',false);
                        $('.payment_response_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>