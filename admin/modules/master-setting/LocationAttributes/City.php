<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h3>City</h3>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-City">Add New City</button>
            </div>
        </div>
        <div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered City_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>City Name</th>
                                <!--<th>Status</th>-->
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                <center>
            </div>
            
            <!-- Modal -->
            <div class="modal fade" id="Add-New-City" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New City</h4></center>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Country :</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <select class="form control City_country_id country-select-option">
                                        <option value="0">--Select Country--</option>
                                        <?php
                                            $sql="SELECT * FROM `countries` WHERE `status`='1'";
                                            $stmt = $link->prepare($sql);
                                            $stmt->execute();
                                            $resullt = $stmt->fetchAll();

                                            foreach ($resullt as $row) 
                                            {
                                                $country_id = $row['id'];
                                                $country_name = $row['name'];

                                                echo "<option value='".$country_id."'>".$country_name."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">State :</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <select class="form control City_state_id country-select-option">
                                        <option value="0">--Select State--</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">City Name:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm City_name" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status_City">
                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn_add_new_City website-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        
        var dataTable_City_list = $('.City_list').DataTable({          //complexion datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            "ajax":{
                url :"datatables/location-attributes/City-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".City_list_processing").css("display","none");
                }
            }
        });

        //fetch all states of selected country
        $('.City_country_id').change(function(){
            var country_id = $(this).val();
            var task = "Fetch_States_Of_Country";

            var data = 'country_id='+country_id+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/location-attributes/City-helper.php',
                success:function(res)
                {
                    $('.City_state_id').html(res);
                    return false;
                }
            });
        });

        //Complexion change status
        $('.City_list tbody').on('click', '.change_status_City', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_City_status";

            var data = 'status='+status+'&id='+id+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/location-attributes/City-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.City_status'+id).html("<div class='alert alert-success City_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.City_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php?main_tab=LocationAttributes&sub_tab=City");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.City_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });

        
        //Complexion add button click
        $('.btn_add_new_City').click(function(){
            var country_id = $('.City_country_id').val();
            var state_id = $('.City_state_id').val();
            var name = $('.City_name').val();
            var task = "Add_New_City";

            if(country_id=='0')
            {
                $('.add_status_City').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select country to fetch states of the country.</div></center>");
                return false;
            }

            if(state_id=='0')
            {
                $('.add_status_City').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select state to add City.</div></center>");
                return false;
            }

            if(name=='' || name==null)
            {
                $('.add_status_City').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter City name.</div></center>");
                return false;
            }

            var data = 'state_id='+state_id+'&name='+name+'&task='+task;
            $('.loading_img').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/location-attributes/City-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status_City').html("<center><div class='alert alert-success add_status_City_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> City Added Successfully.</div></center>");
                            $('.add_status_City_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php?main_tab=LocationAttributes&sub_tab=City");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status_City').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>