<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h3>Country</h3>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Country">Add New Country</button>
            </div>
        </div>
        <div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered Country_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Short Name</th>
                                <th>Country Name</th>
                                <th>Phone Code</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                <center>
            </div>
            
            <!-- Modal -->
            <div class="modal fade" id="Add-New-Country" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Country</h4></center>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Short Name:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                                    <input type="text" class="form-control form-control-sm Country_sortname" maxlength="2" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Country Name:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm Country_name" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Phone Code:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <input type="text" class="form-control form-control-sm Country_phonecode" maxlength="5"/>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <center>
                                        <div class="alert alert-info" style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                                            <span class='fa fa-check'></span>If phone code not available then enter <strong>0</strong> in phone code text input.
                                        </div>
                                    </center>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status_Country">
                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn_add_new_Country website-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        // 1 Capitalize string - convert textbox user entered text to uppercase
        jQuery('.Country_sortname').keyup(function() {
            $(this).val($(this).val().toUpperCase());
        });

        var dataTable_Country_list = $('.Country_list').DataTable({          //complexion datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            "ajax":{
                url :"datatables/location-attributes/Country-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".Country_list_processing").css("display","none");
                }
            }
        });

        //Complexion change status
        $('.Country_list tbody').on('click', '.change_status_Country', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_Country_status";

            var data = 'status='+status+'&id='+id+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/location-attributes/Country-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.Country_status'+id).html("<div class='alert alert-success Country_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.Country_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php?main_tab=LocationAttributes&sub_tab=Country");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.Country_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });

        
        //Complexion add button click
        $('.btn_add_new_Country').click(function(){
            var sortname = $('.Country_sortname').val();
            var name = $('.Country_name').val();
            var phonecode = $('.Country_phonecode').val();

            var task = "Add_New_Country";

            if(sortname=='' || sortname==null)
            {
                $('.add_status_Country').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter country short code.</div></center>");
                return false;
            }

            if(name=='' || name==null)
            {
                $('.add_status_Country').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter country name.</div></center>");
                return false;
            }

            if(phonecode=='' || phonecode==null)
            {
                $('.add_status_Country').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter phone code.</div></center>");
                return false;
            }

            var data = 'sortname='+sortname+'&name='+name+'&phonecode='+phonecode+'&task='+task;
            $('.loading_img').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/location-attributes/Country-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status_Country').html("<center><div class='alert alert-success add_status_Country_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Country Added Successfully.</div></center>");
                            $('.add_status_Country_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php?main_tab=LocationAttributes&sub_tab=Country");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status_Country').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>