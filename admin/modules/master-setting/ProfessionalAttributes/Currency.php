<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h3>Currency</h3>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Currency">Add New Currency</button>
            </div>
        </div>
        <div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered Currency_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Country</th>
                                <th>Currency</th>
                                <th>Code</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                <center>
            </div>
            
            <!-- Modal -->
            <div class="modal fade" id="Add-New-Currency" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Currency</h4></center>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Country:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm Currency_country" placeholder="Eg: India" />
                                </div>
                            </div>
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Currency:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm Currency_currency" placeholder="Eg: Indian Rupee" />
                                </div>
                            </div>
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Code:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm Currency_code" placeholder="Eg: INR" />
                                </div>
                            </div>
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Status:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='form-control  Currency_status' name='Currency_status' checked>
                                </div>
                            </div>
                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status_Currency">
                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn_add_new_Currency website-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        var dataTable_Currency_list = $('.Currency_list').DataTable({          //complexion datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,

            "ajax":{
                url :"datatables/professional-attributes/Currency-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".Currency_list_processing").css("display","none");
                }
            }
        });

        //Complexion change status
        $('.Currency_list tbody').on('click', '.change_status_Currency', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_Currency_status";

            var data = 'status='+status+'&id='+id+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/professional-attributes/Currency-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.Currency_status'+id).html("<div class='alert alert-success Currency_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.Currency_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php?main_tab=ProfessionalAttributes&sub_tab=Currency");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.Currency_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });

        
        //Complexion add button click
        $('.btn_add_new_Currency').click(function(){
            var country = $('.Currency_country').val();
            var currency = $('.Currency_currency').val();
            var code = $('.Currency_code').val();
            var status = $('.Currency_status').is(":checked");
            if(status==true)
            {
                status='1';
            }
            else
            if(status==false)
            {
                status='0';
            }

            var task = "Add_New_Currency";

            if(country=='' || country==null)
            {
                $('.add_status_Currency').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter Country.</div></center>");
                return false;
            }

            if(currency=='' || currency==null)
            {
                $('.add_status_Currency').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter Currency.</div></center>");
                return false;
            }

            if(code=='' || code==null)
            {
                $('.add_status_Currency').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter Currency Code.</div></center>");
                return false;
            }

            var data = 'country='+country+'&currency='+currency+'&code='+code+'&status='+status+'&task='+task;
            $('.loading_img').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/professional-attributes/Currency-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status_Currency').html("<center><div class='alert alert-success add_status_Currency_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Currency Added Successfully.</div></center>");
                            $('.add_status_Currency_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php?main_tab=ProfessionalAttributes&sub_tab=Currency");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status_Currency').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>