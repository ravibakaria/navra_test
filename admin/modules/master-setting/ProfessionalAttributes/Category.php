<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h3>Category</h3>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Category">Add New Category</button>
            </div>
        </div>
        <div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered Category_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                <center>
            </div>
            
            <!-- Modal -->
            <div class="modal fade" id="Add-New-Category" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Category</h4></center>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Category:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm Category_name" />
                                </div>
                            </div>
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Status:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='form-control  Category_status' name='Category_status' checked>
                                </div>
                            </div>
                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status_Category">
                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn_add_new_Category website-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        var dataTable_Category_list = $('.Category_list').DataTable({          //complexion datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            
            "ajax":{
                url :"datatables/professional-attributes/Category-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".Category_list_processing").css("display","none");
                }
            }
        });

        //Complexion change status
        $('.Category_list tbody').on('click', '.change_status_Category', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_Category_status";

            var data = 'status='+status+'&id='+id+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/professional-attributes/Category-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.Category_status'+id).html("<div class='alert alert-success Category_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.Category_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php?main_tab=ProfessionalAttributes&sub_tab=Category");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.Category_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });

        
        //Complexion add button click
        $('.btn_add_new_Category').click(function(){
            var name = $('.Category_name').val();
            var status = $('.Category_status').is(":checked");
            if(status==true)
            {
                status='1';
            }
            else
            if(status==false)
            {
                status='0';
            }

            var task = "Add_New_Category";

            if(name=='' || name==null)
            {
                $('.add_status_Category').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter Category.</div></center>");
                return false;
            }

            var data = 'name='+name+'&status='+status+'&task='+task;
            $('.loading_img').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/professional-attributes/Category-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status_Category').html("<center><div class='alert alert-success add_status_Category_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Category Added Successfully.</div></center>");
                            $('.add_status_Category_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php?main_tab=ProfessionalAttributes&sub_tab=Category");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status_Category').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>