
	<div class="container inner-tab">
		<br/>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<ul class="nav data-tabs nav-tabs" role="tablist">
				  	<li class="<?php if(($main_tab=='PersonalAttributes' || $main_tab==null) && ($sub_tab=='BodyType' || $sub_tab==null)) { echo 'active';}?>"><a href="#BodyType" role="tab" data-toggle="tab" class="tab-links">Body Type </a></li>
				  	<li class="<?php if(($main_tab=='PersonalAttributes') && $sub_tab=='Complexion') { echo 'active';}?>"><a href="#Complexion" role="tab" data-toggle="tab" class="tab-links">Complexion</a></li>
				  	<li class="<?php if(($main_tab=='PersonalAttributes') && $sub_tab=='EatingHabbit') { echo 'active';}?>"><a href="#EatingHabbit" role="tab" data-toggle="tab" class="tab-links">Eating Habbit</a></li>
				  	<li class="<?php if(($main_tab=='PersonalAttributes') && $sub_tab=='MaritalStatus') { echo 'active';}?>"><a href="#MaritalStatus" role="tab" data-toggle="tab" class="tab-links">Marital Status</a></li>
				  	<li class="<?php if(($main_tab=='PersonalAttributes') && $sub_tab=='SmokingHabbit') { echo 'active';}?>"><a href="#SmokingHabbit" role="tab" data-toggle="tab" class="tab-links">Smoking Habbit</a></li>
				  	<li class="<?php if(($main_tab=='PersonalAttributes') && $sub_tab=='DrinkingHabbit') { echo 'active';}?>"><a href="#DrinkingHabbit" role="tab" data-toggle="tab" class="tab-links">Drinking Habbit</a></li>
				  	<li class="<?php if(($main_tab=='PersonalAttributes') && $sub_tab=='SpecialCase') { echo 'active';}?>"><a href="#SpecialCase" role="tab" data-toggle="tab" class="tab-links">Special Case</a></li>
				  	<li class="<?php if(($main_tab=='PersonalAttributes') && $sub_tab=='Gender') { echo 'active';}?>"><a href="#Gender" role="tab" data-toggle="tab" class="tab-links">Gender</a></li>
				</ul>
				<div class="tab-content">
					<!-- BodyType Setup Start-->
			  		<div class="tab-pane <?php if($main_tab==null && $sub_tab==null) { echo 'active';}?>" id="BodyType">
			  			<?php include("modules/master-setting/PersonalAttributes/BodyType.php");?>
			  		</div>

			  		<!-- Complexion Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='PersonalAttributes') && $sub_tab=='Complexion') { echo 'active';}?>" id="Complexion">
			  			<?php include("modules/master-setting/PersonalAttributes/complexion.php");?>
			  		</div>

			  		<!-- EatingHabbit Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='PersonalAttributes') && $sub_tab=='EatingHabbit') { echo 'active';}?>" id="EatingHabbit">
			  			<?php include("modules/master-setting/PersonalAttributes/EatingHabbit.php");?>
			  		</div>

			  		<!-- MaritalStatus Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='PersonalAttributes') && $sub_tab=='MaritalStatus') { echo 'active';}?>" id="MaritalStatus">
			  			<?php include("modules/master-setting/PersonalAttributes/MaritalStatus.php");?>
			  		</div>

			  		<!-- SmokingHabbit Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='PersonalAttributes') && $sub_tab=='SmokingHabbit') { echo 'active';}?>" id="SmokingHabbit">
			  			<?php include("modules/master-setting/PersonalAttributes/SmokingHabbit.php");?>
			  		</div>

			  		<!-- DrinkingHabbit Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='PersonalAttributes') && $sub_tab=='DrinkingHabbit') { echo 'active';}?>" id="DrinkingHabbit">
			  			<?php include("modules/master-setting/PersonalAttributes/DrinkingHabbit.php");?>
			  		</div>

			  		<!-- SpecialCase Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='PersonalAttributes') && $sub_tab=='SpecialCase') { echo 'active';}?>" id="SpecialCase">
			  			<?php include("modules/master-setting/PersonalAttributes/SpecialCase.php");?>
			  		</div>

			  		<!-- Gender Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='PersonalAttributes') && $sub_tab=='Gender') { echo 'active';}?>" id="Gender">
			  			<?php include("modules/master-setting/PersonalAttributes/Gender.php");?>
			  		</div>
			  	</div>
			</div>
		</div>
	</div>
