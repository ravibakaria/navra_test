<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3>Smoking Habbit</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-SmokingHabbit">Add New Smoking Habbit</button>
            </div>
        </div>
        <div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered SmokingHabbit_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $sql = "SELECT * FROM `smokehabbit`";
                                $stmt1   = $link->prepare($sql);
                                $stmt1->execute();
                                $queryTot = $stmt1->rowCount();
                                $result = $stmt1->fetchAll();
                                foreach( $result as $row )
                                {
                                    echo "<tr>";
                                        echo "<td>".$row['id']."</td>";
                                        echo "<td>".$row['name']."</td>";
                                        $id = $row['id'];
                                        $name = $row['name'];
                                        $status = $row['status'];
                                        if($status=='0')
                                        {
                                            echo "<td> <input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' data-size='mini' class='SmokingHabbit_current_status' value='".$status.",".$id."'>
                                                <div class='SmokingHabbit_status".$id."'></div>
                                                </td>";
                                        }
                                        else
                                        if($status=='1')
                                        {
                                            echo "<td> <input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' data-size='mini' class='SmokingHabbit_current_status' checked  value='".$status.",".$id."'>
                                                <div class='SmokingHabbit_status".$id."'></div>
                                                </td>";
                                        }
                                        echo "<td><a href='update-Smoking-Habbit-attributes.php?id=$id' type='button' class='btn btn-sm btn-primary website-button'>Edit</a></td>";
                                    echo "</tr>";
                                
                                }
                            ?>
                        </tbody>
                    </table>
                <center>
            </div>
            
            <!-- Modal -->
            <div class="modal fade" id="Add-New-SmokingHabbit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Smoking Habbit</h4></center>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Smoking Habbit:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm SmokingHabbit_name" />
                                </div>
                            </div>
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Status:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='form-control  SmokingHabbit_status' name='SmokingHabbit_status' checked>
                                </div>
                            </div>
                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status_SmokingHabbit">
                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn_add_new_SmokingHabbit  website-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        var dataTable_SmokingHabbit_list = $('.SmokingHabbit_list').DataTable({     //SmokingHabbit datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            
            "ajax":{
                url :"datatables/personal-attributes/smoking-habbit-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".SmokingHabbit_list_processing").css("display","none");
                }
            }
        });

        //Smoking Habbit change status
        $('.SmokingHabbit_list tbody').on('click', '.change_status_SmokingHabbit', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_SmokingHabbit_status";

            var data = 'status='+status+'&id='+id+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/personal-attributes/SmokingHabbit-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.SmokingHabbit_current_status').attr('disabled',true);
                        $('.SmokingHabbit_status'+id).html("<div class='alert alert-success SmokingHabbit_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.SmokingHabbit_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php?main_tab=PersonalAttributes&sub_tab=SmokingHabbit");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.SmokingHabbit_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });

        //Complexion add button click
        $('.btn_add_new_SmokingHabbit').click(function(){
            var name = $('.SmokingHabbit_name').val();
            var status = $('.SmokingHabbit_status').is(":checked");
            if(status==true)
            {
                status='1';
            }
            else
            if(status==false)
            {
                status='0';
            }

            var task = "Add_New_SmokingHabbit";

            if(name=='' || name==null)
            {
                $('.add_status_SmokingHabbit').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter smoking habbit.</div></center>");
                return false;
            }

            var data = 'name='+name+'&status='+status+'&task='+task;
            $('.loading_img').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/personal-attributes/SmokingHabbit-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status_SmokingHabbit').html("<center><div class='alert alert-success add_status_SmokingHabbit_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Smoking Habbit Added Successfully.</div></center>");
                            $('.add_status_SmokingHabbit_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php?main_tab=PersonalAttributes&sub_tab=SmokingHabbit");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status_SmokingHabbit').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>