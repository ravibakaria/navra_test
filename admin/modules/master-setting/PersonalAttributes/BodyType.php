<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3>Body Type</h3>
            </div>     
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">           
                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Body-Type">Add New Body Type</button>
            </div>
        </div>
        <div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered body_type_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                <center>
            </div>
            
            <!-- Modal -->
            <div class="modal fade" id="Add-New-Body-Type" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Body Type</h4></center>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Body Type:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm body_type_name" />
                                </div>
                            </div>
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Status:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='form-control  body_type_status' name='body_type_status' checked>
                                </div>
                            </div>
                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status">
                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn_add_new_body_type website-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
       var dataTable_body_type_list = $('.body_type_list').DataTable({          //Body Type datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            
            "ajax":{
                url :"datatables/personal-attributes/body-type-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".body_type_list_processing").css("display","none");
                }
            }
        });
        
        //Body type change status
        $('.body_type_list tbody').on('click', '.change_status_body_type', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_Body_Type_status";

            var data = 'status='+status+'&id='+id+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/personal-attributes/body-type-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.body_type_status'+id).html("<div class='alert alert-success body_type_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.body_type_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.body_type_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });

        //Body Type add button click
        $('.btn_add_new_body_type').click(function(){
            var name = $('.body_type_name').val();
            var status = $('.body_type_status').is(":checked");
            if(status==true)
            {
                status='1';
            }
            else
            if(status==false)
            {
                status='0';
            }

            var task = "Add_New_Body_Type";

            if(name=='' || name==null)
            {
                $('.add_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter body type.</div></center>");
                return false;
            }

            var data = 'name='+name+'&status='+status+'&task='+task;
            $('.loading_img').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/master-data/personal-attributes/body-type-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status').html("<center><div class='alert alert-success add_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Body Type Added Successfully.</div></center>");
                            $('.add_status_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("master-data-setup.php");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
        
    });
</script>