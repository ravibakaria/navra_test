<div class="container inner-tab">
	<br/>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<ul class="nav data-tabs nav-tabs" role="tablist">
			  	<li class="<?php if(($main_tab=='LocationAttributes' || $main_tab==null) && ($sub_tab=='Country' || $sub_tab==null)) { echo 'active';}?>"><a href="#Country" role="tab" data-toggle="tab" class="tab-links">Country </a></li>

			  	<li class="<?php if(($main_tab=='LocationAttributes') && $sub_tab=='State') { echo 'active';}?>"><a href="#State" role="tab" data-toggle="tab" class="tab-links">State</a></li>

			  	<li class="<?php if(($main_tab=='LocationAttributes') && $sub_tab=='City') { echo 'active';}?>"><a href="#City" role="tab" data-toggle="tab" class="tab-links">City</a></li>
		  	</ul>
			<div class="tab-content">
				<!-- Country Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='LocationAttributes' || $main_tab==null) && ($sub_tab=='Country' || $sub_tab==null)) { echo 'active';}?>" id="Country">
		  			<?php include("modules/master-setting/LocationAttributes/Country.php");?>
		  		</div>

		  		<!-- State Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='LocationAttributes') && $sub_tab=='State') { echo 'active';}?>" id="State">
		  			<?php include("modules/master-setting/LocationAttributes/State.php");?>
		  		</div>

		  		<!-- City Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='LocationAttributes') && $sub_tab=='City') { echo 'active';}?>" id="City">
		  			<?php include("modules/master-setting/LocationAttributes/City.php");?>
		  		</div>
		  	</div>
		</div>
	</div>
</div>