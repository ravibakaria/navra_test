<div class="container inner-tab">
	<br/>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<ul class="nav data-tabs nav-tabs" role="tablist">
			  	<li class="<?php if(($main_tab=='ProfessionalAttributes' || $main_tab==null) && ($sub_tab=='Education' || $sub_tab==null)) { echo 'active';}?>"><a href="#Education" role="tab" data-toggle="tab" class="tab-links">Education </a></li>
			  	<li class="<?php if(($main_tab=='ProfessionalAttributes') && $sub_tab=='Employment') { echo 'active';}?>"><a href="#Employment" role="tab" data-toggle="tab" class="tab-links">Employment</a></li>
			  	<li class="<?php if(($main_tab=='ProfessionalAttributes') && $sub_tab=='Category') { echo 'active';}?>"><a href="#Category" role="tab" data-toggle="tab" class="tab-links">Category</a></li>
			  	<li class="<?php if(($main_tab=='ProfessionalAttributes') && $sub_tab=='Designation') { echo 'active';}?>"><a href="#Designation" role="tab" data-toggle="tab" class="tab-links">Designation</a></li>
			  	<li class="<?php if(($main_tab=='ProfessionalAttributes') && $sub_tab=='Industry') { echo 'active';}?>"><a href="#Industry" role="tab" data-toggle="tab" class="tab-links">Industry</a></li>
			  	<li class="<?php if(($main_tab=='ProfessionalAttributes') && $sub_tab=='Currency') { echo 'active';}?>"><a href="#Currency" role="tab" data-toggle="tab" class="tab-links">Currency</a></li>
			</ul>
			<div class="tab-content">
				<!-- Education Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='ProfessionalAttributes' || $main_tab==null) && ($sub_tab=='Education' || $sub_tab==null)) { echo 'active';}?>" id="Education">
		  			<?php include("modules/master-setting/ProfessionalAttributes/Education.php");?>
		  		</div>

		  		<!-- Employment Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='ProfessionalAttributes') && $sub_tab=='Employment') { echo 'active';}?>" id="Employment">
		  			<?php include("modules/master-setting/ProfessionalAttributes/Employment.php");?>
		  		</div>

		  		<!-- Category Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='ProfessionalAttributes') && $sub_tab=='Category') { echo 'active';}?>" id="Category">
		  			<?php include("modules/master-setting/ProfessionalAttributes/Category.php");?>
		  		</div>

		  		<!-- Designation Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='ProfessionalAttributes') && $sub_tab=='Designation') { echo 'active';}?>" id="Designation">
		  			<?php include("modules/master-setting/ProfessionalAttributes/Designation.php");?>
		  		</div>

		  		<!-- Industry Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='ProfessionalAttributes') && $sub_tab=='Industry') { echo 'active';}?>" id="Industry">
		  			<?php include("modules/master-setting/ProfessionalAttributes/Industry.php");?>
		  		</div>

		  		<!-- Currency Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='ProfessionalAttributes') && $sub_tab=='Currency') { echo 'active';}?>" id="Currency">
		  			<?php include("modules/master-setting/ProfessionalAttributes/Currency.php");?>
		  		</div>

		  	</div>
		</div>
	</div>
</div>