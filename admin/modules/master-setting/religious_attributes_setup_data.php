<div class="container inner-tab">
	<br/>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<ul class="nav data-tabs nav-tabs" role="tablist">
			  	<li class="<?php if(($main_tab=='ReligiousAttributes' || $main_tab==null) && ($sub_tab=='Religion' || $sub_tab==null)) { echo 'active';}?>"><a href="#Religion" role="tab" data-toggle="tab" class="tab-links">Religion </a></li>

			  	<li class="<?php if(($main_tab=='ReligiousAttributes') && $sub_tab=='Caste') { echo 'active';}?>"><a href="#Caste" role="tab" data-toggle="tab" class="tab-links">Caste</a></li>

			  	<li class="<?php if(($main_tab=='ReligiousAttributes') && $sub_tab=='MotherTongue') { echo 'active';}?>"><a href="#MotherTongue" role="tab" data-toggle="tab" class="tab-links">Mother Tongue</a></li>
		  	</ul>
			<div class="tab-content">
				<!-- Religion Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='ReligiousAttributes' || $main_tab==null) && ($sub_tab=='Religion' || $sub_tab==null)) { echo 'active';}?>" id="Religion">
		  			<?php include("modules/master-setting/ReligiousAttributes/Religion.php");?>
		  		</div>

		  		<!-- Caste Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='ReligiousAttributes') && $sub_tab=='Caste') { echo 'active';}?>" id="Caste">
		  			<?php include("modules/master-setting/ReligiousAttributes/Caste.php");?>
		  		</div>

		  		<!-- MotherTongue Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='ReligiousAttributes') && $sub_tab=='MotherTongue') { echo 'active';}?>" id="MotherTongue">
		  			<?php include("modules/master-setting/ReligiousAttributes/MotherTongue.php");?>
		  		</div>
		  	</div>
		</div>
	</div>
</div>