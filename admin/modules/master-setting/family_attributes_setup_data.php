<div class="container inner-tab">
	<br/>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<ul class="nav data-tabs nav-tabs" role="tablist">
			  	<li class="<?php if(($main_tab=='FamilyAttributes' || $main_tab==null) && ($sub_tab=='FamilyValue' || $sub_tab==null)) { echo 'active';}?>"><a href="#FamilyValue" role="tab" data-toggle="tab" class="tab-links">Family Value </a></li>

			  	<li class="<?php if(($main_tab=='FamilyAttributes') && $sub_tab=='FamilyType') { echo 'active';}?>"><a href="#FamilyType" role="tab" data-toggle="tab" class="tab-links">Family Type</a></li>

			  	<li class="<?php if(($main_tab=='FamilyAttributes') && $sub_tab=='FamilyStatus') { echo 'active';}?>"><a href="#FamilyStatus" role="tab" data-toggle="tab" class="tab-links">Family Status</a></li>
		  	</ul>
			<div class="tab-content">
				<!-- FamilyValue Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='FamilyAttributes' || $main_tab==null) && ($sub_tab=='FamilyValue' || $sub_tab==null)) { echo 'active';}?>" id="FamilyValue">
		  			<?php include("modules/master-setting/FamilyAttributes/FamilyValue.php");?>
		  		</div>

		  		<!-- FamilyType Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='FamilyAttributes') && $sub_tab=='FamilyType') { echo 'active';}?>" id="FamilyType">
		  			<?php include("modules/master-setting/FamilyAttributes/FamilyType.php");?>
		  		</div>

		  		<!-- FamilyStatus Setup Start-->
		  		<div class="tab-pane <?php if(($main_tab=='FamilyAttributes') && $sub_tab=='FamilyStatus') { echo 'active';}?>" id="FamilyStatus">
		  			<?php include("modules/master-setting/FamilyAttributes/FamilyStatus.php");?>
		  		</div>
		  	</div>
		</div>
	</div>
</div>