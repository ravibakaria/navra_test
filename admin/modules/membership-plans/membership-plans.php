<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<div class="row">
	            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
	                <h2>Membership Plan Setting</h2>
	            </div>
	            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
	                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Membership-Plan">Add New Membership Plan</button>
	            </div>
	        </div>
	        <br/>
	    	<div class="row">
	            <div>
	                <center>
	                    <table id='datatable-info' class='table-hover table-striped table-bordered membership_plans_list' style="width:100%">
	                        <thead>
	                            <tr>
	                                <th>ID</th>
	                                <th>Plan Name</th>
	                                <th>Number Of Contacts</th>
	                                <th>Number Of Months</th>
	                                <th>Price</th>
	                                <th>Status</th>
	                                <th>Action</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	<?php
						    		if($count>0)
						    		{
						    			foreach ($result as $row) 
						    			{
						    				$id = $row['id'];
						    				$membership_plan_name = $row['membership_plan_name'];
										    $number_of_contacts = $row['number_of_contacts'];
										    $number_of_duration_months = $row['number_of_duration_months'];
										    $price = $row['price'];
										    $status = $row['status'];

										    if($status=='1')
										    {
										    	$display_status = "<b class='label label-success text-right' style='text-align:right;'>Active</b>";
										    }
										    else
										    if($status=='0')
										    {
										    	$display_status = "<b class='label label-danger text-right' style='text-align:right;'>Disabled</b>";
										    }

										    echo "<tr>";
										    	echo "<td><a href='update-membership-plan-setup.php?id=$id' style='color:black;text-decoration:none;'>".$id."</a></td>";
										    	echo "<td><a href='update-membership-plan-setup.php?id=$id' style='color:black;text-decoration:none;'>".$membership_plan_name."</a></td>";
										    	echo "<td><a href='update-membership-plan-setup.php?id=$id' style='color:black;text-decoration:none;'>".$number_of_contacts."</a></td>";
										    	echo "<td><a href='update-membership-plan-setup.php?id=$id' style='color:black;text-decoration:none;'>".$number_of_duration_months."</a></td>";
										    	echo "<td><a href='update-membership-plan-setup.php?id=$id' style='color:black;text-decoration:none;'>".$price." &nbsp;[".$DefaultCurrencyCode."] </a></td>";

										    	echo "<td><a href='javascript:void(0)' class='change_status_membership_plan' value='$status,$id' id='$status,$id' style='text-decoration:none;'>".$display_status."</a>
										    		<div class='membership_plan_status".$id."'></div>
										    	</td>";

										    	echo "<td><a href='update-membership-plan-setup.php?id=$id'><i class='fa fa-pencil fa-2x edit-fa'></i></a></td>";
										    echo "</tr>";
						    			}
						    		}
						    		else
						    		{
						    			echo "<tr>";
						    				echo "<td colspan='4'>No records found!</td>";
						    			echo "</tr>";
						    		}
						    	?>
	                        </tbody>
	                    </table>
	                <center>
	            </div>
	        </div>

	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        	<!-- Modal Add new membership plan -->
	            <div class="modal fade" id="Add-New-Membership-Plan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	                <div class="modal-dialog modal-dialog-centered membership-plan-model" role="document">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Membership Plan</h4></center>
	                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                            </button>
	                        </div>
	                        <div class="modal-body">
	                            <div class="row input-row">
									<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
										Plan Name:
									</div>
									<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
										<input type='text' class="form-control membership_plan_name">
									</div>
									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="alert alert-info" style='padding: 10px; margin-bottom: 10px;'><span class='fa fa-exclamation-circle'></span> Please do not use underscore(_) in plan name.</div>
									</div>
								</div>

								<div class="row input-row">
									<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
										Number Of Contacts:
									</div>
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
										<input type='number' class="form-control number_of_contacts">
									</div>
									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<div class="alert alert-info" style='padding: 10px; margin-bottom: 10px;'><span class='fa fa-exclamation-circle'></span> Put 0(zero) for unlimited contacts.</div>
									</div>
								</div>

								<div class="row input-row">
									<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
										Number Of Months:
									</div>
									<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
										<select class="number_of_duration_months form-control">
											<option value="0">--Select--</option>
											<?php
												for($i=1;$i<=12;$i++)
												{
													echo "<option value='".$i."'>".$i." Month</option>";
												}
											?>
										</select>
									</div>
								</div>

								<div class="row input-row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										Price: <?php echo "[".$DefaultCurrencyCode."]";?>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<input type='number' class="form-control price">
									</div>
								</div>

								<div class="row input-row">
	                                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
	                                    <lable class="control-label">Status:</lable>
	                                </div>
	                                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
	                                    <input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='form-control  plan_status' name='plan_status' checked>
	                                </div>
	                            </div>

	                            <div class="row input-row">
	                            	<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>

	                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status_membership_plans">
	                                    
	                                </div>
	                            </div>
	                        </div>
	                        <div class="modal-footer">
	                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                            <button type="button" class="btn btn-primary btn_add_new_meta_tag website-button">Add</button>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

<script>
    $(document).ready(function(){
    	/*   Prevent entering charaters in number of contacts text box   */
        $(".number_of_contacts").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.add_status_membership_plans').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Only numbers allowed.</div>").show().fadeOut(3000);
                return false;
            }
        })

        /*   Prevent entering charaters in price text box   */
        $(".price").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.add_status_membership_plans').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Only numbers allowed.</div>").show().fadeOut(3000);
                return false;
            }
        })

    	$('.btn_add_new_meta_tag').click(function(){
    		var membership_plan_name = $('.membership_plan_name').val();
    		var number_of_contacts = $('.number_of_contacts').val();
    		var number_of_duration_months = $('.number_of_duration_months').val();
    		var price = $('.price').val();
    		var status = $('.plan_status').is(":checked");
    		var task = "Add-New-Membership-Plan";

    		if(status==true)
            {
                status='1';
            }
            else
            if(status==false)
            {
                status='0';
            }

            if(membership_plan_name=='' || membership_plan_name==null)
            {
            	$('.add_status_membership_plans').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong> Please enter membership plan.</div></center>");
                return false;
            }

            if(number_of_contacts=='' || number_of_contacts==null)
            {
            	$('.add_status_membership_plans').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong> Please enter number of contacts.</div></center>");
                return false;
            }

            if(number_of_duration_months=='0')
            {
            	$('.add_status_membership_plans').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong> Please select number of months.</div></center>");
                return false;
            }

            if(price=='' || price==null)
            {
            	$('.add_status_membership_plans').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong> Please enter price for this plan.</div></center>");
                return false;
            }

            $('.add_status_membership_plans').html("");

            var data = 'membership_plan_name='+membership_plan_name+'&number_of_contacts='+number_of_contacts+'&number_of_duration_months='+number_of_duration_months+'&price='+price+'&status='+status+'&task='+task;

            $('.loading_img').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/membership-plan/membership-plan-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status_membership_plans').html("<center><div class='alert alert-success add_status_membership_plans_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Membership Plan Added Successfully.</div></center>");
                            $('.add_status_membership_plans_success').fadeTo(2000, 500).slideUp(500, function(){
                                  window.location.assign("membership-plan-setup.php?main_tab=MembershipPlans&sub_tab=MembershipPlans_Setup");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status_membership_plans').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
    	});

    	//Membership plan change status
        $('.change_status_membership_plan').click(function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_membership_plan_status";

            var data = 'status='+status+'&id='+id+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/membership-plan/membership-plan-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                    	$('.membership_plan_status'+id).html("<div class='alert alert-success membership_plan_status_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.membership_plan_status_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("membership-plan-setup.php?main_tab=MembershipPlans&sub_tab=MembershipPlans_Setup");
                                  return false;
                            });
                        
                    }
                    else
                    {
                        $('.membership_plan_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });
    });
</script>