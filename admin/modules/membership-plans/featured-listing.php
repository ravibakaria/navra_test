<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt   = $link->prepare("SELECT * FROM `featured_listing_plan`");
	$stmt->execute();
	$result = $stmt->fetch();
	$count=$stmt->rowCount();

	$FeaturedMembershipAddOrNot = $result['FeaturedMembershipAddOrNot'];
    $FeaturdListingPrice = $result['FeaturdListingPrice'];
?>
	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	                <h2>Featured Listing Setting</h2>
	            </div>
	            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	                
	            </div>
	        </div>
	        <br/>
	        <div class="row input-row">
                <div class="col-xs-10 col-sm-10 col-md-4 col-lg-4">
                    Add Featured Membership in Membersip Plan:
                </div>
                <div class="col-xs-2 col-sm-2 col-md-4 col-lg-4">
                    <input type="checkbox" class="FeaturedMembershipAddOrNot" <?php if($count>0 && ($FeaturedMembershipAddOrNot=='1')) { echo 'checked';}?>>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This featured listing plan will be activated in all membership plan.
                    </div>
                </div>
            </div>
            <div class="row input-row FeaturedMembershipData" <?php if($FeaturedMembershipAddOrNot!='1') { echo 'style=display:none;';}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Featured Listing Price:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <input type="number" class="form-control FeaturdListingPrice" value="<?php echo @$FeaturdListingPrice; ?>">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This featured listing plan price is for 30 days.<br/>
                        If member take membership plan for 90 days then this featured listing price will be (<i>price*3</i>)
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 featured_listing_status">
	                
	            </div>
	            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
	                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_featured_listing' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
	                <center><button class="btn btn-success btn-sm btn_submit_featured_listing website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
	            </div>
	        </div>
	    </div>
    </div>

<script>
    $(document).ready(function(){
    	/*   Prevent entering charaters in number of contacts text box   */
        $(".FeaturdListingPrice").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.featured_listing_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Only numbers allowed.</div>").show().fadeOut(3000);
                return false;
            }
        })

    	/*    FeaturedMembershipAddOrNot start   */
            $('.FeaturedMembershipAddOrNot').change(function(){

                if($(this).is(':checked'))
                {
                    var FeaturedMembershipAddOrNot = '1';
                }
                else
                {
                    var FeaturedMembershipAddOrNot = '0';
                }

                if(FeaturedMembershipAddOrNot=='1')
                {
                    $('.FeaturedMembershipData').show();
                    return false;
                }
                else
                {
                    $('.FeaturedMembershipData').hide();
                    return false;
                }    
            });
        /*    FeaturedMembershipAddOrNot end   */

        $('.btn_submit_featured_listing').click(function(){
        	if($('.FeaturedMembershipAddOrNot').is(':checked'))
            {
                var FeaturedMembershipAddOrNot = '1';
            }
            else
            {
                var FeaturedMembershipAddOrNot = '0';
            }

            var FeaturdListingPrice = $('.FeaturdListingPrice').val();
            var task = "update-featured-listing-plan";

            if(FeaturedMembershipAddOrNot=='1' && (FeaturdListingPrice=='' || FeaturdListingPrice==null))
            {
            	$('.featured_listing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter featured listing price.</div></center>");
                    return false
            }

            $('.featured_listing_status').html("");
            $('.loading_img_featured_listing').show();

            var data = 'FeaturedMembershipAddOrNot='+FeaturedMembershipAddOrNot+'&FeaturdListingPrice='+FeaturdListingPrice+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/membership-plan/membership-plan-helper.php',
                success:function(res)
                {
                    $('.loading_img_featured_listing').hide();
                    if(res=='success')
                    {
                        $('.featured_listing_status').html("<center><div class='alert alert-success featured_listing_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Featured Listing Plan Updated Successfully.</div></center>");
                            $('.featured_listing_status_success').fadeTo(2000, 500).slideUp(500, function(){
                                  window.location.assign("membership-plan-setup.php?main_tab=MembershipPlans&sub_tab=FeaturedListing_Setup");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.featured_listing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>