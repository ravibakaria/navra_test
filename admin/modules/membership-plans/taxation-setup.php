<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt   = $link->prepare("SELECT * FROM `taxation_setup`");
	$stmt->execute();
	$result = $stmt->fetch();
	$count=$stmt->rowCount();

	$taxationAddOrNot = $result['taxationAddOrNot'];
    $taxation_Name = $result['taxation_Name'];
    $taxationPercent = $result['taxationPercent'];
?>
	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	                <h2>Taxation Setting</h2>
	            </div>
	            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	                
	            </div>
	        </div>
	        <br/>
	        <div class="row input-row">
                <div class="col-xs-10 col-sm-10 col-md-4 col-lg-4">
                    Add Taxation in Membersip Plan:
                </div>
                <div class="col-xs-2 col-sm-2 col-md-4 col-lg-4">
                    <input type="checkbox" class="taxationAddOrNot" <?php if($count>0 && ($taxationAddOrNot=='1')) { echo 'checked';}?>>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This tax will be applied in all membership/featured listing plan.
                    </div>
                </div>
            </div>
            <div class="row input-row taxation_Data" <?php if($taxationAddOrNot!='1') { echo 'style=display:none;';}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Taxation Name:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <input type="text" class="form-control taxation_Name" value="<?php echo @$taxation_Name; ?>">
                </div>
            </div>
            <div class="row input-row taxation_Data" <?php if($taxationAddOrNot!='1') { echo 'style=display:none;';}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Tax Percentage:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <input type="number" class="form-control taxationPercent" value="<?php echo @$taxationPercent; ?>">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This tax percentage will be calculated on final total amount of membership plan + Featured listing plan.
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 taxation_status">
	                
	            </div>
	            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
	                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_taxation' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
	                <center><button class="btn btn-success btn-sm btn_submit_taxation website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
	            </div>
	        </div>
	    </div>
    </div>

<script>
    $(document).ready(function(){
    	/*   Prevent entering charaters in taxation percentage text box   */
        $(".taxationPercent").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.taxation_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Only numbers allowed.</div>").show().fadeOut(3000);
                return false;
            }
        })

        /*    taxationAddOrNot start   */
            $('.taxationAddOrNot').change(function(){

                if($(this).is(':checked'))
                {
                    var taxationAddOrNot = '1';
                }
                else
                {
                    var taxationAddOrNot = '0';
                }

                if(taxationAddOrNot=='1')
                {
                    $('.taxation_Data').show();
                    return false;
                }
                else
                {
                    $('.taxation_Data').hide();
                    return false;
                }    
            });
        /*    taxationAddOrNot end   */

        $('.btn_submit_taxation').click(function(){
        	if($('.taxationAddOrNot').is(':checked'))
            {
                var taxationAddOrNot = '1';
            }
            else
            {
                var taxationAddOrNot = '0';
            }

            var taxation_Name = $('.taxation_Name').val();
            var taxationPercent = $('.taxationPercent').val();
            var task = "Update-Taxation-Setup";

            if(taxationAddOrNot=='1' && (taxation_Name=='' || taxation_Name==null))
            {
            	$('.taxation_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter tax name.</div></center>");
                return false
            }

            if(taxationAddOrNot=='1' && (taxationPercent=='' || taxationPercent==null))
            {
            	$('.taxation_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter tax percentage.</div></center>");
                return false
            }

            var data = 'taxationAddOrNot='+taxationAddOrNot+'&taxation_Name='+taxation_Name+'&taxationPercent='+taxationPercent+'&task='+task;
            $('.taxation_status').html("");
            $('loading_img_taxation').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/membership-plan/membership-plan-helper.php',
                success:function(res)
                {
                    $('.loading_img_taxation').hide();
                    if(res=='success')
                    {
                        $('.taxation_status').html("<center><div class='alert alert-success taxation_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Taxation Updated Successfully.</div></center>");
                            $('.taxation_status_success').fadeTo(2000, 500).slideUp(500, function(){
                                  window.location.assign("membership-plan-setup.php?main_tab=MembershipPlans&sub_tab=Taxation_Setup");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.taxation_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>