<br/>
<div class="row vertical-divider vertical-devider-admin">
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<h4>Profile Photo</h4>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row profile-row">
				<?php
					$stmtprofile_pic = $link->prepare("SELECT * FROM `profilepic` WHERE `userid`='$user_id'"); 
				    $stmtprofile_pic->execute();
				    $count_user_profile_pic=$stmtprofile_pic->rowCount();
				    $result_profile_pic = $stmtprofile_pic->fetch();
				    
				    $profile_pic = '../users/uploads/'.$user_id.'/profile/'.$result_profile_pic['photo'];
				    $created_at = $result_profile_pic['created_at'];
				    $updated_at = $result_profile_pic['updated_at'];

				    if($updated_at!='' || $updated_at!=null || $updated_at!='0000-00-00 00:00:00')
			    	{
			    		$last_updated_on = $updated_at;
			    	}
			    	else
			    	{
			    		$last_updated_on = $created_at;
			    	}

				    if($count_user_profile_pic>0)
				    {
				?>
					<div class="isotope-item document">
							<div class="thumbnail">
								<div class="thumb-preview">
									<a class="thumb-image" href="<?php echo $profile_pic;?>">
										<img src="<?php echo $profile_pic;?>" class="img-responsive member-photos">
									</a>
								</div>
								<div class="mg-description">
									<small class="pull-left">Uploaded On</small>
									<small class="pull-right"><?php echo date('d-M-Y H:i:A',strtotime($last_updated_on));?></small>
								</div><br/>
								<div class="row">
									<center><img src="../images/loader/loader.gif" class='img-responsive loading_img_profile_pic'' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
								</div><br/>
								<div class="row">
									<center><a class="btn btn-danger btn-sm member_profile_photo_delete" ><i class="fa fa-trash-o"></i> Delete Profile Photo</a></center>
								</div>
							</div>
							<input type="hidden" class="user_id" value="<?php echo $user_id;?>">
						</div>
				<?php
					}
					else
					{
						echo "<div class='alert alert-danger'>Profile photo not uploaded by member</div>";
					}
				?>
			</div>
			<div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    				<div class="member-profile-photo-update-status"></div>
    			</div>
    		</div>
		</div>
	</div>
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
    	<h4>Personal Photos</h4>
    			
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    		<div class="row profile-row">
    			<?php
    				$sql_user_photos = "SELECT * FROM usergallery WHERE userid = '$user_id' AND status<>'2'";
    				$stmt_user_photos = $link->prepare($sql_user_photos);
    				$stmt_user_photos->execute();
    				$count_user_photos = $stmt_user_photos->rowCount();
    				$result = $stmt_user_photos->fetchAll();

    				if($count_user_photos==0)
    				{
    					echo "<div class='alert alert-danger'>No photos uploaded by member.</div>";
    				}
    				else
    				if($count_user_photos>0)
    				{
    					foreach( $result as $row )
						{
							$photo_id =  $row['id'];
							$photo =  '../users/uploads/'.$user_id.'/photo/'.$row['photo'];
							$created_at = $row['created_at'];
					?>
						<div class="isotope-item document col-sm-6 col-md-4 col-lg-4">
							<div class="thumbnail">
								<div class="thumb-preview">
									<a class="thumb-image" href="<?php echo $photo;?>">
										<img src="<?php echo $photo;?>" class="img-responsive member-photos">
									</a>
								</div>
								<div class="mg-description">
									<small class="pull-left">Uploaded On</small>
									<small class="pull-right"><?php echo date('d-M-Y H:i:A',strtotime($created_at));?></small>
								</div><br/>
								<div class="row">
									<center><img src="../images/loader/loader.gif" class='img-responsive <?php echo $photo_id;?>_loading_img'' id='<?php echo $photo_id;?>_loading_img' style='width:50px; height:50px; display:none;'/></center>
								</div><br/>
								<div class="row">
									<center><a class="btn btn-danger btn-sm member_photos_delete" id="<?php echo $photo_id;?>"><i class="fa fa-trash-o"></i> Delete</a></center>
								</div>
							</div>
							<input type="hidden" class="user_id" value="<?php echo $user_id;?>">
						</div>	
					<?php
						}
    				}
    			?>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    				<div class="member-photo-update-status"></div>
    			</div>
    		</div>
    	</div>
    </div>
</div>

<script>
	$(document).ready(function(){
		/****************    Delete profile photo    *******************/
		$('.member_profile_photo_delete').click(function(){
			var user_id = $('.user_id').val();
			var task = "delete-profile-photo";
			var reason_delete_profile_pic = prompt("Please enter profile photo reject reason");

			if(reason_delete_profile_pic=='' || reason_delete_profile_pic==null)
			{
				$('.member-profile-photo-update-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span> <strong> Empty! </strong><br/> Please enter reason for profile photo reject.</center></div>");
            	return false;
			}

			var data = "user_id="+user_id+"&reason_delete_profile_pic="+reason_delete_profile_pic+"&task="+task;
			$('.loading_img_profile_pic').show();
			$('.member_profile_photo_delete').attr('disabled',true);
			$('.member-profile-photo-update-status').html("");

			$.ajax({
				type:'post',
				data:data,
				url:'query/member-attributes/Member-Photo-helper.php',
				success:function(res)
				{
					$('.loading_img_profile_pic').hide();
					if(res=='success')
					{
						$('.member-profile-photo-update-status').html("<div class='alert alert-success member-profile-photo-update-status-new' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span> <strong> Success! </strong><br/> Profile photo removed successfully .</center></div>");
						$('.member-profile-photo-update-status-new').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign('member-profile.php?id='+user_id+"&main_tab=MemberAttributes&sub_tab=MemberPhotos");
                        });
                		return false;
					}
					else
					{
						$('.member_profile_photo_delete').attr('disabled',false);
						$('.member-profile-photo-update-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-times'></span> <strong> Empty! </strong><br/> "+res+"</center></div>");
                		return false;
					}
				}
			});
		});

		/****************    Delete personal photos    *******************/
		$('.member_photos_delete').click(function(){
			var user_id = $('.user_id').val();
			var photo_id = $(this).attr('id');
			var task = "Update-Member-Photo-Status";
			
			var reason = prompt("Please enter photo reject reason");

			if(reason=='' || reason==null)
			{
				$('.member-photo-update-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span> <strong> Empty! </strong><br/> Please enter reason for photo reject.</center></div>");
            	return false;
			}

			$('.member_photos_delete').attr('disabled',true);
			$('.'+photo_id+'_loading_img').show();

			var data = 'user_id='+user_id+'&photo_id='+photo_id+'&reason='+reason+'&task='+task;
			$('.member-photo-update-status').html("");

			$.ajax({
				type:'post',
				data:data,
				url:'query/member-attributes/Member-Photo-helper.php',
				success:function(res)
				{
					$('.'+photo_id+'_loading_img').hide();
					if(res=='success')
					{
						$('.member-photo-update-status').html("<div class='alert alert-success member-photo-update-status-new' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span> <strong> Success! </strong><br/> Photo removed successfully .</center></div>");
						$('.member-photo-update-status-new').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign('member-profile.php?id='+user_id+"&main_tab=MemberAttributes&sub_tab=MemberPhotos");
                        });
                		return false;
					}
					else
					{
						$('.member_photos_delete').attr('disabled',false);
						$('.member-photo-update-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-times'></span> <strong> Empty! </strong><br/> "+res+"</center></div>");
                		return false;
					}
				}
			});

		});
	});
</script>