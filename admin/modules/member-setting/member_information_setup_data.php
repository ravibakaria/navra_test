<?php
	$website_base_path = getWebsiteBasePath();
	$profile_id = getUserUniqueCode($user_id);
	$profile_link = $website_base_path.'/users/Profile-'.$profile_id.'.html';
?>

<input type="hidden" class="Member_id" value="<?php echo $user_id;?>">
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    		<div class="row profile-row">
    			<div class="text-left col-xs-12 col-sm-12 col-md-6 col-lg-6">
    				<h3>Basic Information</h3>
    			</div>
    			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4" >
    				    <button class="btn btn-primary admin-view-profile website-button" data-toggle="modal" data-target="#EditProfileModel">Edit Profile</button>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				    <a href="<?php echo $profile_link;?>" class='btn btn-primary text-right admin-view-profile website-button' target='_blank'>View Public Profile</a>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <center><button class="btn btn-primary btn_reset_password_Member admin-view-profile website-button">Reset & Email Password</button></center>
                    </div>
    			</div>
    		</div>
    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>First Name</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= getUserFirstName($user_id); ?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Last Name</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= getUserLastName($user_id); ?>
    			</div>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Profile ID</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= getUserUniqueCode($user_id); ?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Email Id</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= getUserEmail($user_id); ?>
    			</div>
    		</div>
    		
    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Gender</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$gender = getUserGender($user_id);
    					if($gender=='1')
    					{
    						echo "Male";
    					}
    					else
    					if($gender=='2')
    					{
    						echo "Female";
    					}
    					else
    					if($gender=='3')
    					{
    						echo "T-Gender";
    					}
    				?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>D O B </b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= getUserDOB($user_id); ?>
    			</div>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Mobile</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= getUserMobile($user_id); ?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Status </b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php 
    					$member_status = getUserStatus($user_id); 
    					if($member_status=='0')
    					{
    						echo "In-Active";
    					}
    					else
    					if($member_status=='1')
    					{
    						echo "Active";
    					}
    					else
    					if($member_status=='2')
    					{
                            $deactivation_reason = getDeactivationReason($user_id);
                            $deactivation_on = getDeactivationOn($user_id);
                            $deactivation_on = date('d-m-Y h:i A', strtotime($deactivation_on));
                            
    						echo "Deactivated &nbsp;&nbsp;&nbsp;";

                            echo "<span class='badge' data-toggle='popover' data-container='body' data-placement='top' title='Deactivated On: $deactivation_on' data-content='Reason: $deactivation_reason'><span class='fa fa-exclamation'></span> </span>";
    					}
                        else
                        if($member_status=='3')
                        {
                            echo "Suspended";
                        }
    				?>
    			</div>
    		</div>

    		<hr class="dotted">

    		<div class="row profile-row">
    			<h3>Personal Information</h3>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Height (cms/fts/mts)</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$height = getUserHeight($user_id); 
    					$height_meter = round(($height)/100,2);
	                	$height_foot = round(($height)/30.48,2);
	                	echo $height.'-cms / '.$height_foot.'-fts / '.$height_meter.'-mts';
    				?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Body Type </b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$body_type = getUserBodyTypeId($user_id);
    					echo getUserBodyTypeName($body_type);
    				?>
    			</div>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Weight (kg)</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= getUserWeight($user_id).' (kg)'; ?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Complexion </b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$complexion = getUserComplexionId($user_id); 
    					echo getUserComplexionName($complexion);
    				?>
    			</div>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Eating Habbit</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$eat_habbit = getUserEatHabbitId($user_id); 
    					echo getUserEatHabbitName($eat_habbit);
    				?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Martial Status </b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$marital_status = getUserMaritalStatusId($user_id); 
    					echo getUserMaritalStatusName($marital_status);
    				?>
    			</div>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Smoking  Habbit</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$smoke_habbit = getUserSmockingHabbitId($user_id); 
    					echo getUserSmockingHabbitName($smoke_habbit);
    				?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Drinking Habbit </b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$drink_habbit = getUserDrinkHabbitId($user_id); 
    					echo getUserDrinkHabbitName($drink_habbit);
    				?>
    			</div>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Special Case</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$special_case = getUserSpecialCaseId($user_id); 
    					echo getUserSpecialCaseName($special_case);
    				?>
    			</div>
    		</div>

    		<hr class="dotted">

    		<div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    				<div class="row">
    					<h3>Religious Information</h3>
    				</div>
    				<div class="row profile-row">
	    				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		    				<b>Religion </b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		    				<?php
		    					$religion = getUserReligionId($user_id); 
		    					echo getUserReligionName($religion);
		    				?>
		    			</div>
		    		</div>

	    			<div class="row profile-row">
		    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		    				<b>Caste </b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		    				<?php
		    					$caste = getUserCastId($user_id); 
		    					echo getUserCastName($caste);
		    				?>
		    			</div>
		    		</div>

		    		<div class="row profile-row">
		    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		    				<b>Mother Tongue</b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		    				<?php
		    					$mothertongue = getUserMotherTongueId($user_id); 
		    					echo getUserMotherTongueName($mothertongue);
		    				?>
		    			</div>
		    		</div>
    			</div>
            </div>

            <hr class="dotted">

            <div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    				<div class="row">
    					<h3>Location Information</h3>
    				</div>
    				<div class="row profile-row">
	    				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		    				<b>City </b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		    				<?php
		    					echo getUserCity($user_id);
		    				?>
		    			</div>
		    		</div>

	    			<div class="row profile-row">
		    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		    				<b>State </b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		    				<?php
		    					echo getUserState($user_id);
		    				?>
		    			</div>
		    		</div>

		    		<div class="row profile-row">
		    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		    				<b>Country</b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		    				<?php
		    					echo getUserCountry($user_id);
		    				?>
		    			</div>
		    		</div>
    			</div>
    		</div>

    		<hr class="dotted">

    		<div class="row profile-row">
    			<h3>Education & Occupation Information</h3>
    		</div>
    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Education</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?= getUserEducation($user_id); ?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Employment</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$occupation = getUserEmploymentId($user_id);
    					echo getUserEmploymentName($occupation);
    				?>
    			</div>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Category</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$designation_category = getUserDesignationCategoryId($user_id);
    					echo getUserDesignationCategoryName($designation_category);
    				?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Designation</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$designation = getUserDesignationId($user_id);
    					echo getUserDesignationName($designation);
    				?>
    			</div>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Industry</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$industry = getUserIndustryId($user_id);
    					echo getUserIndustryName($industry);
    				?>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
    				<b>Monthly Income</b>
    			</div>
    			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
    				<?php
    					$income = getUserMonthlyIncome($user_id);

    					$income_currency = getUserIncomeCurrency($user_id);
    					$currency_name = getUserIncomeCurrencyName($income_currency);
    					$currency_code = getUserIncomeCurrencyCode($income_currency);

    					echo $currency_code.' '.$income.' ('.$currency_name.')';
    				?>
    			</div>
    		</div>

    		<hr class="dotted">

    		<div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    				<div class="row">
    					<h3>Family Information</h3>
    				</div>
    				<div class="row profile-row">
	    				<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
		    				<b>Family Value </b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
		    				<?php
		    					echo getUserFamilyValueUser($user_id);
		    				?>
		    			</div>
		    		</div>

	    			<div class="row profile-row">
		    			<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
		    				<b>Family Type </b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
		    				<?php
		    					echo getUserFamilyTypeUser($user_id);
		    				?>
		    			</div>
		    		</div>

		    		<div class="row profile-row">
		    			<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
		    				<b>Family Status</b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
		    				<?php
		    					echo getUserFamilyStatusUser($user_id);
		    				?>
		    			</div>
		    		</div>
    			</div>
    			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    				<div class="row">
    					<h3>Security Information</h3>
    				</div>
    				<div class="row profile-row">
	    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<b>Registered On </b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<?php
		    					echo getUserRegisteredOn($user_id);
		    				?>
		    			</div>
		    		</div>

	    			<div class="row profile-row">
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<b>Email Verified On </b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<?php
		    					$email_verified_or_not = getUserVerifiedOrNot($user_id);
		    					if($email_verified_or_not=='1')
		    					{
		    						echo getUserVerifiedOn($user_id);
		    					}
		    					else
		    					{
		    						echo "Not Verified";
		    					}
		    				?>
		    			</div>
		    		</div>

		    		<div class="row profile-row">
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<b>Last Password Update On</b>
		    			</div>
		    			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		    				<?php
		    					echo getUserUpdatedOn($user_id);
		    				?>
		    			</div>
		    		</div>
    			</div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="row">
                        <h3>Last Login</h3>
                    </div>
                    <div class="row profile-row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <b>Last Logged in On: </b>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <?php
                                $member_last_logged_in_on = getLastUserLoginDate($user_id);

                                if($member_last_logged_in_on!='' || $member_last_logged_in_on!=null)
                                {
                                    echo $member_last_logged_in_on;
                                }
                                else
                                {
                                    echo "NA";
                                }
                            ?>
                        </div>
                    </div>
                    <div class="row profile-row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <b>IP Address: </b>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <?php
                                $member_last_logged_in_ip = getLastUserLoginIP($user_id);

                                if($member_last_logged_in_ip!='' || $member_last_logged_in_ip!=null)
                                {
                                    echo $member_last_logged_in_ip;
                                }
                                else
                                {
                                    echo "NA";
                                }
                            ?>
                        </div>
                    </div>
                </div>
    		</div>

    		<hr class="dotted">

    		<div class="row profile-row">
    			<h3>Short Description</h3>
    		</div>
    		<div class="row profile-row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        				<b>About Member: </b>
        			</div>
                    <br/>
        			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <?php
                            $member_myself = getUserAboutMyself($user_id);
                            if($member_myself!='' || $member_myself!=null)
                            {
                                echo $member_myself;
                            }
                            else
                            {
                                echo "Not updated by member";
                            }
                        ?>
        			</div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <b>About Member Family: </b>
                    </div>
                    <br/>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <?php
                            $member_myself_about_family = getUserAboutFamily($user_id);
                            if($member_myself_about_family!='' || $member_myself_about_family!=null)
                            {
                                echo $member_myself_about_family;
                            }
                            else
                            {
                                echo "Not updated by member";
                            }
                        ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <b>Member Interested In: </b>
                    </div>
                    <br/>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <?php
                            $member_myself_about_interest = getUserAboutInterest($user_id);
                            if($member_myself_about_interest!='' || $member_myself_about_interest!=null)
                            {
                                echo $member_myself_about_interest;
                            }
                            else
                            {
                                echo "Not updated by member";
                            }
                        ?>
                    </div>
                </div>
    		</div>
    	</div>

    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<!-- Edit Profile Modal -->
	        <div class="modal fade" id="EditProfileModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	            <div class="modal-dialog modal-dialog-centered add-item-model-template" role="document">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <center><h2 class="modal-title" id="exampleModalLongTitle">Edit Member Information</h2></center>
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                        </button>
	                    </div>
	                    <div class="modal-body">
	                    	<div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Basic Information</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">First Name:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm firstname" value="<?php echo getUserFirstName($user_id);?>"/>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Last Name:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm lastname" value="<?php echo getUserLastName($user_id);?>" />
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Profile Id:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm unique_code" value="<?php echo getUserUniqueCode($user_id);?>" disabled/>
		                            </div>
		                        </div>
	                        </div>
	                        <div class="row input-row">
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Email Id:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm email" value="<?php echo getUserEmail($user_id);?>"/>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Gender:</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                           		<select class="form-control gender">
		                           			<?php
		                           				$user_gender = getUserGender($user_id);
		                           				$sql_gender = "SELECT * FROM gender WHERE status='1'";
		                           				$stmt = $link->prepare($sql_gender);
		                           				$stmt->execute();
		                           				$result = $stmt->fetchAll();

		                           				foreach ($result as $row) 
		                           				{
		                           					$id=$row['id'];
		                           					$name=$row['name'];

		                           					if($id==$user_gender)
		                           					{
		                           						echo "<option value='".$id."' selected>".$name."</option>";
		                           					}
		                           					else
		                           					{
		                           						echo "<option value='".$id."'>".$name."</option>";
		                           					}
		                           				}
	                           				?>
		                           		</select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Date Of Birth</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm dob" name='dob' value="<?php echo getUserDOB($user_id);?>" />
		                            </div>
		                        </div>
	                        </div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Mobile</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <input type="text" class="form-control form-control-sm phonenumber" value="<?php echo getUserMobile($user_id);?>" maxlength="20"/>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Status</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control status">
		                                	<?php
		                                		$user_status = getUserStatus($user_id);

		                                		if($user_status=='0')
		                                		{
		                                			echo "<option value='0' selected>In-Active</option>";
		                                			echo "<option value='1'>Active</option>";
		                                			echo "<option value='2'>Deactivate</option>";
                                                    echo "<option value='3'>Suspend</option>";
		                                		}
		                                		else
		                                		if($user_status=='1')
		                                		{
		                                			echo "<option value='0'>In-Active</option>";
		                                			echo "<option value='1' selected>Active</option>";
		                                			echo "<option value='2'>Deactivate</option>";
                                                    echo "<option value='3'>Suspended</option>";
		                                		}
		                                		else
		                                		if($user_status=='2')
		                                		{
		                                			echo "<option value='0'>In-Active</option>";
		                                			echo "<option value='1'>Active</option>";
		                                			echo "<option value='2' selected>Deactivate</option>";
                                                    echo "<option value='3'>Suspend</option>";
		                                		}
		                                		else
		                                		if($user_status=='3')
		                                		{
		                                			echo "<option value='0'>In-Active</option>";
		                                			echo "<option value='1'>Active</option>";
		                                			echo "<option value='2'>Deactivate</option>";
                                                    echo "<option value='3' selected>Suspend</option>";
		                                		}
		                                	?>
		                                </select>
		                            </div>
		                        </div>
	                        </div>

	                        <hr class="dotted">

	                        <div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Personal Information</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Height (cms/fts/mts)</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control height">
			                                <?php
			                                	$height = getUserHeight($user_id);
			                                	for($i=54;$i<273;$i++)
												{
													$i_meter = number_format(round(($i)/100,2),2,'.','');
							                		$i_foot = number_format(round(($i)/30.48,2),2,'.','');

													if($i==$height)
													{
														echo "<option value=".$i." selected>".$i."-cms / ".$i_foot."-fts / ".$i_meter."-mts</option>";
													}
													else
													{
														echo "<option value=".$i.">".$i."-cms / ".$i_foot."-fts / ".$i_meter."-mts</option>";
													}
												}
			                                ?>
			                            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Body Type</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control body_type">
											<?php
												$body_type = getUserBodyTypeId($user_id); 
												$stmt = $link->prepare("SELECT * FROM `bodytype` WHERE `status`='1'"); 
										        $stmt->execute();
										        $count=$stmt->rowCount();
										        $result = $stmt->fetchAll();
										        
										        foreach ($result as $row) 
										        {
										        	$id = $row['id'];
										        	$name = $row['name'];

										        	if($id == $body_type)
										        	{
										        		echo "<option value='".$id."' selected>".$name."</option>";
										        	}
										        	else
										        	{
										        		echo "<option value='".$id."'>".$name."</option>";
										        	}
										        }
											?>
										</select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Weight (kg)</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control weight">
											<?php
												$weight = getUserWeight($user_id);
												for($i=35;$i<161;$i++)
												{
													if($i==$weight)
													{
														echo "<option value=".$i." selected>".$i."</option>";
													}
													else
													{
														echo "<option value=".$i.">".$i."</option>";
													}
												}
											?>
										</select>
		                            </div>
		                        </div>
	                        </div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Complexion</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control complexion">
											<?php
												$complexion = getUserComplexionId($user_id);
												$stmt = $link->prepare("SELECT * FROM `complexion` WHERE `status`='1'"); 
										        $stmt->execute();
										        $count=$stmt->rowCount();
										        $result = $stmt->fetchAll();
										        
										        foreach ($result as $row) 
										        {
										        	$id = $row['id'];
										        	$name = $row['name'];

										        	if($id == $complexion)
										        	{
										        		echo "<option value='".$id."' selected>".$name."</option>";
										        	}
										        	else
										        	{
										        		echo "<option value='".$id."'>".$name."</option>";
										        	}
										        }
											?>
										</select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Martial Status</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control marital_status">
											<?php
												$marital_status = getUserMaritalStatusId($user_id);
												$stmt = $link->prepare("SELECT * FROM `maritalstatus` WHERE `status`='1'"); 
										        $stmt->execute();
										        $count=$stmt->rowCount();
										        $result = $stmt->fetchAll();
										        
										        foreach ($result as $row) 
										        {
										        	$id = $row['id'];
										        	$name = $row['name'];

										        	if($id == $marital_status)
										        	{
										        		echo "<option value='".$id."' selected>".$name."</option>";
										        	}
										        	else
										        	{
										        		echo "<option value='".$id."'>".$name."</option>";
										        	}
										        }
											?>
										</select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Special Case</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control special_case">
											<?php
												$special_case = getUserSpecialCaseId($user_id);
												$stmt = $link->prepare("SELECT * FROM `specialcases` WHERE `status`='1'"); 
										        $stmt->execute();
										        $count=$stmt->rowCount();
										        $result = $stmt->fetchAll();
										        
										        foreach ($result as $row) 
										        {
										        	$id = $row['id'];
										        	$name = $row['name'];

										        	if($id == $special_case)
										        	{
										        		echo "<option value='".$id."' selected>".$name."</option>";
										        	}
										        	else
										        	{
										        		echo "<option value='".$id."'>".$name."</option>";
										        	}
										        }
											?>
										</select>
		                            </div>
		                        </div>
	                        </div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Eating Habbit</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control eat_habbit">
											<?php
												$eat_habbit = getUserEatHabbitId($user_id);
												$stmt = $link->prepare("SELECT * FROM `eathabbit` WHERE `status`='1'"); 
										        $stmt->execute();
										        $count=$stmt->rowCount();
										        $result = $stmt->fetchAll();
										        
										        foreach ($result as $row) 
										        {
										        	$id = $row['id'];
										        	$name = $row['name'];

										        	if($id == $eat_habbit)
										        	{
										        		echo "<option value='".$id."' selected>".$name."</option>";
										        	}
										        	else
										        	{
										        		echo "<option value='".$id."'>".$name."</option>";
										        	}
										        }
											?>
										</select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Smoking Habbit</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control smoke_habbit">
											<?php
												$smoke_habbit = getUserSmockingHabbitId($user_id);
												$stmt = $link->prepare("SELECT * FROM `smokehabbit` WHERE `status`='1'"); 
										        $stmt->execute();
										        $count=$stmt->rowCount();
										        $result = $stmt->fetchAll();
										        
										        foreach ($result as $row) 
										        {
										        	$id = $row['id'];
										        	$name = $row['name'];

										        	if($id == $smoke_habbit)
										        	{
										        		echo "<option value='".$id."' selected>".$name."</option>";
										        	}
										        	else
										        	{
										        		echo "<option value='".$id."'>".$name."</option>";
										        	}
										        }
											?>
										</select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Drinking Habbit</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control drink_habbit">
											<?php
												$drink_habbit = getUserDrinkHabbitId($user_id);
												$stmt = $link->prepare("SELECT * FROM `drinkhabbit` WHERE `status`='1'"); 
										        $stmt->execute();
										        $count=$stmt->rowCount();
										        $result = $stmt->fetchAll();
										        
										        foreach ($result as $row) 
										        {
										        	$id = $row['id'];
										        	$name = $row['name'];

										        	if($id == $drink_habbit)
										        	{
										        		echo "<option value='".$id."' selected>".$name."</option>";
										        	}
										        	else
										        	{
										        		echo "<option value='".$id."'>".$name."</option>";
										        	}
										        }
											?>
										</select>
		                            </div>
		                        </div>
	                        </div>

	                        <hr class="dotted">

	                        <div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Religious Information</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Religion</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control religion" id="religion" required>
							              	<?php
							              		$religion = getUserReligionId($user_id);
							              		$query  = "SELECT * FROM `religion` WHERE status='1' ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$religion_name =  $row['name'];
													$religion_id = $row['id']; 

													if($religion_id==$religion)
													{
														echo "<option value='".$religion_id."' selected>".$religion_name."</option>";
													}
													else
													{
														echo "<option value=".$religion_id.">".$religion_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Caste</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control caste" id="caste" required>
							              	<?php
							              		$caste = getUserCastId($user_id);
							              		$query  = "SELECT * FROM `caste` WHERE status='1' ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$caste_name =  $row['name'];
													$caste_id = $row['id']; 

													if($caste_id==$caste)
													{
														echo "<option value='".$caste_id."' selected>".$caste_name."</option>";
													}
													else
													{
														echo "<option value=".$caste_id.">".$caste_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Mother Tongue</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control mother_tongue" id="mother_tongue" required>
							              	<?php
							              		$mother_tongue = getUserMotherTongueId($user_id);
							              		$query  = "SELECT * FROM `mothertongue` WHERE status='1' ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$mothertongue_name =  $row['name'];
													$mothertongue_id = $row['id'];

													if($mothertongue_id==$mother_tongue)
													{
														echo "<option value='".$mothertongue_id."' selected>".$mothertongue_name."</option>";
													}
													else
													{
														echo "<option value=".$mothertongue_id.">".$mothertongue_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
	                        </div>

	                        <hr class="dotted">

	                        <div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Location Information</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Country</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control country" id="country">
							              	<?php
							              		$country = getUserCountryId($user_id);
							              		$query  = "SELECT * FROM `countries` ORDER BY `name` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$country_name =  $row['name'];
													$country_id = $row['id']; 

													if($country_id==$country)
													{
														echo "<option value=".$country_id." selected>".$country_name."</option>";
													}
													else
													{
														echo "<option value=".$country_id.">".$country_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">State</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control state" id="state">
											<?php
												if($country!='0')
												{
													$state = getUserStateId($user_id);
													$caste_sql  = "SELECT * FROM `states` WHERE `country_id`='$country' ORDER BY `name` ASC";
									                $stmt   = $link->prepare($caste_sql);
									                $stmt->execute();
									                $states_userRow = $stmt->fetchAll();
									                
									                foreach( $states_userRow as $row )
													{
														$state_name =  $row['name'];
														$state_id = $row['id']; 

														if($state_id==$state)
														{
															echo "<option value=".$state_id." selected>".$state_name."</option>";
														}
														else
														{
															echo "<option value=".$state_id.">".$state_name."</option>";
														}
													}
												}
								                
								            ?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">City</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control city" id="city" required>
											<?php
												if($state!='0')
												{
													$city = getUserCityId($user_id);
													$city_sql  = "SELECT * FROM `cities` WHERE `state_id`='$state' ORDER BY `name` ASC";
									                $stmt   = $link->prepare($city_sql);
									                $stmt->execute();
									                $city_userRow = $stmt->fetchAll();
									                
									                foreach( $city_userRow as $row )
													{
														$city_name =  $row['name'];
														$city_id = $row['id']; 

														if($city_id==$city)
														{
															echo "<option value=".$city_id." selected>".$city_name."</option>";
														}
														else
														{
															echo "<option value=".$city_id.">".$city_name."</option>";
														}
													}
												}
								                
								            ?>
							            </select>
		                            </div>
		                        </div>
	                        </div>

	                        <hr class="dotted">

	                        <div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Education & Occupation Information</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Education</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control education" id="education" required>
							              	<?php
							              		$education = getUserEducationId($user_id);
							              		$query  = "SELECT * FROM `educationname` WHERE `status`='1' ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$education_name =  $row['name'];
													$education_id = $row['id']; 

													if($education_id==$education)
													{
														echo "<option value=".$education_id." selected>".$education_name."</option>";
													}
													else
													{
														echo "<option value=".$education_id.">".$education_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Occupation</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control occupation" id="occupation" required>
							              	<?php
							              		$occupation = getUserEmploymentId($user_id);
							              		$query  = "SELECT * FROM `employment` WHERE `status`='1' ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$employment_name =  $row['name'];
													$employment_id = $row['id']; 

													if($employment_id==$occupation)
													{
														echo "<option value=".$employment_id." selected>".$employment_name."</option>";
													}
													else
													{
														echo "<option value=".$employment_id.">".$employment_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Category</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control designation_category" id="designation_category" required>
							              	<?php
							              		$designation_category = getUserDesignationCategoryId($user_id);
							              		$query  = "SELECT * FROM `designation_category` WHERE `status`='1' ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$designation_category_name =  $row['name'];
													$designation_category_id = $row['id']; 

													if($designation_category==$designation_category_id)
													{
														echo "<option value=".$designation_category_id." selected>".$designation_category_name."</option>";
													}
													else
													{
														echo "<option value=".$designation_category_id.">".$designation_category_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
	                        </div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Designation</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control designation" id="designation" required>
							              	<?php
							              		$designation = getUserDesignationId($user_id);
							              		$query  = "SELECT * FROM `designation` WHERE `designation_category`='$designation_category'  AND `status`='1'";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$designation_name =  $row['name'];
													$designation_id = $row['id']; 

													if($designation==$designation_id)
													{
														echo "<option value=".$designation_id." selected>".$designation_name."</option>";
													}
													else
													{
														echo "<option value=".$designation_id.">".$designation_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Industry</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control industry" id="industry" required>
							              	<?php
							              		$industry = getUserIndustryId($id);
							              		$query  = "SELECT * FROM `industry` WHERE `status`='1' ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$industry_name =  $row['name'];
													$industry_id = $row['id']; 

													if($industry_id==$industry)
													{
														echo "<option value=".$industry_id." selected>".$industry_name."</option>";
													}
													else
													{
														echo "<option value=".$industry_id.">".$industry_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Currency</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control income_currency" id="income_currency">
							              	<?php
							              		$income_currency = getUserIncomeCurrency($user_id);
							              		$query  = "SELECT * FROM `currency` WHERE `status`='1' ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$currency_name =  $row['currency']." [".$row['code']."]";
													$currency_id = $row['id']; 

													if($currency_id==$income_currency)
													{
														echo "<option value=".$currency_id." selected>".$currency_name."</option>";
													}
													else
													{
														echo "<option value=".$currency_id.">".$currency_name."</option>";
													}
												}
							              	?>
							            </select>
		                            </div>
		                        </div>
	                        </div>

	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Monthly Salary</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<input type="text" class="form-control form-control-sm income" value="<?php echo getUserMonthlyIncome($user_id);?>"/>
		                            </div>
		                        </div>
		                    </div>

		                    <hr class="dotted">

	                        <div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Family Information</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Family Value</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                            	<select class="form-control fam_val" id="fam_val" required>
							                <?php
							                	$fam_val = getUserFamilyValueId($user_id);
							                    $stmt = $link->prepare("SELECT * FROM `familyvalue` WHERE status='1'"); 
							                    $stmt->execute();
							                    $count=$stmt->rowCount();
							                    $result = $stmt->fetchAll();

							                    foreach ($result as $row) 
							                    {
							                        $id = $row['id'];
							                        $name = $row['name'];

							                        if($id == $fam_val)
							                        {
							                            echo "<option value='".$id."' selected>".$name."</option>";
							                        }
							                        else
							                        {
							                            echo "<option value='".$id."'>".$name."</option>";
							                        }
							                    }
							                ?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Family Type</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control fam_type" id="fam_type" required>
							                <?php
							                	$fam_type = getUserFamilyTypeId($user_id);
							                    $stmt = $link->prepare("SELECT * FROM `familytype` WHERE status='1'"); 
							                    $stmt->execute();
							                    $count=$stmt->rowCount();
							                    $result = $stmt->fetchAll();

							                    foreach ($result as $row) 
							                    {
							                        $id = $row['id'];
							                        $name = $row['name'];

							                        if($id == $fam_type)
							                        {
							                            echo "<option value='".$id."' selected>".$name."</option>";
							                        }
							                        else
							                        {
							                            echo "<option value='".$id."'>".$name."</option>";
							                        }
							                    }
							                ?>
							            </select>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 member-profile-edit">
		                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
		                                <lable class="control-label">Family Status</lable>
		                            </div>
		                            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
		                                <select class="form-control fam_stat" id="fam_stat">
							                <?php
							                	$fam_stat = getUserFamilyStateId($user_id);
							                    $stmt = $link->prepare("SELECT * FROM `familystatus` WHERE status='1'"); 
							                    $stmt->execute();
							                    $count=$stmt->rowCount();
							                    $result = $stmt->fetchAll();

							                    foreach ($result as $row) 
							                    {
							                        $id = $row['id'];
							                        $name = $row['name'];

							                        if($id == $fam_stat)
							                        {
							                            echo "<option value='".$id."' selected>".$name."</option>";
							                        }
							                        else
							                        {
							                            echo "<option value='".$id."'>".$name."</option>";
							                        }
							                    }
							                ?>
							            </select>
		                            </div>
		                        </div>
	                        </div>

	                        <hr class="dotted">

	                        <div class="row input-row">
	                    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                    				<h3>Short Description</h3>
	                    			</div>
	                    		</div>
	                    	</div>
	                        <div class="row input-row">
	                        	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		                                <lable class="control-label">About Member</lable>
		                            </div>
		                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		                            	<textarea name="short_desc" id="short_desc" rows="6" maxlength="300" class="form-control short_desc" ><?php echo getUserAboutMyself($user_id); ?></textarea>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		                                <lable class="control-label">About Member Family</lable>
		                            </div>
		                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		                                <textarea name="short_desc_family" id="short_desc_family" rows="6" maxlength="300" class="form-control short_desc_family" ><?php echo getUserAboutFamily($user_id); ?></textarea>
		                            </div>
		                        </div>
		                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		                                <lable class="control-label">Member Interested In</lable>
		                            </div>
		                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		                                <textarea name="short_desc_interest" id="short_desc_interest" rows="6" maxlength="300" class="form-control short_desc_interest" ><?php echo getUserAboutInterest($user_id); ?></textarea>
		                            </div>
		                        </div>
	                        </div>

	                        <input type="hidden" class="user_id" value="<?php echo $user_id;?>">
	                        <div class="row">
	                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:70px; height:70px; display:none;'/></center>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 edit-user-profile-status">
	                            </div>
	                        </div>
	                    </div>
	                    <div class="modal-footer">
	                        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="margin-top:10px !important;">Close</button>
	                        <button type="button" class="btn btn-primary btn_edit_user_profile website-button">Update</button>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    </div>
</div>

<script>
	$(document).ready(function(){

		/*  Datepicker setting  */
		var date_input=$('input[name="dob"]');
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        
        date_input.datepicker({
          format: 'yyyy-mm-dd',
          container: container,
          todayHighlight: true,
          autoclose: true,
          orientation: "auto top",
        });

        /*   Prevent entering charaters in mobile & phone number   */
        $(".phonenumber").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
               return false;
            }
        })

        /************   Fetch states of country    **************/
        $('.country').change(function(){
        	var country_id = $(this).val();
        	var task = "Fetch_States_Of_Country";

        	var data = 'country_id='+country_id+'&task='+task;

        	$.ajax({
                type:'post',
                data:data,
                url:'query/member-attributes/Update-Member-Profile-helper.php',
                success:function(res)
                {
                	$('.state').html(res);
                	return false;
                }
            });
        });

        /************   Fetch cities of state    **************/
        $('.state').change(function(){
        	var state_id = $(this).val();
        	var task = "Fetch_Cities_Of_State";

        	var data = 'state_id='+state_id+'&task='+task;

        	$.ajax({
                type:'post',
                data:data,
                url:'query/member-attributes/Update-Member-Profile-helper.php',
                success:function(res)
                {
                	$('.city').html(res);
                	return false;
                }
            });
        });


        /************   Button update click    **************/
        $('.btn_edit_user_profile').click(function(){
        	var user_id = $('.user_id').val();
        	var firstname = $('.firstname').val();
        	var lastname = $('.lastname').val();
        	var unique_code = $('.unique_code').val();
        	var email = $('.email').val();
        	var gender = $('.gender').val();
        	var dob = $('.dob').val();
        	var phonenumber = $('.phonenumber').val();
        	var status = $('.status').val();
        	var height = $('.height').val();
        	var body_type = $('.body_type').val();
        	var weight = $('.weight').val();
        	var complexion = $('.complexion').val();
        	var marital_status = $('.marital_status').val();
        	var special_case = $('.special_case').val();
        	var eat_habbit = $('.eat_habbit').val();
        	var smoke_habbit = $('.smoke_habbit').val();
        	var drink_habbit = $('.drink_habbit').val();
        	var religion = $('.religion').val();
        	var caste = $('.caste').val();
        	var mother_tongue = $('.mother_tongue').val();
        	var country = $('.country').val();
        	var state = $('.state').val();
        	var city = $('.city').val();
        	var education = $('.education').val();
        	var occupation = $('.occupation').val();
        	var designation_category = $('.designation_category').val();
        	var designation = $('.designation').val();
        	var industry = $('.industry').val();
        	var income_currency = $('.income_currency').val();
        	var income = $('.income').val();
        	var fam_val = $('.fam_val').val();
        	var fam_type = $('.fam_type').val();
        	var fam_stat = $('.fam_stat').val();
        	var short_desc = $('.short_desc').val();
        	var short_desc_family = $('.short_desc_family').val();
        	var short_desc_interest = $('.short_desc_interest').val();
        	//alert(city);return false;
        	

            /* First name Validation */
            if(firstname=='' || firstname==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong><br/> Please enter first name of member.</center></div>");
                return false;
            }


            /* Last name Validation */
            if(lastname=='' || lastname==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong><br/> Please enter last name of member.</center></div>");
                return false;
            }

            /* Date of Birth validation */
            if(dob=='' || dob==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong><br/> Please enter member DOB.</center></div>");
                return false;
            }
            /* Country validation */
            if(country=='0' || country=='' || country==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong><br/> Please select country.</center></div>");
                return false;
            }

            /* State validation */
            if(state=='0' || state=='' || state==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong><br/> Please select state.</center></div>");
                return false;
            }

            /* city validation */
            if(city=='0' || city=='' || city==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong><br/> Please select city.</center></div>");
                return false;
            }


            /* Email Validation */
            if(email=='' || email==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong><br/> Please enter email id.</center></div>");
                return false;
            }

            function validateEmail($email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $email );
            }

            if(email!='' && !validateEmail(email)) 
            { 
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Invalid data! </strong><br/> Please enter valid email.</center></div>");
                return false;
            }


            /* Gender validation */
            if(gender=='0')
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong><br/> Please select gender of member.</center></div>");
                return false;
            }

            /* Phone number validation */
            if(phonenumber=='' || phonenumber==null)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong><br/> Please enter mobile number.</center></div>");
                return false;
            }

            if(phonenumber!='' && phonenumber.length<4)
            {
                $('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Invalid data! </strong><br/> Mobile number should be minimum 4 digit.</center></div>");
                return false;
            }

        	var task = "update_member_profile_information";

        	$('.edit-user-profile-status').html("");

        	var data = 'user_id='+user_id+'&firstname='+firstname+'&lastname='+lastname+'&unique_code='+unique_code+'&email='+email+'&gender='+gender+'&dob='+dob+'&phonenumber='+phonenumber+'&status='+status+'&height='+height+'&body_type='+body_type+'&weight='+weight+'&complexion='+complexion+'&marital_status='+marital_status+'&special_case='+special_case+'&eat_habbit='+eat_habbit+'&smoke_habbit='+smoke_habbit+'&drink_habbit='+drink_habbit+'&religion='+religion+'&caste='+caste+'&mother_tongue='+mother_tongue+'&country='+country+'&state='+state+'&city='+city+'&education='+education+'&occupation='+occupation+'&designation_category='+designation_category+'&designation='+designation+'&industry='+industry+'&income_currency='+income_currency+'&income='+income+'&fam_val='+fam_val+'&fam_type='+fam_type+'&fam_stat='+fam_stat+'&short_desc='+short_desc+'&short_desc_family='+short_desc_family+'&short_desc_interest='+short_desc_interest+'&task='+task;
        	
        	$('.loading_img').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/member-attributes/Update-Member-Profile-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                    	$('.edit-user-profile-status').html("<div class='alert alert-success edit_user_profile_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span><strong> Sucess! </strong> Member Profile updated successfully.</center></div>");
                    }
                	else
                	{
                		$('.edit-user-profile-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Error! </strong><br/> "+res+"</center></div>");
                        return false;
                	}
            	}
            });

        });

        /***********   Reset Password of Member   ***************/
        $('.btn_reset_password_Member').click(function(){
            var id = $('.Member_id').val();
            var task = "Reset_Member_Password";

            var data = 'id='+id+'&task='+task;
            
            $('.loading_img_password_reset').show();
            $('.btn_reset_password_Member').attr('disabled',true);
            $.ajax({
                type:'post',
                data:data,
                url:'query/member-attributes/Member-helper.php',
                success:function(res)
                {
                    $('.loading_img_password_reset').hide();
                    if(res=='success')
                    {
                        $('.password_update_status').html("<div class='alert alert-success password_update_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span><strong> Success! </strong> Password sent successfully.</center></div>");
                        $('.password_update_success_status').fadeTo(2500, 500).slideUp(500, function(){
                                  return false;
                            });
                    }
                    else
                    {
                        $('.btn_reset_password_Member').attr('disabled',false);
                        $('.password_update_status').html("<div class='alert alert-danger password_update_error_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-times'></span><strong> Error! </strong> "+res+"</center></div>");
                        $('.password_update_error_status').fadeTo(2500, 500).slideUp(500, function(){
                                  return false;
                            });
                    }
                }
            });
        });
    });
</script>