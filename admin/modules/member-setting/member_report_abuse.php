<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
        	<div class="switch-field">
        		<input class="form-check-input abuse_profile" type="radio" name="abuse_profile" id="abuse_profile1" value="Abused_By_Me" checked>
                <label for='abuse_profile1'><i class="fa fa-check-square-o Abused_By_Me_check"></i> Profiles Reported By <?= getUserFirstName($user_id); ?></label>

                <input class="form-check-input abuse_profile" type="radio" name="abuse_profile" id="abuse_profile2" value="Abused_By_Others" >
                <label for='abuse_profile2'><i class="fa fa-check-square-o Abused_By_Others_check"></i> Members Reported <?= getUserFirstName($user_id); ?>'s profile</label>
        	</div>

        </div>
        <div class="row">
        	<div class="report_abuse_result_data">
        		<!--     report_abuse result     -->
        	</div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){

		var value = $('input[type=radio][name=abuse_profile]').val();
		var user_id_x = $('.user_id').val();
		var task = "Get_Report_Abuse_Profile";
		
		if(value=="Abused_By_Me")
		{
			$(".Abused_By_Me_check").show();
			$(".Abused_By_Others_check").hide();
		}
		else
		{
			$(".Abused_By_Me_check").hide();
			$(".Abused_By_Others_check").show();
		}

		var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

		$.ajax({
			type:'post',
        	data:data,
        	url:'query/member-attributes/abuse_profiles_helper.php',
        	success:function(res)
        	{
        		$('.report_abuse_result_data').html(res);
        	}
        });

		$('input[type=radio][name=abuse_profile]').change(function(){
			var value = $(this).val();
			var user_id_x = $('.user_id').val();
			var task = "Get_Report_Abuse_Profile";
			
			if(value=="Abused_By_Me")
			{
				$(".Abused_By_Me_check").show();
				$(".Abused_By_Others_check").hide();
			}
			else
			{
				$(".Abused_By_Me_check").hide();
				$(".Abused_By_Others_check").show();
			}
			
			var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/member-attributes/abuse_profiles_helper.php',
	        	success:function(res)
	        	{
	        		$('.report_abuse_result_data').html(res);
	        	}
	        });
		});
	});
</script>