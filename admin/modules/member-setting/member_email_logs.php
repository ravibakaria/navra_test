<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
        	<div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered Member_email_log_list' style="width:100%">
                        <thead>
                            <tr>
                            	<th>Email Sent On</th>
                                <th>For Activity</th>
                            </tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                <center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var userid = $('.user_id').val();
        
        var dataTable_Member_email_log_list = $('.Member_email_log_list').DataTable({ 
            "order": [[ 0, "desc" ]],
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            
            "ajax":{
                url :"datatables/member-attributes/Member-email-log-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                data: function (data) {
                    data.userid = userid;
                },
                error: function(){
                    $(".Member_email_log_list_processing").css("display","none");
                }
            }
        });
    });
</script>