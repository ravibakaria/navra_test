<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h2>Report Abused Data</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                
            </div>
        </div>
        <div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered report_abuse_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Profile ID</th>
                                <th>First Name</th>
                                <th>Email Id</th>
                                <th>Abused Count</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                    </table>
                <center>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){
		var dataTable_Member_list = $('.report_abuse_list').DataTable({          //Member datatable
            "bProcessing": true,
            "serverSide": true,
            "ajax":{
                url :"datatables/member-attributes/Report-abuse-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".report_abuse_list_processing").css("display","none");
                }
            }
        });
	});
</script>