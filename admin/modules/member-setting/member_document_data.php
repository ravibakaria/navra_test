<br/>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    		<div class="row profile-row">
    			<?php
					$stmt = $link->prepare("SELECT * FROM `documents` WHERE `userid`='$user_id' AND status<>'2'"); 
				    $stmt->execute();
				    $count=$stmt->rowCount();

				    if($count==0)
				    {
				    	echo "<div class='alert alert-danger'>Member not uploaded any documents yet.</div>";
				    }

				    $result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$photo_id =  $row['id'];
						$photo_type =  $row['photo_type'];
						$photo_number = $row['photo_number'];
						$photo =  '../users/uploads/'.$user_id.'/documents/'.$row['photo'];
						$status =  $row['status'];
						$created_at = $row['created_at']; 
				?>
				<div class="isotope-item document col-sm-6 col-md-4 col-lg-4">
					<div class="thumbnail">
						<div class="thumb-preview">
							<a class="thumb-image" href="<?php echo $photo;?>">
								<img src="<?php echo $photo;?>" class="doc-image  member-photos" style="width: 200px !important;height: 200px !important;">
							</a>
						</div>
						<div class="mg-description">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
									<label class="pull-left"><b>Document Type</b></label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
									<label class="pull-left"><?php echo $photo_type;?></label>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
									<label class="pull-left"><b>Document No</b></label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
									<label class="pull-left"><?php echo $photo_number;?></label>
								</div>
							</div>
				
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
									<label class="pull-left"><b>Uploaded On</b></label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
									<label class="pull-left"><?php echo date('d-M-Y H:i:A',strtotime($created_at));?></label>
								</div>
							</div>
							
							<div class="mg-description">
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
										<label class="pull-left"><b>Status</b></label>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
										<label class="pull-left">
											<?php 
												if($status == '0')
												{
													$doc_status = "<b class='label label-warning text-right' style='text-align:right;'><span class='fa fa-exclamation-circle'></span> Pending verification</b>";
												}
												else
												if($status == '1')
												{
													$doc_status = "<b class='label label-success text-right' style='text-align:right;'>
														<span class='fa fa-check'></span> Verified Successfully</b>";
												}
												echo $doc_status;
											?>
										</label>
									</div>
								</div>
							</div>

							<hr class="dotted">
							
							<div class="mg-description">
								<div class="row">
									<?php
										if($status == '0')
										{
											echo "<div class='col-xs-6 col-sm-6 col-md-7 col-lg-7'>";
											echo "<input type='radio' name='".$photo_id."_status' value='1' class='radio_1'> Approve";
											echo "&nbsp;&nbsp;&nbsp;";
											echo "<input type='radio'  name='".$photo_id."_status' value='0' class='radio_1'> Reject";
											echo "</div>";
											echo "<div class='col-xs-6 col-sm-6 col-md-5 col-lg-5 text-left'>";
											echo "<button class='btn btn-primary btn-sm btn_update_document_status website-button' id='$photo_id'>Save</button>";
											echo "</div>";

											echo "<input type='hidden' class='user_id' value='$user_id'>";
										}
										else
										if($status == '1')
										{
											echo "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
											echo "<center>
												<b class='label label-success text-right' style='text-align:right;'>
														Document already Verified.</b>
												</center>";
											echo "<br/></div>";
										}
									?>
									
								</div>
							</div>

							<div class="mg-description">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                                <center><img src="../images/loader/loader.gif" class='img-responsive <?php echo $photo_id;?>_loading_img'' id='<?php echo $photo_id;?>_loading_img' style='width:50px; height:50px; display:none;'/></center>
		                            </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
					}
				?>
    		</div>

    		<div class="row profile-row">
    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    				<div class="update-member-document-status"></div>
    			</div>
    		</div>
    	</div>
    </div>
</div>

<script>
	$(document).ready(function(){
		$('.btn_update_document_status').click(function(){
			var doc_id = $(this).attr('id');
			var status = $("input[name='"+doc_id+"_status']:checked"). val();
			var user_id = $('.user_id').val();
			var reason=null;
			var task = "Update_Member_Document_Status";

			if(status=='' || status==null || status=='undefined')
			{
				$('.update-member-document-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Empty! </strong><br/> Please select Approve-Or-Reject document.</center></div>");
                return false;
			}

			if(status=='0')
			{
				reason = prompt("Please enter document reject reason");
			}
			else
			if(status=='1')
			{
				reason='approved-reason';
			}

			if(reason=='' || reason==null)
			{
				$('.update-member-document-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span> <strong> Empty! </strong><br/> Please enter reason for document reject.</center></div>");
            	return false;
			}

			var data = 'user_id='+user_id+'&doc_id='+doc_id+'&status='+status+'&reason='+reason+'&task='+task;
			$('.btn_update_document_status').attr('disabled',true);
			$('.'+doc_id+'_loading_img').show();

			$.ajax({
				type:'post',
				data:data,
				url:'query/member-attributes/Member-Document-helper.php',
				success:function(res)
				{
					$('.'+doc_id+'_loading_img').hide();
					if(res=='success' && status=='1')
					{
						$('.update-member-document-status').html("<div class='alert alert-success update-member-document-status-new' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span> <strong> Success! </strong><br/> Document approved successfully .</center></div>");
						$('.update-member-document-status-new').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign('member-profile.php?id='+user_id+"&main_tab=MemberAttributes&sub_tab=MemberDocuments");
                        });
                		return false;
					}
					else
					if(res=='success' && status=='0')
					{
						$('.update-member-document-status').html("<div class='alert alert-success update-member-document-status-new' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span> <strong> Success! </strong><br/> Document rejected & removed successfully .</center></div>");
						$('.update-member-document-status-new').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign('member-profile.php?id='+user_id+"&main_tab=MemberAttributes&sub_tab=MemberDocuments");
                        });
                		return false;
					}
					else
					{
						$('.btn_update_document_status').attr('disabled',false);
						$('.update-member-document-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-times'></span> <strong> Empty! </strong><br/> "+res+"</center></div>");
                		return false;
					}
				}
			});
		});
		
	});
</script>