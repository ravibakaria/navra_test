<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
    		<div class="switch-field">
        		<input class="form-check-input interest_profile" type="radio" name="interest_profile" id="interest_profile1" value="Interest_By_Me" checked>
                <label for='interest_profile1'><i class="fa fa-check-square-o Interest_By_Me_check"></i> Interest Sent By <?= getUserFirstName($user_id); ?></label>

                <input class="form-check-input interest_profile" type="radio" name="interest_profile" id="interest_profile2" value="Interest_By_Others" >
                <label for='interest_profile2'><i class="fa fa-check-square-o Interest_By_Others_check"></i> Interest Received By <?= getUserFirstName($user_id); ?></label>
        	</div>
        </div>
        <div class="row">
        	<div class="result_data">
        		<!--     Interest result     -->
        	</div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){

		var value = $('input[type=radio][name=interest_profile]').val();
		var user_id_x = $('.user_id').val();
		var task = "Get_Interested_Profile";
		
		if(value=="Interest_By_Me")
		{
			$(".Interest_By_Me_check").show();
			$(".Interest_By_Others_check").hide();
		}
		else
		{
			$(".Interest_By_Me_check").hide();
			$(".Interest_By_Others_check").show();
		}

		var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

		$.ajax({
			type:'post',
        	data:data,
        	url:'query/member-attributes/interest_profiles_helper.php',
        	success:function(res)
        	{
        		$('.result_data').html(res);
        	}
        });

		$('input[type=radio][name=interest_profile]').change(function(){
			var value = $(this).val();
			var user_id_x = $('.user_id').val();
			var task = "Get_Interested_Profile";
			
			if(value=="Interest_By_Me")
			{
				$(".Interest_By_Me_check").show();
				$(".Interest_By_Others_check").hide();
			}
			else
			{
				$(".Interest_By_Me_check").hide();
				$(".Interest_By_Others_check").show();
			}

			var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/member-attributes/interest_profiles_helper.php',
	        	success:function(res)
	        	{
	        		$('.result_data').html(res);
	        	}
	        });
		});
	});
</script>