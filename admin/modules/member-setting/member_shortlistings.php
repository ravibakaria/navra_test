<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
        	<div class="switch-field interest-switch-field">
        		<input class="form-check-input shortlist_profile" type="radio" name="shortlist_profile" id="shortlist_profile1" value="Shortlist_By_Me" checked>
                <label for='shortlist_profile1'><i class="fa fa-check-square-o Shortlist_By_Me_check"></i> Shortlisted Profiles By <?= getUserFirstName($user_id); ?></label>

                <input class="form-check-input shortlist_profile" type="radio" name="shortlist_profile" id="shortlist_profile2" value="Shortlist_By_Others" >
                <label for='shortlist_profile2'><i class="fa fa-check-square-o Shortlist_By_Others_check"></i> Members Shortlisted <?= getUserFirstName($user_id); ?>'s Profile</label>
        	</div>

        </div>
        <div class="row">
        	<div class="shortlist_result_data">
        		<!--     shortlisting result     -->
        	</div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){

		var value = $('input[type=radio][name=shortlist_profile]').val();
		var user_id_x = $('.user_id').val();
		var task = "Get_Shortlisted_Profile";
		
		if(value=="Shortlist_By_Me")
		{
			$(".Shortlist_By_Me_check").show();
			$(".Shortlist_By_Others_check").hide();
		}
		else
		{
			$(".Shortlist_By_Me_check").hide();
			$(".Shortlist_By_Others_check").show();
		}

		var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

		$.ajax({
			type:'post',
        	data:data,
        	url:'query/member-attributes/shortlist_profiles_helper.php',
        	success:function(res)
        	{
        		$('.shortlist_result_data').html(res);
        	}
        });

		$('input[type=radio][name=shortlist_profile]').change(function(){
			var value = $(this).val();
			var user_id_x = $('.user_id').val();
			var task = "Get_Shortlisted_Profile";
			
			if(value=="Shortlist_By_Me")
			{
				$(".Shortlist_By_Me_check").show();
				$(".Shortlist_By_Others_check").hide();
			}
			else
			{
				$(".Shortlist_By_Me_check").hide();
				$(".Shortlist_By_Others_check").show();
			}
			
			var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/member-attributes/shortlist_profiles_helper.php',
	        	success:function(res)
	        	{
	        		$('.shortlist_result_data').html(res);
	        	}
	        });
		});
	});
</script>