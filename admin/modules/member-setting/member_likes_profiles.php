<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
    		<div class="switch-field">
        		<input class="form-check-input liked_profile" type="radio" name="liked_profile" id="liked_profile1" value="Liked_By_Me" checked>
                <label for='liked_profile1'><i class="fa fa-check-square-o Liked_By_Me_check"></i> <?= getUserFirstName($user_id); ?>'s Liked Profiles</label>

                <input class="form-check-input liked_profile" type="radio" name="liked_profile" id="liked_profile2" value="Liked_By_Others" >
                <label for='liked_profile2'><i class="fa fa-check-square-o Liked_By_Others_check"></i> Members Liked <?= getUserFirstName($user_id); ?>'s Profile</label>
        	</div>

        </div>
        <div class="row">
        	<div class="liked_result_data">
        		<!--     Liked result     -->
        	</div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){

		var value = $('input[type=radio][name=liked_profile]').val();
		var user_id_x = $('.user_id').val();
		var task = "Get_Liked_Profile";
		
		if(value=="Liked_By_Me")
		{
			$(".Liked_By_Me_check").show();
			$(".Liked_By_Others_check").hide();
		}
		else
		{
			$(".Liked_By_Me_check").hide();
			$(".Liked_By_Others_check").show();
		}

		var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

		$.ajax({
			type:'post',
        	data:data,
        	url:'query/member-attributes/liked_profiles_helper.php',
        	success:function(res)
        	{
        		$('.liked_result_data').html(res);
        	}
        });

		$('input[type=radio][name=liked_profile]').change(function(){
			var value = $(this).val();
			var user_id_x = $('.user_id').val();
			var task = "Get_Liked_Profile";
			
			if(value=="Liked_By_Me")
			{
				$(".Liked_By_Me_check").show();
				$(".Liked_By_Others_check").hide();
			}
			else
			{
				$(".Liked_By_Me_check").hide();
				$(".Liked_By_Others_check").show();
			}
			
			var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/member-attributes/liked_profiles_helper.php',
	        	success:function(res)
	        	{
	        		$('.liked_result_data').html(res);
	        	}
	        });
		});
	});
</script>