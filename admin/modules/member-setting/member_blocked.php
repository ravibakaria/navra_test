<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
        	<div class="switch-field interest-switch-field">
        		<input class="form-check-input blocked_profile" type="radio" name="blocked_profile" id="blocked_profile1" value="Blocked_By_Me" checked>
                <label for='blocked_profile1'><i class="fa fa-check-square-o Blocked_By_Me_check"></i> Blocked Profiles By <?= getUserFirstName($user_id); ?></label>

                <input class="form-check-input blocked_profile" type="radio" name="blocked_profile" id="blocked_profile2" value="Blocked_By_Others" >
                <label for='blocked_profile2'><i class="fa fa-check-square-o Blocked_By_Others_check"></i> Members Blocked <?= getUserFirstName($user_id); ?>'s Profile</label>
        	</div>

        </div>
        <div class="row">
        	<div class="blocked_result_data">
        		<!--     Interest result     -->
        	</div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){

		var value = $('input[type=radio][name=blocked_profile]').val();
		var user_id_x = $('.user_id').val();
		var task = "Get_Blocked_Profile";
		
		if(value=="Blocked_By_Me")
		{
			$(".Blocked_By_Me_check").show();
			$(".Blocked_By_Others_check").hide();
		}
		else
		{
			$(".Blocked_By_Me_check").hide();
			$(".Blocked_By_Others_check").show();
		}

		var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

		$.ajax({
			type:'post',
        	data:data,
        	url:'query/member-attributes/blocked_profiles_helper.php',
        	success:function(res)
        	{
        		$('.blocked_result_data').html(res);
        	}
        });

		$('input[type=radio][name=blocked_profile]').change(function(){
			var value = $(this).val();
			var user_id_x = $('.user_id').val();
			var task = "Get_Blocked_Profile";
			
			if(value=="Blocked_By_Me")
			{
				$(".Blocked_By_Me_check").show();
				$(".Blocked_By_Others_check").hide();
			}
			else
			{
				$(".Blocked_By_Me_check").hide();
				$(".Blocked_By_Others_check").show();
			}

			var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/member-attributes/blocked_profiles_helper.php',
	        	success:function(res)
	        	{
	        		$('.blocked_result_data').html(res);
	        	}
	        });
		});
	});
</script>