<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h2>Members</h2>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Member">Add New Member</button>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <h3>Apply Filters <i class='fa fa-filter'></i>:</h3> 
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                    Membership: 
                    <select class='form-control memberhip' data-column="1">
                        <option value='-1'>Select/Reset--</option>
                        <option value='1'>Free</option>
                        <option value='2'>Premium</option>
                        <option value='3'>Featured</option>
                    </select>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                    Status
                    <select class='form-control member-status' data-column="4">
                        <option value='-1'>Select/Reset--</option>
                        <option value='11'>In-Active</option>
                        <option value='1'>Active</option>
                        <option value='2'>Deactivated</option>
                        <option value='3'>Suspended</option>
                    </select>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                    Gender
                    <select class='form-control gender' data-column="5">
                        <option value='0'>Select/Reset--</option>
                        <?php
                            $sql_gender = "SELECT * FROM gender WHERE status='1'";
                            $stmt_gender = $link->prepare($sql_gender);
                            $stmt_gender->execute();
                            $count_gender = $stmt_gender->rowCount();
                            if($count_gender>0)
                            {
                                $result_gender = $stmt_gender->fetchAll();
                                foreach ($result_gender as $row_gender) 
                                {
                                    $id = $row_gender['id'];
                                    $name = $row_gender['name'];

                                    echo "<option value='".$id."'>".$name."</option>";
                                }
                            }
                        ?>
                    </select>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                    Marital Status
                    <select class='form-control marital-status' data-column="6">
                        <option value='0'>Select/Reset--</option>
                        <?php
                            $sql_marital_status = "SELECT * FROM maritalstatus WHERE status='1'";
                            $stmt_marital_status = $link->prepare($sql_marital_status);
                            $stmt_marital_status->execute();
                            $count_marital_status = $stmt_marital_status->rowCount();
                            if($count_marital_status>0)
                            {
                                $result_marital_status = $stmt_marital_status->fetchAll();
                                foreach ($result_marital_status as $row_marital_status) 
                                {
                                    $id = $row_marital_status['id'];
                                    $name = $row_marital_status['name'];

                                    echo "<option value='".$id."'>".$name."</option>";
                                }
                            }
                        ?>
                    </select>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                    Age From
                    <select class='form-control from_date' data-column="2">
                        <option value='-1'>Select/Reset--</option>
                        <?php
                            for($i=18;$i<100;$i++)
                            {
                                echo "<option value='".$i."'>".$i."</option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                    To Age
                    <select class='form-control to_date' data-column="3">
                        <option value='-1'>Select/Reset--</option>
                        <?php
                            for($i=18;$i<100;$i++)
                            {
                                echo "<option value='".$i."'>".$i."</option>";
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <hr class="dotted">
        <div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered Member_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email Id</th>
                                <th>Profile ID</th>
                                <th>Registration Date</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                    </table>
                <center>
            </div>
            
            <!-- Modal -->
            <div class="modal fade add-new-member-modal" id="Add-New-Member" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Member</h4></center>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">First Name:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                                    <input type="text" class="form-control form-control-sm Member_firstname" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Last Name:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                                    <input type="text" class="form-control form-control-sm Member_lastname" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Country:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                                    <select class="form-control country">
                                        <option value="0">--Select Country--</option>
                                        <?php
                                            $sql_country = "SELECT * FROM countries WHERE status='1'";
                                            $stmt_country = $link->prepare($sql_country);
                                            $stmt_country->execute();
                                            $result_country = $stmt_country->fetchAll();
                                            foreach ($result_country as $row_country) 
                                            {
                                                $country_id = $row_country['id'];
                                                $country_name = $row_country['name'];

                                                echo "<option value='".$country_id."'>".$country_name."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">State:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                                    <select class="form-control state">
                                        <option value="0">--Select State--</option>
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">City:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                                    <select class="form-control city">
                                        <option value="0">--Select City--</option>
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Email Id:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                                    <input type="text" class="form-control form-control-sm Member_email" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Mobile:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                                    <div class="input-group">
                                        <span class="input-group-addon country_code">
                                            
                                        </span>
                                        <input type="text" name="phonenumber" class="form-control phonenumber" placeholder="Mobile Number" maxlength="20">
                                    </div>
                                    <input type="hidden"  name="phonecode" class="phonecode" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">DOB:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                                    <input type="text" class="form-control form-control-sm Member_dob" name="Member_dob" placeholder="YYYY-MM-DD"/>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Gender:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                                    <select class="form-control Member_gender">
                                        <option value="0">--Select Gender--</option>
                                        <?php
                                            $sql_gender = "SELECT * FROM `gender` WHERE `status`='1'";
                                            $stmt = $link->prepare($sql_gender);
                                            $stmt->execute();
                                            $result = $stmt->fetchAll();

                                            foreach ($result as $row) 
                                            {
                                                $gender_id = $row['id'];
                                                $gender_name = $row['name'];

                                                echo "<option value='".$gender_id."'>".$gender_name."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Status:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='form-control  Member_status' name='Member_status' checked>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status_Member">
                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn_add_new_Member website-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        /*  Datepicker setting  */
        var date_input=$('input[name="Member_dob"]');
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        
        date_input.datepicker({
          format: 'yyyy-mm-dd',
          container: container,
          todayHighlight: true,
          autoclose: true,
        })

        var dataTable_Member_list = $('.Member_list').DataTable({          //Member datatable
            "order": [[ 0, "desc" ]],
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            
            "ajax":{
                url :"datatables/member-attributes/Member-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".Member_list_processing").css("display","none");
                }
            }
        });

        $('.memberhip').change(function(){          // filter member state
            var i =$(this).attr('data-column');
            var v =$(this).val();

            dataTable_Member_list.columns(i).search(v).draw();
        });


        $('.member-status').change(function(){          // filter member state
            var i =$(this).attr('data-column');
            var v =$(this).val();

            dataTable_Member_list.columns(i).search(v).draw();
        });

        $('.gender').change(function(){          // filter member gender
            var i =$(this).attr('data-column');
            var v =$(this).val();

            dataTable_Member_list.columns(i).search(v).draw();
        });

        $('.marital-status').change(function(){          // filter member marital status
            var i =$(this).attr('data-column');
            var v =$(this).val();

            dataTable_Member_list.columns(i).search(v).draw();
        });

        $('.from_date').change(function(){          // filter member age from
            var i =$(this).attr('data-column');
            var v =$(this).val();

            dataTable_Member_list.columns(i).search(v).draw();
        });

        $('.to_date').change(function(){          // filter member age upto
            var i =$(this).attr('data-column');
            var v =$(this).val();

            dataTable_Member_list.columns(i).search(v).draw();
        });

        /*   Prevent entering charaters in mobile & phone number   */
        $(".phonenumber").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                return false;
            }
        });

        $('.country').change(function(){         //Fetch states
            var country_id = $(this).val();
            var task = "Fetch_state_data";  

            $.ajax({
                type:'post',
                data:'country_id='+country_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    var result = $.parseJSON(res);
                    $('.state').html(result[0]);
                    $('.country_code').html("+"+result[1]);
                    $('.phonecode').val(result[1]);
                }
            });         
        });

        $('.state').change(function(){         //Fetch cities
            var state_id = $(this).val();
            var task = "Fetch_city_data";  
            $.ajax({
                type:'post',
                data:'state_id='+state_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    $('.city').html(res);
                }
            });         
        });

        //Member change status
        $('.Member_list tbody').on('click', '.change_status_Member', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_Member_status";

            var data = 'status='+status+'&id='+id+'&task='+task;
            //alert(data);return false;
            $.ajax({
                type:'post',
                data:data,
                url:'query/member-attributes/Member-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.Member_status'+id).html("<div class='alert alert-success Member_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.Member_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("member-setup.php?main_tab=MemberAttributes&sub_tab=Member_Setup");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.Member_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });

        
        //Member add button click
        $('.btn_add_new_Member').click(function(){
            var firstname = $('.Member_firstname').val();
            var lastname = $('.Member_lastname').val();
            var country = $('.country').val();
            var state = $('.state').val();
            var city = $('.city').val();
            var email = $('.Member_email').val();
            var phonecode = $('.phonecode').val();
            var phonenumber = $('.phonenumber').val();
            var dob = $('.Member_dob').val();
            var gender = $('.Member_gender').val();
            var status = $('.Member_status').is(":checked");
            var task = "Add_New_Member";

            if(status==true)
            {
                status='1';
            }
            else
            if(status==false)
            {
                status='0';
            }

            if(firstname=='' || firstname==null)
            {
                $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter member first name.</div></center>");
                return false;
            }

            if(lastname=='' || lastname==null)
            {
                $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter member last name.</div></center>");
                return false;
            }

            if(country=='0')
            {
                $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select country.</div></center>");
                return false;
            }

            if(state=='0')
            {
                $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select state.</div></center>");
                return false;
            }

            if(city=='0')
            {
                $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select city.</div></center>");
                return false;
            }

            if(email=='' || email==null)
            {
                $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter member email id.</div></center>");
                return false;
            }

            function validateEmail($email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $email );
            }

            if( !validateEmail(email)) 
            { 
                $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Please Enter valid email.</div></center>");
                return false;
            }

            if(phonenumber=='' || phonenumber==null)
            {
                $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter mobile number.</div></center>");
                return false;
            }

            if(dob=='' || dob==null)
            {
                $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select date of birth.</div></center>");
                return false;
            }

            if(gender=='0')
            {
                $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please select member gender.</div></center>");
                return false;
            }

            $('.loading_img').show();
            
            var data = 'firstname='+firstname+'&lastname='+lastname+'&email='+email+'&dob='+dob+'&gender='+gender+'&status='+status+'&task='+task;
            

            $.ajax({
                type:'post',
                data:data,
                url:'query/member-attributes/Member-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status_Member').html("<center><div class='alert alert-success add_status_Member_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Member Added Successfully.</div></center>");
                            $('.add_status_Member_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("member-setup.php?main_tab=MemberAttributes&sub_tab=Member_Setup");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status_Member').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>