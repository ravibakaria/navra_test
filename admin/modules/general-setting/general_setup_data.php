<?php
	$stmt   = $link->prepare("SELECT * FROM `generalsetup`");
	$stmt->execute();
	$result = $stmt->fetch();
	$count=$stmt->rowCount();

	$WebSiteBasePath = $result['WebSiteBasePath'];
	$WebSiteTitle = $result['WebSiteTitle'];
    $WebSiteTagline = $result['WebSiteTagline'];
    $EmailAddress = $result['EmailAddress'];
    $LogoURL = $result['LogoURL'];
    $FaviconURL = $result['FaviconURL'];
    $TermOfServiceURL = $result['TermOfServiceURL'];
    $PrivacyPolicyURL = $result['PrivacyPolicyURL'];
    $AllowedFileAttachmentTypes = $result['AllowedFileAttachmentTypes'];


    //General security setting data
    $stmt1   = $link->prepare("SELECT * FROM `generalsecurity`");
	$stmt1->execute();
	$result1 = $stmt1->fetch();
	$count1=$stmt1->rowCount();
	
    $recaptchaAllowed = $result1['recaptchaAllowed'];
    $reCaptchaSiteKey = $result1['reCaptchaSiteKey'];
    $reCaptchaSecretKey = $result1['reCaptchaSecretKey'];
    $MinimumUserPasswordLength = $result1['MinimumUserPasswordLength'];
    $MaximumUserPasswordLength = $result1['MaximumUserPasswordLength'];

    //Localization Setting data
    $stmt2   = $link->prepare("SELECT * FROM `localizationsetup`");
	$stmt2->execute();
	$result2 = $stmt2->fetch();
	$count_localization_setting=$stmt2->rowCount();

	//Mail relay setup data
	$stmt3   = $link->prepare("SELECT * FROM `mailrelaysetup`");
	$stmt3->execute();
	$result3 = $stmt3->fetch();
	$count_mail_relay_setup=$stmt3->rowCount();
?>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Website Basepath:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<input type="text" class="form-control WebSiteBasePath" value="<?php echo $WebSiteBasePath;?>" disabled>
			</div>
		</div>
		
		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Website Title:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<input type="text" class="form-control WebSiteTitle" value="<?php echo $WebSiteTitle;?>">
			</div>
		</div>
		
		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Website Tag Line:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<input type="text" class="form-control WebSiteTagline" value="<?php echo $WebSiteTagline;?>"/>
			</div>
		</div>
		
		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Website Title/Tag Line Preview:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <?php echo $WebSiteTitle;?> | <?php echo $WebSiteTagline;?><br/>
                </div>
			</div>
		</div>
		
		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Notification Email Address:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<input type="text" class="form-control EmailAddress" value="<?php echo $EmailAddress;?>">
			</div>
		</div>
		
		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Upload Logo: 				
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<input type="file" name="LogoURL" id="LogoURL" class="form-control LogoURL" />
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    Recommended Size: 300px X 120px<br/>
                </div>
				<div class="status_LogoURL">
			
				</div>
			</div>
		</div>
		
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Logo Preview: 
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"  id="uploaded_image">
				<center>
					<?php
						if($LogoURL!='' || $LogoURL!=null)
						{
							echo "<img src='$LogoURL' alt='$WebSiteTitle' style='width:285px;height:60px;background-color:#dddddd;'>";
						}
						else
						{
							echo "No image uploaded till now.";
						}
					?>
				<center>
			</div>
		</div>
		
		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Favicon URL:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<input type="file" name="FaviconURL" id="FaviconURL" class="form-control FaviconURL" />
				<div class="status_favicon">
			
				</div>
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Favicon Preview: 
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"  id="uploaded_image_favicon">
				<center>
					<?php
						if($FaviconURL!='' || $FaviconURL!=null)
						{
							echo "<img src='$FaviconURL' alt='$FaviconURL' >";
						}
						else
						{
							echo "No favicon uploaded till now.";
						}
					?>
				</center>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Loader Image:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<input type="file" name="LoaderImage" id="LoaderImage" class="form-control LoaderImage" />
				<div class="status_loader">
			
				</div>
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Loader Image: 
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"  id="uploaded_image_loader">
				<center>
					<img src='../images/loader/loader.gif' alt='Loading' style="width:80px;height:80px;">
				</center>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<div class="alert alert-info margin-top"><strong>Note: </strong> Only <strong>gif</strong> image file is allowed.
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Term Of Service URL:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<input type="text" class="form-control TermOfServiceURL" value="<?php echo $TermOfServiceURL;?>">
			</div>
		</div>
		
		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Privacy Policy URL:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<input type="text" class="form-control PrivacyPolicyURL" value="<?php echo $PrivacyPolicyURL;?>">
			</div>
		</div>
		
		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				Allowed File Attachment Types:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<input type="text" class="form-control AllowedFileAttachmentTypes" value="<?php echo $AllowedFileAttachmentTypes; ?>" >
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<div class="alert alert-info margin-top"><strong>Note: </strong> Please enter file types as comma(,) seperated.<br/>
					For Ex: jpg,png,bmp etc.
				</div>
			</div>
		</div>
		
		<hr class="solid">

	</div>
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<h3>Current Status of settings</h3><hr/>
		<?php
			if($WebSiteTitle=='' || $WebSiteTitle==null)
			{
				echo "<p class='text-danger'><span class='fa fa-times'></span> Update Website Title.</p>";
			}
			else
			{
				echo "<p class='text-success'><span class='fa fa-check'></span> Website Title Updated</p>";
			}

			if($WebSiteTagline=='' || $WebSiteTagline==null)
			{
				echo "<p class='text-danger'><span class='fa fa-times'></span> Update WebSite Tagline.</p>";
			}
			else
			{
				echo "<p class='text-success'><span class='fa fa-check'></span>WebSite Tagline Updated.</p>";
			}

			if($LogoURL=='' || $LogoURL==null)
			{
				echo "<p class='text-danger'><span class='fa fa-times'></span> Upload WebSite Logo pending.</p>";
			}
			else
			{
				echo "<p class='text-success'><span class='fa fa-check'></span> WebSite Logo Uploaded.</p>";
			}

			if($FaviconURL=='' || $FaviconURL==null)
			{
				echo "<p class='text-danger'><span class='fa fa-times'></span> Upload WebSite Favicon pending.</p>";
			}
			else
			{
				echo "<p class='text-success'><span class='fa fa-check'></span> WebSite Favicon Uploaded.</p>";
			}

			if($recaptchaAllowed=='1')
			{
				echo "<p class='text-success'><span class='fa fa-check'></span> Recaptcha applied for website.</p>";
			}
			else
			if($recaptchaAllowed=='0' || $recaptchaAllowed==null)
			{
				echo "<a href='?main_tab=ProjectAttributes&sub_tab=General_Security_Setup'><p class='text-danger'><span class='fa fa-times'></span> Recaptcha not applied for website.</p></a>";
			}

			if($MinimumUserPasswordLength=='' || $MinimumUserPasswordLength==null)
			{
				echo "<a href='?main_tab=ProjectAttributes&sub_tab=General_Security_Setup'><p class='text-danger'><span class='fa fa-times'></span> Minimum User Password Length not defined.</p></a>";
			}
			else
			{
				echo "<p class='text-success'><span class='fa fa-check'></span> Minimum User Password Length defined.</p>";
			}

			if($MaximumUserPasswordLength=='' || $MaximumUserPasswordLength==null)
			{
				echo "<a href='?main_tab=ProjectAttributes&sub_tab=General_Security_Setup'><p class='text-danger'><span class='fa fa-times'></span> Maximum User Password Length not defined.</p></a>";
			}
			else
			{
				echo "<p class='text-success'><span class='fa fa-check'></span> Maximum User Password Length defined.</p>";
			}

			if($count_localization_setting==0)
			{
				echo "<a href='?main_tab=ProjectAttributes&sub_tab=Localization'><p class='text-danger'><span class='fa fa-times'></span> Localization setting not defined.</p></a>";
			}
			else
			if($count_localization_setting>0)
			{
				echo "<p class='text-success'><span class='fa fa-check'></span> Localization setting defined.</p>";
			}

			if($count_mail_relay_setup==0)
			{
				echo "<a href='?main_tab=ProjectAttributes&sub_tab=Mail_Relay_Settings'><p class='text-danger'><span class='fa fa-times'></span> Mail Relay setting not defined.</p></a>";
			}
			else
			if($count_localization_setting>0)
			{
				echo "<p class='text-success'><span class='fa fa-check'></span> Mail Relay setting defined.</p>";
			}

		?>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 generalsetup_status">
				<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<center><button class="btn btn-success btn_generalsetup website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
			</div>
		</div>
	</div>
</div>
<!-- LOGO URL  Model Start  -->
<div id="uploadLogoModal" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
	  		<div class="modal-header">
	    		<button type="button" class="close" data-dismiss="modal">&times;</button>
	    		<h4 class="modal-title">Upload & Crop Logo Pic</h4>
	  		</div>
	  		<div class="modal-body">
	    		<div class="row">
						<div class="col-md-8 text-center">
						  <div id="image_demo_LogoURL" style="width:400px; margin-top:30px"></div>
						</div>
						<div class="col-md-4" style="padding-top:30px;">
							<br />
							<br />
							<br/>
						  <button class="btn btn-success crop_image website-button">Crop & Save Image</button>
					</div>
				</div>
	  		</div>
	  		<div class="modal-footer">
	    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  		</div>
		</div>
	</div>
</div>
<!-- LOGO URL  Model End  -->

<!-- Favicon URL  Model Start  -->
<div id="uploadFaviconModal" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
	  		<div class="modal-header">
	    		<button type="button" class="close" data-dismiss="modal">&times;</button>
	    		<h4 class="modal-title">Upload & Crop Favicon Pic</h4>
	  		</div>
	  		<div class="modal-body">
	    		<div class="row">
						<div class="col-md-8 text-center">
						  <div id="image_demo_favicon" style="width:32px; margin-top:30px"></div>
						</div>
						<div class="col-md-4" style="padding-top:30px;">
							<br />
							<br />
							<br/>
						  <button class="btn btn-success crop_image_favicon website-button">Crop & Save Image</button>
					</div>
				</div>
	  		</div>
	  		<div class="modal-footer">
	    		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  		</div>
		</div>
	</div>
</div>
<!-- Favicon URL  Model End -->