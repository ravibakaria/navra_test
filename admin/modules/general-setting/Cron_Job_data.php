<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt   = $link->prepare("SELECT * FROM `cron_jobs`");
	$stmt->execute();
	$result = $stmt->fetch();
	$count=$stmt->rowCount();

	$EmailVerificationReminderRunOrNot = $result['EmailVerificationReminderRunOrNot'];
    $EmailVerificationReminderRunAt = $result['EmailVerificationReminderRunAt'];
    $ProfileCompletenessReminderRunOrNot = $result['ProfileCompletenessReminderRunOrNot'];
    $ProfileCompletenessRunAt = $result['ProfileCompletenessRunAt'];
    $BirthdayReminderRunOrNot = $result['BirthdayReminderRunOrNot'];
    $BirthdayReminderRunAt = $result['BirthdayReminderRunAt'];
    $NewProfileReminderRunOrNot = $result['NewProfileReminderRunOrNot'];
    $NewProfileReminderFrom = $result['NewProfileReminderFrom'];
    $NewProfileReminderRunAt = $result['NewProfileReminderRunAt'];
    $UnreadMessageReminderRunOrNot = $result['UnreadMessageReminderRunOrNot'];
    $UnreadMessageReminderRunAt = $result['UnreadMessageReminderRunAt'];
    $MembershipRenewalReminderRunOrNot = $result['MembershipRenewalReminderRunOrNot'];
    $MembershipRenewalReminderRunAt = $result['MembershipRenewalReminderRunAt'];

    //Email verification hour & minute
    $EmailVerificationHourMinute = explode(':',$EmailVerificationReminderRunAt);
    $EmailVerificationReminderHour = @$EmailVerificationHourMinute[0];
    $EmailVerificationReminderMinute = @$EmailVerificationHourMinute[1];

    //Profile Completeness hour & minute
    $ProfileCompletenessHourMinute = explode(':',$ProfileCompletenessRunAt);
    $ProfileCompletenessReminderHour = @$ProfileCompletenessHourMinute[0];
    $ProfileCompletenessReminderMinute = @$ProfileCompletenessHourMinute[1];

    //Birthday hour & minute
    $BirthdayHourMinute = explode(':',$BirthdayReminderRunAt);
    $BirthdayReminderHour = @$BirthdayHourMinute[0];
    $BirthdayReminderMinute = @$BirthdayHourMinute[1];

    //New Profile hour & minute
    $NewProfileHourMinute = explode(':',$NewProfileReminderRunAt);
    $NewProfileReminderHour = @$NewProfileHourMinute[0];
    $NewProfileReminderMinute = @$NewProfileHourMinute[1];

    //Unread Message hour & minute
    $UnreadMessageHourMinute = explode(':',$UnreadMessageReminderRunAt);
    $UnreadMessageReminderHour = @$UnreadMessageHourMinute[0];
    $UnreadMessageReminderMinute = @$UnreadMessageHourMinute[1];

    //Membership Renewal hour & minute
    $MembershipRenewalHourMinute = explode(':',$MembershipRenewalReminderRunAt);
    $MembershipRenewalReminderHour = @$MembershipRenewalHourMinute[0];
    $MembershipRenewalReminderMinute = @$MembershipRenewalHourMinute[1];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <!--     Email Verification Reminder Start     -->
            <div class="row input-row">
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Send Email Verification Reminder:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <input type="checkbox" class="EmailVerificationReminderRunOrNot" <?php if($count>0 && ($EmailVerificationReminderRunOrNot=='1')) { echo 'checked';}?>>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This Reminder will be sent to all the members who have not verified there email address.       
                        <br/>
                        This reminder will run daily at time which you provided.<br/>
                    </div>
                </div>
            </div>
            <div class="row input-row EmailVerificationReminderData" <?php if($EmailVerificationReminderRunOrNot!='1') { echo 'style=display:none;';}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Email Verification Reminder Run At:
                </div>
                <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2">
                    Hours: <input class="EmailVerificationReminderHour" type="number" min="0" max="23" placeholder="23" value="<?php echo @$EmailVerificationReminderHour;?>">
                </div>
                <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2">
                    Minutes: <input class="EmailVerificationReminderMinute" type="number" min="0" max="59" placeholder="00" value="<?php echo @$EmailVerificationReminderMinute;?>">
                </div>
            </div>
        <!--     Email Verification Reminder End     -->

        <hr class="dotted">

        <!--     Profile Incompleteness Reminder Start     -->
            <div class="row input-row">
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Send Profile Completeness Reminder:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <input type="checkbox" class="ProfileCompletenessReminderRunOrNot" <?php if($count>0 && ($ProfileCompletenessReminderRunOrNot=='1')) { echo 'checked';}?>>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This Reminder will be sent to all the active members who have not completed there profile.       
                        <br/>
                        This reminder will run daily at time which you provided.<br/>
                    </div>
                </div>
            </div>
            <div class="row input-row ProfileCompletenessReminderData" <?php if($ProfileCompletenessReminderRunOrNot!='1') { echo 'style=display:none;';}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Profile Completeness Reminder Run At:
                </div>
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    Hours: <input class="ProfileCompletenessReminderHour" type="number" min="0" max="23" placeholder="23" value="<?php echo @$ProfileCompletenessReminderHour;?>">
                </div>
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    Minutes: <input class="ProfileCompletenessReminderMinute" type="number" min="0" max="59" placeholder="00" value="<?php echo @$ProfileCompletenessReminderMinute;?>">
                </div>
            </div>
        <!--     Profile Incompleteness Reminder End     -->

        <hr class="dotted">

        <!--     Birthday Reminder Start     -->
            <div class="row input-row">
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Send Birthday Reminder:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <input type="checkbox" class="BirthdayReminderRunOrNot" <?php if($count>0 && ($BirthdayReminderRunOrNot=='1')) { echo 'checked';}?>>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This Reminder will be sent to all the active members who have birthday on today.       
                        <br/>
                        This reminder will run daily at time which you provided.<br/>
                    </div>
                </div>
            </div>
            <div class="row input-row BirthdayReminderData" <?php if($BirthdayReminderRunOrNot!='1') { echo 'style=display:none;';}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Birthday Reminder Run At:
                </div>
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    Hours: <input class="BirthdayReminderHour" type="number" min="0" max="23" placeholder="23" value="<?php echo @$BirthdayReminderHour;?>">
                </div>
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    Minutes: <input class="BirthdayReminderMinute" type="number" min="0" max="59" placeholder="00" value="<?php echo @$BirthdayReminderMinute;?>">
                </div>
            </div>
        <!--     Profile Incompleteness Reminder End     -->

        <hr class="dotted">

        <!--     New Profile Reminder Start     -->
            <div class="row input-row">
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Send New Profile Notification:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <input type="checkbox" class="NewProfileReminderRunOrNot" <?php if($count>0 && ($NewProfileReminderRunOrNot=='1')) { echo 'checked';}?>>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> All the new member profiles added from last day,week or month(from selected option by you) will be sent to active members.
                    </div>
                </div>
            </div>
            <div class="row input-row NewProfileReminderData" <?php if($NewProfileReminderRunOrNot!='1') { echo 'style=display:none;';}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    New Profile Notification Run At:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class="row">
                        <select class="NewProfileReminderFrom">
                        <?php
                            if($NewProfileReminderFrom=='daily')
                            {
                                echo "<option value='".$NewProfileReminderFrom."' selected>Daily</option>";
                            }
                            else
                            {
                                echo "<option value='daily'>Daily</option>";
                            }

                            if($NewProfileReminderFrom=='weekly')
                            {
                                echo "<option value='".$NewProfileReminderFrom."' selected>Weekly</option>";
                            }
                            else
                            {
                                echo "<option value='weekly'>Weekly</option>";
                            }

                            if($NewProfileReminderFrom=='monthly')
                            {
                                echo "<option value='".$NewProfileReminderFrom."' selected>Monthly</option>";
                            }
                            else
                            {
                                echo "<option value='monthly'>Monthly</option>";
                            }
                        ?>
                        </select>
                    </div>
                    <br/>
                    <div class="row">
                        Hours: <input class="NewProfileReminderHour" type="number" min="0" max="23" placeholder="23" value="<?php echo @$NewProfileReminderHour;?>">&nbsp;&nbsp;&nbsp;
                        Minutes: <input class="NewProfileReminderMinute" type="number" min="0" max="59" placeholder="00" value="<?php echo @$NewProfileReminderMinute;?>">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <strong>Notification Runs at: </strong> 
                        <ul>
                            <li><b>Daily: </b> At configured time</li>
                            <li><b>Weekly: </b> Every Sunday At configured time</li>
                            <li><b>Monthly: </b> On 1<sup>st</sup> of every month At configured time</li>
                        </ul>
                    </div>
                </div>
            </div>
        <!--     Profile Incompleteness Reminder End     -->

        <hr class="dotted">

        <!--     Unread Message Reminder Start     -->
            <div class="row input-row">
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Send Unread Message Reminder:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <input type="checkbox" class="UnreadMessageReminderRunOrNot" <?php if($count>0 && ($UnreadMessageReminderRunOrNot=='1')) { echo 'checked';}?>>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This Reminder will be sent to active members who have received new chat messages from members.       
                        <br/>
                        This reminder will run daily at time which you provided.<br/>
                    </div>
                </div>
            </div>
            <div class="row input-row UnreadMessageReminderData" <?php if($UnreadMessageReminderRunOrNot!='1') { echo 'style=display:none;';}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Unread Message Reminder Run At:
                </div>
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    Hours: <input class="UnreadMessageReminderHour" type="number" min="0" max="23" placeholder="23" value="<?php echo @$UnreadMessageReminderHour;?>">
                </div>
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    Minutes: <input class="UnreadMessageReminderMinute" type="number" min="0" max="59" placeholder="00" value="<?php echo @$UnreadMessageReminderMinute;?>">
                </div>
            </div>
        <!--     Profile Incompleteness Reminder End     -->

        <hr class="dotted">

        <!--     Membership Renewal Reminder Start     -->
            <div class="row input-row">
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Send Membership Renewal Reminder:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <input type="checkbox" class="MembershipRenewalReminderRunOrNot" <?php if($count>0 && ($MembershipRenewalReminderRunOrNot=='1')) { echo 'checked';}?>>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This Reminder will be sent to active members whos membership will be expiring in next 1 day, 7 days or 15 days.       
                        <br/>
                        This reminder will run daily at time which you provided.<br/>
                    </div>
                </div>
            </div>
            <div class="row input-row MembershipRenewalReminderData" <?php if($MembershipRenewalReminderRunOrNot!='1') { echo 'style=display:none;';}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Membership Renewal Reminder Run At:
                </div>
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    Hours: <input class="MembershipRenewalReminderHour" type="number" min="0" max="23" placeholder="23" value="<?php echo @$MembershipRenewalReminderHour;?>">
                </div>
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    Minutes: <input class="MembershipRenewalReminderMinute" type="number" min="0" max="59" placeholder="00" value="<?php echo @$MembershipRenewalReminderMinute;?>">
                </div>
            </div>
        <!--     Profile Incompleteness Reminder End     -->

        <hr class="dotted">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cron_job_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_cron_job' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><button class="btn btn-success btn_submit_cron_job website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        /*    EmailVerificationReminder start   */
            $('.EmailVerificationReminderRunOrNot').change(function(){

                if($(this).is(':checked'))
                {
                    var EmailVerificationReminderRunOrNot = '1';
                }
                else
                {
                    var EmailVerificationReminderRunOrNot = '0';
                }

                if(EmailVerificationReminderRunOrNot=='1')
                {
                    $('.EmailVerificationReminderData').show();
                    return false;
                }
                else
                {
                    $('.EmailVerificationReminderData').hide();
                    return false;
                }    
            });
        /*    EmailVerificationReminder end   */

        /*    ProfileCompletenessReminder start   */
            $('.ProfileCompletenessReminderRunOrNot').change(function(){

                if($(this).is(':checked'))
                {
                    var ProfileCompletenessReminderRunOrNot = '1';
                }
                else
                {
                    var ProfileCompletenessReminderRunOrNot = '0';
                }

                if(ProfileCompletenessReminderRunOrNot=='1')
                {
                    $('.ProfileCompletenessReminderData').show();
                    return false;
                }
                else
                {
                    $('.ProfileCompletenessReminderData').hide();
                    return false;
                }    
            });
        /*    ProfileCompletenessReminder end   */

        /*    BirthdayReminder start   */
            $('.BirthdayReminderRunOrNot').change(function(){

                if($(this).is(':checked'))
                {
                    var BirthdayReminderRunOrNot = '1';
                }
                else
                {
                    var BirthdayReminderRunOrNot = '0';
                }

                if(BirthdayReminderRunOrNot=='1')
                {
                    $('.BirthdayReminderData').show();
                    return false;
                }
                else
                {
                    $('.BirthdayReminderData').hide();
                    return false;
                }    
            });
        /*    BirthdayReminder start   */

        /*    NewProfileReminder start   */
            $('.NewProfileReminderRunOrNot').change(function(){

                if($(this).is(':checked'))
                {
                    var NewProfileReminderRunOrNot = '1';
                }
                else
                {
                    var NewProfileReminderRunOrNot = '0';
                }

                if(NewProfileReminderRunOrNot=='1')
                {
                    $('.NewProfileReminderData').show();
                    return false;
                }
                else
                {
                    $('.NewProfileReminderData').hide();
                    return false;
                }    
            });
        /*    NewProfileReminder start   */

        /*    UnreadMessageReminder start   */
            $('.UnreadMessageReminderRunOrNot').change(function(){

                if($(this).is(':checked'))
                {
                    var UnreadMessageReminderRunOrNot = '1';
                }
                else
                {
                    var UnreadMessageReminderRunOrNot = '0';
                }

                if(UnreadMessageReminderRunOrNot=='1')
                {
                    $('.UnreadMessageReminderData').show();
                    return false;
                }
                else
                {
                    $('.UnreadMessageReminderData').hide();
                    return false;
                }    
            });
        /*    BirthdayReminder start   */

        /*    MembershipRenewalReminder start   */
            $('.MembershipRenewalReminderRunOrNot').change(function(){

                if($(this).is(':checked'))
                {
                    var MembershipRenewalReminderRunOrNot = '1';
                }
                else
                {
                    var MembershipRenewalReminderRunOrNot = '0';
                }

                if(MembershipRenewalReminderRunOrNot=='1')
                {
                    $('.MembershipRenewalReminderData').show();
                    return false;
                }
                else
                {
                    $('.MembershipRenewalReminderData').hide();
                    return false;
                }    
            });
        /*    BirthdayReminder start   */

        $('.btn_submit_cron_job').click(function(){

            /*****    Email Verification Reminder Start    *****/
                if($('.EmailVerificationReminderRunOrNot').is(':checked'))
                {
                    var EmailVerificationReminderRunOrNot = '1';
                }
                else
                {
                    var EmailVerificationReminderRunOrNot = '0';
                }

                var EmailVerificationReminderHour = $('.EmailVerificationReminderHour').val();
                var EmailVerificationReminderMinute = $('.EmailVerificationReminderMinute').val();

                var EmailVerificationReminderRunAt = EmailVerificationReminderHour+':'+EmailVerificationReminderMinute+':00';

                if(EmailVerificationReminderRunOrNot=='1' && ((EmailVerificationReminderHour=='00' || EmailVerificationReminderHour=='' || EmailVerificationReminderHour==null) && (EmailVerificationReminderMinute=='00' || EmailVerificationReminderMinute=='' || EmailVerificationReminderMinute==null)))
                {
                    $('.cron_job_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter time to run Email Verification Reminder at time.</div></center>");
                    return false
                }

                if(EmailVerificationReminderHour=='' || EmailVerificationReminderHour==null)
                {
                    EmailVerificationReminderRunAt = '00:'+EmailVerificationReminderMinute+':00';
                }

                if(EmailVerificationReminderMinute=='' || EmailVerificationReminderMinute==null)
                {
                    EmailVerificationReminderRunAt = EmailVerificationReminderHour+':00'+':00';
                }
            /*****    Email Verification Reminder End    *****/

            /*****    Profile Completeness Reminder Start    *****/
                if($('.ProfileCompletenessReminderRunOrNot').is(':checked'))
                {
                    var ProfileCompletenessReminderRunOrNot = '1';
                }
                else
                {
                    var ProfileCompletenessReminderRunOrNot = '0';
                }

                var ProfileCompletenessReminderHour = $('.ProfileCompletenessReminderHour').val();
                var ProfileCompletenessReminderMinute = $('.ProfileCompletenessReminderMinute').val();

                var ProfileCompletenessRunAt = ProfileCompletenessReminderHour+':'+ProfileCompletenessReminderMinute+':00';

                if(ProfileCompletenessReminderRunOrNot=='1' && ((ProfileCompletenessReminderHour=='00' || ProfileCompletenessReminderHour=='' || ProfileCompletenessReminderHour==null) && (ProfileCompletenessReminderMinute=='00' || ProfileCompletenessReminderMinute=='' || ProfileCompletenessReminderMinute==null)))
                {
                    $('.cron_job_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter time to run Profile completeness Reminder at time.</div></center>");
                    return false
                }

                if(ProfileCompletenessReminderHour=='' || ProfileCompletenessReminderHour==null)
                {
                    ProfileCompletenessRunAt = '00:'+ProfileCompletenessReminderMinute+':00';
                }

                if(ProfileCompletenessReminderMinute=='' || ProfileCompletenessReminderMinute==null)
                {
                    ProfileCompletenessRunAt = ProfileCompletenessReminderHour+':00'+':00';
                }
            /*****    Profile Completeness Reminder End    *****/
            
            /*****    Birthday Reminder Start    *****/
                if($('.BirthdayReminderRunOrNot').is(':checked'))
                {
                    var BirthdayReminderRunOrNot = '1';
                }
                else
                {
                    var BirthdayReminderRunOrNot = '0';
                }

                var BirthdayReminderHour = $('.BirthdayReminderHour').val();
                var BirthdayReminderMinute = $('.BirthdayReminderMinute').val();

                var BirthdayReminderRunAt = BirthdayReminderHour+':'+BirthdayReminderMinute+':00';

                if(BirthdayReminderRunOrNot=='1' && ((BirthdayReminderHour=='00' || BirthdayReminderHour=='' || BirthdayReminderHour==null) && (BirthdayReminderMinute=='00' || BirthdayReminderMinute=='' || BirthdayReminderMinute==null)))
                {
                    $('.cron_job_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter time to send birthday wish Email time.</div></center>");
                    return false
                }

                if(BirthdayReminderHour=='' || BirthdayReminderHour==null)
                {
                    BirthdayReminderRunAt = '00:'+BirthdayReminderMinute+':00';
                }

                if(BirthdayReminderMinute=='' || BirthdayReminderMinute==null)
                {
                    BirthdayReminderRunAt = BirthdayReminderHour+':00'+':00';
                }
            /*****    Birtday Reminder End    *****/

            /*****    New Profile Reminder Start    *****/
                if($('.NewProfileReminderRunOrNot').is(':checked'))
                {
                    var NewProfileReminderRunOrNot = '1';
                }
                else
                {
                    var NewProfileReminderRunOrNot = '0';
                }

                var NewProfileReminderFrom = $('.NewProfileReminderFrom').val();
                var NewProfileReminderHour = $('.NewProfileReminderHour').val();
                var NewProfileReminderMinute = $('.NewProfileReminderMinute').val();

                var NewProfileReminderRunAt = NewProfileReminderHour+':'+NewProfileReminderMinute+':00';

                if(NewProfileReminderRunOrNot=='1' && ((NewProfileReminderHour=='00' || NewProfileReminderHour=='' || NewProfileReminderHour==null) && (NewProfileReminderMinute=='00' || NewProfileReminderMinute=='' || NewProfileReminderMinute==null)))
                {
                    $('.cron_job_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter time to run New profile Reminder at time.</div></center>");
                    return false
                }

                if(NewProfileReminderHour=='' || NewProfileReminderHour==null)
                {
                    NewProfileReminderRunAt = '00:'+NewProfileReminderMinute+':00';
                }

                if(NewProfileReminderMinute=='' || NewProfileReminderMinute==null)
                {
                    NewProfileReminderRunAt = NewProfileReminderHour+':00'+':00';
                }
            /*****    New Profile Reminder End    *****/

            /*****    Unread Message Reminder Start    *****/
                if($('.UnreadMessageReminderRunOrNot').is(':checked'))
                {
                    var UnreadMessageReminderRunOrNot = '1';
                }
                else
                {
                    var UnreadMessageReminderRunOrNot = '0';
                }

                var UnreadMessageReminderHour = $('.UnreadMessageReminderHour').val();
                var UnreadMessageReminderMinute = $('.UnreadMessageReminderMinute').val();

                var UnreadMessageReminderRunAt = UnreadMessageReminderHour+':'+UnreadMessageReminderMinute+':00';

                if(UnreadMessageReminderRunOrNot=='1' && ((UnreadMessageReminderHour=='00' || UnreadMessageReminderHour=='' || UnreadMessageReminderHour==null) && (UnreadMessageReminderMinute=='00' || UnreadMessageReminderMinute=='' || UnreadMessageReminderMinute==null)))
                {
                    $('.cron_job_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter time to run Email Unread Message Reminder at time.</div></center>");
                    return false
                }

                if(UnreadMessageReminderHour=='' || UnreadMessageReminderHour==null)
                {
                    UnreadMessageReminderRunAt = '00:'+UnreadMessageReminderMinute+':00';
                }

                if(UnreadMessageReminderMinute=='' || UnreadMessageReminderMinute==null)
                {
                    UnreadMessageReminderRunAt = UnreadMessageReminderHour+':00'+':00';
                }
            /*****    Unread Message Reminder End    *****/

            /*****    Membership Renewal Reminder Start    *****/
                if($('.MembershipRenewalReminderRunOrNot').is(':checked'))
                {
                    var MembershipRenewalReminderRunOrNot = '1';
                }
                else
                {
                    var MembershipRenewalReminderRunOrNot = '0';
                }

                var MembershipRenewalReminderHour = $('.MembershipRenewalReminderHour').val();
                var MembershipRenewalReminderMinute = $('.MembershipRenewalReminderMinute').val();

                var MembershipRenewalReminderRunAt = MembershipRenewalReminderHour+':'+MembershipRenewalReminderMinute+':00';

                if(MembershipRenewalReminderRunOrNot=='1' && ((MembershipRenewalReminderHour=='00' || MembershipRenewalReminderHour=='' || MembershipRenewalReminderHour==null) && (MembershipRenewalReminderMinute=='00' || MembershipRenewalReminderMinute=='' || MembershipRenewalReminderMinute==null)))
                {
                    $('.cron_job_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter time to run Membership Renewal Reminder at time.</div></center>");
                    return false
                }

                if(MembershipRenewalReminderHour=='' || MembershipRenewalReminderHour==null)
                {
                    MembershipRenewalReminderRunAt = '00:'+MembershipRenewalReminderMinute+':00';
                }

                if(MembershipRenewalReminderMinute=='' || MembershipRenewalReminderMinute==null)
                {
                    MembershipRenewalReminderRunAt = MembershipRenewalReminderHour+':00'+':00';
                }
            /*****    Membership Renewal Reminder End    *****/

            var task = "Update_Cron_Job_Sheduler";
            var data = 'EmailVerificationReminderRunOrNot='+EmailVerificationReminderRunOrNot+'&EmailVerificationReminderRunAt='+EmailVerificationReminderRunAt+'&ProfileCompletenessReminderRunOrNot='+ProfileCompletenessReminderRunOrNot+'&ProfileCompletenessRunAt='+ProfileCompletenessRunAt+'&BirthdayReminderRunOrNot='+BirthdayReminderRunOrNot+'&BirthdayReminderRunAt='+BirthdayReminderRunAt+'&NewProfileReminderRunOrNot='+NewProfileReminderRunOrNot+'&NewProfileReminderFrom='+NewProfileReminderFrom+'&NewProfileReminderRunAt='+NewProfileReminderRunAt+'&UnreadMessageReminderRunOrNot='+UnreadMessageReminderRunOrNot+'&UnreadMessageReminderRunAt='+UnreadMessageReminderRunAt+'&MembershipRenewalReminderRunOrNot='+MembershipRenewalReminderRunOrNot+'&MembershipRenewalReminderRunAt='+MembershipRenewalReminderRunAt+'&task='+task;

            $('.loading_img_cron_job').show();
            $('.cron_job_status').html("");

            $.ajax({
                type:'post',
                data:data,
                url:'query/general_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_cron_job').hide();
                    if(res=='success')
                    {
                        $('.cron_job_status').html("<center><div class='alert alert-success cron_job_status_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
                        $('.cron_job_status_success_status').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Cron_Job_Settings");
                        });
                    }
                    else
                    {
                        $('.cron_job_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>