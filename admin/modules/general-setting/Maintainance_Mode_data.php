<?php
	$stmt   = $link->prepare("SELECT * FROM `maintainance_mode_setting`");
	$stmt->execute();
	$result = $stmt->fetch();
	$count=$stmt->rowCount();

	$maintainance_mode = $result['maintainance_mode'];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <!--     Maintainance Mode Start     -->
            <div class="row input-row">
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Activate Maintainance Mode:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
                    <input type="checkbox" class="maintainance_mode" <?php if($count>0 && ($maintainance_mode=='1')) { echo 'checked';}?>>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you activate/on this maintainance mode then maintainance mode page will be displaying to all users who accessing website.<br/>
                    </div>
                </div>
            </div>
        <!--     Maintainance Mode End     -->
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 maintainance_mode_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_maintainance_mode' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><button class="btn btn-success btn_submit_maintainance_mode website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
    	$('.btn_submit_maintainance_mode').click(function(){
    		if($('.maintainance_mode').is(':checked'))
            {
                var maintainance_mode = '1';
            }
            else
            {
                var maintainance_mode = '0';
            }

            var task = "update-maintainance-mode";

            var data = 'maintainance_mode='+maintainance_mode+'&task='+task;

            $('.loading_img_maintainance_mode').show();
            $('.maintainance_mode_status').html("");

            $.ajax({
                type:'post',
                data:data,
                url:'query/general_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img_maintainance_mode').hide();
                    if(res=='success')
                    {
                        $('.maintainance_mode_status').html("<center><div class='alert alert-success maintainance_mode_status_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
                        $('.maintainance_mode_status_status').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Maintainance_Mode_Settings");
                        });
                    }
                    else
                    {
                        $('.maintainance_mode_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
    	});
    });
</script>