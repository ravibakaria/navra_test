<?php
	$stmt   = $link->prepare("SELECT * FROM `generalsecurity`");
	$stmt->execute();
	$result = $stmt->fetch();
	$count=$stmt->rowCount();
	
    $recaptchaAllowed = $result['recaptchaAllowed'];
    $reCaptchaSiteKey = $result['reCaptchaSiteKey'];
    $reCaptchaSecretKey = $result['reCaptchaSecretKey'];
    $MinimumUserPasswordLength = $result['MinimumUserPasswordLength'];
    $MaximumUserPasswordLength = $result['MaximumUserPasswordLength'];
    $hideLastNameOfMember = $result['hideLastNameOfMember'];

    $stmt1   = $link->prepare("SELECT * FROM `temp_recaptcha`");
    $stmt1->execute();
    $result1 = $stmt1->fetch();
    $count1=$stmt1->rowCount();
    
    $reCaptchaSiteKeyTemp = $result1['reCaptchaSiteKey'];
    $reCaptchaSecretKeyTemp = $result1['reCaptchaSecretKey'];

    $i = $j = null;
?>
<input type="hidden" class="recaptchaAllowed_default" value="<?php echo $recaptchaAllowed; ?>">
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Apply recaptcha v3:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
                <input type="checkbox" class="recaptchaAllowed" <?php if($count>0 && ($recaptchaAllowed=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong>Please tick <strong>"Apply recaptcha"</strong> checkbox to apply recaptcha on website pages.
                </div>
            </div>
        </div>
        <div class="row input-row recaptcha-div">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                reCaptchaSiteKey:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input type="text" class="form-control reCaptchaSiteKey" value="<?php echo $reCaptchaSiteKey;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 status_reCaptchaSiteKey">
                
            </div>
        </div>

        <div class="row input-row recaptcha-div">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                reCaptchaSecretKey:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input type="text" class="form-control reCaptchaSecretKey" value="<?php echo $reCaptchaSecretKey;?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 status_reCaptchaSecretKey">
                
            </div>
        </div>
      

        <div class="row input-row recaptcha-div">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 recaptcha_status_show">
                
            </div>
        </div> 

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Minimum User Password Length:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <select class="form-control MinimumUserPasswordLength">
                    <?php
                        for($i=8;$i<101;$i++)
                        {
                            if(($MinimumUserPasswordLength=='' || $MinimumUserPasswordLength==null) && $i==10)
                            {
                                echo "<option value='".$i."' selected>".$i."</option>";
                            }
                            else
                            if(($MinimumUserPasswordLength!='' || $MinimumUserPasswordLength!=null) && $MinimumUserPasswordLength==$i)
                            {
                                echo "<option value='".$i."' selected>".$i."</option>";
                            }
                            else
                            {
                                echo "<option value='".$i."'>".$i."</option>";
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Maximum User Password Length:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <select class="form-control MaximumUserPasswordLength">
                    <?php
                        for($j=8;$j<101;$j++)
                        {
                            if(($MaximumUserPasswordLength=='' || $MaximumUserPasswordLength==null) && $j==20)
                            {
                                echo "<option value='".$j."' selected>".$j."</option>";
                            }
                            else
                            if(($MaximumUserPasswordLength!='' || $MaximumUserPasswordLength!=null) && $MaximumUserPasswordLength==$j)
                            {
                                echo "<option value='".$j."' selected>".$j."</option>";
                            }
                            else
                            {
                                echo "<option value='".$j."'>".$j."</option>";
                            }
                        }
                    ?>
                </select>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Hide Last Name Of Member :
            </div>
            <div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
                <input type="checkbox" class="hideLastNameOfMember" <?php if($count>0 && ($hideLastNameOfMember=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>For Ex: </strong>  If Member name is 'John Smith' then it will display 'John SXXXXXXXXXX' on member profile page.
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 generalsecuritysetup_status">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><button class="btn btn-success btn_generalsecuritysetup website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function(){

        var recaptchaAllowed_default = $('.recaptchaAllowed_default').val();

        if(recaptchaAllowed_default=='0' || recaptchaAllowed_default=='' || recaptchaAllowed_default==null || recaptchaAllowed_default=='undefined')
        {
            $('.recaptcha-div').hide();
        }
        

        /*******   clear reCaptchaSiteKey status start    ******/
            $('.reCaptchaSiteKey').keypress(function(){
                $('.status_reCaptchaSiteKey').html("");
                $('.g-recaptcha').attr('data-sitekey','');
            });

            $('.reCaptchaSiteKey').click(function(){
                $('.status_reCaptchaSiteKey').html("");
            });
        /*******     clear reCaptchaSiteKey status end        ********/

        /*********      clear reCaptchaSecretKey status start   *******/
            $('.reCaptchaSecretKey').keypress(function(){
                $('.status_reCaptchaSecretKey').html("");
            });

            $('.reCaptchaSecretKey').click(function(){
                $('.status_reCaptchaSecretKey').html("");
            });
        /********      clear reCaptchaSecretKey status end    *********/

        /********    Show recaptcha input text box  start  *********/
            $(".recaptchaAllowed").click(function(){
                if($(".recaptchaAllowed").is(':checked'))
                {
                    var recaptchaAllowed = '1';
                }
                else
                {
                    var recaptchaAllowed = '0';
                }

                if(recaptchaAllowed=='1')
                {
                    $('.recaptcha-div').show();
                }
                else
                if(recaptchaAllowed=='0')
                {
                    $('.recaptcha-div').hide();
                }
            });
        /********    Show recaptcha input text box  end  *********/

        /********    General Setup Save start  *********/
            $('.btn_generalsecuritysetup').click(function(){

                if($(".recaptchaAllowed").is(':checked'))
                {
                    var recaptchaAllowed = '1';
                }
                else
                {
                    var recaptchaAllowed = '0';
                }
                var reCaptchaSiteKey = $('.reCaptchaSiteKey').val();
                var reCaptchaSecretKey = $('.reCaptchaSecretKey').val();
                var MinimumUserPasswordLength = $('.MinimumUserPasswordLength').val();
                var MaximumUserPasswordLength = $('.MaximumUserPasswordLength').val();
                var recaptcha_status;

                if($(".hideLastNameOfMember").is(':checked'))
                {
                    var hideLastNameOfMember = '1';
                }
                else
                {
                    var hideLastNameOfMember = '0';
                }

                var task = "Update_General_Security_Setup";

                if(recaptchaAllowed=='1')
                {
                    if(reCaptchaSiteKey=='' || reCaptchaSiteKey==null)
                    {
                        $('.status_reCaptchaSiteKey').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter reCaptchaSiteKey..</div></center>");
                        recaptcha_status='0';
                    }

                    if(reCaptchaSecretKey=='' || reCaptchaSecretKey==null)
                    {
                        $('.status_reCaptchaSecretKey').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter reCaptchaSecretKey..</div></center>");
                        recaptcha_status='0';
                    }
                }

                if(recaptcha_status=='0')
                {
                    return false;
                }

                var data = 'recaptchaAllowed='+recaptchaAllowed+'&reCaptchaSiteKey='+reCaptchaSiteKey+'&reCaptchaSecretKey='+reCaptchaSecretKey+'&MinimumUserPasswordLength='+MinimumUserPasswordLength+'&MaximumUserPasswordLength='+MaximumUserPasswordLength+'&hideLastNameOfMember='+hideLastNameOfMember+'&task='+task;
                
                $('.loading_img').show();

                $.ajax({
                    type:'post',
                    data:data,
                    url:'query/general_setup_helper.php',
                    success:function(res)
                    {
                        $('.loading_img').hide();
                        if(res=='success')
                        {
                            $('.btn_generalsecuritysetup').attr('disabled',true);
                            $('.generalsecuritysetup_status').html("<center><div class='alert alert-success general_security_setup_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
                            $('.general_security_setup_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                $('.btn_generalsecuritysetup').attr('disabled',false);
                                  window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=General_Security_Setup");
                            });
                        }
                        else
                        {
                            $('.generalsecuritysetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                            return false;
                        }
                    }
                });
            });
        /********    General Setup Save end  *********/
    });
</script>