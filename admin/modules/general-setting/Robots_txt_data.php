<?php
    $websiteBasePath = getWebsiteBasePath();

    $stmt   = $link->prepare("SELECT * FROM `robots_txt`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    $robots_txt_allowed = $result['robots_txt_allowed'];

    if($robots_txt_allowed=='1')
    {
        $robots_txt_file_name = '../robots.txt';
        $robots_txt = file_get_contents($robots_txt_file_name);
    }
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Allow Robots.txt:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
                <input type="checkbox" class="robots_txt_allowed" <?php if($count>0 && ($robots_txt_allowed=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong>If you enable this property then <b><i>"robots.txt"</i></b> will be created in root folder of project otherwise it will be deleted from root folder of project.<br>
                </div>
            </div>
        </div>
        <div class="row input-row robot_txt_data" <?php if($robots_txt_allowed!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Robots.txt Content:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <textarea class='form-control robots_txt' rows="8"><?php if($count>0) { echo @$robots_txt;}?></textarea>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 robots_txt_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <center><button class="btn btn-success btn_robots_txt website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <center><button class="btn btn-success btn_robots_txt_reset website-button"><i class="fa fa-refresh"></i> &nbsp;Reset Default</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $('.robots_txt_allowed').change(function(){

            if($(this).is(':checked'))
            {
                var robots_txt_allowed = '1';
            }
            else
            {
                var robots_txt_allowed = '0';
            }

            if(robots_txt_allowed=='1')
            {
                $('.robot_txt_data').show();
                return false;
            }
            else
            {
                $('.robot_txt_data').hide();
                return false;
            }
        });

        $('.btn_robots_txt').click(function(){
            if($('.robots_txt_allowed').is(':checked'))
            {
                var robots_txt_allowed = '1';
            }
            else
            {
                var robots_txt_allowed = '0';
            }

            var robots_txt = $('.robots_txt').val();
            var task = "update-robots-txt-generation";

            var data = {
                robots_txt_allowed : robots_txt_allowed,
                robots_txt : robots_txt,
                task : task
            }

            $.ajax({
                type : 'post',
                dataType : 'json',
                data : data,
                url : 'query/general_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.robots_txt_status').html("<center><div class='alert alert-success robots_txt_status_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
                        $('.robots_txt_status_status').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Robots_txt_Settings");
                        });
                    }
                    else
                    {
                        $('.robots_txt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });

        $('.btn_robots_txt_reset').click(function(){
            var task = "reset-robots-txt-default";
            
            var data = {
                task : task
            }

            $('.loading_img').show();

            $.ajax({
                type:'post',
                dataType:'json',
                data:data,
                url:'query/general_setup_helper.php',
                success:function(res){
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.robots_txt_status').html("<center><div class='alert alert-success robots_txt_status_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Robots.txt resets to default Successfully.</div></center>");
                        $('.robots_txt_status_status').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Robots_txt_Settings");
                        });
                    }
                    else
                    {
                        $('.robots_txt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>