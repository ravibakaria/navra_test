<?php
	$stmt   = $link->prepare("SELECT * FROM `localizationsetup`");
	$stmt->execute();
	$result = $stmt->fetch();
	$count=$stmt->rowCount();

	$DefaultCountry = $result['DefaultCountry'];
    $DefaultTimeZone = $result['DefaultTimeZone'];
    $DefaultCurrency = $result['DefaultCurrency'];
?>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Default Country:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<select class="form-control DefaultCountry">
					<option value="0">---Select Country---</option>
					<?php
						$query  = "SELECT * FROM `countries` ORDER BY `id` ASC";
						$stmt   = $link->prepare($query);
						$stmt->execute();
						$result = $stmt->fetchAll();
						foreach( $result as $row )
						{
							$countries_name =  $row['name'];
							
							if($countries_name==$DefaultCountry)
							{
								echo "<option value='".$countries_name."' selected>".$countries_name."</option>";
							}
							else
							{
								echo "<option value=".$countries_name.">".$countries_name."</option>";
							}
						}
					?>
				</select>
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Default Time Zone:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<select class="form-control DefaultTimeZone">
					<option value="0">---Select Time Zone---</option>
					<?php
						$query  = "SELECT * FROM `timezone` ORDER BY `zone_name` ASC";
						$stmt   = $link->prepare($query);
						$stmt->execute();
						$result = $stmt->fetchAll();
						foreach( $result as $row )
						{
							$zone_name =  $row['zone_name'];

							if($zone_name==$DefaultTimeZone)
							{
								echo "<option value='".$zone_name."' selected>".$zone_name."</option>";
							}
							else
							{
								echo "<option value=".$zone_name.">".$zone_name."</option>";
							}
						}
					?>
				</select>
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Default Currency:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<select class="form-control DefaultCurrency">
					<option value="0">---Select Currency---</option>
					<?php
						$query  = "SELECT * FROM `currency` ORDER BY `currency` ASC";
						$stmt   = $link->prepare($query);
						$stmt->execute();
						$result = $stmt->fetchAll();
						foreach( $result as $row )
						{
							$currency =  $row['currency'];

							if($currency==$DefaultCurrency)
							{
								echo "<option value='".$currency."' selected>".$currency."</option>";
							}
							else
							{
								echo "<option value='".$currency."''>".$currency."</option>";
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 localization_status">
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<center><button class="btn btn-success btn_localization website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
			</div>
		</div>
	</div>
</div>

<script>
	/********    Localization Setup Save    ********/
	$('.btn_localization').click(function(){
		var DefaultCountry = $('.DefaultCountry').val();
		var DefaultTimeZone = $('.DefaultTimeZone').val();
		var DefaultCurrency = $('.DefaultCurrency').val();
		var task = "Update_Localization_Setup";

		if(DefaultCountry=='0')
		{
			$('.localization_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> Please select country</div></center>");
					return false;
		}

		if(DefaultTimeZone=='0')
		{
			$('.localization_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> Please select time zone.</div></center>");
					return false;
		}

		if(DefaultCurrency=='0')
		{
			$('.localization_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> Please select default currency.</div></center>");
					return false;
		}


		var data = 'DefaultCountry='+DefaultCountry+'&DefaultTimeZone='+DefaultTimeZone+'&DefaultCurrency='+DefaultCurrency+'&task='+task;

		$('.loading_img').show();

		$.ajax({
			type:'post',
        	data:data,
        	url:'query/general_setup_helper.php',
        	success:function(res)
        	{
        		$('.loading_img').hide();
        		if(res=='success')
        		{
        			$('.btn_localization').attr('disabled',true);
        			$('.localization_status').html("<center><div class='alert alert-success localization_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
        			$('.localization_success_status').fadeTo(1000, 500).slideUp(500, function(){
        				$('.btn_localization').attr('disabled',false);
						  window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Localization");
                    });
        		}
        		else
        		{
        			$('.localization_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
					return false;
        		}
        	}
        });
	});
</script>