
	<div class="container inner-tab">
		<br/>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<ul class="nav data-tabs nav-tabs" role="tablist">
				  	<li class="<?php if(($main_tab=='ProjectAttributes' || $main_tab==null) && ($sub_tab=='General_Setup' || $sub_tab==null)) { echo 'active';}?>"><a href="#General_Setup" role="tab" data-toggle="tab" class="tab-links">General</a></li>

				  	<li class="<?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='General_Security_Setup')) { echo 'active';}?>"><a href="#General_Security_Setup" role="tab" data-toggle="tab" class="tab-links">Security </a></li>

				  	<li class="<?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Localization')) { echo 'active';}?>"><a href="#Localization" role="tab" data-toggle="tab" class="tab-links">Localization</a></li>

				  	<li class="<?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Mail_Relay_Settings')) { echo 'active';}?>"><a href="#Mail_Relay_Settings" role="tab" data-toggle="tab" class="tab-links">Mail Relay</a></li>

				  	<li class="<?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Maintainance_Mode_Settings')) { echo 'active';}?>"><a href="#Maintainance_Mode_Settings" role="tab" data-toggle="tab" class="tab-links">Maintainance Mode</a></li>
				</ul>
				
				<div class="tab-content">
					<!-- General Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes' || $main_tab==null) && ($sub_tab=='General_Setup' || $sub_tab==null)) { echo 'active';}?>" id="General_Setup">
			  			<h2>General Setting</h2><br/>
			  			<?php include("modules/general-setting/general_setup_data.php");?>
			  		</div>
			  		<!-- General Setup End-->

			  		<!-- General Security Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='General_Security_Setup')) { echo 'active';}?>" id="General_Security_Setup">
			  			<h2>Security Setting</h2><br/>
			  			<?php include("modules/general-setting/general_security_setup_data.php");?>
			  		</div>
			  		<!-- General Security Setup End-->

			  		<!-- Localization Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Localization')) { echo 'active';}?>" id="Localization">
			  			<h2>Localization Setting</h2><br/>
			  			<?php include("modules/general-setting/localization_data.php");?>
			  		</div>
			  		<!-- Localization Setup End-->

			  		<!-- Mail_Relay_Settings Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Mail_Relay_Settings')) { echo 'active';}?>" id="Mail_Relay_Settings">
			  			<h2>Mail Relay Setting</h2><br/>
			  			<?php include("modules/general-setting/Mail_Relay_data.php");?>
			  		</div>
			  		<!-- Mail_Relay_Settings Setup End-->

			  		<!-- Maintainance_Mode_Settings Setup Start-->
			  		<div class="tab-pane <?php if(($main_tab=='ProjectAttributes') && ($sub_tab=='Maintainance_Mode_Settings')) { echo 'active';}?>" id="Maintainance_Mode_Settings">
			  			<h2>Maintainance Mode Setting</h2><br/>
			  			<?php include("modules/general-setting/Maintainance_Mode_data.php");?>
			  		</div>
			  		<!-- Maintainance_Mode_Settings Setup End-->
			  	</div>
			</div>
		</div>
	</div>
