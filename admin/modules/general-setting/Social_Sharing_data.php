<?php
	$stmt   = $link->prepare("SELECT * FROM `social_sharing`");
	$stmt->execute();
	$result = $stmt->fetch();
	$count=$stmt->rowCount();

	$social_sharing_display = $result['social_sharing_display'];
    $social_sharing_data = $result['social_sharing_data'];
    $website_facebook_display = $result['website_facebook_display'];
    $website_facebook_data = $result['website_facebook_data'];
    $website_twitter_display = $result['website_twitter_display'];
    $website_twitter_data = $result['website_twitter_data'];
    $website_instagram_display = $result['website_instagram_display'];
    $website_instagram_data = $result['website_instagram_data'];
    $website_tumbler_display = $result['website_tumbler_display'];
    $website_tumbler_data = $result['website_tumbler_data'];
    $website_youtube_display = $result['website_youtube_display'];
    $website_youtube_data = $result['website_youtube_data'];
    $website_pinterest_display = $result['website_pinterest_display'];
    $website_pinterest_data = $result['website_pinterest_data'];
    $website_myspace_display = $result['website_myspace_display'];
    $website_myspace_data = $result['website_myspace_data'];
    $website_linkedin_display = $result['website_linkedin_display'];
    $website_linkedin_data = $result['website_linkedin_data'];
    $website_VKontakte_display = $result['website_VKontakte_display'];
    $website_VKontakte_data = $result['website_VKontakte_data'];
    $website_foursquare_display = $result['website_foursquare_display'];
    $website_foursquare_data = $result['website_foursquare_data'];
    $website_flicker_display = $result['website_flicker_display'];
    $website_flicker_data = $result['website_flicker_data'];
    $website_vine_display = $result['website_vine_display'];
    $website_vine_data = $result['website_vine_data'];
    $website_blogger_display = $result['website_blogger_display'];
    $website_blogger_data = $result['website_blogger_data'];
    $website_quora_display = $result['website_quora_display'];
    $website_quora_data = $result['website_quora_data'];
    $website_reddit_display = $result['website_reddit_display'];
    $website_reddit_data = $result['website_reddit_data'];

    if($social_sharing_data!='' || $social_sharing_data!=null)
    {
        $social_sharing_script_file_name = '../'.$social_sharing_data;
        @$social_sharing_data = file_get_contents($social_sharing_script_file_name);
    }
?>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Social Sharing Display:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="social_sharing_display" <?php if($count>0 && ($social_sharing_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row social_sharing_data_div" <?php if($social_sharing_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Social Sharing Data:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<textarea class='form-control social_sharing_data' rows="5"><?php if($count>0) { echo $social_sharing_data;}?></textarea>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Please use any one following link to get social sharing script & paste.<br/>
					<ul>
						<li>
							<a href="https://www.addthis.com/" target="_blank">AddThis</a>
						</li>
						<li>
							<a href="https://www.sharethis.com/" target="_blank">Sharethis</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Facebook link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_facebook_display" <?php if($count>0 && ($website_facebook_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_facebook_data_div" <?php if($website_facebook_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Facebook link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_facebook_data' value="<?php if($count>0) { echo $website_facebook_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your facebook page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Twitter link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_twitter_display" <?php if($count>0 && ($website_twitter_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_twitter_data_div" <?php if($website_twitter_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Twitter link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_twitter_data' value="<?php if($count>0) { echo $website_twitter_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your twitter page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Instagram link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_instagram_display" <?php if($count>0 && ($website_instagram_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_instagram_data_div" <?php if($website_instagram_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Instagram link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_instagram_data' value="<?php if($count>0) { echo $website_instagram_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your instagram page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Tumbler link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_tumbler_display" <?php if($count>0 && ($website_tumbler_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_tumbler_data_div" <?php if($website_tumbler_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Tumbler link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_tumbler_data' value="<?php if($count>0) { echo $website_tumbler_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your tumbler page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Youtube link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_youtube_display" <?php if($count>0 && ($website_youtube_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_youtube_data_div" <?php if($website_youtube_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Youtube link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_youtube_data' value="<?php if($count>0) { echo $website_youtube_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your youtube channel page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Pinterest link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_pinterest_display" <?php if($count>0 && ($website_pinterest_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_pinterest_data_div" <?php if($website_pinterest_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Pinterest link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_pinterest_data' value="<?php if($count>0) { echo $website_pinterest_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your pinterest page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Myspace link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_myspace_display" <?php if($count>0 && ($website_myspace_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_myspace_data_div" <?php if($website_myspace_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Myspace link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_myspace_data' value="<?php if($count>0) { echo $website_myspace_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your myspace page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Linkedin link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_linkedin_display" <?php if($count>0 && ($website_linkedin_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_linkedin_data_div" <?php if($website_linkedin_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Linkedin link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_linkedin_data' value="<?php if($count>0) { echo $website_linkedin_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your linkedin page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable VKontakte link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_VKontakte_display" <?php if($count>0 && ($website_VKontakte_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_VKontakte_data_div" <?php if($website_VKontakte_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				VKontakte link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_VKontakte_data' value="<?php if($count>0) { echo $website_VKontakte_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your VKontakte page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Foursquare link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_foursquare_display" <?php if($count>0 && ($website_foursquare_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_foursquare_data_div" <?php if($website_foursquare_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Foursquare link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_foursquare_data' value="<?php if($count>0) { echo $website_foursquare_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your Foursquare page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Flicker link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_flicker_display" <?php if($count>0 && ($website_flicker_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_flicker_data_div" <?php if($website_flicker_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Flicker link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_flicker_data' value="<?php if($count>0) { echo $website_flicker_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your flicker page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Vine link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_vine_display" <?php if($count>0 && ($website_vine_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_vine_data_div" <?php if($website_vine_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Vine link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_vine_data' value="<?php if($count>0) { echo $website_vine_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your vine page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Blogger link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_blogger_display" <?php if($count>0 && ($website_blogger_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_blogger_data_div" <?php if($website_blogger_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Blogger link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_blogger_data' value="<?php if($count>0) { echo $website_blogger_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your Blogger page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Quora link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_quora_display" <?php if($count>0 && ($website_quora_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_quora_data_div" <?php if($website_quora_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Quora link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_quora_data' value="<?php if($count>0) { echo $website_quora_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your Quara page link.<br/>
				</div>
			</div>
		</div>

		<hr class="dotted">

		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Enable Reddit link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="checkbox" class="website_reddit_display" <?php if($count>0 && ($website_reddit_display=='1')) { echo 'checked';}?>>
			</div>
		</div>
		<div class="row input-row website_reddit_data_div" <?php if($website_reddit_display!='1') { echo 'style=display:none;';}?>>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Reddit link:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class='form-control website_reddit_data' value="<?php if($count>0) { echo $website_reddit_data;}?>">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
					<span class='fa fa-exclamation-circle'></span> 
					<strong>Note: </strong> Enter your Reddit page link.<br/>
				</div>
			</div>
		</div>


		<hr class="dotted">
	</div>
	
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 social_sharing_status">
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<center><button class="btn btn-success btn_social_sharing website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('.social_sharing_display').change(function(){    //social sharing
			if($(this).is(':checked'))
            {
                var social_sharing_display = '1';
            }
            else
            {
                var social_sharing_display = '0';
            }

            if(social_sharing_display=='1')
            {
            	$('.social_sharing_data_div').show();
            	return false;
            }
            else
            {
            	$('.social_sharing_data_div').hide();
            	return false;
            }
		});

		$('.website_facebook_display').change(function(){    //facebook page sharing
			if($(this).is(':checked'))
            {
                var website_facebook_display = '1';
            }
            else
            {
                var website_facebook_display = '0';
            }

            if(website_facebook_display=='1')
            {
            	$('.website_facebook_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_facebook_data_div').hide();
            	return false;
            }
		});

		$('.website_twitter_display').change(function(){    //twitter page sharing
			if($(this).is(':checked'))
            {
                var website_twitter_display = '1';
            }
            else
            {
                var website_twitter_display = '0';
            }

            if(website_twitter_display=='1')
            {
            	$('.website_twitter_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_twitter_data_div').hide();
            	return false;
            }
		});

		$('.website_instagram_display').change(function(){    //instagram page sharing
			if($(this).is(':checked'))
            {
                var website_instagram_display = '1';
            }
            else
            {
                var website_instagram_display = '0';
            }

            if(website_instagram_display=='1')
            {
            	$('.website_instagram_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_instagram_data_div').hide();
            	return false;
            }
		});

		$('.website_tumbler_display').change(function(){    //tumbler page sharing
			if($(this).is(':checked'))
            {
                var website_tumbler_display = '1';
            }
            else
            {
                var website_tumbler_display = '0';
            }

            if(website_tumbler_display=='1')
            {
            	$('.website_tumbler_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_tumbler_data_div').hide();
            	return false;
            }
		});

		$('.website_youtube_display').change(function(){    //youtube page sharing
			if($(this).is(':checked'))
            {
                var website_youtube_display = '1';
            }
            else
            {
                var website_youtube_display = '0';
            }

            if(website_youtube_display=='1')
            {
            	$('.website_youtube_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_youtube_data_div').hide();
            	return false;
            }
		});

		$('.website_pinterest_display').change(function(){    //pinterest page sharing
			if($(this).is(':checked'))
            {
                var website_pinterest_display = '1';
            }
            else
            {
                var website_pinterest_display = '0';
            }

            if(website_pinterest_display=='1')
            {
            	$('.website_pinterest_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_pinterest_data_div').hide();
            	return false;
            }
		});

		$('.website_myspace_display').change(function(){    //myspace page sharing
			if($(this).is(':checked'))
            {
                var website_myspace_display = '1';
            }
            else
            {
                var website_myspace_display = '0';
            }

            if(website_myspace_display=='1')
            {
            	$('.website_myspace_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_myspace_data_div').hide();
            	return false;
            }
		});

		$('.website_linkedin_display').change(function(){    //Linkedin page sharing
			if($(this).is(':checked'))
            {
                var website_linkedin_display = '1';
            }
            else
            {
                var website_linkedin_display = '0';
            }

            if(website_linkedin_display=='1')
            {
            	$('.website_linkedin_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_linkedin_data_div').hide();
            	return false;
            }
		});

		$('.website_VKontakte_display').change(function(){    //Vkontakte page sharing
			if($(this).is(':checked'))
            {
                var website_VKontakte_display = '1';
            }
            else
            {
                var website_VKontakte_display = '0';
            }

            if(website_VKontakte_display=='1')
            {
            	$('.website_VKontakte_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_VKontakte_data_div').hide();
            	return false;
            }
		});

		$('.website_foursquare_display').change(function(){    //foursquare page sharing
			if($(this).is(':checked'))
            {
                var website_foursquare_display = '1';
            }
            else
            {
                var website_foursquare_display = '0';
            }

            if(website_foursquare_display=='1')
            {
            	$('.website_foursquare_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_foursquare_data_div').hide();
            	return false;
            }
		});

		$('.website_flicker_display').change(function(){    //flicker page sharing
			if($(this).is(':checked'))
            {
                var website_flicker_display = '1';
            }
            else
            {
                var website_flicker_display = '0';
            }

            if(website_flicker_display=='1')
            {
            	$('.website_flicker_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_flicker_data_div').hide();
            	return false;
            }
		});

		$('.website_vine_display').change(function(){    //vine page sharing
			if($(this).is(':checked'))
            {
                var website_vine_display = '1';
            }
            else
            {
                var website_vine_display = '0';
            }

            if(website_vine_display=='1')
            {
            	$('.website_vine_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_vine_data_div').hide();
            	return false;
            }
		});

		$('.website_blogger_display').change(function(){    //blogger page sharing
			if($(this).is(':checked'))
            {
                var website_blogger_display = '1';
            }
            else
            {
                var website_blogger_display = '0';
            }

            if(website_blogger_display=='1')
            {
            	$('.website_blogger_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_blogger_data_div').hide();
            	return false;
            }
		});

		$('.website_quora_display').change(function(){    //quora page sharing
			if($(this).is(':checked'))
            {
                var website_quora_display = '1';
            }
            else
            {
                var website_quora_display = '0';
            }

            if(website_quora_display=='1')
            {
            	$('.website_quora_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_quora_data_div').hide();
            	return false;
            }
		});

		$('.website_reddit_display').change(function(){    //reddit page sharing
			if($(this).is(':checked'))
            {
                var website_reddit_display = '1';
            }
            else
            {
                var website_reddit_display = '0';
            }

            if(website_reddit_display=='1')
            {
            	$('.website_reddit_data_div').show();
            	return false;
            }
            else
            {
            	$('.website_reddit_data_div').hide();
            	return false;
            }
		});

		$('.btn_social_sharing').click(function(){
			if($('.social_sharing_display').is(':checked'))   // social sharing
            {
                var social_sharing_display = '1';
            }
            else
            {
                var social_sharing_display = '0';
            }

            if($('.website_facebook_display').is(':checked'))   // facebook sharing
            {
                var website_facebook_display = '1';
            }
            else
            {
                var website_facebook_display = '0';
            }

            if($('.website_twitter_display').is(':checked'))   // twitter sharing
            {
                var website_twitter_display = '1';
            }
            else
            {
                var website_twitter_display = '0';
            }

            if($('.website_instagram_display').is(':checked'))   // instagram sharing
            {
                var website_instagram_display = '1';
            }
            else
            {
                var website_instagram_display = '0';
            }

            if($('.website_tumbler_display').is(':checked'))   // tumbler sharing
            {
                var website_tumbler_display = '1';
            }
            else
            {
                var website_tumbler_display = '0';
            }

            if($('.website_youtube_display').is(':checked'))   // youtube sharing
            {
                var website_youtube_display = '1';
            }
            else
            {
                var website_youtube_display = '0';
            }

            if($('.website_pinterest_display').is(':checked'))   // pinterest sharing
            {
                var website_pinterest_display = '1';
            }
            else
            {
                var website_pinterest_display = '0';
            }

            if($('.website_myspace_display').is(':checked'))   // myspace sharing
            {
                var website_myspace_display = '1';
            }
            else
            {
                var website_myspace_display = '0';
            }

            if($('.website_linkedin_display').is(':checked'))   // Linkedin sharing
            {
                var website_linkedin_display = '1';
            }
            else
            {
                var website_linkedin_display = '0';
            }

            if($('.website_VKontakte_display').is(':checked'))   // VKontakte sharing
            {
                var website_VKontakte_display = '1';
            }
            else
            {
                var website_VKontakte_display = '0';
            }

            if($('.website_foursquare_display').is(':checked'))   // foursquare sharing
            {
                var website_foursquare_display = '1';
            }
            else
            {
                var website_foursquare_display = '0';
            }

            if($('.website_flicker_display').is(':checked'))   // flicker sharing
            {
                var website_flicker_display = '1';
            }
            else
            {
                var website_flicker_display = '0';
            }

            if($('.website_vine_display').is(':checked'))   // vine sharing
            {
                var website_vine_display = '1';
            }
            else
            {
                var website_vine_display = '0';
            }

            if($('.website_blogger_display').is(':checked'))   // blogger sharing
            {
                var website_blogger_display = '1';
            }
            else
            {
                var website_blogger_display = '0';
            }

            if($('.website_quora_display').is(':checked'))   // quora sharing
            {
                var website_quora_display = '1';
            }
            else
            {
                var website_quora_display = '0';
            }

            if($('.website_reddit_display').is(':checked'))   // reddit sharing
            {
                var website_reddit_display = '1';
            }
            else
            {
                var website_reddit_display = '0';
            }

            var social_sharing_data = $('.social_sharing_data').val();
            var website_facebook_data = $('.website_facebook_data').val();
            var website_twitter_data = $('.website_twitter_data').val();
            var website_instagram_data = $('.website_instagram_data').val();
            var website_tumbler_data = $('.website_tumbler_data').val();
            var website_youtube_data = $('.website_youtube_data').val();
            var website_pinterest_data = $('.website_pinterest_data').val();
            var website_myspace_data = $('.website_myspace_data').val();
            var website_linkedin_data = $('.website_linkedin_data').val();
            var website_VKontakte_data = $('.website_VKontakte_data').val();
            var website_foursquare_data = $('.website_foursquare_data').val();
            var website_flicker_data = $('.website_flicker_data').val();
            var website_vine_data = $('.website_vine_data').val();
            var website_blogger_data = $('.website_blogger_data').val();
            var website_quora_data = $('.website_quora_data').val();
            var website_reddit_data = $('.website_reddit_data').val();

            var task = "Add_Social_sharing";

            if(social_sharing_display=='1' && (social_sharing_data=='' || social_sharing_data==null))   // social sharing data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter social sharing script.</div></center>");
            	return false;
            }

            if(website_facebook_display=='1' && (website_facebook_data=='' || website_facebook_data==null))   // facebook data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter facebook page link.</div></center>");
            	return false;
            }

            if(website_twitter_display=='1' && (website_twitter_data=='' || website_twitter_data==null))   // twitter data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter twitter page link.</div></center>");
            	return false;
            }

            if(website_instagram_display=='1' && (website_instagram_data=='' || website_instagram_data==null))   // instagram data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter instagram page link.</div></center>");
            	return false;
            }

            if(website_tumbler_display=='1' && (website_tumbler_data=='' || website_tumbler_data==null))   // tumbler data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter tumbler page link.</div></center>");
            	return false;
            }

            if(website_youtube_display=='1' && (website_youtube_data=='' || website_youtube_data==null))   // youtube data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter youtube page link.</div></center>");
            	return false;
            }

            if(website_pinterest_display=='1' && (website_pinterest_data=='' || website_pinterest_data==null))   // pinterest data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter pinterest page link.</div></center>");
            	return false;
            }

            if(website_myspace_display=='1' && (website_myspace_data=='' || website_myspace_data==null))   // myspace data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter myspace page link.</div></center>");
            	return false;
            }

            if(website_linkedin_display=='1' && (website_linkedin_data=='' || website_linkedin_data==null))   // Linkedin data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter likedin page link.</div></center>");
            	return false;
            }

            if(website_VKontakte_display=='1' && (website_VKontakte_data=='' || website_VKontakte_data==null))   // VKontakte data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please enter VKontakte page link.</div></center>");
            	return false;
            }

            if(website_foursquare_display=='1' && (website_foursquare_data=='' || website_foursquare_data==null))   // Foursquare data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please enter foursquare page link.</div></center>");
            	return false;
            }

            if(website_flicker_display=='1' && (website_flicker_data=='' || website_flicker_data==null))   // Flicker data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please enter flicker page link.</div></center>");
            	return false;
            }

            if(website_vine_display=='1' && (website_vine_data=='' || website_vine_data==null))   // Vine data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please enter vine page link.</div></center>");
            	return false;
            }

            if(website_blogger_display=='1' && (website_blogger_data=='' || website_blogger_data==null))   // blogger data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please enter blogger page link.</div></center>");
            	return false;
            }

            if(website_quora_display=='1' && (website_quora_data=='' || website_quora_data==null))   // quora data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please enter quora page link.</div></center>");
            	return false;
            }

            if(website_reddit_display=='1' && (website_reddit_data=='' || website_reddit_data==null))   // reddit data validation
            {
            	$('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please enter reddit page link.</div></center>");
            	return false;
            }

            $('.loading_img').show();

            $('.social_sharing_status').html("");

            var data = 'social_sharing_display='+social_sharing_display+'&social_sharing_data='+social_sharing_data+'&website_facebook_display='+website_facebook_display+'&website_facebook_data='+website_facebook_data+'&website_twitter_display='+website_twitter_display+'&website_twitter_data='+website_twitter_data+'&website_instagram_display='+website_instagram_display+'&website_instagram_data='+website_instagram_data+'&website_tumbler_display='+website_tumbler_display+'&website_tumbler_data='+website_tumbler_data+'&website_youtube_display='+website_youtube_display+'&website_youtube_data='+website_youtube_data+'&website_pinterest_display='+website_pinterest_display+'&website_pinterest_data='+website_pinterest_data+'&website_myspace_display='+website_myspace_display+'&website_myspace_data='+website_myspace_data+'&website_linkedin_display='+website_linkedin_display+'&website_linkedin_data='+website_linkedin_data+'&website_VKontakte_display='+website_VKontakte_display+'&website_VKontakte_data='+website_VKontakte_data+'&website_foursquare_display='+website_foursquare_display+'&website_foursquare_data='+website_foursquare_data+'&website_flicker_display='+website_flicker_display+'&website_flicker_data='+website_flicker_data+'&website_vine_display='+website_vine_display+'&website_vine_data='+website_vine_data+'&website_blogger_display='+website_blogger_display+'&website_blogger_data='+website_blogger_data+'&website_quora_display='+website_quora_display+'&website_quora_data='+website_quora_data+'&website_reddit_display='+website_reddit_display+'&website_reddit_data='+website_reddit_data+'&task='+task;

            $.ajax({
                type:'post',
                data:data,
                url:'query/Social_sharing_data_helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.social_sharing_status').html("<center><div class='alert alert-success custom_script_status_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
                        $('.custom_script_status_success_status').fadeTo(1000, 500).slideUp(500, function(){
                        	window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Social_Sharing_Settings");
                        });
                    }
                    else
                    {
                        $('.social_sharing_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
		});
	});
</script>