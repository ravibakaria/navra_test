<?php
    $websiteBasePath = getWebsiteBasePath();

	$stmt   = $link->prepare("SELECT * FROM `custom_meta_tags`");
	$stmt->execute();
	$result = $stmt->fetchAll();
	$count=$stmt->rowCount();

?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                <button class="btn btn-primary btn-sm website-button add-meta-tag-button" data-toggle="modal" data-target="#Add-New-Meta-Tag">Add New Meta Tag</button>
            </div>
        </div>
    	<div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered meta_tag_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tag</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
					    		if($count>0)
					    		{
					    			foreach ($result as $row) 
					    			{
					    				$id = $row['id'];
					    				
					    				$meta_tag = str_replace("--lt","&lt",$row['meta_tag']);

					    				echo "<tr>";
					    					echo "<td><a href='update-custom-meta-data.php?id=$id' style='color:black;text-decoration:none;'>".$id."</a></td>";
					    					echo "<td><a href='update-custom-meta-data.php?id=$id' style='color:black;text-decoration:none;'>".$meta_tag."</td></a>";
					    					echo "<td>
					    						<a href='update-custom-meta-data.php?id=$id'><i class='fa fa-pencil fa-2x edit-fa'></i></a> &nbsp;&nbsp; <a><i class='fa fa-times fa-2x delete_meta' id='$id' style='color:red;'></i></a>
					    						<center><img src='../images/loader/loader.gif' class='img-responsive loading_img' id='loading_img_$id' style='width:20px; height:20px; display:none;'></center>
					    						<div class='delete_status_$id'></div>
					    						</td>";
					    				echo "</tr>";
					    			}
					    			
					    		}
					    		else
					    		{
					    			echo "<tr>";
					    				echo "<td colspan='4'>No records found!</td>";
					    			echo "</tr>";
					    		}
					    	?>
                        </tbody>
                    </table>
                <center>
            </div>

            <!-- Modal Add new custom meta tag -->
            <div class="modal fade" id="Add-New-Meta-Tag" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Custom Meta Tag</h4></center>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Meta tag :</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                	<textarea class="form-control meta_tag" rows='4'></textarea>
                                </div>
                            </div>

                            <div class="row input-row">
                            	<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status_meta_tag">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn_add_new_meta_tag website-button">Add</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
    	//$('.meta_tag_list').DataTable();

    	/**************     Add Meta Tag     ****************/
    	$('.btn_add_new_meta_tag').click(function(){
    		var meta_tag = $('.meta_tag').val();
    		var meta_tag = meta_tag.replace("<", "--lt");
    		var meta_tag = meta_tag.replace(/'/g, '"');
    		var task = "add_custom_meta_tag";

    		if(meta_tag=='' || meta_tag==null)
    		{
    			$('.add_status_meta_tag').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> Please enter meta tag.</div>");
                        return false;
    		}

    		var data = 'meta_tag='+meta_tag+'&task='+task;
            $('.add_status_meta_tag').html("");

            $('.loading_img').show();

            $.ajax({
                type:'post',
                data:data,
                url:'query/general_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status_meta_tag').html("<center><div class='alert alert-success add_status_meta_tag_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Meta tag Added Successfully.</div></center>");
                            $('.add_status_meta_tag_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Custome_Meta_Tags_Settings");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status_meta_tag').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
    	});


    	/**************     Delete Meta tag     *************/
    	$('.delete_meta').click(function(){
    		var id = $(this).attr('id');
    		var task = "delete_custom_meta_tag";

    		var r = confirm("Are you sure want to delete this meta tag");

    		if(r==true)
    		{
    			$('loading_img_'+id).show();
    			var data = 'id='+id+'&task='+task;

    			$.ajax({
	                type:'post',
	                data:data,
	                url:'query/general_setup_helper.php',
	                success:function(res)
	                {
	                    $('loading_img_'+id).show();
	                    if(res=='success')
	                    {
	                        $('.delete_status_'+id).html("<center><div class='alert alert-success delete_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Meta tag deleted Successfully.</div></center>");
	                            $('.delete_status_success').fadeTo(1000, 500).slideUp(500, function(){
	                                  window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Custome_Meta_Tags_Settings");
	                                  return false;
	                            });
	                    }
	                    else
	                    {
	                        $('.delete_status_'+id).html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	                        return false;
	                    }
	                }
	            });
    		}
    		else
    		if(r==false)
    		{
    			return false;
    		}

    		return false;
    	});
    });
</script>