<?php
    $websiteBasePath = getWebsiteBasePath();

    $stmt   = $link->prepare("SELECT * FROM `sitemap_generation`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    $sitemap_generation_allowed = $result['sitemap_generation_allowed'];
    $SitemapGenerationRunAt = $result['SitemapGenerationRunAt'];

    //Sitemap Generation hour & minute
    $SitemapGenerationHourMinute = explode(':',$SitemapGenerationRunAt);
    $SitemapGenerationReminderHour = @$SitemapGenerationHourMinute[0];
    $SitemapGenerationReminderMinute = @$SitemapGenerationHourMinute[1];
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Allow Sitemap Generation:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
                <input type="checkbox" class="sitemap_generation_allowed" <?php if($count>0 && ($sitemap_generation_allowed=='1')) { echo 'checked';}?>>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong>If you enable this property then <b><i>"sitemap.xml"</i></b> will be created in root folder of project otherwise it will be deleted from root folder of project.<br>
                </div>
            </div>
        </div>
        <div class="row input-row SitemapGenerationData" <?php if($sitemap_generation_allowed!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Sitemap Generation Run At:
            </div>
            <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2">
                Hours: <input class="SitemapGenerationReminderHour" type="number" min="0" max="23" placeholder="23" value="<?php echo @$SitemapGenerationReminderHour;?>">
            </div>
            <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2">
                Minutes: <input class="SitemapGenerationReminderMinute" type="number" min="0" max="59" placeholder="00" value="<?php echo @$SitemapGenerationReminderMinute;?>">
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sitemap_generation_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><button class="btn btn-success btn_sitemap_generate website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.sitemap_generation_allowed').change(function(){

            if($(this).is(':checked'))
            {
                var sitemap_generation_allowed = '1';
            }
            else
            {
                var sitemap_generation_allowed = '0';
            }

            if(sitemap_generation_allowed=='1')
            {
                $('.SitemapGenerationData').show();
                return false;
            }
            else
            {
                $('.SitemapGenerationData').hide();
                return false;
            }
        });
        
        $('.btn_sitemap_generate').click(function(){
            if($('.sitemap_generation_allowed').is(':checked'))
            {
                var sitemap_generation_allowed = '1';
            }
            else
            {
                var sitemap_generation_allowed = '0';
            }

            var SitemapGenerationReminderHour = $('.SitemapGenerationReminderHour').val();
            var SitemapGenerationReminderMinute = $('.SitemapGenerationReminderMinute').val();

            var SitemapGenerationRunAt = SitemapGenerationReminderHour+':'+SitemapGenerationReminderMinute+':00';
            var task = "update-sitemap-generation";

            if(sitemap_generation_allowed=='1' && ((SitemapGenerationReminderHour=='00' || SitemapGenerationReminderHour=='' || SitemapGenerationReminderHour==null) && (SitemapGenerationReminderMinute=='00' || SitemapGenerationReminderMinute=='' || SitemapGenerationReminderMinute==null)))
            {
                $('.sitemap_generation_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter time to run sitemap generation at time.</div></center>");
                return false
            }


            var data = {
                sitemap_generation_allowed : sitemap_generation_allowed,
                SitemapGenerationRunAt : SitemapGenerationRunAt,
                task : task
            }

            $.ajax({
                type : 'post',
                dataType : 'json',
                data : data,
                url : 'query/general_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.sitemap_generation_status').html("<center><div class='alert alert-success sitemap_generation_status_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
                        $('.sitemap_generation_status_status').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Sitemap_generation_Settings");
                        });
                    }
                    else
                    {
                        $('.sitemap_generation_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>