<?php
    $websiteBasePath = getWebsiteBasePath();

    $stmt   = $link->prepare("SELECT * FROM `custom_script`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    $header_script_display = $result['header_script_display'];
    $header_script = $result['header_script'];
    $footer_script_display = $result['footer_script_display'];
    $footer_script = $result['footer_script'];

    if($header_script!='' || $header_script!=null)
    {
        $header_script_file_name = '../'.$header_script;
        $header_script = file_get_contents($header_script_file_name);
    }
    
    if($footer_script!='' || $footer_script!=null)
    {
        $footer_script_file_name = '../'.$footer_script;
        $footer_script = file_get_contents($footer_script_file_name);
    }
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Header Script Display:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input type="checkbox" class="header_script_display" <?php if($count>0 && ($header_script_display=='1')) { echo 'checked';}?>>
            </div>
        </div>
        <div class="row input-row header_script_data" <?php if($header_script_display!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Header Script:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <textarea class='form-control header_script' rows="5"><?php if($count>0) { echo $header_script;}?></textarea>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This header script will be included in head tag.<i>Ex. Google analytics script</i>       
                    <br/>
                    <ul>
                        <li>You can add multiple scripts one after another</li>
                        <li>For Ex. &lt;script&gt; /* script code-1 */ &lt;/script&gt;<br/>
                        &lt;script&gt; /* script code-2 */ &lt;/script&gt;</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Footer Script Display:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input type="checkbox" class="footer_script_display" <?php if($count>0 && ($footer_script_display=='1')) { echo 'checked';}?>>
            </div>
        </div>
        <div class="row input-row footer_script_data"  <?php if($footer_script_display!='1') { echo 'style=display:none;';}?>>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Footer Script:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <textarea class='form-control footer_script' rows="5"><?php if($count>0) { echo $footer_script;}?></textarea>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> This footer script will be included after end of  body tag.<br/>
                    <ul>
                        <li>You can add multiple scripts one after another</li>
                        <li>For Ex. &lt;script&gt; /* script code-1 */ &lt;/script&gt;<br/>
                        &lt;script&gt; /* script code-2 */ &lt;/script&gt;</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 custom_script_status">
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><button class="btn btn-success btn_custom_script website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.header_script_display').change(function(){

            if($(this).is(':checked'))
            {
                var header_script_display = '1';
            }
            else
            {
                var header_script_display = '0';
            }

            if(header_script_display=='1')
            {
                $('.header_script_data').show();
                return false;
            }
            else
            {
                $('.header_script_data').hide();
                return false;
            }
        });

        $('.footer_script_display').change(function(){

            if($(this).is(':checked'))
            {
                var footer_script_display = '1';
            }
            else
            {
                var footer_script_display = '0';
            }

            if(footer_script_display=='1')
            {
                $('.footer_script_data').show();
                return false;
            }
            else
            {
                $('.footer_script_data').hide();
                return false;
            }
        });

        $('.btn_custom_script').click(function(){
            if($('.header_script_display').is(':checked'))
            {
                var header_script_display = '1';
            }
            else
            {
                var header_script_display = '0';
            }

            if($('.footer_script_display').is(':checked'))
            {
                var footer_script_display = '1';
            }
            else
            {
                var footer_script_display = '0';
            }

            var header_script = $('.header_script').val();
            var footer_script = $('.footer_script').val();
            var task = "Update_Custom_Script_Data";

            if(header_script_display=='1' && (header_script=='' || header_script=='undefined' || header_script==null))
            {
                $('.custom_script_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter header script text.</div></center>");
                return false
            }

            if(footer_script_display=='1' && (footer_script=='' || footer_script=='undefined' || footer_script==null))
            {
                $('.custom_script_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter footer script text.</div></center>");
                return false
            }

            $('.loading_img').show();
            
            var data = {
                header_script_display : header_script_display,
                header_script : header_script,
                footer_script_display : footer_script_display,
                footer_script : footer_script,
                task:task
            }
            
            
            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url:'query/general_setup_helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.custom_script_status').html("<center><div class='alert alert-success custom_script_status_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
                        $('.custom_script_status_success_status').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Custom_Scripts_Settings");
                        });
                    }
                    else
                    {
                        $('.custom_script_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>