<?php
	$stmt   = $link->prepare("SELECT * FROM `mailrelaysetup`");
	$stmt->execute();
	$result = $stmt->fetch();
	$count=$stmt->rowCount();

	$FromName = $result['FromName'];
    $FromEmail = $result['FromEmail'];
    $BCCEmail = $result['BCCEmail'];
    $SMTPHost = $result['SMTPHost'];
    $SMTPPort = $result['SMTPPort'];
    $SMTPUsername = $result['SMTPUsername'];
    $SMTPPassword = $result['SMTPPassword'];
    $SMTPSSL = $result['SMTPSSL'];
    $GlobalEmailSignature = $result['GlobalEmailSignature'];
    $emailFooterText = $result['emailFooterText'];

    if($SMTPHost=='' || $SMTPHost==null)
    {
    	$SMTPHost = getWebsiteBasePath();
    }

    if($SMTPPort=='' || $SMTPPort==null)
    {
    	$SMTPPort = '25';
    }
?>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				From Name:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class="form-control FromName" value="<?php echo $FromName;?>">
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 status_FromName">
				
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				From Email:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class="form-control FromEmail" value="<?php echo $FromEmail;?>">
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 status_FromEmail">
				
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				BCC Email:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class="form-control BCCEmail" value="<?php echo $BCCEmail;?>">
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				SMTP Host/SMTP IP Address:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class="form-control SMTPHost" value="<?php echo $SMTPHost;?>" placeholder="<?php echo getWebsiteBasePath();?>">
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 status_SMTPHost">
				
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    <strong>Note: </strong> If test mail failed with SMTP Host then please check with SMTP host IP Address.<br/>
                </div>
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				SMTP Port:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class="form-control SMTPPort" value="<?php echo $SMTPPort;?>" placeholder="25">
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 status_SMTPPort">
				
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				SMTP Username:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="text" class="form-control SMTPUsername" value="<?php echo $SMTPUsername;?>">
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 status_SMTPUsername">
				
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				SMTP Password:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<input type="password" class="form-control SMTPPassword" value="<?php echo $SMTPPassword;?>">
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 status_SMTPPassword">
				
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				SMTP SSL:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<select class="form-control SMTPSSL">
					<option value='none' <?php if($SMTPSSL=='none') echo 'selected';?>>None</option>
					<option value='ssl' <?php if($SMTPSSL=='ssl') echo 'selected';?>>SSL</option>
					<option value='tls' <?php if($SMTPSSL=='tls') echo 'selected';?>>TLS</option>
				</select>
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<center><button class="btn btn-info btn_validate_mail_setting website-button">Validate Mail Setting</button></center>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 status_mail_setting">
				<center><img src="../images/loader/loader.gif" class='img-responsive loading_img1' id='loading_img1' style='width:45px; height:45px;display:none; '/></center>
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Global Email Signature:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
				<textarea rows="5" class="form-control GlobalEmailSignature"><?php echo $GlobalEmailSignature;?></textarea>
			</div>
		</div>
		<div class="row input-row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				Email Footer text:
			</div>
			<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
				<textarea rows="5" class="form-control emailFooterText"><?php echo $emailFooterText;?></textarea>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mail_relay_status">
				<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<center><button class="btn btn-success btn_mail_relay website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){

		$('.GlobalEmailSignature').richText();

		$('.emailFooterText').richText();

		/* empty error message  */
		$('.FromName, .FromEmail, .BCCEmail, .SMTPHost, .SMTPPort, .SMTPUsername, .SMTPPassword, .SMTPSSL').keypress(function(){
            $('.status_FromName, .status_FromEmail, .status_SMTPHost, .status_SMTPPort, .status_SMTPUsername, .status_SMTPPassword, .status_SMTPSSL').html("");
        });

        /* empty error message  */
		$('.FromName, .FromEmail, .BCCEmail, .SMTPHost, .SMTPPort, .SMTPUsername, .SMTPPassword, .SMTPSSL').click(function(){
            $('.status_FromName, .status_FromEmail, .status_SMTPHost, .status_SMTPPort, .status_SMTPUsername, .status_SMTPPassword, .status_SMTPSSL').html("");
        });

		/********    Test Mail Setting start   *******/
			$('.btn_validate_mail_setting').click(function(){
				var FromName = $('.FromName').val();
				var FromEmail = $('.FromEmail').val();
				var BCCEmail = $('.BCCEmail').val();
				var SMTPHost = $('.SMTPHost').val();
				var SMTPPort = $('.SMTPPort').val();
				var SMTPUsername = $('.SMTPUsername').val();
				var SMTPPassword = $('.SMTPPassword').val();
				var SMTPSSL = $('.SMTPSSL').val();
				var status_mail_setting;
				var task = "Validate_mail_Relay_Setting";

				if(FromName=='' || FromName==null)
				{
					$('.status_FromName').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter From Name.</div></center>");
                    status_mail_setting='0';
				}

				if(FromEmail=='' || FromEmail==null)
				{
					$('.status_FromEmail').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter From Email.</div></center>");
                    status_mail_setting='0';
				}

				if(SMTPHost=='' || SMTPHost==null)
				{
					$('.status_SMTPHost').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter SMTP Host.</div></center>");
                    status_mail_setting='0';
				}

				if(SMTPPort=='' || SMTPPort==null)
				{
					$('.status_SMTPPort').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter SMTP Port.</div></center>");
                    status_mail_setting='0';
				}

				if(SMTPUsername=='' || SMTPUsername==null)
				{
					$('.status_SMTPUsername').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter SMTP Username.</div></center>");
                    status_mail_setting='0';
				}

				if(SMTPPassword=='' || SMTPPassword==null)
				{
					$('.status_SMTPPassword').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter SMTP Password.</div></center>");
                    status_mail_setting='0';
				}

				if(SMTPSSL=='' || SMTPSSL==null)
				{
					$('.status_SMTPSSL').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter SMTP SSL.</div></center>");
                    status_mail_setting='0';
				}

				if(status_mail_setting=='0')
				{
					return false;
				}

				$('.loading_img1').show();

				$.ajax({
					type:'post',
	            	data:'FromName='+FromName+'&FromEmail='+FromEmail+'&BCCEmail='+BCCEmail+'&SMTPHost='+SMTPHost+'&SMTPPort='+SMTPPort+'&SMTPUsername='+SMTPUsername+'&SMTPPassword='+SMTPPassword+'&SMTPSSL='+SMTPSSL+'&task='+task,
	            	url:'query/general_setup_helper.php',
	            	success:function(res)
	            	{
	            		$('.loading_img1').hide();
		        		if(res=='success')
		        		{
		        			$('.btn_validate_mail_setting').attr('disabled',true);
		        			$('.status_mail_setting').html("<center><div class='alert alert-success mail_relay_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Test Mail sent successfully.</div></center>");
		        		}
		        		else
		        		{
		        			$('.status_mail_setting').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
							return false;
		        		}
	            	}
				});
			});
		/********    Test Mail Setting end   *******/


		/********    Mail Relay Setup Save start    ********/
			$('.btn_mail_relay').click(function(){
				var FromName = $('.FromName').val();
				var FromEmail = $('.FromEmail').val();
				var BCCEmail = $('.BCCEmail').val();
				var SMTPHost = $('.SMTPHost').val();
				var SMTPPort = $('.SMTPPort').val();
				var SMTPUsername = $('.SMTPUsername').val();
				var SMTPPassword = $('.SMTPPassword').val();
				var SMTPSSL = $('.SMTPSSL').val();
				var GlobalEmailSignature = $('.GlobalEmailSignature').val();
				var emailFooterText = $('.emailFooterText').val();
				var status_mail_setting;
				var task = "Update_Mail_Relay_Setup";

				if(FromName=='' || FromName==null)
				{
					$('.status_FromName').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter From Name.</div></center>");
                    status_mail_setting='0';
				}

				if(FromEmail=='' || FromEmail==null)
				{
					$('.status_FromEmail').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter From Email.</div></center>");
                    status_mail_setting='0';
				}

				if(SMTPHost=='' || SMTPHost==null)
				{
					$('.status_SMTPHost').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter SMTP Host.</div></center>");
                    status_mail_setting='0';
				}

				if(SMTPPort=='' || SMTPPort==null)
				{
					$('.status_SMTPPort').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter SMTP Port.</div></center>");
                    status_mail_setting='0';
				}

				if(SMTPUsername=='' || SMTPUsername==null)
				{
					$('.status_SMTPUsername').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter SMTP Username.</div></center>");
                    status_mail_setting='0';
				}

				if(SMTPPassword=='' || SMTPPassword==null)
				{
					$('.status_SMTPPassword').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter SMTP Password.</div></center>");
                    status_mail_setting='0';
				}

				if(SMTPSSL=='' || SMTPSSL==null)
				{
					$('.status_SMTPSSL').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span> Please Enter SMTP SSL.</div></center>");
                    status_mail_setting='0';
				}

				if(status_mail_setting=='0')
				{
					return false;
				}
				
				var data = {
	                FromName : FromName,
	                FromEmail : FromEmail,
	                BCCEmail : BCCEmail,
	                SMTPHost : SMTPHost,
	                SMTPPort : SMTPPort,
	                SMTPUsername : SMTPUsername,
	                SMTPPassword : SMTPPassword,
	                SMTPSSL : SMTPSSL,
	                GlobalEmailSignature : GlobalEmailSignature,
	                emailFooterText : emailFooterText,
	                task:task
	            }

				$('.loading_img').show();

				$.ajax({
					type:'post',
	                dataType: 'json',
	                data:data,                
	                url:'query/general_setup_helper.php',
		        	success:function(res)
		        	{
		        		$('.loading_img').hide();
		        		if(res=='success')
		        		{
		        			$('.btn_mail_relay').attr('disabled',true);
		        			$('.mail_relay_status').html("<center><div class='alert alert-success mail_relay_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data Updated Successfull.</div></center>");
		        			$('.mail_relay_success_status').fadeTo(1000, 500).slideUp(500, function(){
		        				$('.btn_mail_relay').attr('disabled',false);
								  	window.location.assign("project-configuration.php?main_tab=ProjectAttributes&sub_tab=Mail_Relay_Settings");
		                    });
		        		}
		        		else
		        		{
		        			$('.mail_relay_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
							return false;
		        		}
		        	}
		        });
			});
		/********    Mail Relay Setup Save end    ********/
	});
</script>