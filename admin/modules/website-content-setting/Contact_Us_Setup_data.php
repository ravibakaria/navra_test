<?php
    $stmt   = $link->prepare("SELECT * FROM `contact_us`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    if($count>0)
    {
    	$enquiry_email = $result['enquiry_email'];
        $companyName = $result['companyName'];
        $street = $result['street'];
        $city = $result['city'];
        $state = $result['state'];
        $country = $result['country'];
        $postalCode = $result['postalCode'];
        $phonecode = $result['phonecode'];
        $phone = $result['phone'];
        $companyEmail = $result['companyEmail'];
        $companyURL = $result['companyURL'];
    }

    if($count==0)
    {
        $enquiry_email = getCompanyEmail();
        $companyName = getWebsiteTitle();
        $companyURL = getWebsiteBasePath();
    }

?>

<div class="row input-row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        Enquiry Email Id
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <input type="text" class="form-control enquiry_email" value="<?php echo $enquiry_email;?>"> 
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">

    </div>
    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
        <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
            <span class='fa fa-exclamation-circle'></span> 
            <strong>Note: </strong> Enquiry form emails will be delivered to this email address.<br/>
        </div>
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        Company Name
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <input type="text" class="form-control companyName" value="<?php echo $companyName;?>" placeholder="Ex. My Company"> 
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        Street
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <input type="text" class="form-control street" value="<?php echo @$street;?>"> 
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        Country
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <select name="country" class="form-control country">
            <option value="0">Select Country</option>
            <?php
                $query  = "SELECT * FROM `countries` ORDER BY `name` ASC";
                $stmt   = $link->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchAll();
                foreach( $result as $row )
                {
                    $country_name =  $row['name'];
                    $country_id = $row['id']; 

                    if(isset($country))
                    {
                        if($country==$country_id)
                        {
                            echo "<option value=".$country_id." selected>".$country_name."</option>";
                        }
                        else
                        {
                            echo "<option value=".$country_id.">".$country_name."</option>";
                        }
                    }
                    else
                    {
                        echo "<option value=".$country_id.">".$country_name."</option>";
                    }
                }
            ?>
        </select> 
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        State
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <select name="state" class="form-control state">
        <?php
            if($count>0)
            {
                $sql_states = "SELECT * FROM states WHERE country_id='$country'";
                $stmt_states= $link->prepare($sql_states);
                $stmt_states->execute();
                $result_states = $stmt_states->fetchAll();

                foreach ($result_states as $row_states) 
                {
                    $state_id = $row_states['id'];
                    $state_name = $row_states['name'];

                    if($state_id==$state)
                    {
                        echo "<option value='".$state_id."' selected>".$state_name."</option>";
                    }
                    else
                    {
                        echo "<option value='".$state_id."'>".$state_name."</option>";
                    }
                }
            }
        ?>
        </select> 
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        City
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <select name="city" class="form-control city">
        <?php
            if($count>0)
            {
                $sql_city = "SELECT * FROM cities WHERE state_id='$state'";
                $stmt_city= $link->prepare($sql_city);
                $stmt_city->execute();
                $result_city = $stmt_city->fetchAll();

                foreach ($result_city as $row_city) 
                {
                    $city_id = $row_city['id'];
                    $city_name = $row_city['name'];

                    if($city_id==$city)
                    {
                        echo "<option value='".$city_id."' selected>".$city_name."</option>";
                    }
                    else
                    {
                        echo "<option value='".$city_id."'>".$city_name."</option>";
                    }
                }
            }
        ?>
        </select> 
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        Postal Code
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <input type="text" class="form-control postalCode" value="<?php echo @$postalCode;?>" placeholder="Ex. 444444"> 
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        Phone
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <div class="input-group">
            <span class="input-group-addon country_code">
                <?php
                    echo @$phonecode;
                ?>
            </span>
            <input type="text" name="phone" class="form-control phone" placeholder="Ex. 1234567890"  value="<?php echo @$phone;?>" maxlength="20">
        </div>
        <input type="hidden"  name="country_phone_code" class="country_phone_code"  value="<?php echo @$phonecode;?>"/>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="phone-error"></div>
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        Company Email
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <input type="text" class="form-control companyEmail" value="<?php echo @$companyEmail;?>" placeholder="Ex. email@email.com"> 
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="email_error"></div>
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        Company URL
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <input type="text" class="form-control companyURL" value="<?php echo $companyURL;?>"  placeholder="Ex. https://www.mycompany.com"> 
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="contact-us-status"></div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <center><button class="btn btn-primary btn_contact_us_submit website-button">Save</button></center>
    </div>
</div>

<script>
    $(document).ready(function() {
        /*   Prevent entering charaters in mobile & phone number   */
        $(".phone").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.phone-error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Albhabets not allowed. Enter Digits only.</div>").show().fadeOut(3000);
                $(".phone").addClass('danger_error');
                return false;
            }
        });

        $('.country').change(function(){         //Fetch states
            var country_id = $(this).val();
            var task = "Fetch_state_data";  

            $.ajax({
                type:'post',
                data:'country_id='+country_id+'&task='+task,
                url:'query/website-content-setting/Contact_Us_helper.php',
                success:function(res)
                {
                    var result = $.parseJSON(res);
                    $('.state').html(result[0]);
                    $('.country_code').html("+"+result[1]);
                    $('.country_phone_code').val(result[1]);
                }
            });         
        });

        $('.state').change(function(){         //Fetch cities
            var state_id = $(this).val();
            var task = "Fetch_city_data";  
            $.ajax({
                type:'post',
                data:'state_id='+state_id+'&task='+task,
                url:'query/website-content-setting/Contact_Us_helper.php',
                success:function(res)
                {
                    $('.city').html(res);
                }
            });         
        });

        $('.companyName').change(function(){
            $(this).removeClass('danger_error');
        });

        $('.street').change(function(){
            $(this).removeClass('danger_error');
        });

        $('.country').change(function(){
            $(this).removeClass('danger_error');
            $('.country_code').removeClass('danger_error');
        });

        $('.state').change(function(){
            $(this).removeClass('danger_error');
        });

        $('.city').change(function(){
            $(this).removeClass('danger_error');
        });

        $('.postalCode').change(function(){
            $(this).removeClass('danger_error');
        });

        $('.phone').change(function(){
            $(this).removeClass('danger_error');
        });

        $('.companyEmail').change(function(){
            $(this).removeClass('danger_error');
            $('.email_error').html("");
        });

        $('.companyURL').change(function(){
            $(this).removeClass('danger_error');
        });
        
        $('.btn_contact_us_submit').click(function(){
            var enquiry_email = $('.enquiry_email').val();
            var companyName = $('.companyName').val();
            var street = $('.street').val();
            var city = $('.city').val();
            var state = $('.state').val();
            var country = $('.country').val();
            var postalCode = $('.postalCode').val();
            var phonecode = $('.country_phone_code').val();
            var phone = $('.phone').val();
            var companyEmail = $('.companyEmail').val();
            var companyURL = $('.companyURL').val();
            var status;
            var task = "Update-Contact-Us-Page";

            if(enquiry_email=='' || enquiry_email==null)
            {
                $('.enquiry_email').addClass('danger_error');
                status=0;
            }

            if(companyName=='' || companyName==null)
            {
                $('.companyName').addClass('danger_error');
                status=0;
            }

            if(street=='' || street==null)
            {
                $('.street').addClass('danger_error');
                status=0;
            }

            if(country=='0' || country==null || country=='undefined')
            {
                $('.country').addClass('danger_error');
                $('.country_code').addClass('danger_error');
                status=0;
            }

            if(state=='0' || state==null || state=='undefined')
            {
                $('.state').addClass('danger_error');
                status=0;
            }

            if(city=='0' || city==null || city=='undefined')
            {
                $('.city').addClass('danger_error');
                status=0;
            }

            if(postalCode=='' || postalCode==null)
            {
                $('.postalCode').addClass('danger_error');
                status=0;
            }

            if(phone=='' || phone==null)
            {
                $('.phone').addClass('danger_error');
                status=0;
            }

            if(companyEmail=='' || companyEmail==null)
            {
                $('.companyEmail').addClass('danger_error');
                status=0;
            }

            function validateEmail($companyEmail) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $companyEmail );
            }

            if(companyEmail!='' && !validateEmail(companyEmail)) 
            { 
                $('.email_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Enter valid Email.</div>");
                $('.companyEmail').addClass('danger_error');
                status=0;
            }

            if(companyURL=='' || companyURL==null)
            {
                $('.companyURL').addClass('danger_error');
                status=0;
            }

            if(status==0)
            {
                $('.contact-us-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span><strong> Error! </strong> Please enter all mandatory data.</div></center>");
                return false;
            }

            $('.contact-us-status').html("");

            
            var data = 'enquiry_email='+enquiry_email+'&companyName='+companyName+'&street='+street+'&city='+city+'&state='+state+'&country='+country+'&postalCode='+postalCode+'&phonecode='+phonecode+'&phone='+phone+'&companyEmail='+companyEmail+'&companyURL='+companyURL+'&task='+task;

            $('.btn_contact_us_submit').attr('disabled',true);
            $('.loading_img').show();

            $.ajax({
                type:'post',
                url:'query/website-content-setting/Contact_Us_helper.php',
                data:data,
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.contact-us-status').html("<center><div class='alert alert-success contact-us-status-new' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span> Data successfullly updated.</div></center>");
                        $('.contact-us-status-new').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes&sub_tab=Contact_Us");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.btn_contact_us_submit').attr('disabled',false);
                        $('.contact-us-status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span><strong>Error! </strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>