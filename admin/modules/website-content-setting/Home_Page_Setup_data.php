<?php
    $stmt   = $link->prepare("SELECT * FROM `homepagesetup`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    if($count>0)
    {
        $homepagebanner = $result['homepagebanner'];
        $homepageHeading= $result['homepageHeading'];
        $footercontentshow = $result['footercontentshow'];
        $footercontent = $result['footercontent'];
        $extracontentstripshow = $result['extracontentstripshow'];
        $extracontentstripdata = $result['extracontentstripdata'];
        $featuredprofilesshow = $result['featuredprofilesshow'];
        $featuredprofilescount = $result['featuredprofilescount'];
        $premiumprofilesshow = $result['premiumprofilesshow'];
        $premiumprofilescount = $result['premiumprofilescount'];
        $recentlyaddedprofilesshow = $result['recentlyaddedprofilesshow'];
        $recentlyaddedprofilescount = $result['recentlyaddedprofilescount'];
        $profilefiltershow = $result['profilefiltershow'];
        $profilefiltervalues = $result['profilefiltervalues'];

        if($profilefiltervalues!='' || $profilefiltervalues!=null)
        {
            $profilefiltervalues = explode(',',$profilefiltervalues);
        }

        $additional_footer_content1_display = $result['additional_footer_content1_display'];
        $additional_footer_content1 = $result['additional_footer_content1'];
        $additional_footer_content2_display = $result['additional_footer_content2_display'];
        $additional_footer_content2 = $result['additional_footer_content2'];
        $additional_footer_content3_display = $result['additional_footer_content3_display'];
        $additional_footer_content3 = $result['additional_footer_content3'];
        $additional_footer_content4_display = $result['additional_footer_content4_display'];
        $additional_footer_content4 = $result['additional_footer_content4'];

    }

    $WebsiteBasePath = getWebsiteBasePath();
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Home Page Banner:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input type="file" name="homepagebanner" id="homepagebanner" class="form-control homepagebanner" />
                <div class="status_homepagebanner">
            
                </div>
                <br/>
                
            </div>
        </div>
        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    <strong>Note: </strong> Recommended size 1350px X 450px<br/>
                    Only files with extention <b><?php echo getAllowedFileAttachmentTypesString();?> </b> allowed to upload.
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4"  id="uploaded_image">
                <?php
                    if($count>0 && ($homepagebanner!='' || $homepagebanner!=null))
                    {
                        echo "<div class='isotope-item document'>
                            <div class='thumbnail'>
                                <div class='thumb-preview'>
                                    <a class='thumb-image' href='$homepagebanner'>
                                        <img src='$homepagebanner' class='img-responsive home-page-banner'>
                                    </a>
                                </div>
                            </div>
                        </div>";

                        
                    }
                    else
                    {
                        $homepagebanner = '../images/home-page-banner/default-wedding-template.png';
                        echo "<div class='isotope-item document'>
                            <div class='thumbnail'>
                                <div class='thumb-preview'>
                                    <a class='thumb-image' href='$homepagebanner'>
                                        <img src='$homepagebanner' class='img-responsive home-page-banner'>
                                    </a>
                                </div>
                            </div>
                        </div>";
                    }
                ?>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Homepage Heading:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                <input type="text" class="form-control homepageHeading" value="<?php echo @$homepageHeading; ?>">
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <input type="text" class="form-control homepageHeading" value="<?php echo @$homepageHeading; ?>">
                <br/>
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    <strong>Note: </strong>  Above text will be shown below  &lt;h1> <br/>
                </div>
            </div>
        </div>
        

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Show Custom Content Section:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                <input type="radio" name="extracontentstripshow" class="extracontentstripshow" value="1" <?php if(@$extracontentstripshow=='1') echo 'checked';?>>&nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="extracontentstripshow" class="extracontentstripshow" value="0" <?php if(@$extracontentstripshow=='0' || @$extracontentstripshow=='' || @$extracontentstripshow==null) echo 'checked';?>>&nbsp;&nbsp;No 
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <label class="control-label">Custom Content Section Data:</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <textarea class="form-control extracontentstripdata" rows="5"><?php if($count>0 && ($extracontentstripdata!='' || $extracontentstripdata!=null)) echo $extracontentstripdata;?></textarea>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Show Footer Content Section:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                <input type="radio" name="footercontentshow" class="footercontentshow" value="1" <?php if(@$footercontentshow=='1') echo 'checked';?>>&nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="footercontentshow" class="footercontentshow" value="0" <?php if(@$footercontentshow=='0' || @$footercontentshow=='' || @$footercontentshow==null) echo 'checked';?>>&nbsp;&nbsp;No 
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <label class="control-label">Footer Section Data:</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <textarea class="form-control footercontent" rows="5"><?php if($count>0 && ($footercontent!='' || $footercontent!=null)) echo $footercontent;?></textarea>
            </div>
        </div>

        <hr/>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Show Featured Profiles:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                <input type="radio" name="featuredprofilesshow" class="featuredprofilesshow" value="1" <?php if(@$featuredprofilesshow=='1') echo 'checked';?>>&nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="featuredprofilesshow" class="featuredprofilesshow" value="0" <?php if(@$featuredprofilesshow=='0' || @$featuredprofilesshow=='' || @$featuredprofilesshow==null) echo 'checked';?>>&nbsp;&nbsp;No 
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Number of rows featured profiles to show:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                <select class="form-control featuredprofilescount">
                    <option value='0'>Select</option>
                    <?php
                        for($i=1;$i<6;$i++)
                        {
                            if($featuredprofilescount==$i)
                            {
                                echo "<option value='".$i."' selected>".$i."</option>";
                            }
                            else
                            {
                                echo "<option value='".$i."'>".$i."</option>";
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    <strong>Note: </strong> Showing 4 profiles per row.<br/>
                </div>
            </div>
        </div>

        <hr/>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Show Premium Profiles:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                <input type="radio" name="premiumprofilesshow" class="premiumprofilesshow" value="1" <?php if(@$premiumprofilesshow=='1') echo 'checked';?>>&nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="premiumprofilesshow" class="premiumprofilesshow" value="0" <?php if(@$premiumprofilesshow=='0' || @$premiumprofilesshow=='' || @$premiumprofilesshow==null) echo 'checked';?>>&nbsp;&nbsp;No 
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Number of rows premium profiles to show:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                <select class="form-control premiumprofilescount">
                    <option value='0'>Select</option>
                    <?php
                        for($i=1;$i<6;$i++)
                        {
                            if($premiumprofilescount==$i)
                            {
                                echo "<option value='".$i."' selected>".$i."</option>";
                            }
                            else
                            {
                                echo "<option value='".$i."'>".$i."</option>";
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    <strong>Note: </strong> Showing 4 profiles per row.<br/>
                </div>
            </div>
        </div>

        <hr/>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Show Recently Added Profiles:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                <input type="radio" name="recentlyaddedprofilesshow" class="recentlyaddedprofilesshow" value="1" <?php if(@$recentlyaddedprofilesshow=='1') echo 'checked';?>>&nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="recentlyaddedprofilesshow" class="recentlyaddedprofilesshow" value="0" <?php if(@$recentlyaddedprofilesshow=='0' || @$recentlyaddedprofilesshow=='' || @$recentlyaddedprofilesshow==null) echo 'checked';?>>&nbsp;&nbsp;No 
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Number of rows recently Added profiles to show:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                <select class="form-control recentlyaddedprofilescount">
                    <option value='0'>Select</option>
                    <?php
                        for($i=1;$i<6;$i++)
                        {
                            if($recentlyaddedprofilescount==$i)
                            {
                                echo "<option value='".$i."' selected>".$i."</option>";
                            }
                            else
                            {
                                echo "<option value='".$i."'>".$i."</option>";
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    <strong>Note: </strong> Showing 4 profiles per row.<br/>
                </div>
            </div>
        </div>

        <hr/>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <label class="control-label">Show Profile Filters:</label>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <input type="radio" name="profilefiltershow" class="profilefiltershow" value="1" <?php if(@$profilefiltershow=='1') echo 'checked';?>>&nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="profilefiltershow" class="profilefiltershow" value="0" <?php if(@$profilefiltershow=='0' || @$profilefiltershow=='' || @$profilefiltershow==null) echo 'checked';?>>&nbsp;&nbsp;No 
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 text-right">
                <input type='checkbox' class="select_all" id="select_all">&nbsp; Select All
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <label class="control-label">Select filters want to show on homepage:</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="row input-row">
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('1',$profilefiltervalues)){ echo "checked"; } ?> value="1">&nbsp; Location
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('2',$profilefiltervalues)){ echo "checked"; } ?> value="2">&nbsp; Religion
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('3',$profilefiltervalues)){ echo "checked"; } ?> value="3">&nbsp; Caste
                    </div>
                </div>

                <div class="row input-row">
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('4',$profilefiltervalues)){ echo "checked"; } ?> value="4">&nbsp; Mother Tongue
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('5',$profilefiltervalues)){ echo "checked"; } ?> value="5">&nbsp; Education
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('6',$profilefiltervalues)){ echo "checked"; } ?> value="6">&nbsp; Profession
                    </div>
                </div>

                <div class="row input-row">
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('7',$profilefiltervalues)){ echo "checked"; } ?> value="7">&nbsp; Marital Status
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('8',$profilefiltervalues)){ echo "checked"; } ?> value="8">&nbsp; Family Value
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('9',$profilefiltervalues)){ echo "checked"; } ?> value="9">&nbsp; Family Type
                    </div>
                </div>

                <div class="row input-row">
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('10',$profilefiltervalues)){ echo "checked"; } ?> value="10">&nbsp; Family Status
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('11',$profilefiltervalues)){ echo "checked"; } ?> value="11">&nbsp; Body Type
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('12',$profilefiltervalues)){ echo "checked"; } ?> value="12">&nbsp; Complexion
                    </div>
                </div>

                <div class="row input-row">
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('13',$profilefiltervalues)){ echo "checked"; } ?> value="13">&nbsp; Eating Habbit
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('14',$profilefiltervalues)){ echo "checked"; } ?> value="14">&nbsp; Smoking Habbit
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <input type="checkbox" name="profilefiltervalues[]" class="profilefilter" <?php if(!empty($profilefiltervalues) && in_array('15',$profilefiltervalues)){ echo "checked"; } ?> value="15">&nbsp; Drinking Habbit
                    </div>
                </div>
            </div>
        </div>

        <hr/>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                Additional Footer Content:
            </div>
            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <input type="checkbox" name="additional_footer_content1_display" class="additional_footer_content1_display" <?php if(@$additional_footer_content1_display=='1'){ echo "checked"; } ?> value="1">&nbsp;  Section 1
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <input type="checkbox" name="additional_footer_content2_display" class="additional_footer_content2_display" <?php if(@$additional_footer_content2_display=='1'){ echo "checked"; } ?> value="1">&nbsp;  Section 2
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <input type="checkbox" name="additional_footer_content3_display" class="additional_footer_content3_display" <?php if(@$additional_footer_content3_display=='1'){ echo "checked"; } ?> value="1">&nbsp;  Section 3
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <input type="checkbox" name="additional_footer_content4_display" class="additional_footer_content4_display" <?php if(@$additional_footer_content4_display=='1'){ echo "checked"; } ?> value="1">&nbsp;  Section 4
                    </div>
                </div>
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                
            </div>
            <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <textarea class='form-control additional_footer_content1' placeholder="Enter section 1 content here" rows="5" <?php if(@$additional_footer_content1_display!='1'){ echo "style=display:none;"; }?>><?php echo @$additional_footer_content1;?></textarea>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <textarea class='form-control additional_footer_content2' placeholder="Enter section 2 content here" rows="5" <?php if(@$additional_footer_content2_display!='1'){ echo "style=display:none;"; }?>><?php echo @$additional_footer_content2;?></textarea>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <textarea class='form-control additional_footer_content3' placeholder="Enter section 3 content here" rows="5" <?php if(@$additional_footer_content3_display!='1'){ echo "style=display:none;"; }?>><?php echo @$additional_footer_content3;?></textarea>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <textarea class='form-control additional_footer_content4' placeholder="Enter section 4 content here" rows="5" <?php if(@$additional_footer_content4_display!='1'){ echo "style=display:none;"; }?>><?php echo @$additional_footer_content4;?></textarea>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 input-row">
            <div class="row">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 homepagesetup_status">
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <center><button class="btn btn-success btn-sm btn_homepagesetup website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
                </div>
            </div>
        </div>

    </div>
</div>
<hr style="border-bottom: 3px solid #000;">

<?php
    $sql1   = "SELECT * FROM `popup_setting`";
    $stmt1   = $link->prepare($sql1);
    $stmt1->execute();
    $result1 = $stmt1->fetch();
    $count1=$stmt1->rowCount();

    if($count1>0)
    {
        $popup_display_or_not = $result1['popup_display_or_not'];
        $popup_image = $result1['popup_image'];
        $destination_url = $result1['destination_url'];
        $target = $result1['target'];
    }
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Homepage Pop-up Setting</h2>
            </div>
        </div>

        <form enctype="multipart/form-data" id="fupForm" >
            <div class="row input-row">
                <div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
                    Enable Popup on homepage:
                </div>
                <div class="col-xs-4 col-sm-4 col-md-1 col-lg-1">
                    <input type="checkbox" name="popup_display_or_not" id="popup_display_or_not" class="popup_display_or_not" <?php if($count1>0 && ($popup_display_or_not=='1')) { echo 'checked';}?> value="1">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                        <span class='fa fa-exclamation-circle'></span> <strong>Note: </strong> If you enable this popup then it will be visible on homepage of website.<br/>
                        Recommended image size <strong><i>600px * 400px</i></strong><br>
                    </div>
                </div>
            </div>

            <div class="row input-row homepage-popup-div" <?php if(@$popup_display_or_not!='1' || $count1==0) { echo "style=display:none;";}
            ?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Popup Image:
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <input type="file" class="form-control popup_image" id="popup_image" name="popup_image" /><br/>
                    <div class="alert alert-info">
                        <strong>Note: </strong> Only files with extention <b><?php echo getAllowedFileAttachmentTypesString();?> </b> allowed to upload.
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <?php
                        if($count1>0 && ($popup_image!='' || $popup_image!=null))
                        {
                            $popup_image_path = $WebsiteBasePath.'/'.$popup_image;
                            echo "<div class='isotope-item document'>
                                <div class='thumbnail'>
                                    <div class='thumb-preview'>
                                        <a class='thumb-image' href='$popup_image_path'>
                                            <img src='$popup_image_path' class='img-responsive homepage-popup'>
                                        </a>
                                    </div>
                                </div>
                            </div>";
                        }
                    ?>
                </div>
            </div>

            <div class="row input-row homepage-popup-div" <?php if(@$popup_display_or_not!='1' || $count1==0) { echo "style=display:none;";}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Destination URL:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <input type="text" class="form-control destination_url" id="destination_url" name="destination_url" placeholder="For Ex: https://www.google.com" value="<?php echo @$destination_url;?>" />
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    
                </div>
            </div>
            <div class="row input-row homepage-popup-div" <?php if(@$popup_display_or_not!='1' || $count1==0) { echo "style=display:none;";}?>>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    Target page open in:
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <select class="form-control '$target'" name="target">
                        <?php

                        ?>
                        <option value="0" <?php if(($count1==0 || $count1>0) && $target=='0') { echo 'selected'; } ?>>Same Window/Tab</option>
                        <option value="1" <?php if($count1>0 && $target=='1') { echo 'selected'; } ?>>New Window/Tab</option>
                    </select>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    
                </div>
            </div>
            <input type="hidden" name="prev_image_available" class="prev_image_available" value="<?php echo $count1;?>">
            <div class="row input-row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 homepage_popup_status">
                
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img_homepage_popup' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
                </div>
            </div>

            <div class="row input-row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <center>
                        <input type="submit" name="submit" class="btn btn-danger submitBtn btn_submit_homepage_popup_data website-button" value="Save Homepage Popup Setting"/>
                    </center>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.extracontentstripdata').richText();
        $('.footercontent').richText();

        var featuredprofilesshow = $("input[name='featuredprofilesshow']:checked").val();
        var premiumprofilesshow = $("input[name='premiumprofilesshow']:checked").val();
        var recentlyaddedprofilesshow = $("input[name='recentlyaddedprofilesshow']:checked").val();

        var profilefiltershow = $("input[name='profilefiltershow']:checked").val();
      
        if(featuredprofilesshow=='0')
        {
            $(".featuredprofilescount").attr('disabled',true);
        }

        if(premiumprofilesshow=='0')
        {
            $(".premiumprofilescount").attr('disabled',true);
        }

        if(recentlyaddedprofilesshow=='0')
        {
            $(".recentlyaddedprofilescount").attr('disabled',true);
        }

        if(profilefiltershow=='0')
        {
            $(".profilefilter").attr('disabled',true);
            $(".select_all").attr('disabled',true);
        }

        $('#select_all').click(function(){
            if($(this).prop("checked")) {
                $(".profilefilter").prop("checked", true);
            } else {
                $(".profilefilter").prop("checked", false);
            }                
        });
        
        $('#homepagebanner').change(function(e){
            var file = $(this).val();
            var ext = file.split('.').pop();
            var img_array = "<?php echo getAllowedFileAttachmentTypesString(); ?>";

            var i = img_array.indexOf(ext);
            if(i<= -1) 
            {
                $('.status_homepagebanner').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 15px;'><span class='fa fa-times'></span><strong> Error! </strong> Invalid image type.</div>");
                return false;
            }

            var formData = new FormData();
            formData.append('file', $(this)[0].files[0]);
            var homepagebanner_img = e.target.files[0].name;

            $.ajax({
                url : 'Homepage_Banner_Upload.php',
                type : 'POST',
                data : formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data) 
                {
                    if(data=='success')
                    {
                        
                        $('.status_homepagebanner').html("<center><div class='alert alert-success status_homepagebanner_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Homepage banner updated successfully.</div></center>");
                        
                        $('.status_homepagebanner_success').fadeTo(1000, 500).slideUp(500, function(){
                            $('.home-page-banner').attr('src','../images/home-page-banner/'+homepagebanner_img);
                            $('.thumb-image').attr('href','../images/home-page-banner/'+homepagebanner_img);
                        });
                    }
                    else
                    {
                        $('.status_homepagebanner').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+data+"</div></center>");
                        return false;
                    }
                }
            });
        });

        $('.extracontentstripshow').change(function(){
            var extracontentstripshow = $(this).val();
            if(extracontentstripshow=='0')
            {
                $(".extracontentstripdata").attr('disabled',true);
            }
            else
            {
                $(".extracontentstripdata").attr('disabled',false);
            }
        });

        $('.featuredprofilesshow').change(function(){
            var featuredprofilesshow = $(this).val();
            if(featuredprofilesshow=='0')
            {
                $(".featuredprofilescount").attr('disabled',true);
            }
            else
            {
                $(".featuredprofilescount").attr('disabled',false);
            }
        });

        $('.premiumprofilesshow').change(function(){
            var premiumprofilesshow = $(this).val();
            if(premiumprofilesshow=='0')
            {
                $(".premiumprofilescount").attr('disabled',true);
            }
            else
            {
                $(".premiumprofilescount").attr('disabled',false);
            }
        });

        $('.recentlyaddedprofilesshow').change(function(){
            var recentlyaddedprofilesshow = $(this).val();
            if(recentlyaddedprofilesshow=='0')
            {
                $(".recentlyaddedprofilescount").attr('disabled',true);
            }
            else
            {
                $(".recentlyaddedprofilescount").attr('disabled',false);
            }
        });

        $('.profilefiltershow').change(function(){
            var profilefiltershow = $(this).val();
            if(profilefiltershow=='0')
            {
                $(".profilefilter").attr('disabled',true);
                $(".select_all").attr('disabled',true);
            }
            else
            {
                $(".profilefilter").attr('disabled',false);
                $(".select_all").attr('disabled',false);
            }
        });

        $('.additional_footer_content1_display').change(function(){
            if($(this).is(':checked'))
            {
                var additional_footer_content1_display = '1';
            }
            else
            {
                var additional_footer_content1_display = '0';
            }

            if(additional_footer_content1_display=='1')
            {
                $('.additional_footer_content1').show();
                return false;
            }
            else
            {
                $('.additional_footer_content1').hide();
                return false;
            }
        });

        $('.additional_footer_content2_display').change(function(){
            if($(this).is(':checked'))
            {
                var additional_footer_content2_display = '1';
            }
            else
            {
                var additional_footer_content2_display = '0';
            }

            if(additional_footer_content2_display=='1')
            {
                $('.additional_footer_content2').show();
                return false;
            }
            else
            {
                $('.additional_footer_content2').hide();
                return false;
            }
        });

        $('.additional_footer_content3_display').change(function(){
            if($(this).is(':checked'))
            {
                var additional_footer_content3_display = '1';
            }
            else
            {
                var additional_footer_content3_display = '0';
            }

            if(additional_footer_content3_display=='1')
            {
                $('.additional_footer_content3').show();
                return false;
            }
            else
            {
                $('.additional_footer_content3').hide();
                return false;
            }
        });

        $('.additional_footer_content4_display').change(function(){
            if($(this).is(':checked'))
            {
                var additional_footer_content4_display = '1';
            }
            else
            {
                var additional_footer_content4_display = '0';
            }

            if(additional_footer_content4_display=='1')
            {
                $('.additional_footer_content4').show();
                return false;
            }
            else
            {
                $('.additional_footer_content4').hide();
                return false;
            }
        });

        /*   Prevent entering charaters in featuredprofilescount   */
        $(".featuredprofilescount, .premiumprofilescount, .recentlyaddedprofilescount").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                return false;
            }
        });

        $('.btn_homepagesetup').click(function(){
            var homepageHeading = $('.homepageHeading').val();
            var footercontentshow = $("input[name='footercontentshow']:checked").val();
            var footercontent = $('.footercontent').val();
            var extracontentstripshow = $("input[name='extracontentstripshow']:checked").val();
            var extracontentstripdata = $(".extracontentstripdata").val();

            var featuredprofilesshow = $("input[name='featuredprofilesshow']:checked").val();
            var featuredprofilescount = $(".featuredprofilescount").val();

            var premiumprofilesshow = $("input[name='premiumprofilesshow']:checked").val();
            var premiumprofilescount = $(".premiumprofilescount").val();

            var recentlyaddedprofilesshow = $("input[name='recentlyaddedprofilesshow']:checked").val();
            var recentlyaddedprofilescount = $(".recentlyaddedprofilescount").val();

            var profilefiltershow = $("input[name='profilefiltershow']:checked").val();
            var profilefiltervalues = [];
            $('.profilefilter:checked').each(function() {
               profilefiltervalues.push($(this).val());
            });

            if(footercontentshow=='undefined')
            {
                footercontentshow = '0';
            }
            if(extracontentstripshow=='undefined')
            {
                extracontentstripshow = '0';
            }
            if(featuredprofilesshow=='undefined')
            {
                featuredprofilesshow = '0';
            }
            if(premiumprofilesshow=='undefined')
            {
                premiumprofilesshow = '0';
            }
            if(recentlyaddedprofilesshow=='undefined')
            {
                recentlyaddedprofilesshow = '0';
            }
            if(profilefiltershow=='undefined')
            {
                profilefiltershow = '0';
            }

            if($('.additional_footer_content1_display').is(':checked'))
            {
                var additional_footer_content1_display = '1';
            }
            else
            {
                var additional_footer_content1_display = '0';
            }

            if($('.additional_footer_content2_display').is(':checked'))
            {
                var additional_footer_content2_display = '1';
            }
            else
            {
                var additional_footer_content2_display = '0';
            }

            if($('.additional_footer_content3_display').is(':checked'))
            {
                var additional_footer_content3_display = '1';
            }
            else
            {
                var additional_footer_content3_display = '0';
            }

            if($('.additional_footer_content4_display').is(':checked'))
            {
                var additional_footer_content4_display = '1';
            }
            else
            {
                var additional_footer_content4_display = '0';
            }

            var additional_footer_content1 = $('.additional_footer_content1').val();
            var additional_footer_content2 = $('.additional_footer_content2').val();
            var additional_footer_content3 = $('.additional_footer_content3').val();
            var additional_footer_content4 = $('.additional_footer_content4').val();

            if(footercontentshow=='1' && (footercontent=='' || footercontent==null || footercontent.length<=0))
            {
                $('.homepagesetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter footer contents.</div></center>");
                return false;
            }

            if(featuredprofilesshow=='1' && featuredprofilescount=='0')
            {
                $('.homepagesetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please select number of rows display featured profiles.</div></center>");
                return false;
            }

            if(premiumprofilesshow=='1' && premiumprofilescount=='0')
            {
                $('.homepagesetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please select number of rows display premium profiles.</div></center>");
                return false;
            }

            if(recentlyaddedprofilesshow=='1' && recentlyaddedprofilescount=='0')
            {
                $('.homepagesetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please select number of rows display recently added profiles.</div></center>");
                return false;
            }

            if(profilefiltershow=='1' && (profilefiltervalues=='' || profilefiltervalues==null))
            {
                $('.homepagesetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please select filters want to display on homepage.</div></center>");
                return false;
            }

            if(additional_footer_content1_display=='1' && (additional_footer_content1=='' || additional_footer_content1==null))
            {
                $('.homepagesetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter section 1 content.</div></center>");
                return false;
            }

            if(additional_footer_content2_display=='1' && (additional_footer_content2=='' || additional_footer_content2==null))
            {
                $('.homepagesetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter section 2 content.</div></center>");
                return false;
            }

            if(additional_footer_content3_display=='1' && (additional_footer_content3=='' || additional_footer_content3==null))
            {
                $('.homepagesetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter section 3 content.</div></center>");
                return false;
            }

            if(additional_footer_content4_display=='1' && (additional_footer_content4=='' || additional_footer_content4==null))
            {
                $('.homepagesetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter section 4 content.</div></center>");
                return false;
            }

            var task = "homepagesetup_update";

            var data = {
                homepageHeading : homepageHeading,
                footercontentshow : footercontentshow,
                footercontent : footercontent,
                extracontentstripshow : extracontentstripshow,
                extracontentstripdata : extracontentstripdata,
                featuredprofilesshow : featuredprofilesshow,
                featuredprofilescount : featuredprofilescount,
                premiumprofilesshow : premiumprofilesshow,
                premiumprofilescount : premiumprofilescount,
                recentlyaddedprofilesshow : recentlyaddedprofilesshow,
                recentlyaddedprofilescount : recentlyaddedprofilescount,
                profilefiltershow : profilefiltershow,
                profilefiltervalues : profilefiltervalues,
                footercontentshow : footercontentshow,
                additional_footer_content1_display : additional_footer_content1_display,
                additional_footer_content1 : additional_footer_content1,
                additional_footer_content2_display : additional_footer_content2_display,
                additional_footer_content2 : additional_footer_content2,
                additional_footer_content3_display : additional_footer_content3_display,
                additional_footer_content3 : additional_footer_content3,
                additional_footer_content4_display : additional_footer_content4_display,
                additional_footer_content4 : additional_footer_content4,
                task:task
            }

            $('.loading_img').show();

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,
                url : 'query/website-content-setting/Homepage_helper.php',
                success : function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.homepagesetup_status').html("<center><div class='alert alert-success homepagesetup_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Homepage data updated successfully.</div></center>");
                        
                        $('.homepagesetup_status_success').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes+&sub_tab=Home_Page");
                            return false;
                        });
                    }
                    else
                    {
                        $('.homepagesetup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
        

        /**************   Homepage Popup Setting    *****************/
        /*    Popup enable or not start   */
            $('.popup_display_or_not').change(function(){

                if($(this).is(':checked'))
                {
                    var popup_display_or_not = '1';
                }
                else
                {
                    var popup_display_or_not = '0';
                }

                if(popup_display_or_not=='1')
                {
                    $('.homepage-popup-div').show();
                    return false;
                }
                else
                {
                    $('.homepage-popup-div').hide();
                    return false;
                }    
            });
        /*    Popup enable or not end   */

        //file type validation
        $("#popup_image").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
                alert('Please select a valid image file (JPEG/JPG/PNG).');
                $("#popup_image").val('');
                return false;
            }
        });

        $('.btn_submit_homepage_popup_data').click(function(){
            if($('.popup_display_or_not').is(':checked'))
            {
                var popup_display_or_not = '1';
            }
            else
            {
                var popup_display_or_not = '0';
            }

            var popup_image = $('#popup_image').val();
            var destination_url = $('.destination_url').val();
            var prev_image_available = $('.prev_image_available').val();

            if(prev_image_available==0 && (popup_display_or_not=='1' && (popup_image=='' || popup_image==null || popup_image=='undefined')))
            {
                $('.homepage_popup_status').html("<div class='alert alert-danger'><strong>Empty! </strong> Please select image for popup.</div>");
                return false;
            }

            if(popup_display_or_not=='1' && (destination_url=='' || destination_url==null || destination_url=='undefined'))
            {
                $('.homepage_popup_status').html("<div class='alert alert-danger'><strong>Empty! </strong> Enter destination url for popup.</div>");
                return false;
            }

            $('.homepage_popup_status').html("");
            $('loading_img_homepage_popup').show();
        });

        $("#fupForm").on('submit', function(e){
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: 'query/website-content-setting/homepage_popup_helper.php',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                    $('.submitBtn').attr("disabled","disabled");
                    $('#fupForm').css("opacity",".5");
                },
                success: function(msg){
                    $('.homepage_popup_status').html("");
                    $('loading_img_homepage_popup').hide();
                    if(msg == 'ok')
                    {
                        $('#fupForm')[0].reset();
                        $('.homepage-popup-div').hide();
                        $('.homepage_popup_status').html("<div class='alert alert-success'><strong>Success! </strong> Popup setting updated successfully.</div>");
                        $('.homepage_popup_status').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes+&sub_tab=Home_Page");
                            return false;
                        });
                    }
                    else
                    {
                        $('.homepage_popup_status').html("<div class='alert alert-danger'><strong>Error! </strong> "+msg+"</div>");
                    }
                    $('#fupForm').css("opacity","");
                    $(".submitBtn").removeAttr("disabled");
                }
            });
        });

    });
</script>