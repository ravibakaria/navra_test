<?php
    $stmt   = $link->prepare("SELECT * FROM `faq_page_show`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    if($count>0)
    {
        $faq_chk_show = $result['display'];
    }
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="row div-header-row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h2>Frequently Asked Questions</h2>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-right">
                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-FAQ">Add New FAQ</button>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-right">
                <a href="../faq.html" class="btn btn-primary btn-sm website-button" target="_blank">Preview Page</a>
            </div>
        </div>
        <br/>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <center><h3>Show on Website: &nbsp; <input type="checkbox" class="faq_chk_show" <?php if($count>0 && $faq_chk_show=='1') { echo 'checked';}?>> &nbsp; &nbsp; <button class="btn btn-sm btn-primary btn_display website-button">Save</button></h3></center>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img1' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="display_status"></div>
                <center><div class="alert alert-info" style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please tick <strong>"Show on Website"</strong> checkbox if you want to show this page link on website.</div></center>
            </div>
        </div>
        <br/>

        <div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered FAQ_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>FAQ</th>
                                <th>Answer</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                </center>
            </div>
        </div>
        
        <!--   Model window for addition  -->
        <div class="modal fade FAQ_add_model" id="Add-New-FAQ" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <center><h4 class="modal-title" id="exampleModalLongTitle">Add New FAQ</h4></center>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <lable class="control-label">Question:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                <input type="text" class="form-control form-control-sm FAQ_question" />
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <lable class="control-label">Answer:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                            	<textarea class="form-control form-control-sm FAQ_answer" rows="5"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status_FAQ">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn_add_new_FAQ website-button">Submit</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function(){
    	var dataTable_User_list = $('.FAQ_list').DataTable({          //Admin datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            
            "ajax":{
                url :"datatables/website-content-attributes/FAQ-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".FAQ_list_processing").css("display","none");
                }
            }
        });

        //Admin add button click
        $('.btn_add_new_FAQ').click(function(){
            var faq_question = $('.FAQ_question').val();
            var faq_answer = $('.FAQ_answer').val();
            var task = "Add_New_FAQ";

            if(faq_question=='' || faq_question==null)
            {
                $('.add_status_FAQ').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter question.</div></center>");
                return false;
            }

            if(faq_answer=='' || faq_answer==null)
            {
                $('.add_status_FAQ').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter answer.</div></center>");
                return false;
            }
            
            $('.loading_img').show();
            
            var data = {
                faq_question : faq_question,
                faq_answer : faq_answer,
                task:task
            }

            $.ajax({
                type:'post',
                dataType: 'json',
                data:data,                
                url:'query/website-content-setting/FAQ_helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status_FAQ').html("<center><div class='alert alert-success add_status_FAQ_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> FAQ Added Successfully.</div></center>");
                            $('.add_status_FAQ_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes+&sub_tab=FAQ");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status_FAQ').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });

        //FAQ change status
        $('.FAQ_list tbody').on('click', '.change_status_FAQ', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_FAQ_status";

            var data = 'status='+status+'&id='+id+'&task='+task;
            //alert(data);return false;
            $.ajax({
                type:'post',
                data:data,
                url:'query/website-content-setting/FAQ_helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.FAQ_status'+id).html("<div class='alert alert-success FAQ_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.FAQ_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes+&sub_tab=FAQ");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.FAQ_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });

        $('.btn_display').click(function(){
            if($(".faq_chk_show").is(':checked'))
            {
                var faq_chk_show = '1';
            }
            else
            {
                var faq_chk_show = '0';
            } 

            var task = "Update_FAQ_website_display_Page";

            var data = 'faq_chk_show='+faq_chk_show+'&task='+task;
            $(this).attr('disabled',true);
            $('.loading_img1').show();
            $.ajax({
                type:'post',
                data:data,
                url:'query/website-content-setting/FAQ_helper.php',
                success:function(res)
                {
                    $('.loading_img1').hide();
                    if(res=='success')
                    {
                        $('.display_status').html("<center><div class='alert alert-success add_status_FAQ_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Success! </strong> Display status updated Successfully.</div></center>");
                            $('.add_status_FAQ_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes+&sub_tab=FAQ");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.display_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>