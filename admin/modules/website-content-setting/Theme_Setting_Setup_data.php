<?php 
	$css_content = file_get_contents("../css/master.css"); 
?>
<div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="row input-row">
    		<div class="form-group">
				<div class="col-md-11">
					<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
			            <span class='fa fa-exclamation-circle'></span> 
			            <strong>Note: </strong> Please use html code editor mode by clicking <b>&lt;/></b> from upper right corner of text editor provided below for easy editing.<br/>
			        </div>
			        <div class="page-wrapper box-content">
			            <textarea rows="20" class="content css_data" name="example"><?php echo $css_content;?></textarea>
			        </div>
				</div>
			</div>
    	</div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_`img' style='width:80px; height:80px; display:none;'/></center>
	    <div class="row status">
	        
	    </div>
	    <div class="row">
	        <center><button class="btn btn-success btn_save website-button">Save</button></center>
	    </div>
		
	</div>
</div>



<script>
	$(document).ready(function(){
		$('.content').richText();
		
		$('.btn_save').click(function(){
			var css_data = $('.css_data').val();
			var task = "update_master_css";
			
			var data = {
                css_data : css_data,
                task:task
            }

			$('.loading_img').show();

			$.ajax({
                type:'post',
                dataType: 'json',
                data:data,                
                url:'query/theme-attributes/theme-setting-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.status').html("<center><div class='alert alert-success css_data_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> CSS data updated Successfully.</div></center>");
                            $('.css_data_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes&sub_tab=Theme_Setting");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
		});
	});
</script>