<?php
    $stmt   = $link->prepare("SELECT * FROM `privacy_policy`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    if($count>0)
    {
    	$privacy_policy_content = $result['content'];
        $privacy_chk_show = $result['display'];
    }
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div  class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label class="control-label">Page Content:</label>
                </div>
                <div  class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    Show on Website: &nbsp; <input type="checkbox" class="privacy_chk_show" <?php if($count>0 && ($privacy_chk_show=='1')) { echo 'checked';}?>>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <center>
                    <a href="../privacy-policy.html" target='_blank' class="btn btn-primary website-button"><i class="fa fa-eye"></i> &nbsp; Preview Page</a>
                </center>
            </div>
        </div>
        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <textarea rows="20" class="form-control privacy_policy_content" name="example"><?php if($count>0 && ($privacy_policy_content!='' || $privacy_policy_content!=null)) echo $privacy_policy_content;?></textarea>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
        <div class="row privacy_policy_status">
            
        </div>
        <div class="row">
            <center><button class="btn btn-success btn_privacy_policy website-button">Save</button></center>
        </div>
    	<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><div class="alert alert-info" style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please tick <strong>"Show on Website"</strong> checkbox if you want to show this page link on website.</div></center>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.privacy_policy_content').richText();

        $('.btn_privacy_policy').click(function(){
        	var content = $('.privacy_policy_content').val();

            if($(".privacy_chk_show").is(':checked'))
            {
                var privacy_chk_show = '1';
            }
            else
            {
                var privacy_chk_show = '0';
            }

        	if(content=='' || content==null)
        	{
        		$('.privacy_policy_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Page Content! </strong> Enter page content for privacy policy page</div></center>");
                return false;
        	}

        	var task = "Update_Privacy_Policy_Page";

        	var data = {
                content : content,
                privacy_chk_show : privacy_chk_show,
                task:task
            }

        	$('.loading_img').show();

        	$.ajax({
                type:'post',
                dataType: 'json',
                data:data,                
                url : 'query/website-content-setting/Privacy_Policy_helper.php',
                success : function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                    	$('.loading_img').hide();
                        $('.privacy_policy_status').html("<center><div class='alert alert-success privacy_policy_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> privacy policy page content updated successfully.</div></center>");
                        
                        $('.privacy_policy_status_success').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes+&sub_tab=Privacy_Policy");
                            return false;
                        });
                    }
                    else
                    {
                        $('.privacy_policy_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>