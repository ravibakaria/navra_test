<?php
    $stmt   = $link->prepare("SELECT * FROM `theme_color_setting`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    if($count>0)
    {
        $primary_color = $result['primary_color'];
        $primary_font_color= $result['primary_font_color'];
        $sidebar_color = $result['sidebar_color'];
        $sidebar_font_color = $result['sidebar_font_color'];
    }
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <label class="control-label">Primary Color:</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                <input type="color" name="primary_color" class="primary_color"  value="<?php echo $primary_color; ?>">
            </div>
            <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    Header & Footer Background Color
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <label class="control-label">Primary Font Color:</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                <input type="color" name="primary_font_color" class="primary_font_color"  value="<?php echo $primary_font_color; ?>">
            </div>
            <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    Header & Footer Font Color
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <label class="control-label">Secondary Color:</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                <input type="color" name="sidebar_color" class="sidebar_color"  value="<?php echo $sidebar_color; ?>">
            </div>
            <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    Secondary Background Color
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            </div>
        </div>

        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <label class="control-label">Secondary Font Color:</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                <input type="color" name="sidebar_font_color" class="sidebar_font_color"  value="<?php echo $sidebar_font_color; ?>">
            </div>
            <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    Secondary Font Color
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            </div>
        </div>
        <hr class="dotted">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 input-row">
            <div class="row">
                <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 color_setup_status">
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <center><button class="btn btn-success btn-sm btn_color_setup website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.btn_color_setup').click(function(){
            var primary_color = $('.primary_color').val();
            var primary_font_color = $('.primary_font_color').val();
            var sidebar_color = $('.sidebar_color').val();
            var sidebar_font_color = $('.sidebar_font_color').val();
            var task = "update_color_setting";

            var data = 'primary_color='+primary_color+'&primary_font_color='+primary_font_color+'&sidebar_color='+sidebar_color+'&sidebar_font_color='+sidebar_font_color+'&task='+task;
            
            $('.loading_img').show();

            $.ajax({
                url : 'query/website-content-setting/Color_setup_helper.php',
                type : 'POST',
                data : data,
                success : function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.color_setup_status').html("<center><div class='alert alert-success color_setup_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Theme color settings updated successfully.</div></center>");
                        
                        $('.color_setup_status_success').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes&sub_tab=Color_Setting");
                            return false;
                        });
                    }
                    else
                    {
                        $('.color_setup_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>