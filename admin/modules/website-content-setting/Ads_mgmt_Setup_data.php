<?php
    $stmt   = $link->prepare("SELECT * FROM `ads_management`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    if($count>0)
    {
        @$leaderBoardDisplay1 = $result['leaderBoardDisplay1'];
        @$leaderBoardData1 = $result['leaderBoardData1'];
        @$leaderBoardDisplay2 = $result['leaderBoardDisplay2'];
        @$leaderBoardData2 = $result['leaderBoardData2'];
        @$leaderBoardDisplay3 = $result['leaderBoardDisplay3'];
        @$leaderBoardData3 = $result['leaderBoardData3'];
        @$squarePopupDisplay1 = $result['squarePopupDisplay1'];
        @$squarePopupData1 = $result['squarePopupData1'];
        @$squarePopupDisplay2 = $result['squarePopupDisplay2'];
        @$squarePopupData2 = $result['squarePopupData2'];
        @$squarePopupDisplay3 = $result['squarePopupDisplay3'];
        @$squarePopupData3 = $result['squarePopupData3'];
        @$skyScrapperDisplay1 = $result['skyScrapperDisplay1'];
        @$skyScrapperData1 = $result['skyScrapperData1'];
        @$skyScrapperDisplay2 = $result['skyScrapperDisplay2'];
        @$skyScrapperData2 = $result['skyScrapperData2'];
       	@$skyScrapperDisplay3 = $result['skyScrapperDisplay3'];
        @$skyScrapperData3 = $result['skyScrapperData3'];
    }
?>

<div class="row input-row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        Leaderboard Ads:
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div class="row">
        	<div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                <div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    <strong>Note: </strong> Recommended size for Leaderboard Ad <b>728px X 90px</b><br/>
            	Below ads will be randomly displayed on website.<br/>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input type="checkbox" name="leaderBoardDisplay1" class="leaderBoardDisplay1" <?php if(@$leaderBoardDisplay1=='1'){ echo "checked"; } ?> value="1"> &nbsp; Leaderboard Ad-1
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input type="checkbox" name="leaderBoardDisplay2" class="leaderBoardDisplay2" <?php if(@$leaderBoardDisplay2=='1'){ echo "checked"; } ?> value="1"> &nbsp; Leaderboard Ad-2
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input type="checkbox" name="leaderBoardDisplay3" class="leaderBoardDisplay3" <?php if(@$leaderBoardDisplay3=='1'){ echo "checked"; } ?> value="1"> &nbsp; Leaderboard Ad-3
            </div>
        </div>
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <textarea class='form-control leaderBoardData1' placeholder="Enter Leaderboard Ad-1 content here" rows="5" <?php if(@$leaderBoardDisplay1!='1'){ echo "style=display:none;"; }?>><?php echo @$leaderBoardData1;?></textarea>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <textarea class='form-control leaderBoardData2' placeholder="Enter Leaderboard Ad-2 content here" rows="5" <?php if(@$leaderBoardDisplay2!='1'){ echo "style=display:none;"; }?>><?php echo @$leaderBoardData2;?></textarea>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <textarea class='form-control leaderBoardData3' placeholder="Enter Leaderboard Ad-3 content here" rows="5" <?php if(@$leaderBoardDisplay3!='1'){ echo "style=display:none;"; }?>><?php echo @$leaderBoardData3;?></textarea>
            </div>
        </div>
    </div>
</div>
<hr/>
<div class="row input-row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        Square Ads:
    </div>
    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
        <div class="row">
        	<div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
        		<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    <strong>Note: </strong> Recommended size for Square pop-up Ad <b>250px X 250px</b><br/>
                	Below ads will be randomly displayed on website.<br/>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input type="checkbox" name="squarePopupDisplay1" class="squarePopupDisplay1" <?php if(@$squarePopupDisplay1=='1'){ echo "checked"; } ?> value="1"> &nbsp; Square Ad-1
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input type="checkbox" name="squarePopupDisplay2" class="squarePopupDisplay2" <?php if(@$squarePopupDisplay2=='1'){ echo "checked"; } ?> value="1"> &nbsp; Square Ad-2
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input type="checkbox" name="squarePopupDisplay3" class="squarePopupDisplay3" <?php if(@$squarePopupDisplay3=='1'){ echo "checked"; } ?> value="1"> &nbsp; Square Ad-3
            </div>
        </div>
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <textarea class='form-control squarePopupData1' placeholder="Enter Square pop-up Ad-1 content here" rows="5" <?php if(@$squarePopupDisplay1!='1'){ echo "style=display:none;"; }?>><?php echo @$squarePopupData1;?></textarea>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <textarea class='form-control squarePopupData2' placeholder="Enter Square pop-up Ad-2 content here" rows="5" <?php if(@$squarePopupDisplay2!='1'){ echo "style=display:none;"; }?>><?php echo @$squarePopupData2;?></textarea>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <textarea class='form-control squarePopupData3' placeholder="Enter Square pop-up Ad-3 content here" rows="5" <?php if(@$squarePopupDisplay3!='1'){ echo "style=display:none;"; }?>><?php echo @$squarePopupData3;?></textarea>
            </div>
        </div>
    </div>
</div>

<hr/>
<div class="row input-row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        Wide Skyscrapper Ads:
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div class="row">
            <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
            	<div class='alert alert-info' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>
                    <span class='fa fa-exclamation-circle'></span> 
                    <strong>Note: </strong> Recommended size for Wide skyscrapper Ad <b>160px X 600px</b><br/>
                	Below ads will be randomly displayed on website.<br/>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input type="checkbox" name="skyScrapperDisplay1" class="skyScrapperDisplay1" <?php if(@$skyScrapperDisplay1=='1'){ echo "checked"; } ?> value="1"> &nbsp; Wide Skyscrapper Ad-1
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input type="checkbox" name="skyScrapperDisplay2" class="skyScrapperDisplay2" <?php if(@$skyScrapperDisplay2=='1'){ echo "checked"; } ?> value="1"> &nbsp; Wide Skyscrapper Ad-2
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input type="checkbox" name="skyScrapperDisplay3" class="skyScrapperDisplay3" <?php if(@$skyScrapperDisplay3=='1'){ echo "checked"; } ?> value="1"> &nbsp; Wide Skyscrapper Ad-3
            </div>
        </div>
    </div>
</div>

<div class="row input-row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <textarea class='form-control skyScrapperData1' placeholder="Enter Skyscrapper Ad-1 content here" rows="5" <?php if(@$skyScrapperDisplay1!='1'){ echo "style=display:none;"; }?>><?php echo @$skyScrapperData1;?></textarea>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <textarea class='form-control skyScrapperData2' placeholder="Enter Skyscrapper Ad-2 content here" rows="5" <?php if(@$skyScrapperDisplay2!='1'){ echo "style=display:none;"; }?>><?php echo @$skyScrapperData2;?></textarea>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <textarea class='form-control skyScrapperData3' placeholder="Enter Skyscrapper Ad-3 content here" rows="5" <?php if(@$skyScrapperDisplay3!='1'){ echo "style=display:none;"; }?>><?php echo @$skyScrapperData3;?></textarea>
            </div>
        </div>
    </div>
</div>
<hr/>
<div class="row input-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 input-row">
        <div class="row">
            <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ad_mgmt_status">
                
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <center><button class="btn btn-success btn-sm btn_ad_mgmt website-button"><i class="fa fa-save"></i> &nbsp;Save</button></center>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){

		/****************    LeaderBoard Ads Start    *****************/
			//LeaderBoard display 1
			$('.leaderBoardDisplay1').change(function(){
	            if($(this).is(':checked'))
	            {
	                var leaderBoardDisplay1 = '1';
	            }
	            else
	            {
	                var leaderBoardDisplay1 = '0';
	            }

	            if(leaderBoardDisplay1=='1')
	            {
	                $('.leaderBoardData1').show();
	                return false;
	            }
	            else
	            {
	                $('.leaderBoardData1').hide();
	                return false;
	            }
	        });

	        //LeaderBoard display 2
			$('.leaderBoardDisplay2').change(function(){
	            if($(this).is(':checked'))
	            {
	                var leaderBoardDisplay2 = '1';
	            }
	            else
	            {
	                var leaderBoardDisplay2 = '0';
	            }

	            if(leaderBoardDisplay2=='1')
	            {
	                $('.leaderBoardData2').show();
	                return false;
	            }
	            else
	            {
	                $('.leaderBoardData2').hide();
	                return false;
	            }
	        });

	        //LeaderBoard display 3
			$('.leaderBoardDisplay3').change(function(){
	            if($(this).is(':checked'))
	            {
	                var leaderBoardDisplay3 = '1';
	            }
	            else
	            {
	                var leaderBoardDisplay3 = '0';
	            }

	            if(leaderBoardDisplay3=='1')
	            {
	                $('.leaderBoardData3').show();
	                return false;
	            }
	            else
	            {
	                $('.leaderBoardData3').hide();
	                return false;
	            }
	        });
        /****************    LeaderBoard Ads End    *****************/

        /****************    Square pop-up Ads Start    *****************/
        	//Square Pop up display 1
			$('.squarePopupDisplay1').change(function(){
	            if($(this).is(':checked'))
	            {
	                var squarePopupDisplay1 = '1';
	            }
	            else
	            {
	                var squarePopupDisplay1 = '0';
	            }

	            if(squarePopupDisplay1=='1')
	            {
	                $('.squarePopupData1').show();
	                return false;
	            }
	            else
	            {
	                $('.squarePopupData1').hide();
	                return false;
	            }
	        });

	        //Square Pop up display 2
			$('.squarePopupDisplay2').change(function(){
	            if($(this).is(':checked'))
	            {
	                var squarePopupDisplay2 = '1';
	            }
	            else
	            {
	                var squarePopupDisplay2 = '0';
	            }

	            if(squarePopupDisplay2=='1')
	            {
	                $('.squarePopupData2').show();
	                return false;
	            }
	            else
	            {
	                $('.squarePopupData2').hide();
	                return false;
	            }
	        });

	        //Square Pop up display 3
			$('.squarePopupDisplay3').change(function(){
	            if($(this).is(':checked'))
	            {
	                var squarePopupDisplay3 = '1';
	            }
	            else
	            {
	                var squarePopupDisplay3 = '0';
	            }

	            if(squarePopupDisplay3=='1')
	            {
	                $('.squarePopupData3').show();
	                return false;
	            }
	            else
	            {
	                $('.squarePopupData3').hide();
	                return false;
	            }
	        });
        /****************    Square pop-up Ads End    *****************/

        /****************    Sktscrapper pop-up Ads Start    *****************/
        	//Skyscrapper display 1
			$('.skyScrapperDisplay1').change(function(){
	            if($(this).is(':checked'))
	            {
	                var skyScrapperDisplay1 = '1';
	            }
	            else
	            {
	                var skyScrapperDisplay1 = '0';
	            }

	            if(skyScrapperDisplay1=='1')
	            {
	                $('.skyScrapperData1').show();
	                return false;
	            }
	            else
	            {
	                $('.skyScrapperData1').hide();
	                return false;
	            }
	        });

	        //Skyscrapper display 2
			$('.skyScrapperDisplay2').change(function(){
	            if($(this).is(':checked'))
	            {
	                var skyScrapperDisplay2 = '1';
	            }
	            else
	            {
	                var skyScrapperDisplay2 = '0';
	            }

	            if(skyScrapperDisplay2=='1')
	            {
	                $('.skyScrapperData2').show();
	                return false;
	            }
	            else
	            {
	                $('.skyScrapperData2').hide();
	                return false;
	            }
	        });

	        //Skyscrapper display 3
			$('.skyScrapperDisplay3').change(function(){
	            if($(this).is(':checked'))
	            {
	                var skyScrapperDisplay3 = '1';
	            }
	            else
	            {
	                var skyScrapperDisplay3 = '0';
	            }

	            if(skyScrapperDisplay3=='1')
	            {
	                $('.skyScrapperData3').show();
	                return false;
	            }
	            else
	            {
	                $('.skyScrapperData3').hide();
	                return false;
	            }
	        });
        /****************    Sktscrapper pop-up Ads End    *****************/


        /*****************     Save button    ******************/
        $('.btn_ad_mgmt').click(function(){

        	/**********  leaderboard ads start   ***********/
        	if($('.leaderBoardDisplay1').is(':checked'))
            {
                var leaderBoardDisplay1 = '1';
            }
            else
            {
                var leaderBoardDisplay1 = '0';
            }

            if($('.leaderBoardDisplay2').is(':checked'))
            {
                var leaderBoardDisplay2 = '1';
            }
            else
            {
                var leaderBoardDisplay2 = '0';
            }

            if($('.leaderBoardDisplay3').is(':checked'))
            {
                var leaderBoardDisplay3 = '1';
            }
            else
            {
                var leaderBoardDisplay3 = '0';
            }
            /**********  leaderboard ads end   ***********/


            /**********  Square popup ads start   ***********/
        	if($('.squarePopupDisplay1').is(':checked'))
            {
                var squarePopupDisplay1 = '1';
            }
            else
            {
                var squarePopupDisplay1 = '0';
            }

            if($('.squarePopupDisplay2').is(':checked'))
            {
                var squarePopupDisplay2 = '1';
            }
            else
            {
                var squarePopupDisplay2 = '0';
            }

            if($('.squarePopupDisplay3').is(':checked'))
            {
                var squarePopupDisplay3 = '1';
            }
            else
            {
                var squarePopupDisplay3 = '0';
            }
            /**********  square popup ads end   ***********/

            /**********  wide skyscrapper ads start   ***********/
        	if($('.skyScrapperDisplay1').is(':checked'))
            {
                var skyScrapperDisplay1 = '1';
            }
            else
            {
                var skyScrapperDisplay1 = '0';
            }

            if($('.skyScrapperDisplay2').is(':checked'))
            {
                var skyScrapperDisplay2 = '1';
            }
            else
            {
                var skyScrapperDisplay2 = '0';
            }

            if($('.skyScrapperDisplay3').is(':checked'))
            {
                var skyScrapperDisplay3 = '1';
            }
            else
            {
                var skyScrapperDisplay3 = '0';
            }
            /**********  wide skyscrapper ads end   ***********/

            var leaderBoardData1 = $('.leaderBoardData1').val();
            var leaderBoardData2 = $('.leaderBoardData2').val();
            var leaderBoardData3 = $('.leaderBoardData3').val();

            var squarePopupData1 = $('.squarePopupData1').val();
            var squarePopupData2 = $('.squarePopupData2').val();
            var squarePopupData3 = $('.squarePopupData3').val();

            var skyScrapperData1 = $('.skyScrapperData1').val();
            var skyScrapperData2 = $('.skyScrapperData2').val();
            var skyScrapperData3 = $('.skyScrapperData3').val();

            var task = "update_ads_mgmt_setup";

            if(leaderBoardDisplay1=='1' && (leaderBoardData1=='' || leaderBoardData1==null))
            {
            	$('.ad_mgmt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter leaderboard 1 script contents.</div></center>");
            	return false;
            }

            if(leaderBoardDisplay2=='1' && (leaderBoardData2=='' || leaderBoardData2==null))
            {
            	$('.ad_mgmt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter leaderboard 2 script contents.</div></center>");
            	return false;
            }

            if(leaderBoardDisplay3=='1' && (leaderBoardData3=='' || leaderBoardData3==null))
            {
            	$('.ad_mgmt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter leaderboard 3 script contents.</div></center>");
            	return false;
            }

            if(squarePopupDisplay1=='1' && (squarePopupData1=='' || squarePopupData1==null))
            {
            	$('.ad_mgmt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter square Pop up 1 script contents.</div></center>");
            	return false;
            }

            if(squarePopupDisplay2=='1' && (squarePopupData2=='' || squarePopupData2==null))
            {
            	$('.ad_mgmt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter square Pop up 2 script contents.</div></center>");
            	return false;
            }

            if(squarePopupDisplay3=='1' && (squarePopupData3=='' || squarePopupData3==null))
            {
            	$('.ad_mgmt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter square Pop up 3 script contents.</div></center>");
            	return false;
            }

            if(skyScrapperDisplay1=='1' && (skyScrapperData1=='' || skyScrapperData1==null))
            {
            	$('.ad_mgmt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter sky Scrapper 1 script contents.</div></center>");
            	return false;
            }


            if(skyScrapperDisplay2=='1' && (skyScrapperData2=='' || skyScrapperData2==null))
            {
            	$('.ad_mgmt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter sky Scrapper 2 script contents.</div></center>");
            	return false;
            }


            if(skyScrapperDisplay3=='1' && (skyScrapperData3=='' || skyScrapperData3==null))
            {
            	$('.ad_mgmt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please enter sky Scrapper 3 script contents.</div></center>");
            	return false;
            }

            var data = {
                leaderBoardDisplay1 : leaderBoardDisplay1,
                leaderBoardData1 : leaderBoardData1,
                leaderBoardDisplay2 : leaderBoardDisplay2,
                leaderBoardData2 : leaderBoardData2,
                leaderBoardDisplay3 : leaderBoardDisplay3,
                leaderBoardData3 : leaderBoardData3,
                squarePopupDisplay1 : squarePopupDisplay1,
                squarePopupData1 : squarePopupData1,
                squarePopupDisplay2 : squarePopupDisplay2,
                squarePopupData2 : squarePopupData2,
                squarePopupDisplay3 : squarePopupDisplay3,
                squarePopupData3 : squarePopupData3,
                skyScrapperDisplay1 : skyScrapperDisplay1,
                skyScrapperData1 : skyScrapperData1,
                skyScrapperDisplay2 : skyScrapperDisplay2,
                skyScrapperData2 : skyScrapperData2,
                skyScrapperDisplay3 : skyScrapperDisplay3,
                skyScrapperData3 : skyScrapperData3,
                task:task
            }

            $.ajax({
            	type:'post',
                dataType: 'json',
                data:data,                
                url:'query/website-content-setting/ads_mgmt_setup_helper.php',
            	success:function(res)
            	{
            		if(res=='success')
            		{
            			$('.ad_mgmt_status').html("<center><div class='alert alert-success about_us_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span> Data successfullly updated.</div></center>");

            			$('.about_us_status_success').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes+&sub_tab=Ads_mgmt");
                            return false;
                        });
            			return false;
            		}
            		else
            		{
            			$('.ad_mgmt_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-times'></span><strong>Error! </strong> "+res+"</div></center>");
            			return false;
            		}
            	}
            });
        });
	});
</script>