<?php
    $stmt   = $link->prepare("SELECT * FROM `terms_of_service`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    if($count>0)
    {
    	$terms_of_service_content = $result['content'];
        $terms_chk_show = $result['display'];
    }
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div  class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label class="control-label">Page Content:</label>
                </div>
                <div  class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    Show on Website: &nbsp; <input type="checkbox" class="terms_chk_show" <?php if($count>0 && ($terms_chk_show=='1')) { echo 'checked';}?>>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <center>
                    <a href="../terms-of-service.html" target='_blank' class="btn btn-primary website-button"><i class="fa fa-eye"></i> &nbsp; Preview Page</a>
                </center>
            </div>
        </div>
        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <textarea rows="20" class="form-control terms_of_service_content" name="example"><?php if($count>0 && ($terms_of_service_content!='' || $terms_of_service_content!=null)) echo $terms_of_service_content;?></textarea>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
        <div class="row terms_of_service_status">
            
        </div>
        <div class="row">
            <center><button class="btn btn-success btn_terms_of_service website-button">Save</button></center>
        </div>
    	<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><div class="alert alert-info" style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please tick <strong>"Show on Website"</strong> checkbox if you want to show this page link on website.</div></center>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.terms_of_service_content').richText();

        $('.btn_terms_of_service').click(function(){
        	var content = $('.terms_of_service_content').val();

            if($(".terms_chk_show").is(':checked'))
            {
                var terms_chk_show = '1';
            }
            else
            {
                var terms_chk_show = '0';
            }

            
        	if(content=='' || content==null)
        	{
        		$('.terms_of_service_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Page Content! </strong> Enter page content for terms of service page</div></center>");
                return false;
        	}

        	var task = "Update_Terms_Of_Service_Page";

        	var data = {
                content : content,
                terms_chk_show : terms_chk_show,
                task:task
            }

        	$('.loading_img').show();

        	$.ajax({
                type:'post',
                dataType: 'json',
                data:data,                
                url : 'query/website-content-setting/Terms_Of_Service_helper.php',
                success : function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                    	$('.loading_img').hide();
                        $('.terms_of_service_status').html("<center><div class='alert alert-success terms_of_service_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Terms of service page content updated successfully.</div></center>");
                        
                        $('.terms_of_service_status_success').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes+&sub_tab=Terms_Of_Service");
                            return false;
                        });
                    }
                    else
                    {
                        $('.terms_of_service_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>