<?php
    $stmt   = $link->prepare("SELECT * FROM `disclaimer`");
    $stmt->execute();
    $result = $stmt->fetch();
    $count=$stmt->rowCount();

    if($count>0)
    {
    	$disclaimer_content = $result['content'];
        $disclaimer_chk_show = $result['display'];
    }
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div  class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label class="control-label">Page Content:</label>
                </div>
                <div  class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    Show on Website: &nbsp; <input type="checkbox" class="disclaimer_chk_show" <?php if($count>0 && ($disclaimer_chk_show=='1')) { echo 'checked';}?>>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <center>
                    <a href="../disclaimer.html" target='_blank' class="btn btn-primary website-button"><i class="fa fa-eye"></i> &nbsp; Preview Page</a>
                </center>
            </div>
        </div>
        <div class="row input-row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <textarea rows="20" class="form-control disclaimer_content" name="example"><?php if($count>0 && ($disclaimer_content!='' || $disclaimer_content!=null)) echo $disclaimer_content;?></textarea>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
        <div class="row disclaimer_status">
            
        </div>
        <div class="row">
            <center><button class="btn btn-success btn_disclaimer website-button">Save</button></center>
        </div>
    	<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <center><div class="alert alert-info" style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span> Please tick <strong>"Show on Website"</strong> checkbox if you want to show this page link on website.</div></center>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.disclaimer_content').richText();

        $('.btn_disclaimer').click(function(){
        	var content = $('.disclaimer_content').val();

            if($(".disclaimer_chk_show").is(':checked'))
            {
                var disclaimer_chk_show = '1';
            }
            else
            {
                var disclaimer_chk_show = '0';
            }

        	if(content=='' || content==null)
        	{
        		$('.disclaimer_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Page Content! </strong> Enter page content for disclaimer page</div></center>");
                return false;
        	}

        	var task = "Update_Disclaimer_Page";

        	var data = {
                content : content,
                disclaimer_chk_show : disclaimer_chk_show,
                task:task
            }

        	$('.loading_img').show();

        	$.ajax({
                type:'post',
                dataType: 'json',
                data:data,                
                url : 'query/website-content-setting/Disclaimer_helper.php',
                success : function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                    	$('.loading_img').hide();
                        $('.disclaimer_status').html("<center><div class='alert alert-success disclaimer_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Disclaimer page content updated successfully.</div></center>");
                        
                        $('.disclaimer_status_success').fadeTo(1000, 500).slideUp(500, function(){
                            window.location.assign("website-content-setup.php?main_tab=WebsiteContentAttributes+&sub_tab=Disclaimer");
                            return false;
                        });
                    }
                    else
                    {
                        $('.contact_us_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });

        });
    });
</script>