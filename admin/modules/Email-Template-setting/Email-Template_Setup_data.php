<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h2>Email Templates</h2>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Email-Template">Add New Email Templates</button>
            </div>
        </div>
        <div class="row">
            <div>
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered Template_list' style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Template Name</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                <center>
            </div>
           
            <!-- Modal -->
            <div class="modal fade" id="Add-New-Email-Template" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered add-item-model-template" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Email Template</h4></center>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    <lable class="control-label">Template Name:</lable>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm emailTemplateName" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    <lable class="control-label">Subject:</lable>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm emailTemplateSubject" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    <lable class="control-label">Message:</lable>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                    <textarea rows="3" class="emailTemplateMessage" name="emailTemplateMessage"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_email_template_status">
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color:black;">
                                    <center><h4>Variables Used for template. {variable_name}: Description</h4></center>
                                    <?php
                                        $sql = "SELECT * FROM `emailtemplatevariables`";
                                        $stmt = $link->prepare($sql);
                                        $stmt->execute();
                                        $result = $stmt->fetchAll();
                                    
                                        $i=1;
                                        foreach ($result as $row) 
                                        {
                                            if($i=='1' || ($i-1)%3==0)
                                            {
                                                echo "<div class='row'>";
                                            }

                                            echo "<div class='col-md-4 col-lg-4'>";
                                                echo $row['name'];
                                            echo "</div>";
                                            $i = $i+1;

                                            if(($i-1)%3==0)
                                            {
                                                echo "</div>";
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn_add_new_template website-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.emailTemplateMessage').richText();

        var dataTable_Member_list = $('.Template_list').DataTable({          //Member datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            
            "ajax":{
                url :"datatables/email-template/template-list-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".Template_list_processing").css("display","none");
                }
            }
        });

        $('.btn_add_new_template').click(function(){
            var emailTemplateName = $('.emailTemplateName').val();
            var emailTemplateSubject = $('.emailTemplateSubject').val();
            var emailTemplateMessage = $('.emailTemplateMessage').val();

            var task = "Add-New-Email-Template";

            if(emailTemplateName=='' || emailTemplateName==null)
            {
                $('.add_email_template_status').html("<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'><center><span class='fa fa-exclamation-triangle'><strong>Empty data ! </strong> Please select template name.</center></div>");
                return false;
            }

            if(emailTemplateSubject=='' || emailTemplateSubject==null)
            {
                $('.add_email_template_status').html("<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'><center><span class='fa fa-exclamation-triangle'><strong>Empty data ! </strong> Please enter subject for email.</center></div>");
                return false;
            }

            if(emailTemplateMessage=='' || emailTemplateMessage==null)
            {
                $('.add_email_template_status').html("<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'><center><span class='fa fa-exclamation-triangle'><strong>Empty data ! </strong> Please enter message for email.</center></div>");
                return false;
            }

            var data = {
                emailTemplateName : emailTemplateName,
                emailTemplateSubject : emailTemplateSubject,
                emailTemplateMessage : emailTemplateMessage,
                task : task
            };

            $('.loading_img').show();

            $.ajax({
                type:'post',
                dataType:'json',
                data:data,
                url:'query/email-template-data/Email-Template-Setup-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $(this).attr('disabled',true);

                        $('.add_email_template_status').html("<div class='alert alert-success emailTemplate_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</center></div>");
                            $('.emailTemplate_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("email-template-setup.php?main_tab=EmailTemplateAttributes&sub_tab=EmailTemplate_Setup");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_email_template_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Error! </strong><br/> "+res+"</center></div>");
                        return false;
                    }
                }
            });
        });
    });
</script>