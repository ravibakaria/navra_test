<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row div-header-row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h2>User Setting</h2>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                <button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Admin">Add New Admin/User</button>
            </div>
        </div>
        <div class="row">
            <div style="">
                <center>
                    <table id='datatable-info' class='table-hover table-striped table-bordered Admin_list' style="width:100%;">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>User Name</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email Id</th>
                                <th>Department</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                <center>
            </div>
            
            <!-- Modal -->
            <div class="modal fade admin_add_model" id="Add-New-Admin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <center><h4 class="modal-title" id="exampleModalLongTitle">Add New Admin</h4></center>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Admin User Name:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm Admin_username" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">First Name:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm Admin_firstname" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Last Name:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm Admin_lastname" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label"> Email Id:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type="text" class="form-control form-control-sm Admin_email" />
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label"> Department:</lable>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <?php
                                        $sql_dept = "SELECT * FROM `departments` WHERE `status`='1'";
                                        $stmt = $link->prepare($sql_dept);
                                        $stmt->execute();
                                        $resullt = $stmt->fetchAll();


                                        foreach ($resullt as $row) 
                                        {
                                            $dept_id = $row['id'];
                                            $dept_name = $row['name'];

                                            echo "<input type='checkbox' name='Admin_departments[]' class='Admin_departments' value='$dept_id' />&nbsp;&nbsp;".$dept_name."&nbsp;&nbsp;&nbsp;";
                                            
                                        }
                                    ?>                   
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <lable class="control-label">Status:</lable>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
                                    <input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='form-control  User_status' name='Admin_status' checked>
                                </div>
                            </div>

                            <div class="row input-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add_status_Admin">
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn_add_new_Admin website-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

         $('#Admin_departments').multiselect({
            includeSelectAllOption: true,
            maxHeight: 400,
            dropUp: true
        });


        var dataTable_User_list = $('.Admin_list').DataTable({          //Admin datatable
            "bProcessing": true,
            "serverSide": true,
            //"stateSave": true,
            
            "ajax":{
                url :"datatables/user-attributes/Admin-response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".Admin_list_processing").css("display","none");
                }
            }
        });

        //Admin change status
        $('.Admin_list tbody').on('click', '.change_status_Admin', function(){
            var status_value = $(this).attr('id');
            var myarray = status_value.split(',');

            var status = myarray[0];
            var id = myarray[1];

            var task = "Change_Admin_status";

            var data = 'status='+status+'&id='+id+'&task='+task;
            //alert(data);return false;
            $.ajax({
                type:'post',
                data:data,
                url:'query/user-attributes/Admin-helper.php',
                success:function(res)
                {
                    if(res=='success')
                    {
                        $('.Admin_status'+id).html("<div class='alert alert-success Admin_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Status updated successfully.</div>");
                            $('.Admin_success_status').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("user-setup.php?main_tab=UserAttributes&sub_tab=User_Setup");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.Admin_status'+id).html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
                        return false;
                    }
                }
            });
        });

        //Admin add button click
        $('.btn_add_new_Admin').click(function(){
            var username = $('.Admin_username').val();
            var firstname = $('.Admin_firstname').val();
            var lastname = $('.Admin_lastname').val();
            var email = $('.Admin_email').val();
            //var department = $('.Admin_departments').val();
            var status = $('.Admin_status').is(":checked");
            var task = "Add_New_Admin";

            var department = [];
            $('input.Admin_departments:checked').each(function() {
                department.push(this.value);
            });

            if(status==true)
            {
                status='1';
            }
            else
            if(status==false)
            {
                status='0';
            }

            if(username=='' || username==null)
            {
                $('.add_status_Admin').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter admin user name.</div></center>");
                return false;
            }

            if(firstname=='' || firstname==null)
            {
                $('.add_status_Admin').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter admin first name.</div></center>");
                return false;
            }

            if(lastname=='' || lastname==null)
            {
                $('.add_status_Admin').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter admin last name.</div></center>");
                return false;
            }

            if(email=='' || email==null)
            {
                $('.add_status_Admin').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter admin email id.</div></center>");
                return false;
            }

            function validateEmail($email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $email );
            }

            if( !validateEmail(email)) 
            { 
                $('.add_status_Admin').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Email id not valid.</div></center>");
                return false;
            }

            if(department==null || department=='')
            { 
                $('.add_status_Admin').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty! </strong> Select department to which admin belong it.</div></center>");
                return false;
            }

            $('.loading_img').show();
            
            var data = 'username='+username+'&firstname='+firstname+'&lastname='+lastname+'&email='+email+'&department='+department+'&status='+status+'&task='+task;
            

            $.ajax({
                type:'post',
                data:data,
                url:'query/user-attributes/Admin-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.add_status_Admin').html("<center><div class='alert alert-success add_status_Admin_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Admin Added Successfully.</div></center>");
                            $('.add_status_Admin_success').fadeTo(1000, 500).slideUp(500, function(){
                                  window.location.assign("user-setup.php?main_tab=UserAttributes&sub_tab=User_Setup");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.add_status_Admin').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
                        return false;
                    }
                }
            });
        });
    });
</script>