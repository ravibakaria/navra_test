<?php
	include("templates/header.php");
	if(isset($_GET['main_tab']) && isset($_GET['sub_tab']))
	{
		$main_tab=quote_smart($_GET['main_tab']);
		$sub_tab=quote_smart($_GET['sub_tab']);
	}
	else
	{
		$main_tab=null;
		$sub_tab=null;
	}

	$search_user_id = $_GET['id'];
	$user_id = $_GET['chat_user_id'];
?>

<html>
<head>
	<title>
		Member Messages -  <?php echo getWebsiteTitle(); ?>
	</title>
	<link rel="stylesheet" href="../css/chat_message.css" />
	
</head>
<body>
	<section role="main" class="content-body update-section">
		<a href="member-profile.php?id=<?php echo $user_id; ?>" id="portletReset" type="button" class="mb-xs mt-xs mr-xs btn btn-default" style="float:right;"><i class="fa fa-arrow-left"></i> Back</a>

		<div class="row admin_start_section">
			<!--button class="btn btn-info show_message">Show</button-->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 message_chat_div">
				<center><h1>Member Chat Messages</h1><center>
				<?php
					if (!is_numeric($search_user_id) || !is_numeric($user_id))
					{
						echo "ERROR : Invalid parameter value";
						exit;
					}
				?>
				<input type="hidden" class="user_id" value="<?php echo $user_id;?>">
			   	<input type="hidden" class="search_user_id" value="<?php echo $search_user_id;?>">

				<div class="chat_area">

				</div><!--chat_area-->
			</div>
		</div>
	</section>
</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		var user_id = $('.user_id').val();
		var search_user_id = $('.search_user_id').val();
		var task = "Get_Chat_Message";
		
		var data = 'user_id='+user_id+'&search_user_id='+search_user_id+'&task='+task;

		//alert(data);return false;
		$.ajax({
			type:'post',
        	data:data,
        	url:'query/member-attributes/get_messages.php',
        	success:function(res)
        	{
        		$('.chat_area').html(res);
        	}
        });
	});
</script>