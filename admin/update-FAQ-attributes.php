<?php
	include("templates/header.php");
?>

<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	
</head>
<body>

	<section role="main" class="content-body">
		<header class="page-header">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Update FAQ Information</span></li>
			</ol>
			
			<ol class="breadcrumbs" style="margin-left: 791px !important;">
				<li><span><a href="<?php echo $previous_page_url; ?>"><span class="fa fa-arrow-left"></a></span>&nbsp;<a href="<?php echo $previous_page_url; ?>" style="margin-top:2px;text-decoration:none;">&nbsp;Back</a>
				</li>
			</ol>
		</header>

		<?php
			$id = quote_smart($_GET['id']);

			if ( !is_numeric( $id ) )
		    {
		        echo 'Error! Invalid Data';
		        exit;
		    }

		    $sql = "SELECT * FROM `faq` WHERE `id`='$id'";
		    $stmt1   = $link->prepare($sql);
            $stmt1->execute();
            $queryTot = $stmt1->rowCount();
            $result = $stmt1->fetch();

           	if($queryTot>0)
           	{
           		$faq_question = $result['faq_question'];
           		$faq_answer = $result['faq_answer'];
           		$status = $result['status'];
		?>
		<br/><br/>
		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-md-2 col-lg-2">
					</div>
					<div class="col-md-8 col-lg-8 edit-master-div">
						<div class="row">
							<center><h4>Update FAQ Information</h4></center>
						</div>
						<hr/>

						<div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                                <lable class="control-label">Question:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-10 col-lg-10">
                                <input type="text" class="form-control form-control-sm FAQ_question" value="<?php echo $faq_question;?>" />
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                                <lable class="control-label">Answer:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-10 col-lg-10">
                            	<textarea class="form-control form-control-sm FAQ_answer" rows="5"><?php echo $faq_answer;?></textarea>
                            </div>
                        </div>

                        <div class="row input-row">
                            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                                <lable class="control-label">Status:</lable>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-10 col-lg-10">
                                <?php
									if($status=='0')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='FAQ_current_status' name='FAQ_current_status' value='".$status."'>";
									}
									else
									if($status=='1')
									{
										echo "<input type='checkbox' data-toggle='toggle' data-on='Active' data-off='Disabled' class='FAQ_current_status' name='FAQ_current_status' checked value='".$status."'>";
									}
								?>
                            </div>
                        </div>
						<input type="hidden" class="FAQ_id" value="<?php echo $id;?>">

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 update_status">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
							</div>	
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><button class="btn btn-success btn_update_FAQ website-button">Update Information</button></center><br/>	
							</div>
						</div>
						
					</div>
					<div class="col-md-2 col-lg-2">
					</div>
				</div>
				<div class="row">
					<br/>
				</div>
			</div>
			
		</div>
		<?php
			}
			else
			{
				echo "<center><h3 class='danger_error'>Sorry! No record found for this id.</h3></center>";
			}
		?>
</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){

		$('.btn_update_FAQ').click(function(){
			var faq_question = $('.FAQ_question').val();
			var faq_answer = $('.FAQ_answer').val();
            var status = $('.FAQ_current_status').is(":checked");
            var id = $('.FAQ_id').val();
			var task = "Update_FAQ_Attributes";

			if(status==true)
			{
				status='1';
			}
			else
			if(status==false)
			{
				status='0';
			}

			if(faq_question=='' || faq_question==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter question.</div></center>");
                return false;
            }

			if(faq_answer=='' || faq_answer==null)
            {
                $('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty data!</strong> Please enter answer.</div></center>");
                return false;
            }

			var data = {
                faq_question : faq_question,
                faq_answer : faq_answer,
                status : status,
                id : id,
                task:task
            }
			
			$('.loading_img').show();
			
			$.ajax({
				type:'post',
                dataType: 'json',
                data:data,                
                url:'query/website-content-setting/FAQ_helper.php',
	        	success:function(res)
	        	{
	        		$('.loading_img').hide();
	        		if(res=='success')
	        		{
	        			$('.update_status').html("<center><div class='alert alert-success update_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Data updated successfully.</div></center>");
	        		}
	        		else
	        		{
	        			$('.update_status').html("<center><div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div></center>");
	        			return false;
	        		}
	        	}
	    	});
		});
	});
</script>