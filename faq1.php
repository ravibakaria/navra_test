<?php
	session_start();
  if(isset($_SESSION['last_page_accessed']))
  {
    unset($_SESSION['last_page_accessed']);
    unset($_SESSION['membership_plan_id']);
    unset($_SESSION['featured_listing']);
    unset($_SESSION['validity']);
    unset($_SESSION['secret_key']);
  }
  include("layout/header.php");      // Header

	$stmt   = $link->prepare("SELECT * FROM `faq` WHERE `status`='1'");
  $stmt->execute();
  $result = $stmt->fetchAll();
  $count=$stmt->rowCount();

?>

<!-- meta info  -->

  <title>
    Frequently Asked Questions  -  <?php echo getWebsiteTitle(); ?>
  </title>

    <meta name="title" content="<?= $site_title.' - Frequently Asked Questions'; ?>" />

    <meta name="description" content="To find Verified Profiles, Register Free! If you are Looking For Groom or Bride – we have a perfect match for you."/>

    <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

    <meta property="og:title" content="<?php echo $site_title.' - Frequently Asked Questions'; ?>" />
    <meta property="og:url" content="<?= $site_url; ?>" />
    <meta property="og:description" content="<?php echo $site_tagline;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
    <meta property="twitter:title" content="<?= $site_title.' - Frequently Asked Questions'; ?>" />
    <meta property="twitter:url" content="<?= $site_url; ?>" />
    <meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

  <style>
    /***********************************************/
    /***************** Accordion ********************/
    /***********************************************/
    @import url('https://fonts.googleapis.com/css?family=Tajawal');

    section{
      padding: 10px 0;
    }

    
    #accordion-style-1 .btn-link {
        font-weight: 400;
        color: #000000;
        background-color: transparent;
        text-decoration: none !important;
        font-size: 16px;
        font-weight: bold;
      padding-left: 25px;
    }

    #accordion-style-1 .card-body {
        border-top: 2px solid #ab47bc;
    }

    #accordion-style-1 .card-header .btn.collapsed .fa.main{
      display:none;
    }

    #accordion-style-1 .card-header .btn .fa.main{
      background: #ab47bc;
        padding: 13px 11px;
        color: #ffffff;
        width: 35px;
        height: 41px;
        position: absolute;
        left: -1px;
        top: 10px;
        border-top-right-radius: 7px;
        border-bottom-right-radius: 7px;
      display:block;
    }

    .faq-data {
      margin-left: 32px;
      margin-right: -15px;
    }
  </style>
<?php
  include("layout/styles.php"); 
  include("layout/menu.php"); 
?>
<section class="blank_section">
  <br/><br/><br/> 
</section>
<!-- Main Page Content Shown Here  -->
  <div class="container-fluid bg-gray" id="accordion-style-1">
    <div class="container">
      <section>
        <div class="row">
          <div class="col-12">
            <h1 class="text-center">Frequently Asked Questions </h1>
          </div>
          <br/>
          <div class="col-10 mx-auto faq-data">
            <div class="accordion" id="accordionExample">
              <?php
                $i = 1;
                foreach ($result as $row) 
                {
                  $id = $row['id'];
                  $faq_question = $row['faq_question'];
                  $faq_answer = $row['faq_answer'];
                  echo "<div class='card card-faq'>
                          <div class='card-header' id='headingOne'>
                            <h2 class='mb-0'>
                          <button class='btn btn-link btn-block text-left' type='button' data-toggle='collapse' data-target='#collapse$id' aria-expanded='true' aria-controls='collapse$id'>
                            <i class='fa fa-angle-double-right mr-3'></i>$faq_question
                          </button>
                              </h2>
                          </div>

                          <div id='collapse$id' class='collapse show fade' aria-labelledby='headingOne' data-parent='#accordionExample'>
                            <div class='card-body'>
                              $faq_answer
                            </div>
                          </div>
                        </div>";
                        echo "<br/>";
                  $i = $i+1;
                }
              ?>
            </div>
        </div>      
      </section>
    </div>
  </div>
</div>
<!-- // Accordion -->
    </div>      
  </div>
</body>
<?php
	include "layout/footer.php";
?>