<?php
	$profile_id = $_GET['profile_id'];
?>
	<title>
		<?php
			echo "Advanced Matrimonial Profile Search - ".getWebsiteTitle();
		?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?> Advanced Matrimonial Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>

<section role="main" class="content-body">
	<header class="page-header">
		<ol class="breadcrumbs">
			<li>
				<a href="index.php">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Advanced Search</span></li>
		</ol>
		
		
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<!-- <li><span><a href="<?php echo $previous_page_url; ?>"><span class="fa fa-arrow-left">&nbsp;Back</span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></li>-->
			</ol>
		</div>
	</header>

	<!-- start: page -->
	<section class="content-with-menu content-with-menu-has-toolbar media-gallery">
	<div class="content-with-menu-container">
		<div class="inner-menu-toggle">
			<a href="#" class="inner-menu-expand upload_a apply_filter" data-open="inner-menu">
			Apply Filter &nbsp;&nbsp;<i class="fa fa-filter"></i>
			</a>
		</div>

		<menu id="content-menu" class="inner-menu" role="menu">
			<div class="nano">
				<div class="nano-content">

					<div class="inner-menu-toggle-inside">
						<a href="#" class="inner-menu-collapse">
						<i class="fa fa-chevron-up visible-xs-inline"></i><i class="fa fa-chevron-left hidden-xs-inline"></i> Cancel
						</a>
						<a href="#" class="inner-menu-expand" data-open="inner-menu">
						Apply Filter<i class="fa fa-chevron-down"></i>
						</a>
					</div>
					<div class="inner-menu-content">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-xs-12 ">
								<strong class="label_adv_search">Profile Id</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								 <input type="text" name="profile_id" value="<?php echo $profile_id;?>" class="form-control profile_id"/> 
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<button class="btn btn-info btn_profile_id">Search</button>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-xs-12 ">
								<strong class="label_adv_search">Looking For</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								 <input type="radio" name="gender" value="1" class="radio_1 gender" <?php if($gender=='1') echo 'checked';?>/> Groom
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								 <input type="radio" name="gender" value="2"  class="radio_1 gender" <?php if($gender=='2') echo 'checked';?>/> Bride
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-xs-12 ">
								<strong class="label_adv_search">Marital Status</strong> 
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<input type="checkbox" name="marital_status[]" <?php if(in_array(1,$marital_status)){ echo "checked"; } ?> class="radio_1 marital_status" value="1" /> Never Married 
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<input type="checkbox" name="marital_status[]" <?php if(in_array(2,$marital_status)){ echo "checked"; } ?> class="radio_1 marital_status" value="2" /> Awaiting Divorce 
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<input type="checkbox" name="marital_status[]" <?php if(in_array(3,$marital_status)){ echo "checked"; } ?> class="radio_1 marital_status" value="3" /> Divorced 
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<input type="checkbox" name="marital_status[]" <?php if(in_array(4,$marital_status)){ echo "checked"; } ?> class="radio_1 marital_status" value="4" /> Widowed 
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<input type="checkbox" name="marital_status[]" <?php if(in_array(5,$marital_status)){ echo "checked"; } ?> class="radio_1 marital_status" value="5" /> Annulled 
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-xs-12 ">
								<strong class="label_adv_search">Age</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm from_date age_form_element" name="from_date">
										<option value="0">--Minimum Age--</option>
										<?php
											for($i=18;$i<61;$i++)
											{
												if($from_date==$i)
												{
													echo "<option value='".$i."' selected>".$i."</option>";
												}
												else
												{
													echo "<option value='".$i."'>".$i."</option>";
												}
											}
										?>
										<option value="61">60+</option>
									</select>
								</center>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm to_date age_form_element" name="to_date">
										<option value="0">--Maximum Age--</option>
										<?php
											for($i=18;$i<61;$i++)
											{
												if($to_date==$i)
												{
													echo "<option value='".$i."' selected>".$i."</option>";
												}
												else
												{
													echo "<option value='".$i."'>".$i."</option>";
												}
											}
										?>
										<option value="61">60+</option>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">Religion</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm religion" name="religion">
										<option value="0">--Religion--</option>
										<?php
											$query  = "SELECT * FROM `religion` ORDER BY `id` ASC";
											$stmt   = $link->prepare($query);
											$stmt->execute();
											$result = $stmt->fetchAll();
											foreach( $result as $row )
											{
												$religion_name =  $row['name'];
												$religion_id = $row['id']; 

												if($religion_id==$religion)
												{
													echo "<option value='".$religion_id."' selected>".$religion_name."</option>";
												}
												else
												{
													echo "<option value='".$religion_id."'>".$religion_name."</option>";
												}
											}
						              	?>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">Caste</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm caste" name="caste">
										<option value="0">--Caste--</option>
										<?php
						              		$query  = "SELECT * FROM `caste` ORDER BY `id` ASC";
											$stmt   = $link->prepare($query);
											$stmt->execute();
											$result = $stmt->fetchAll();
											foreach( $result as $row )
											{
												$caste_id = $row['id'];
												$caste_name = $row['name'];
												if($caste_id==$caste)
												{
													echo "<option value='".$caste_id."' selected>".$caste_name."</option>";
												}
												else
												{
													echo "<option value='".$caste_id."'>".$caste_name."</option>";
												}
											}
						              	?>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">Mother Tongue</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm mother_tongue" name="mother_tongue">
										<option value="0">--Mother Tongue--</option>
										<?php
						              		$query  = "SELECT * FROM `mothertongue` ORDER BY `id` ASC";
											$stmt   = $link->prepare($query);
											$stmt->execute();
											$result = $stmt->fetchAll();
											foreach( $result as $row )
											{
												$mothertongue_name =  $row['name'];
												$mothertongue_id = $row['id'];
												if($mothertongue_id==$mother_tongue)
												{
													echo "<option value='".$mothertongue_id."' selected>".$mothertongue_name."</option>";
												}
												else
												{
													echo "<option value='".$mothertongue_id."'>".$mothertongue_name."</option>";
												}
											}
						              	?>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">Education</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm education" name="education">
										<option value="0">--Education--</option>
										<?php
						              		$query  = "SELECT * FROM `educationname` ORDER BY `id` ASC";
											$stmt   = $link->prepare($query);
											$stmt->execute();
											$result = $stmt->fetchAll();
											foreach( $result as $row )
											{
												$education_name =  $row['name'];
												$education_id = $row['id']; 
												if($education == $education_id)
												{
													echo "<option value='".$education_id."' selected>".$education_name."</option>";
												}
												else
												{
													echo "<option value=".$education_id.">".$education_name."</option>";
												}
											}
						              	?>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">Occupation</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm occupation" name="occupation">
										<option value="0">--Occupation--</option>
										<?php
						              		$query  = "SELECT * FROM `employment` ORDER BY `id` ASC";
											$stmt   = $link->prepare($query);
											$stmt->execute();
											$result = $stmt->fetchAll();
											foreach( $result as $row )
											{
												$employment_name =  $row['name'];
												$employment_id = $row['id']; 

												if($occupation==$employment_id)
												{
													echo "<option value=".$employment_id.">".$employment_name."</option>";
												}
												else
												{
													echo "<option value=".$employment_id.">".$employment_name."</option>";
												}
											}
						              	?>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">Income Currency</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm currency" name="currency">
										<option value="0">--Income Currency--</option>
										<?php
						              		$query  = "SELECT * FROM `currency` ORDER BY `id` ASC";
											$stmt   = $link->prepare($query);
											$stmt->execute();
											$result = $stmt->fetchAll();
											foreach( $result as $row )
											{
												$currency_name =  $row['currency']." [".$row['code']."]";
												$currency_id = $row['id']; 

												if($currency==$currency_id)
												{
													echo "<option value='".$currency_id."' selected>".$currency_name."</option>";
												}
												else
												{
													echo "<option value='".$currency_id."'>".$currency_name."</option>";
												}
											}
						              	?>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">Minimum Monthly Income</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm monthly_income_from_filtered" name="monthly_income_from_filtered">
										<option value="0">--Minimum Income--</option>
										<option value="1000">1000</option>
										<option value="2000">2000</option>
										<option value="3000">3000</option>
										<option value="4000">4000</option>
										<option value="5000">5000</option>
										<option value="6000">6000</option>
										<option value="7000">7000</option>
										<option value="8000">8000</option>
										<option value="9000">9000</option>
										<option value="10000">10000</option>
										<option value="20000">20000</option>
										<option value="30000">30000</option>
										<option value="40000">40000</option>
										<option value="50000">50000</option>
										<option value="60000">60000</option>
										<option value="70000">70000</option>
										<option value="80000">80000</option>
										<option value="90000">90000</option>
										<option value="100000">100000</option>
										<option value="200000">200000</option>
										<option value="300000">300000</option>
										<option value="400000">400000</option>
										<option value="500000">500000</option>
										<option value="600000">600000</option>
										<option value="700000">700000</option>
										<option value="800000">800000</option>
										<option value="900000">900000</option>
										<option value="1000000">1000000</option>
										<option value="1000001">Above 1000000</option>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">Maximum Monthly Income</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm monthly_income_to_filtered" name="monthly_income_to_filtered">
										<option value="0">--Maximum Income--</option>
										<option value="1000">1000</option>
										<option value="2000">2000</option>
										<option value="3000">3000</option>
										<option value="4000">4000</option>
										<option value="5000">5000</option>
										<option value="6000">6000</option>
										<option value="7000">7000</option>
										<option value="8000">8000</option>
										<option value="9000">9000</option>
										<option value="10000">10000</option>
										<option value="20000">20000</option>
										<option value="30000">30000</option>
										<option value="40000">40000</option>
										<option value="50000">50000</option>
										<option value="60000">60000</option>
										<option value="70000">70000</option>
										<option value="80000">80000</option>
										<option value="90000">90000</option>
										<option value="100000">100000</option>
										<option value="200000">200000</option>
										<option value="300000">300000</option>
										<option value="400000">400000</option>
										<option value="500000">500000</option>
										<option value="600000">600000</option>
										<option value="700000">700000</option>
										<option value="800000">800000</option>
										<option value="900000">900000</option>
										<option value="1000000">1000000</option>
										<option value="1000001">Above 1000000</option>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">Country</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm country" name="country"></strong>
										<option value="0">--Country--</option>
										<?php
						              		$query  = "SELECT * FROM `countries` ORDER BY `name` ASC";
											$stmt   = $link->prepare($query);
											$stmt->execute();
											$result = $stmt->fetchAll();
											foreach( $result as $row )
											{
												$country_name =  $row['name'];
												$country_id = $row['id']; 

												if($country == $country_id)
												{
													echo "<option value='".$country_id."' selected>".$country_name."</option>";
												}
												else
												{
													echo "<option value='".$country_id."'>".$country_name."</option>";
												}
											}
						              	?>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">State</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm state" name="state">
										<option value="0">--State--</option>
										<?php
											$caste_sql  = "SELECT * FROM `states` WHERE `country_id`='$country' ORDER BY `name` ASC";
							                $stmt   = $link->prepare($caste_sql);
							                $stmt->execute();
							                $states_userRow = $stmt->fetchAll();
							                
							                foreach( $states_userRow as $row )
											{
												$state_name =  $row['name'];
												$state_id = $row['id']; 

												if($state_id==$state)
												{
													echo "<option value='".$state_id."' selected>".$state_name."</option>";
												}
												else
												{
													echo "<option value='".$state_id."'>".$state_name."</option>";
												}
											}
										?>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 ">
								<strong class="label_adv_search">City</strong>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-xs-6 comman-advanced-search-filter">
								<center>
									<select class="form-control form control-sm city" name="city">
										<option value="0">--City--</option>
										<?php
											if($state!='0')
											{
												$caste_sql  = "SELECT * FROM `cities` WHERE `state_id`='$state' ORDER BY `name` ASC";
								                $stmt   = $link->prepare($caste_sql);
								                $stmt->execute();
								                $city_userRow = $stmt->fetchAll();
								                
								                foreach( $city_userRow as $row )
												{
													$city_name =  $row['name'];
													$city_id = $row['id']; 
													if($city==$city_id)
													{
														echo "<option value='".$city_id."' selected>".$city_name."</option>";
													}
													else
													{
														echo "<option value='".$city_id."'>".$city_name."</option>";
													}
												}
											}
							                
							            ?>
									</select>
								</center>
							</div>
						</div>
						<hr/>
						<div>
							<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
						</div>
						<div class="my_photos_status"></div>
					</div>
				</div>
			</div>
		</menu>
		<div class="inner-body mg-main adv-search-result-div">
			<div class="inner-toolbar clearfix">
				
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<input type="hidden" name="monthly_income_from" class="monthly_income_from" value="<?php echo $monthly_income_from?>">

				<input type="hidden" name="monthly_income_to" class="monthly_income_to" value="<?php echo $monthly_income_to?>">
				<?php
					}
				?>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 result_data adv-search-result">
				<!--    Result from ajax query/advanced_search_result_helper.php   -->
			</div>
		</div>
	<!-- end: page -->
</section>

</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>
</html>
<script>
	$(document).ready(function(){
		var dataTable_Member_list = $('.tbl-profile').DataTable({
			"ordering": false,
			"bInfo" : false
		});
		
		var profile_id = $('.profile_id').val();
		var gender = $("input[name='gender']:checked").val();
		var marital_status = [];
		$('input[type=checkbox]:checked').each(function() {
			marital_status .push(this.value);
		});

		var from_date = $('.from_date').val();
		var to_date = $('.to_date').val();
		var religion = $('.religion').val();
		var caste = $('.caste').val();
		var mother_tongue = $('.mother_tongue').val();
		var education = $('.education').val();
		var occupation = $('.occupation').val();
		var currency = $('.currency').val();
		var monthly_income_from = $('.monthly_income_from').val();
		var monthly_income_to = $('.monthly_income_to').val();
		var country = $('.country').val();
		var state = $('.state').val();
		var city = $('.city').val();
		var task = "Get_Advanced_Search_Result";
		
		var data = 'profile_id='+profile_id+'&gender='+gender+'&marital_status='+marital_status+'&from_date='+from_date+'&to_date='+to_date+'&religion='+religion+'&caste='+caste+'&mother_tongue='+mother_tongue+'&education='+education+'&occupation='+occupation+'&currency='+currency+'&monthly_income_from='+monthly_income_from+'&monthly_income_to='+monthly_income_to+'&country='+country+'&state='+state+'&city='+city+'&task='+task;
		//alert(data);return false;
		$.ajax({
			type:'post',
        	data:data,
        	url:'query/advanced_search_result_helper.php',
        	success:function(res)
        	{
        		//alert(res);return false;
        		$('.result_data').html(res);
        	}
        });

		var filter_data = function() {
			var profile_id = $('.profile_id').val();
		    var gender = $("input[name='gender']:checked").val();
			var marital_status = [];
			$('input[type=checkbox]:checked').each(function() {
				marital_status .push(this.value);
			});

			var from_date = $('.from_date').val();
			var to_date = $('.to_date').val();
			var religion = $('.religion').val();
			var caste = $('.caste').val();
			var mother_tongue = $('.mother_tongue').val();
			var education = $('.education').val();
			var occupation = $('.occupation').val();
			var currency = $('.currency').val();
			var monthly_income_from = $('.monthly_income_from_filtered').val();
			var monthly_income_to = $('.monthly_income_to_filtered').val();
			var country = $('.country').val();
			var state = $('.state').val();
			var city = $('.city').val();
		    return {
		    			profile_id:profile_id,
		    			gender:gender,
		    			marital_status:marital_status,
		    			from_date:from_date,
		    			to_date:to_date,
		    			religion:religion,
		    			caste:caste,
		    			mother_tongue:mother_tongue,
		    			education:education,
		    			occupation:occupation,
		    			currency:currency,
		    			monthly_income_from:monthly_income_from,
		    			monthly_income_to:monthly_income_to,
		    			country:country,
		    			state:state,
		    			city:city
		    	}
		};

		$('.gender, .marital_status').click(function(){
			var adv_filtered_data = filter_data();
			var task = "Get_Advanced_Search_Result";
			var data = 'gender='+adv_filtered_data['gender']+'&marital_status='+adv_filtered_data['marital_status']+'&from_date='+adv_filtered_data['from_date']+'&to_date='+adv_filtered_data['to_date']+'&religion='+adv_filtered_data['religion']+'&caste='+adv_filtered_data['caste']+'&mother_tongue='+adv_filtered_data['mother_tongue']+'&education='+adv_filtered_data['education']+'&occupation='+adv_filtered_data['occupation']+'&currency='+adv_filtered_data['currency']+'&monthly_income_from='+adv_filtered_data['monthly_income_from']+'&monthly_income_to='+adv_filtered_data['monthly_income_to']+'&country='+adv_filtered_data['country']+'&state='+adv_filtered_data['state']+'&city='+adv_filtered_data['city']+'&task='+task;

			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/advanced_search_result_helper.php',
	        	success:function(res)
	        	{
	        		$('.result_data').html(res);
	        	}
	        });
		});
		
		$('.from_date, .to_date, .religion, .caste, .mother_tongue, .education, .occupation, .currency, .monthly_income_from_filtered, .monthly_income_to_filtered, .city').change(function(){
			var adv_filtered_data = filter_data();
			var task = "Get_Advanced_Search_Result";
			var data = 'gender='+adv_filtered_data['gender']+'&marital_status='+adv_filtered_data['marital_status']+'&from_date='+adv_filtered_data['from_date']+'&to_date='+adv_filtered_data['to_date']+'&religion='+adv_filtered_data['religion']+'&caste='+adv_filtered_data['caste']+'&mother_tongue='+adv_filtered_data['mother_tongue']+'&education='+adv_filtered_data['education']+'&occupation='+adv_filtered_data['occupation']+'&currency='+adv_filtered_data['currency']+'&monthly_income_from='+adv_filtered_data['monthly_income_from']+'&monthly_income_to='+adv_filtered_data['monthly_income_to']+'&country='+adv_filtered_data['country']+'&state='+adv_filtered_data['state']+'&city='+adv_filtered_data['city']+'&task='+task;

			$.ajax({
				type:'post',
	        	data:data,
	        	url:'query/advanced_search_result_helper.php',
	        	success:function(res)
	        	{
	        		$('.result_data').html(res);
	        	}
	        });
		});

		$('.country').change(function(){
			var country_id = $(this).val();
			var task = "Fetch_state_data";  

			$.ajax({
				type:'post',
            	data:'country_id='+country_id+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.state').html(res);
            	}
			});

			var adv_filtered_data = filter_data();
			var task1 = "Get_Advanced_Search_Result";
			var data1 = 'gender='+adv_filtered_data['gender']+'&marital_status='+adv_filtered_data['marital_status']+'&from_date='+adv_filtered_data['from_date']+'&to_date='+adv_filtered_data['to_date']+'&religion='+adv_filtered_data['religion']+'&caste='+adv_filtered_data['caste']+'&mother_tongue='+adv_filtered_data['mother_tongue']+'&education='+adv_filtered_data['education']+'&occupation='+adv_filtered_data['occupation']+'&currency='+adv_filtered_data['currency']+'&monthly_income_from='+adv_filtered_data['monthly_income_from']+'&monthly_income_to='+adv_filtered_data['monthly_income_to']+'&country='+adv_filtered_data['country']+'&state='+adv_filtered_data['state']+'&city='+adv_filtered_data['city']+'&task='+task1;

			$.ajax({
				type:'post',
	        	data:data1,
	        	url:'query/advanced_search_result_helper.php',
	        	success:function(res)
	        	{
	        		$('.result_data').html(res);
	        	}
	        });
		});

		$('.state').change(function(){
			var state_id = $(this).val();
			var task = "Fetch_city_data";  
			$.ajax({
				type:'post',
            	data:'state_id='+state_id+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.city').html(res);
            	}
			});

			var adv_filtered_data = filter_data();
			var task1 = "Get_Advanced_Search_Result";
			var data1 = 'gender='+adv_filtered_data['gender']+'&marital_status='+adv_filtered_data['marital_status']+'&from_date='+adv_filtered_data['from_date']+'&to_date='+adv_filtered_data['to_date']+'&religion='+adv_filtered_data['religion']+'&caste='+adv_filtered_data['caste']+'&mother_tongue='+adv_filtered_data['mother_tongue']+'&education='+adv_filtered_data['education']+'&occupation='+adv_filtered_data['occupation']+'&currency='+adv_filtered_data['currency']+'&monthly_income_from='+adv_filtered_data['monthly_income_from']+'&monthly_income_to='+adv_filtered_data['monthly_income_to']+'&country='+adv_filtered_data['country']+'&state='+adv_filtered_data['state']+'&city='+adv_filtered_data['city']+'&task='+task1;

			$.ajax({
				type:'post',
	        	data:data1,
	        	url:'query/advanced_search_result_helper.php',
	        	success:function(res)
	        	{
	        		$('.result_data').html(res);
	        	}
	        });
		});

		$('.btn_profile_id').click(function(){
			var profile_id = $('.profile_id').val();
			//alert(profile_id);
			var adv_filtered_data = filter_data();
			var task = "Get_Advanced_Search_Result";
			var data1 = 'profile_id='+adv_filtered_data['profile_id']+'&gender='+adv_filtered_data['gender']+'&marital_status='+adv_filtered_data['marital_status']+'&from_date='+adv_filtered_data['from_date']+'&to_date='+adv_filtered_data['to_date']+'&religion='+adv_filtered_data['religion']+'&caste='+adv_filtered_data['caste']+'&mother_tongue='+adv_filtered_data['mother_tongue']+'&education='+adv_filtered_data['education']+'&occupation='+adv_filtered_data['occupation']+'&currency='+adv_filtered_data['currency']+'&monthly_income_from='+adv_filtered_data['monthly_income_from']+'&monthly_income_to='+adv_filtered_data['monthly_income_to']+'&country='+adv_filtered_data['country']+'&state='+adv_filtered_data['state']+'&city='+adv_filtered_data['city']+'&task='+task;

			$.ajax({
				type:'post',
	        	data:data1,
	        	url:'query/advanced_search_result_helper.php',
	        	success:function(res)
	        	{
	        		$('.result_data').html(res);
	        	}
	        });
		});

	});

	
	
	$('.apply_filter').click(function(){
		$('.nano').collapse();
	});
</script>