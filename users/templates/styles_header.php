	<!-- CSS -->
	<link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="../css/bootstrap-glyphicons.css" type="text/css">
	<link rel="stylesheet" href="../css/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="../css/datepicker3.css" type="text/css">
	<link rel="stylesheet" href="../css/theme.css" type="text/css">
	<link rel="stylesheet" href="../css/default.css" type="text/css">
	<link rel="stylesheet" href="../css/theme-custom.css" type="text/css">
	<link rel="stylesheet" href="../css/master.css" type="text/css">
	<link rel="stylesheet" href="../css/custom.css" type="text/css">
	<link rel="stylesheet" href="../config/style.php" type="text/css">
	<link rel="stylesheet" href="../css/richtext.min.css" type="text/css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
	<link rel="stylesheet" href="../css/dataTables.bootstrap.min.css" type="text/css">
	
	<?php
	    $sql_custom_meta = "SELECT * FROM `custom_meta_tags`";
	    $stmt_custom_meta = $link->prepare($sql_custom_meta);
	    $stmt_custom_meta->execute();
	    $count_custom_meta = $stmt_custom_meta->rowCount();

	    if($count_custom_meta>0)
	    {
	      $result_custom_meta = $stmt_custom_meta->fetchAll();
	      foreach ($result_custom_meta as $row_custom_meta) 
	      {
	          $meta_tag = $row_custom_meta['meta_tag'];
	          $meta_tag = str_replace("--lt","<",$meta_tag);

	          echo $meta_tag;
	          echo "\n";
	      }
	    }
	?>

	<script src="../js/modernizr.js"></script>
	

	<!-- Header Script  -->
    <?php 
    	$include_header_script = getHeaderScriptDisplay();
    	$header_script = getHeaderScript();
    	if($include_header_script!='' && $include_header_script!=null && $include_header_script!='0')
    	{
    		@$header_script_data = file_get_contents($websiteBasePath.'/'.$header_script);
    		echo "$header_script_data";
    	}
    ?>
</head>
<body>

	<section class="body">

		<!-- start: header -->
		<header class="header">
			
			<div class="logo-container">
				<a href="../" class="navbar-brand logo">
					<?php 
						$logo = getLogoURL();
						$sitetitle = getWebsiteTitle();
						$siteBasePath = getWebsiteBasePath();
						if($logo!='' || $logo!=null)
						{
							echo "<img src='$logo' class='img-responsive logo-img' alt='$sitetitle'/>";
						}
						else
						{
							echo "<img src='$siteBasePath/images/logo/default-logo.jpg' class='img-responsive logo-img' />";
						}
					?>
				</a>
			</div>
			
			<?php
				if(isset($_SESSION['logged_in']) && (isset($_SESSION['client_user']) && $_SESSION['client_user']!=''))
				{						
			?>	
				
				<!-- start: search & user box -->
					<div class="header-right" style="width:50%">

						

						<span class="separator"></span>

						<span class="logged_in">
							<strong>You Last logged in On: 
								<?php 
									$last_logged_On = getLastUserLoginDate($user_id);
									echo date('d-M-Y h:i A',strtotime($last_logged_On));
								?>
							</strong>
						
							<strong>From IP:<?php echo getLastUserLoginIP($user_id);?></strong>
						</span>
						<span class="separator"></span>
						<div id="userbox" class="userbox">
							<a href="#" data-toggle="dropdown">
								<figure class="profile-picture">
									<?php
										if($count>0)
										{
											echo "<img src='$profile_pic' alt='$user_id' class='img-circle' data-lock-picture='$profile_pic' alt='$user_name'/>";
										}
										else
										{
									?>
											<img src="../images/no_profile_pic.png" alt="<?php echo $user_name?>" class="img-circle" data-lock-picture="../images/no_profile_pic.png" />
									<?php	
										}
									?>
								</figure>
								<div class="profile-info" data-lock-name="<?php echo $user_name?>" data-lock-email="<?php echo $user_email?>">
									<span class="name"><?php echo $user_name;?></span>
									<span class="role">ID: <?php echo $unique_code;?></span>
								</div>
				
								<i class="fa custom-caret"></i>
							</a>
				
							<div class="dropdown-menu home_profile_menu">
								<ul class="list-unstyled">
									<li class="divider"></li>
									<li>
										<a role="menuitem" tabindex="-1" href="<?php echo $websiteBasePath.'/users/Profile-'.$unique_code.'.html';?>" target="_blank"><i class="fa fa-eye"></i> View My Public Profile</a>
									</li>
									<li>
										<a role="menuitem" tabindex="-1" href="change-password.php"><i class="fa fa-key"></i> Change Password</a>
									</li>
									<li>
										<a role="menuitem" tabindex="-1" href="logout.php"><i class="fa fa-power-off"></i> Logout</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				<!-- end: search & user box -->
			<?php
				}
				else
				{
			?>
				<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
			        <ul class="nav navbar-nav homepage-nav">
			          <li><a href="advanced-search.php" class="top_nav_links">Advance Search</a></li>
			          <li><a href="<?php echo $websiteBasePath; ?>/free-register.php" class="top_nav_links">Free Registration</a></li>
			          	<?php
			              	$membership_plan_available_count = getMembershipPlanCount();
			              	if($membership_plan_available_count>1)
			              	{
			                	echo "<li><a href='$websiteBasePath/membership-plans.php' class='top_nav_links'>Membership Plans</a></li>";
			              	}

			              	$wedding_directory_display_or_not = getWeddingDirectoryDisplayOrNot();

				            if($wedding_directory_display_or_not=='1')
				            {
				                echo "<li><a href='$websiteBasePath/directory/' class='top_nav_links'>Wedding Directory</a></li>";
				            }

			              	if(isset($_SESSION['logged_in']) && (isset($_SESSION['client_user']) && $_SESSION['client_user']!=''))
				            {
				              echo "<li><a href='logout.php'  class='top_nav_links'>Logout</a></li>";
				            }
				            else
				            if(isset($_SESSION['logged_in']) && (isset($_SESSION['vendor_user']) && $_SESSION['vendor_user']!=''))
				            {
				              echo "<li><a href='$websiteBasePath/directory/dashboard.php' class='top_nav_links'>My Account</a></li>";
				              echo "<li><a href='$websiteBasePath/directory/logout.php'  class='top_nav_links'>Logout</a></li>";
				            }
				            else
				            {
				            	echo "<li><a href='$websiteBasePath/login.php' class='top_nav_links'>Login</a></li>";
				            }
			          	?>

			          
			        </ul>
			      </div>
			<?php		
				}
			?>
				