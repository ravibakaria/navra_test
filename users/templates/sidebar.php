<!--    Secondary Menu Start   -->
	<div class="navbar navbar-default secondary-navigation secondary-navigation-member" role="navigation">
	    <div class="row">

	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" width="10%">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	        </div>
	        <div class="collapse navbar-collapse">
	            <ul class="nav navbar-nav secondary-navigation-ul">
	                <li class="<?php if($current_file=='index.php') { echo 'nav-active'; } ?> ">
						<a href="index.php">
							<i class="fa fa-home" aria-hidden="true"></i>
							<span>Dashboard</span>
						</a>
					</li>
					<li class="dropdown">
	                    <a href="#" class="dropdown-toggle " data-toggle="dropdown">
	                    	<i class="fa fa-users" aria-hidden="true"></i>
	                    	Profile <b class="caret"></b></a>
	                    <ul class="dropdown dropdown-menu">
	                        <li class="<?php if($current_file=='my-profile.php' || $current_file=='upload-profile-picture.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="my-profile.php">
									<i class="fa fa-user" aria-hidden="true"></i>
									<span>My Profile</span>
								</a>
							</li>

							<li class="<?php if($current_file=='my-photos.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="my-photos.php">
									<i class="fa fa-camera" aria-hidden="true"></i>
									<span>My Photos</span>
								</a>
							</li>

							<li class="<?php if($current_file=='my-documents.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="my-documents.php">
									<i class="fa fa-files-o" aria-hidden="true"></i>
									<span>My Documents</span>
								</a>
							</li>

							<li class="<?php if($current_file=='my-preferences.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="my-preferences.php">
									<i class="fa fa-star-o" aria-hidden="true"></i>
									<span>My Preferences</span>
								</a>
							</li>

							<li class="<?php if($current_file=='my-chat-messages.php' || $current_file=='message.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="my-chat-messages.php">
									<i class="fa fa-wechat" aria-hidden="true"></i>
									<span>My Chat Messages</span>
								</a>
							</li>
						</ul>
					</li>

					<li class="dropdown">
	                    <a href="#" class="dropdown-toggle " data-toggle="dropdown">
	                    	<i class="fa fa-users" aria-hidden="true"></i>
	                    	Match Results <b class="caret"></b></a>
	                    <ul class="dropdown dropdown-menu">
	                        <li class="<?php if($current_file=='my-matches.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="my-matches.php">
									<i class="fa fa-star" aria-hidden="true"></i>
									<span>My Matches</span>
								</a>
							</li>

							<li class="<?php if($current_file=='shortlisted-profiles.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="shortlisted-profiles.php">
									<i class="fa fa-heart-o" aria-hidden="true"></i>
									<span>Shortlisted Profiles</span>
								</a>
							</li>

							<li class="<?php if($current_file=='interested-prospects.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="interested-prospects.php">
									<i class="fa fa-heart" aria-hidden="true"></i>
									<span>Interested Prospects</span>
								</a>
							</li>

							<li class="<?php if($current_file=='blocked-profiles.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="blocked-profiles.php">
									<i class="fa fa-ban" aria-hidden="true"></i>
									<span>Blocked Profiles</span>
								</a>
							</li>
						</ul>
					</li>

					<li class="<?php if($current_file=='advanced-search.php' || $current_file=='advanced-search-result.php') { echo 'nav-active'; } ?>">
						<a href="advanced-search.php">
							<i class="fa fa-search" aria-hidden="true"></i>
							<span>Advanced Search</span>
						</a>
					</li>

					<li class="dropdown">
	                    <a href="#" class="dropdown-toggle " data-toggle="dropdown">
	                    	<i class="fa fa-users" aria-hidden="true"></i>
	                    	Utilities <b class="caret"></b></a>
	                    <ul class="dropdown dropdown-menu">
	                        <li class="<?php if($current_file=='profile-likes.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="profile-likes.php">
									<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
									<span>Profile Likes</span>
								</a>
							</li>

							<li class="<?php if($current_file=='invite-friends.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="invite-friends.php">
									<i class="fa fa-envelope" aria-hidden="true"></i>
									<span>Invite Friends</span>
								</a>
							</li>

							<li class="<?php if($current_file=='report-abuse.php') { echo 'nav-active'; } ?> sidebar-links">
								<a href="report-abuse.php">
									<i class="fa fa-times" aria-hidden="true"></i>
									<span>Report Abuse</span>
								</a>
							</li>
						</ul>
					</li>

					<li class="<?php if($current_file=='my-orders.php') { echo 'nav-active'; } ?>">
						<a href="my-orders.php">
							<i class="fa fa-bars" aria-hidden="true"></i>
							<span>My Orders</span>
						</a>
					</li>

					<?php
						$is_wedding_directory_display = getWeddingDirectoryDisplayOrNot();
						if($is_wedding_directory_display=='1')
						{
					?>
					<li>
						<a href="../directory/">
							<i class="fa fa-wikipedia-w" aria-hidden="true"></i>
							<span>Wedding directory</span>
						</a>
					</li>
					<?php		
						}
					?>

					<li>
						<div class="search-profile-id">
	                    	<div class="col-xs-8 col-sm-8">
	                    		<input type="text" name="profile_id" class="form-control profile_id" placeholder="Profile Id"/>
	                    	</div>
	                    	<div class="col-xs-1 col-sm-1">
	                    		<i class="fa fa-search fa-2x profile-id-search"></i>
	                        	<!--button class="btn btn-info btn_profile_id website-button">Search</button-->
	                        </div>
	                    </div> <!-- form group [rows] -->
					</li>
	            </ul>
	        </div><!--/.nav-collapse -->
	    </div>
	</div>
	<!--    Secondary Menu End   -->


</header>
<!-- end: header -->

<div class="inner-wrapper user-page-top-header">	