<?php
	if(!isset($_SESSION))
	{
		session_start();
	}
	
	require_once "../config/config.php";
	require_once "../config/dbconnect.php";
	include "../config/functions.php";
	include "../config/setup-values.php";

	$maintainance_mode = getMaintainanceModeActiveOrNot();

	if($maintainance_mode=='1')
	{
	    echo "<script>window.location.assign('../website-maintainance.php');</script>";
	    exit;
	}

	$current_file = basename($_SERVER['PHP_SELF']);
	
	if(isset($_SESSION['logged_in']) && (isset($_SESSION['client_user']) && $_SESSION['client_user']!=''))
	{
		if(isset($_SESSION['last_page_accessed']) && ($_SESSION['last_page_accessed']!='' || $_SESSION['last_page_accessed']!=null))
		{
			$query_redirect_from = $_SESSION['last_page_accessed'];
			$membership_plan_id = $_SESSION['membership_plan_id'];
			$featured_listing = $_SESSION['featured_listing'];
			$validity = $_SESSION['validity'];
			$secret_key = $_SESSION['secret_key'];

			unset($_SESSION['membership_plan_id']);
			unset($_SESSION['featured_listing']);
			unset($_SESSION['validity']);
			unset($_SESSION['secret_key']);

			header("Location:$WebSiteBasePath/order-summary.php?membership_plan_id=$membership_plan_id&featured_listing=$featured_listing&validity=$validity&secret_key=$secret_key");
			exit;
		}

		if(isset($_SESSION['order_id']) && ($_SESSION['order_id']!='' || $_SESSION['order_id']!=null))
		{
			unset($_SESSION['order_id']);
		}
		
		$user_id = $_SESSION['user_id'];
		$user_email = $_SESSION['user_email'];
		
		$stmt = $link->prepare("SELECT * FROM `clients` WHERE `id`='$user_id' AND `email`='$user_email'");
		$stmt->execute();
	    $UserInfo = $stmt->fetch();

		$user_name = $UserInfo['firstname']." ".$UserInfo['lastname'];
		$firstname = $UserInfo['firstname'];
		$lastname = $UserInfo['lastname'];
		$dob = $UserInfo['dob'];
		$gender = $UserInfo['gender'];
		$unique_code = $UserInfo['unique_code'];
		$email = $UserInfo['email'];
		$phonenumber = $UserInfo['phonenumber'];

		$stmt = $link->prepare("SELECT * FROM `profilepic` WHERE `userid`='$user_id'"); 
	    $stmt->execute();
	    $count=$stmt->rowCount();
	    $result = $stmt->fetch();
	    
	    $profile_pic = 'uploads/'.$user_id.'/profile/'.$result['photo'];
	}
	
	$siteName = getWebsiteTitle();
	$siteTagline = getWebSiteTagline();
	$site_url = getWebsiteBasePath();
	$site_email = getWebSiteEmail();

	$logo=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo[] = $logoURL[$i];
		}

		$logo = implode('/',$logo);
	}

	if($logo=='' || $logo==null)
 	{
    	$logo = "images/logo/default-logo.jpg";
  	}
?>
<!doctype html>
<html lang="en-US">
<head>
	<!-- Basic -->
	<meta charset="UTF-8">
	
	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<?php 
    $websiteBasePath = getWebsiteBasePath();
    if(getFaviconURL()!='' || getFaviconURL()!=null)
    {
        $favicon_arr=array();
        $favicon_get = getFaviconURL();
        if($favicon_get!='' || $favicon_get!=null)
        {
          $favicon_get = explode('/',$favicon_get);

          for($i=1;$i<count($favicon_get);$i++)
          {
            $favicon_arr[] = $favicon_get[$i];
          }

          $favicon = implode('/',$favicon_arr);
        }
        echo "<link rel='icon' type='image/png' href='$websiteBasePath/$favicon' sizes='32x32'>";
    }
    else
    {
        echo "<link rel='icon' type='image/png' href='$websiteBasePath/images/favicon/default-favicon.png' sizes='32x32'>";
    }
  ?>
	
	<script type="application/ld+json">
  	{
    	"@context": "http://schema.org",
	    "@type": "Organization",
	    "name": "<?= $siteName;?>",
	    "alternateName": "<?= $siteTagline;?>",
	    "url": "<?= $site_url;?>",
	    "logo": "<?= $site_url.'/'.$logo; ?>",
	    "description": "<?= $siteName.' - '.$siteTagline;?>",
	    "email": "<?= $site_email; ?>"
  	}
	</script>