<?php
    if(!isset($_SESSION))
    {
        session_start();
    }

    if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
    {
        header('Location:../login.php');
        exit;
    }
    $profile_completeness_percent = null;
	//Profile pic
	$stmt_profile_pic = $link->prepare("SELECT * FROM `profilepic` WHERE `userid`='$user_id'"); 
    $stmt_profile_pic->execute();
    $count_profile_pic = $stmt_profile_pic->rowCount();

    //Personal Info
    $stmt_personal_info = $link->prepare("SELECT * FROM `profilebasic` WHERE `userid`='$user_id'"); 
    $stmt_personal_info->execute();
    $count_personal_info = $stmt_personal_info->rowCount();

    //Religious Information
    $stmt_religious_info = $link->prepare("SELECT * FROM `profilereligion` WHERE `userid`='$user_id'"); 
    $stmt_religious_info->execute();
    $count_religious_info = $stmt_religious_info->rowCount();

    //Location Information
    $stmt_location_info = $link->prepare("SELECT * FROM `address` WHERE `userid`='$user_id'"); 
    $stmt_location_info->execute();
    $count_location_info = $stmt_location_info->rowCount();

    //Education Information
    $stmt_education_info = $link->prepare("SELECT * FROM `eduocc` WHERE `userid`='$user_id'"); 
    $stmt_education_info->execute();
    $count_education_info = $stmt_education_info->rowCount();

    //Family Information
    $stmt_family_info = $link->prepare("SELECT * FROM `family` WHERE `userid`='$user_id'"); 
    $stmt_family_info->execute();
    $count_family_info = $stmt_family_info->rowCount();

    //About Yourself
    $stmt_myself_info = $link->prepare("SELECT * FROM `profiledesc` WHERE `userid`='$user_id'"); 
    $stmt_myself_info->execute();
    $count_myself_info = $stmt_myself_info->rowCount();
    
    if($count_profile_pic>0)
    {
       $profile_completeness_percent = $profile_completeness_percent+10;
    }

    if($count_personal_info>0)
    {
       $profile_completeness_percent = $profile_completeness_percent+15;
    }

    if($count_religious_info>0)
    {
       $profile_completeness_percent = $profile_completeness_percent+15;
    }

    if($count_location_info>0)
    {
       $profile_completeness_percent = $profile_completeness_percent+15;
    }

    if($count_education_info>0)
    {
       $profile_completeness_percent = $profile_completeness_percent+15;
    }

    if($count_family_info>0)
    {
       $profile_completeness_percent = $profile_completeness_percent+15;
    }

    if($count_myself_info>0)
    {
       $profile_completeness_percent = $profile_completeness_percent+15;
    }

?>