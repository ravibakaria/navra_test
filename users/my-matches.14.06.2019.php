<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}
	
	include("templates/header.php");

	$WebsiteBasePath = getWebsiteBasePath();
?>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>
	<style type="text/css">
		.show_more_main {
		    margin: 15px 25px;
		}
		.show_more {
		    background-color: #f8f8f8;
		    background-image: -webkit-linear-gradient(top,#fcfcfc 0,#f8f8f8 100%);
		    background-image: linear-gradient(top,#fcfcfc 0,#f8f8f8 100%);
		    border: 1px solid;
		    border-color: #d3d3d3;
		    color: #333;
		    font-size: 12px;
		    outline: 0;
		}
		.show_more {
		    cursor: pointer;
		    display: block;
		    padding: 10px 0;
		    text-align: center;
		    font-weight:bold;
		}
		.loding {
		    background-color: #e9e9e9;
		    border: 1px solid;
		    border-color: #c6c6c6;
		    color: #333;
		    font-size: 12px;
		    display: block;
		    text-align: center;
		    padding: 10px 0;
		    outline: 0;
		    font-weight:bold;
		}
		.loding_txt {
		    background-image: url(<?php echo $WebsiteBasePath.'/images/loader/load-more.gif'?>);
		    background-position: left;
		    background-repeat: no-repeat;
		    border: 0;
		    display: inline-block;
		    height: 16px;
		    padding-left: 20px;
		}
	</style>
<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
<body style="background-color:#fff;">

	<section role="main" class="content-body main-section-start">

		<!-- start: page -->
		<div class='row start_section_my_photos'>
			<div class='advanced-search-result-header'>
				<h1>Preferred/Matched Profiles.</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 my-preference-result">
				<div class="row my-match-result">
					<?php
						$where = $sqlTot = $sqlRec = "";

						$sql_search_preference = "SELECT * FROM preferences WHERE userid='$user_id'";
						$stmt   = $link->prepare($sql_search_preference);
				        $stmt->execute();
				        $count = $stmt->rowCount();
				        if($count>0)
				        {
				        	$search_preference = $stmt->fetch();
				        	$gender = $search_preference['gender'];
				        	$marital_status = $search_preference['marital_status'];
				        	$city = $search_preference['city'];
				        	$state = $search_preference['state'];
				        	$country = $search_preference['country'];
				        	$religion = $search_preference['religion'];
				        	$mother_tongue = $search_preference['mother_tongue'];
				        	$caste = $search_preference['caste'];
				        	$education = $search_preference['education'];
				        	$occupation = $search_preference['occupation'];
				        	$currency = $search_preference['currency'];
				        	$monthly_income_from = $search_preference['monthly_income_from'];
				        	$monthly_income_to = $search_preference['monthly_income_to'];
				        	$from_date = $search_preference['from_date'];
				        	$to_date = $search_preference['to_date'];
				        	$body_type = $search_preference['body_type'];
				        	$complexion = $search_preference['complexion'];
				        	$height_from = $search_preference['height_from'];
				        	$height_to = $search_preference['height_to'];
				        	$fam_type = $search_preference['fam_type'];
				        	$smoke_habbit = $search_preference['smoke_habbit'];
				        	$drink_habbit = $search_preference['drink_habbit'];
				        	$eat_habbit = $search_preference['eat_habbit'];

				        	$sql = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
				        	clients as A 
				        	LEFT OUTER JOIN
							profilebasic AS B ON A.id = B.userid
							LEFT OUTER JOIN
							address AS C ON A.id = C.userid
							LEFT OUTER JOIN
							profilereligion AS D ON A.id = D.userid
							LEFT OUTER JOIN
							eduocc AS E ON A.id = E.userid
							LEFT OUTER JOIN
							profilepic AS F ON A.id = F.userid
							LEFT OUTER JOIN
							family AS G ON A.id = G.userid
							WHERE A.gender='$gender' AND A.status='1'";

							/******  Profile Client Start *****/
								if($from_date=='0')      // Age form
								{
									$where .="";
								}
								else
								{
									$age_from = date('Y-m-d', strtotime('-'.$from_date.' years'));
									$where .=" AND A.dob >='$age_from'";
								}

								if($to_date=='0')      // Age to
								{
									$where .="";
								}
								else
								{
									$age_from_chk = strtotime($age_from);
			       					$age_to = date('Y-m-d',strtotime("+".$to_date." years",$age_from_chk));
									$where .=" AND A.dob <='$age_to'";
								}
							/******  Profile Client End *****/

							/******  Profile Basic Start *****/
								if($marital_status=='0' || $marital_status=='' || $marital_status==null)         // Marital status
								{
									$where .="";
								}
								else
								{
									$where .=" AND B.marital_status IN($marital_status)";
								}

								if($body_type=='0' || $body_type=='-1')         // body_type
								{
									$where .="";
								}
								else
								{
									$where .=" AND B.body_type ='$body_type'";
								}

								if($complexion=='0' || $complexion=='-1')         // complexion
								{
									$where .="";
								}
								else
								{
									$where .=" AND B.complexion ='$complexion'";
								}

								if($height_from=='0')         // height_from
								{
									$where .="";
								}
								else
								{
									$where .=" AND B.height_from >='$height_from'";
								}

								if($height_to=='0')         // height_to
								{
									$where .="";
								}
								else
								{
									$where .=" AND B.height_to <='$height_to'";
								}

								if($fam_type=='0' || $fam_type=='-1')         // fam_type
								{
									$where .="";
								}
								else
								{
									$where .=" AND B.fam_type ='$fam_type'";
								}

								if($smoke_habbit=='0' || $smoke_habbit=='-1')         // smoke_habbit
								{
									$where .="";
								}
								else
								{
									$where .=" AND B.smoke_habbit ='$smoke_habbit'";
								}

								if($drink_habbit=='0' || $drink_habbit=='-1')         // drink_habbit
								{
									$where .="";
								}
								else
								{
									$where .=" AND B.drink_habbit ='$drink_habbit'";
								}

								if($eat_habbit=='0' || $eat_habbit=='-1')         // eat_habbit
								{
									$where .="";
								}
								else
								{
									$where .=" AND B.eat_habbit ='$eat_habbit'";
								}

								if($eat_habbit=='0' || $eat_habbit=='-1')         // eat_habbit
								{
									$where .="";
								}
								else
								{
									$where .=" AND B.eat_habbit ='$eat_habbit'";
								}
							/******  Profile Basic End *****/


							/******  Profile location Start *****/
								if($city=='0' || $city=='-1')    // City
								{
									$where .="";
								}
								else
								{
									$where .=" AND C.city ='$city'";
								}

								if($state=='0' || $state=='-1')     //state
								{
									$where .="";
								}
								else
								{
									$where .=" AND C.state ='$state'";
								}

								if($country=='0' || $country=='-1')     //country
								{
									$where .="";
								}
								else
								{
									$where .=" AND C.country ='$country'";
								}
							/******  Profile location End *****/


							/******  Profile Education Start *****/
								if($education=='0' || $education=='-1')     //country
								{
									$where .="";
								}
								else
								{
									$where .=" AND E.education ='$education'";
								}

								if($occupation=='0' || $occupation=='-1')     //occupation
								{
									$where .="";
								}
								else
								{
									$where .=" AND E.occupation ='$occupation'";
								}

								if($currency=='0')     //currency
								{
									$where .="";
								}
								else
								{
									$where .=" AND E.currency ='$currency'";
								}

								if($monthly_income_from=='' || $monthly_income_from==null)     //monthly_income_from
								{
									$where .="";
								}
								else
								{
									$where .=" AND E.monthly_income_from >='$monthly_income_from'";
								}

								if($monthly_income_to=='' || $monthly_income_to==null)     //monthly_income_to
								{
									$where .="";
								}
								else
								{
									$where .=" AND E.monthly_income_to <='$monthly_income_to'";
								}
							/******  Profile Education  *****/


							/******  Profile Relegious Start *****/
								if($religion=='0' || $religion=='-1')     //religion
								{
									$where .="";
								}
								else
								{
									$where .=" AND D.country ='$religion'";
								}

								if($mother_tongue=='0' || $mother_tongue=='-1')     //mother_tongue
								{
									$where .="";
								}
								else
								{
									$where .=" AND D.mother_tongue ='$mother_tongue'";
								}

								if($caste=='0' || $caste=='-1')     //caste
								{
									$where .="";
								}
								else
								{
									$where .=" AND D.caste ='$caste'";
								}

							/******  Profile Relegious End *****/
							
							$where .=" ORDER BY rand() LIMIT 3";
							
							$sqlRec .=  $sql.$where;
							//echo $sqlRec;
							$stmt_search   = $link->prepare($sqlRec);
							$stmt_search->execute();
							$sqlTot = $stmt_search->rowCount();
							$result_search = $stmt_search->fetchAll();

							if($sqlTot>0)
							{
								
								echo "<div class='matched-profile-heading'>
										<h2>$sqlTot profiles found.</h2>
									</div>";
								
								$i=0;
								$profile_id_arr = array();
								foreach( $result_search as $row )
								{
									$city = $row['city'];
									$state = $row['state'];
									$country = $row['country'];
									$edu = $row['education'];
									$occ = $row['occupation'];
									$religion = $row['religion'];
									$caste = $row['caste'];
									$mother_tongue = $row['mother_tongue'];
									$u_code = $row['unique_code'];
									$dobx = $row['dob'];
									$profileid = $row['id'];
									$gender = $row['gender'];
									$height = $row['height'];
									$salary = $row['income'];
									$currency = getUserIncomeCurrencyCode($row['income_currency']);
									$fam_type = $row['fam_type'];
									
									$search_data = 'Profile-'.$u_code;
									$pro_url =  $search_data.".html";
									$search_user_fname=$row['firstname'];
									$search_user_name = $row['firstname']." ".$row['lastname'];
									$prefer_user_id =$row['idx'];
									$photo =$row['photo'];
									$today = date('Y-m-d');
									$diff = date_diff(date_create($dobx), date_create($today));
									$age_search_user = $diff->format('%y');
									$user_occ = user_occ($occ);

									$profile_id_arr[] = $u_code;

									if($i==0 || ($i>0 && $i%3==0))
									{
										echo "<div class='row'>";
									}
									?>

									<a href="<?php echo $pro_url; ?>" style="text-decoration:none; color:#000000;">
										<div class="card-my-matches col-xs-12 col-sm-12 col-md-4 col-lg-4">
											<span itemscope itemtype='http://schema.org/Person'>
												<meta itemprop='name' content="<?= $search_user_fname; ?>"/>
												<link itemprop='url' href="<?= $site_url.'/users/'.$pro_url; ?>;"/>
												<meta itemprop='description' content="Matrimonial Profile of <?php echo $search_user_fname; ?> From <?php echo $city_Name;?>, <?php echo $state_Name;?>, <?php echo $country_Name;?>. <?php echo $search_user_fname.'\'s '.$siteName;?>  Profile ID is <?php echo $u_code;?>. <?php echo $search_user_fname;?>'s Date Of Birth is <?php echo $dispaly_user_dob;?>. <?php echo $search_user_fname;?>'s mother tongue is <?php echo getUserMotherTongueName($mother_tongue);?>. <?php echo $search_user_fname;?> lives in a <?php echo $fam_type_name;?>. <?php echo $search_user_fname;?> belongs to <?php echo $fam_status_name;?> family. <?php echo $search_user_fname;?> follows <?php echo $fam_value_name;?> Family Values."/>
												<meta itemprop='address' content="<?= $city_Name.','.$state_Name.','.$country_Name; ?>"/>
												<meta itemprop='birthDate' content="<?= $dispaly_user_dob; ?>"/>
												<meta itemprop='gender' content="<?= $meta_gender; ?>"/>
												<meta itemprop='givenName' content="<?= $search_user_fname; ?>"/>
												<meta itemprop='height' content="<?= $height; ?>"/>
												<meta itemprop='homeLocation' content="<?= $city_Name; ?>"/>
												<meta itemprop='jobTitle' content="<?= $occupation_name; ?>"/>
												<meta itemprop='knowsLanguage' content="<?= getUserMotherTongueName($search_user_mother_tongue); ?>"/>
												<meta itemprop='nationality' content="<?= $country_Name; ?>"/>
												<meta itemprop='netWorth' content="<?= $salary; ?>"/>
												<meta itemprop='weight' content="<?= $weight; ?>"/>
											</span>

											<?php
												if($photo=='' || $photo==null)
												{
													echo "<img src='$site_url/images/no_profile_pic.png'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
												}
												else
												{
													echo "<img src='$site_url/users/uploads/$prefer_user_id/profile/$photo'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
												}
											?>
											<br><br>
											<h4>
												<b>
													<?php
														echo "<strong>";
														echo ucwords($search_user_fname);
														echo "</strong>";
													?>	
												</b>
											</h4>
											<p class="title">
												Profile ID: <?php echo $u_code; ?><br>
												From <?php echo substr(getUserCityName($city),0,11); ?>
											</p>

											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong class="">Age: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
														
													echo $age_search_user." Years.";
												?>	
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong class="">Height: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
														
													if($height!='' || $height!=null)
													{
														$search_user_height_meter = round(($height)/100,2);
									                	$search_user_height_foot = round(($height)/30.48,2);
									                	echo $height.'-cms/ '.$search_user_height_foot.'-fts/ '.$search_user_height_meter.'-mts';
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Religion: </strong>
													</p> 
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($religion!='' || $religion!=null)
													{
														echo getUserReligionName($religion);
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Caste: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($caste!='' || $caste!=null)
													{
														echo getUserCastName($caste);
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row" style='height:54px;'>
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Education: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($edu!='' || $edu!=null)
													{
														echo getUserEducationName($edu);
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row" style='height:54px;'>
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Occupation: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;&nbsp;";
									                	
													if($occ!='' || $occ!=null)
													{
														echo user_occ($occ);
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Salary: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($salary!='' || $salary!=null)
													{
														echo $currency." ".$salary." Monthly";
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Mother Tongue: </strong>
													</p> 
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($mother_tongue!='' || $mother_tongue!=null)
													{
														echo getUserMotherTongueName($mother_tongue);
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
										</div>
									</a>
							<?php
									$i++;
									if($i%3==0 && $i!=$sqlTot)
									{
										echo "</div>";
									}
								}
								echo "</div>";
								echo "<div class='row show_more_main' id='show_more_main$profileid'>";
									$profile_id_arr = implode(',',$profile_id_arr);
									
									echo "<span id='$profile_id_arr' class='show_more' title='Load more'>Show more >></span>";
									echo "<span class='loding' style='display: none;'><span class='loding_txt'>Loading...</span></span>";
									
								echo "</div>";	
							}
							else
							if($sqlTot==0)
							{
								echo "<h3 class='error'>Oops! No profile found with your preference.</h3>";
								echo "</div>";
							}
				        }
					?>
					
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
	          	<?php
            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
	            	if($display_skyscrapper_ad=='1')
	            	{
	            		echo "<div class='skyscrapper-ad'>";
	              			echo $skyscrapper_ad = getRandomSkyScrapperAdData();
	              		echo "</div>";
	            	}
	          	?>
			</div>
		</div>
		<br><br><br>
		<div class='leaderboard-ad'>
          	<?php
            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
            	if($display_leaderboard_ad=='1')
            	{
              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
            	}
          	?>
        </div>
	<!-- end: page -->
	</section>
</div>
</body>
<?php
	include("templates/footer.php");
?>
<script>
	$(document).ready(function(){
		var dataTable_Member_list = $('.tbl-profile').DataTable({
			"ordering": false,
			"bInfo" : false,
			"oLanguage": {
		      "sLengthMenu": "Showing _MENU_ rows",
		    }
		});

		$('.show_more').click(function(){
			var ID = $(this).attr('id');
			var div_id = $('.show_more_main').attr('id');
		
	        $(this).hide();
	        $('.loding').show();
	        $.ajax({
	            type:'POST',
	            url:'query/my-matches-load-more.php',
	            data:'id='+ID,
	            success:function(html){
	                $('#show_more_main'+div_id).hide();
	                $('.my-match-result').append(html);
	            }
	        });
		});
	});
</script>
</html>