<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}
	
	include("templates/header.php");

	$today_datetime = date('Y-m-d H:i:s');

	$basepath = $WebSiteBasePath.'/';
	$user_id = $_SESSION['user_id'];
	if(isset($_POST['my_photos_upload']))
	{
		$sql_chk = $link->prepare("SELECT * FROM `documents` WHERE `userid`='$user_id' AND status <> '2'"); 
	    $sql_chk->execute();
	    $count=$sql_chk->rowCount();
	    
	    $no_files = 1;
	    $remaining_files = (6-$count);
	    if($no_files>$remaining_files)
	    {
	    	$file_upload_count = $remaining_files;
	    }
	    else
	    {
	    	$file_upload_count = $no_files;
	    }
	    
		if($remaining_files<=0)
		{
			$errorMessage = "You are already uploaded maximun number of photos.<br/>To upload new photos remove previous photo.<br/> Then reupload new photo";
		}
		else
		{
			if(isset($_FILES['my_photos']['name']) &&  $_FILES['my_photos']['name']!='')
			{

				ini_set("post_max_size", "2M");
				ini_set("upload_max_filesize", "2M");
				ini_set("memory_limit", "2M");

				$target_dir = 'uploads/'.$user_id.'/documents/';
				if (!file_exists($target_dir)) 
				{
					try 
					{
						mkdir($target_dir, 0777, true);
					} 
					catch (Exception $ex) 
					{
						die("error");
					}
				}
				$filename = $_FILES["my_photos"]["name"];
				
				$errorMessage    = null;
				$successMessage = null;				
				$file_type = $_FILES["my_photos"]["type"];
				$file_size = $_FILES["my_photos"]["size"];
				$file_tmp  = $_FILES['my_photos']['tmp_name'];
				$file_name = $_FILES["my_photos"]["name"];
				
				$file_name =str_replace(",","_",$file_name);
				$file_ext  = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));	

				//$extensions= array('jpg','jpeg','png');
				$extensions= getAllowedFileAttachmentTypes();
				if(in_array($file_ext,$extensions)=== false)
				{
					if($errorMessage=='' || $errorMessage=null)
					{
						$errorMessage="File extension not allowed, please choose a jpg/jpeg/png file.";
					}
				}
				//echo $errorMessage;exit;
				if($file_size > 2097152 || $file_size==0)
				{
					if($errorMessage=='' || $errorMessage=null)
					{
						$errorMessage='File size must be less than 2 MB';
					}
				}

				if (file_exists($target_dir.$file_name)) 
				{
					if($errorMessage=='' || $errorMessage=null)
					{
						$errorMessage = "You have alredy uploaded photo.";
					}
				}	

				if($errorMessage=='' || $errorMessage==null)
				{
					if (move_uploaded_file($file_tmp,$target_dir.$file_name)) 
				    {
				    	$doc_type = $_POST['doc_type'];
				    	$doc_number = $_POST['doc_number'];
				    	$sql_insert = "INSERT INTO `documents`(`userid`, `photo_type`, `photo_number`, `photo`, `created_at`) VALUES ('$user_id','$doc_type','$doc_number','$file_name','$today_datetime')";

				    	if($link->exec($sql_insert))
						{
							$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','upload document','$doc_type-$doc_number-$file_name','$IP_Address',now())";
							$link->exec($sql_member_log);

							$successMessage = "Your document uploaded successfully.";
							echo "<script>window.location.assign('my-documents.php');</script>";
						}
						else
						{
							$errorMessage = "Sorry, Something went wrong. Try after some time.";
						}
				    }
				}
				
			}
		}
	}
?>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
<section role="main" class="content-body main-section-start">

	<!-- start: page -->
	<div class='row start_section_my_photos'>
		<div class='advanced-search-result-header'>
			<h1>Upload Document</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="row">
						<h2>General Instructions</h2>
						<ul style="list-style-type:circle">
							<li>
								Only files with extention <b><?php echo getAllowedFileAttachmentTypesString();?> </b> allowed to upload.
							</li>
							<li>
								File size should not exceeds <b>2 MB </b>.
							</li>
							<li>
								You are only allowed to upload <b> maximum 6 </b> documents.
							</li>
							<li>
								Upload any 3 documents from list.
								<ul>
									<?php
										$sql_document_types = "SELECT `name` FROM `identity_document_types` WHERE `status`='1'";
										$stmt_document_types = $link->prepare($sql_document_types);
										$stmt_document_types->execute();
										$result_document_types = $stmt_document_types->fetchAll();

										foreach ($result_document_types as $row_document_types) 
										{
											$document_type = $row_document_types['name'];

											echo "<li>
													<b> $document_type </b>
												</li>";
										}
									?>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<form action="<?php echo $_SERVER['PHP_SELF'];?>" method='POST'  enctype='multipart/form-data'>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
								<label for="doc_type_label" class="control-label">Document Type:</label>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
								<select class="form-control doc_type" name="doc_type">
									<option value="0">--Select Document Type--</option>
									<?php
										foreach ($result_document_types as $row_document_types) 
										{
											$document_type = $row_document_types['name'];

											echo "<option value='".$document_type."'>".$document_type."</option>";
										}
									?>
								</select>
							</div>
						</div>
						<div class="clearfix">&nbsp;</div>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
								<label for="doc_type_label" class="control-label">Document Number:</label>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-9 col-lg-9">
								<input type="text" class="form-control  doc_number" name="doc_number" placeholder="Document Number"/>
							</div>
						</div>
						<div class="clearfix">&nbsp;</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top">
								<input type="file" name="my_photos" id="my_photos"  class='form-control my_photos  file-upload'>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top">
								<div class="my_photos_status"></div>
							</div>
							
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top">
								<center>
									<button type="submit" class="btn btn-sm btn-sm btn-view-profile btn-primary my_photos_upload website-button" name="my_photos_upload">
									Upload
									</button>
								</center>
							</div>
						</div>
					</form>
				</div>
			</div>
			<hr>
			<div class="row">
				
				<div class="start-section-my-documents">
					<center><img src="../images/loader/loader.gif" class='img-responsive loading_img1' id='loading_img1' style='width:80px; height:80px; display:none;'/></center>
					<?php
						if(isset($errorMessage) && ($errorMessage!='' || $errorMessage!=null))
						{
							echo "<div class='alert alert-danger'>".$errorMessage."</div>";
						}

						if(isset($successMessage) && ($successMessage!='' || $successMessage!=null))
						{
							echo "<div class='alert alert-success'>".$successMessage."</div>";
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="mg-files"  data-sort-destination data-sort-id="media-gallery">
					<?php
						$stmt = $link->prepare("SELECT * FROM `documents` WHERE `userid`='$user_id' AND status<>'2'"); 
					    $stmt->execute();
					    $count=$stmt->rowCount();

					    if($count==0)
					    {
					    	echo "<h4>You have not uploaded any documents yet.</h4>";
					    }
					    else
					    if($count>0)
					    {	
					    	echo "<h3>Your uploaded documents.</h3>";
					    }

					    $result = $stmt->fetchAll();
						foreach( $result as $row )
						{
							$photo_id =  $row['id'];
							$photo_type =  $row['photo_type'];
							$photo_number = $row['photo_number'];
							$photo =  'uploads/'.$user_id.'/documents/'.$row['photo'];
							$status =  $row['status'];
							$created_at = $row['created_at']; 

					?>
					<div class="isotope-item document col-sm-6 col-md-4 col-lg-4">
						<div class="thumbnail">
							<div class="popup-gallery">
								<a class="thumb-image" href="<?php echo $photo;?>"  title="<?php echo $photo_type.' - '.$photo_number; ?>">
									<img src="<?php echo $photo;?>" class="doc-image">
								</a>
							</div>
							<div class="mg-description">
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<label class="pull-left"><b>Document Type:</b></label>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<label class="pull-left"><?php echo $photo_type;?></label>
									</div>
								</div>

								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<label class="pull-left"><b>Document No:</b></label>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<label class="pull-left"><?php echo $photo_number;?></label>
									</div>
								</div>
					
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<label class="pull-left"><b>Uploaded On:</b></label>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<label class="pull-left"><?php echo date('d-M-Y h:i A',strtotime($created_at));?></label>
									</div>
								</div>
							
								<div class="mg-description">
									<div class="row">
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<label class="pull-left"><b>Status:</b></label>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<label class="pull-left">
												<?php 
													if($status == '0')
													{
														$doc_status = "<b class='label label-warning text-right' style='text-align:right;'><span class='fa fa-exclamation-circle'></span> Pending</b>";
													}
													else
													if($status == '1')
													{
														$doc_status = "<b class='label label-success text-right' style='text-align:right;'>
															<span class='fa fa-check'></span> Verified</b>";
													}
													echo $doc_status;
												?>
											</label>
										</div>
									</div>
								</div>
							
								<div class="mg-description">
									<div class="row">
										<?php
											if($status == '0')
											{
												echo "<center><button class='btn btn-danger btn-sm my_photos_delete' id='$photo_id'>Delete</button>";
											}
										?>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php
						}
					?>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<div class='skyscrapper-ad'>
	          	<?php
            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
	            	if($display_skyscrapper_ad=='1')
	            	{
	              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
	            	}
	          	?>
	        </div>
		</div>
	</div>
	<div class='leaderboard-ad'>
      	<?php
        	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
        	if($display_leaderboard_ad=='1')
        	{
          		echo $leaderboard_ad = getRandomLeaderBoardAdData();
        	}
      	?>
    </div>
	<!-- end: page -->



</section>
</div>
</body>
<?php
	include("templates/footer.php");
?>
</html>
<script>
	$(document).ready(function(){
		$('#my_photos').click(function(){
			$('.my_photos_status').html("");
		});

		$("#my_photos").change(function(){
			var file = $(this).val();
	  		var ext = file.split('.').pop();
	  		var img_array = "<?php echo getAllowedFileAttachmentTypesString(); ?>";

	  		var i = img_array.indexOf(ext);

	  		if(i <= -1) 
	  		{
	  			$('.my_photos_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 15px;'><strong>Invalid data!</strong> Please select <?php echo getAllowedFileAttachmentTypesString(); ?> file type images.</div>");
	  			$(this).val("");
				return false;
	  		}
		});


		$('.my_photos_upload').click(function(){
			var doc_type = $(".doc_type").val();
			var doc_number = $(".doc_number").val();
			var my_photos = $("#my_photos").val();

			if(doc_type=='0')
			{
				$('.my_photos_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty!</strong> Please select document type from list.</div>");
				return false;
			}

			if(doc_number=='' || doc_number==null)
			{
				$('.my_photos_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty!</strong> Enter Document Number.</div>");
				return false;
			}

			if(my_photos=='' || my_photos==null)
			{
				$('.my_photos_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'>Select document files to upload</div>");
				return false;
			}

			$('.my_photos_status').html("");
			$('.loading_img').show();
			
		});

		$('.my_photos_delete').click(function(){
			var my_document_remove = $(this).attr('id');
			
			var task = "Delete_document";
			$('.loading_img1').show();
			$.ajax({                //delete user photos
				type : 'post',
				url : 'query/my_document_helper.php',
				data : 'my_document_remove='+my_document_remove+'&task='+task,
				success : function(res)
				{
					$('.loading_img1').hide();
					if(res=='success')
					{
						$('.start-section-my-documents').html("<div class='alert alert-success photo_delete_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong><i class='fa fa-check'></i> Success! </strong>Document deleted successfully.</div>");
						$('.start-section-my-documents').fadeTo(2000, 500).slideUp(500, function(){
                            $('.start-section-my-documents').slideUp(500);
                            window.location.assign('my-documents.php');
                        });
					}
					else
					{
						$('.start-section-my-documents').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error! </strong>"+res+"</div>");
					}
				}
			});
		});
		
	});
</script>