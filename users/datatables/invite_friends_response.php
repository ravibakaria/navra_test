<?php
	session_start();
	require_once '../../config/config.php'; 
	include('../../config/dbconnect.php');    //database connection
	include('../../config/functions.php');   //strip query string

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
    {
        header('Location:../login.php');
        exit;
    }

    $user_id = $_SESSION['user_id'];

	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;
	//print_r($params);
	//define index of column
	$columns = array( 
		0 =>'invited_email',
		1 =>'invitedOn',
		2 =>'invitation_sent_status',
        3 =>'invitation_status'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value if exist
	if( !empty($params['search']['value']) ) {   
		$where .=" AND ( invited_email LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR invitedOn LIKE '%".quote_smart($params['search']['value'])."%' ";
        $where .=" OR invitation_sent_status LIKE '%".quote_smart($params['search']['value'])."%' ";
		$where .=" OR invitation_status LIKE '%".quote_smart($params['search']['value'])."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT invited_email,invitedOn,invitation_sent_status,invitation_status FROM `invite_friends` WHERE userid='$user_id'";

	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	$sqlRec .=  " ORDER BY ". $columns[quote_smart($params['order'][0]['column'])]."   ".quote_smart($params['order'][0]['dir'])."  LIMIT ".quote_smart($params['start'])." ,".quote_smart($params['length'])." ";

 	
	$stmt1   = $link->prepare($sqlTot);
    $stmt1->execute();
    $totalRecords = $stmt1->rowCount();

	$stmt2   = $link->prepare($sqlRec);
    $stmt2->execute();
    $total_rows = $stmt2->rowCount();

    $result = $stmt2->fetchAll();

    foreach( $result as $row )
    {
        $invitation_sent_status = $row['2'];
        if($invitation_sent_status=='0')
        {
            $invitation_sent_status="<b class='label label-danger text-right' style='text-align:right;'>In progress</b>";
        }
        else
        if($invitation_sent_status=='1')
        {
            $invitation_sent_status="<b class='label label-success text-right' style='text-align:right;'>Invitation Sent</b>";
        }

    	$invitation_status = $row['3'];
        if($invitation_status=='0')
        {
            $invitation_status = "<b class='label label-danger text-right' style='text-align:right;'>Not Registered</b>";
        }
        else
        if($invitation_status=='0')
        {
            $invitation_status = "<b class='label label-success text-right' style='text-align:right;'>Registered</b>";
        }

        $row['2'] = $invitation_sent_status;
        $row['3'] = $invitation_status;
        
        $data[] = $row;
    }

    $json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>