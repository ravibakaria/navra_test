<?php
	include("templates/header.php");

	$logo=array();
  	$logoURL = getLogoURL();
  	if($logoURL!='' || $logoURL!=null)
  	{
    	$logoURL = explode('/',$logoURL);

    	for($i=1;$i<count($logoURL);$i++)
    	{
      		$logo[] = $logoURL[$i];
    	}

    $logo = implode('/',$logo);
  	}
?>
	<?php

		$siteName = getWebsiteTitle();
		$siteTagline = getWebSiteTagline();
		$siteBasePath = getWebsiteBasePath();

		$search_code = $_GET['search'];
		$search_code_val = explode('-',$search_code);
		$search_type = $search_code_val[1];    //category
		$search_value = $search_code_val[2];   //value
		$search_value_original = $search_code_val[2];   //value

		if($search_type=='City' || $search_type=='State' || $search_type=='Country')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "$search_value Matrimonial Profiles - $search_value Matrimony - $siteName";

			echo "<title>$search_value Matrimonial Profiles - $search_value Matrimony - $siteName</title>
			";

			echo "<meta name='title' content='$search_value Matrimonial Profiles - $search_value Matrimony - $siteName' />";

			echo $similar_description = "<meta name='description' content='$search_value Matrimony. Matrimonial Profiles of $siteName members living in $search_value. If you are looking for Bride or Groom living in $search_value then we have a perfect match for you in $search_value. Search matrimonial profiles of people living in $search_value having your preferred religion, caste, mother tongue and various education or occupation backgrounds.'>";
		}

		if($search_type=='Religion')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "$search_value Matrimonial Profiles - $search_value Matrimony - $siteName";

			echo "<title>$search_value Matrimonial Profiles - $search_value Matrimony - $siteName</title>
			";

			echo "<meta name='title' content='$search_value Matrimonial Profiles - $search_value Matrimony - $siteName' />";

			echo $similar_description = "<meta name='description' content='$search_value Matrimony. $search_value Matrimonial Profiles of $siteName members. If you are looking for $search_value Bride or $search_value Groom then we have a perfect match for you.'>";
		}

		if($search_type=='Caste')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "$search_value Matrimonial Profiles - $search_value Matrimony - $siteName";

			echo "<title>$search_value Matrimonial Profiles - $search_value Matrimony - $siteName</title>
			";

			echo "<meta name='title' content='$search_value Matrimonial Profiles - $search_value Matrimony - $siteName' />";

			echo $similar_description = "<meta name='description' content='$search_value Matrimony. $search_value Matrimonial Profiles of $siteName members. If you are looking for $search_value Bride or $search_value Groom then we have a perfect match for you. Search $search_value matrimonial profiles of people having your preferred religion, mother tongue and various education or occupation backgrounds.'>";
		}

		if($search_type=='MotherTongue')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "$search_value Matrimonial Profiles - $search_value Matrimony - $siteName";

			echo "<title>$search_value Matrimonial Profiles - $search_value Matrimony - $siteName</title>
			";

			echo "<meta name='title' content='$search_value Matrimonial Profiles - $search_value Matrimony - $siteName' />";

			echo $similar_description = "<meta name='description' content='$search_value Matrimony. $search_value Matrimonial Profiles of $siteName members. If you are looking for $search_value Bride or $search_value Groom then we have a perfect match for you. Search $search_value matrimonial profiles of people having your preferred religion, mother tongue and various education or occupation backgrounds.'>";
		}

		if($search_type=='BodyType')
		{	
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "Matrimonial Profiles Of $search_value Body Type - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Body Type - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Body Type - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value body type on $siteName. If you are looking for Bride or Groom with $search_value body type then we have a perfect match for you. Search matrimonial profiles with $search_value body type of people having your preferred religion, caste, mother tongue and various education or occupation backgrounds.'>";
		}

		if($search_type=='Complexion')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "Matrimonial Profiles Of $search_value Complexion - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Complexion - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Complexion - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value Complexion on $siteName. If you are looking for Bride or Groom with $search_value Complexion then we have a perfect match for you. Search matrimonial profiles with $search_value Complexion of people having your preferred religion, caste, mother tongue and various education or occupation backgrounds.'>";
		}

		if($search_type=='EatingHabit')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "Matrimonial Profiles Of $search_value Eating Habit - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Eating Habit - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Eating Habit - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value Eating Habit on $siteName. If you are looking for Bride or Groom with $search_value Eating Habit then we have a perfect match for you. Search matrimonial profiles with $search_value Eating Habit of people having your preferred religion, caste, mother tongue and various occupation backgrounds.'>";
		}

		if($search_type=='DrinkingHabit')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "Matrimonial Profiles Of $search_value Drinking Habit - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Drinking Habit - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Drinking Habit - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value Drinking Habit on $siteName. If you are looking for Bride or Groom with $search_value Drinking Habit then we have a perfect match for you. Search matrimonial profiles with $search_value Drinking Habit of people having your preferred religion, caste, mother tongue and various occupation backgrounds.'>";
		}

		if($search_type=='SmokingHabit')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "Matrimonial Profiles Of $search_value Smoking Habit - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Smoking Habit - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Smoking Habit - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value Smoking Habit on $siteName. If you are looking for Bride or Groom with $search_value Smoking Habit then we have a perfect match for you. Search matrimonial profiles with $search_value Smoking Habit of people having your preferred religion, caste, mother tongue and various occupation backgrounds.'>";
		}

		if($search_type=='MaritalStatus')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "Matrimonial Profiles Of $search_value Marital Status - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Marital Status - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Marital Status - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value Marital Status on $siteName. If you are looking for Bride or Groom with $search_value Marital Status then we have a perfect match for you. Search matrimonial profiles with $search_value Marital Status of people having your preferred religion, caste, mother tongue and various education or occupation backgrounds.'>";
		}

		if($search_type=='Education')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "Matrimonial Profiles Of $search_value Educated - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Educated - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Educated - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value education on $siteName. If you are looking for Bride or Groom with $search_value Education then we have a perfect match for you. Search matrimonial profiles with $search_value Education of people having your preferred religion, caste, mother tongue and various occupation backgrounds.'>";
		}

		if($search_type=='Occupation')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "Matrimonial Profiles Of $search_value Occupation - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Occupation - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Occupation - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value Occupation on $siteName. If you are looking for Bride or Groom with $search_value Occupation then we have a perfect match for you. Search matrimonial profiles with $search_value Occupation of people having your preferred religion, caste, Occupation and various education or occupation backgrounds.'>";
		}

		if($search_type=='FamilyValue')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "Matrimonial Profiles Of $search_value Family Values - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Family Values - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Family Values - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value Family Values on $siteName. If you are looking for Bride or Groom with $search_value Family Values then we have a perfect match for you. Search matrimonial profiles with $search_value Family Values of people having your preferred religion, Family Values, Family Values and various education or occupation backgrounds.'>";
		}

		if($search_type=='FamilyType')
		{
			$search_value = str_replace('_',' ',$search_value);

			$similar_title = "Matrimonial Profiles Of $search_value Family Type - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Family Type - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Family Type - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value Family Type on $siteName. If you are looking for Bride or Groom with $search_value Family Type then we have a perfect match for you. Search matrimonial profiles with $search_value Family Type of people having your preferred religion, Family Type, Family Type and various education or occupation backgrounds.'>";
		}

		if($search_type=='FamilyStatus')
		{
			$search_value = str_replace('_',' ',$search_value);
			
			$similar_title = "Matrimonial Profiles Of $search_value Family Status - $siteName";

			echo "<title>Matrimonial Profiles Of $search_value Family Status - $siteName</title>
			";

			echo "<meta name='title' content='Matrimonial Profiles Of $search_value Family Status - $siteName' />";

			echo $similar_description = "<meta name='description' content='Matrimonial Profiles of members having $search_value Family Status on $siteName. If you are looking for Bride or Groom with $search_value Family Status then we have a perfect match for you. Search matrimonial profiles with $search_value Family Status of people having your preferred religion, Family Status, Family Status and various education or occupation backgrounds.'>";
		}

		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		$title = ucwords($string);
	?>
	
	<meta name="keywords" content="<?php echo $siteName;?>, <?php echo $siteTagline; ?>, <?php echo $title; ?>">

	<meta property="og:title" content="<?= $similar_title; ?>" />
  	<meta property="og:url" content="<?= $siteBasePath.'/users/Search-'.$search_type.'-'.$search_value_original.'.html';?>" />
  	<meta property="og:description" content="<?= $similar_description; ?>" />
  	<meta property="og:image" content="<?= $siteBasePath.'/'.$logo;?>" />
  	<meta property="twitter:title" content="<?= $similar_title; ?>" />
  	<meta property="twitter:url" content="<?= $siteBasePath.'/users/Search-'.$search_type.'-'.$search_value_original.'.html';?>" />
  	<meta property="twitter:description" content="<?= $similar_description; ?>" />
  	<meta property="twitter:image" content="<?= $siteBasePath.'/'.$logo;?>" />

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>	
	<div style="background-color:#fff;width:100%">

		<section role="main" class="content-body main-section-start">

			<!-- start: page -->
			<div class="row matched-profiles-categorywise">
				<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
					<div class="start_section start_section_search_categories">
						<?php
							$search_code = $_GET['search'];
							$search_code_val = explode('-',$search_code);
							//print_r($search_code_val);
							$search_type = $search_code_val[1];
							$search_value = $search_code_val[2];

							$pro_url = '';

							if(!isset($user_id) || $user_id=='' || $user_id==null)
							{
								$user_id = '0';
							}

							if(!isset($gender) || $gender=='' || $gender==null)
							{
								$gender = '0';
							}

							if($search_type=='Religion')
							{
								$search_religion_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>Profiles from $search_religion_name Religion</h1>";
								$sql_get_id = "SELECT id FROM `religion` WHERE name='$search_religion_name'";
								$stmt   = $link->prepare($sql_get_id);
						        $stmt->execute();
						        $search_get_id = $stmt->fetch();
						        $search_religion_id = $search_get_id['id'];
								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.id IN(SELECT `userid` FROM `profilereligion` WHERE `religion` = '$search_religion_id' ) AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='Caste')
							{
								$search_caste_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>Profiles from $search_caste_name Caste</h1>";
								$sql_get_id = "SELECT id FROM `caste` WHERE name='$search_caste_name'";
								$stmt   = $link->prepare($sql_get_id);
						        $stmt->execute();
						        $search_get_id = $stmt->fetch();
						        $search_caste_id = $search_get_id['id'];
								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.id IN(SELECT `userid` FROM `profilereligion` WHERE `caste` = '$search_caste_id' ) AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='MotherTongue')
							{
								$search_mothertongue_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>Profiles from $search_mothertongue_name Mother Tongue</h1>";
								$sql_get_id = "SELECT id FROM `mothertongue` WHERE name='$search_mothertongue_name'";
								$stmt   = $link->prepare($sql_get_id);
						        $stmt->execute();
						        $search_get_id = $stmt->fetch();
						        $search_mothertongue_id = $search_get_id['id'];
								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.id IN(SELECT `userid` FROM `profilereligion` WHERE `mother_tongue` = '$search_mothertongue_id' ) AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='Manglik')
							{
								$header_text = "<h1>Profiles from $search_value Manglik Status</h1>";
								
								if($search_value == "Yes")
								{
								  	$manglik = "1";
								}
								elseif($search_value == "No")
								{
								   	$manglik = "2";
								}
								elseif($search_value == "Partial")
								{
								   	$manglik = "3";
								}
								else
								{
								  	$manglik = "";
								}

								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.id IN(SELECT `userid` FROM `profilereligion` WHERE `manglik` = '$manglik' ) AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='City')
							{
								$search_city_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>Profiles from $search_city_name City</h1>";
								$sql_get_id = "SELECT id FROM `cities` WHERE name='$search_city_name'";
								$stmt   = $link->prepare($sql_get_id);
						        $stmt->execute();
						        $search_get_id = $stmt->fetch();
						        $search_city_id = $search_get_id['id'];
								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.id IN(SELECT `userid` FROM `address` WHERE `city` = '$search_city_id' ) AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='State')
							{
								$search_state_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>Profiles from $search_state_name State</h1>";
								$sql_get_id = "SELECT id FROM `states` WHERE name='$search_state_name'";
								$stmt   = $link->prepare($sql_get_id);
						        $stmt->execute();
						        $search_get_id = $stmt->fetch();
						        $search_state_id = $search_get_id['id'];
								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.id IN(SELECT `userid` FROM `address` WHERE `state` = '$search_state_id' ) AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='Country')
							{
								$search_country_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>Profiles from $search_country_name Country</h1>";
								$sql_get_id = "SELECT id FROM `countries` WHERE name='$search_country_name'";
								$stmt   = $link->prepare($sql_get_id);
						        $stmt->execute();
						        $search_get_id = $stmt->fetch();
						        $search_country_id = $search_get_id['id'];
								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.id IN(SELECT `userid` FROM `address` WHERE `country` = '$search_country_id' ) AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='Education')
							{
								$search_education_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>Profiles With $search_education_name</h1>";
								$sql_get_id = "SELECT id FROM `educationname` WHERE name='$search_education_name'";
								$stmt   = $link->prepare($sql_get_id);
						        $stmt->execute();
						        $search_get_id = $stmt->fetch();
						        $search_education_id = $search_get_id['id'];
								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.id IN(SELECT `userid` FROM `eduocc` WHERE `education` = '$search_education_id' ) AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='Occupation')
							{
								$search_occupation_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>Profiles With $search_occupation_name</h1>";

								$sql_get_id = "SELECT id FROM `employment` WHERE name='$search_occupation_name'";
								$stmt   = $link->prepare($sql_get_id);
						        $stmt->execute();
						        $search_get_id = $stmt->fetch();
						        $search_occ_id = $search_get_id['id'];
								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.id IN(SELECT `userid` FROM `eduocc` WHERE `occupation` = '$search_occ_id' ) AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='BodyType')
							{
								$search_body_type_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>Profiles With $search_body_type_name Body Type</h1>";
								
								$sql_id = "SELECT id FROM `bodytype` WHERE name='$search_body_type_name'";
								$stmt   = $link->prepare($sql_id);
						        $stmt->execute();
						        $search_id = $stmt->fetch();
						        $search_id = $search_id['id'];

								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE B.body_type='$search_id' AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='Complexion')
							{
								$search_complexion_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>	Profiles With $search_complexion_name Complexion</h1>";
								
								$sql_id = "SELECT id FROM `complexion` WHERE name='$search_complexion_name'";
								$stmt   = $link->prepare($sql_id);
						        $stmt->execute();
						        $search_id = $stmt->fetch();
						        $search_id = $search_id['id'];

								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE B.complexion='$search_id' AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='EatingHabit')
							{
								$search_eatinghabbit_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>	Profiles With $search_eatinghabbit_name Eating Habbit</h1>";
								
								$sql_id = "SELECT id FROM `eathabbit` WHERE name='$search_eatinghabbit_name'";
								$stmt   = $link->prepare($sql_id);
						        $stmt->execute();
						        $search_id = $stmt->fetch();
						        $search_id = $search_id['id'];

								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE B.eat_habbit='$search_id' AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='SmokingHabit')
							{
								$search_smokinghabbit_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>	Profiles With $search_smokinghabbit_name Smoking Habbit</h1>";
								
								$sql_id = "SELECT id FROM `smokehabbit` WHERE name='$search_smokinghabbit_name'";
								$stmt   = $link->prepare($sql_id);
						        $stmt->execute();
						        $search_id = $stmt->fetch();
						        $search_id = $search_id['id'];

								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE B.smoke_habbit='$search_id' AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='DrinkingHabit')
							{
								$search_drinkinghabbit_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>	Profiles With $search_drinkinghabbit_name Drinking Habbit</h1>";
								
								$sql_id = "SELECT id FROM `drinkhabbit` WHERE name='$search_drinkinghabbit_name'";
								$stmt   = $link->prepare($sql_id);
						        $stmt->execute();
						        $search_id = $stmt->fetch();
						        $search_id = $search_id['id'];

								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE B.drink_habbit='$search_id' AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='MaritalStatus')
							{
								$search_maritalstatus_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>	Profiles With $search_maritalstatus_name</h1>";

								$sql_id = "SELECT id FROM `maritalstatus` WHERE name='$search_maritalstatus_name'";
								$stmt   = $link->prepare($sql_id);
						        $stmt->execute();
						        $search_id = $stmt->fetch();
						        $search_id = $search_id['id'];
								
								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE B.marital_status='$search_id' AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='FamilyValue')
							{
								$search_familyval_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>	Profiles With $search_familyval_name Family</h1>";
								
								$sql_id = "SELECT id FROM `familyvalue` WHERE name='$search_familyval_name'";
								$stmt   = $link->prepare($sql_id);
						        $stmt->execute();
						        $search_id = $stmt->fetch();
						        $search_id = $search_id['id'];

								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE G.fam_val='$search_id' AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='FamilyType')
							{
								$search_familytype_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>	Profiles With $search_familytype_name</h1>";
								
								$sql_id = "SELECT id FROM `familytype` WHERE name='$search_familytype_name'";
								$stmt   = $link->prepare($sql_id);
						        $stmt->execute();
						        $search_id = $stmt->fetch();
						        $search_id = $search_id['id'];

								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE G.fam_type='$search_id' AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='FamilyStatus')
							{
								$search_familystatus_name = str_replace('_',' ',$search_value);

								$header_text = "<h1>Profiles With $search_familystatus_name Family</h1>";
								
								$sql_id = "SELECT id FROM `familystatus` WHERE name='$search_familystatus_name'";
								$stmt   = $link->prepare($sql_id);
						        $stmt->execute();
						        $search_id = $stmt->fetch();
						        $search_id = $search_id['id'];


								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE G.fam_stat='$search_id' AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}
							else
							if($search_type=='Similar')
							{
								$header_text = "<h2 class='search_header_text'>	All similar profiles</h2>";
								$sql_search_criteria = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.gender='$search_value' AND A.status='1' AND A.id <> '$user_id' AND A.gender <> '$gender'";
							}

								$stmt_search   = $link->prepare($sql_search_criteria);
								$stmt_search->execute();
								$sqlTot = $stmt_search->rowCount();
								$result_search = $stmt_search->fetchAll();

								echo "
									<div class='advanced-search-result-header'>
										$header_text
										<h2>$sqlTot profiles found.</h2>
									</div>";

								if($sqlTot>0)
								{
									$i=0;
									
									foreach( $result_search as $row )
									{
										$city = $row['city'];
										$city_Name = getUserCityName($city);
										$state = $row['state'];
										$state_Name = getUserStateName($state);
										$country = $row['country'];
										$country_Name = getUserCountryName($country);
										$edu = $row['education'];
										$occ = $row['occupation'];
										$religion = $row['religion'];
										$caste = $row['caste'];
										$mother_tongue = $row['mother_tongue'];
										$u_code = $row['unique_code'];
										$dobx = $row['dob'];
										$dispaly_user_dob = date('d-M-Y',strtotime($dobx));
										$profileid = $row['id'];
										$gender = $row['gender'];
										$height = $row['height'];
										$weight = $row['weight'];
										if($gender=='1')
								        {
								        	$meta_gender = "Male";
								        }
								        else
								        if($gender=='2')
								        {
								        	$meta_gender = "Female";
								        }
								        else
								        if($gender=='3')
								        {
								        	$meta_gender = "T-Gender";
								        }

								        $occupation = $row['occupation'];
								        $occupation_name = getUserEmploymentName($occupation);
										$salary = $row['income'];
										$currency = getUserIncomeCurrencyCode($row['income_currency']);
										$salary_currency = $row['income_currency'];
										$fam_type = $row['fam_type'];
										
										$search_data = 'Profile-'.$u_code;
										$pro_url = $search_data.".html";
										$search_user_fname=$row['firstname'];
										$search_user_name = $row['firstname']." ".$row['lastname'];
										$prefer_user_id =$row['idx'];
										$photo =$row['photo'];
										$today = date('Y-m-d');
										$diff = date_diff(date_create($dobx), date_create($today));
										$age_search_user = $diff->format('%y');
										$age_search_user_month = $diff->format('%m');
										$site_url = getWebsiteBasePath();

										
								?>
								<a href="<?php echo $pro_url; ?>" style="text-decoration:none; color:#000000;">
									<div class="card-matched-profiles col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<span itemscope itemtype='http://schema.org/Person'>
											<meta itemprop='name' content="<?= $search_user_fname; ?>"/>
											<link itemprop='url' href="<?= $site_url.'/users/'.$pro_url; ?>;"/>
											<meta itemprop='description' content="Matrimonial Profile of <?php echo $search_user_fname; ?> From <?php echo $city_Name;?>, <?php echo $state_Name;?>, <?php echo $country_Name;?>. <?php echo $search_user_fname.'\'s '.$siteName;?>  Profile ID is <?php echo $u_code;?>. <?php echo $search_user_fname;?>'s Date Of Birth is <?php echo $dispaly_user_dob;?>. <?php echo $search_user_fname;?>'s mother tongue is <?php echo getUserMotherTongueName($mother_tongue);?>. <?php echo $search_user_fname;?> lives in a <?php echo $fam_type_name;?>. <?php echo $search_user_fname;?> belongs to <?php echo $fam_status_name;?> family. <?php echo $search_user_fname;?> follows <?php echo $fam_value_name;?> Family Values."/>
											<meta itemprop='address' content="<?= $city_Name.','.$state_Name.','.$country_Name; ?>"/>
											<meta itemprop='birthDate' content="<?= $dispaly_user_dob; ?>"/>
											<meta itemprop='gender' content="<?= $meta_gender; ?>"/>
											<meta itemprop='givenName' content="<?= $search_user_fname; ?>"/>
											<meta itemprop='height' content="<?= $height; ?>"/>
											<meta itemprop='homeLocation' content="<?= $city_Name; ?>"/>
											<meta itemprop='jobTitle' content="<?= $occupation_name; ?>"/>
											<meta itemprop='knowsLanguage' content="<?= getUserMotherTongueName($search_user_mother_tongue); ?>"/>
											<meta itemprop='nationality' content="<?= $country_Name; ?>"/>
											<meta itemprop='netWorth' content="<?= $salary; ?>"/>
											<meta itemprop='weight' content="<?= $weight; ?>"/>
										</span>

										<?php
											if($photo=='' || $photo==null)
											{
												echo "<img src='$site_url/images/no_profile_pic.png'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
											}
											else
											{
												echo "<img src='$site_url/users/uploads/$prefer_user_id/profile/$photo'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
											}
										?>
										<br><br>
										<h4>
											<b>
												<?php
													echo "<strong>";
													echo ucwords($search_user_fname);
													echo "</strong>";
												?>	
											</b>
										</h4>
										<p class="title">
											Profile ID: <?php echo $u_code; ?><br>
											From <?php echo substr(getUserCityName($city),0,11); ?>
										</p>

										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong class="">Age: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
													
												echo $age_search_user." Years.";
											?>	
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong class="">Height: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
													
												if($height!='' || $height!=null)
												{
													$search_user_height_meter = round(($height)/100,2);
								                	$search_user_height_foot = round(($height)/30.48,2);
								                	echo $height.'-cms/ '.$search_user_height_foot.'-fts/ '.$search_user_height_meter.'-mts';
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Religion: </strong>
												</p> 
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($religion!='' || $religion!=null)
												{
													echo getUserReligionName($religion);
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Caste: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($caste!='' || $caste!=null)
												{
													echo getUserCastName($caste);
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row" style="height:54px;">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Education: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($edu!='' || $edu!=null)
												{
													echo getUserEducationName($edu);
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row" style="height:54px;">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Occupation: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;&nbsp;";
								                	
												if($occ!='' || $occ!=null)
												{
													echo user_occ($occ);
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Salary: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($salary!='' || $salary!=null)
												{
													echo $currency." ".$salary." Monthly";
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Mother Tongue: </strong>
												</p> 
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($mother_tongue!='' || $mother_tongue!=null)
												{
													echo getUserMotherTongueName($mother_tongue);
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
									</div>
								</a>
								
							<?php
										
								}
								
							}
							else
							{
								echo "<h3 class='error'>Oops! No profile match with your preference.</h3>";
							}
						?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		          	<?php
	            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
		            	if($display_skyscrapper_ad=='1')
		            	{
		            		echo "<div class='skyscrapper-ad'>";
		              			echo $skyscrapper_ad = getRandomSkyScrapperAdData();
		              		echo "</div>";
		            	}
		          	?>
				</div>
			</div>
		</section>
	</div>
</div>
</section>
<?php
	include("templates/footer.php");
?>

<?php
    $sql_AddThisScript = "SELECT social_sharing_display,social_sharing_data FROM `social_sharing`";
    $stmt= $link->prepare($sql_AddThisScript); 
    $stmt->execute();
    $count=$stmt->rowCount();
    $result = $stmt->fetch();
    $social_sharing_display = $result['social_sharing_display'];
    $social_sharing_data = $result['social_sharing_data'];

    if($social_sharing_display=='1')
    {
        if($social_sharing_data!='' || $social_sharing_data!=null)
        {
            $social_sharing_data = file_get_contents($websiteBasePath.'/'.$social_sharing_data);
            echo "$social_sharing_data";
        }
    }
?>

</body>
</html>