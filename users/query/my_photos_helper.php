<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$task = quote_smart($_POST['task']);

	if($task=="Delete_Photo_Gallery") 
	{
		$my_photos_remove = $_POST['my_photos_remove'];

		$sql_chk = $link->prepare("SELECT * FROM `usergallery` WHERE `userid`='$user_id' AND id='$my_photos_remove'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();
        $result = $sql_chk->fetch();

        $file_delete = $result['photo'];
        if($count>0)
        {
        	$sql_delete = "DELETE FROM `usergallery` WHERE `userid`='$user_id' AND id='$my_photos_remove'";

        	if($link->exec($sql_delete))
			{
				unlink('../uploads/'.$user_id.'/photo/'.$file_delete);

				$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','delete','photo-$file_delete','$IP_Address',now())";
				$link->exec($sql_member_log);

				echo "success";
				exit;
			}
			else
			{
				echo "Something went wrong. Try after some time.";
				exit;
			}
        }
        else
        {
        	echo "Invalid Parameter value";
        }
        
	}
?>