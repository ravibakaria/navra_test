<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string
	include "../../config/setup-values.php";

	$task = quote_smart($_POST['task']);

	if($task=="Get_Chat_Message") 
	{
		$user_id = $_POST['user_id'];
		$search_user_id = $_POST['search_user_id'];
		$output = null;
		if (!is_numeric($user_id) || !is_numeric($search_user_id))
		{
			echo "ERROR : Invalid parameter value";
			exit;
		}

		$my_first_name = getUserFirstName($user_id);
		$my_profile_pic = getUserProfilePic($user_id);
		if($my_profile_pic=='' || $my_profile_pic==null)
		{
			$my_profile_pic = $WebSiteBasePath.'/images/no_profile_pic.png';
		}
		else
		{
			$my_profile_pic = 'uploads/'.$user_id.'/profile/'.$my_profile_pic;
		}

		$user_first_name = getUserFirstName($search_user_id);
		$user_UniqueCode = getUserUniqueCode($search_user_id);
		$user_profile_pic = getUserProfilePic($search_user_id);
		if($user_profile_pic=='' || $user_profile_pic==null)
		{
			$user_profile_pic = $WebSiteBasePath.'/images/no_profile_pic.png';
		}
		else
		{
			$user_profile_pic = 'uploads/'.$search_user_id.'/profile/'.$user_profile_pic;
		}

		$search_user_first_name = getUserFirstName($search_user_id);

		$sql_self_message = "SELECT  * FROM    messagechat a
		WHERE   (a.userFrom = '$user_id' AND a.userTo = '$search_user_id') OR
		        (a.userFrom = $search_user_id AND a.userTo = '$user_id')
		ORDER  BY messagedOn ASC";

		$stmt   = $link->prepare($sql_self_message);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$count=$stmt->rowCount();

		$sql_blocked_user = "SELECT  * FROM  blockedusers WHERE blocker='$user_id' AND blockedUser='$search_user_id' AND status='blocked'";

		$stmt1 = $link->prepare($sql_blocked_user);
		$stmt1->execute();
		$count_block=$stmt1->rowCount();
		$output .= "<ul class='list-unstyled'>";
		if($count>0)
		{
			foreach( $result as $row )
			{
				$from = $row['userFrom'];
				$to = $row['userTo'];
				$message = $row['message'];
				$messagedOn = $row['messagedOn'];

				$messagedOn = date('d-M-Y h:i A',strtotime($messagedOn));

				if($from==$user_id)
				{
					$output .= "<li class='left clearfix admin_chat'>
						<span class='chat-img1 pull-right'>
							
							<center><img src='$my_profile_pic' alt='$my_first_name' class='img-circle'></center>
						</span>
						<div class='chat-body1 clearfix'>
							<p class='self_msg pull-right'><strong class='self_text'> You wrote : </strong> $message<br/>
							<strong class='self_text chat_time'>Sent :</strong> <i class='message-time'> $messagedOn</i> 
							</p>
							
						</div>
						</li>";
				}
				else
				{
					$pro_url = $WebSiteBasePath.'/users/Profile-'. $user_UniqueCode.".html";
					$output .= "<li class='left clearfix'>
						<span class='chat-img1 pull-left'>
							<center><a href='$pro_url'><img src='$user_profile_pic' alt='$user_first_name' class='img-circle'></a></center>
						</span>
						<div class='chat-body1 clearfix'>
							<p class='user_msg pull-left'><strong class='user_text'>$user_first_name writes :</strong> $message<br/>
								<strong class='user_text chat_time'>Received :</strong> <i class='message-time'> $messagedOn</i>
							</p>
							
						</div>
						</li>";
				}

			}
		}
		else
		{
			$output .= "<h3 style='color:red;'>No messages found.....</h3>";
		}
		$output .= "<li id='message' tabindex='0'> Type your message below.</li>";
		$output .= "</ul>";
		echo $output;
		exit;
	}
?>