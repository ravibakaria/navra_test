<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string
	include('../../config/setup-values.php');   //get master setup values
    include('../../config/email/email_style.php');   //get master setup values
    include('../../config/email/email_templates.php');   //get master setup values
    include('../../config/email/email_process.php');
	
    $today_datetime = date('Y-m-d H:i:s');
    
    $WebSiteBasePath = getWebsiteBasePath();
    $sitetitle = getWebsiteTitle();
	$logo_array=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo_array[] = $logoURL[$i];
		}

		$logo_path = implode('/',$logo_array);
	}

	if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }

	$task = quote_smart($_POST['task']);

	if($task=="Send_Chat_Message") 
	{
		$userFrom = $_POST['userFrom'];
		$userTo = $_POST['userTo'];
		$chat_message = $_POST['message'];

		if($userFrom != $user_id)
		{
			echo "invalid_user";
			exit;
		}

		$sql_chk_block_user = $link->prepare("SELECT id FROM blockedusers WHERE blocker='$userTo' AND blockedUser='$userFrom' AND  status='blocked'"); 
        $sql_chk_block_user->execute();
        $count=$sql_chk_block_user->rowCount();

        if($count>0)
        {
        	echo "blocked_user";
			exit;
        }

        $sql_chk_blocked_user = $link->prepare("SELECT id FROM blockedusers WHERE blocker='$userFrom' AND blockedUser='$userTo' AND  status='blocked'"); 
        $sql_chk_blocked_user->execute();
        $count_blocked_user=$sql_chk_blocked_user->rowCount();

        if($count_blocked_user>0)
        {
        	echo "you_blocked_user";
			exit;
        }

        $sql_chat_id_chk = "SELECT  * FROM    messagechat a
		WHERE   (a.userFrom = '$user_id' AND a.userTo = '$userTo') OR
		        (a.userFrom = $userTo AND a.userTo = '$user_id')
		ORDER  BY messagedOn ASC";
		$stmt = $link->prepare($sql_chat_id_chk);
		$stmt->execute();
		$result = $stmt->fetch();
		$count_chatId=$stmt->rowCount();

		if($count_chatId==0)
		{
			$chatId = generateChatId($user_id,$userTo);
		}
		else
		if($count_chatId>0)
		{
			$chatId = $result['chatid'];
		}

		if($count_chatId==0)
		{
			$email_user_to = getUserEmail($userTo);
			$firstname_user_to = getUserFirstName($userTo);

			$firstname = getUserFirstName($userFrom);
			$profilePic = getUserProfilePic($userFrom);
			$ProfileId = getUserUniqueCode($userFrom);
			$City = getUserCity($userFrom);
			$State = getUserState($userFrom);
			$Country = getUserCountry($userFrom);
			$Age = getUserAge($userFrom);
			$Education = getUserEducation($userFrom);

			if($profilePic!='' || $profilePic!=null)
			{
				$profilePic_show = $WebSiteBasePath.'/users/uploads/'.$userFrom.'/profile/'.$profilePic;
			}
			else
			{
				$profilePic_show = $WebSiteBasePath.'/images/no_profile_pic.png';
			}

			$profile_link = $WebSiteBasePath.'/users/show-profile.php?search=Profile-'.$ProfileId;

			$sender_profile .= "<center>
								<div class='card-my-matches' style='width:100%;'>
									<a href='$WebSiteBasePath/users/Profile-$ProfileId.html' style='text-decoration:none;color:#000000;' target='_blank'>
										<img src='$profilePic_show' class='profile_img member-profile-image' alt='$firstname' title='$firstname' style='width:100%;height:auto;'/>
										<h4><b><strong>".ucwords($firstname)."</strong></b></h4>
										<p class='title'>
											Profile ID: $ProfileId <br/>
										</p>
										City: $City<br/>
										State: $State<br/>
										Country: $Country<br/>
										Age: $Age Years<br/>
										Education: $Education<br/>
									</a>
									<a href='$WebSiteBasePath/users/Profile-$ProfileId.html'><button  style='background-color:$primary_color;color:$primary_font_color;padding:5px;'><b>View Profile</b></button>
										<br/>
									</a>
								</div>
							</center>";
						
			$message_received_link = $WebSiteBasePath.'/users/message.php?id='.$userFrom;
			$message_received_page_link = "<a href='$message_received_link' target='_blank'>More Info</a>";

			$SocialSharing = getSocialSharingLinks();   // social sharing links

			$EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

			$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

            $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

            $MessageReceivedMessage = str_replace(array('$first_name','$site_name','$sender_profile','$message_received_page_link','$signature'),array($firstname_user_to,$WebSiteTitle,$sender_profile,$message_received_page_link,$GlobalEmailSignature),$MessageReceivedMessage);  //replace footer variables with value

            $subject = $MessageReceivedSubject;
                
            $message  = '<!DOCTYPE html>';
            $message .= '<html lang="en">
                <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width">
                <title></title>
                <style type="text/css">'.$EmailCSS.'</style>
                </head>
                <body style="margin: 0; padding: 0;">';
            //echo $message;exit;
            $message .= $EmailGlobalHeader;

            $message .= $MessageReceivedMessage;                                               
           	$message .= $EmailGlobalFooter;
            
            $mailto = $email_user_to;
            $mailtoname = $firstname_user_to;
            
            $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

            $sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$userTo','private message','from $ProfileId',now())";
            $link->exec($sql_member_email_log);
		}

        $sql_insert_message = "INSERT INTO messagechat(chatid,userFrom,userTo,message,messagedOn) VALUES('$chatId','$userFrom','$userTo','$chat_message','$today_datetime')";

        $ProfileId_Of_userTo = getUserUniqueCode($userTo);
        $sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$userFrom','sent private message','to profile-$ProfileId_Of_userTo','$IP_Address',now())";

        if($link->exec($sql_insert_message) && $link->exec($sql_member_log))
		{
        	echo "success";
			exit;
        }
	}


	if($task=="Block_User") 
	{
		$block_userFrom = $_POST['block_userFrom'];
		$block_userTo = $_POST['block_userTo'];
		$block_message = $_POST['block_message'];

		if($block_userFrom != $user_id)
		{
			echo "invalid_user";
			exit;
		}

        $sql_insert_message = "INSERT INTO blockedusers(blocker,blockedUser,blockComment,blockedOn,status) VALUES('$block_userFrom','$block_userTo','$block_message','$today_datetime','blocked')";

        $ProfileId_Of_block_userTo = getUserUniqueCode($block_userTo);
        $sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$block_userFrom','block','profile-$ProfileId_Of_block_userTo','$IP_Address',now())";

        if($link->exec($sql_insert_message) && $link->exec($sql_member_log))
		{
        	echo "success";
			exit;
        }
	}

	if($task=="Un_Block_User") 
	{
		$un_block_userFrom = $_POST['un_block_userFrom'];
		$un_block_userTo = $_POST['un_block_userTo'];
		$un_block_message = $_POST['un_block_message'];

		if($un_block_userFrom != $user_id)
		{
			echo "invalid_user";
			exit;
		}

		$sql_chk_un_block_user = $link->prepare("SELECT id FROM blockedusers WHERE blocker='$un_block_userFrom' AND blockedUser='$un_block_userTo'"); 
        $sql_chk_un_block_user->execute();
        $count_un_block=$sql_chk_un_block_user->rowCount();

        if($count_un_block>0)
        {
        	$sql_update_message = "UPDATE blockedusers SET status='un_blocked',unblocked_Comment='$un_block_message',unblocked_On='$today_datetime' WHERE blocker='$un_block_userFrom' AND blockedUser='$un_block_userTo' AND status='blocked'";

        	$ProfileId_Of_un_block_userTo = getUserUniqueCode($un_block_userTo);
        	$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$un_block_userFrom','un-block','profile-$ProfileId_Of_un_block_userTo','$IP_Address',now())";

        	if($link->exec($sql_update_message) && $link->exec($sql_member_log))
			{
	        	echo "success";
				exit;
	        }
        }
	}
?>