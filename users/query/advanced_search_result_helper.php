<?php
	session_start();
	/*if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];*/
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$task = quote_smart($_POST['task']);

	if($task=="Get_Advanced_Search_Result") 
	{
		$where = $sqlTot = $sqlRec =  "";
		$data = null;
		$output = null;
		$today = date('Y-m-d');
		
		$gender = $_POST['gender'];
		$marital_status = $_POST['marital_status'];
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$religion = $_POST['religion'];
		$caste = $_POST['caste'];
		$mother_tongue = $_POST['mother_tongue'];
		$education = $_POST['education'];
		$occupation = $_POST['occupation'];
		$currency = $_POST['currency'];
		$monthly_income_from = $_POST['monthly_income_from'];
		$monthly_income_to = $_POST['monthly_income_to'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$city = $_POST['city'];

		$data = 'gender='.$gender.'&marital_status='.$marital_status.'&from_date='.$from_date.'&to_date='.$to_date.'&religion='.$religion.'&caste='.$caste.'&mother_tongue='.$mother_tongue.'&education='.$education.'&occupation='.$occupation.'&currency='.$currency.'&monthly_income_from='.$monthly_income_from.'&monthly_income_to='.$monthly_income_to.'&country='.$country.'&state='.$state.'&city='.$city ;

		$data = str_replace("&",'-',$data);
		$sql = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
        	clients as A 
        	LEFT OUTER JOIN
			profilebasic AS B ON A.id = B.userid
			LEFT OUTER JOIN
			address AS C ON A.id = C.userid
			LEFT OUTER JOIN
			profilereligion AS D ON A.id = D.userid
			LEFT OUTER JOIN
			eduocc AS E ON A.id = E.userid
			LEFT OUTER JOIN
			profilepic AS F ON A.id = F.userid
			LEFT OUTER JOIN
			family AS G ON A.id = G.userid
			WHERE A.gender='$gender' AND A.status='1'";

	/******  Profile Client Start *****/

		/***    Age Filter    ****/
		$age_from = date('Y-m-d', strtotime('-'.$from_date.' years'));
		$age_to = date('Y-m-d',strtotime("-".$to_date." years"));

		if($from_date!='0' && $to_date!='0')
		{
			$where .=" AND A.dob BETWEEN '$age_to' AND '$age_from' ";
		}
		else
		if($from_date=='0' && $to_date=='0')
		{
			$where .="";
		}
		else
		if($from_date!='0' || $to_date!='0')
		{
			if($from_date!='0' && $to_date=='0')
			{
				
				$where .=" AND A.dob <='$age_from'  ";
			}

			if($to_date!='0' && $from_date=='0')
			{
				
				$where .=" AND A.dob >='$age_to'  ";
			}
		}

	/******  Profile Client End *****/

	/******  Profile Basic Start *****/
		if($marital_status=='' || $marital_status=='0')        // Marital status
		{
			$where .="";
		}
		else
		{
			$where .=" AND B.marital_status IN($marital_status)";
		}

	/******  Profile Basic End *****/

	/******  Profile location Start *****/
		if($city=='0')    // City
		{
			$where .="";
		}
		else
		{
			$where .=" AND C.city ='$city'";
		}
		
		if($state=='0')     //state
		{
			$where .="";
		}
		else
		{
			$where .=" AND C.state ='$state'";
		}

		if($country=='0')     //country
		{
			$where .="";
		}
		else
		{
			$where .=" AND C.country ='$country'";
		}
	/******  Profile location End *****/


	/******  Profile Education Start *****/
		if($education=='0')     //country
		{
			$where .="";
		}
		else
		{
			$where .=" AND E.education ='$education'";
		}

		if($occupation=='0')     //occupation
		{
			$where .="";
		}
		else
		{
			$where .=" AND E.occupation ='$occupation'";
		}

		if($currency=='0')     //currency
		{
			$where .="";
		}
		else
		{
			$where .=" AND E.income_currency ='$currency'";
		}

		if($monthly_income_from=='' || $monthly_income_from==null || $monthly_income_from=='0' )     //monthly_income_from
		{
			$where .="";
		}
		else
		{
			if(($monthly_income_from!='' || $monthly_income_from!=null || $monthly_income_from!='0' )  && $monthly_income_from=='1000001')
			{
				$where .=" AND E.income >$monthly_income_from";
			}
			else
			if(($monthly_income_from!='' || $monthly_income_from!=null || $monthly_income_from!='0' )  && $monthly_income_from!='1000001')
			{
				$where .=" AND E.income >=$monthly_income_from";
			}
		}

		if($monthly_income_to=='' || $monthly_income_to==null || $monthly_income_to=='0')     //monthly_income_to
		{
			$where .="";
		}
		else
		{
			if(($monthly_income_to!='' || $monthly_income_to!=null || $monthly_income_to!='0') && $monthly_income_to=='1000001')
			{
				$where .=" AND E.income >$monthly_income_to";
			}
			else
			if(($monthly_income_to!='' || $monthly_income_to!=null || $monthly_income_to!='0') && $monthly_income_to!='1000001')
			{
				$where .=" AND E.income <=$monthly_income_to";
			}
		}
	/******  Profile Education  *****/


	/******  Profile Relegious Start *****/
		if($religion=='0')     //religion
		{
			$where .="";
		}
		else
		{
			$where .=" AND D.religion ='$religion'";
		}

		if($mother_tongue=='0')     //mother_tongue
		{
			$where .="";
		}
		else
		{
			$where .=" AND D.mother_tongue ='$mother_tongue'";
		}

		if($caste=='0')     //caste
		{
			$where .="";
		}
		else
		{
			$where .=" AND D.caste ='$caste'";
		}
	/******  Profile Relegious End *****/

		$where .= " ORDER BY A.created_at DESC ";
		/*****  Limit  *****/
		$sqlRec .=  $sql.$where;
	//echo $sqlRec;exit;

		$stmt_search   = $link->prepare($sqlRec);
		$stmt_search->execute();
		$sqlTot = $stmt_search->rowCount();
		$result_search = $stmt_search->fetchAll();

//print_r($result_search);
		if($sqlTot>0)
		{	
			
			$output .= "<div class='row'>
						<div class='advanced-search-result-header'>";
			$output .= "<h2>$sqlTot profiles found.</h2>";
			$output .= "</div></div>";

			$i=0;

			foreach( $result_search as $row )
			{
				
				$city = $row['city'];
				$city_Name = getUserCityName($city);
				$state = $row['state'];
				$state_Name = getUserStateName($state);
				$country = $row['country'];
				$country_Name = getUserCountryName($country);
				$edu = $row['education'];
				$occ = $row['occupation'];
				$religion = $row['religion'];
				$caste = $row['caste'];
				$mother_tongue = $row['mother_tongue'];
				$u_code = $row['unique_code'];
				$dobx = $row['dob'];
				$dispaly_user_dob = date('d-M-Y',strtotime($dobx));
				$profileid = $row['id'];
				$gender = $row['gender'];
				$height = $row['height'];
				$weight = $row['weight'];
				if($gender=='1')
		        {
		        	$meta_gender = "Male";
		        }
		        else
		        if($gender=='2')
		        {
		        	$meta_gender = "Female";
		        }
		        else
		        if($gender=='3')
		        {
		        	$meta_gender = "T-Gender";
		        }

		        $occupation = $row['occupation'];
		        $occupation_name = getUserEmploymentName($occupation);
				$salary = $row['income'];
				$currency = getUserIncomeCurrencyCode($row['income_currency']);
				$salary_currency = $row['income_currency'];
				$fam_type = $row['fam_type'];
				
				$search_data = 'Profile-'.$u_code;
				$pro_url = $search_data.".html";
				$search_user_fname=$row['firstname'];
				$search_user_name = $row['firstname']." ".$row['lastname'];
				$prefer_user_id =$row['idx'];
				$photo =$row['photo'];
				$today = date('Y-m-d');
				$diff = date_diff(date_create($dobx), date_create($today));
				$age_search_user = $diff->format('%y');
				$age_search_user_month = $diff->format('%m');
				$site_url = getWebsiteBasePath();
				//$user_occ = user_occ($occ);
				
				if($i==0 || $i%3==0)
				{
					$output .= "<div class='row'>";
				}

				$output .= "<a href='$pro_url' style='text-decoration:none; color:#000000;'>
							<div class='card-my-matches col-xs-12 col-sm-12 col-md-4 col-lg-4'>
							<span itemscope itemtype='http://schema.org/Person'>
								<meta itemprop='name' content='".ucwords($search_user_fname)."'/>
								<link itemprop='url' href='".$site_url.'/users/'.$pro_url."'/>
								<meta itemprop='address' content='".$city_Name.','.$state_Name.','.$country_Name."'/>
								<meta itemprop='birthDate' content='$dispaly_user_dob'/>
								<meta itemprop='gender' content='$meta_gender'/>
								<meta itemprop='givenName' content='".ucwords($search_user_fname)."'/>
								<meta itemprop='hasOccupation' content='$occupation_name'/>
								<meta itemprop='height' content='$height'/>
								<meta itemprop='homeLocation' content='$city_Name'/>
								<meta itemprop='jobTitle' content='$occupation_name'/>
								<meta itemprop='knowsLanguage' content='".getUserMotherTongueName($mother_tongue)."'/>
								<meta itemprop='nationality' content='$country_Name'/>
								<meta itemprop='netWorth' content='$salary'/>
								<meta itemprop='weight' content='$weight'/>
							</span>";
						if($photo=='' || $photo==null)
						{
							$output .= "<img src='$site_url/images/no_profile_pic.png'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
						}
						else
						{
							$output .= "<img src='$site_url/users/uploads/$prefer_user_id/profile/$photo'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
						}

						$output .= "<br><br>";
						$output .= "<h4>
										<b>
											<strong>
												".ucwords($search_user_fname)."
											</strong>
										</b>
									</h4>";
						$output .= "<p class='title'>
										Profile ID: $u_code<br>
										From ".substr(getUserCityName($city),0,11)."
									</p>";
						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Age: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp
												
											$age_search_user Years.
											</p>
										</div>
									</div>";
						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Height: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
												&nbsp";
												
												if($height!='' || $height!=null)
												{
													$search_user_height_meter = round(($height)/100,2);
								                	$search_user_height_foot = round(($height)/30.48,2);
								                	$output .= " $height-cms/ $search_user_height_foot-fts/ $search_user_height_meter-mts";
												}
												else
												{
													$output .=  "-NA-";
												}

						$output .= "		</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Religion: </strong>
											</p> 
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp";
							                	
											if($religion!='' || $religion!=null)
											{
												$output .= getUserReligionName($religion);
											}
											else
											{
												$output .= "-NA-";
											}

						$output .=	"</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Caste: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp";
							                	
											if($caste!='' || $caste!=null)
											{
												$output .= getUserCastName($caste);
											}
											else
											{
												$output .= "-NA-";
											}

						$output .=	"</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row' style='height:54px;'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Education: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
										 	&nbsp;";
							                	
											if($edu!='' || $edu!=null)
											{
												$output .= getUserEducationName($edu);
											}
											else
											{
												$output .= "-NA-";
											}

						$output .=	"</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row' style='height:54px;'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Occupation: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp;&nbsp;";
							                	
											if($occ!='' || $occ!=null)
											{
												$output .= user_occ($occ);
											}
											else
											{
												$output .= "-NA-";
											}
						$output .=	"</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Salary: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp;";
							                	
											if($salary!='' || $salary!=null)
											{
												$output .= $currency." ".$salary." Monthly";
											}
											else
											{
												$output .= "-NA-";
											}

						$output .=	"</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Mother Tongue: </strong>
											</p> 
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp;";
							                	
											if($mother_tongue!='' || $mother_tongue!=null)
											{
												$output .= getUserMotherTongueName($mother_tongue);
											}
											else
											{
												$output .= "-NA-";
											}

						$output .=	"</p>
										</div>
									</div>";

						$output .= "</div>
								</a>";
					$i++;
					if($i%3==0)
					{
						$output .= "</div>";
					}
			}
		}
		else
		if($sqlTot==0)
		{
			$output = "<h3 class='error'>No result found.</h3>";
		}
		echo $output;
	}
?>