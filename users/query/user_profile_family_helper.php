<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$today_datetime = date('Y-m-d H:i:s');

	$task = quote_smart($_POST['task']);

	if($task=="Update_Family_Info") 
	{
		$fam_val = quote_smart($_POST['fam_val']);
		$fam_type = quote_smart($_POST['fam_type']);
		$fam_stat = quote_smart($_POST['fam_stat']);

		$sql_chk = $link->prepare("SELECT userid FROM `family` WHERE `userid`='$user_id'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();

        if($count==0)
		{
			$sql_query = "INSERT INTO `family`(userid,fam_val,fam_type,fam_stat,created_at) VALUES('$user_id','$fam_val','$fam_type','$fam_stat','$today_datetime')";
		}
		else
		if($count>0)
		{
			$sql_query = "UPDATE `family` SET `fam_val`='$fam_val',`fam_type`='$fam_type',`fam_stat`=$fam_stat,`updated_at`='$today_datetime' WHERE `userid`='$user_id'";
		}

		$profile_completeness_entry = getUserProfileCompleteEntry($user_id);
		if($profile_completeness_entry=='0')
		{
			$sql_update_profile = "INSERT INTO `profile_completeness`(`userid`,`family_info`) VALUES('$user_id','1')";
		}
		else
		{
			$sql_update_profile = "UPDATE `profile_completeness` SET `family_info`='1' WHERE `userid`='$user_id'";
		}

		$link->exec($sql_update_profile);

		if($link->exec($sql_query))
		{
			$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','updated','family info','$IP_Address',now())";
			$link->exec($sql_member_log);

			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>