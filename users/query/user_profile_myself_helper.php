<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$today_datetime = date('Y-m-d H:i:s');

	$task = quote_smart($_POST['task']);

	if($task=="Update_Myself_Info") 
	{
		$short_desc = quote_smart($_POST['short_desc']);
		$short_desc_family = quote_smart($_POST['short_desc_family']);
		$short_desc_interest = quote_smart($_POST['short_desc_interest']);

		$sql_chk = $link->prepare("SELECT userid FROM `profiledesc` WHERE `userid`='$user_id'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();

        if($count==0)
		{
			$sql_query = "INSERT INTO `profiledesc`(userid,short_desc,short_desc_family,short_desc_interest,created_at) VALUES('$user_id','$short_desc','$short_desc_family','$short_desc_interest','$today_datetime')";
		}
		else
		if($count>0)
		{
			$sql_query = "UPDATE `profiledesc` SET `short_desc`='$short_desc',`short_desc_family`='$short_desc_family',`short_desc_interest`='$short_desc_interest',`updated_at`='$today_datetime' WHERE `userid`='$user_id'";
		}

		$profile_completeness_entry = getUserProfileCompleteEntry($user_id);
		if($profile_completeness_entry=='0')
		{
			$sql_update_profile = "INSERT INTO `profile_completeness`(`userid`,`about_info`) VALUES('$user_id','1')";
		}
		else
		{
			$sql_update_profile = "UPDATE `profile_completeness` SET `about_info`='1' WHERE `userid`='$user_id'";
		}

		$link->exec($sql_update_profile);

		//echo $sql_query;exit;
		if($link->exec($sql_query))
		{
			$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','updated','about self info','$IP_Address',now())";
			$link->exec($sql_member_log);

			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>