<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];

	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$today_datetime = date('Y-m-d H:i:s');

	$userUniqueCode = getUserUniqueCode($user_id);

	$task = quote_smart($_POST['task']);

	if($task=="Send_Invitation_Email") 
	{
		function multiexplode ($delimiters,$string) 
		{
		    $ready = str_replace($delimiters, $delimiters[0], $string);
		    $value = explode($delimiters[0], $ready);
		    return  $value;
		}

		$invited_email = $_POST['invited_email'];

		$invited_email_array = multiexplode(array(",",";"," ","\n"),$invited_email);

		for($i=0;$i<count($invited_email_array);$i++) 
		{
			if($invited_email_array[$i]=='' || $invited_email_array[$i]==null)
			{
				continue;
			}

			$invited_email_id = $invited_email_array[$i];

			// Validate email
			if (filter_var($invited_email_id, FILTER_VALIDATE_EMAIL)) 
			{
			    $sql_chk = "SELECT * FROM `invite_friends` WHERE `invited_email` LIKE '%$invited_email_id%'";
				$stmt = $link->prepare($sql_chk);
				$stmt->execute();
				$count=$stmt->rowCount();

				if($count>0)
				{
					continue;
				}
				else
				{
					$sql_insert="INSERT INTO `invite_friends`(`userid`,`unique_code`,`invited_email`,`invitedOn`,`invitation_status`) VALUES('$user_id','$userUniqueCode','$invited_email_id','$today_datetime','0')";

					$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$userid','sent','invitation email','$IP_Address',now())";

					$link->exec($sql_insert);
					$link->exec($sql_member_log);
				}
			} 
			else 
			{
			    continue;
			}
		}
		echo "success";
		exit;
	}
?>