<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$today_datetime = date('Y-m-d H:i:s');

	$task = quote_smart($_POST['task']);

	if($task=="My_Preferences_Update") 
	{
		$gender = $_POST['gender'];
		$marital_status = $_POST['marital_status'];
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$religion = $_POST['religion'];
		$caste = $_POST['caste'];
		$mother_tongue = $_POST['mother_tongue'];
		$education = $_POST['education'];
		$religion = $_POST['religion'];
		$occupation = $_POST['occupation'];
		$currency = $_POST['currency'];
		$monthly_income_from = $_POST['monthly_income_from'];
		$monthly_income_to = $_POST['monthly_income_to'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$city = $_POST['city'];
		$body_type = $_POST['body_type'];
		$complexion = $_POST['complexion'];
		$height_from = $_POST['height_from'];
		$height_to = $_POST['height_to'];
		$fam_type = $_POST['fam_type'];
		$smoke_habbit = $_POST['smoke_habbit'];
		$drink_habbit = $_POST['drink_habbit'];
		$eat_habbit = $_POST['eat_habbit'];

		$sql_chk = $link->prepare("SELECT userid FROM `preferences` WHERE `userid`='$user_id'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();

        if($count==0)
        {
        	$sql = "INSERT INTO `preferences`(userid,gender,marital_status,city,state,country,religion,mother_tongue,caste,education,occupation,currency,monthly_income_from,monthly_income_to,from_date,to_date,body_type,complexion,height_from,height_to,fam_type,smoke_habbit,drink_habbit,eat_habbit,created_at) VALUES('$user_id','$gender','$marital_status','$city','$state','$country','$religion','$mother_tongue','$caste','$education','$occupation','$currency','$monthly_income_from','$monthly_income_to','$from_date','$to_date','$body_type','$complexion','$height_from','$height_to','$fam_type','$smoke_habbit','$drink_habbit','$eat_habbit','$today_datetime')";
        	$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','added','preferences','$IP_Address',now())";
        }
        else
        if($count>0)
        {
        	$sql = "UPDATE `preferences` SET gender='$gender',marital_status='$marital_status',city='$city',state='$state',country='$country',religion='$religion',mother_tongue='$mother_tongue',caste='$caste',education='$education',occupation='$occupation',currency='$currency',monthly_income_from='$monthly_income_from',monthly_income_to='$monthly_income_to',from_date='$from_date',to_date='$to_date',body_type='$body_type',complexion='$complexion',height_from='$height_from',height_to='$height_to',fam_type='$fam_type',smoke_habbit='$smoke_habbit',drink_habbit='$drink_habbit',eat_habbit='$eat_habbit',updated_at='$today_datetime' WHERE `userid`='$user_id'";
        	$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','update','preferences','$IP_Address',now())";
        }

        if($link->exec($sql))
		{
			$link->exec($sql_member_log);

			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>