<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$today_datetime = date('Y-m-d H:i:s');
    
	$task = quote_smart($_POST['task']);

	if($task=="Update_Education_Info") 
	{
		$education = quote_smart($_POST['education']);
		$occupation = quote_smart($_POST['occupation']);
		$designation_category = quote_smart($_POST['designation_category']);
		$designation = quote_smart($_POST['designation']);
		$industry = quote_smart($_POST['industry']);
		$income_currency = quote_smart($_POST['income_currency']);
		$income = quote_smart($_POST['income']);

		$sql_chk = $link->prepare("SELECT userid FROM `eduocc` WHERE `userid`='$user_id'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();

        if($count==0)
		{
			$sql_query = "INSERT INTO `eduocc`(userid,education,occupation,designation_category,designation,industry,income_currency,income,created_at) VALUES('$user_id','$education','$occupation','$designation_category','$designation','$industry','$income_currency','$income','$today_datetime')";
		}
		else
		if($count>0)
		{
			$sql_query = "UPDATE `eduocc` SET `education`='$education',`occupation`='$occupation',`designation_category`='$designation_category',`designation`='$designation',`industry`='$industry',`income_currency`='$income_currency',`income`=$income,`updated_at`='$today_datetime' WHERE `userid`='$user_id'";
		}

		$profile_completeness_entry = getUserProfileCompleteEntry($user_id);
		if($profile_completeness_entry=='0')
		{
			$sql_update_profile = "INSERT INTO `profile_completeness`(`userid`,`education_info`) VALUES('$user_id','1')";
		}
		else
		{
			$sql_update_profile = "UPDATE `profile_completeness` SET `education_info`='1' WHERE `userid`='$user_id'";
		}

		$link->exec($sql_update_profile);

		if($link->exec($sql_query))
		{
			$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','updated','education info','$IP_Address',now())";
			$link->exec($sql_member_log);

			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong. Try after some time.";
			exit;
		}
	}
?>