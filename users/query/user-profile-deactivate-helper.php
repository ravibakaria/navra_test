<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id_x = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$today_datetime = date('Y-m-d H:i:s');

	$task = quote_smart($_POST['task']);

	if($task=="Deactivate_My_Profile") 
	{
		$user_id = $_POST['user_id'];
		$reason = $_POST['reason'];

		if($user_id_x!=$user_id)
		{
			echo "Invalid user! Please try after some time";
			exit;
		}

		$IP = $_SERVER['REMOTE_ADDR'];

		$update_clients = "UPDATE clients SET status='2',updated_at='$today_datetime' WHERE id='$user_id'";
		$insert_reason = "INSERT INTO deactivated_profiles(userid,task,reason,updated_on,IP) VALUES('$user_id','Deactivate','$reason','$today_datetime','$IP')";

		if($link->exec($update_clients) && $link->exec($insert_reason))
		{
			$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','deactivate','current profile','$IP_Address',now())";
			$link->exec($sql_member_log);

			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong.Please try after some time.";
			exit;
		}
	}

	if($task=="Activate_My_Profile") 
	{
		$user_id = $_POST['user_id'];
		$reason = $_POST['reason'];

		if($user_id_x!=$user_id)
		{
			echo "Invalid user! Please try after some time";
			exit;
		}

		$IP = $_SERVER['REMOTE_ADDR'];

		$update_clients = "UPDATE clients SET status='1',updated_at='$today_datetime' WHERE id='$user_id'";
		$insert_reason = "INSERT INTO deactivated_profiles(userid,task,reason,updated_on,IP) VALUES('$user_id','Activate','$reason','$today_datetime','$IP')";

		if($link->exec($update_clients) && $link->exec($insert_reason))
		{
			$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','activate','current profile','$IP_Address',now())";
			$link->exec($sql_member_log);

			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong.Please try after some time.";
			exit;
		}
	}
?>