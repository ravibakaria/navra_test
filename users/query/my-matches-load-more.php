<?php 
     ob_start();
     session_start();
     if(!isset($_SESSION['logged_in']))
     {
          header('Location: ../login.php');
          exit;
     }

     $user_id = $_SESSION['user_id'];
     $email = $_SESSION['user_email'];
     require_once "../../config/config.php";
     require_once "../../config/dbconnect.php";
     include('../../config/functions.php');       //strip query string

     $siteName = getWebsiteTitle();
     $siteTagline = getWebSiteTagline();
     $site_url = getWebsiteBasePath();
     $site_email = getWebSiteEmail();

     $output = '';  
     $profileid = '';  
     $i = null;
     sleep(1);

     $where = $sqlTot = $sqlRec = "";

     $sql_search_preference = "SELECT * FROM preferences WHERE userid='$user_id'";
     $stmt   = $link->prepare($sql_search_preference);
     $stmt->execute();
     $count = $stmt->rowCount();
     if($count>0)
     {
          $search_preference = $stmt->fetch();
          $gender = $search_preference['gender'];
          $marital_status = $search_preference['marital_status'];
          $city = $search_preference['city'];
          $state = $search_preference['state'];
          $country = $search_preference['country'];
          $religion = $search_preference['religion'];
          $mother_tongue = $search_preference['mother_tongue'];
          $caste = $search_preference['caste'];
          $education = $search_preference['education'];
          $occupation = $search_preference['occupation'];
          $currency = $search_preference['currency'];
          $monthly_income_from = $search_preference['monthly_income_from'];
          $monthly_income_to = $search_preference['monthly_income_to'];
          $from_date = $search_preference['from_date'];
          $to_date = $search_preference['to_date'];
          $body_type = $search_preference['body_type'];
          $complexion = $search_preference['complexion'];
          $height_from = $search_preference['height_from'];
          $height_to = $search_preference['height_to'];
          $fam_type = $search_preference['fam_type'];
          $smoke_habbit = $search_preference['smoke_habbit'];
          $drink_habbit = $search_preference['drink_habbit'];
          $eat_habbit = $search_preference['eat_habbit'];

          $sql = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
          clients as A 
          LEFT OUTER JOIN
          profilebasic AS B ON A.id = B.userid
          LEFT OUTER JOIN
          address AS C ON A.id = C.userid
          LEFT OUTER JOIN
          profilereligion AS D ON A.id = D.userid
          LEFT OUTER JOIN
          eduocc AS E ON A.id = E.userid
          LEFT OUTER JOIN
          profilepic AS F ON A.id = F.userid
          LEFT OUTER JOIN
          family AS G ON A.id = G.userid
          WHERE A.gender='$gender' AND A.status='1'";

          /******  Profile Client Start *****/
               if($from_date=='0')      // Age form
               {
                    $where .="";
               }
               else
               {
                    $age_from = date('Y-m-d', strtotime('-'.$from_date.' years'));
                    $where .=" AND A.dob >='$age_from'";
               }

               if($to_date=='0')      // Age to
               {
                    $where .="";
               }
               else
               {
                    $age_from_chk = strtotime($age_from);
                    $age_to = date('Y-m-d',strtotime("+".$to_date." years",$age_from_chk));
                    $where .=" AND A.dob <='$age_to'";
               }
          /******  Profile Client End *****/

          /******  Profile Basic Start *****/
               if($marital_status=='0' || $marital_status=='' || $marital_status==null)         // Marital status
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND B.marital_status IN($marital_status)";
               }

               if($body_type=='0' || $body_type=='-1')         // body_type
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND B.body_type ='$body_type'";
               }

               if($complexion=='0' || $complexion=='-1')         // complexion
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND B.complexion ='$complexion'";
               }

               if($height_from=='0')         // height_from
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND B.height_from >='$height_from'";
               }

               if($height_to=='0')         // height_to
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND B.height_to <='$height_to'";
               }

               if($fam_type=='0' || $fam_type=='-1')         // fam_type
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND B.fam_type ='$fam_type'";
               }

               if($smoke_habbit=='0' || $smoke_habbit=='-1')         // smoke_habbit
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND B.smoke_habbit ='$smoke_habbit'";
               }

               if($drink_habbit=='0' || $drink_habbit=='-1')         // drink_habbit
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND B.drink_habbit ='$drink_habbit'";
               }

               if($eat_habbit=='0' || $eat_habbit=='-1')         // eat_habbit
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND B.eat_habbit ='$eat_habbit'";
               }

               if($eat_habbit=='0' || $eat_habbit=='-1')         // eat_habbit
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND B.eat_habbit ='$eat_habbit'";
               }
          /******  Profile Basic End *****/


          /******  Profile location Start *****/
               if($city=='0' || $city=='-1')    // City
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND C.city ='$city'";
               }

               if($state=='0' || $state=='-1')     //state
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND C.state ='$state'";
               }

               if($country=='0' || $country=='-1')     //country
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND C.country ='$country'";
               }
          /******  Profile location End *****/


          /******  Profile Education Start *****/
               if($education=='0' || $education=='-1')     //country
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND E.education ='$education'";
               }

               if($occupation=='0' || $occupation=='-1')     //occupation
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND E.occupation ='$occupation'";
               }

               if($currency=='0')     //currency
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND E.currency ='$currency'";
               }

               if($monthly_income_from=='' || $monthly_income_from==null)     //monthly_income_from
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND E.monthly_income_from >='$monthly_income_from'";
               }

               if($monthly_income_to=='' || $monthly_income_to==null)     //monthly_income_to
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND E.monthly_income_to <='$monthly_income_to'";
               }
          /******  Profile Education  *****/


          /******  Profile Relegious Start *****/
               if($religion=='0' || $religion=='-1')     //religion
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND D.country ='$religion'";
               }

               if($mother_tongue=='0' || $mother_tongue=='-1')     //mother_tongue
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND D.mother_tongue ='$mother_tongue'";
               }

               if($caste=='0' || $caste=='-1')     //caste
               {
                    $where .="";
               }
               else
               {
                    $where .=" AND D.caste ='$caste'";
               }

          /******  Profile Relegious End *****/
          $user_arr_id = '"'.$_POST['user_arr_id'].'"';
          
          $user_arr_string = str_replace('"', '', $user_arr_id);
          $user_arr = explode(",",$user_arr_string);

          $where .= " AND A.id NOT IN(".$user_arr_id.") ORDER BY rand() LIMIT 3 ";
          //$where .=" ORDER BY rand()";
          
          $sqlRec .=  $sql.$where;
          //echo $sqlRec;
          $stmt_search   = $link->prepare($sqlRec);
          $stmt_search->execute();
          $sqlTot = $stmt_search->rowCount();
          $result_search = $stmt_search->fetchAll();

          if($sqlTot>0)
          {
                                                       
               foreach( $result_search as $row )
               {
                    $city = $row['city'];
                    $state = $row['state'];
                    $country = $row['country'];
                    $edu = $row['education'];
                    $occ = $row['occupation'];
                    $religion = $row['religion'];
                    $caste = $row['caste'];
                    $mother_tongue = $row['mother_tongue'];
                    $u_code = $row['unique_code'];
                    $dobx = $row['dob'];
                    $profileid = $row['id'];
                    $gender = $row['gender'];
                    $height = $row['height'];
                    $weight = $row['weight'];
                    $salary = $row['income'];
                    $currency = getUserIncomeCurrencyCode($row['income_currency']);
                    $fam_type = $row['fam_type'];
                    $familystatus = $row['fam_stat'];
                    $familyvalue = $row['fam_val'];

                    $search_data = 'Profile-'.$u_code;
                    $pro_url =  $search_data.".html";
                    $search_user_fname=$row['firstname'];
                    $search_user_name = $row['firstname']." ".$row['lastname'];
                    $prefer_user_id =$row['idx'];
                    $photo =$row['photo'];
                    $today = date('Y-m-d');
                    $diff = date_diff(date_create($dobx), date_create($today));
                    $age_search_user = $diff->format('%y');
                    $user_occ = user_occ($occ);
                    
                    $city_Name = getUserCityName($city);
                    $state_Name = getUserStateName($state);
                    $country_Name = getUserCountryName($country);

                    $dispaly_user_dob = date('d-M-Y',strtotime($dobx));

                    $fam_type_name = getUserFamilyTypeName($fam_type);
                    $fam_status_name = getUserFamilyStateName($familystatus);
                    $fam_value_name = getUserFamilyValueName($familyvalue);

                    $occupation_name = getUserEmploymentName($user_occ);

                    $user_arr[] = $prefer_user_id;
                    
                    $i = $i+1;

                    $output .= '<div>
                              <a href='.$pro_url.' style="text-decoration:none; color:#000000;">
                                   <div class="card-my-matches col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                        <span itemscope itemtype="http://schema.org/Person">
                                             <meta itemprop="name" content='.$search_user_fname.' />
                                             <link itemprop="url" href='.$site_url.'/users/'.$pro_url.'/>
                                             <meta itemprop="description" content="Matrimonial Profile of '.$search_user_fname.' From '.$city_Name.', '.$state_Name.', '.$country_Name.' '.$search_user_fname.' '.$siteName.'  Profile ID is '.$u_code.' '.$search_user_fname.' Date Of Birth is '.$dispaly_user_dob.' '.$search_user_fname.' mother tongue is '.getUserMotherTongueName($mother_tongue).' '. $search_user_fname.' lives in a '.$fam_type_name.' '.$search_user_fname.' belongs to '.$fam_status_name.' family'. $search_user_fname.' follows '.$fam_value_name.' Family Values."/>
                                             <meta itemprop="address" content='.$city_Name.' , '.$state_Name.' , '.$country_Name.' "/>
                                             <meta itemprop="birthDate" content='.$dispaly_user_dob.'"/>
                                             <meta itemprop="gender" content='.$gender.'/>
                                             <meta itemprop="givenName" content='.$search_user_fname.'/>
                                             <meta itemprop="height" content='.$height.'/>
                                             <meta itemprop="homeLocation" content='.$city_Name.'/>
                                             <meta itemprop="jobTitle" content='.$occupation_name.'/>
                                             <meta itemprop="knowsLanguage" content='.getUserMotherTongueName($mother_tongue).'/>
                                             <meta itemprop="nationality" content='.$country_Name.'/>
                                             <meta itemprop="netWorth" content='.$salary.'/>
                                             <meta itemprop="weight" content='.$weight.'/>
                                        </span>';

                                        
                                             if($photo=='' || $photo==null)
                                             {
                                                  $output .=  '<img src='.$site_url.'/images/no_profile_pic.png  class="profile_img member-profile-image" alt='.$search_user_fname.' title='.$search_user_fname.' style="width:100%"/>';
                                             }
                                             else
                                             {
                                                  $output .=  '<img src='.$site_url.'/users/uploads/'.$prefer_user_id.'/profile/'.$photo.'  class="profile_img member-profile-image" alt='.$search_user_fname.' title='.$search_user_fname.' style="width:100%"/>';
                                             }
                                        
                                        $output .= '<br><br>
                                        <h4>
                                             <b>
                                                  <strong>
                                                  '.ucwords($search_user_fname).'</strong>
                                             </b>
                                        </h4>
                                        <p class="title">
                                             Profile ID: '.$u_code.'<br>
                                             From '.substr(getUserCityName($city),0,11).'
                                        </p>

                                        <div class="row member-profile-info-row">
                                             <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
                                                  <p align="justify">
                                                       <strong class="">Age: </strong> 
                                                  </p>
                                             </div>
                                             <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
                                                  <p align="justify">
                                                  &nbsp
                                                       
                                                  '.$age_search_user.'" Years."
                                                  </p>
                                             </div>
                                        </div>
                                        <div class="row member-profile-info-row">
                                             <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
                                                  <p align="justify">
                                                       <strong class="">Height: </strong> 
                                                  </p>
                                             </div>
                                             <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
                                                  <p align="justify">
                                                  &nbsp;';

                                                       
                                                  if($height!='' || $height!=null)
                                                  {
                                                       $search_user_height_meter = round(($height)/100,2);
                                                  $search_user_height_foot = round(($height)/30.48,2);
                                                  $output .=  $height.'-cms/ '.$search_user_height_foot.'-fts/ '.$search_user_height_meter.'-mts';
                                                  }
                                                  else
                                                  {
                                                       $output .=  "-NA-";
                                                  }
                                             
                                                  $output .= '</p>
                                             </div>
                                        </div>
                                        <div class="row member-profile-info-row">
                                             <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
                                                  <p align="justify">
                                                       <strong>Religion: </strong>
                                                  </p> 
                                             </div>
                                             <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
                                                  <p align="justify">
                                             &nbsp;';
                                                  
                                                  if($religion!='' || $religion!=null)
                                                  {
                                                       $output .=  getUserReligionName($religion);
                                                  }
                                                  else
                                                  {
                                                       $output .=  "-NA-";
                                                  }
                                             
                                             $output .= '</p>
                                             </div>
                                        </div>
                                        <div class="row member-profile-info-row">
                                             <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
                                                  <p align="justify">
                                                       <strong>Caste: </strong> 
                                                  </p>
                                             </div>
                                             <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
                                                  <p align="justify">
                                             &nbsp;';
                                                  
                                                  if($caste!='' || $caste!=null)
                                                  {
                                                       $output .=  getUserCastName($caste);
                                                  }
                                                  else
                                                  {
                                                       $output .=  "-NA-";
                                                  }
                                             
                                             $output .= '</p>
                                             </div>
                                        </div>
                                        <div class="row member-profile-info-row" style="height:54px;">
                                             <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
                                                  <p align="justify">
                                                       <strong>Education: </strong> 
                                                  </p>
                                             </div>
                                             <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
                                                  <p align="justify">
                                             &nbsp;';
                                                  
                                                  if($edu!='' || $edu!=null)
                                                  {
                                                       $output .=  getUserEducationName($edu);
                                                  }
                                                  else
                                                  {
                                                       $output .=  "-NA-";
                                                  }
                                             $output .= '</p>
                                             </div>
                                        </div>
                                        <div class="row member-profile-info-row" style="height:54px;"">
                                             <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
                                                  <p align="justify">
                                                       <strong>Occupation: </strong> 
                                                  </p>
                                             </div>
                                             <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
                                                  <p align="justify">
                                             &nbsp;&nbsp;';
                                                  
                                                  if($occ!='' || $occ!=null)
                                                  {
                                                       $output .=  user_occ($occ);
                                                  }
                                                  else
                                                  {
                                                       $output .=  "-NA-";
                                                  }
                                             
                                             $output .= '</p>
                                             </div>
                                        </div>
                                        <div class="row member-profile-info-row">
                                             <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
                                                  <p align="justify">
                                                       <strong>Salary: </strong> 
                                                  </p>
                                             </div>
                                             <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
                                                  <p align="justify">
                                             &nbsp;';
                                                  
                                                  if($salary!='' || $salary!=null)
                                                  {
                                                       $output .= $currency." ".$salary." Monthly";
                                                  }
                                                  else
                                                  {
                                                       $output .= "-NA-";
                                                  }
                                             
                                             $output .= '</p>
                                             </div>
                                        </div>
                                        <div class="row member-profile-info-row">
                                             <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
                                                  <p align="justify">
                                                       <strong>Mother Tongue: </strong>
                                                  </p> 
                                             </div>
                                             <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
                                                  <p align="justify">
                                             &nbsp;';
                                                  
                                                  if($mother_tongue!='' || $mother_tongue!=null)
                                                  {
                                                       $output .= getUserMotherTongueName($mother_tongue);
                                                  }
                                                  else
                                                  {
                                                       $output .= "-NA-";
                                                  }
                                             
                                             $output .= '</p>
                                             </div>
                                        </div>
                                   </div>
                              </a>
                         </div>';
                    ?>
          <?php

               }

               if($i%3!=0)
               {
                    $k = $i%3;
                    $k= 3-$k;
                    for($j=0;$j<$k;$j++)
                    {
                         $output .= "<div>
                                        <div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
                                        </div>
                                   </div>";
                    }
               }
               $output .= "<div class='row'><br></div>";

               $userid_arr_string = implode('","',$user_arr);

               $output .=  "<div id='remove_row' style='text-align: -webkit-center;'>  
                    <center><button type='button' name='btn_more' data-vid='$prefer_user_id' data-uid='$userid_arr_string' id='btn_more' class='btn btn-block btn-success form-control'><b>Load More</b> <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i> </button> </center>
               </div>";
          }
          
          echo $output;

   }
?>