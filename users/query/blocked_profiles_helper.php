<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$task = quote_smart($_POST['task']);

	if($task=="Get_Blocked_Profile") 
	{
		$value = $_POST['value'];
		$user_id_x = $_POST['user_id_x'];
		$output = null;

		if($user_id != $user_id_x)
		{
			echo "Unauthorized Access";
			exit;
		}

		$sql_search_blocked = "SELECT * FROM blockedusers WHERE $value='$user_id' AND status='blocked' AND blockedUser IN(SELECT id FROM clients C WHERE C.status='1')";
		$stmt   = $link->prepare($sql_search_blocked);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetchAll();

        if($count>0)
        {
        	if($value=='blockedUser')
			{
				$header = "Users blocked your profile";
			}
			else
			{
				$header = "Users blocked by you";
			}

        	$i=0;
        	$output .= "<div class=''>
						<h2>$count profile blocked by you.</h2>
					</div>";

			
			foreach( $result as $row )
			{

				if($value=='blockedUser')
				{
					$blocked_user_id = $row['blocker'];
					$header = "Users blocked your profile.";
				}
				else
				{
					$blocked_user_id = $row['blockedUser'];
					$header = "<h4>Users blocked by you";
				}
				
				$sql_shortlist_info = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
		        	clients as A 
		        	LEFT OUTER JOIN
					profilebasic AS B ON A.id = B.userid
					LEFT OUTER JOIN
					address AS C ON A.id = C.userid
					LEFT OUTER JOIN
					profilereligion AS D ON A.id = D.userid
					LEFT OUTER JOIN
					eduocc AS E ON A.id = E.userid
					LEFT OUTER JOIN
					profilepic AS F ON A.id = F.userid
					LEFT OUTER JOIN
					family AS G ON A.id = G.userid
					WHERE A.id='$blocked_user_id' AND A.status='1' ";

				$stmt_search   = $link->prepare($sql_shortlist_info);
				$stmt_search->execute();
				$sqlTot = $stmt->rowCount();
				$result_search = $stmt_search->fetch();

				$city = $result_search['city'];
				$city_Name = getUserCityName($city);
				$state = $result_search['state'];
				$state_Name = getUserStateName($state);
				$country = $result_search['country'];
				$country_Name = getUserCountryName($country);
				$edu = $result_search['education'];
				$occ = $result_search['occupation'];
				$religion = $result_search['religion'];
				$caste = $result_search['caste'];
				$mother_tongue = $result_search['mother_tongue'];
				$u_code = $result_search['unique_code'];
				$dobx = $result_search['dob'];
				$dispaly_user_dob = date('d-M-Y',strtotime($dobx));
				$profileid = $result_search['id'];
				$gender = $result_search['gender'];
				$height = $result_search['height'];
				$weight = $result_search['weight'];
				if($gender=='1')
		        {
		        	$meta_gender = "Male";
		        }
		        else
		        if($gender=='2')
		        {
		        	$meta_gender = "Female";
		        }
		        else
		        if($gender=='3')
		        {
		        	$meta_gender = "T-Gender";
		        }

		        $occupation = $result_search['occupation'];
		        $occupation_name = getUserEmploymentName($occupation);
				$salary = $result_search['income'];
				$currency = getUserIncomeCurrencyCode($result_search['income_currency']);
				$salary_currency = $result_search['income_currency'];
				$fam_type = $result_search['fam_type'];
				
				$search_data = 'Profile-'.$u_code;
				$pro_url = $search_data.".html";
				$search_user_fname=$result_search['firstname'];
				$search_user_name = $result_search['firstname']." ".$result_search['lastname'];
				$prefer_user_id =$result_search['idx'];
				$photo =$result_search['photo'];
				$today = date('Y-m-d');
				$diff = date_diff(date_create($dobx), date_create($today));
				$age_search_user = $diff->format('%y');
				$age_search_user_month = $diff->format('%m');
				$site_url = getWebsiteBasePath();

				if($i==0 || ($i%3==0))
				{
					$output .= "<div class='row'>";
				}

				$output .= "<a href='$pro_url' style='text-decoration:none; color:#000000;'>
							<div class='card-my-matches col-xs-12 col-sm-12 col-md-4 col-lg-4'>
							<span itemscope itemtype='http://schema.org/Person'>
								<meta itemprop='name' content='".ucwords($search_user_fname)."'/>
								<link itemprop='url' href='".$site_url.'/users/'.$pro_url."'/>
								<meta itemprop='address' content='".$city_Name.','.$state_Name.','.$country_Name."'/>
								<meta itemprop='birthDate' content='$dispaly_user_dob'/>
								<meta itemprop='gender' content='$meta_gender'/>
								<meta itemprop='givenName' content='".ucwords($search_user_fname)."'/>
								<meta itemprop='hasOccupation' content='$occupation_name'/>
								<meta itemprop='height' content='$height'/>
								<meta itemprop='homeLocation' content='$city_Name'/>
								<meta itemprop='jobTitle' content='$occupation_name'/>
								<meta itemprop='knowsLanguage' content='".getUserMotherTongueName($mother_tongue)."'/>
								<meta itemprop='nationality' content='$country_Name'/>
								<meta itemprop='netWorth' content='$salary'/>
								<meta itemprop='weight' content='$weight'/>
							</span>";
						if($photo=='' || $photo==null)
						{
							$output .= "<img src='$site_url/images/no_profile_pic.png'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
						}
						else
						{
							$output .= "<img src='$site_url/users/uploads/$prefer_user_id/profile/$photo'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
						}

						$output .= "<br><br>";
						$output .= "<h4>
										<b>
											<strong>
												".ucwords($search_user_fname)."
											</strong>
										</b>
									</h4>";
						$output .= "<p class='title'>
										Profile ID: $u_code<br>
										From ".substr(getUserCityName($city),0,11)."
									</p>";
						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Age: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp
												
											$age_search_user Years.
											</p>
										</div>
									</div>";
						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Height: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
												&nbsp";
												
												if($height!='' || $height!=null)
												{
													$search_user_height_meter = round(($height)/100,2);
								                	$search_user_height_foot = round(($height)/30.48,2);
								                	$output .= " $height-cms/ $search_user_height_foot-fts/ $search_user_height_meter-mts";
												}
												else
												{
													$output .=  "-NA-";
												}

						$output .= "		</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Religion: </strong>
											</p> 
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp";
							                	
											if($religion!='' || $religion!=null)
											{
												$output .= getUserReligionName($religion);
											}
											else
											{
												$output .= "-NA-";
											}

						$output .=	"</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Caste: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp";
							                	
											if($caste!='' || $caste!=null)
											{
												$output .= getUserCastName($caste);
											}
											else
											{
												$output .= "-NA-";
											}

						$output .=	"</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row' style='height:54px;'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Education: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
										 	&nbsp;";
							                	
											if($edu!='' || $edu!=null)
											{
												$output .= getUserEducationName($edu);
											}
											else
											{
												$output .= "-NA-";
											}

						$output .=	"</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row' style='height:54px;'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Occupation: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp;&nbsp;";
							                	
											if($occ!='' || $occ!=null)
											{
												$output .= user_occ($occ);
											}
											else
											{
												$output .= "-NA-";
											}
						$output .=	"</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Salary: </strong> 
											</p>
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp;";
							                	
											if($salary!='' || $salary!=null)
											{
												$output .= $currency." ".$salary." Monthly";
											}
											else
											{
												$output .= "-NA-";
											}

						$output .=	"</p>
										</div>
									</div>";

						$output .= "<div class='row member-profile-info-row'>
										<div class='col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile'>
											<p align='justify'>
												<strong>Mother Tongue: </strong>
											</p> 
										</div>
										<div class='col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data'>
											<p align='justify'>
											&nbsp;";
							                	
											if($mother_tongue!='' || $mother_tongue!=null)
											{
												$output .= getUserMotherTongueName($mother_tongue);
											}
											else
											{
												$output .= "-NA-";
											}

						$output .=	"</p>
										</div>
									</div>";

						$output .= "</div>
								</a>";

					$i++;
					if($i%3==0)
					{
						$output .= "</div>";
					}
			}
			
		}
		else
    	{
    		$output .= "<center><h4>Oops! No record found.</h4></center>";
    	}

    	echo $output;
	}
?>
