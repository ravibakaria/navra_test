<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string
	include('../../config/setup-values.php');   //get master setup values
	include('../../config/email/email_style.php');   //get master setup values
	include('../../config/email/email_templates.php');   //get master setup values
	include('../../config/email/email_process.php');

	$today_datetime = date('Y-m-d H:i:s');

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	$profile_id = getUserUniqueCode($user_id);

	$WebSiteBasePath = getWebsiteBasePath();
	$sitetitle = getWebsiteTitle();
	$logo_array=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo_array[] = $logoURL[$i];
		}

		$logo_path = implode('/',$logo_array);
	}

	if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }


	$task = quote_smart($_POST['task']);

	if($task=="Shortlist_Profile")       //profile shortlist
	{
		$user_id_x = $_POST['user_id_x'];
		$search_user_id = $_POST['search_user_id'];

		if($user_id!=$user_id_x)
		{
			echo "Unauthorised access.";
			exit;
		}

		$sql_chk = $link->prepare("SELECT * FROM `shortlisted` where `userid` = '$user_id_x' AND `short_id`='$search_user_id'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();

        if($count>0)
        {
        	echo "Already shortlisted!";
        	exit;
        }
        else
        {
        	$sql_insert_shortlist = "INSERT INTO `shortlisted`(`userid`,`short_id`,`created_at`) VALUES('$user_id_x','$search_user_id','$today_datetime')";

        	$ProfileId_Of_short_id = getUserUniqueCode($search_user_id);
        	$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id_x','shortlisted ','profile-$ProfileId_Of_short_id','$IP_Address',now())";

        	if($link->exec($sql_insert_shortlist) && $link->exec($sql_member_log))
			{
	        	echo "success";
				exit;
	        }
        }
	}

	if($task=="Interest_Profile")       //profile interest
	{
		$user_id_x = $_POST['user_id_x'];
		$search_user_id = $_POST['search_user_id'];

		if($user_id!=$user_id_x)
		{
			echo "Unauthorised access.";
			exit;
		}

		$sql_chk = $link->prepare("SELECT * FROM `interested` where `userid` = '$user_id_x' AND `inter_id`='$search_user_id'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();
		
        if($count>0)
        {
        	echo "Interest Sent Already!";
        	exit;
        }
        else
        {
        	$sql_insert_interest = "INSERT INTO `interested`(`userid`,`inter_id`,`created_at`) VALUES('$user_id_x','$search_user_id','$today_datetime')";

        	$ProfileId_Of_inter_id = getUserUniqueCode($search_user_id);
        	$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id_x','send interest','to profile-$ProfileId_Of_inter_id','$IP_Address',now())";

        	if($link->exec($sql_insert_interest))
			{
				/****  sender user   ****/
				
				$profilePic = getUserProfilePic($user_id_x);
				$ProfileId = getUserUniqueCode($user_id_x);
				$FirstName = getUserFirstName($user_id_x);
				$City = getUserCity($user_id_x);
				$State = getUserState($user_id_x);
				$Country = getUserCountry($user_id_x);
				$Age = getUserAge($user_id_x);
				$Education = getUserEducation($user_id_x);

				if($profilePic!='' || $profilePic!=null)
				{
					$profilePic_show = $WebSiteBasePath.'/users/uploads/'.$user_id_x.'/profile/'.$profilePic;
				}
				else
				{
					$profilePic_show = $WebSiteBasePath.'/images/no_profile_pic.png';
				}
				
				$sender_profile .= "<center>
								<div class='card-my-matches' style='width:100%;'>
									<a href='$WebSiteBasePath/users/Profile-$ProfileId.html' style='text-decoration:none;color:#000000;' target='_blank'>
										<img src='$profilePic_show' class='profile_img member-profile-image' alt='$FirstName' title='$FirstName' style='width:100%;height:auto;'/>
										<h4><b><strong>".ucwords($FirstName)."</strong></b></h4>
										<p class='title'>
											Profile ID: $ProfileId <br/>
										</p>
										City: $City<br/>
										State: $State<br/>
										Country: $Country<br/>
										Age: $Age Years<br/>
										Education: $Education<br/>
									</a>
								</div>
							</center>";

				/****  interest sent to user   ****/
				$first_name = getUserFirstName($search_user_id);
				$email_search_user = getUserEmail($search_user_id);

				$SocialSharing = getSocialSharingLinks();   // social sharing links
				
				$interest_received_page_link = "<a href='$WebSiteBasePath/users/interested-prospects.php'>More Details</a>";

	            $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

				$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

				$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

				$InterestRecievedMessage = str_replace(array('$first_name','$site_name','$sender_profile','$interest_received_page_link','$signature'),array($first_name,$WebSiteTitle,$sender_profile,$interest_received_page_link,$GlobalEmailSignature),$InterestRecievedMessage);  //replace footer variables with value
				//echo $InterestRecievedMessage;exit;
				$subject = $InterestRecievedSubject;
				$message  = '<!DOCTYPE html>';
				$message .= '<html lang="en">
					<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width">
					<title></title>
					<style type="text/css">'.$EmailCSS.'</style>
					</head>
					<body style="margin: 0; padding: 0;">';
				//echo $message;exit;
				$message .= $EmailGlobalHeader;

				$message .= $InterestRecievedMessage;

				$message .= $EmailGlobalFooter;

				$mailto = $email_search_user;
				$mailtoname = $first_name;
		//echo $message;exit;
				$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

				$sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$search_user_id','interest received','from $ProfileId',now())";
            	$link->exec($sql_member_email_log);

				if($emailResponse == 'success')
				{
					$errorMessage = "success";
				}
				else
				{
					$errorMessage = "Error in sending verification link";
				}
	        }
        }
        echo $errorMessage;
        exit;
	}

	if($task=="Request_Photo")       //request photo
	{
		$user_id_x = $_POST['user_id_x'];
		$search_user_id = $_POST['search_user_id'];

		$search_user_firstname = getUserFirstName($search_user_id);
		$search_user_emailid = getUserEmail($search_user_id);

		if($user_id!=$user_id_x)
		{
			echo "Unauthorised access.";
			exit;
		}

		$sql_chk = $link->prepare("SELECT * FROM `requestphoto` where `userid` = '$user_id_x' AND `requested_id`='$search_user_id'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();
        //echo $count;exit;
        if($count>0)
        {
        	echo "Photo requested already!";
        	exit;
        }
        else
        {
        	$profilePic = getUserProfilePic($user_id_x);
			$ProfileId = getUserUniqueCode($user_id_x);
			$FirstName = getUserFirstName($user_id_x);
			$City = getUserCity($user_id_x);
			$State = getUserState($user_id_x);
			$Country = getUserCountry($user_id_x);
			$Age = getUserAge($user_id_x);
			$Education = getUserEducation($user_id_x);

			if($profilePic!='' || $profilePic!=null)
			{
				$profilePic_show = $WebSiteBasePath.'/users/uploads/'.$user_id_x.'/profile/'.$profilePic;
			}
			else
			{
				$profilePic_show = $WebSiteBasePath.'/images/no_profile_pic.png';
			}

			$sender_profile .= "<center>
							<div class='card-my-matches' style='width:100%;'>
								<a href='$WebSiteBasePath/users/Profile-$ProfileId.html' style='text-decoration:none;color:#000000;' target='_blank'>
									<img src='$profilePic_show' class='profile_img member-profile-image' alt='$FirstName' title='$FirstName' style='width:100%;height:auto;'/>
									<h4><b><strong>".ucwords($FirstName)."</strong></b></h4>
									<p class='title'>
										Profile ID: $ProfileId <br/>
									</p>
									City: $City<br/>
									State: $State<br/>
									Country: $Country<br/>
									Age: $Age Years<br/>
									Education: $Education<br/>
								</a>
							</div>
						</center>";

        	$sql_insert_interest = "INSERT INTO `requestphoto`(`userid`,`requested_id`,`requested_at`) VALUES('$user_id_x','$search_user_id','$today_datetime')";
 			//echo $sql_insert_interest;exit;

        	$ProfileId_Of_userTo = getUserUniqueCode($search_user_id);
 			$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id_x','requested photo','to profile-$ProfileId_Of_userTo','$IP_Address',now())";

        	if($link->exec($sql_insert_interest))
			{
				$link->exec($sql_member_log);     //activity log entry

				$upload_photo_link	= "<a href='$WebSiteBasePath/users/my-photos.php' target='_blank' style='text-decoration:none;'>Upload Photo</a>";

	    		$SocialSharing = getSocialSharingLinks();   // social sharing links

				$EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

				$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

				$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

				$RequestPhotoMessage = str_replace(array('$first_name','$site_name','$sender_profile','$upload_photo_link','$signature'),array($FirstName,$WebSiteTitle,$sender_profile,$upload_photo_link,$GlobalEmailSignature),$RequestPhotoMessage);  //replace footer variables with value

				$subject = $RequestPhotoSubject;
				$message  = '<!DOCTYPE html>';
				$message .= '<html lang="en">
					<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width">
					<title></title>
					<style type="text/css">'.$EmailCSS.'</style>
					</head>
					<body style="margin: 0; padding: 0;">';
				//echo $message;exit;
				$message .= $EmailGlobalHeader;

				$message .= $RequestPhotoMessage;

				$message .= $GlobalEmailSignature;

				$message .= $EmailGlobalFooter;

			
				$mailto = $search_user_emailid;
				$mailtoname = $search_user_firstname;
				$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

				$sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$requested_id','photo request','from $ProfileId',now())";
            	$link->exec($sql_member_email_log);

				if($emailResponse == 'success')
				{
					echo "success";
				}
				else
				{
					echo "Error in sending verification link";
					//$errorMessage = $emailResponse;
				}
				exit;
	        }
        }
	}

	if($task=="Like_Profile")       //profile like
	{
		$userid = $_POST['userid'];
		$liked_userid = $_POST['liked_userid'];

		if($user_id!=$userid)
		{
			echo "Unauthorised access.";
			exit;
		}

		$sql_chk = $link->prepare("SELECT * FROM `profile_like` where `userid` = '$userid' AND `liked_userid`='$liked_userid'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();

        if($count>0)
        {
        	echo "You have already liked this profile";
        	exit;
        }
        else
        {
        	$sql_insert_liked_profile = "INSERT INTO `profile_like`(`userid`,`liked_userid`,`liked_on`) VALUES('$userid','$liked_userid','$today_datetime')";

        	$ProfileId_Of_userTo = getUserUniqueCode($liked_userid);
        	$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$userid','liked','profile-$ProfileId_Of_userTo','$IP_Address',now())";

        	if($link->exec($sql_insert_liked_profile))
			{
				$link->exec($sql_member_log);    // activity log

				$profilePic = getUserProfilePic($userid);
				$ProfileId = getUserUniqueCode($userid);
				$FirstName = getUserFirstName($userid);
				$City = getUserCity($userid);
				$State = getUserState($userid);
				$Country = getUserCountry($userid);
				$Age = getUserAge($userid);
				$Education = getUserEducation($userid);

				if($profilePic!='' || $profilePic!=null)
				{
					$profilePic_show = $WebSiteBasePath.'/users/uploads/'.$userid.'/profile/'.$profilePic;
				}
				else
				{
					$profilePic_show = $WebSiteBasePath.'/images/no_profile_pic.png';
				}

				$sender_profile .= "<center>
								<div class='card-my-matches' style='width:100%;'>
									<a href='$WebSiteBasePath/users/Profile-$ProfileId.html' style='text-decoration:none;color:#000000;' target='_blank'>
										<img src='$profilePic_show' class='profile_img member-profile-image' alt='$FirstName' title='$FirstName' style='width:100%;height:auto;'/>
										<h4><b><strong>".ucwords($FirstName)."</strong></b></h4>
										<p class='title'>
											Profile ID: $ProfileId <br/>
										</p>
										City: $City<br/>
										State: $State<br/>
										Country: $Country<br/>
										Age: $Age Years<br/>
										Education: $Education<br/>
									</a>
									<br/>
									<a href='$WebSiteBasePath/users/Profile-$ProfileId.html'><button  style='background-color:$primary_color;color:$primary_font_color;padding:5px;'><b>View Profile</b></button>
									</a>
								</div>
							</center>";

				/****  interest sent to user   ****/
				$first_name = getUserFirstName($liked_userid);
				$email_search_user = getUserEmail($liked_userid);

				$like_received_page_link = "<a href='$WebSiteBasePath/users/profile-likes.php'>More Details</a>";

				$SocialSharing = getSocialSharingLinks();   // social sharing links
				
	            $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

				$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

				$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

				$LikeRecievedMessage = str_replace(array('$first_name','$site_name','$sender_profile','$like_received_page_link','$signature'),array($first_name,$WebSiteTitle,$sender_profile,$like_received_page_link,$GlobalEmailSignature),$LikeRecievedMessage);  //replace footer variables with value
				//echo $InterestRecievedMessage;exit;
				$subject = $LikeRecievedSubject;
				$message  = '<!DOCTYPE html>';
				$message .= '<html lang="en">
					<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width">
					<title></title>
					<style type="text/css">'.$EmailCSS.'</style>
					</head>
					<body style="margin: 0; padding: 0;">';
				//echo $message;exit;
				$message .= $EmailGlobalHeader;

				$message .= $LikeRecievedMessage;

				$message .= $EmailGlobalFooter;

				$mailto = $email_search_user;
				$mailtoname = $first_name;
				
				$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

				$sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$liked_userid','liked profile','from $ProfileId',now())";
            	$link->exec($sql_member_email_log);

				if($emailResponse == 'success')
				{
					$errorMessage = "success";
				}
				else
				{
					$errorMessage = "Error in sending verification link";
				}


	        	echo $errorMessage;
				exit;
	        }
        }
	}
?>