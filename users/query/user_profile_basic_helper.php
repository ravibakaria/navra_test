<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$today_datetime = date('Y-m-d H:i:s');

	$task = quote_smart($_POST['task']);

	if($task=="Update_Basic_Info") 
	{
		$firstname = quote_smart($_POST['firstname']);
		$lastname = quote_smart($_POST['lastname']);
		$dob = quote_smart($_POST['dob']);
		$phonenumber = quote_smart($_POST['phonenumber']);
		$height = quote_smart($_POST['height']);
		$body_type = quote_smart($_POST['body_type']);
		$weight = quote_smart($_POST['weight']);
		$complexion = quote_smart($_POST['complexion']);
		$eat_habbit = quote_smart($_POST['eat_habbit']);;
		$marital_status = quote_smart($_POST['marital_status']);
		$smoke_habbit = quote_smart($_POST['smoke_habbit']);
		$special_case = quote_smart($_POST['special_case']);
		$drink_habbit = quote_smart($_POST['drink_habbit']);

		$age = (date('Y') - date('Y',strtotime($dob)));

		if($age<18)
		{
			echo "age_error";
			exit;
		}
		
		$sql_chk = $link->prepare("SELECT name FROM `profilebasic` WHERE `userid`='$user_id'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();
        
		if($firstname!='' && $lastname!='' && $dob!='')
		{	
			$sql_chk_client_info = "SELECT dob,phonenumber FROM `clients` WHERE `id`<>'$user_id' AND phonenumber='$phonenumber'";
			$sql_chk_client_info = $link->prepare($sql_chk_client_info); 
	        $sql_chk_client_info->execute();
	        $count_client_info=$sql_chk_client_info->rowCount();

	        if($count_client_info>0)
	        {
	        	echo "Duplicate mobile number! Please use different mobile number.";
				exit;
	        }
	        else
	        {
	        	$sql_update = "UPDATE `clients` SET `firstname`='$firstname',`lastname`='$lastname',`dob`='$dob',`phonenumber`='$phonenumber',`updated_at`='$today_datetime' WHERE `id`='$user_id'";
	        }
			
			if($count==0)
			{
				$sql_update2 = "INSERT INTO `profilebasic`(userid,name,height,weight,eat_habbit,smoke_habbit,body_type,complexion,marital_status,special_case,drink_habbit,created_at) VALUES('$user_id','$firstname','$height','$weight','$eat_habbit','$smoke_habbit','$body_type','$complexion','$marital_status','$special_case','$drink_habbit','$today_datetime')";
			}
			else
			if($count>0)
			{
				$sql_update2 = "UPDATE `profilebasic` SET `height`='$height',`weight`='$weight',`eat_habbit`=$eat_habbit,`smoke_habbit`='$smoke_habbit',`body_type`='$body_type',`complexion`=$complexion,`marital_status`='$marital_status',`special_case`='$special_case',`drink_habbit`=$drink_habbit,`updated_at`='$today_datetime' WHERE `userid`='$user_id'";
			}
			
			$profile_completeness_entry = getUserProfileCompleteEntry($user_id);
			if($profile_completeness_entry=='0')
			{
				$sql_update_profile = "INSERT INTO `profile_completeness`(`userid`,`basic_info`) VALUES('$user_id','1')";
			}
			else
			{
				$sql_update_profile = "UPDATE `profile_completeness` SET `basic_info`='1' WHERE `userid`='$user_id'";
			}

			$link->exec($sql_update_profile);

			if($link->exec($sql_update) && $link->exec($sql_update2))
			{
				$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','updated','basic info','$IP_Address',now())";
				$link->exec($sql_member_log);
				
				echo "success";
				exit;
			}
			else
			{
				echo "Something went wrong. Try after some time.";
				exit;
			}
		}
		else
		{
			echo "Invalid parameters.";
			exit;
		}
	}
?>