<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string

	$today_datetime = date('Y-m-d H:i:s');

	$task = quote_smart($_POST['task']);

	if($task=="User_Change_Password") 
	{
		$crr_pwd = $_POST['crr_pwd'];
		$new_pwd = $_POST['new_pwd'];
		$cnf_new_pwd = $_POST['cnf_new_pwd'];

		$MinimumUserPasswordLength = getMinimumUserPasswordLength();
		$MaximumUserPasswordLength = getMaximumUserPasswordLength();

		if($MinimumUserPasswordLength=='0' || $MinimumUserPasswordLength=='' || $MinimumUserPasswordLength==null)
		{
			$MinimumUserPasswordLength='8';
		}

		if($MaximumUserPasswordLength=='0' || $MaximumUserPasswordLength=='' || $MaximumUserPasswordLength==null)
		{
			$MaximumUserPasswordLength='40';
		}


		$crr_pwd_hash = md5(trim($_POST['crr_pwd']));

		$sql  = "SELECT * FROM `clients` WHERE id='$user_id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $userRow = $stmt->fetch();

        $db_pass = $userRow['password'];

        if($crr_pwd_hash == $db_pass)
        {
        	$haveuppercase = preg_match('/[A-Z]/', $new_pwd);
			$havenumeric = preg_match('/[0-9]/', $new_pwd);
			$havespecial = preg_match('/[!@#$%^&)*_(+=}{|:;,.<>}]/', $new_pwd);

			if (!$haveuppercase)
			{
				$errorMessage = 'Password must have atleast one upper case character.';
			}
			else if (!$havenumeric)
			{
				$errorMessage = 'Password must have atleast one digit.';
			}
			else if (!$havespecial)
			{
				$errorMessage = 'Password must have atleast one of the special characters [!@#$%^&)*_(+=}{|:;,.<>}]';
			}
			else if (strlen($new_pwd) < $MinimumUserPasswordLength)
			{
				$errorMessage = 'Password must be of minimum 8 characters long.';
			}
			else if (strlen($new_pwd) > $MaximumUserPasswordLength)
			{
				$errorMessage = 'Password must be of maximum 15 characters long.';
			}
			else
			{
				$updated_pass = md5(trim($new_pwd));
				$sql_update = "UPDATE clients SET password='$updated_pass',updated_at='$today_datetime' WHERE id='$user_id'";

				$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','update','login password','$IP_Address',now())";

				if($link->exec($sql_update) && $link->exec($sql_member_log))
				{
					$errorMessage = "success";
				}
				else
				{
					$errorMessage = "Something went wrong. Try after some time.";
				}
			}
        }
        else
        {
        	$errorMessage =  "Current password does not match with our records! Please try after some time.";
        }

        echo $errorMessage;
        exit;
	}
?>