<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	$user_id = $_SESSION['user_id'];
	$email = $_SESSION['user_email'];
	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string
	include "../../config/setup-values.php";

	$today_datetime = date('Y-m-d H:i:s');
    
	$task = quote_smart($_POST['task']);

	/************   Fetch profile info   ******************/
	if($task=="fetch_profile_info") 
	{
		$output = null;
		$profile_id = $_POST['profile_id'];

		$sql_search_user_info = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.unique_code='$profile_id'";
			
		$stmt   = $link->prepare($sql_search_user_info);
        $stmt->execute();
        $count = $stmt->rowCount();

        if($count>0)
        {
	    	$base_path = $WebSiteBasePath.'/';
	    	$today = date('Y-m-d');
	    	$photo = null;
	    	$row = $stmt->fetch();

	    	$city = $row['city'];
			$city_Name = getUserCityName($city);
			$state = $row['state'];
			$state_Name = getUserStateName($state);
			$country = $row['country'];
			$country_Name = getUserCountryName($country);
			$edu = $row['education'];
			$occ = $row['occupation'];
			$religion = $row['religion'];
			$caste = $row['caste'];
			$mother_tongue = $row['mother_tongue'];
			$u_code = $row['unique_code'];
			$dobx = $row['dob'];
			$dispaly_user_dob = date('d-M-Y',strtotime($dobx));
			$profileid = $row['id'];
			$gender = $row['gender'];
			$height = $row['height'];
			$weight = $row['weight'];
			if($gender=='1')
	        {
	        	$meta_gender = "Male";
	        }
	        else
	        if($gender=='2')
	        {
	        	$meta_gender = "Female";
	        }
	        else
	        if($gender=='3')
	        {
	        	$meta_gender = "T-Gender";
	        }
	        $occupation = $row['occupation'];
			$occupation_name = getUserEmploymentName($occupation);
			$salary = $row['income'];
			$fam_type = $row['fam_type'];

			$search_data = 'Profile-'.$u_code;
			$pro_url = $search_data.".html";
			$search_user_fname=$row['firstname'];
			$search_user_name = $row['firstname']." ".$row['lastname'];
			$prefer_user_id =$row['idx'];
			$photo =$row['photo'];
			$today = date('Y-m-d');
			$diff = date_diff(date_create($dobx), date_create($today));
			$age_search_user = $diff->format('%y');
			$user_occ = user_occ($occ);
			$site_url = getWebsiteBasePath();
	
	        $output .= "<div>";
		        $output .= "<a href='$pro_url' target='_blank' style='text-decoration: none;color:#000000;'>";
			        $output .= "<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>";
				        $output .= "<center>";
					        $output .= "<div class='profile-sidebar'>";
					        	$output .= "<div class='profile-usertitle'>
					        					<div class='profile-usertitle-name'>
					        						<strong>
					        						".ucwords($search_user_fname)."<br/> From ".substr(getUserCityName($city),0,11)."
					        						</strong>
					        					</div>
											</div>";
								$output .= "<!-- SIDEBAR USERPIC -->
											<div class='profile-userpic'>
												<center>";
									if($photo=='' || $photo==null)
									{
										$output .= "<img itemprop='image' src='$site_url/images/no_profile_pic.png'  class='profile_img' alt='$search_user_fname' title='$search_user_fname'/>";
									}
									else
									{
										$output .= "<img itemprop='image' src='$site_url/users/uploads/$prefer_user_id/profile/$photo'  class='profile_img' alt='$search_user_fname' title='$search_user_fname'/>";
									}

									$output .="</center>
											</div>
											<!-- END SIDEBAR USERPIC -->";
								$output .="</div>
									</center>";
							$output .= "</div>";
					    $output .= "</center>";
			        $output .= "</div>";

			        $output .= "<div class='col-xs-12 col-sm-12 col-md-8 col-lg-8 profile_info_data'>";
			        	$output .= "</center>";
			        		$output .= "<div class='row'>
			        						<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
			        							<strong>Age: </strong> 
											</div>
											<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>
												&nbsp ".$age_search_user." Years.
											</div>
										</div>";
							$output .= "<div class='row'>
			        						<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
			        							<strong>Height: </strong> 
											</div>
											<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>
												&nbsp";
												
												if($height!='' || $height!=null)
												{
													$search_user_height_meter = round(($height)/100,2);
											        $search_user_height_foot = round(($height)/30.48,2);
											        $output .= $height.'-cms/ '.$search_user_height_foot.'-fts/ '.$search_user_height_meter.'-mts';
												}
												else
												{
													$output .= "-NA-";
												}
											$output .= "</div>
										</div>";
							$output .= "<div class='row'>
			        						<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
			        							<strong>Religion: </strong> 
											</div>
											<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>
												&nbsp";
												
												if($religion!='' || $religion!=null)
												{
													$output .= getUserReligionName($religion);
												}
												else
												{
													$output .= "-NA-";
												}
											$output .= "</div>
										</div>";
							$output .= "<div class='row'>
			        						<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
			        							<strong>Caste: </strong> 
											</div>
											<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>
												&nbsp";
												
												if($caste!='' || $caste!=null)
												{
													$output .= getUserCastName($caste);
												}
												else
												{
													$output .= "-NA-";
												}
											$output .= "</div>
										</div>";
							$output .= "<div class='row'>
			        						<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
			        							<strong>Education: </strong> 
											</div>
											<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>
												&nbsp";
												
												if($edu!='' || $edu!=null)
												{
													$output .= getUserEducationName($edu);
												}
												else
												{
													$output .= "-NA-";
												}
											$output .= "</div>
										</div>";
							$output .= "<div class='row'>
			        						<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
			        							<strong>Occupation: </strong> 
											</div>
											<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>
												&nbsp";
												
												if($occ!='' || $occ!=null)
												{
													$output .= user_occ($occ);
												}
												else
												{
													$output .= "-NA-";
												}
											$output .= "</div>
										</div>";
							$output .= "<div class='row'>
			        						<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
			        							<strong>Salary: </strong> 
											</div>
											<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>
												&nbsp";
												
												if($salary!='' || $salary!=null)
												{
													$output .= $salary." Monthly";
												}
												else
												{
													$output .= "-NA-";
												}
											$output .= "</div>
										</div>";
							$output .= "<div class='row'>
			        						<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
			        							<strong>Mother Tongue: </strong> 
											</div>
											<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>
												&nbsp";
												
												if($mother_tongue!='' || $mother_tongue!=null)
												{
													$output .= getUserMotherTongueName($mother_tongue);
												}
												else
												{
													$output .= "-NA-";
												}
											$output .= "</div>
										</div>";

						$output .= "</center>";	
					$output .= "</div>";
		        $output .= "</a>";
		        $output .= "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
								<br/>
								<center>------- <a href='$pro_url' class='btn btn-sm btn-primary website-button' target='_blank'>View Profile</a> -------
								</center>
								<hr class='profile-seperator-mobile'>
							</div>";
		    $output .= "</div>";
	    }
	    else
	    {
	    	$output .= "no_record_found";
	    }

	    echo $output;
	    exit;
	}

	/************   Submit report abuse   ******************/
	if($task=="Submit_report_abuse") 
	{
		$profile_id = $_POST['fetched_profile_id'];
		$report_abuse_comment = quote_smart($_POST['report_abuse_comment']);

		$abused_user_id = getUserIdFromUniqueId($profile_id);

		$sql_chk = "SELECT * FROM `report_abuse` WHERE `abused_by`='$user_id' AND `abused_user_id`='$abused_user_id' AND `abused_user_profile_id`='$profile_id'";
		$stmt = $link->prepare($sql_chk);
		$stmt->execute();
		$count = $stmt->rowCount();

		if($count>0)
		{
			echo "You have already reported this profile";
			exit;
		}

		$sql_insert = "INSERT INTO `report_abuse`(`abused_by`,`abused_user_id`,`abused_user_profile_id`,`abused_comment`,`abused_on`) VALUES('$user_id','$abused_user_id','$profile_id','$report_abuse_comment','$today_datetime')";

		$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','reported abuse','profile-$profile_id','$IP_Address',now())";

		if($link->exec($sql_insert) && $link->exec($sql_member_log))
		{
			echo "success";
			exit;
		}
		else
		{
			echo "Something went wrong! Try after some time.";
			exit;
		}
	}
?>
