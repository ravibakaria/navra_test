<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	require_once "../../config/config.php";
	require_once "../../config/dbconnect.php";
	include('../../config/functions.php');       //strip query string
	include "../../config/setup-values.php";

	$logged_in = CheckUserLoggedIn();
	if($logged_in=='0')
	{
		echo "<script>window.location.assign('login.php');</script>";
		exit;
	}

	$user_id = $_SESSION['user_id'];

	$WebSiteBasePath = getWebsiteBasePath();
	$sitetitle = getWebsiteTitle();
	$logo_array=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo_array[] = $logoURL[$i];
		}

		$logo_path = implode('/',$logo_array);
	}

	if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive payment-receipt-logo-img' style='padding-left:35px;padding-bottom:5px;'/>";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }

   	$receipt_id = quote_smart($_POST['receipt_id']);
   	$output = null;

   	$sql_get_pp_details = "SELECT * FROM `payment_transactions` WHERE `id`='$receipt_id' AND userid='$user_id'";
   	$stmt_get_pp_details = $link->prepare($sql_get_pp_details);
   	$stmt_get_pp_details->execute();
   	$count_get_pp_details = $stmt_get_pp_details->rowCount();

   	if($count_get_pp_details==0)
   	{
   		$output = "<center><h1 style='color:red;'>ERROR! Invalid Parameter.<h1></center><br>";
   		exit;
   	}

   	$result_get_pp_details = $stmt_get_pp_details->fetch();
	$receipt_number = $result_get_pp_details['id'];
	$userid = $result_get_pp_details['userid'];
	$OrderNumber = $result_get_pp_details['OrderNumber'];
	$membership_plan = $result_get_pp_details['membership_plan'];
	$membership_plan_name = $result_get_pp_details['membership_plan_name'];
	$membership_plan_id = $result_get_pp_details['membership_plan_id'];
	$membership_contacts = $result_get_pp_details['membership_contacts'];
	$membership_plan_amount = $result_get_pp_details['membership_plan_amount'];
	$membership_plan_expiry_date = $result_get_pp_details['membership_plan_expiry_date'];
	$featured_listing = $result_get_pp_details['featured_listing'];
	$featured_listing_amount = $result_get_pp_details['featured_listing_amount'];
	$featured_listing_expiry_date = $result_get_pp_details['featured_listing_expiry_date'];
	$tax_applied = $result_get_pp_details['tax_applied'];
	$tax_name = $result_get_pp_details['tax_name'];
	$tax_percent = number_format(round($result_get_pp_details['tax_percent'],2),2,'.','');
	$tax_amount = $result_get_pp_details['tax_amount'];
	$total_amount = $result_get_pp_details['total_amount'];
	$tenure = $result_get_pp_details['tenure'];
	$transact_id = $result_get_pp_details['transact_id'];
	$request_id = $result_get_pp_details['request_id'];
	$payment_gateway = $result_get_pp_details['payment_gateway'];
	$payment_method = $result_get_pp_details['payment_method'];
	$created_at = $result_get_pp_details['created_at'];
	$status = $result_get_pp_details['status'];

	if($status=='0')
	{
		$status_display = 'UNPAID';
	}
	else
	if($status=='1')
	{
		$status_display = 'PAID';
	}
	else
	if($status=='2')
	{
		$status_display = 'CANCELED';
	}
	else
	if($status=='3')
	{
		$status_display = 'REFUND';
	}


	if($membership_contacts=='0')
	{
		$membership_contacts="Unlimited";
	}

	/************   User info Details start   **************/
	$first_name = getUserFirstName($user_id);
	$last_name = getUserLastName($user_id);
	$user_name = $first_name." ".$last_name;
	$email = getUserEmail($user_id);
	$mobile = getUserMobile($user_id);
	$phone = '+'.getUserCountryPhoneCode($user_id).'-'.getUserMobile($user_id);
	$profileId = getUserUniqueCode($user_id);

	/************   User info Details End   **************/

	$DefaultCurrency = getDefaultCurrency();
    $DefaultCurrencyCode = getDefaultCurrencyCode($DefaultCurrency);

    $purchase_date = date('d F Y',strtotime($created_at));

    $subtotal = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');

    $total_tax = number_format(round($tax_amount,2),2,'.','');
    $total_amount_paid = number_format(round($total_amount,2),2,'.','');	

	$output = "<div class='container'>
		<div class='row'>
	        <div class='well payment-summary-well col-xs-12 col-sm-12 col-md-7 col-md-offset-3'>
	        	<div class='row payment-logo-row' style='width:110%;margin-left:-19px;'>
					<div class='col-xs-12 col-sm-12 col-md-12'>
						$logo
					</div>
				</div>
				<br/>
				<div class='row'>
					<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center'>
						<h3>Payment Receipt</h3>
						<br>
					</div>
				</div>

	            <div class='row'>
	                <div class='col-xs-6 col-sm-6 col-md-6'>
	                	<strong>To:</strong>
	                    <address>
	                        <strong>$user_name</strong>
	                        <br>
	                        Email: $email
	                        <br>
	                        Profile Id: $profileId
	                        <br>
	                        Phone: $phone
	                    </address>
	                </div>
	                <div class='col-xs-6 col-sm-6 col-md-6 text-right'>
	                	<br>
	                	<address>
	                        <strong>Date:</strong>
	                        $purchase_date<br>
	                        <strong>Receipt Number:</strong>
	                        $receipt_number<br>
	                        <strong>Order Number:</strong>
	                        $OrderNumber<br>
	                        <strong>Payment Status:</strong>
	                        $status_display
	                    </address>
	                </div>
	            </div>

	            <div class='row'>
	            	<div class='col-xs-12 col-sm-12 col-md-12'>
		                <table id='datatable-info' class='table-responsive table-hover table-bordered' style='width:100%'>
		                    <thead>
		                        <tr>
		                            <th>Description</th>
		                            <th>Amount</th>
		                        </tr>
		                    </thead>
		                    <tbody>";
	?>
		                    	<?php
		                    		if($membership_plan=='Yes')
		                    		{
		                    			$membership_plan_description = $membership_plan_name." Membership Plan - ".$membership_contacts." Contacts - Validity ".$tenure." Months";
		                    			$output .= "<tr>";
		                    				$output .= "<td>".$membership_plan_description."</td>";
		                    				$output .= "<td style='text-align:right'>".$DefaultCurrencyCode.":".$membership_plan_amount."</td>";
		                    			$output .= "</tr>";
		                    		}
		                    	?>
		                     
		                        <?php
		                        	if($featured_listing=='Yes')
		                        	{
		                        		$featured_plan_description = " Featured Listing -  Validity ".$tenure." Months";
		                    			$output .= "<tr>";
		                    				$output .= "<td>".$featured_plan_description."</td>";
		                    				$output .= "<td style='text-align:right'>".$DefaultCurrencyCode.":".$featured_listing_amount."</td>";
		                    			$output .= "</tr>";
		                    		}
		                        

		                        	$output .= "<tr>
			                        	<td style='text-align:right'><b>Sub Total<b></td>
			                        	<td style='text-align:right'> $DefaultCurrencyCode:$subtotal</td>
			                        </tr>";
								?>
		                        <?php
		                        	if($tax_applied=='1')
		                        	{
		                        		$output .= "<tr>";
		                    				$output .= "<td style='text-align:right'><b>".$tax_percent."% ".$tax_name."</b></td>";
		                    				$output .= "<td style='text-align:right'>".$DefaultCurrencyCode.":".$tax_amount."</td>";
		                    			$output .= "</tr>";
		                        	}
		                        ?>

		                        <?php
		                        	$output .= "<tr>";
	                    				$output .= "<td style='text-align:right'><b>Total</b></td>";
	                    				$output .= "<td style='text-align:right'>".$DefaultCurrencyCode.":".$total_amount."</td>";
	                    			$output .= "</tr>";
		                        
		                    $output .= "</tbody>
							                </table>
							            </div>
						            </div>

						            <br/>";

	            $output .= "<div class='row'>
	            	<div class='col-xs-12 col-sm-12 col-md-12'>
	                	<table id='datatable-info' class='table-responsive table-hover table-bordered' style='width:100%''>
	                		<tr>
	                            <th>Transaction Date</th>
	                            <td>$purchase_date</td>
	                        </tr>
	                        <tr>
	                            <th>Gateway</th>
	                            <td>$payment_gateway</td>
	                        </tr>
	                        <tr>
	                            <th>Payment Method</th>
	                            <td>$payment_method</td>
	                        </tr>
	                        <tr>
	                            <th>Transaction ID</th>
	                            <td>$transact_id</td>
	                        </tr>
	                        <tr>
	                            <th>Amount</th>
	                            <td>$DefaultCurrencyCode:$total_amount</td>
	                        </tr>
	                	</table>
	                </div>
	            </div>
	            <br/><br/>
	            <div class='row payment-footer-row'>
					<div class='col-xs-12 col-sm-12 col-md-12'>
						<br/>
						<h4><?php echo $sitetitle;?></h4>";
	?>
						<?php
							$stmt   = $link->prepare("SELECT * FROM `contact_us`");
		                    $stmt->execute();
		                    $result = $stmt->fetch();
		                    $count=$stmt->rowCount();

		                    if($count>0 && $result['companyName']!='')
		                    {
		                        $enquiry_email = $result['enquiry_email'];
		                        $companyName = $result['companyName'];
		                        $street = $result['street'];
		                        $city = $result['city'];
		                        $state = $result['state'];
		                        $country = $result['country'];
		                        $postalCode = $result['postalCode'];
		                        $phonecode = $result['phonecode'];
		                        $phone = $result['phone'];
		                        $companyEmail = $result['companyEmail'];
		                        $companyURL = $result['companyURL'];

		                        $city_name = getUserCityName($city);
		                        $state_name = getUserStateName($state);
		                        $country_name = getUserCountryName($country);

		                        $output .= "<b>Address: ".$street.", ".$city_name." - ".$postalCode.", ".$state_name." ".$country_name."</b><br/>";
		                        $output .= "<b>Phone: +".$phonecode." ".$phone." | Email: ".$companyEmail."</b><br/>";
		                        $output .= "<b>Website: ".$companyURL."</b>";
		                    }

						$output .= "<br/>
					</div>
				</div>
	        </div>
	    </div>
	</div>";


echo $output;
exit;
?>