<?php
	session_start();
	if(isset($_SESSION['last_page_accessed']))
	{
		unset($_SESSION['last_page_accessed']);
	}
	
	include("templates/header.php");

	$site_title = getWebsiteTitle();
	$site_tagline = getWebSiteTagline();
	$site_url = getWebsiteBasePath();

	$logo=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo[] = $logoURL[$i];
		}

		$logo = implode('/',$logo);
	}
?>
<html class="fixed sidebar-left-collapsed">
	<title>
		<?php
			echo "Advanced Matrimonial Profile Search - ".getWebsiteTitle();
		?>
	</title>

	<meta name="title" content="<?= $siteName.' - Matrimonial Profile Search'; ?>" />

	<meta name="description" content="<?php echo getWebsiteTitle();?> Advanced Matrimonial Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members"/>


<?php
	$siteName = getWebsiteTitle();
	$siteTagline = getWebSiteTagline();
	$site_url = getWebsiteBasePath();
	$site_email = getWebSiteEmail();

	$logo=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo[] = $logoURL[$i];
		}

		$logo = implode('/',$logo);
	}
?>
	<meta name="keywords" content="<?php echo $siteName;?>, <?php echo $siteTagline; ?>, Advanced Matrimonial Profile Search">

	<meta property="og:title" content="<?php echo $siteName.' - '.$siteTagline;?>" />
  	<meta property="og:url" content="<?= $site_url; ?>" />
  	<meta property="og:description" content="<?php echo $siteName;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  	<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
  	<meta property="twitter:title" content="<?= $siteName.' - '.$siteTagline; ?>" />
  	<meta property="twitter:url" content="<?= $site_url; ?>" />
  	<meta property="twitter:description" content="<?php echo $siteName;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
  	<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />
  	
<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
<body style="background-color:#fff;">
	<section role="main" class="content-body main-section-start advanced-search-filters">
		
		<div class='row start_section_my_photos'>
			<div class='advanced-search-result-header'>
				<h1>Advanced Search</h1>
			</div>
		</div>
		<div class="row advanced-search-preference">
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 my-preference-result advanced-search-data">
				<div class="row" id="my_preferences">
					<div class="panel panel-info">
						<form method="POST" action="advanced-search-result.php">
							<div class="panel-body">

								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<?php 
											if(isset($_SESSION['logged_in']) && (isset($_SESSION['client_user']) && $_SESSION['client_user']!=''))
											{
												$user_gender = getUserGender($user_id);
											}
											else
											{
												$user_gender = null;
											}

											$sql_gender = "SELECT * FROM gender WHERE status='1'";
											$stmt = $link->prepare($sql_gender);
											$stmt->execute();
											$result=$stmt->fetchAll();

											foreach ($result as $row) 
											{
												$gender_id = $row['id'];
												$gender_name=$row['name'];
												if($gender_id=='1')
												{
													$gender_display="Groom";
												}
												else
												if($gender_id=='2')
												{
													$gender_display="Bride";
												}
												else
												$gender_display=$gender_name;
										

												echo "<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 comman_form_element'>				
														<input type='radio' name='gender' value='$gender_id' class='radio_1' id='radio_$gender_id'>  <label for='radio_$gender_id' class='control-label'>&nbsp;Looking For $gender_display </label>
												</div>";
											}
										?>
									</div>
								</div>

								<div class="clearfix">&nbsp;</div>

								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<?php
											$sql_marital_status = "SELECT * FROM maritalstatus WHERE status='1'";
											$stmt = $link->prepare($sql_marital_status);
											$stmt->execute();
											$result=$stmt->fetchAll();

											foreach ($result as $row) 
											{
												$marital_status_id = $row['id'];
												$marital_status_name=$row['name'];

												echo "<div class='col-xs-6 col-sm-6 col-md-2 col-lg-2 comman_form_element'>
									            	<input type='checkbox' name='marital_status[]' class='radio_1' value='$marital_status_id' id='marital_status_$marital_status_id'/> <label for='marital_status_$marital_status_id' class='control-label'>&nbsp;".$marital_status_name."</label>
									            </div>";
											}
										?>
									</div>
								</div>

								<div class="clearfix">&nbsp;</div>

								<!--  Age  -->
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							            <div class="row">
											<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
												<select class="form-control form control-sm from_date" name="from_date">
													<option value="0">--Minimum Age--</option>
													<?php
														for($i=18;$i<61;$i++)
														{
															echo "<option value='".$i."'>".$i."</option>";
														}
													?>
													<option value="61">60+</option>
												</select>
											</div>
											<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3  comman_form_element">
												<select class="form-control form control-sm to_date" name="to_date">
												<option value="0">--Maximum Age--</option>
												<?php
													for($i=18;$i<61;$i++)
													{
														echo "<option value='".$i."'>".$i."</option>";
													}
												?>
												<option value="61">60+</option>
												</select>
											</div>
										</div>
									</div>
								</div>

								<div class="clearfix">&nbsp;</div>

								<!--  Religious  -->
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
												<select class="form-control form control-sm religion" name="religion">
													<option value="0">--Religion--</option>
													<?php
														$query  = "SELECT * FROM `religion` ORDER BY `id` ASC";
														$stmt   = $link->prepare($query);
														$stmt->execute();
														$result = $stmt->fetchAll();
														foreach( $result as $row )
														{
															$religion_name =  $row['name'];
															$religion_id = $row['id']; 

															echo "<option value=".$religion_id.">".$religion_name."</option>";
														}
									              	?>
												</select>
											</div>
											<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
												<select class="form-control form control-sm caste" name="caste">
													<option value="0">--Caste--</option>
													<?php
									              		$query  = "SELECT * FROM `caste` ORDER BY `id` ASC";
														$stmt   = $link->prepare($query);
														$stmt->execute();
														$result = $stmt->fetchAll();
														foreach( $result as $row )
														{
															$caste_id = $row['id'];
															$caste_name = $row['name'];
															echo "<option value=".$caste_id.">".$caste_name."</option>";
														}
									              	?>
												</select>
											</div>
											
											<div class=" col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
												<select class="form-control form control-sm mother_tongue" name="mother_tongue">
													<option value="0">--Mother Tongue--</option>
													<?php
									              		$query  = "SELECT * FROM `mothertongue` ORDER BY `id` ASC";
														$stmt   = $link->prepare($query);
														$stmt->execute();
														$result = $stmt->fetchAll();
														foreach( $result as $row )
														{
															$mothertongue_name =  $row['name'];
															$mothertongue_id = $row['id'];

															echo "<option value=".$mothertongue_id.">".$mothertongue_name."</option>";
														}
									              	?>
												</select>
											</div>
										</div>
									</div>
								</div>

								<div class="clearfix">&nbsp;</div>

								<!--  Education/Occupation  -->
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
										<select class="form-control form control-sm education" name="education">
											<option value="0">--Education--</option>
											<?php
							              		$query  = "SELECT * FROM `educationname` ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$education_name =  $row['name'];
													$education_id = $row['id']; echo "<option value=".$education_id.">".$education_name."</option>";
												}
							              	?>
										</select>
									</div>

									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
										<select class="form-control form control-sm occupation" name="occupation">
											<option value="0">--Occupation--</option>
											<?php
							              		$query  = "SELECT * FROM `employment` ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$employment_name =  $row['name'];
													$employment_id = $row['id']; 

													echo "<option value=".$employment_id.">".$employment_name."</option>";
												}
							              	?>
										</select>
									</div>
								</div>

								<div class="clearfix">&nbsp;</div>

								<!--  Monthly Income  -->
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
										
										<select class="form-control form control-sm currency" name="currency">
											<option value="0">--Income Currency--</option>
											<?php
							              		$query  = "SELECT * FROM `currency` ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$currency_name =  $row['currency']." [".$row['code']."]";
													$currency_id = $row['id']; 

													echo "<option value=".$currency_id.">".$currency_name."</option>";
												}
							              	?>
										</select>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
										
										<input type="text" class="form-control monthly_income_from" name="monthly_income_from" maxlength="6" placeholder="Minimum Income">
									</div>

									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
										<input type="text" class="form-control monthly_income_to" name="monthly_income_to" maxlength="6" placeholder="Maximum Income">
									</div>
								</div>

								<div class="clearfix">&nbsp;</div>

								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
										<select class="form-control country" id="country"  name="country">
							              	<option value="0">Select Country</option>
							              	<?php
							              		$query  = "SELECT * FROM `countries` ORDER BY `name` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$country_name =  $row['name'];
													$country_id = $row['id']; 

													echo "<option value=".$country_id.">".$country_name."</option>";
												}
							              	?>
							            </select>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
										<select class="form-control state" id="state" name="state">
											<option value="0">Select State</option>
											
							            </select>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 comman_form_element">
										<select class="form-control city" id="city"  name="city">
											<option value="0">Select City</option>
											
							            </select>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:60px; height:60px; display:none;' alt='loader'/></center>
								<div class="row form_status">
									
								</div>
								<div class="row">
									<center><input type="submit" name="search" value="Search" class="btn btn-success btn-view-profile  btn_submit website-button" /></center>
								</div>
							</div>	
						</form>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<div class='skyscrapper-ad'>
		          	<?php
	            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
		            	if($display_skyscrapper_ad=='1')
		            	{
		              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
		            	}
		          	?>
		        </div>
			</div>
		</div>
		<br/>
		<div class='leaderboard-ad'>
          	<?php
            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
            	if($display_leaderboard_ad=='1')
            	{
              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
            	}
          	?>
        </div>
	<!-- end: page -->
	</section>
</div>
</body>
<?php
	include("templates/footer.php");
?>
</html>
<script>
	$(document).ready(function(){

		/*   Prevent entering charaters in monthly income  */
        $(".monthly_income_from").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                return false;
            }
         
        });

        /*   Prevent entering charaters in monthly income  */
        $(".monthly_income_to").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                return false;
            }
        });

		$('.country').change(function(){         //Fetch states
        	var country_id = $(this).val();
			var task = "Fetch_state_data";  

			$.ajax({
				type:'post',
            	data:'country_id='+country_id+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.state').html(res);
            	}
			});     	
        });

        $('.state').change(function(){         //Fetch cities
        	var state_id = $(this).val();
			var task = "Fetch_city_data";  
			$.ajax({
				type:'post',
            	data:'state_id='+state_id+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.city').html(res);
            	}
			});      	
        });

        $('.from_date').change(function(){         //Fetch cities
        	var from_date = $(this).val();
			var task = "Fetch_age_data";  
			$.ajax({
				type:'post',
            	data:'from_date='+from_date+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.to_date').html(res);
            	}
			});      	
        });

        $('.btn_profile_search').click(function(){
        	var profile_id = $('.profile_id').val();

        	if(profile_id=='' || profile_id==null)
        	{
        		$('.status_profile_id').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> Please enter profile id.</div>");
				return false;
        	}

        	window.location.assign('Profile-'+profile_id+'.html');
        	return false;
        });

        $('.btn_submit').click(function(){
        	var gender = $("input[name='gender']:checked"). val();
        	var marital_status = [];
			$('input[type=checkbox]:checked').each(function() {
    			marital_status .push(this.value);
			});

        	if(gender=='' || gender==null || gender=='undefined')
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 5px;margin-right: 5px;'><span class='fa fa-exclamation-circle'></span><strong> Oops!</strong> Please choose  whether you are looking for <strong>Bride / Groom / T-Gender</strong>.</div>");
				return false;
			}

			if(marital_status=='' || marital_status==null)
			{
				$('.form_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Oops!</strong> Select marital status to proceed further.</div>");
				return false;
			}

			$('.loading_img').show();
        });
    });
</script>