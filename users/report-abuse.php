<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}
	
	include("templates/header.php");
	$basepath = $WebSiteBasePath.'/';
	$user_id = $_SESSION['user_id'];

?>
<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>

<section role="main" class="content-body main-section-start">

	<!-- start: page -->
		<div class='row start_section_my_photos'>
			<div class='advanced-search-result-header'>
				<h1>Report Abuse</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label for="profile_id">Enter Profile Id</label>
							</div>
							<div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
								<input type="text" class="form-control profile_id" >
							</div>
							<div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
								<button class="btn btn-primary btn_fetch_profile_info website-button">Search</button>
							</div>
						</div>

						<div class="row report-abuse-comment-div" style="display:none">
							<hr class="dotted">
						</div>

						<div class="row report-abuse-comment-div" style="display:none">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label for="profile_id">Profile Id</label>
							</div>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type='text' class='fetched_profile_id' disabled>
							</div>
						</div>
						<br>
						<div class="row report-abuse-comment-div" style="display:none">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<label for="profile_id">Enter Reason / Comment</label>
							</div>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<textarea class="form-control report_abuse_comment" rows="3" placeholder="Please provide proper reason why do you want to report this profile id as abuse."></textarea>
							</div>
						</div>
						<br>
						<div class="row report-abuse-comment-div" style="display:none">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 report-abuse-status">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img_submit' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><button class='btn btn-primary btn-report-abuse website-button'>Report Abuse</button></center>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profile_id_data">
								<!-- Searched result  -->
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<hr class="dotted">
		<div class="row report-abuse-table-data">
			<h2>Your reported profiles</h2>
			<table id='datatable-info' class='table-hover table-striped table-bordered abused_list' style="width:100%">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Profile Id</th>
                        <th>Name</th>
                        <th>Comment</th>
                        <th>Reported On</th>
                    </tr>
                </thead>

                <?php
                	$sql_abused_data = "SELECT A.*,B.firstname FROM `report_abuse` AS A JOIN `clients` AS B ON A.abused_user_id=B.id WHERE A.abused_by='$user_id'";
                	$stmt = $link->prepare($sql_abused_data);
                	$stmt->execute();
                	$count = $stmt->rowCount();
                	$result = $stmt->fetchAll();
                	$i = 1;
                	foreach ($result as $row) 
                	{
                		$abused_firstname = $row['firstname'];
                		$abused_user_profile_id = $row['abused_user_profile_id'];
                		$abused_comment = $row['abused_comment'];
                		$abused_on = $row['abused_on'];

                		$abused_on = date('d-M-Y h:i A',strtotime($abused_on));

                		echo "<tr>";
                			echo "<td>".$i."</td>";
                			echo "<td>".$abused_user_profile_id."</td>";
                			echo "<td>".$abused_firstname."</td>";
                			echo "<td>".$abused_comment."</td>";
                			echo "<td>".$abused_on."</td>";
                		echo "</tr>";

                		$i = $i+1;
                	}
                ?>
            </table>
        </div>
	<!-- end: page -->	
</section>

</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>
</html>

<script>
	$(document).ready(function(){

		$('.abused_list').DataTable();

		/**********    Fetch profile id information    *************/
		$('.btn_fetch_profile_info').click(function(){
			var profile_id = $('.profile_id').val();
			var task = "fetch_profile_info";

			var data = 'profile_id='+profile_id+'&task='+task;
			

			if(profile_id=='' || profile_id==null)
			{
				$('.profile_id_data').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span> Please enter profile Id.</center></div>");	
				$('.report-abuse-comment-div').hide();
				return false;
			}

			$('.loading_img').show();

			if(profile_id!='' || profile_id!=null)
			{
				$.ajax({
					type:'post',
	            	data:data,
	            	url:'query/report_abuse_helper.php',
	            	success:function(res)
	            	{
	            		$('.loading_img').hide();

	            		if(res=='no_record_found')
	            		{
	            			$('.profile_id_data').html("Sorry, No record found for this profile id.");
		            		$('.report-abuse-comment-div').hide();
		            		return false;
	            		}
	            		else
	            		{
	            			$('.profile_id_data').html(res);
		            		$('.report-abuse-comment-div').show();
		            		$('.fetched_profile_id').attr('value',profile_id);
	            		}	
	            	}
	            });					
			}
		});

		/**********   Submit report abuse     ************/
		$('.btn-report-abuse').click(function(){
			var fetched_profile_id = $('.fetched_profile_id').val();
			var report_abuse_comment = $('.report_abuse_comment').val();
			var task = "Submit_report_abuse";

			if(report_abuse_comment=='' || report_abuse_comment==null)
			{
				$('.report-abuse-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span> Please enter valid reason / comments why do you want to report this profile id.</center></div>");
				return false;
			}

			$('.btn-report-abuse').attr('disabled',true);

			$('.loading_img_submit').show();
			var data = 'fetched_profile_id='+fetched_profile_id+'&report_abuse_comment='+report_abuse_comment+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/report_abuse_helper.php',
            	success:function(res)
            	{
            		$('.loading_img_submit').hide();

            		if(res=='success')
            		{
            			$('.btn-report-abuse').attr('disabled',true);
            			$('.report-abuse-status').html("<div class='alert alert-success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span> Report abused submitted successfully.</center></div>");
            			$('.report-abuse-status').fadeTo(2000, 500).slideUp(500, function(){
                            $('.report-abuse-status').slideUp(2000);
                            window.location.assign('report-abuse.php');
                        });
            		}
            		else
            		{
            			$('.btn-report-abuse').attr('disabled',false);
            			$('.report-abuse-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span> "+res+"</center></div>");
            		}	
            	}
            });	
		});
	});
</script>