<?php
if(!isset($_GET['action4']))
{

	$sql  = "SELECT * FROM `eduocc` WHERE userid='$user_id'";
	$stmt   = $link->prepare($sql);
	$stmt->execute();
	$userRow = $stmt->fetch();
	$education = $userRow['education'];
	$occupation = $userRow['occupation'];
	$designation_category = $userRow['designation_category'];
	$designation = $userRow['designation'];
	$industry = $userRow['industry'];
	$income_currency = $userRow['income_currency'];
	$income = $userRow['income'];
?>

<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-4 control-label" for="education">Education: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-8">
		<span class="education" id="education">
            <?php
                echo getUserEducationName($education);
            ?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-4 control-label" for="occupation">Employment/ Occupation: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-8">
		<span>
            <?php
                echo getUserEmploymentName($occupation);
            ?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-4 control-label" for="designation">Designation: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-8">
		<span class="designation" id="designation">
            <?php
                $rel_sql  = "SELECT * FROM `designation_category` WHERE id='$designation_category'";
                $stmt   = $link->prepare($rel_sql);
                $stmt->execute();
                $rel_userRow = $stmt->fetch();

                $rel_sql2  = "SELECT * FROM `designation` WHERE id='$designation'";
                $stmt2   = $link->prepare($rel_sql2);
                $stmt2->execute();
                $rel_userRow2 = $stmt2->fetch();

                if($rel_userRow['name']!='' && $rel_userRow2['name']!='')
                {
                	echo $rel_userRow['name'].'<br/>[ '.$rel_userRow2['name'].' ]';
                }
            ?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-4 control-label" for="industry">Industry: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-8">
		<span class="industry" id="industry">
            <?php
                echo getUserIndustryName($industry);
            ?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-4 control-label" for="income_currency">Currency: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-8">
		<span>
            <?php
                $rel_sql  = "SELECT * FROM `currency` WHERE id='$income_currency'";
                $stmt   = $link->prepare($rel_sql);
                $stmt->execute();
                $count=$stmt->rowCount();
                $rel_userRow = $stmt->fetch();
                if($count!=0)
                {
                	echo $rel_userRow['code']." [".$rel_userRow['currency']." - ".$rel_userRow['country']."] ";
                }
                
            ?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-4 control-label" for="income"> Monthly Income: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-8">
		<span>
            <?php
                echo $income = $userRow['income'];
            ?>
        </span>
	</div>
</div>

<?php
}
else
{
	$action = $_GET['action4'];	
	if($action=='edu')
	{
		$sql  = "SELECT * FROM `eduocc` WHERE userid='$user_id'";
		$stmt   = $link->prepare($sql);
		$stmt->execute();
		$userRow = $stmt->fetch();
		$education = $userRow['education'];
		$occupation = $userRow['occupation'];
		$designation_category = $userRow['designation_category'];
		$designation = $userRow['designation'];
		$industry = $userRow['industry'];
		$income_currency = $userRow['income_currency'];
		$income = $userRow['income'];
?>
<div class="form-group">
	<label class="col-md-3 control-label" for="religion">Education</label>
	<div class="col-md-8">
		<span>
			<select class="form-control education" id="education" required>
              	<option value="0">Select Education</option>
              	<?php
              		$query  = "SELECT * FROM `educationname` WHERE `status`='1' ORDER BY `id` ASC";
					$stmt   = $link->prepare($query);
					$stmt->execute();
					$result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$education_name =  $row['name'];
						$education_id = $row['id']; 

						if($education_id==$education)
						{
							echo "<option value=".$education_id." selected>".$education_name."</option>";
						}
						else
						{
							echo "<option value=".$education_id.">".$education_name."</option>";
						}
					}
              	?>
            </select>
        </span>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label" for="occupation">Employment/
	Occupation</label>
	<div class="col-md-8">
		<span>
			<select class="form-control occupation" id="occupation" required>
              	<option value="0">Select Employment</option>
              	<?php
              		$query  = "SELECT * FROM `employment` WHERE `status`='1' ORDER BY `id` ASC";
					$stmt   = $link->prepare($query);
					$stmt->execute();
					$result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$employment_name =  $row['name'];
						$employment_id = $row['id']; 

						if($employment_id==$occupation)
						{
							echo "<option value=".$employment_id." selected>".$employment_name."</option>";
						}
						else
						{
							echo "<option value=".$employment_id.">".$employment_name."</option>";
						}
					}
              	?>
            </select>
        </span>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label" for="designation_category">Category</label>
	<div class="col-md-8">
		<span>
			<select class="form-control designation_category" id="designation_category" required>
              	<option value="0">Select Category</option>
              	<?php
              		$query  = "SELECT * FROM `designation_category` WHERE `status`='1' ORDER BY `id` ASC";
					$stmt   = $link->prepare($query);
					$stmt->execute();
					$result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$designation_category_name =  $row['name'];
						$designation_category_id = $row['id']; 

						if($designation_category==$designation_category_id)
						{
							echo "<option value=".$designation_category_id." selected>".$designation_category_name."</option>";
						}
						else
						{
							echo "<option value=".$designation_category_id.">".$designation_category_name."</option>";
						}
					}
              	?>
            </select>
        </span>
    </div>
</div>
<div class="form-group">
	<label class="col-md-3 control-label" for="designation">Designation</label>
	<div class="col-md-8">
        <span>
			<select class="form-control designation" id="designation" required>
              	<option value="0">Select Designation</option>
              	<?php
              		$query  = "SELECT * FROM `designation` WHERE `designation_category`='$designation_category'  AND `status`='1'";
					$stmt   = $link->prepare($query);
					$stmt->execute();
					$result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$designation_name =  $row['name'];
						$designation_id = $row['id']; 

						if($designation==$designation_id)
						{
							echo "<option value=".$designation_id." selected>".$designation_name."</option>";
						}
						else
						{
							echo "<option value=".$designation_id.">".$designation_name."</option>";
						}
					}
              	?>
            </select>
        </span>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label" for="industry">Industry</label>
	<div class="col-md-8">
		<span>
			<select class="form-control industry" id="industry" required>
              	<option value="0">Select Industry</option>
              	<?php
              		$query  = "SELECT * FROM `industry` WHERE `status`='1' ORDER BY `id` ASC";
					$stmt   = $link->prepare($query);
					$stmt->execute();
					$result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$industry_name =  $row['name'];
						$industry_id = $row['id']; 

						if($industry_id==$industry)
						{
							echo "<option value=".$industry_id." selected>".$industry_name."</option>";
						}
						else
						{
							echo "<option value=".$industry_id.">".$industry_name."</option>";
						}
					}
              	?>
            </select>
        </span>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label" for="income_currency">Currency</label>
	<div class="col-md-8">
		<span>
			<select class="form-control income_currency" id="income_currency">
              	<option value="0">Select Currency</option>
              	<?php
              		$query  = "SELECT * FROM `currency` WHERE `status`='1' ORDER BY `id` ASC";
					$stmt   = $link->prepare($query);
					$stmt->execute();
					$result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$currency_name =  $row['currency']." [".$row['code']."]";
						$currency_id = $row['id']; 

						if($currency_id==$income_currency)
						{
							echo "<option value=".$currency_id." selected>".$currency_name."</option>";
						}
						else
						{
							echo "<option value=".$currency_id.">".$currency_name."</option>";
						}
					}
              	?>
            </select>

        </span>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label" for="income">Monthly Income</label>
	<div class="col-md-8">
		<span>
          	<input type="text" class="form-control income" value="<?php echo $income;?>" maxlength="6">
        </span>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="form-group education_status">
			<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
		</div>	
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="form-group">
			<center><button class="btn btn-success btn_education_submit website-button">Update Changes >></button></center>
		</div>
	</div>
</div>
<?php
	}
}
?>