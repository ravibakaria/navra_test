<?php
if(!isset($_GET['action2']))
{

	$sql  = "SELECT * FROM `profilereligion` WHERE userid='$user_id'";
	$stmt   = $link->prepare($sql);
	$stmt->execute();
	$userRow = $stmt->fetch();
	$religion = $userRow['religion'];
	$caste = $userRow['caste'];
	$mother_tongue = $userRow['mother_tongue'];
?>


<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-6 control-label" for="religion">Religion : </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-6">
		<span class="religion" id="religion">
            <?php
                echo getUserReligionName($religion);
           	?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-6 control-label" for="caste"> Caste : </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-6">
		<span class="caste" id="caste">
            <?php
                echo getUserCastName($caste);
            ?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-6 control-label" for="mother_tongue"> Mother Tongue : </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-6">
		<span class="mother_tongue" id="mother_tongue">
            <?php
                echo getUserMotherTongueName($mother_tongue);
            ?>
        </span>
	</div>
</div>
<?php
}

else
{
	$action = $_GET['action2'];	
	if($action=='religious')
	{
		$sql  = "SELECT * FROM `profilereligion` WHERE userid='$user_id'";
		$stmt   = $link->prepare($sql);
		$stmt->execute();
		$userRow = $stmt->fetch();
		$religion = $userRow['religion'];
		$caste = $userRow['caste'];
		$mother_tongue = $userRow['mother_tongue'];
	?>

<div class="form-group">
	<label class="col-md-3 control-label" for="religion">Religion</label>
	<div class="col-md-8">
		<span>
			<select class="form-control religion" id="religion" required>
              	<option value="0">Select Religion</option>
              	<?php
              		$query  = "SELECT * FROM `religion` WHERE status='1' ORDER BY `id` ASC";
					$stmt   = $link->prepare($query);
					$stmt->execute();
					$result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$religion_name =  $row['name'];
						$religion_id = $row['id']; 

						if($religion_id==$religion)
						{
							echo "<option value='".$religion_id."' selected>".$religion_name."</option>";
						}
						else
						{
							echo "<option value=".$religion_id.">".$religion_name."</option>";
						}
					}
              	?>
            </select>
        </span>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label" for="caste">Caste</label>
	<div class="col-md-8">
		<span>
			<select class="form-control caste" id="caste" required>
              	<option value="0">Select Caste</option>
              	<?php
              		$query  = "SELECT * FROM `caste` WHERE status='1' ORDER BY `id` ASC";
					$stmt   = $link->prepare($query);
					$stmt->execute();
					$result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$caste_name =  $row['name'];
						$caste_id = $row['id']; 

						if($caste_id==$caste)
						{
							echo "<option value='".$caste_id."' selected>".$caste_name."</option>";
						}
						else
						{
							echo "<option value=".$caste_id.">".$caste_name."</option>";
						}
					}
              	?>
            </select>
        </span>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label" for="mother_tongue">Mother Tongue</label>
	<div class="col-md-8">
		<span>
			<select class="form-control mother_tongue" id="mother_tongue" required>
              	<option value="0">Select Mother Tongue</option>
              	<?php
              		$query  = "SELECT * FROM `mothertongue` WHERE status='1' ORDER BY `id` ASC";
					$stmt   = $link->prepare($query);
					$stmt->execute();
					$result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$mothertongue_name =  $row['name'];
						$mothertongue_id = $row['id'];

						if($mothertongue_id==$mother_tongue)
						{
							echo "<option value='".$mothertongue_id."' selected>".$mothertongue_name."</option>";
						}
						else
						{
							echo "<option value=".$mothertongue_id.">".$mothertongue_name."</option>";
						}
					}
              	?>
            </select>
        </span>
	</div>
</div>

<hr>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="form-group religious_status">
			<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
		</div>	
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="form-group">
			<center><button class="btn btn-success btn_religious_submit website-button">Update Changes >></button></center>
		</div>
	</div>
</div>
<?php
	}
}
?>