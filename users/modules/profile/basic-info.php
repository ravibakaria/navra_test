<?php
if(!isset($_GET['action']))
{
$sql  = "SELECT * FROM `profilebasic` WHERE userid='$user_id'";
$stmt   = $link->prepare($sql);
$stmt->execute();
$userRow = $stmt->fetch();
$height = $userRow['height'];
$weight = $userRow['weight'];
$eat_habbit = $userRow['eat_habbit'];
$smoke_habbit = $userRow['smoke_habbit'];
$body_type = $userRow['body_type'];
$complexion = $userRow['complexion'];
$special_case = $userRow['special_case'];
$marital_status = $userRow['marital_status'];
$drink_habbit = $userRow['drink_habbit'];
?>

<!--  First Name Start -->
<div class="row">
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-xs-6 col-md-4 control-label" for="profileFirstName">First Name </label>
			<div class="col-sm-6 col-xs-6 col-xs-6 col-md-8">
				<span class="firstname" id="firstname">
	                <?php echo $firstname; ?>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="lastname">Last Name </label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="lastname" id="lastname">
	                <?php echo $lastname; ?>
	            </span>
			</div>
		</div>
	</div>
</div>
<!--  First Name End -->
<hr>

<!--  Profile ID & Date Of Birth Start-->
<div class="row">
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="unique_code">Profile ID </label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="unique_code" id="unique_code">
	                <?php echo $unique_code; ?>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="dob">Date Of Birth </label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="dob" id="dob">
	                <?php echo date('d-M-Y',strtotime($dob)); ?>
	            </span>
			</div>
		</div>
	</div>
</div>
<!--  Profile ID & Date Of Birth End-->
<hr>

<!--  Email Address & Phone Number Start-->
<div class="row">
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="email">Email Id</label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="email" id="email">
	                <?php echo $email; ?>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="phonenumber">Mobile</label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="phonenumber" id="phonenumber">
	               <?php echo '+'.getUserCountryPhoneCode($user_id); ?><b>-</b> <?php echo $phonenumber; ?>
	            </span>
			</div>
		</div>
	</div>
</div>
<!--  Email Address & Phone Number End-->
<hr>

<!--  Height & Body Type Start-->
<div class="row">
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="height">Height (cms/fts/mts)</label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="height" id="height">
	                <?php 
	                	$height_meter = round(($height)/100,2);
	                	$height_foot = round(($height)/30.48,2);
	                	echo $height.'-cms / '.$height_foot.'-fts / '.$height_meter.'-mts';
	                ?>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="body_type">Body Type</label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="body_type" id="body_type">
	                <?php 
	                	echo getUserBodyTypeName($body_type);
	                ?>
	            </span>
			</div>
		</div>
	</div>
</div>
<!--  Height & Body Type End-->
<hr>

<!--  Weight & Complexion Start-->
<div class="row">
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="weight">Weight (kg)</label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="weight" id="weight">
	                <?php echo $weight; ?>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="complexion"> Complexion</label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="complexion" id="complexion">
	                <?php 
	                	echo getUserComplexionName($complexion);
		            ?>
	            </span>
			</div>
		</div>
	</div>
</div>
<!--  Weight & Complexion End-->
<hr>

<!--  Eating Habbit & Martial Status Start-->
<div class="row">
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="eat_habbit">Eating Habbit</label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="eat_habbit" id="eat_habbit">
					<?php 
	                	echo getUserEatHabbitName($eat_habbit);
		            ?>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="marital_status">Martial Status</label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="marital_status" id="marital_status">
					<?php 
	                	echo getUserMaritalStatusName($marital_status);
		            ?>
	            </span>
			</div>
		</div>
	</div>
</div>
<!--  Eating Habbit & Martial Status End-->
<hr>

<!--  Smoking Habbit & Special Case Start-->
<div class="row">
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="smoke_habbit"> Smoking Habbit</label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="smoke_habbit" id="smoke_habbit">
	                <?php 
	                	echo getUserSmockingHabbitName($smoke_habbit);
	                ?>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-4 control-label" for="special_case">Special Case</label>
			<div class="col-sm-6 col-xs-6 col-md-8">
				<span class="special_case" id="special_case">
	                <?php 
	                	echo getUserSpecialCaseName($special_case);
	                ?>
	            </span>
			</div>
		</div>
	</div>
</div>
<!--  Smoking Habbit & Special Case End-->
<hr>

<!--  Drinking Habbit  Start-->
<div class="row">
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-4 control-label" for="drink_habbit"> Drinking Habbit</label>
			<div class="col-md-8">
				<span class="drink_habbit" id="drink_habbit">
	                <?php 
	                	echo getUserDrinkHabbitName($drink_habbit);
	                ?>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-6">
		<!--div class="form-group">
			<label class="col-md-4 control-label" for="special_case">Special Case</label>
			<div class="col-md-8">
				<span class="special_case" id="special_case">
	                <?php echo $special_case; ?>
	            </span>
			</div>
		</div-->
	</div>
</div>
<!--  Drinking Habbit  End-->
<?php
}
else
{
	$action = $_GET['action'];	
	if($action=='basic')
	{
		$sql  = "SELECT * FROM `profilebasic` WHERE userid='$user_id'";
		$stmt   = $link->prepare($sql);
		$stmt->execute();
		$userRow = $stmt->fetch();
		$height = $userRow['height'];
		$weight = $userRow['weight'];
		$eat_habbit = $userRow['eat_habbit'];
		$smoke_habbit = $userRow['smoke_habbit'];
		$body_type = $userRow['body_type'];
		$complexion = $userRow['complexion'];
		$special_case = $userRow['special_case'];
		$marital_status = $userRow['marital_status'];
		$drink_habbit = $userRow['drink_habbit'];
	?>
<div class="row">
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="profileFirstName">First Name</label>
			<div class="col-md-8">
				<span>
	                <input type="text" class="form-control firstname" id="firstname" value="<?php echo $firstname; ?>" placeholder="First Name">
	            </span>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="lastname">Last Name</label>
			<div class="col-md-8">
				<span>
	                <input type="text" class="form-control lastname" id="lastname" value="<?php echo $lastname; ?>" placeholder="Last Name">
	            </span>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="unique_code">Profile ID</label>
			<div class="col-md-8">
				<span>
	                <input type="text" class="form-control unique_code" id="unique_code" value="<?php echo $unique_code; ?>" disabled>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="dob">Date Of Birth</label>
			<div class="col-md-8">
				<span>
	                <input type="text" name="dob" class="form-control dob" id="dob" value="<?php echo $dob; ?>"  placeholder="YYYY-MM-DD">
	            </span>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="email">Email Address</label>
			<div class="col-md-8">
				<span>
	                <input type="text" class="form-control email" id="email" value="<?php echo $email; ?>" disabled>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="phonenumber">Phone Number</label>
			<div class="col-md-8 input-group phone-with-country-code">
				<span class="input-group-addon country_code">
					<?php
						echo '+'.getUserCountryPhoneCode($user_id);
					?>
				</span>
				<input type="text" class="form-control phonenumber" id="phonenumber" value="<?php echo $phonenumber; ?>">
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="height">Height (cms/fts/mts)</label>
			<div class="col-md-8">
				<span>
					<select class="form-control height">
						<option value="0">Select Height</option>
					<?php
						for($i=54;$i<273;$i++)
						{
							$i_meter = round(($i)/100,2);
	                		$i_foot = round(($i)/30.48,2);

							if($i==$height)
							{
								echo "<option value=".$i." selected>".$i."-cms / ".$i_foot."-fts / ".$i_meter."-mts</option>";
							}
							else
							{
								echo "<option value=".$i.">".$i."-cms / ".$i_foot."-fts / ".$i_meter."-mts</option>";
							}
						}
					?>
					</select>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="body_type">Body Type</label>
			<div class="col-md-8">
				<span>
					<select class="form-control body_type">
						<option value="0">Select Body Type</option>
						<?php
							$stmt = $link->prepare("SELECT * FROM `bodytype` WHERE `status`='1'"); 
					        $stmt->execute();
					        $count=$stmt->rowCount();
					        $result = $stmt->fetchAll();
					        
					        foreach ($result as $row) 
					        {
					        	$id = $row['id'];
					        	$name = $row['name'];

					        	if($id == $body_type)
					        	{
					        		echo "<option value='".$id."' selected>".$name."</option>";
					        	}
					        	else
					        	{
					        		echo "<option value='".$id."'>".$name."</option>";
					        	}
					        }
						?>
					</select>
	            </span>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="weight">Weight (kg)</label>
			<div class="col-md-8">
				<span>
					<select class="form-control weight">
						<option value="0">Select Weight</option>
					<?php
						for($i=35;$i<161;$i++)
						{
							if($i==$weight)
							{
								echo "<option value=".$i." selected>".$i."</option>";
							}
							else
							{
								echo "<option value=".$i.">".$i."</option>";
							}
						}
					?>
					</select>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="complexion">Complexion</label>
			<div class="col-md-8">
				<span>
					<select class="form-control complexion">
						<option value="0">Select Complexion</option>
						<?php
							$stmt = $link->prepare("SELECT * FROM `complexion` WHERE `status`='1'"); 
					        $stmt->execute();
					        $count=$stmt->rowCount();
					        $result = $stmt->fetchAll();
					        
					        foreach ($result as $row) 
					        {
					        	$id = $row['id'];
					        	$name = $row['name'];

					        	if($id == $complexion)
					        	{
					        		echo "<option value='".$id."' selected>".$name."</option>";
					        	}
					        	else
					        	{
					        		echo "<option value='".$id."'>".$name."</option>";
					        	}
					        }
						?>
					</select>
	            </span>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="eat_habbit">Eating Habbit</label>
			<div class="col-md-8">
				<span>
					<select class="form-control eat_habbit">
						<option value="0">Select Eating Habbit</option>
						<?php
							$stmt = $link->prepare("SELECT * FROM `eathabbit` WHERE `status`='1'"); 
					        $stmt->execute();
					        $count=$stmt->rowCount();
					        $result = $stmt->fetchAll();
					        
					        foreach ($result as $row) 
					        {
					        	$id = $row['id'];
					        	$name = $row['name'];

					        	if($id == $eat_habbit)
					        	{
					        		echo "<option value='".$id."' selected>".$name."</option>";
					        	}
					        	else
					        	{
					        		echo "<option value='".$id."'>".$name."</option>";
					        	}
					        }
						?>
					</select>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="marital_status">Martial Status</label>
			<div class="col-md-8">
				<span>
					<select class="form-control marital_status">
						<option value="0">Select Marital Status</option>
						<?php
							$stmt = $link->prepare("SELECT * FROM `maritalstatus` WHERE `status`='1'"); 
					        $stmt->execute();
					        $count=$stmt->rowCount();
					        $result = $stmt->fetchAll();
					        
					        foreach ($result as $row) 
					        {
					        	$id = $row['id'];
					        	$name = $row['name'];

					        	if($id == $marital_status)
					        	{
					        		echo "<option value='".$id."' selected>".$name."</option>";
					        	}
					        	else
					        	{
					        		echo "<option value='".$id."'>".$name."</option>";
					        	}
					        }
						?>
					</select>
	            </span>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="smoke_habbit">Smoking Habbit</label>
			<div class="col-md-8">
				<span>
					<select class="form-control smoke_habbit">
						<option value="0">Select Smoking Habbit</option>
						<?php
							$stmt = $link->prepare("SELECT * FROM `smokehabbit` WHERE `status`='1'"); 
					        $stmt->execute();
					        $count=$stmt->rowCount();
					        $result = $stmt->fetchAll();
					        
					        foreach ($result as $row) 
					        {
					        	$id = $row['id'];
					        	$name = $row['name'];

					        	if($id == $smoke_habbit)
					        	{
					        		echo "<option value='".$id."' selected>".$name."</option>";
					        	}
					        	else
					        	{
					        		echo "<option value='".$id."'>".$name."</option>";
					        	}
					        }
						?>
					</select>
	            </span>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="special_case">Special Case</label>
			<div class="col-md-8">
				<span>
					<select class="form-control special_case">
						<option value="0">Select Special Case</option>
						<?php
							$stmt = $link->prepare("SELECT * FROM `specialcases` WHERE `status`='1'"); 
					        $stmt->execute();
					        $count=$stmt->rowCount();
					        $result = $stmt->fetchAll();
					        
					        foreach ($result as $row) 
					        {
					        	$id = $row['id'];
					        	$name = $row['name'];

					        	if($id == $special_case)
					        	{
					        		echo "<option value='".$id."' selected>".$name."</option>";
					        	}
					        	else
					        	{
					        		echo "<option value='".$id."'>".$name."</option>";
					        	}
					        }
						?>
					</select>
	            </span>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6 col-lg-6">
		<div class="form-group">
			<label class="col-md-3 control-label" for="drink_habbit">Drinking Habbit</label>
			<div class="col-md-8">
				<span>
					<select class="form-control drink_habbit">
						<option value="0">Select Drink Habbit</option>
						<?php
							$stmt = $link->prepare("SELECT * FROM `drinkhabbit` WHERE `status`='1'"); 
					        $stmt->execute();
					        $count=$stmt->rowCount();
					        $result = $stmt->fetchAll();
					        
					        foreach ($result as $row) 
					        {
					        	$id = $row['id'];
					        	$name = $row['name'];

					        	if($id == $drink_habbit)
					        	{
					        		echo "<option value='".$id."' selected>".$name."</option>";
					        	}
					        	else
					        	{
					        		echo "<option value='".$id."'>".$name."</option>";
					        	}
					        }
						?>
					</select>
	            </span>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="form-group basic_status">
			<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
		</div>	
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="form-group">
			<center><button class="btn btn-success btn_basic_submit website-button">Update Changes >></button></center>
		</div>
	</div>
</div>

<?php
	}
}
?>