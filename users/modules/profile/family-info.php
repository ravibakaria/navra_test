<?php
if(!isset($_GET['action5']))
{

	$sql  = "SELECT * FROM `family` WHERE userid='$user_id'";
    $stmt   = $link->prepare($sql);
    $stmt->execute();
    $userRow = $stmt->fetch();
    $fam_val = $userRow['fam_val'];
    $fam_type = $userRow['fam_type'];
    $fam_stat = $userRow['fam_stat'];
?>

<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-6 control-label" for="fam_val">Family Value: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-6">
		<span class="fam_val" id="fam_val">
            <?php
                echo getUserFamilyValueName($fam_val);
            ?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-6 control-label" for="fam_type">Family Type: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-6">
		<span class="fam_type" id="fam_type">
            <?php
                echo getUserFamilyTypeName($fam_type);
            ?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-6 control-label" for="fam_stat">Family Status: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-6">
		<span class="fam_stat" id="fam_stat">
            <?php
                echo getUserFamilyStateName($fam_stat);
            ?>
        </span>
	</div>
</div>

<?php
}
else
{
    $action = $_GET['action5']; 
    if($action=='family')
    {
        $sql  = "SELECT * FROM `family` WHERE userid='$user_id'";
        $stmt   = $link->prepare($sql);
        $stmt->execute();
        $userRow = $stmt->fetch();
        $fam_val = $userRow['fam_val'];
        $fam_type = $userRow['fam_type'];
        $fam_stat = $userRow['fam_stat'];
?>
<div class="form-group">
    <label class="col-md-3 control-label" for="fam_val">Family Value</label>
    <div class="col-md-8">
        <span>
            <select class="form-control fam_val" id="fam_val" required>
                <option value="0">Select Family Value</option>
                <?php
                    $stmt = $link->prepare("SELECT * FROM `familyvalue` WHERE status='1'"); 
                    $stmt->execute();
                    $count=$stmt->rowCount();
                    $result = $stmt->fetchAll();

                    foreach ($result as $row) 
                    {
                        $id = $row['id'];
                        $name = $row['name'];

                        if($id == $fam_val)
                        {
                            echo "<option value='".$id."' selected>".$name."</option>";
                        }
                        else
                        {
                            echo "<option value='".$id."'>".$name."</option>";
                        }
                    }
                ?>
            </select>
        </span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="fam_type">Family Type</label>
    <div class="col-md-8">
        <span>
            <select class="form-control fam_type" id="fam_type" required>
                <option value="0">Select Family Type</option>
                <?php
                    $stmt = $link->prepare("SELECT * FROM `familytype` WHERE status='1'"); 
                    $stmt->execute();
                    $count=$stmt->rowCount();
                    $result = $stmt->fetchAll();

                    foreach ($result as $row) 
                    {
                        $id = $row['id'];
                        $name = $row['name'];

                        if($id == $fam_type)
                        {
                            echo "<option value='".$id."' selected>".$name."</option>";
                        }
                        else
                        {
                            echo "<option value='".$id."'>".$name."</option>";
                        }
                    }
                ?>
            </select>
        </span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="fam_stat">Family Status</label>
    <div class="col-md-8">
        <span>
            <select class="form-control fam_stat" id="fam_stat">
                <option value="0">Select Family Status</option>
                <?php
                    $stmt = $link->prepare("SELECT * FROM `familystatus` WHERE status='1'"); 
                    $stmt->execute();
                    $count=$stmt->rowCount();
                    $result = $stmt->fetchAll();

                    foreach ($result as $row) 
                    {
                        $id = $row['id'];
                        $name = $row['name'];

                        if($id == $fam_stat)
                        {
                            echo "<option value='".$id."' selected>".$name."</option>";
                        }
                        else
                        {
                            echo "<option value='".$id."'>".$name."</option>";
                        }
                    }
                ?>
            </select>
        </span>
    </div>
</div> 
<hr>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group family_status">
            <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
        </div>  
    </div>
</div>
<hr>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
            <center><button class="btn btn-success btn_family_submit website-button">Update Changes >></button></center>
        </div>
    </div>
</div>
<?php
    }
}

?>