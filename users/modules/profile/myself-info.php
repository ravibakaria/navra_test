<?php
if(!isset($_GET['action6']))
{
	$sql  = "SELECT * FROM `profiledesc` WHERE userid='$user_id'";
	$stmt   = $link->prepare($sql);
	$stmt->execute();
	$userRow = $stmt->fetch();

	$short_desc = $userRow['short_desc'];
	$short_desc_family = $userRow['short_desc_family'];
	$short_desc_interest = $userRow['short_desc_interest'];
?>
<div class="row">
	<div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
		<p class="myself-note">For your security reasons do not share your name, contact details or social media links below.</p>
		<div class="form-group">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
				<div >
					<p>Short Description About Yourself : </p>
				</div>
				<div>
					<p><?php echo $short_desc; ?></p>
				</div>
				
				<div>
					<hr/>
				</div>

				<div>
					<p> Short Description About Your Family : </p>
				</div>
				<div>
					<p><?php echo $short_desc_family; ?></p>
				</div>

				<div>
					<hr/>
				</div>

				<div>
					<p> I am looking for someone who is… : </p>
				</div>
				<div> 
					<p><?php echo $short_desc_interest; ?></p>
				</div>
			</div>
		</div>
		<!--div class="form-group">
			<label class="col-sm-6 col-xs-6 col-md-6 col-md-6 control-label" for="profileFirstName">Short Description About Yourself</label>
			<div class="col-sm-6 col-xs-6 col-md-6 col-md-6">
				<span class="firstname" id="firstname">
	                <p style="text-align: justify;padding: 1.5%;"><?php echo $short_desc; ?></p>
	            </span>
			</div>
		</div-->
	</div>
</div>

<?php
}

else
{
	$action = $_GET['action6']; 
    if($action=='myself')
    {
		$sql  = "SELECT * FROM `profiledesc` WHERE userid='$user_id'";
		$stmt   = $link->prepare($sql);
		$stmt->execute();
		$userRow = $stmt->fetch();
		$short_desc = $userRow['short_desc'];
		$short_desc_family = $userRow['short_desc_family'];
		$short_desc_interest = $userRow['short_desc_interest'];
?>
<div class="col-md-12 col-lg-12">
	<p class="gray-bg">For your security reasons do not share your name, contact details or social media links below.</p>
	<div class="form-group">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
			Short Description About Yourself 
			<textarea name="short_desc" id="short_desc" rows="3" maxlength="300" class="form-control short_desc" ><?php echo $userRow['short_desc']; ?></textarea> 
			<p align="right">Remaining characters: <span class="remaining_short_desc">300</span></p> 

			Short Description About Your Family 
			<textarea name="short_desc_family" id="short_desc_family" rows="3" maxlength="300" class="form-control short_desc_family" ><?php echo $userRow['short_desc_family']; ?></textarea> 
			<p align="right">Remaining characters: <span class="remaining_short_desc_family">300</span></p> 

			I am looking for someone who is…  
			<textarea name="short_desc_interest" id="short_desc_interest" rows="3" maxlength="300" class="form-control short_desc_interest" ><?php echo $userRow['short_desc_interest']; ?></textarea> 
			<p align="right">Remaining characters: <span class="remaining_short_desc_interest">300</span></p> 
		</div>
	</div>
</div>
<hr>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group myself_status">
            <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
        </div>  
    </div>
</div>
<hr>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
            <center><button class="btn btn-success btn_myself_submit website-button">Update Changes >></button></center>
        </div>
    </div>
</div>
<?php
	}
}
?>