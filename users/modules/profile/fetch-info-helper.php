<?php
	ob_start();
	session_start();
	if(!isset($_SESSION['logged_in']))
	{
		header('Location: ../login.php');
		exit;
	}

	require_once "../../../config/config.php";
	require_once "../../../config/dbconnect.php";
	include('../../../config/functions.php');       //strip query string

	$task = quote_smart($_POST['task']);

	/*     Fetch states W.R.T. $country_id     */
	if($task == "Fetch_state_data")
	{
		$response = null;
    	$country_id = quote_smart($_POST['country_id']);

    	$query  = "SELECT * FROM `states` WHERE `country_id`='$country_id' ORDER BY `name` ASC";
		$stmt   = $link->prepare($query);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$response = "<option value='0' selected>Select State</option>";
		foreach( $result as $row )
		{
			$state_name =  $row['name'];
			$state_id = $row['id']; 

			$response .= "<option value='".$state_id."'>".$state_name."</option>";
		}

		echo $response;
		exit;
    }

    /*     Fetch cities W.R.T. $state_id     */
    if($task == "Fetch_city_data")
	{
		$response = null;
    	$state_id = quote_smart($_POST['state_id']);

    	$query  = "SELECT * FROM `cities` WHERE `state_id`='$state_id' ORDER BY `name` ASC";
		$stmt   = $link->prepare($query);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$response = "<option value='0' selected>Select City</option>";
		foreach( $result as $row )
		{
			$city_name =  $row['name'];
			$city_id = $row['id']; 

			$response .= "<option value='".$city_id."'>".$city_name."</option>";
		}

		echo $response;
		exit;
    }

    /*     Fetch designations W.R.T. $designation_category     */
    if($task == "Fetch_designation_data")
	{
		$response = null;
    	$designation_category = quote_smart($_POST['designation_category']);

    	$query  = "SELECT * FROM `designation` WHERE `designation_category`='$designation_category' ORDER BY `name` ASC";
		$stmt   = $link->prepare($query);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$response = "<option value='0'>Select Designation</option>";
		foreach( $result as $row )
		{
			$designation_name =  $row['name'];
			$designation_id = $row['id']; 

			$response .= "<option value='".$designation_id."'>".$designation_name."</option>";
		}

		echo $response;
		exit;
    }

    /*     Fetch age range upto W.R.T. start_age      */
    if($task == "Fetch_age_data")
	{
		$response = null;
    	$from_date = quote_smart($_POST['from_date']);

    	for($i=$from_date;$i<61;$i++)
    	{
    		$response .= "<option value='".$i."'>".$i."</option>";
    	}

    	$response .= "<option value='61'>60+</option>";

		echo $response;
		exit;
    }

    /*     Fetch height range upto W.R.T. start_height      */
    if($task == "Fetch_height_data")
	{
		$response = null;
    	$height_from = quote_smart($_POST['height_from']);

    	for($i=$height_from;$i<273;$i++)
    	{
    		$i_meter = round(($i)/100,2);
    		$i_foot = round(($i)/30.48,2);
    		$response .= "<option value=".$i.">".$i."-cms / ".$i_foot."-fts / ".$i_meter."-mts</option>";
    	}

		echo $response;
		exit;
    }
?>	