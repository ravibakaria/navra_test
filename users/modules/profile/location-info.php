<?php
if(!isset($_GET['action3']))
{

	$sql  = "SELECT * FROM `address` WHERE userid='$user_id'";
	$stmt   = $link->prepare($sql);
	$stmt->execute();
	$userRow = $stmt->fetch();
	$city = $userRow['city'];
	$state = $userRow['state'];
	$country = $userRow['country'];
?>


<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-6 control-label" for="country">Country: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-6">
		<span class="country" id="country">
            <?php
                echo getUserCountryName($country);
           	?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-6 control-label" for="state">State: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-6">
		<span class="state" id="state">
            <?php
                echo getUserStateName($state);
            ?>
        </span>
	</div>
</div>
<hr/>
<div class="form-group">
	<label class="col-sm-6 col-xs-6 col-xs-6 col-md-6 control-label" for="city">City: </label>
	<div class="col-sm-6 col-xs-6 col-xs-6 col-md-6">
		<span class="city" id="city">
            <?php
                echo getUserCityName($city);
            ?>
        </span>
	</div>
</div>

<?php
}
else
{
	$action = $_GET['action3'];	
	if($action=='location')
	{
		$sql  = "SELECT * FROM `address` WHERE userid='$user_id'";
		$stmt   = $link->prepare($sql);
		$stmt->execute();
		$userRow = $stmt->fetch();
		$city = $userRow['city'];
		$state = $userRow['state'];
		$country = $userRow['country'];
	?>
<div class="form-group">
	<label class="col-md-3 control-label" for="religion">Country</label>
	<div class="col-md-8">
		<span>
			<select class="form-control country" id="country" required>
              	<option value="0">Select Country</option>
              	<?php
              		$query  = "SELECT * FROM `countries` ORDER BY `name` ASC";
					$stmt   = $link->prepare($query);
					$stmt->execute();
					$result = $stmt->fetchAll();
					foreach( $result as $row )
					{
						$country_name =  $row['name'];
						$country_id = $row['id']; 

						if($country_id==$country)
						{
							echo "<option value=".$country_id." selected>".$country_name."</option>";
						}
						else
						{
							echo "<option value=".$country_id.">".$country_name."</option>";
						}
					}
              	?>
            </select>
        </span>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label" for="state">State</label>
	<div class="col-md-8">
		<span>
			<select class="form-control state" id="state" required>
				<?php
					if($country!='0')
					{
						$caste_sql  = "SELECT * FROM `states` WHERE `country_id`='$country' ORDER BY `name` ASC";
		                $stmt   = $link->prepare($caste_sql);
		                $stmt->execute();
		                $states_userRow = $stmt->fetchAll();
		                
		                foreach( $states_userRow as $row )
						{
							$state_name =  $row['name'];
							$state_id = $row['id']; 

							if($state_id==$state)
							{
								echo "<option value=".$state_id." selected>".$state_name."</option>";
							}
							else
							{
								echo "<option value=".$state_id.">".$state_name."</option>";
							}
						}
					}
	                
	            ?>
            </select>
        </span>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label" for="city">City</label>
	<div class="col-md-8">
		<span>
			<select class="form-control city" id="city" required>
				<?php
					if($state!='0')
					{
						$caste_sql  = "SELECT * FROM `cities` WHERE `state_id`='$state' ORDER BY `name` ASC";
		                $stmt   = $link->prepare($caste_sql);
		                $stmt->execute();
		                $city_userRow = $stmt->fetchAll();
		                
		                foreach( $city_userRow as $row )
						{
							$city_name =  $row['name'];
							$city_id = $row['id']; 

							if($city_id==$city)
							{
								echo "<option value=".$city_id." selected>".$city_name."</option>";
							}
							else
							{
								echo "<option value=".$city_id.">".$city_name."</option>";
							}
						}
					}
	                
	            ?>
            </select>
        </span>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="form-group location_status">
			<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
		</div>	
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="form-group">
			<center><button class="btn btn-success btn_location_submit website-button">Update Changes >></button></center>
		</div>
	</div>
</div>
<?php
	}
}
?>