<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}
	
	include("templates/header.php");

?>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
	
<?php
	$res_count  = "SELECT * FROM `preferences` WHERE `userid`='$user_id'";
	$stmt   = $link->prepare($res_count);
	$stmt->execute();
	$row_data = $stmt->fetch();
	
	$gender = $row_data['gender'];
	$marital_status = $row_data['marital_status'];
	$country = $row_data['country'];
	$state = $row_data['state'];
	$city = $row_data['city'];
	$religion = $row_data['religion'];
	$mother_tongue = $row_data['mother_tongue'];
	$caste = $row_data['caste'];
	$education = $row_data['education'];
	$occupation = $row_data['occupation'];
	$currency = $row_data['currency'];
	$monthly_income_from = $row_data['monthly_income_from'];
	$monthly_income_to = $row_data['monthly_income_to'];
	$from_date = $row_data['from_date'];
	$to_date = $row_data['to_date'];
	$body_type = $row_data['body_type'];
	$complexion = $row_data['complexion'];
	$height_from = $row_data['height_from'];
	$height_to = $row_data['height_to'];
	$fam_type = $row_data['fam_type'];
	$smoke_habbit = $row_data['smoke_habbit'];
	$drink_habbit = $row_data['drink_habbit'];
	$eat_habbit = $row_data['eat_habbit'];
	$manglik = $row_data['manglik'];
?>

	<section role="main" class="content-body main-section-start">			

			<div class='row'>
				<h1>My Partner Preferences</h1>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
					<div class="row">
						<div class="panel panel-info" id="my_preferences_selection">
							<div class="panel-body">
								<h2>Select your partner preference form below characterisitics.</h2><br/>
								<div class="row">
									<div class="form-group">
										<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
											<label for="gender" class="control-label">
												Looking For: 
											</label>
										</div>
										
										<div class="col-xs-8 col-sm-8 col-md-2 col-lg-2">	
											<select class="form-control gender">
												<option value="0">--Seect Gender--</option>
												<?php
													$query  = "SELECT * FROM `gender` WHERE `status`='1' ORDER BY `id` ASC";
													$stmt   = $link->prepare($query);
													$stmt->execute();
													$result = $stmt->fetchAll();
													foreach( $result as $row )
													{
														$name =  $row['name'];
														$id = $row['id']; 

														if(strtolower($name)=='male')
														{
															$name = "Groom";
														}
														else
														if(strtolower($name)=='female')
														{
															$name = "Bride";
														}
														else
														{
															$name = "T-Gender";
														}

														if($id==$gender)
														{
															echo "<option value=".$id." selected>".$name."</option>";
														}
														else
														{
															echo "<option value=".$id.">".$name."</option>";
														}
													}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="clearfix">&nbsp;</div>
								<div class="row">
									<div class="form-group">
										<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
											<label for="marital_status" class="control-label">
												Interested In:
											</label>
										</div>
										<div class="col-xs-8 col-sm-8 col-md-10 col-lg-10">
											<?php
												if($marital_status!='' || $marital_status!=null || $marital_status!=0) 
												{
													$record = explode(",",$marital_status);
												}
												else
												{
													$record =array('0');
												}
								            ?>

								            <?php
												$query  = "SELECT * FROM `maritalstatus` WHERE `status`='1' ORDER BY `id` ASC";
												$stmt   = $link->prepare($query);
												$stmt->execute();
												$result = $stmt->fetchAll();
												foreach( $result as $row )
												{
													$name =  $row['name'];
													$id = $row['id'];

													echo "<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>";
											?>
								            	<input type="checkbox" name="marital_status[]" <?php if(in_array($id,$record)){ echo "checked"; } ?> class="radio_1" value="<?php echo $id;?>" id="marital_status_<?php echo $id;?>" />
								          <label for="marital_status_<?php echo $id;?>" class='control-label marital_status_preference'><?php echo $name;?>
								            	</label>
								            </div>
							                <?php

												} 
											?>
										</div>
									</div>
								</div>
								<hr class="dotted short">
								<div class="row vertical-divider">
								  	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								  		<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="age" class="control-label">
														Age:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 interest_div">
													<div class="row">
														<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 interest_div">
															<select class="form-control form control-sm from_date">
																<option value="0">--From--</option>
																<?php
																	for($i=18;$i<61;$i++)
																	{
																		if($from_date == $i)
																		{
																			echo "<option value='".$i."' selected>".$i."</option>";
																		}
																		else
																		{
																			echo "<option value='".$i."'>".$i."</option>";
																		}
																	}
																?>
																<option value="61">60+</option>
															</select>
														</div>
														
														<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 interest_div">
															<select class="form-control form control-sm to_date">
															<option value="0">--To--</option>
															<?php
																for($i=18;$i<61;$i++)
																{
																	if($to_date == $i)
																	{
																		echo "<option value='".$i."' selected>".$i."</option>";
																	}
																	else
																	{
																		echo "<option value='".$i."'>".$i."</option>";
																	}
																}
															?>
															<option value="61">60+</option>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="religion" class="control-label">
														Religion:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-4 col-lg-4 interest_div">
													<select class="form-control form control-sm religion">
														<option value="0">--Religion--</option>
														<?php
															$query  = "SELECT * FROM `religion` WHERE `status`='1' ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$religion_name =  $row['name'];
																$religion_id = $row['id']; 

																if($religion_id==$religion)
																{
																	echo "<option value='".$religion_id."' selected>".$religion_name."</option>";
																}
																else
																{
																	echo "<option value=".$religion_id.">".$religion_name."</option>";
																}
															}

															if($religion=='-1')
															{
																echo "<option value='-1' selected>--- Any ---</option>";
															}
															else
															{
																echo "<option value='-1'>--- Any ---</option>";
															}
										              	?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="caste" class="control-label">
														Caste:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control form control-sm caste">
														<option value="0">--Caste--</option>
														<?php
										              		$query  = "SELECT * FROM `caste` WHERE `status`='1' ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$caste_name =  $row['name'];
																$caste_id = $row['id']; 

																if($caste_id==$caste)
																{
																	echo "<option value='".$caste_id."' selected>".$caste_name."</option>";
																}
																else
																{
																	echo "<option value=".$caste_id.">".$caste_name."</option>";
																}
															}

															if($caste=='-1')
															{
																echo "<option value='-1' selected>--- Any ---</option>";
															}
															else
															{
																echo "<option value='-1'>--- Any ---</option>";
															}
										              	?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="mother_tongue" class="control-label">
														Mother Tongue:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control form control-sm mother_tongue">
														<option value="0">--Mother Tongue--</option>
														<?php
										              		$query  = "SELECT * FROM `mothertongue` WHERE `status`='1' ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$mothertongue_name =  $row['name'];
																$mothertongue_id = $row['id'];

																if($mothertongue_id==$mother_tongue)
																{
																	echo "<option value='".$mothertongue_id."' selected>".$mothertongue_name."</option>";
																}
																else
																{
																	echo "<option value=".$mothertongue_id.">".$mothertongue_name."</option>";
																}
															}

															if($mother_tongue=='-1')
															{
																echo "<option value='-1' selected>--- Any ---</option>";
															}
															else
															{
																echo "<option value='-1'>--- Any ---</option>";
															}
										              	?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="education" class="control-label">
														Minimum Education:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control form control-sm education">
														<option value="0">--Education--</option>
														<?php
										              		$query  = "SELECT * FROM `educationname` WHERE `status`='1' ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$education_name =  $row['name'];
																$education_id = $row['id']; 

																if($education_id==$education)
																{
																	echo "<option value=".$education_id." selected>".$education_name."</option>";
																}
																else
																{
																	echo "<option value=".$education_id.">".$education_name."</option>";
																}
															}

															if($education=='-1')
															{
																echo "<option value='-1' selected>--- Any ---</option>";
															}
															else
															{
																echo "<option value='-1'>--- Any ---</option>";
															}
										              	?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="occupation" class="control-label">
														Occupation:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control form control-sm occupation">
														<option value="0">--Occupation--</option>
														<?php
										              		$query  = "SELECT * FROM `employment` ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$employment_name =  $row['name'];
																$employment_id = $row['id']; 

																if($employment_id==$occupation)
																{
																	echo "<option value=".$employment_id." selected>".$employment_name."</option>";
																}
																else
																{
																	echo "<option value=".$employment_id.">".$employment_name."</option>";
																}
															}

															if($occupation=='-1')
															{
																echo "<option value='-1' selected>--- Any ---</option>";
															}
															else
															{
																echo "<option value='-1'>--- Any ---</option>";
															}
										              	?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="currency" class="control-label">
														Income Currency:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control form control-sm currency">
														<option value="0">--Income Currency--</option>
														<?php
										              		$query  = "SELECT * FROM `currency` ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$currency_name =  $row['currency']." [".$row['code']."]";
																$currency_id = $row['id']; 

																if($currency_id==$currency)
																{
																	echo "<option value=".$currency_id." selected>".$currency_name."</option>";
																}
																else
																{
																	echo "<option value=".$currency_id.">".$currency_name."</option>";
																}
															}
										              	?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="monthly_income" class="control-label">
														Monthly Income:
													</label>
												</div>

												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 interest_div">
													<div class="row">	
														<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 interest_div">
															<input type="number" class="form-control monthly_income_from" value="<?php echo $monthly_income_from;?>" maxlength="6" placeholder="From">
														</div>
														<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 interest_div">
															<input type="number" class="form-control monthly_income_to" value="<?php echo $monthly_income_to;?>" maxlength="6" placeholder="To">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
								  		<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="country" class="control-label">
														Country:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control country" id="country" required>
										              	<option value="0">--Country--</option>
										              	<?php
										              		$query  = "SELECT * FROM `countries` ORDER BY `name` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$country_name =  $row['name'];
																$country_id = $row['id']; 

																if($country_id==$country)
																{
																	echo "<option value=".$country_id." selected>".$country_name."</option>";
																}
																else
																{
																	echo "<option value=".$country_id.">".$country_name."</option>";
																}
															}

															if($country=='-1')
															{
																echo "<option value='-1' selected>--- Any ---</option>";
															}
															else
															{
																echo "<option value='-1'>--- Any ---</option>";
															}
										              	?>
										            </select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="state" class="control-label">
														State:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control state" id="state" required>
														<option value="0">--State--</option>
														<?php
															if($country!='0')
															{
																$caste_sql  = "SELECT * FROM `states` WHERE `country_id`='$country' ORDER BY `name` ASC";
												                $stmt   = $link->prepare($caste_sql);
												                $stmt->execute();
												                $states_userRow = $stmt->fetchAll();
												                
												                foreach( $states_userRow as $row )
																{
																	$state_name =  $row['name'];
																	$state_id = $row['id']; 

																	if($state_id==$state)
																	{
																		echo "<option value=".$state_id." selected>".$state_name."</option>";
																	}
																	else
																	{
																		echo "<option value=".$state_id.">".$state_name."</option>";
																	}
																}


																if($state=='-1')
																{
																	echo "<option value='-1' selected>--- Any ---</option>";
																}
																else
																{
																	echo "<option value='-1'>--- Any ---</option>";
																}
															}
											                
											            ?>
										            </select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="city" class="control-label">
														City:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control city" id="city" required>
														<option value="0">--City--</option>
														<?php
															if($state!='0')
															{
																$caste_sql  = "SELECT * FROM `cities` WHERE `state_id`='$state' ORDER BY `name` ASC";
												                $stmt   = $link->prepare($caste_sql);
												                $stmt->execute();
												                $city_userRow = $stmt->fetchAll();
												                
												                foreach( $city_userRow as $row )
																{
																	$city_name =  $row['name'];
																	$city_id = $row['id']; 

																	if($city_id==$city)
																	{
																		echo "<option value=".$city_id." selected>".$city_name."</option>";
																	}
																	else
																	{
																		echo "<option value=".$city_id.">".$city_name."</option>";
																	}
																}

																if($city=='-1')
																{
																	echo "<option value='-1' selected>--- Any ---</option>";
																}
																else
																{
																	echo "<option value='-1'>--- Any ---</option>";
																}
															}
											                
											            ?>
										            </select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
								  	</div>
								  	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								  		<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="age" class="control-label">
														Body Type:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control form control-sm body_type">
														<option value="0">--Body Type--</option>
														<?php
										              		$query  = "SELECT * FROM `bodytype` WHERE `status`='1' ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$name =  $row['name'];
																$id = $row['id']; 

																if($id==$body_type)
																{
																	echo "<option value=".$id." selected>".$name."</option>";
																}
																else
																{
																	echo "<option value=".$id.">".$name."</option>";
																}
															}
										              	?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="age" class="control-label">
														Complexion:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control complexion">
														<option value="0">--Complexion--</option>
														<?php
										              		$query  = "SELECT * FROM `complexion` ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$complexion_name =  $row['name'];
																$complexion_id = $row['id']; 

																if($complexion==$complexion_id)
																{
																	echo "<option value=".$complexion_id." selected>".$complexion_name."</option>";
																}
																else
																{
																	echo "<option value=".$complexion_id.">".$complexion_name."</option>";
																}
															}
										              	?>
										              	<option value="-1">--- Any ---</option>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="height" class="control-label">
														Height From:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control height_from">
														<option value="0">--From--</option>
														<?php
															for($i=54;$i<273;$i++)
															{
																$i_meter = round(($i)/100,2);
			                									$i_foot = round(($i)/30.48,2);
																if($i==$height_from)
																{
																	echo "<option value=".$i." selected>".$i."-cms / ".$i_foot."-fts / ".$i_meter."-mts</option>";
																}
																else
																{
																	echo "<option value=".$i.">".$i."-cms / ".$i_foot."-fts / ".$i_meter."-mts</option>";
																}
															}
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="height" class="control-label">
														Height To:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control height_to">
														<option value="0">--To--</option>
														<?php
															for($i=54;$i<273;$i++)
															{
																$i_meter = round(($i)/100,2);
			                									$i_foot = round(($i)/30.48,2);
																if($i==$height_to)
																{
																	echo "<option value=".$i." selected>".$i."-cms / ".$i_foot."-fts / ".$i_meter."-mts</option>";
																}
																else
																{
																	echo "<option value=".$i.">".$i."-cms / ".$i_foot."-fts / ".$i_meter."-mts</option>";
																}
															}
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="height" class="control-label">
														Family Type:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control fam_type" id="fam_type" required>
										                <option value="0">--Family Type--</option>
										                <?php
										              		$query  = "SELECT * FROM `familytype` WHERE `status`='1' ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$name =  $row['name'];
																$id = $row['id']; 

																if($id==$fam_type)
																{
																	echo "<option value=".$id." selected>".$name."</option>";
																}
																else
																{
																	echo "<option value=".$id.">".$name."</option>";
																}
															}
										              	?>
										            </select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="smoke_habbit" class="control-label">
														Smoking Habbit:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control smoke_habbit">
														<option value="0">--Smoking Habbit--</option>
														<?php
										              		$query  = "SELECT * FROM `smokehabbit` WHERE `status`='1' ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$name =  $row['name'];
																$id = $row['id']; 

																if($id==$smoke_habbit)
																{
																	echo "<option value=".$id." selected>".$name."</option>";
																}
																else
																{
																	echo "<option value=".$id.">".$name."</option>";
																}
															}
										              	?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="drink_habbit" class="control-label">
														Drink Habbit:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control drink_habbit">
														<option value="0">--Drink Habbit--</option>
														<?php
										              		$query  = "SELECT * FROM `drinkhabbit` WHERE `status`='1' ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$name =  $row['name'];
																$id = $row['id']; 

																if($id==$drink_habbit)
																{
																	echo "<option value=".$id." selected>".$name."</option>";
																}
																else
																{
																	echo "<option value=".$id.">".$name."</option>";
																}
															}
										              	?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 label_div">
													<label for="eat_habbit" class="control-label">
														Eat Habbit:
													</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 interest_div">
													<select class="form-control eat_habbit">
														<option value="0">--Eating Habbit--</option>
														<?php
										              		$query  = "SELECT * FROM `eathabbit` WHERE `status`='1' ORDER BY `id` ASC";
															$stmt   = $link->prepare($query);
															$stmt->execute();
															$result = $stmt->fetchAll();
															foreach( $result as $row )
															{
																$name =  $row['name'];
																$id = $row['id']; 

																if($id==$eat_habbit)
																{
																	echo "<option value=".$id." selected>".$name."</option>";
																}
																else
																{
																	echo "<option value=".$id.">".$name."</option>";
																}
															}
										              	?>
													</select>
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
								  	</div>
								</div>				
							</div>
							<div class="panel-footer">
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form_status">
								  		 <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
								  	</div>
								  	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								  		<center><button class="btn btn-primary btn-block btn_submit website-button">Save My Preference</button></center>
								  	</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<div class='skyscrapper-ad'>
			          	<?php
		            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
			            	if($display_skyscrapper_ad=='1')
			            	{
			              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
			            	}
			          	?>
			        </div>
				</div>
			</div>
			
			<div class="row">
				<div class='leaderboard-ad'>
		          	<?php
		            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
		            	if($display_leaderboard_ad=='1')
		            	{
		              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
		            	}
		          	?>
		        </div>
		    </div>
		</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		$('.country').change(function(){         //Fetch states
        	var country_id = $(this).val();
			var task = "Fetch_state_data";  

			$.ajax({
				type:'post',
            	data:'country_id='+country_id+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.state').html(res);
            	}
			});     	
        });

        $('.state').change(function(){         //Fetch cities
        	var state_id = $(this).val();
			var task = "Fetch_city_data";  
			$.ajax({
				type:'post',
            	data:'state_id='+state_id+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.city').html(res);
            	}
			});      	
        });

        $('.from_date').change(function(){         //Fetch cities
        	var from_date = $(this).val();
			var task = "Fetch_age_data";  
			$.ajax({
				type:'post',
            	data:'from_date='+from_date+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.to_date').html(res);
            	}
			});      	
        });

        $('.height_from').change(function(){         //Fetch cities
        	var height_from = $(this).val();
			var task = "Fetch_height_data";  
			$.ajax({
				type:'post',
            	data:'height_from='+height_from+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.height_to').html(res);
            	}
			});      	
        });

		$('.btn_submit').click(function(){
			var gender = $('.gender').val();
			var marital_status = [];
			$('input[type=checkbox]:checked').each(function() {
    			marital_status .push(this.value);
			});

			var from_date = $(".from_date").val();
			var to_date = $(".to_date").val();
			var religion = $(".religion").val();
			var caste = $(".caste").val();
			var mother_tongue = $(".mother_tongue").val();
			var education = $(".education").val();
			var religion = $(".religion").val();
			var occupation = $(".occupation").val();
			var currency = $(".currency").val();
			var monthly_income_from = $(".monthly_income_from").val();
			var monthly_income_to = $(".monthly_income_to").val();
			var country = $(".country").val();
			var state = $(".state").val();
			var city = $(".city").val();
			var body_type = $(".body_type").val();
			var complexion = $(".complexion").val();
			var height_from = $(".height_from").val();
			var height_to = $(".height_to").val();
			var fam_type = $(".fam_type").val();
			var smoke_habbit = $(".smoke_habbit").val();
			var drink_habbit = $(".drink_habbit").val();
			var eat_habbit = $(".eat_habbit").val();
			var task = "My_Preferences_Update";

			if(gender=='' || gender==null || gender=='undefined')
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong> Oops!</strong> Please choose whether you are looking for <strong>Bride</strong> or <strong>Groom</strong>.</div>");
				$(".radio_1").addClass("danger_error");
				return false;
			}

			if(((monthly_income_from!='' || monthly_income_from!=null) && monthly_income_from<0) || ((monthly_income_to!='' || monthly_income_to!=null) && monthly_income_to<0))
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong> Oops!</strong> Please enter valid monthly income.</div>");
				$(".monthly_income").addClass("danger_error");
				return false;
			}

			var data = 'gender='+gender+'&marital_status='+marital_status+'&from_date='+from_date+'&to_date='+to_date+'&religion='+religion+'&caste='+caste+'&mother_tongue='+mother_tongue+'&education='+education+'&religion='+religion+'&occupation='+occupation+'&currency='+currency+'&monthly_income_from='+monthly_income_from+'&monthly_income_to='+monthly_income_to+'&country='+country+'&state='+state+'&city='+city+'&body_type='+body_type+'&complexion='+complexion+'&height_from='+height_from+'&height_to='+height_to+'&fam_type='+fam_type+'&smoke_habbit='+smoke_habbit+'&drink_habbit='+drink_habbit+'&eat_habbit='+eat_habbit+'&task='+task;

			$('.loading_img').show();

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/my_preferences_helper.php',
            	success:function(res)
            	{
            		$('.loading_img').hide();

            		if(res=='success')
            		{
            			$('.form_status').html("<div class='alert alert-success success_result'><span class='fa fa-check'></span><strong> Sucess! </strong> Your Preferences Updated Successfully.</div>");
            			$('.success_result').fadeTo(2000, 500).slideUp(500, function(){
                            window.location.assign('my-preferences.php');
                        });
            		}
            		else
            		{
            			$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-triangle'></span><strong> Error! </strong>"+res+"</div>");
            		}
            	}
            });
		});
	});
</script>