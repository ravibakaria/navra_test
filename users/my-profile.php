<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}

	include("templates/header.php");

	include("templates/profile_completeness_data.php");
?>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>
	
	<?php
		$user_id = $_SESSION['user_id'];
		echo "<input type='hidden' class='user_id' value='$user_id'>";
		$search_value = getUserUniqueCode($user_id);

		$sql_search_user_info = "SELECT * FROM clients WHERE unique_code='$search_value' AND status=1";
		
		$stmt   = $link->prepare($sql_search_user_info);
        $stmt->execute();
        $search_userRow = $stmt->fetch();
    	$search_user_id = $search_userRow['id'];
    	$search_user_first_name = getUserFirstName($search_user_id);
    	$search_user_last_name = $search_userRow['lastname'];
	    $search_user_lastName = substr($search_user_last_name,0,1);
	    $search_user_lastName = $search_user_lastName.'XXXXXXXXX';
	    $profile_link = getWebsiteBasePath()."/users/Profile-$search_value.html";
    	$search_user_dob = getUserDOB($search_user_id);
    	$dispaly_user_dob = date('d-M-Y',strtotime($search_user_dob));
    	$search_user_city = getUserCity($search_user_id);
    	$search_user_state = getUserState($search_user_id);
    	$search_user_country = getUserCountry($search_user_id);
    	$search_user_gender = getUserGender($search_user_id);
    	$search_user_mother_tongue = getUserMotherTongueName($search_user_id);
    	$search_user_family_type = getUserFamilyTypeUser($search_user_id);
    	$search_user_family_status = getUserFamilyStatusUser($search_user_id);
    	$search_user_family_value = getUserFamilyValueUser($search_user_id);
    	if($search_user_gender=='1')
        {
        	$display_gender = "His";
        	$meta_gender = "Male";
        }
        else
        if($search_user_gender=='2')
        {
        	$display_gender = "Her";
        	$meta_gender = "Female";
        }
        else
        if($search_user_gender=='3')
        {
        	$display_gender = "His/Her";
        	$meta_gender = "T-Gender";
        }

    	$siteName = getWebsiteTitle();
    	$WebsiteBasePath = getWebsiteBasePath();
		
		$siteTagline = getWebSiteTagline();
		$sql_search_user_profile = "SELECT photo FROM profilepic WHERE userid='$search_user_id'";
		$stmt1   = $link->prepare($sql_search_user_profile);
        $stmt1->execute();
        $count_user_pic = $stmt1->rowCount();
        if($count_user_pic>0)
        {
        	$search_user_profile = $stmt1->fetch();
	        $search_user_profile_pic_file_name = $search_user_profile['photo'];
	        $search_user_profile_pic = getWebsiteBasePath().'/users/uploads/'.$search_user_id.'/profile/'.$search_user_profile['photo'];
        }
        else
        {
        	$search_user_profile_pic = getWebsiteBasePath().'/images/no_profile_pic.png';
        }

        $sql_search_user_basic = "SELECT * FROM profilebasic WHERE userid='$search_user_id'";
		$stmt   = $link->prepare($sql_search_user_basic);
        $stmt->execute();
        $count_userbasic = $stmt->rowCount();
        $search_userbasic = $stmt->fetch();
        $search_user_height = $search_userbasic['height'];
        $search_user_weight = $search_userbasic['weight'];
        $search_user_eat_habbit = $search_userbasic['eat_habbit'];
        $search_user_smoke_habbit = $search_userbasic['smoke_habbit'];
        $search_user_body_type = $search_userbasic['body_type'];
        $search_user_complexion = $search_userbasic['complexion'];
        $search_user_marital_status = $search_userbasic['marital_status'];
        $search_user_special_case = $search_userbasic['special_case'];
        $search_user_drink_habbit = $search_userbasic['drink_habbit'];

        $sql_search_user_education = "SELECT education,occupation,income,income_currency FROM eduocc WHERE userid='$search_user_id'";
		$stmt   = $link->prepare($sql_search_user_education);
        $stmt->execute();
        $count_user_education = $stmt->rowCount();
        $search_usereducation = $stmt->fetch();
        $search_user_edu_name = $search_usereducation['education'];
        $search_user_occ_name = $search_usereducation['occupation'];
        $search_user_income = $search_usereducation['income'];
        $search_user_curr_name = $search_usereducation['income_currency'];

        $search_user_occ_name_meta = getUserEmploymentName($search_user_occ_name);
	?>

	<meta property="og:title" content="Matrimonial Profile of <?php echo $search_user_first_name; ?> - <?= $siteName; ?>" />
  	<meta property="og:url" content="<?= $profile_link; ?>" />
  	<meta property="og:description" content="Matrimonial Profile of <?php echo $search_user_first_name; ?> From <?php echo $search_user_city;?>, <?php echo $search_user_state;?>, <?php echo $search_user_country;?>. <?php echo $search_user_first_name.'\'s '.$siteName;?>  Profile ID is <?php echo $search_value;?>. <?php echo $search_user_first_name;?>'s Date Of Birth is <?php echo $dispaly_user_dob;?>. <?php echo $search_user_first_name;?>'s mother tongue is <?php echo $search_user_mother_tongue;?>. <?php echo $search_user_first_name;?> lives in a <?php echo $search_user_family_type;?>. <?php echo $search_user_first_name;?> belongs to <?php echo $search_user_family_status;?> family. <?php echo $search_user_first_name;?> follows <?php echo $search_user_family_value;?> Family Values." />
  	<meta property="og:image" content="<?= $search_user_profile_pic;?>" />
  	<meta property="twitter:title" content="Matrimonial Profile of <?php echo $search_user_first_name; ?> - <?= $siteName; ?>" />
  	<meta property="twitter:url" content="<?= $profile_link; ?>" />
  	<meta property="twitter:description" content="Matrimonial Profile of <?php echo $search_user_first_name; ?> From <?php echo $search_user_city;?>, <?php echo $search_user_state;?>, <?php echo $search_user_country;?>. <?php echo $search_user_first_name.'\'s '.$siteName;?>  Profile ID is <?php echo $search_value;?>. <?php echo $search_user_first_name;?>'s Date Of Birth is <?php echo $dispaly_user_dob;?>. <?php echo $search_user_first_name;?>'s mother tongue is <?php echo $search_user_mother_tongue;?>. <?php echo $search_user_first_name;?> lives in a <?php echo $search_user_family_type;?>. <?php echo $search_user_first_name;?> belongs to <?php echo $search_user_family_status;?> family. <?php echo $search_user_first_name;?> follows <?php echo $search_user_family_value;?> Family Values." />
  	<meta property="twitter:image" content="<?= $search_user_profile_pic;?>" />
<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>


<section role="main" class="content-body main-section-start">

	<!-- start: page -->
	<div class='row start_section_my_photos'>
		<div class='advanced-search-result-header'>
			<h1>My Profile <a href="<?php echo $WebsiteBasePath.'/users/Profile-'.$search_value.'.html';?>"><span class="fa fa-eye"></span></a></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">

			<div class="row my-profile-data">
				<strong class="profile_info_data">Edit Profile Photo:</strong> &nbsp;&nbsp;&nbsp;<a href='upload-profile-picture.php' class='btn btn-primary btn-sm website-button'>Edit</a>
				<div class="panel-body profile_status">
					<div class="thumb-info mb-md">
						<center>
							<?php
								if($count_user_pic>0)
								{
									echo "<a href='upload-profile-picture.php'><img src='$search_user_profile_pic' class='rounded img-responsive' alt='$user_name'></a>";
								}
								else
								{
									echo "<a href='upload-profile-picture.php'><img src='$WebsiteBasePath/images/no_profile_pic.png' class='rounded img-responsive' alt='$user_name'></a>";
								}
							?>
						</center>
						<div class="thumb-info-title user-profile-thumb">
							<span class="thumb-info-inner"><center><?php echo $user_name?></center></span>
							<span class="thumb-info-type"><center>Your Profile ID: <?php echo $unique_code?></center></span>
						</div>
					</div>
					<br/>
					<div class="widget-toggle-expand mb-md">
						<div class="widget-header">
							<h3 class="profile_info_data">Profile Score &nbsp;&nbsp;<strong><?php echo $profile_completeness_percent; ?>%</strong></h6>
						</div>
						<div class="widget-content-collapsed">
							<div class="progress progress-xs light">
								<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $profile_completeness_percent;?>%;">
									<?php echo $profile_completeness_percent;?>%
								</div>
							</div>
						</div>
						<div class="widget-content-expanded">
							<ul class="simple-todo-list">
								<li>
									<?php 
									if($count_profile_pic>0)
										{
											echo "<p class='text-success'><span class='fa fa-check'></span> Profile Picture Updated</p>";
										}
										else
										{
											echo "<p class='text-danger'><span class='fa fa-times'></span> Update Profile Picture</p>";
										}	
									?>
								</li>

								<li>
									<?php 
									if($count_personal_info>0)
										{
											echo "<p class='text-success'><span class='fa fa-check'></span> Personal Info Updated</p>";
										}
										else
										{
											echo "<p class='text-danger'><span class='fa fa-times'></span> Update Personal Info</p>";
										}	
									?>
								</li>
								<li>
									<?php 
									if($count_religious_info>0)
										{
											echo "<p class='text-success'><span class='fa fa-check'></span> Religious Info Updated</p>";
										}
										else
										{
											echo "<p class='text-danger'><span class='fa fa-times'></span> Update Religious Info</p>";
										}	
									?>	
								</li>
								<li>
									<?php 
									if($count_location_info>0)
										{
											echo "<p class='text-success'><span class='fa fa-check'></span> Location Info Updated</p>";
										}
										else
										{
											echo "<p class='text-danger'><span class='fa fa-times'></span> Update Location Info</p>";
										}	
									?>
								</li>
								<li>
									<?php 
										if($count_education_info>0)
										{
											echo "<p class='text-success'><span class='fa fa-check'></span> Education Info Updated</p>";
										}
										else
										{
											echo "<p class='text-danger'><span class='fa fa-times'></span> Update Education Info</p>";
										}	
									?>
								</li>
								<li>
									<?php 
										if($count_family_info>0)
										{
											echo "<p class='text-success'><span class='fa fa-check'></span> Family Info Updated</p>";
										}
										else
										{
											echo "<p class='text-danger'><span class='fa fa-times'></span> Update Family Info</p>";
										}	
									?>
								</li>
								<li>
									<?php 
										if($count_myself_info>0)
										{
											echo "<p class='text-success'><span class='fa fa-check'></span> About Yourself Updated</p>";
										}
										else
										{
											echo "<p class='text-danger'><span class='fa fa-times'></span> Update About Yourself</p>";
										}	
									?>
								</li>
							</ul>
						</div>
					</div>
					<hr>
					<div class="widget-toggle-expand mb-md">
						<div class="widget-header">
							<?php
								$profile_status = getUserStatus($user_id);

								if($profile_status=='1')
								{
							?>
								<center><button class="btn btn-primary btn_deactivate_profile website-button">Deactivate Profile</button> <span class="badge" data-toggle="tooltip" data-placement="top" title="Note: If you deactivate your profile, then it will not be visible to other members. You can re-activate your account in future."><span class="fa fa-exclamation"></span> Note</span></center>
							<?php
								}
								else
								if($profile_status=='2')
								{
							?>
								<center><button class="btn btn-primary btn_activate_profile website-button">Activate Profile</button> <span class="badge" data-toggle="tooltip" data-placement="top" title="Note: If you activate your profile, your profile will be visible to other members."><span class="fa fa-exclamation"></span> Note</span></center>
							<?php
								}
							?>
						</div>
						<div class="widget-content-collapsed">
							<center><img src="../images/loader/loader.gif" class='img-responsive loading_img_deactivate_profile' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
							<div class="deactivate_profile_status"></div>
						</div>
					</div>
					<hr>
					<div class='skyscrapper-ad'>
		          		<?php
		            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
			            	if($display_skyscrapper_ad=='1')
			            	{
			              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
			            	}
			          	?>
			        </div>					

					
				</div>
			</div>

		</div>
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
			<?php
				if(isset($_GET['action']))
				{
					$action = $_GET['action'];
				}
			?>
			<div class="row my-profile-details">
				<h2>Update profile details</h2>
			</div>
			<div class="row">
				<div class="panel-group" id="UserProfile" role="tablist" aria-multiselectable="true">
					<div class="row profile-rows" id="basic">
						<div class="panel" id="user_profile_panel">
							<div class="panel-heading">
								<h3>Basic Information</h3> 
								<?php
									if(!isset($_GET['action']))
									{
										echo "<a href='my-profile.php?action=basic' class='btn btn-sm  btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
									}
									else
									if(isset($_GET['action']) && $_GET['action']!='basic')
									{
										echo "<a href='my-profile.php?action=basic' class='btn btn-sm btn-primary website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
									}
								?>
							</div>
							<div id="basic_profile" class="panel-collapse collapse in">
								<div class="panel-body">
									<fieldset class="user-profile-fieldset">
										<?php require_once 'modules/profile/basic-info.php'; ?>
									</fieldset>
								</div>
							</div>
						</div>
					</div>	
					<hr/>
					<div class="row">
						<div class="col-md-6 col-lg-6" id="religious">
							<div class="row profile-rows">
								<div class="panel" id="user_profile_panel">
									<div class="panel-heading">
										<h3>Religious Information</h3> 
										<?php
											if(!isset($_GET['action2']))
											{
												echo "<a href='my-profile.php?action2=religious' class='btn btn-sm btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
											}
											else
											if(isset($_GET['action2']) && $_GET['action2']!='religious')
											{
												echo "<a href='my-profile.php?action2=religious' class='btn btn-sm btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
											}
										?>
									</div>
									<div id="religious-info" class="panel-collapse collapse in">
										<div class="panel-body">
											<fieldset class="user-profile-fieldset">
												<?php require_once 'modules/profile/religious-info.php'; ?>
											</fieldset>
										</div>
									</div>
								</div>
							</div>
							<div class="row profile-rows">
								<br/>
							</div>
							<div class="row profile-rows">
									<div class="panel" id="user_profile_panel">
										<div class="panel-heading">
											<h3>Location Information</h3> 
											<?php
												if(!isset($_GET['action3']))
												{
													echo "<a href='my-profile.php?action3=location' class='btn btn-sm btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
												}
												else
												if(isset($_GET['action3']) && $_GET['action3']!='location')
												{
													echo "<a href='my-profile.php?action3=location' class='btn btn-sm btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
												}
											?>
										</div>
										<div id="location-info" class="panel-collapse collapse in">
											<div class="panel-body">
												<fieldset class="user-profile-fieldset">
													<?php require_once 'modules/profile/location-info.php'; ?>
												</fieldset>
											</div>
										</div>
									</div>
							</div>
						</div>
						
						<div class="col-md-6 col-lg-6" id="education">
							<div class="row profile-rows">
								<div class="panel" id="user_profile_panel">
									<div class="panel-heading">
										<h3>Education / Occupation Information</h3>
										<?php
											if(!isset($_GET['action4']))
											{
												echo "<a href='my-profile.php?action4=edu' class='btn btn-sm btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
											}
											else
											if(isset($_GET['action4']) && $_GET['action4']!='edu')
											{
												echo "<a href='my-profile.php?action4=edu' class='btn btn-sm btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
											}
										?>
									</div>
									<div id="edu-info" class="panel-collapse collapse in">
										<div class="panel-body">
											<fieldset class="user-profile-fieldset">
												<?php require_once 'modules/profile/edu-info.php'; ?>
											</fieldset>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>

					<hr/>
					<div class='leaderboard-ad text-center'>
			          	<?php
			            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
			            	if($display_leaderboard_ad=='1')
			            	{
			              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
			            	}
			          	?>
			        </div>
					<div class="row">
						<div class="col-md-6 col-lg-6" id="family">
							<div class="row profile-rows">
								<div class="panel" id="user_profile_panel">
									<div class="panel-heading">
										<h3>Family Information</h3>
										<?php
											if(!isset($_GET['action5']))
											{
												echo "<a href='my-profile.php?action5=family' class='btn btn-sm btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
											}
											else
											if(isset($_GET['action5']) && $_GET['action5']!='family')
											{
												echo "<a href='my-profile.php?action5=family' class='btn btn-sm btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
											}
										?>
									</div>
									<div id="family-info" class="panel-collapse collapse in">
										<div class="panel-body">
											<fieldset class="user-profile-fieldset">
												<?php require_once 'modules/profile/family-info.php'; ?>
											</fieldset>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-6" id="myself">
							<div class="row profile-rows">
								<div class="panel" id="user_profile_panel">
									<div class="panel-heading">
										<h3>About</h3>
										<?php
											if(!isset($_GET['action6']))
											{
												echo "<a href='my-profile.php?action6=myself' class='btn btn-sm btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
											}
											else
											if(isset($_GET['action6']) && $_GET['action6']!='myself')
											{
												echo "<a href='my-profile.php?action6=myself' class='btn btn-sm btn-primary edit-button website-button'><i class='ace-icon fa fa-pencil red'></i> Edit</a>";
											}
										?>
									</div>
									<div id="myself-info" class="panel-collapse collapse in">
										<div class="panel-body">
											<fieldset class="user-profile-fieldset">
												<?php require_once 'modules/profile/myself-info.php'; ?>
											</fieldset>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end: page -->
</section>
</div>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	function toggleIcon(e) {
	    $(e.target)
	        .prev('.panel-heading')
	        .find(".more-less")
	        .toggleClass('glyphicon-plus glyphicon-minus');
	}
	$('.panel-group').on('hidden.bs.collapse', toggleIcon);
	$('.panel-group').on('shown.bs.collapse', toggleIcon);

	/*  Datepicker setting  */
	var date_input=$('input[name="dob"]');
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    
    date_input.datepicker({
      format: 'yyyy-mm-dd',
      container: container,
      todayHighlight: true,
      autoclose: true,
    })

	$(document).ready(function(){
		

        $('.country').change(function(){         //Fetch states
        	var country_id = $(this).val();
			var task = "Fetch_state_data";  

			$.ajax({
				type:'post',
            	data:'country_id='+country_id+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.state').html(res);
            	}
			});     	
        });

        $('.state').change(function(){         //Fetch cities
        	var state_id = $(this).val();
			var task = "Fetch_city_data";  
			$.ajax({
				type:'post',
            	data:'state_id='+state_id+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.city').html(res);
            	}
			});      	
        });

        $('.designation_category').change(function(){         //designation 
        	var designation_category = $(this).val();
			var task = "Fetch_designation_data";  

			$.ajax({
				type:'post',
            	data:'designation_category='+designation_category+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.designation').html(res);
            	}
			});      	
        });

        /* removing basic profile error_class  */
        $('.firstname, .lastname, .dob, .phonenumber, .height, .body_type, .weight, .complexion, .eat_habbit, .marital_status, .smoke_habbit, .special_case, .drink_habbit').click(function(){
            $(this).removeClass("danger_error");
            $('.basic_status').html("");
        });

        /*  Basic information Submit button   */
		$('.btn_basic_submit').click(function(){
			var firstname = $('.firstname').val();
			var lastname = $('.lastname').val();
			var dob = $('.dob').val();
			var phonenumber = $('.phonenumber').val();
			var height = $('.height').val();
			var body_type = $('.body_type').val();
			var weight = $('.weight').val();
			var complexion = $('.complexion').val();
			var eat_habbit = $('.eat_habbit').val();
			var marital_status = $('.marital_status').val();
			var smoke_habbit = $('.smoke_habbit').val();
			var special_case = $('.special_case').val();
			var drink_habbit = $('.drink_habbit').val();
			var task = "Update_Basic_Info";
			
			if(firstname=='' || firstname=='')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Enter your first name.</div>");
				$(".firstname").addClass("danger_error");
				return false;
			}

			if(lastname=='' || lastname=='')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Enter your last name.</div>");
				$(".lastname").addClass("danger_error");
				return false;
			}

			if(dob=='' || dob=='')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Enter select your date of birth.</div>");
				$(".dob").addClass("danger_error");
				return false;
			}

			if(phonenumber=='' || phonenumber=='')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Enter mobile number.</div>");
				$(".phonenumber").addClass("danger_error");
				return false;
			}

			if(height=='0')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select height in [cm].</div>");
				$(".height").addClass("danger_error");
				return false;
			}

			if(body_type=='0')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select body type.</div>");
				$(".body_type").addClass("danger_error");
				return false;
			}

			if(weight=='0')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select weight in [kg].</div>");
				$(".weight").addClass("danger_error");
				return false;
			}

			if(complexion=='0')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select complexion.</div>");
				$(".complexion").addClass("danger_error");
				return false;
			}

			if(eat_habbit=='0')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select eating habbit.</div>");
				$(".eat_habbit").addClass("danger_error");
				return false;
			}

			if(marital_status=='0')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select marital status.</div>");
				$(".marital_status").addClass("danger_error");
				return false;
			}

			if(smoke_habbit=='0')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select smoking habbit.</div>");
				$(".smoke_habbit").addClass("danger_error");
				return false;
			}

			if(special_case=='0')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select special case.</div>");
				$(".special_case").addClass("danger_error");
				return false;
			}

			if(drink_habbit=='0')
			{
				$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select drinking habbit.</div>");
				$(".drink_habbit").addClass("danger_error");
				return false;
			}

			$('.loading_img').show();
			var data = 'firstname='+firstname+'&lastname='+lastname+'&dob='+dob+'&phonenumber='+phonenumber+'&height='+height+'&body_type='+body_type+'&weight='+weight+'&complexion='+complexion+'&eat_habbit='+eat_habbit+'&marital_status='+marital_status+'&smoke_habbit='+smoke_habbit+'&special_case='+special_case+'&drink_habbit='+drink_habbit+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/user_profile_basic_helper.php',
            	success:function(res)
            	{
            		$('.loading_img').hide();
            		if(res=='age_error')
            		{
            			$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-triangle'></span><strong>Error! </strong> Oops? Its seems that you are below 18 year. You cannot proceeed further.</div>");
            			$(".dob").addClass("danger_error");
            			return false;
            		}
            		else
            		if(res=='success')
            		{
            			$('.basic_status').html("<div class='alert alert-success' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-check'></span><strong> Sucess!</strong> Basic Profile Information Updated Successfully.</div>");
            			$('.basic_status').fadeTo(2000, 500).slideUp(500, function(){
                            $('.basic_status').slideUp(500);
                            window.location.assign('my-profile.php');
                        });
            		}
            		else
            		{
            			$('.basic_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-triangle'></span><strong>Error! </strong>"+res+"</div>");
            			return false;
            		}
            	}
			})
		});

		
		/* removing religious info error_class  */
        $('.religion, .caste, .mother_tongue, .manglik').click(function(){
            $(this).removeClass("danger_error");
            $('.religious_status').html("");
        });

		/*  religious information Submit button   */
		$('.btn_religious_submit').click(function(){
			var religion = $('.religion').val();
			var caste = $('.caste').val();
			var mother_tongue = $('.mother_tongue').val();
			var task = "Update_Religion_Info";

			if(religion=='0')
			{
				$('.religious_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select religion.</div>");
				$(".religion").addClass("danger_error");
				return false;
			}

			if(caste=='0')
			{
				$('.religious_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select caste.</div>");
				$(".caste").addClass("danger_error");
				return false;
			}

			if(mother_tongue=='0')
			{
				$('.religious_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select mother tongue.</div>");
				$(".mother_tongue").addClass("danger_error");
				return false;
			}

			var data = 'religion='+religion+'&caste='+caste+'&mother_tongue='+mother_tongue+'&task='+task;

			$('.loading_img').show();

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/user_profile_religious_helper.php',
            	success:function(res)
            	{
            		if(res=='success')
            		{
            			$('.religious_status').html("<div class='alert alert-success' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-check'></span><strong> Sucess!</strong> Religious Information Updated Successfully.</div>");
            			$('.religious_status').fadeTo(2000, 500).slideUp(500, function(){
                            $('.religious_status').slideUp(500);
                            window.location.assign('my-profile.php');
                        });
            		}
            		else
            		{
            			$('.religious_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-triangle'></span><strong>Error! </strong>"+res+"</div>");
            			return false;
            		}
            	}
            });
		});


		/* removing religious info error_class  */
        $('.education, .occupation, .designation_category, .designation, .industry, .income_currency, .income').click(function(){
            $(this).removeClass("danger_error");
            $('.education_status').html("");
        });

		/*  religious information Submit button   */
		$('.btn_education_submit').click(function(){
			var education = $('.education').val();
			var occupation = $('.occupation').val();
			var designation_category = $('.designation_category').val();
			var designation = $('.designation').val();
			var industry = $('.industry').val();
			var income_currency = $('.income_currency').val();
			var income = $('.income').val();
			var task = "Update_Education_Info";

			if(education=='0')
			{
				$('.education_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select education.</div>");
				$(".education").addClass("danger_error");
				return false;
			}

			if(occupation=='0')
			{
				$('.education_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select occupation.</div>");
				$(".occupation").addClass("danger_error");
				return false;
			}

			if(designation_category=='0' && occupation!='1')
			{
				$('.education_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select designation category.</div>");
				$(".designation_category").addClass("danger_error");
				return false;
			}

			if(designation=='0' && occupation!='1')
			{
				$('.education_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select designation.</div>");
				$(".designation").addClass("danger_error");
				return false;
			}

			if(industry=='0' && occupation!='1')
			{
				$('.education_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select industry.</div>");
				$(".industry").addClass("danger_error");
				return false;
			}

			if(income_currency=='0' && occupation!='1')
			{
				$('.education_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select income currency.</div>");
				$(".income_currency").addClass("danger_error");
				return false;
			}

			if((income=='' || income==null) && occupation!='1')
			{
				$('.education_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select monthly income.</div>");
				$(".income").addClass("danger_error");
				return false;
			}
			else
			if((income=='' || income==null) && occupation=='1')
			{
				income=0;
			}

			var data = 'education='+education+'&occupation='+occupation+'&designation_category='+designation_category+'&designation='+designation+'&industry='+industry+'&income_currency='+income_currency+'&income='+income+'&task='+task;

			$('.loading_img').show();

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/user_profile_education_helper.php',
            	success:function(res)
            	{
            		if(res=='success')
            		{
            			$('.education_status').html("<div class='alert alert-success' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-check'></span><strong> Sucess!</strong> Educational Information Updated Successfully.</div>");
            			$('.education_status').fadeTo(2000, 500).slideUp(500, function(){
                            $('.education_status').slideUp(500);
                            window.location.assign('my-profile.php');
                        });
            		}
            		else
            		{
            			$('.education_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-triangle'></span><strong>Error! </strong>"+res+"</div>");
            			return false;
            		}
            	}
            });

		});


		/* removing location info error_class  */
        $('.city, .state, .country').click(function(){
            $(this).removeClass("danger_error");
            $('.location_status').html("");
        });

		/*  location information Submit button   */
		$('.btn_location_submit').click(function(){
			var country = $('.country').val();
			var state = $('.state').val();
			var city = $('.city').val();
			var task = "Update_Location_Info";

			if(country=='0')
			{
				$('.location_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select country.</div>");
				$(".country").addClass("danger_error");
				return false;
			}
			
			if(state=='0' || state==null)
			{
				$('.location_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select state.</div>");
				$(".state").addClass("danger_error");
				return false;
			}

			if(city=='0' || city==null)
			{
				$('.location_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select city.</div>");
				$(".city").addClass("danger_error");
				return false;
			}

			var data = 'country='+country+'&state='+state+'&city='+city+'&task='+task;

			$('.loading_img').show();

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/user_profile_location_helper.php',
            	success:function(res)
            	{
            		if(res=='success')
            		{
            			$('.location_status').html("<div class='alert alert-success' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-check'></span><strong> Sucess!</strong> Location Information Updated Successfully.</div>");
            			$('.location_status').fadeTo(2000, 500).slideUp(500, function(){
                            $('.location_status').slideUp(500);
                            window.location.assign('my-profile.php');
                        });
            		}
            		else
            		{
            			$('.location_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-triangle'></span><strong>Error! </strong>"+res+"</div>");
            			return false;
            		}
            	}
            });
        });

        /* removing family info error_class  */
        $('.fam_val, .fam_type, .fam_stat').click(function(){
            $(this).removeClass("danger_error");
            $('.family_status').html("");
        });

		/*  family information Submit button   */
		$('.btn_family_submit').click(function(){
			var fam_val = $('.fam_val').val();
			var fam_type = $('.fam_type').val();
			var fam_stat = $('.fam_stat').val();
			var task = "Update_Family_Info";

			if(fam_val=='0')
			{
				$('.family_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select family value.</div>");
				$(".fam_val").addClass("danger_error");
				return false;
			}

			if(fam_type=='0')
			{
				$('.family_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select family type.</div>");
				$(".fam_type").addClass("danger_error");
				return false;
			}

			if(fam_stat=='0')
			{
				$('.family_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Select family status.</div>");
				$(".fam_stat").addClass("danger_error");
				return false;
			}

			var data = 'fam_val='+fam_val+'&fam_type='+fam_type+'&fam_stat='+fam_stat+'&task='+task;

			$('.loading_img').show();

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/user_profile_family_helper.php',
            	success:function(res)
            	{
            		if(res=='success')
            		{
            			$('.family_status').html("<div class='alert alert-success' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-check'></span><strong> Sucess!</strong> Family Information Updated Successfully.</div>");
            			$('.family_status').fadeTo(2000, 500).slideUp(500, function(){
                            $('.family_status').slideUp(500);
                            window.location.assign('my-profile.php');
                        });
            		}
            		else
            		{
            			$('.family_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-triangle'></span><strong>Error! </strong>"+res+"</div>");
            			return false;
            		}
            	}
            });
		});	

		/* removing about yourself info error_class  */
        $('.short_desc, .short_desc_family, .fam_stat').click(function(){
            $(this).removeClass("danger_error");
            $('.family_status').html("");
        });

        /*   updating number of short_desc remaining characters   */
        $(".short_desc").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
           	var s_d = $('.short_desc').val();
           	var s_d_length = s_d.length;
           	if(s_d_length<300)
           	{
           		$('.remaining_short_desc').html(299-s_d_length);
           	}
        });

        /*   updating number of short_desc_family remaining characters   */
        $(".short_desc_family").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
           	var s_d_f = $('.short_desc_family').val();
           	var s_d_f_length = s_d_f.length;
           	if(s_d_f_length<300)
           	{
           		$('.remaining_short_desc_family').html(299-s_d_f_length);
           	}
           	
        });

        /*   updating number of short_desc_interest remaining characters   */
        $(".short_desc_interest").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
           	var s_d_i = $('.short_desc_interest').val();
           	var s_d_i_length = s_d_i.length;
           	if(s_d_i_length<300)
           	{
           		$('.remaining_short_desc_interest').html(299-s_d_i_length);
           	}
           	
        });

		/*  family about yourself Submit button   */
		$('.btn_myself_submit').click(function(){
			var short_desc = $('.short_desc').val();
			var short_desc_family = $('.short_desc_family').val();
			var short_desc_interest = $('.short_desc_interest').val();
			var task = "Update_Myself_Info";

			if(short_desc=='' || short_desc==null)
			{
				$('.myself_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Write something about yourself.</div>");
				$(".short_desc").addClass("danger_error");
				return false;
			}

			if(short_desc_family=='' || short_desc_family==null)
			{
				$('.myself_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Write something about your family.</div>");
				$(".short_desc_family").addClass("danger_error");
				return false;
			}

			if(short_desc_interest=='' || short_desc_interest==null)
			{
				$('.myself_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span><strong> Empty!</strong> Write something about you looking for profile.</div>");
				$(".short_desc_interest").addClass("danger_error");
				return false;
			}

			var data = 'short_desc='+short_desc+'&short_desc_family='+short_desc_family+'&short_desc_interest='+short_desc_interest+'&task='+task;

			$('.loading_img').show();

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/user_profile_myself_helper.php',
            	success:function(res)
            	{
            		if(res=='success')
            		{
            			$('.myself_status').html("<div class='alert alert-success' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-check'></span><strong> Sucess!</strong> Information About Yourself Updated Successfully.</div>");
            			$('.myself_status').fadeTo(2000, 500).slideUp(500, function(){
                            $('.myself_status').slideUp(500);
                            window.location.assign('my-profile.php');
                        });
            		}
            		else
            		{
            			$('.myself_status').html("<div class='alert alert-danger' style='padding: 10px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-triangle'></span><strong>Error! </strong>"+res+"</div>");
            			return false;
            		}
            	}
            });
		});	


		/*************    Deactivate Profile    *************/
		$('.btn_deactivate_profile').click(function(){
			var user_id = $('.user_id').val();
			var reason;
			var task = "Deactivate_My_Profile";

			reason = prompt("Please enter reason for deactivating your account.");

			if(reason=='' || reason==null)
			{
				$('.deactivate_profile_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span> <strong> Empty! </strong><br/> Please enter reason for deactivating account.</center></div>");
            	return false;
			}

			$('.loading_img_deactivate_profile').show();
			$('.deactivate_profile_status').html("");
			
			var data = "user_id="+user_id+'&reason='+reason+'&task='+task;
			$('.btn_deactivate_profile').attr('disabled',true);

			$.ajax({
				type:'post',
				data:data,
				url:'query/user-profile-deactivate-helper.php',
				success:function(res)
				{
					$('.loading_img_deactivate_profile').hide();
					if(res=='success')
					{
						$('.deactivate_profile_status').html("<div class='alert alert-success deactivate_profile_status_status_new' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span> <strong> Success! </strong><br/> Profile deactivated successfully .</center></div>");
						$('.deactivate_profile_status_status_new').fadeTo(1500, 500).slideUp(500, function(){
                            window.location.assign('logout.php');
                        });
					}
					else
					{
						$('.btn_deactivate_profile').attr('disabled',false);
						$('.deactivate_profile_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-times'></span> <strong> Error! </strong><br/> "+res+"</center></div>");
                		return false;
					}
				}
			});
		});


		/*************    Activate Profile    *************/
		$('.btn_activate_profile').click(function(){
			var user_id = $('.user_id').val();
			var reason;
			var task = "Activate_My_Profile";

			reason = prompt("Please enter comments for activating your account.");

			if(reason=='' || reason==null)
			{
				$('.deactivate_profile_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span> <strong> Empty! </strong><br/> Please enter reason for activating account.</center></div>");
            	return false;
			}

			$('.loading_img_deactivate_profile').show();

			var data = "user_id="+user_id+'&reason='+reason+'&task='+task;
			$('.btn_activate_profile').attr('disabled',true);

			$.ajax({
				type:'post',
				data:data,
				url:'query/user-profile-deactivate-helper.php',
				success:function(res)
				{
					$('.loading_img_deactivate_profile').hide();
					if(res=='success')
					{
						$('.deactivate_profile_status').html("<div class='alert alert-success deactivate_profile_status_status_new' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span> <strong> Success! </strong><br/> Profile deactivated successfully .</center></div>");
						$('.deactivate_profile_status_status_new').fadeTo(3000, 500).slideUp(500, function(){
                            window.location.assign('my-profile.php');
                        });
					}
					else
					{
						$('.btn_activate_profile').attr('disabled',false);
						$('.deactivate_profile_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-times'></span> <strong> Error! </strong><br/> "+res+"</center></div>");
                		return false;
					}
				}
			});
		});
	});		
</script>
