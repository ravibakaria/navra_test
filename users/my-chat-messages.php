<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}
	
	include("templates/header.php");
?>
<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>
	<!-- Profile Slider CSS -->
	<!--link rel="stylesheet" href="../assets/stylesheets/message.css" /-->
	<link rel="stylesheet" href="../css/chat_message.css" />
	<style>
		img{ max-width:100%;}
	</style>
<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
<body style="background-color:#fff;">

	<section role="main" class="content-body main-section-start">

		<div class="row start_section">
			<h1>My Chat Messages.</h1>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="messaging">
							<div class="inbox_msg">
								<div class="inbox_people">
									<div class="headind_srch">
										<div class="recent_heading">
											<h2>Recent</h2>
										</div>
										<div class="srch_bar">
											<div class="stylish-input-group">
												<input type="text" class="search-bar search_user"  placeholder="Search" >
											</div>
										</div>
									</div>

									<input type="hidden" class="user_id" value="<?php echo $user_id;?>">

									<div class="inbox_chat">
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<div class='skyscrapper-ad'>
				          	<?php
			            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
				            	if($display_skyscrapper_ad=='1')
				            	{
				              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
				            	}
				          	?>
				        </div>
					</div>
				</div>
			</div>
		</div>
		<div class='leaderboard-ad'>
          	<?php
            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
            	if($display_leaderboard_ad=='1')
            	{
              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
            	}
          	?>
        </div>
	</section>
</div>
</body>
<?php
	include("templates/footer.php");
?>
</html>

<script>
	$(document).ready(function(){
		var task = "fetch_chat_list";
		$.ajax({
			type:'post',
        	data:'task='+task,
        	url:'query/fetch-chat-list.php',
        	success:function(res)
        	{
        		$('.inbox_chat').html(res);
        	}
        });

        $('.search_user').keyup(function(e){
        	var search_user = String.fromCharCode(e.which);
        	var user_name = $('.search_user').val();
        	var task = "fetch_chat_list_userName";
        	if(user_name.length=='0')
        	{
        		user_name='0';
        	}
        	$.ajax({
				type:'post',
	        	data:'user_name='+user_name+'&task='+task,
	        	url:'query/fetch-chat-list.php',
	        	success:function(res)
	        	{
	        		$('.inbox_chat').html(res);
	        	}
	        });
        });
	});
</script>