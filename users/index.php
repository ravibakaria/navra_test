<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}
	include("templates/header.php");

	$user_id = $_SESSION['user_id'];

	include("templates/profile_pic_check.php");
?>

	<title>
		Dashboard - <?php echo getWebsiteTitle();?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>

		<section role="main" class="content-body main-section-start">
			

			<?php
				$profile_status = getUserStatus($user_id);
				if($profile_status=='1')
				{
					$profile_display_class="success-status";
					$profile_status = "Active";
				}
				else
				if($profile_status=='2')
				{
					$profile_display_class="error";
					$profile_status = "Deactivated";
				}	
			?>
			<div class="row start_section_my_photos">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right logged_in_user">
		        	<span>
						<strong>You Last logged in On: 
							<?php 
								$last_logged_On = getLastUserLoginDate($user_id);
								echo date('d-M-Y h:i A',strtotime($last_logged_On));
							?>
						</strong>
						
						<strong>From IP:<?php echo getLastUserLoginIP($user_id);?></strong>
					</span>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
					<b style="color:<?php echo $sidebar_color;?>">My Profile Status: </b>
					<b class="<?php echo $profile_display_class; ?>"><?php echo $profile_status; ?></b>
				</div>
			</div>

			<!--   Membership plan & Featured Listing   -->
			<div class="row">
				<div class="col-md-12 col-lg-3 col-xl-3">
					<div class="row">
						<!-- Profile View start-->
						<div class="col-md-12 col-lg-12 col-xl-12">
							<section class="panel panel-featured-left dashboard-panels">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-primary user-dashboard-summary-icons">
												<i class="fa fa-bullseye user-dashboard-glyphicon"></i>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title">Profile Views</h4>
												<div class="primary">
													<strong class="amount">
														<?php 
															$profileid = getUserUniqueCode($user_id);
															echo getProfileViewCount($user_id,$profileid)."<br><br><br>";
														?>
															
													</strong>

												</div>
											</div>
											
										</div>
									</div>
								</div>
							</section>	
						</div>
						<!-- Profile View End-->
					</div>
				</div>
				<div class="col-md-12 col-lg-9 col-xl-9">
					<div class="row">
						<!-- Membership Plans start-->
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left dashboard-panels">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-warning user-dashboard-summary-icons">
												<i class="fa fa-star user-dashboard-glyphicon"></i>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title"><strong>Membership Plan Details</strong></h4>
												<div class="primary">
													<h4 class="title">
														<?php 
															$is_premium_member = getMemberIsPremiumOrNot($user_id);

															if($is_premium_member>0)
															{
																$membership_expire_on = getMembershipValidity($user_id);
																$contacts_used = getContactsUsed($user_id);

																$total_contacts = null;

																$stmt = "SELECT * FROM `payment_transactions` WHERE userid='$user_id' AND membership_plan='Yes'"; 
														        $stmt = $link->prepare($stmt);
														        $stmt->execute();
														        $count = $stmt->rowCount();
														        if($count==0)
														        {
														        	$total_contacts = '0';
														        }
														        else
														        if($count>0)
														        {
														        	$result = $stmt->fetchAll();
														        	foreach ($result as $row) 
														        	{
														        		$membership_contacts = $row['membership_contacts'];

														        		if($membership_contacts=='0')
														        		{
														        			$total_contacts = 'Unlimited';
														        			break;
														        		}
														        		else
														        		{
														        			$total_contacts = $total_contacts+$membership_contacts;
														        		}
														        		
														        	}
														        	if($total_contacts=='Unlimited')
														        	{
														        		$total_contacts='Unlimited';
														        	}
														        	else
														        	{
														        		$total_contacts = $total_contacts-$contacts_used;
														        	}
														        }
														        	
														       

																echo "Expiry Date:".date('d-F-Y',strtotime($membership_expire_on))." <br/>";

																echo "Contacts Remaining : ".$total_contacts." <br/>";
															}
															else
															{
																echo "No plan purchased.";

																if(getMembershipPlanCount()>1)
																{
																	echo "<br><p align='right'><a href='../membership-plans.php' style='text-decoration:none;'>Purchase Here >>></a></p>";
																}
															}
														?>
															
													</h4>
												</div>
											</div>
											<div class="summary-footer">
												<a href="my-orders.php" class="btn btn-xs website-button">More info</a>
											</div>
										</div>
									</div>
								</div>
							</section>	
						</div>
						<!-- Membership Plans End-->

						<!-- Featured Listing start-->
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left dashboard-panels">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-warning user-dashboard-summary-icons">
												<i class="fa fa-star-o user-dashboard-glyphicon"></i>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title"><strong>Featured Listing</strong></h4>
												<div class="primary">
													<h4 class="title">
														<?php 
															$is_featured_member = getMemberIsFeaturedOrNot($user_id);
															if($is_featured_member>0)
															{
																$featured_listing_validity = getFeaturedListingValidity($user_id);
																echo "Expiry Date:".date('d-F-Y',strtotime($featured_listing_validity))." <br/>";

																$getRemainingTotalDaysValidity = getRemainingTotalDaysValidity($featured_listing_validity);
																echo "Total days remaining: ".$getRemainingTotalDaysValidity."<br>";
															}
															else
															{
																echo "No plan purchased.";

																if(getFeaturedListingActivatedOrNot()=='1')
																{
																	echo "<br><p align='right'><a href='../membership-plans.php' style='text-decoration:none;'>Purchase Here >>> </a></p> ";
																}
															}
															
														?>	
													</h4>
												</div>
											</div>
											<div class="summary-footer">
												<a href="my-orders.php" class="btn btn-xs website-button">More info</a>
											</div>
										</div>
									</div>
								</div>
							</section>	
						</div>
						<!-- Featured Listing End-->
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 col-lg-3 col-xl-3">
					<section class="panel panel-featured-left dashboard-panels">
						<div class="panel-body">
							<div class="widget-summary">
								<div class="widget-summary-col">
									<div class="summary user-dashboard-profile-summary">
										<center><strong >Profile Score</strong> </center>
										<div class="info">
											<center>
											<?php
												$profile_percent='0';
												$get_user_profile_entry = getUserProfileCompleteEntry($user_id);
												if($get_user_profile_entry=='0')
												{
													echo "<div class='widget-summary-col widget-summary-col-icon'>
														<div class='summary-icon bg-danger  user-dashboard-profile-complete-icons'>
															$profile_percent %
														</div>
													</div>";
													echo "<hr class='profile_complete_seperate_hr'>";
													echo "<p align='justify'><b>Profile Completeness</b></p>";
													echo "<a href='upload-profile-picture.php' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Profile Picture</p></a>";
													echo "<a href='my-profile.php?action=basic' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Personal Info</p></a>";
													echo "<a href='my-profile.php?action2=religious' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Religious Info</p></a>";
													echo "<a href='my-profile.php?action3=location' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Location Info</p></a>";
													echo "<a href='my-profile.php?action4=edu' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Education Info</p></a>";
													echo "<a href='my-profile.php?action5=family' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Family Info</p></a>";
													echo "<a href='my-profile.php?action6=myself' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update About Yourself</p></a>";

												}
												else
												{
													$sql="SELECT * FROM `profile_completeness` WHERE `userid`='$user_id'";
													$stmt=$link->prepare($sql);
													$stmt->execute();
													$result = $stmt->fetch();

													$profile_pic = $result['profile_pic'];
													$basic_info = $result['basic_info'];
													$religious_info = $result['religious_info'];
													$education_info = $result['education_info'];
													$location_info = $result['location_info'];
													$family_info = $result['family_info'];
													$about_info = $result['about_info'];
													
													//Profile Pic
													if($profile_pic=='1')
													{
														$profile_pic_text = "<p class='text-success' align='justify'><span class='fa fa-check'></span> Profile Picture Updated</p>";
														$profile_percent = $profile_percent+10;
													}
													else
													{
														$profile_pic_text =  "<a href='upload-profile-picture.php' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Profile Picture</p></a>";
													}

													//Personal Info
													if($basic_info=='1')
													{
														$basic_info_text =  "<p class='text-success' align='justify'><span class='fa fa-check'></span> Personal Info Updated</p>";
														$profile_percent = $profile_percent+15;
													}
													else
													{
														$basic_info_text =  "<a href='my-profile.php?action=basic' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Personal Info</p></a>";
													}

													//Religious info
													if($religious_info=='1')
													{
														$religious_info_text = "<p class='text-success' align='justify'><span class='fa fa-check'></span> Religious Info Updated</p>";
														$profile_percent = $profile_percent+15;
													}
													else
													{
														$religious_info_text = "<a href='my-profile.php?action2=religious' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Religious Info</p></a>";
													}

													//Location info
													if($location_info=='1')
													{
														$location_info_text = "<p class='text-success' align='justify'><span class='fa fa-check'></span> Location Info Updated</p>";
														$profile_percent = $profile_percent+15;
													}
													else
													{
														$location_info_text = "<a href='my-profile.php?action3=location' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Location Info</p></a>";
													}

													//Education info
													if($education_info=='1')
													{
														$education_info_text = "<p class='text-success' align='justify'><span class='fa fa-check'></span> Education Info Updated</p>";
														$profile_percent = $profile_percent+15;
													}
													else
													{
														$education_info_text = "<a href='my-profile.php?action4=edu' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Education Info</p></a>";
													}

													//Family info
													if($family_info=='1')
													{
														$family_info_text = "<p class='text-success' align='justify'><span class='fa fa-check'></span> Family Info Updated</p>";
														$profile_percent = $profile_percent+15;
													}
													else
													{
														$family_info_text = "<a href='my-profile.php?action5=family' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update Family Info</p></a>";
													}

													//About Yourself info
													if($about_info=='1')
													{
														$about_info_text = "<p class='text-success' align='justify'><span class='fa fa-check' align='justify'></span> About Yourself Updated</p>";
														$profile_percent = $profile_percent+15;
													}
													else
													{
														$about_info_text = "<a href='my-profile.php?action6=myself' style='text-decoration:none;'><p class='text-danger' align='justify'><span class='fa fa-times'></span> Update About Yourself</p></a>";
													}

													if($profile_percent<=60)
													{
														echo "<div class='widget-summary-col widget-summary-col-icon'>
														<center><div class='summary-icon bg-danger user-dashboard-profile-complete-icons profile-percent'>
															$profile_percent %
														</div>
														</center>
													</div>";
													}
													else
													if($profile_percent>=61 && $profile_percent<=80)
													{
														echo "<div class='widget-summary-col widget-summary-col-icon'>
														<center><div class='summary-icon bg-warning user-dashboard-profile-complete-icons profile-percent'>
															$profile_percent %
														</div>
														</center>
													</div>";
													}
													else
													if($profile_percent>=81)
													{
														echo "<div class='widget-summary-col widget-summary-col-icon'>
														<center><div class='summary-icon bg-success user-dashboard-profile-complete-icons profile-percent'>
															$profile_percent %
														</div>
														</center>
													</div>";
													}

													echo "<hr/>";
													echo $profile_pic_text;
													echo $basic_info_text;
													echo $religious_info_text;
													echo $location_info_text;
													echo $education_info_text;
													echo $family_info_text;
													echo $about_info_text;
												}
											?>
											</center>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				

				<div class="col-md-12 col-lg-9 col-xl-9">
					<div class="row">
						<!-- Interest Sent start-->
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left dashboard-panels">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-danger user-dashboard-summary-icons">
												<span class="fa fa-heart-o user-dashboard-glyphicon"></span>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title">Interest Sent</h4>
												<div class="info">
													<strong class="amount"><?php echo getUserInterestSent($user_id);?></strong>
												</div>
											</div>
											<div class="summary-footer">
												<a href="interested-prospects.php?interest=sent" class="btn btn-xs website-button">More info</a>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<!-- Interest Sent End-->

						<!-- Interest received Start-->
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left dashboard-panels">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-danger user-dashboard-summary-icons">
												<span class="fa fa-heart user-dashboard-glyphicon"></span>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title">Interest Received</h4>
												<div class="info">
													<strong class="amount"><?php echo getUserInterestReceived($user_id);?></strong>
												</div>
											</div>
											<div class="summary-footer">
												<a href="interested-prospects.php?interest=received" class="btn btn-xs website-button">More info</a>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<!-- Interest received Start-->

						<!-- Shortlist profiles start-->
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left dashboard-panels">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-primary user-dashboard-summary-icons">
												<span class="fa fa-check-square-o user-dashboard-glyphicon"></span>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title">Shortlisted Profiles</h4>
												<div class="info">
													<strong class="amount"><?php echo getUsershortlistedProfiles($user_id);?></strong>
												</div>
											</div>
											<div class="summary-footer">
												<a href="shortlisted-profiles.php?shortlist=by_me" class="btn btn-xs website-button">More info</a>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<!-- Shortlist profiles End-->

						<!-- User Shortlist By profiles start-->
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left dashboard-panels">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-primary user-dashboard-summary-icons">
												<span class="fa fa-check user-dashboard-glyphicon"></span>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title">Users Shortlisted Your Profile</h4>
												<div class="info">
													<strong class="amount"><?php echo getUsershortlistedYourProfile($user_id);?></strong>
												</div>
											</div>
											<div class="summary-footer">
												<a href="shortlisted-profiles.php?shortlist=by_others" class="btn btn-xs website-button">More info</a>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<!-- User Shortlist By profiles End-->

						<!-- New Messages recieved-->
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left dashboard-panels">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-success user-dashboard-summary-icons">
												<span class="fa fa-comments-o user-dashboard-glyphicon"></span>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title">New Messages Received</h4>
												<div class="info">
													<strong class="amount"><?php echo getUserNewMessage($user_id);?></strong>
												</div>
											</div>
											<div class="summary-footer">
												<a href="my-chat-messages.php" class="btn btn-xs website-button">More info</a>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<!-- Interest Sent End-->

						<!-- New Messages recieved-->
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left dashboard-panels">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-success user-dashboard-summary-icons">
												<i class="fa fa-delicious user-dashboard-glyphicon"></i>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title">My Matches</h4>
												<div class="info">
													<strong class="amount"><?php echo getUserMatchCount($user_id);?></strong>
												</div>
											</div>
											<div class="summary-footer">
												<a href="my-matches.php" class="btn btn-xs website-button">More info</a>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<!-- Interest Sent End-->
					</div>
				</div>

			</div>

			
			<!-- end: page -->
		</section>
	</div>

</section>

<?php
	include("templates/footer.php");
?>