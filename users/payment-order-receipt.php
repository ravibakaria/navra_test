<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	require_once "../config/config.php";
	require_once "../config/dbconnect.php";
	include "../config/functions.php";
	include "../config/setup-values.php";

	$logged_in = CheckUserLoggedIn();
	if($logged_in=='0')
	{
		echo "<script>window.location.assign('login.php');</script>";
		exit;
	}

	$user_id = $_SESSION['user_id'];

	$WebSiteBasePath = getWebsiteBasePath();
	$sitetitle = getWebsiteTitle();
	$logo_array=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo_array[] = $logoURL[$i];
		}

		$logo_path = implode('/',$logo_array);
	}

	if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive payment-receipt-logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }

   	$request_id = quote_smart($_GET['request_id']);

   	$sql_get_pp_details = "SELECT * FROM `payment_transactions` WHERE (`request_id` LIKE '$request_id' OR OrderNumber='$request_id') AND `userid` = '$user_id'";
   	$stmt_get_pp_details = $link->prepare($sql_get_pp_details);
   	$stmt_get_pp_details->execute();
   	$count_get_pp_details = $stmt_get_pp_details->rowCount();

   	if($count_get_pp_details==0)
   	{
   		echo "<br><br><br><br><br>";
   		echo "<center><h1 style='color:red;'>ERROR! Invalid Parameter.<h1></center><br>";
   		echo "<center><a href='index.php'>Back To Home</a></center>";
   		exit;
   	}
   	else
   	{
   		if($count_get_pp_details==1)
   		{
   			$result_get_pp_details = $stmt_get_pp_details->fetch();
   			$receipt_number = $result_get_pp_details['id'];
   			$userid = $result_get_pp_details['userid'];
   			$membership_plan = $result_get_pp_details['membership_plan'];
   			$membership_plan_name = $result_get_pp_details['membership_plan_name'];
   			$membership_plan_id = $result_get_pp_details['membership_plan_id'];
   			$membership_contacts = $result_get_pp_details['membership_contacts'];
   			$membership_plan_amount = $result_get_pp_details['membership_plan_amount'];
   			$membership_plan_expiry_date = $result_get_pp_details['membership_plan_expiry_date'];
   			$featured_listing = $result_get_pp_details['featured_listing'];
   			$featured_listing_amount = $result_get_pp_details['featured_listing_amount'];
   			$featured_listing_expiry_date = $result_get_pp_details['featured_listing_expiry_date'];
   			$tax_applied = $result_get_pp_details['tax_applied'];
   			$tax_name = $result_get_pp_details['tax_name'];
   			$tax_percent = number_format(round($result_get_pp_details['tax_percent'],2),2,'.','');
   			$tax_amount = $result_get_pp_details['tax_amount'];
   			$total_amount = $result_get_pp_details['total_amount'];
   			$tenure = $result_get_pp_details['tenure'];
   			$transact_id = $result_get_pp_details['transact_id'];
   			$request_id = $result_get_pp_details['request_id'];
   			$payment_gateway = $result_get_pp_details['payment_gateway'];
   			$payment_method = $result_get_pp_details['payment_method'];
   			$created_at = $result_get_pp_details['created_at'];
   			$OrderNumber = $result_get_pp_details['OrderNumber'];
   			$status = $result_get_pp_details['status'];

   			if($membership_contacts=='0')
   			{
   				$membership_contacts="Unlimited";
   			}
   		}

   		if($user_id!=$userid)
	   	{
	   		echo "<br><br><br><br><br>";
	   		echo "<center><h1 style='color:red;'>ERROR! Invalid Parameter.<h1></center><br>";
	   		echo "<center><a href='index.php'>Back To Home</a></center>";
	   		exit;
	   	}

	   	/************   User info Details start   **************/
		$first_name = getUserFirstName($user_id);
		$last_name = getUserLastName($user_id);
		$user_name = $first_name." ".$last_name;
		$email = getUserEmail($user_id);
		$mobile = getUserMobile($user_id);
		$phone = '+'.getUserCountryPhoneCode($user_id).'-'.getUserMobile($user_id);
		$profileId = getUserUniqueCode($user_id);

		/************   User info Details End   **************/

		$DefaultCurrency = getDefaultCurrency();
	    $DefaultCurrencyCode = getDefaultCurrencyCode($DefaultCurrency);

	    $purchase_date = date('d F Y',strtotime($created_at));

	    $subtotal = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');

	    $total_tax = number_format(round($tax_amount,2),2,'.','');
	    $total_amount_paid = number_format(round($total_amount,2),2,'.','');
   	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US">
<head>
	<!-- Basic -->
	<meta charset="UTF-8">
	
	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	
	<title>
		<?php
			$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
			$string = str_replace("-", " ", $a);
			echo $title = ucwords($string);
		?>  -  <?php echo getWebsiteTitle(); ?>
	</title>

	<?php
		if(getFaviconURL()!='' || getFaviconURL()!=null)
	    {
	        $favicon_arr=array();
	        $favicon_get = getFaviconURL();
	        if($favicon_get!='' || $favicon_get!=null)
	        {
	          $favicon_get = explode('/',$favicon_get);

	          for($i=1;$i<count($favicon_get);$i++)
	          {
	            $favicon_arr[] = $favicon_get[$i];
	          }

	          $favicon = implode('/',$favicon_arr);
	        }
	        echo "<link rel='icon' type='image/png' href='$WebSiteBasePath/$favicon' sizes='32x32'>";
	    }
	    else
	    {
	        echo "<link rel='icon' type='image/png' href='$WebSiteBasePath/images/favicon/default-favicon.png' sizes='32x32'>";
	    }
	?>

	<!-- CSS -->
	<link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="../css/bootstrap-glyphicons.css" type="text/css">
	<link rel="stylesheet" href="../css/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="../css/theme.css" type="text/css">
	<link rel="stylesheet" href="../css/default.css" type="text/css">
	<link rel="stylesheet" href="../css/theme-custom.css" type="text/css">
	<link rel="stylesheet" href="../css/master.css" type="text/css">
	<link rel="stylesheet" href="../css/custom.css" type="text/css">
	<link rel="stylesheet" href="../config/style.php" type="text/css">
</head>

<body>
	
	<div class="container">
		<div class="row" id='printarea'>
	        <div class="well payment-summary-well col-xs-12 col-sm-12 col-md-7 col-md-offset-3">
	        	<div class="row payment-logo-row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<?php echo $logo; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
						<h3>Payment Receipt</h3>
						<br>
					</div>
				</div>

	            <div class="row">
	                <div class="col-xs-6 col-sm-6 col-md-6">
	                	<strong>To:</strong>
	                    <address>
	                        <strong><?php echo $user_name; ?></strong>
	                        <br>
	                        Email: <?php echo $email; ?>
	                        <br>
	                        Profile Id: <?php echo $profileId; ?>
	                        <br>
	                        Phone: <?php echo $phone; ?>
	                    </address>
	                </div>
	                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
	                	<br>
	                	<address>
	                        <strong>Date:</strong>
	                        <?php echo $purchase_date; ?><br>
	                        <strong>Receipt Number:</strong>
	                        <?php echo $receipt_number; ?><br>
	                        <strong>Order Number:</strong>
	                        <?php echo $OrderNumber; ?><br>
	                        <strong>Payment Status:</strong>
	                        <?php
	                        	if($status=='0')
	                        	{
	                        		$paymentStatus = "UN-PAID";
	                        	}
	                        	else
	                        	if($status=='1')
	                        	{
	                        		$paymentStatus = "PAID";
	                        	}
	                        	echo $paymentStatus;
	                        ?>
	                    </address>
	                </div>
	            </div>

	            <div class="row">
	            	<div class="col-xs-12 col-sm-12 col-md-12">
		                <table id='datatable-info' class='table-responsive table-hover table-bordered' style="width:100%">
		                    <thead>
		                        <tr>
		                            <th>Description</th>
		                            <th>Amount</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    		if($membership_plan=='Yes')
		                    		{
		                    			$membership_plan_description = $membership_plan_name." Membership Plan - ".$membership_contacts." Contacts - Validity ".$tenure." Months";
		                    			echo "<tr>";
		                    				echo "<td>".$membership_plan_description."</td>";
		                    				echo "<td style='text-align:right'>".$DefaultCurrencyCode.":".$membership_plan_amount."</td>";
		                    			echo "</tr>";
		                    		}
		                    	?>
		                     
		                        <?php
		                        	if($featured_listing=='Yes')
		                        	{
		                        		$featured_plan_description = " Featured Listing -  Validity ".$tenure." Months";
		                    			echo "<tr>";
		                    				echo "<td>".$featured_plan_description."</td>";
		                    				echo "<td style='text-align:right'>".$DefaultCurrencyCode.":".$featured_listing_amount."</td>";
		                    			echo "</tr>";
		                    		}
		                        ?>

		                        <tr>
		                        	<td style="text-align:right"><b>Sub Total<b></td>
		                        	<td style="text-align:right"><?php echo $DefaultCurrencyCode.":".$subtotal;?></td>
		                        </tr>

		                        <?php
		                        	if($tax_applied=='1')
		                        	{
		                        		echo "<tr>";
		                    				echo "<td style='text-align:right'><b>".$tax_percent."% ".$tax_name."</b></td>";
		                    				echo "<td style='text-align:right'>".$DefaultCurrencyCode.":".$tax_amount."</td>";
		                    			echo "</tr>";
		                        	}
		                        ?>

		                        <?php
		                        	echo "<tr>";
	                    				echo "<td style='text-align:right'><b>Total</b></td>";
	                    				echo "<td style='text-align:right'>".$DefaultCurrencyCode.":".$total_amount."</td>";
	                    			echo "</tr>";
		                        ?>
		                    </tbody>
		                </table>
		            </div>
	            </div>

	            <br/>

	            <div class="row">
	            	<div class="col-xs-12 col-sm-12 col-md-12">
	                	<table id='datatable-info' class='table-responsive table-hover table-bordered' style="width:100%">
	                		<tr>
	                            <th>Transaction Date</th>
	                            <td><?php echo $purchase_date; ?></td>
	                        </tr>
	                        <tr>
	                            <th>Gateway</th>
	                            <td><?php echo $payment_gateway; ?></td>
	                        </tr>
	                        <tr>
	                            <th>Payment Method</th>
	                            <td><?php echo $payment_method; ?></td>
	                        </tr>
	                        <tr>
	                            <th>Transaction ID</th>
	                            <td><?php echo $transact_id; ?></td>
	                        </tr>
	                        <tr>
	                            <th>Amount</th>
	                            <td><?php echo $DefaultCurrencyCode.":".$total_amount; ?></td>
	                        </tr>
	                	</table>
	                </div>
	            </div>
	            <br/><br/>
	            <div class="row payment-footer-row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<br/>
						<h4><?php echo $sitetitle;?></h4>
						<?php
							$stmt   = $link->prepare("SELECT * FROM `contact_us`");
		                    $stmt->execute();
		                    $result = $stmt->fetch();
		                    $count=$stmt->rowCount();

		                    if($count>0 && $result['companyName']!='')
		                    {
		                        $enquiry_email = $result['enquiry_email'];
		                        $companyName = $result['companyName'];
		                        $street = $result['street'];
		                        $city = $result['city'];
		                        $state = $result['state'];
		                        $country = $result['country'];
		                        $postalCode = $result['postalCode'];
		                        $phonecode = $result['phonecode'];
		                        $phone = $result['phone'];
		                        $companyEmail = $result['companyEmail'];
		                        $companyURL = $result['companyURL'];

		                        $city_name = getUserCityName($city);
		                        $state_name = getUserStateName($state);
		                        $country_name = getUserCountryName($country);

		                        echo "<b>Address: ".$street.", ".$city_name." - ".$postalCode.", ".$state_name." ".$country_name."</b><br/>";
		                        echo "<b>Phone: +".$phonecode." ".$phone." | Email: ".$companyEmail."</b><br/>";
		                        echo "<b>Website: ".$companyURL."</b>";
		                    }
						?>
						<br/>
					</div>
				</div>
	        </div>
	    </div>
	</div>
		
	<div class="row" id='non-printarea'>
    	<center><a href="javascript:void(0)" class="print_payment_receipt">Print</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href="index.php">Home</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href="my-orders.php">My Orders</a></center><br>
    </div>

	<!-- Scripts -->
	<script src="../js/jquery.js"></script>
	<script src="../js/jquery.browser.mobile.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/nanoscroller.js"></script>
	<script src="../js/magnific-popup.js"></script>

</body>
</html>

<script>
	$(document).ready(function(){
		$('.print_payment_receipt').click(function(){
			window.print();
		});
	});
</script>