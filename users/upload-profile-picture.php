<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}
	
	include("templates/header.php");

?>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> - <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>
	<link rel="stylesheet" href="../css/croppie.css" />

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
		<section role="main" class="content-body main-section-start">

			<div class='row'>
				<div class=''>
					<h1>Update My Profile Picture</h1>
				</div>
			</div>
			<!-- start: page -->
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<br/><br/>
									<h2>General Instructions</h2>
									<ul style="list-style-type:circle">
										<li>
											Only files with extention <b><?php echo getAllowedFileAttachmentTypesString();?> </b> is  allowed to upload.
										</li>
										<li>
											Please upload clear & visible images.
										</li>
									</ul>
									
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="" align="center">
					  					<div id="uploaded_image">
					  						<?php
												if($count>0)
												{
													echo "<img src='$profile_pic' class='rounded img_preview' alt='$user_name' >";
												}
												else
												{
													echo "<img src='../images/no_profile_pic.png' class='rounded img_preview' alt='$user_name'>";
												}
											?>
					  					</div>
					  				</div>
					  				<br/>
					  				<div class="" align="center">
					  					<div class="row">
						  					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						  						<center><input type="file" name="upload_image" id="upload_image" class="form-control file-upload"/></center>
						  					</div>
						  					<div class="col-xs-12 col-sm-12 col-md-12  col-md-12 status">
						  						
						  					</div>
						  				</div>
					  				</div>
								</div>
				  				
					  			

					  			<div id="uploadimageModal" class="modal" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content">
								      		<div class="modal-header">
								        		<button type="button" class="close" data-dismiss="modal" style="margin-top:0px;">&times;</button>
								        		<h4 class="modal-title">Upload & Crop Profile Pic</h4>
								      		</div>
								      		<div class="modal-body">
								        		<div class="row">
								  					<div class="col-md-8 text-center">
														  <div id="image_demo" style="width:350px; margin-top:30px"></div>
								  					</div>
								  					<div class="col-md-4" style="padding-top:30px;">
								  						<br />
								  						<br />
								  						<br/>
														  <button class="btn btn-success crop_image">Crop & Save Image</button>
													</div>
												</div>
								      		</div>
								      		<div class="modal-footer">
								        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								      		</div>
								    	</div>
								    </div>
								</div>
					  		</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<div class='skyscrapper-ad'>
				          	<?php
			            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
				            	if($display_skyscrapper_ad=='1')
				            	{
				              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
				            	}
				          	?>
				        </div>
					</div>
				</div>
				<div class='leaderboard-ad'>
		          	<?php
		            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
		            	if($display_leaderboard_ad=='1')
		            	{
		              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
		            	}
		          	?>
		        </div>
			<!-- end: page -->
		</section>
	</div>

</section>

</body>
<?php
	include("templates/footer.php");
?>
<script src="../js/croppie.js"></script>
<script>  
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:300,
      height:300,
      type:'square' //circle
    },
    boundary:{
      width:350,
      height:350
    }
  });

  $('#upload_image').on('change', function(){

  	var file = $(this).val();
	var ext = file.split('.').pop();
	var img_array = "<?php echo getAllowedFileAttachmentTypesString(); ?>";

	var i = img_array.indexOf(ext);

	if(i > -1) 
	{
	    var reader = new FileReader();
	    reader.onload = function (event) {
	      $image_crop.croppie('bind', {
	        url: event.target.result
	      }).then(function(){
	        console.log('jQuery bind complete');
	      });
	    }
	    reader.readAsDataURL(this.files[0]);
	    $('#uploadimageModal').modal('show');
	} 
	else 
	{
	    $('.status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 15px;'><span class='fa fa-times'></span><strong> Error! </strong> Invalid image type.</div>");
	    return false;
	}
    
});

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"upload_profile_pic.php",
        type: "POST",
        data:{"image": response},
        success:function(res)
        {
          $('#uploadimageModal').modal('hide');
          $('#uploaded_image').html(res);
          $('.status').html("<div class='alert alert-success success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Profile Updated  Successfully.</div>");
          $('.success_status').fadeTo(2000, 500).slideUp(500, function(){
				location.reload();
            });
        }
      });
    })
  });

});  
</script>