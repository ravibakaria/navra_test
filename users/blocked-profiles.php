<?php
	include("templates/header.php");
?>
<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
<body style="background-color:#fff;width:100%;">

	<section role="main" class="content-body main-section-start">

		<!-- start: page -->
		<div class='row start_section_my_photos'>
			<div class='advanced-search-result-header'>
				<h1>Blocked Profiles.</h1>
			</div>
		</div>

		<div class="row my-preference-result">
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
				<div class="row">
					<input type="hidden" class="block_profile1" value="blocker" />
					<input type="hidden" class="user_id_x" value="<?php echo $user_id;?>" />
					<div class="result_data">
						
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
	          	<?php
            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
	            	if($display_skyscrapper_ad=='1')
	            	{
	            		echo "<div class='skyscrapper-ad'>";
	              			echo $skyscrapper_ad = getRandomSkyScrapperAdData();
	              		echo "</div>";
	            	}
	          	?>
			</div>
		</div>
		
		<div class='leaderboard-ad'>
          	<?php
            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
            	if($display_leaderboard_ad=='1')
            	{
              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
            	}
          	?>
        </div>
	<!-- end: page -->
	</section>
</div>
</body>
<?php
	include("templates/footer.php");
?>
</html>

<script>
	$(document).ready(function(){
		var value = $('.block_profile1').val();
		var user_id_x = $('.user_id_x').val();
		var task = "Get_Blocked_Profile";
		
		var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

		$.ajax({
			type:'post',
        	data:data,
        	url:'query/blocked_profiles_helper.php',
        	success:function(res)
        	{
        		$('.result_data').html(res);
        	}
        });

		$('input[type=radio][name=block_profile]').change(function() {
			var value = $(this).val();
			var user_id_x = $('.user_id_x').val();
			var task = "Get_Blocked_Profile";
			
			var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/blocked_profiles_helper.php',
            	success:function(res)
            	{
            		$('.result_data').html(res);
            	}
            });
		});

		$('.btn_unblock').click(function(){
			alert('1');return false;
		});
	});
</script>