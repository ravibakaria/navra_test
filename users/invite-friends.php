<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}
	
	include("templates/header.php");

?>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
	<section role="main" class="content-body main-section-start">

			<!-- start: page -->
				<div class='row start_section_my_photos'>
					<div class='advanced-search-result-header'>
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
							<h1>Invite Friends</h1>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
							<button class="btn btn-primary btn-sm website-button" data-toggle="modal" data-target="#Add-New-Invite-Now">Invite Now</button>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
						</div>
					</div>
				</div>

				<div class="row invite-friends-table-data">
					<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
						<div class="row">
			            	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
			            		<div>
			            			<table id='datatable-info' class='table-hover table-striped table-bordered invite_friends_table' style="width:100%">
			                        <thead>
			                            <tr>
			                                <th>Email Id</th>
			                                <th>Invited On</th>
			                                <th>Invitation Sent</th>
			                                <th>Invitation Response</th>
			                            </tr>
			                        </thead>
			                    </table>
				                </div>
				            </div>

				            <!-- Modal -->
				            <div class="modal fade" id="Add-New-Invite-Now" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				                <div class="modal-dialog modal-dialog-centered add-item-model" role="document">
				                    <div class="modal-content">
				                        <div class="modal-header">
				                            <center><h2 class="modal-title" id="exampleModalLongTitle">Invite New Friends</h2></center>
				                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                            <span aria-hidden="true">&times;</span>
				                            </button>
				                        </div>
				                        <div class="modal-body">
				                        	<div class="row">
				                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				                                    <div class="alert alert-info">
				                                    	<span class='fa fa-exclamation-circle'></span> Please enter each email address on new line.
				                                    </div>
				                                </div>
				                            </div>
				                            <div class="row">
				                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				                                    <lable class="control-label">Email Ids:</lable>
				                                </div>
				                                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
				                                    <textarea rows="8" class="form-control invited_email invited-email-textarea" name="invited_email"></textarea>
				                                </div>
				                            </div>
				                            <div class="row">
				                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				                                    <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:40px; height:40px; display:none;'/></center>
				                                </div>
				                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 invite_email_status">
				                                    
				                                </div>
				                            </div>
				                        </div>
				                        <div class="modal-footer">
				                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				                            <button type="button" class="btn btn-primary btn_invite_friends website-button">Submit</button>
				                        </div>
				                    </div>
				                </div>
				            </div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<div class='skyscrapper-ad'>
				          	<?php
			            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
				            	if($display_skyscrapper_ad=='1')
				            	{
				              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
				            	}
				          	?>
				        </div>
					</div>
				</div>
				<div class='leaderboard-ad'>
		          	<?php
		            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
		            	if($display_leaderboard_ad=='1')
		            	{
		              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
		            	}
		          	?>
		        </div>
			<!-- end: page -->
		</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		
		var dataTable_FamilyStatus_list = $('.invite_friends_table').DataTable({          //complexion datatable
            "bProcessing": true,
            "serverSide": true,
            "oLanguage": {
		      "sLengthMenu": "Showing _MENU_ rows",
		    },
            "ajax":{
                url :"datatables/invite_friends_response.php", // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                error: function(){
                    $(".invite_friends_table_processing").css("display","none");
                }
            }
        });

		$('.btn_invite_friends').click(function(){
			var invited_email = $('.invited_email').val();
			var task = "Send_Invitation_Email";

			if(invited_email=='' || invited_email==null)
			{
				$('.invite_email_status').html("<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'><center><span class='fa fa-exclamation-triangle'><strong>Empty data ! </strong> Please enter emails to send invitation.</center></div>");
                return false;	
			}

			$('.loading_img').show();

			var data = 'invited_email='+invited_email+'&task='+task;

			$.ajax({
                type:'post',
                data:data,
                url:'query/invite-friends-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    if(res=='success')
                    {
                        $('.invite_email_status').html("<div class='alert alert-success invite_email_status_success' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span><strong> Sucess! </strong> Invitation uploaded successfully.<br/>You can check status of invitation from invitation data.</center></div>");
                            $('.invite_email_status_success').fadeTo(5000, 500).slideUp(500, function(){
                                  window.location.assign("invite-friends.php");
                                  return false;
                            });
                    }
                    else
                    {
                        $('.invite_email_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Error! </strong><br/> "+res+"</center></div>");
                        return false;
                    }
                }
            });
		});
	});
</script>