<?php
	include("templates/header.php");

?>
<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
	$MinPassLength = getMinimumUserPasswordLength();
	$MaxPassLength = getMaximumUserPasswordLength();
?>
<body class="change-password-body">

	<section role="main" class="content-body" style="margin-top:-50px;">
		<header class="page-header">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Change Password</span></li>
			</ol>
		
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<!-- <li><span><a href="<?php echo $previous_page_url; ?>"><span class="fa fa-arrow-left">&nbsp;Back</span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></li>-->
				</ol>
			</div>
		</header>
		<section class="body-sign change-password-body-sign">
			<div class="center-sign">
				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none title-login-forgot"><i class="fa fa-user mr-xs"></i> Change Password</h2>
					</div>
					<div class="panel-body">
						<form>
							<input type="hidden" class="MinPassLength" value="<?php echo $MinPassLength;?>">
							<input type="hidden" class="MaxPassLength" value="<?php echo $MaxPassLength;?>">

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Current Password</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="crr_pwd" type="password" class="form-control input-lg crr_pwd" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">New Password</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="new_pwd" type="password" class="form-control input-lg new_pwd" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Confirm New Password</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="cnf_new_pwd" type="password" class="form-control input-lg cnf_new_pwd" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>
							<div class="form-group mb-lg">
								<div class="input-group input-group-icon form_status">
									<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:50px; height:50px; display:none;'/></center>
								</div>
							</div>
							<div class="mb-xs text-center">
								<button type="submit" class="btn btn-facebook mb-md ml-xs mr-xs btn_change_password website-button">Change Password</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</section>
</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){

		/* removing religious info error_class  */
        $('.crr_pwd, .new_pwd, .cnf_new_pwd').click(function(){
            $(this).removeClass("danger_error");
            $('.form_status').html("");
        });

		$('.btn_change_password').click(function(){
			var crr_pwd = $('.crr_pwd').val();
			var new_pwd = $('.new_pwd').val();
			var cnf_new_pwd = $('.cnf_new_pwd').val();
			var MinPassLength = $('.MinPassLength').val();
			var MaxPassLength = $('.MaxPassLength').val();

			var task = "User_Change_Password";

			if(MinPassLength=='0' || MinPassLength=='' || MinPassLength==null)
			{
				MinPassLength='8';
			}

			if(MaxPassLength=='0' || MaxPassLength=='' || MaxPassLength==null)
			{
				MaxPassLength='40';
			}


			if(crr_pwd=='' || crr_pwd==null)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter your current password</div>");
				$(".crr_pwd").addClass("danger_error");
				return false;
			}

			if(new_pwd=='' || new_pwd==null)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter new password</div>");
				$(".new_pwd").addClass("danger_error");
				return false;
			}

			if(cnf_new_pwd=='' || cnf_new_pwd==null)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Confirm new password</div>");
				$(".cnf_new_pwd").addClass("danger_error");
				return false;
			}

			if(new_pwd!='' && new_pwd.length < MinPassLength)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Invalid data!</strong>  Password should be of minimum "+MinPassLength+" characters long!</div>");
				$(".new_pwd").addClass("danger_error");
				return false;
			}

			if(new_pwd!='' && new_pwd.length > MaxPassLength)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Invalid data!</strong>  Password length not exceed more than "+MaxPassLength+" !</div>");
				$(".new_pwd").addClass("danger_error");
				return false;
			}

			if(new_pwd!=cnf_new_pwd)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Match Error!</strong> New & Confirm password does not match.</div>");
				$(".new_pwd").addClass("danger_error");
				$(".cnf_new_pwd").addClass("danger_error");
				return false;
			}

			if(crr_pwd==new_pwd)
			{
				$('.form_status').html("<div class='alert alert-danger'><span class='fa fa-exclamation-circle'></span><strong>Error! </strong> Current password & new password should not be same.</div>");
				$(".crr_pwd").addClass("danger_error");
				$(".new_pwd").addClass("danger_error");
				return false;
			}

			$('.crr_pwd').removeClass("danger_error");
			$('.new_pwd').removeClass("danger_error");
			$('.cnf_new_pwd').removeClass("danger_error");

			var data = 'crr_pwd='+crr_pwd+'&new_pwd='+new_pwd+'&cnf_new_pwd='+cnf_new_pwd+'&task='+task;
			
			$('.loading_img').show();

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/change_password_helper.php',
            	success:function(res)
            	{
            		$('.loading_img').hide();

            		if(res=='success')
            		{
            			$('.form_status').html("<div class='alert alert-success login_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-check'></span><strong> Success! </strong><br/> Your password updated successfully.</div>");
            			return false;
            		}
            		else
            		if(res=='db_pass_error')
            		{
            			$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> Your Password does not match with database password.</div>");
            			$(".crr_pwd").addClass("danger_error");
            			return false;
            		}
            		else
            		{
            			$('.form_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><center><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</center></div>");
            			$(".new_pwd").addClass("danger_error");
            			return false;
            		}
            	}
            });
            return false;
		});
		
	});
</script>