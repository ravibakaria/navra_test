<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}
	
	include("templates/header.php");
	$user_id = $_SESSION['user_id'];
	$DefaultCurrency = getDefaultCurrency();
	$DefaultCurrencyCode = getDefaultCurrencyCode($DefaultCurrency);
	$today = date('Y-m-d');
?>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
		<section role="main" class="content-body main-section-start">

			<div class='row start_section_my_photos'>
				<div class="">
					<div class='advanced-search-result-header'>
						<div style="width:50%;"><h1>My Orders Details</h1></div>
						<div class="text-right" style="margin-top: -22px;"><a class="btn  website-button" href="../membership-plans.php" style="text-decoration:none;">Purchase New Plan >>></a></div>
						<br>
						<table id='myorder-table' class='table-hover table-striped table-bordered mb-none my-orders' style="width:100%;">
							<thead>
		                        <tr>
		                            <th></th>
		                            <th></th>
		                            <th></th>
		                            <th colspan="3"><center>Membership</center></th>
		                            <th colspan="2"><center>Featured Listing</center></th>
		                            <th></th>
		                            <th></th>
		                            <th></th>
		                            <th></th>
		                            <th></th>
		                            <th></th>
		                        </tr>
		                        <tr>
		                            <th>Receipt#</th>
		                            <th style="width:70px;">Activation<br/> Date</th>
		                            <th>Order#</th>
		                            <th>Plan Name</th>
		                            <th>Status</th>
		                            <th>Expiry Date</th>
		                            <th>Status</th>
		                            <th>Expiry Date</th>
		                            <th>Tenure</th>
		                            <th>Total Price</th>
		                            <th>Transaction Status</th>
		                            <th>Payment Gateway</th>
		                            <th><center>View</center></th>
		                            <th><center>Print</center></th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    		$sql_order_list = "SELECT * FROM payment_transactions WHERE userid='$user_id' ORDER BY created_at DESC";
		                    		$stmt_order_list = $link->prepare($sql_order_list);
		                    		$stmt_order_list->execute();
		                    		$count_order_list = $stmt_order_list->rowCount();
		                    		if($count_order_list>0)
		                    		{
		                    			$result_order_list = $stmt_order_list->fetchAll();

		                    			foreach ($result_order_list as $row_order_list) 
		                    			{
		                    				$id = $row_order_list['id'];
		                    				$OrderNumber = $row_order_list['OrderNumber'];
		                    				$membership_plan = $row_order_list['membership_plan'];
		                    				$membership_plan_name = $row_order_list['membership_plan_name'];
		                    				$membership_contacts = $row_order_list['membership_contacts'];
		                    				$membership_plan_amount = $row_order_list['membership_plan_amount'];
		                    				$membership_plan_expiry_date = $row_order_list['membership_plan_expiry_date'];
		                    				$featured_listing = $row_order_list['featured_listing'];
		                    				$featured_listing_amount = $row_order_list['featured_listing_amount'];
		                    				$featured_listing_expiry_date = $row_order_list['featured_listing_expiry_date'];
		                    				$tax_applied = $row_order_list['tax_applied'];
		                    				$tax_name = $row_order_list['tax_name'];
		                    				$tax_percent = $row_order_list['tax_percent'];
		                    				$tax_amount = $row_order_list['tax_amount'];
		                    				$total_amount = $row_order_list['total_amount'];
		                    				$tenure = $row_order_list['tenure'];
		                    				$transact_id = $row_order_list['transact_id'];
		                    				$request_id = $row_order_list['request_id'];
		                    				$payment_gateway = $row_order_list['payment_gateway'];
		                    				$payment_method = $row_order_list['payment_method'];
		                    				$created_at = $row_order_list['created_at'];
		                    				$status = $row_order_list['status'];

		                    				if($status=='0')
			                    			{
			                    				$status_display = "<center><b class='label label-warning'>Unpaid</b></center>";
			                    			}
			                    			else
			                    			if($status=='1')
			                    			{
			                    				$status_display = "<center><b class='label label-success'>Paid</b></center>";
			                    			}
			                    			else
			                    			if($status=='2')
			                    			{
			                    				$status_display = "<center><b class='label label-danger'>Canceled</b></center>";
			                    			}
			                    			else
			                    			if($status=='3')
			                    			{
			                    				$status_display = "<center><b class='label label-info'>Refund</b></center>";
			                    			}

		                    				echo "<tr>";

		                    				if($status=='1')
		                    				{
		                    					echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".$id."</a></td>";
		                    					echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".date('d-F-Y',strtotime($created_at))."</a></td>";
		                    					echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".$OrderNumber."</a></td>";	
		                    				}	
	                						else
	                						{
	                							echo "<td>".$id."</td>";
		                    					echo "<td>".date('d-F-Y',strtotime($created_at))."</td>";
		                    					echo "<td>".$OrderNumber."</td>";	
	                						}


		                    					
	                    					if($membership_plan=='Yes' && $status=='1')
	                    					{
			                    				echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".$membership_plan_name."</a></td>";
	                							echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:green'><center><strong><i class='fa fa-check'></strong></center></a></td>";
	                							if($membership_plan_expiry_date<$today)
	                							{
	                								echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:red'>".date('d-M-Y',strtotime($membership_plan_expiry_date))."</a></td>";
	                							}
	                							else
	                							if($membership_plan_expiry_date>=$today)
	                							{
	                								echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000;'>".date('d-M-Y',strtotime($membership_plan_expiry_date))."</a></td>";
	                							}
	                							
	                    					}
	                    					else
	                    					{
	                    						echo "<td>".$membership_plan_name."</td>";
	                    						echo "<td><center><strong>-</strong></center></td>";
	                    						echo "<td><center><strong>-</strong></center></td>";
	                    					}

			                    			if($featured_listing=='Yes' && $status=='1')
			                    			{
			                    				echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:green'><center><strong><i class='fa fa-check'></strong></center></a></td>";
			                    				if($featured_listing_expiry_date<$today)
			                    				{
			                    					echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:red;'>".date('d-M-Y',strtotime($featured_listing_expiry_date))."</a></td>";
			                    				}
			                    				else
			                    				if($featured_listing_expiry_date>=$today)
			                    				{
			                    					echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".date('d-M-Y',strtotime($featured_listing_expiry_date))."</a></td>";
			                    				}
			                    			}
			                    			else
			                    			{
			                    				echo "<td><center><strong>-</strong></center></td>";
			                    				echo "<td><center><strong>-</strong></center></td>";
			                    			}

	                                        if($status=='1')
	                                        {
	                                        	echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".$tenure." Months</a></td>";
			                    				echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".$DefaultCurrencyCode.":".$total_amount."</a></td>";
			                    				echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;'>".$status_display."</a></td>";
			                    				echo "<td><center><strong>".$payment_gateway."</strong></center></td>";
			                    				echo "<td><button class='btn btn-sm' data-toggle='modal' data-target='#plan_".$id."' style='background-color:Transparent;'><img src='../images/view-icon.png' style='height:25px;width:25px;'></button></td>";
			                    				echo "<td><button class='btn btn-sm btn_print_receipt ' id=".$id."  style='background-color:Transparent;' ><img src='../images/print-icon.png' style='height:25px;width:25px;'> </button></td>";
	                                        }
	                                        else
	                                        {
	                                        	echo "<td>".$tenure." Months</td>";
				                    			echo "<td>".$DefaultCurrencyCode.":".$total_amount."</td>";
	                                            echo "<td>".$status_display."</td>";
	                                            echo "<td><center><strong>".$payment_gateway."</strong></center></td>";
	                                            echo "<td><center><b>-</b></center></td>";
	                                            echo "<td><button class='btn btn-sm btn_print_receipt ' id=".$id."  style='background-color:Transparent;' ><img src='../images/print-icon.png' style='height:25px;width:25px;'> </button></td>";
	                                        }
			                    				
			                    			echo "</tr>";

			                    	?>
		                    		<!-- Modal View order details -->
						            <div class="modal fade payment-summary-model" id="plan_<?php echo $id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
						                <div class="modal-dialog modal-dialog-centered membership-plan-model" role="document">
						                    <div class="modal-content">
						                        <div class="modal-header">
						                            <h4 class="modal-title" id="exampleModalLongTitle">Order Details (Receipt ID# <?php echo $id;?>)</h4>
						                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                            <span aria-hidden="true">&times;</span>
						                            </button>
						                        </div>
						                        <div class="modal-body">
						                        	<?php
						                        		if($membership_plan=='Yes')
		                            					{
						                        	?>
						                            <div class="row">
						                            	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						                            		<strong>Membership Plan</strong>
						                            	</div>
						                            	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						                            		<?php
						                            			
			                            						if($membership_contacts=='0')
			                            						{
			                            							$membership_contacts="Unlimited";
			                            						}

			                            						echo "<b>Plan Name: </b>";
			                            						echo $membership_plan_name."<br>";

			                            						echo "<b>Contacts: </b>";
			                            						echo $membership_contacts." Contacts<br>";

			                            						echo "<b>Plan Validity: </b>";
			                            						echo $tenure." Months<br>";

			                            						echo "<b>Expires On: </b>";
			                            						echo date('d-F-Y',strtotime($membership_plan_expiry_date))."<br/>";
			                            						
			                            						echo "<b>Plan Price: </b>";
			                            						echo $DefaultCurrencyCode.":".$membership_plan_amount;
						                            		?>
						                            	</div>
						                            </div>
						                            <hr class="dotted">
						                            <?php
						                            	}
						                            ?>

						                           	<?php
						                           		if($featured_listing=='Yes')
	                        							{
						                           	?> 
						                            <div class="row">
						                            	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						                            		<strong>Featured Listing</strong>
						                            	</div>
						                            	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						                            		<?php
						                            			
			                            						echo "<b>Plan Name: </b>";
			                            						echo "Featured Listing Plan<br>";

			                            						echo "<b>Plan Validity: </b>";
			                            						echo $tenure." Months<br>";

			                            						echo "<b>Expires On: </b>";
			                            						echo date('d-F-Y',strtotime($featured_listing_expiry_date))."<br/>";

			                            						echo "<b>Plan Price: </b>";
			                            						echo $DefaultCurrencyCode.":".$featured_listing_amount;
						                            		?>
						                            	</div>
						                            </div>
						                            <hr class="dotted">
						                            <?php
						                            	}
						                            ?>

						                            <?php
						                           		if($tax_applied=='1')
	                        							{
						                           	?>
						                           	<div class="row">
						                            	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						                            		<strong>Tax Details</strong>
						                            	</div>
						                            	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						                            		<?php
						                            			echo $tax_name." | ".$tax_percent."% | ".$DefaultCurrencyCode.":".$tax_amount;
						                            		?>
						                            	</div>
						                            </div>
						                           	<hr class="dotted">
						                            <?php
						                            	}
						                            ?>

						                            <div class="row">
						                            	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						                            		<strong>Total Amount</strong>
						                            	</div>
						                            	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						                            		<?php
						                            			echo $DefaultCurrencyCode.":".$total_amount."";
						                            		?>
						                            	</div>
						                            </div>
						                            <hr class="dotted">

						                            <div class="row">
						                            	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						                            		<strong>Transaction Details</strong>
						                            	</div>
						                            	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						                            		<?php
						                            			echo "<b>Transaction ID: </b>";
			                            						echo $transact_id."<br>";

			                            						echo "<b>Payment Gateway: </b>";
			                            						echo $payment_gateway."<br>";

			                            						echo "<b>Payment Method: </b>";
			                            						echo $payment_method."<br>";
						                            		?>
						                            	</div>
						                            </div>
						                            
						                        </div>
						                    </div>
						                </div>
						            </div>
							        
			                    	<?php
		                    			}
		                    			
		                    		}
		                    		else
		                    		{
		                    			echo "<tr>";
		                    				echo "<td colspan='14'>No orders found!</td>";
		                    			echo "</tr>";
		                    		}
		                    	?>
		                    </tbody>
						</table>
					</div>
				</div>
			</div>

			<div>
				<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
			</div>

			<div class="receipt-print" id="receipt_print" style="display:none;">
				<!--     receipt data     -->
			</div>
		</section>
	</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>
	
<script>
	$(document).ready(function(){

		//$('.my-orders').DataTable();
		$('.my-orders').DataTable({
		    "order": [], //or asc 
		});

		function printContent(el)
		{
			var restorepage = $('body').html();
			var printcontent = $('#' + el).clone();
			$('body').empty().html(printcontent);
			window.print();
			$('body').html(restorepage);
		}

		$('.btn_print_receipt').click(function(){
			var receipt_id = $(this).attr('id');
			var task = "fetch_receipt_print";

			$('.loading_img').show();
			var data = 'receipt_id='+receipt_id+'&task='+task;

			$.ajax({
                type:'post',
                data:data,
                url:'query/order-receipt-helper.php',
                success:function(res)
                {
                    $('.loading_img').hide();
                    $('.receipt-print').html(res);
                    $('.receipt-print').show();
                    printContent('receipt_print');
                    $('.receipt-print').html("");
                    location.reload();
                }
            });
		});
	});
</script>