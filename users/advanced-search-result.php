<?php
	include("templates/header.php");
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(isset($_SESSION['last_page_accessed']))
	{
		unset($_SESSION['last_page_accessed']);
	}
	
	if(!empty($_POST))
	{
		$_SESSION['search_data']=$_POST;
	}

	
	//print_r($_SESSION['search_data']);exit;
	$gender = $_SESSION['search_data']['gender'];


	if(isset($_SESSION['search_data']['marital_status']))
	{
		$marital_status_show = implode(',', $_SESSION['search_data']['marital_status']);
		$marital_status = $_SESSION['search_data']['marital_status'];
	}
	else
	{
		$marital_status=null;
		$sql_get_marital_status = "SELECT * FROM maritalstatus WHERE status='1'";
		$stmt=$link->prepare($sql_get_marital_status);
		$stmt->execute();
		$result = $stmt->fetchAll();

		foreach ($result as $row) 
		{
			$marital_status[] = $row['id'];
		}
		$marital_status_show = '0';
	}

	if(!empty($_GET))
	{
		$gender = $_GET['gender'];
		$marital_status = array('1','2','3','4','5');
	}


	@$from_date = $_SESSION['search_data']['from_date'];
	@$to_date = $_SESSION['search_data']['to_date'];
	@$religion = $_SESSION['search_data']['religion'];
	@$caste = $_SESSION['search_data']['caste'];
	@$mother_tongue = $_SESSION['search_data']['mother_tongue'];
	@$education = $_SESSION['search_data']['education'];
	@$occupation = $_SESSION['search_data']['occupation'];
	@$currency = $_SESSION['search_data']['currency'];
	@$monthly_income_from = $_SESSION['search_data']['monthly_income_from'];
	@$monthly_income_to = $_SESSION['search_data']['monthly_income_to'];
	@$country = $_SESSION['search_data']['country'];
	@$state = $_SESSION['search_data']['state'];
	@$city = $_SESSION['search_data']['city'];
	
?>
<html class="fixed sidebar-left-collapsed">
	<title>
		<?php
			echo "Advanced Matrimonial Profile Result - ".getWebsiteTitle();
		?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?> Advanced Matrimonial Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members"/>
<?php
	$siteName = getWebsiteTitle();
	$siteTagline = getWebSiteTagline();
	$site_url = getWebsiteBasePath();
	$site_email = getWebSiteEmail();

	$logo=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo[] = $logoURL[$i];
		}

		$logo = implode('/',$logo);
	}
?>
	<meta name="keywords" content="<?php echo $siteName;?>, <?php echo $siteTagline; ?>, Advanced Matrimonial Profile Result">
<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>


<section role="main" class="content-body main-section-start">
	<div class="row ">
		<p class="advanced-search-close-button">
	    	<button type="button" class="btn btn-danger" id="filter-close" data-toggle="collapse" data-target="#filter-panel">
	        <i class="fa fa-times"></i>
	        </button>
	    </p>
	</div>
	<div class="row filter-panel">
		
	    <div id="filter-panel" class="collapse filter-panel">
	        <div class="panel panel-default">
	            <div class="panel-body advanced-search-body">
	                <form class="form-inline" role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
	                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		                    <div class="form-group advanced-filter-elements">
		                        <?php
									$sql_gender = "SELECT * FROM gender WHERE status='1'";
									$stmt = $link->prepare($sql_gender);
									$stmt->execute();
									$result=$stmt->fetchAll();

									foreach ($result as $row) 
									{
										$gender_id = $row['id'];
										$gender_name=$row['name'];
										if($gender_id=='1')
										{
											$gender_display="Groom";
										}
										else
										if($gender_id=='2')
										{
											$gender_display="Bride";
										}
										else
										$gender_display=$gender_name;
								
										if($gender==$gender_id)
										{
											echo "&nbsp;
											<input type='radio' name='gender' value='$gender_id' class='radio_1 gender' id='radio_$gender_id' checked>  
												<label for='radio_$gender_id' class='control-label'>&nbsp;Looking For $gender_display
												</label> ";
										}
										else
										{
											echo "&nbsp;<input type='radio' name='gender' value='$gender_id' class='radio_1 gender' id='radio_$gender_id'>  
											 <label for='radio_$gender_id' class='control-label'>&nbsp;Looking For $gender_display 
											 </label>";
										}
									}
								?>
		                    </div><!-- form group [search] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
		                    <div class="form-group advanced-filter-elements">
		                        <?php
									$sql_marital_status = "SELECT * FROM maritalstatus WHERE status='1'";
									$stmt = $link->prepare($sql_marital_status);
									$stmt->execute();
									$result=$stmt->fetchAll();

									foreach ($result as $row) 
									{
										$marital_status_id = $row['id'];
										$marital_status_name=$row['name'];

										if(in_array($marital_status_id,$marital_status))
										{
											echo "<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4'>
								            	<input type='checkbox' name='marital_status[]' class='radio_1 marital_status' value='$marital_status_id' id='marital_status_$marital_status_id' checked/> 
								            		<label for='marital_status_$marital_status_id' class='control-label'>&nbsp;".$marital_status_name."
								            		</label>
								            	</div>";
										}
										else
										{
											echo "<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4'>
								            	<input type='checkbox' name='marital_status[]' class='radio_1 marital_status' value='$marital_status_id' id='marital_status_$marital_status_id'/> 
								            		<label for='marital_status_$marital_status_id' class='control-label'>&nbsp;".$marital_status_name."
								            		</label>
								            	</div>";
										}
									}
								?>                                
		                    </div> <!-- form group [order by] --> 
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm from_date age_form_element" name="from_date">
									<option value="0">--Select Minimum Age--</option>
									<?php
										for($i=18;$i<61;$i++)
										{
											if($from_date==$i)
											{
												echo "<option value='".$i."' selected>".$i."</option>";
											}
											else
											{
												echo "<option value='".$i."'>".$i."</option>";
											}
										}
									?>
									<option value="61">60+</option>
								</select>&nbsp;&nbsp;&nbsp;
		                        <select class="form-control form control-sm to_date age_form_element" name="to_date">
									<option value="0">--Select Maximum Age--</option>
									<?php
										for($i=18;$i<61;$i++)
										{
											if($to_date==$i)
											{
												echo "<option value='".$i."' selected>".$i."</option>";
											}
											else
											{
												echo "<option value='".$i."'>".$i."</option>";
											}
										}
									?>
									<option value="61">60+</option>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>
	                    
	                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm religion" name="religion" style="width:100%;">
									<option value="0">-- Select Religion --</option>
									<?php
										$query  = "SELECT * FROM `religion` ORDER BY `id` ASC";
										$stmt   = $link->prepare($query);
										$stmt->execute();
										$result = $stmt->fetchAll();
										foreach( $result as $row )
										{
											$religion_name =  $row['name'];
											$religion_id = $row['id']; 

											if($religion_id==$religion)
											{
												echo "<option value='".$religion_id."' selected>".$religion_name."</option>";
											}
											else
											{
												echo "<option value='".$religion_id."'>".$religion_name."</option>";
											}
										}
					              	?>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm caste" name="caste" style="width:100%;">
									<option value="0">-- Select Caste --</option>
									<?php
					              		$query  = "SELECT * FROM `caste` ORDER BY `id` ASC";
										$stmt   = $link->prepare($query);
										$stmt->execute();
										$result = $stmt->fetchAll();
										foreach( $result as $row )
										{
											$caste_id = $row['id'];
											$caste_name = $row['name'];
											if($caste_id==$caste)
											{
												echo "<option value='".$caste_id."' selected>".$caste_name."</option>";
											}
											else
											{
												echo "<option value='".$caste_id."'>".$caste_name."</option>";
											}
										}
					              	?>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm mother_tongue" name="mother_tongue">
									<option value="0">-- Mother Tongue --</option>
									<?php
					              		$query  = "SELECT * FROM `mothertongue` ORDER BY `id` ASC";
										$stmt   = $link->prepare($query);
										$stmt->execute();
										$result = $stmt->fetchAll();
										foreach( $result as $row )
										{
											$mothertongue_name =  $row['name'];
											$mothertongue_id = $row['id'];
											if($mothertongue_id==$mother_tongue)
											{
												echo "<option value='".$mothertongue_id."' selected>".$mothertongue_name."</option>";
											}
											else
											{
												echo "<option value='".$mothertongue_id."'>".$mothertongue_name."</option>";
											}
										}
					              	?>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm education" name="education">
									<option value="0">--Education--</option>
									<?php
					              		$query  = "SELECT * FROM `educationname` ORDER BY `id` ASC";
										$stmt   = $link->prepare($query);
										$stmt->execute();
										$result = $stmt->fetchAll();
										foreach( $result as $row )
										{
											$education_name =  $row['name'];
											$education_id = $row['id']; 
											if($education == $education_id)
											{
												echo "<option value='".$education_id."' selected>".$education_name."</option>";
											}
											else
											{
												echo "<option value=".$education_id.">".$education_name."</option>";
											}
										}
					              	?>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm occupation" name="occupation">
									<option value="0">--Occupation--</option>
									<?php
					              		$query  = "SELECT * FROM `employment` ORDER BY `id` ASC";
										$stmt   = $link->prepare($query);
										$stmt->execute();
										$result = $stmt->fetchAll();
										foreach( $result as $row )
										{
											$employment_name =  $row['name'];
											$employment_id = $row['id']; 

											if($occupation==$employment_id)
											{
												echo "<option value=".$employment_id.">".$employment_name."</option>";
											}
											else
											{
												echo "<option value=".$employment_id.">".$employment_name."</option>";
											}
										}
					              	?>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm currency" name="currency" style="width:100%;">
									<option value="0">--Income Currency--</option>
									<?php
					              		$query  = "SELECT * FROM `currency` ORDER BY `id` ASC";
										$stmt   = $link->prepare($query);
										$stmt->execute();
										$result = $stmt->fetchAll();
										foreach( $result as $row )
										{
											$currency_name =  $row['currency']." [".$row['code']."]";
											$currency_id = $row['id']; 

											if($currency==$currency_id)
											{
												echo "<option value='".$currency_id."' selected>".$currency_name."</option>";
											}
											else
											{
												echo "<option value='".$currency_id."'>".$currency_name."</option>";
											}
										}
					              	?>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm monthly_income_from" name="monthly_income_from">
									<option value="0">--Minimum Income--</option>
									<option value="1000">1000</option>
									<option value="2000">2000</option>
									<option value="3000">3000</option>
									<option value="4000">4000</option>
									<option value="5000">5000</option>
									<option value="6000">6000</option>
									<option value="7000">7000</option>
									<option value="8000">8000</option>
									<option value="9000">9000</option>
									<option value="10000">10000</option>
									<option value="20000">20000</option>
									<option value="30000">30000</option>
									<option value="40000">40000</option>
									<option value="50000">50000</option>
									<option value="60000">60000</option>
									<option value="70000">70000</option>
									<option value="80000">80000</option>
									<option value="90000">90000</option>
									<option value="100000">100000</option>
									<option value="200000">200000</option>
									<option value="300000">300000</option>
									<option value="400000">400000</option>
									<option value="500000">500000</option>
									<option value="600000">600000</option>
									<option value="700000">700000</option>
									<option value="800000">800000</option>
									<option value="900000">900000</option>
									<option value="1000000">1000000</option>
									<option value="1000001">Above 1000000</option>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm monthly_income_to" name="monthly_income_to">
									<option value="0">--Maximum Income--</option>
									<option value="1000">1000</option>
									<option value="2000">2000</option>
									<option value="3000">3000</option>
									<option value="4000">4000</option>
									<option value="5000">5000</option>
									<option value="6000">6000</option>
									<option value="7000">7000</option>
									<option value="8000">8000</option>
									<option value="9000">9000</option>
									<option value="10000">10000</option>
									<option value="20000">20000</option>
									<option value="30000">30000</option>
									<option value="40000">40000</option>
									<option value="50000">50000</option>
									<option value="60000">60000</option>
									<option value="70000">70000</option>
									<option value="80000">80000</option>
									<option value="90000">90000</option>
									<option value="100000">100000</option>
									<option value="200000">200000</option>
									<option value="300000">300000</option>
									<option value="400000">400000</option>
									<option value="500000">500000</option>
									<option value="600000">600000</option>
									<option value="700000">700000</option>
									<option value="800000">800000</option>
									<option value="900000">900000</option>
									<option value="1000000">1000000</option>
									<option value="1000001">Above 1000000</option>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm country" name="country" style="width:100%;">
									<option value="0">--Country--</option>
									<?php
					              		$query  = "SELECT * FROM `countries` ORDER BY `name` ASC";
										$stmt   = $link->prepare($query);
										$stmt->execute();
										$result = $stmt->fetchAll();
										foreach( $result as $row )
										{
											$country_name =  $row['name'];
											$country_id = $row['id']; 

											if($country == $country_id)
											{
												echo "<option value='".$country_id."' selected>".$country_name."</option>";
											}
											else
											{
												echo "<option value='".$country_id."'>".$country_name."</option>";
											}
										}
					              	?>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm state" name="state" style="width:100%;">
									<option value="0">-- Select State --</option>
									<?php
										$caste_sql  = "SELECT * FROM `states` WHERE `country_id`='$country' ORDER BY `name` ASC";
						                $stmt   = $link->prepare($caste_sql);
						                $stmt->execute();
						                $states_userRow = $stmt->fetchAll();
						                
						                foreach( $states_userRow as $row )
										{
											$state_name =  $row['name'];
											$state_id = $row['id']; 

											if($state_id==$state)
											{
												echo "<option value='".$state_id."' selected>".$state_name."</option>";
											}
											else
											{
												echo "<option value='".$state_id."'>".$state_name."</option>";
											}
										}
									?>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

		                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		                    <div class="form-group advanced-filter-elements">
		                    	<select class="form-control form control-sm city" name="city" style="width:100%;">
									<option value="0">-- Select City --</option>
									<?php
										if($state!='0')
										{
											$caste_sql  = "SELECT * FROM `cities` WHERE `state_id`='$state' ORDER BY `name` ASC";
							                $stmt   = $link->prepare($caste_sql);
							                $stmt->execute();
							                $city_userRow = $stmt->fetchAll();
							                
							                foreach( $city_userRow as $row )
											{
												$city_name =  $row['name'];
												$city_id = $row['id']; 
												if($city==$city_id)
												{
													echo "<option value='".$city_id."' selected>".$city_name."</option>";
												}
												else
												{
													echo "<option value='".$city_id."'>".$city_name."</option>";
												}
											}
										}
						                
						            ?>
								</select>
		                    </div> <!-- form group [rows] -->
		                </div>

	                    <div class="form-group text-center">    
	                        <button type="submit" class="btn btn-default filter-col website-button search-filter">
	                            <i class="fa fa-search"></i> Search
	                        </button>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>    
	    
	</div>
	
	<!-- start: page -->
	
	<div class="filter-result">
		<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
			<div class="text-right">
				<button type="button" class="btn btn-default filter-open website-button" id="filter-open" data-toggle="collapse" data-target="#filter-panel">
			        <i class="fa fa-filter"></i> Filter
			   	</button>
			</div>
			<div class="result_data adv-search-result">

			</div>
			<!--    Result from ajax query/advanced_search_result_helper.php   -->
		</div>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
			<?php
        		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
            	if($display_skyscrapper_ad=='1')
            	{
            		echo "<div class='skyscrapper-ad'>";
              			echo $skyscrapper_ad = getRandomSkyScrapperAdData();
              		echo "</div>";
            	}
          	?>
		</div>
	</div>

		<!-- end: page -->
	
	<div class="row">
		<br/>
	</div>
</div>

</section>
</body>
<?php
	include("templates/footer.php");
?>
</html>
<script>
	$(document).ready(function(){

		$("#filter-open").click(function(e) {
			$('#filter-close').show();
			$(this).hide();
		});

		// HIDE BUTTON:
		$("#filter-close").click(function(e) {
			$('#filter-open').show();
			$(this).hide();
		});

		$('.squarepopup-ad').hide();
		var dataTable_Member_list = $('.tbl-profile').DataTable({
			"ordering": false,
			"bInfo" : false
		});

		var gender = $("input[name='gender']:checked").val();
		var marital_status = [];
		$('input[type=checkbox]:checked').each(function() {
			marital_status .push(this.value);
		});

		var from_date = $('.from_date').val();
		var to_date = $('.to_date').val();
		var religion = $('.religion').val();
		var caste = $('.caste').val();
		var mother_tongue = $('.mother_tongue').val();
		var education = $('.education').val();
		var occupation = $('.occupation').val();
		var currency = $('.currency').val();
		var monthly_income_from = $('.monthly_income_from').val();
		var monthly_income_to = $('.monthly_income_to').val();
		var country = $('.country').val();
		var state = $('.state').val();
		var city = $('.city').val();
		var task = "Get_Advanced_Search_Result";
		
		var data = 'gender='+gender+'&marital_status='+marital_status+'&from_date='+from_date+'&to_date='+to_date+'&religion='+religion+'&caste='+caste+'&mother_tongue='+mother_tongue+'&education='+education+'&occupation='+occupation+'&currency='+currency+'&monthly_income_from='+monthly_income_from+'&monthly_income_to='+monthly_income_to+'&country='+country+'&state='+state+'&city='+city+'&task='+task;
		//alert(data);return false;
		$.ajax({
			type:'post',
        	data:data,
        	url:'query/advanced_search_result_helper.php',
        	success:function(res)
        	{
        		//alert(res);return false;
        		$('.result_data').html(res);
        	}
        });

		var filter_data = function() {
			var gender = $("input[name='gender']:checked").val();
		    var marital_status = [];
			$('input[type=checkbox]:checked').each(function() {
				marital_status .push(this.value);
			});

			var from_date = $('.from_date').val();
			var to_date = $('.to_date').val();
			var religion = $('.religion').val();
			var caste = $('.caste').val();
			var mother_tongue = $('.mother_tongue').val();
			var education = $('.education').val();
			var occupation = $('.occupation').val();
			var currency = $('.currency').val();
			var monthly_income_from = $('.monthly_income_from').val();
			var monthly_income_to = $('.monthly_income_to_filtered').val();
			var country = $('.country').val();
			var state = $('.state').val();
			var city = $('.city').val();
		    return {
		    			gender:gender,
		    			marital_status:marital_status,
		    			from_date:from_date,
		    			to_date:to_date,
		    			religion:religion,
		    			caste:caste,
		    			mother_tongue:mother_tongue,
		    			education:education,
		    			occupation:occupation,
		    			currency:currency,
		    			monthly_income_from:monthly_income_from,
		    			monthly_income_to:monthly_income_to,
		    			country:country,
		    			state:state,
		    			city:city
		    	}
		};


		$('.country').change(function(){
			var country_id = $(this).val();
			var task = "Fetch_state_data";  

			$.ajax({
				type:'post',
            	data:'country_id='+country_id+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.state').html(res);
            	}
			});
		});

		$('.state').change(function(){
			var state_id = $(this).val();
			var task = "Fetch_city_data";  
			$.ajax({
				type:'post',
            	data:'state_id='+state_id+'&task='+task,
            	url:'modules/profile/fetch-info-helper.php',
            	success:function(res)
            	{
            		$('.city').html(res);
            	}
			});
		});

		$('.btn_profile_id').click(function(){
			var profile_id = $('.profile_id').val();

			if(profile_id=='' || profile_id==null)
        	{
        		$('.status_profile_id').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-left: 10px;margin-right: -20px;'><span class='fa fa-exclamation-circle'></span>Please enter profile id.</div>");
				return false;
        	}

        	window.location.assign('Profile-'+profile_id+'.html');
        	return false;
		});
	});

</script>