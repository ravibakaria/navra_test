<?php
	include("templates/header.php");
	if(!isset($_SESSION['logged_in']) && (@$_SESSION['client_user']=='' || @$_SESSION['client_user']==null))
	{
		echo "<script>window.location.assign('../login.php');</script>";
		exit;
	}
?>
<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>
	<!-- Profile Slider CSS -->
	<!--link rel="stylesheet" href="../assets/stylesheets/message.css" /-->
	<link rel="stylesheet" href="../css/chat_message.css" />
<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
<body style="background-color:#fff;">
	<?php
		$search_user_id = $_GET['id'];
		$user_first_name = getUserFirstName($search_user_id);
		$user_UniqueCode = getUserUniqueCode($search_user_id);
		$user_profile_pic = getUserProfilePic($search_user_id);
		if($user_profile_pic!='' || $user_profile_pic!=null)
		{
			$user_profile_pic = 'uploads/'.$search_user_id.'/profile/'.$user_profile_pic;
		}
		else
		{
			$user_profile_pic = $WebSiteBasePath.'/images/no_profile_pic.png';
		}
		
		$pro_url = $WebSiteBasePath.'/users/Profile-'. $user_UniqueCode.".html";
	?>
	<section role="main" class="content-body main-section-start">

	<!-- start: page -->
		<div class='row start_section_my_photos'>
			<div class='advanced-search-result-header'>
				<h1>My Messages</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
				<div class="row">
					<!--button class="btn btn-info show_message">Show</button-->
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 message_chat_div">
						<?php
							if (!is_numeric($search_user_id))
							{
								echo "ERROR : Invalid parameter value";
								exit;
							}
						?>
						<input type="hidden" class="user_id" value="<?php echo $user_id;?>">
					   	<input type="hidden" class="search_user_id" value="<?php echo $search_user_id;?>">

						<div class="chat_area">

						</div><!--chat_area-->

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 message_write">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							   	<textarea class="form-control message" placeholder="Type your message (Maximum 500 characters)"></textarea>
							   	<input type="hidden" class="userFrom" value="<?php echo $user_id;?>">
							   	<input type="hidden" class="userTo" value="<?php echo $search_user_id;?>">
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<br/>
							<center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:60px; height:60px; display:none;'/></center>
						</div>
						
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
								   	<button class="btn btn-success send_message website-button">Send</button>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
									<?php 
										$block_status = unblockbtn($user_id,$search_user_id);
										if($block_status>0)
										{
											echo "<button class='btn btn-danger btn-block-unblock' data-toggle='modal' data-target='#un_blockModal'>UnBlock</button>";
										}
										else
										{
											echo "<button class='btn btn-danger btn-block-unblock' data-toggle='modal' data-target='#blockModal'>Block</button>";
										}
									?>
								   	
								</div>
								<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 result_status">
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<br/>
						</div>
						
						
					</div>

					<!-- Modal Block User-->
					<div id="blockModal" class="modal fade model_main" role="dialog" >
						<div class="modal-dialog">

						<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header" style="background-color:#0d47a1;color:white;">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<center><strong>Block User</strong></center>
								</div>
								<div class="modal-body">
								   	<textarea class="form-control block_message" placeholder="Type your comment for blocking user (Maximum 500 characters)" rows="5"></textarea>
								   	<input type="hidden" class="block_userFrom" value="<?php echo $user_id;?>">
								   	<input type="hidden" class="block_userTo" value="<?php echo $search_user_id;?>">
								   	<br/>
								   	<div class="block_status"></div>
								   	<center><img src="../images/loader/loader.gif" class='img-responsive loading_img_block' id='loading_img_block' style='width:50px; height:50px;display:none;'/></center>
								</div>
								<div class="modal-footer footer_model">
									<center><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								
									<button type="button" class="btn btn-danger btn_block">Block</button></center>
								</div>
							</div>

						</div>
					</div>
					
					<!-- Modal Unblock User-->
					<div id="un_blockModal" class="modal fade model_main" role="dialog" >
						<div class="modal-dialog">

						<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header" style="background-color:#0d47a1;color:white;">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<center><h4 class="modal-title">Un - Block User</h4></center>
								</div>
								<div class="modal-body">
								   	<textarea class="form-control un_block_message" placeholder="Type your comment for unblocking user (Maximum 500 characters)" rows="5"></textarea>
								   	<input type="hidden" class="un_block_userFrom" value="<?php echo $user_id;?>">
								   	<input type="hidden" class="un_block_userTo" value="<?php echo $search_user_id;?>">
								   	<br/>
								   	<div class="un_block_status"></div>
								   	<center><img src="../images/loader/loader.gif" class='img-responsive loading_img_unblock' id='loading_img_unblock' style='width:50px; height:50px;display:none;'/></center>
								</div>
								<div class="modal-footer footer_model">
									<center><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								
									<button type="button" class="btn btn-success btn_un_block">Un-Block</button></center>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<div class='skyscrapper-ad'>
		          	<?php
	            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
		            	if($display_skyscrapper_ad=='1')
		            	{
		              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
		            	}
		          	?>
		        </div>
			</div>
		</div>
		<div class='leaderboard-ad'>
          	<?php
            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
            	if($display_leaderboard_ad=='1')
            	{
              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
            	}
          	?>
        </div>
	<!-- end: page -->
	
	</section>
</div>
</body>
<?php
	include("templates/footer.php");
?>
</html>

<script>
	var tid = setInterval(myMessage, 30000);
	function myMessage() {
		var user_id = $('.user_id').val();
		var search_user_id = $('.search_user_id').val();
		var task = "Get_Chat_Message";
		//alert('1');return false;
		var data = 'user_id='+user_id+'&search_user_id='+search_user_id+'&task='+task;

		$.ajax({
			type:'post',
        	data:data,
        	url:'query/get_messages.php',
        	success:function(res)
        	{
        		$('.chat_area').html(res);
        		//$('#message')[0].focus();
        	}
        });
	}
</script>

<script>
	$(document).ready(function(){

		var user_id = $('.user_id').val();
		var search_user_id = $('.search_user_id').val();
		var task = "Get_Chat_Message";
		//alert('1');return false;
		var data = 'user_id='+user_id+'&search_user_id='+search_user_id+'&task='+task;

		$.ajax({
			type:'post',
        	data:data,
        	url:'query/get_messages.php',
        	success:function(res)
        	{
        		$('.chat_area').html(res);
        		$('#message')[0].focus();
        	}
        });

		/* removing religious info error_class  */
        $('.message,.block_message,.un_block_message').click(function(){
            $(this).removeClass("danger_error");
            $('.result_status').html("");
            $('.block_status').html("");
            $('.un_block_status').html("");
        });

		$('.send_message').click(function(){
			var userFrom = $('.userFrom').val();
			var userTo = $('.userTo').val();
			var message = $('.message').val();

			var task = "Send_Chat_Message";

			if(message=='' || message==null)
			{
				$('.result_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter message & then click on send button.</div>");
				$(".message").addClass("danger_error");
				return false;
			}

			$('.send_message').attr('disabled',true);
			$('.loading_img').show();
			
			var data = 'userFrom='+userFrom+'&userTo='+userTo+'&message='+message+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/message_helper.php',
            	success:function(res)
            	{
            		$('.loading_img').hide();
            		
            		if(res=='success')
            		{
            			$('.send_message').attr('disabled',true);
            			$('.result_status').html("<div class='alert alert-success success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Message Sent.</div>");
            			$('.success_status').fadeTo(1000, 500).slideUp(500, function(){
            				window.location.assign("message.php?id="+userTo);
                        });
            		}
            		else
            		if(res=='invalid_user')
            		{
            			$('.result_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Invalid!</strong> Unauthorized user access.</div>");
						return false;
            		}
            		else
            		if(res=='blocked_user')
            		{
            			$('.result_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Oops!</strong> Profile not available.</div>");
						return false;
            		}
            		else
            		if(res=='you_blocked_user')
            		{
            			$('.result_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Oops!</strong> You blocked this user.<br> You cannot send or receive messages to this user.</div>");
						return false;
            		}
            		else
            		{
            			$('.result_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}	
            	}
            });
		});

		/*  Block User start */
		$('.btn_block').click(function(){
			var block_userFrom = $('.block_userFrom').val();
			var block_userTo = $('.block_userTo').val();
			var block_message = $('.block_message').val();

			var task = "Block_User";

			if(block_message=='' || block_message==null)
			{
				$('.block_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter message & then submit.</div>");
				$(".block_message").addClass("danger_error");
				return false;
			}

			var data = 'block_userFrom='+block_userFrom+'&block_userTo='+block_userTo+'&block_message='+block_message+'&task='+task;
			
			$('.block_status').html("");
            $('.loading_img_block').show();
            
			$.ajax({
				type:'post',
            	data:data,
            	url:'query/message_helper.php',
            	success:function(res)
            	{
            	    $('.loading_img_block').hide();
            	    if(res=='success')
            		{
            			$('.btn_block').attr('disabled',true);
            			$('.block_status').html("<div class='alert alert-success block_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Blocked Successfully.</div>");
            			$('.block_success_status').fadeTo(1000, 500).slideUp(500, function(){
            				$('#blockModal').modal('hide');
            				location.reload();
                        });
            		}
            		else
            		if(res=='invalid_user')
            		{
            			$('.block_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Invalid!</strong> Unauthorized user access.</div>");
						return false;
            		}
            		else
            		if(res=='already_block')
            		{
            			$('.block_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Oops!</strong> User already blocked.</div>");
						return false;
            		}
            		else
            		{
            			$('.block_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}	
            	}
            });
		});
		/*  Block User End   */

		/*   Unblock User Start    */
		$('.btn_un_block').click(function(){
			var un_block_userFrom = $('.un_block_userFrom').val();
			var un_block_userTo = $('.un_block_userTo').val();
			var un_block_message = $('.un_block_message').val();

			var task = "Un_Block_User";

			if(un_block_message=='' || un_block_message==null)
			{
				$('.un_block_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Empty!</strong> Enter message & then submit.</div>");
				$(".un_block_message").addClass("danger_error");
				return false;
			}

			var data = 'un_block_userFrom='+un_block_userFrom+'&un_block_userTo='+un_block_userTo+'&un_block_message='+un_block_message+'&task='+task;
			
            $('.un_block_status').html("");
            $('.loading_img_unblock').show();
            
			$.ajax({
				type:'post',
            	data:data,
            	url:'query/message_helper.php',
            	success:function(res)
            	{
            	    $('.loading_img_unblock').hide();
            		if(res=='success')
            		{
            			$('.un_btn_block').attr('disabled',true);
            			$('.un_block_status').html("<div class='alert alert-success un_block_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span><strong> Sucess! </strong> Un-Blocked Successfully.</div>");
            			$('.un_block_success_status').fadeTo(1000, 500).slideUp(500, function(){
            				$('#un_blockModal').modal('hide');
            				location.reload();
                        });
            		}
            		else
            		if(res=='invalid_user')
            		{
            			$('.un_block_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Invalid!</strong> Unauthorized user access.</div>");
						return false;
            		}
            		else
            		{
            			$('.un_block_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}	
            	}
            });
		});
		/*   Unblock User End    */

	});
</script>