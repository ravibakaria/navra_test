<?php
	include("templates/header.php");
?>
	<title>
		<?php
			echo "Advanced Matrimonial Profile Search - ".getWebsiteTitle();
		?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?> Advanced Matrimonial Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
<body style="background-color:#fff;">

	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Profile Search</h2>
		
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="index.php">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Profile Search</span></li>
				</ol>
		
				<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
			</div>
		</header>

		<div class="row start_section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php
				$search_code = $_GET['data'];
				$search_code_val = explode('-',$search_code);
				//print_r($search_code_val);
				$gender = explode('=',$search_code_val[1])[1];
				$marital_status = explode('=',$search_code_val[2])[1];
				$from_date = explode('=',$search_code_val[3])[1];
				$to_date = explode('=',$search_code_val[4])[1];
				$religion = explode('=',$search_code_val[5])[1];
				$caste = explode('=',$search_code_val[6])[1];
				$mother_tongue = explode('=',$search_code_val[7])[1];
				$education = explode('=',$search_code_val[8])[1];
				$occupation = explode('=',$search_code_val[9])[1];
				$currency = explode('=',$search_code_val[10])[1];
				$monthly_income_from = explode('=',$search_code_val[11])[1];
				$monthly_income_to = explode('=',$search_code_val[12])[1];
				$country = explode('=',$search_code_val[13])[1];
				$state = explode('=',$search_code_val[14])[1];
				$city = explode('=',$search_code_val[15])[1];
				//echo $gender.'='.$from_date.'='.$to_date.'='.$religion.'='.$caste.'='.$mother_tongue.'='.$education.'='.$occupation.'='.$currency.'='.$monthly_income_from.'='.$monthly_income_to.'='.$country.'='.$state.'='.$city;

				$where = $sqlTot = $sqlRec =  "";
				$sql = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM clients as A 
	        	LEFT OUTER JOIN
				profilebasic AS B ON A.id = B.userid
				LEFT OUTER JOIN
				address AS C ON A.id = C.userid
				LEFT OUTER JOIN
				profilereligion AS D ON A.id = D.userid
				LEFT OUTER JOIN
				eduocc AS E ON A.id = E.userid
				LEFT OUTER JOIN
				profilepic AS F ON A.id = F.userid
				LEFT OUTER JOIN
				family AS G ON A.id = G.userid
				WHERE A.gender='$gender' AND A.status=1";

				/******  Profile Client Start *****/
				if($from_date=='0' || !isset($_POST['$from_date']))      // Age form
				{
					$where .="";
				}
				else
				{
					$age_from = date('Y-m-d', strtotime('-'.$from_date.' years'));
					$where .=" AND A.dob >='$age_from'";
				}

				if($to_date=='0' || !isset($_POST['$to_date']))      // Age to
				{
					$where .="";
				}
				else
				{
					$age_from_chk = strtotime($age_from);
						$age_to = date('Y-m-d',strtotime("+".$to_date." years",$age_from_chk));
					$where .=" AND A.dob <='$age_to'";
				}
				/******  Profile Client End *****/

				/******  Profile Basic Start *****/
				if(empty($marital_status) )        // Marital status
				{
					$where .="";
				}
				else
				{
					$where .=" AND B.marital_status IN($marital_status)";
				}
				/******  Profile Basic End *****/

				/******  Profile location Start *****/
				if($city=='0')    // City
				{
					$where .="";
				}
				else
				{
					$where .=" AND C.city ='$city'";
				}
				
				if($state=='0')     //state
				{
					$where .="";
				}
				else
				{
					$where .=" AND C.state ='$state'";
				}

				if($country=='0')     //country
				{
					$where .="";
				}
				else
				{
					$where .=" AND C.country ='$country'";
				}
				/******  Profile location End *****/


				/******  Profile Education Start *****/
				if($education=='0')     //country
				{
					$where .="";
				}
				else
				{
					$where .=" AND E.education ='$education'";
				}

				if($occupation=='0')     //occupation
				{
					$where .="";
				}
				else
				{
					$where .=" AND E.occupation ='$occupation'";
				}

				if($currency=='0')     //currency
				{
					$where .="";
				}
				else
				{
					$where .=" AND E.currency ='$currency'";
				}

				if($monthly_income_from=='' || $monthly_income_from==null )     //monthly_income_from
				{
					$where .="";
				}
				else
				{
					$where .=" AND E.monthly_income_from >='$monthly_income_from'";
				}

				if($monthly_income_to=='' || $monthly_income_to==null)     //monthly_income_to
				{
					$where .="";
				}
				else
				{
					$where .=" AND E.monthly_income_to <='$monthly_income_to'";
				}
				/******  Profile Education  *****/


				/******  Profile Relegious Start *****/
				if($religion=='0')     //religion
				{
					$where .="";
				}
				else
				{
					$where .=" AND D.country ='$religion'";
				}

				if($mother_tongue=='0')     //mother_tongue
				{
					$where .="";
				}
				else
				{
					$where .=" AND D.mother_tongue ='$mother_tongue'";
				}

				if($caste=='0')     //caste
				{
					$where .="";
				}
				else
				{
					$where .=" AND D.caste ='$caste'";
				}
				/******  Profile Relegious End *****/

				$where .=" ORDER BY rand()";
								
				$sqlRec .=  $sql.$where;

				$stmt_search   = $link->prepare($sqlRec);
				$stmt_search->execute();
				$sqlTot = $stmt_search->rowCount();
				$result_search = $stmt_search->fetchAll();

				if($sqlTot>0)
				{	
					echo "<div>";
						echo "<h3 class='heading_page'>Profiles which matches your search criteria.</h3>";
					echo "</div>";
					$i=0;
					echo "<div class='row row-eq-height'>";
					foreach( $result_search as $row )
					{

						$city = $row['city'];
						$state = $row['state'];
						$country = $row['country'];
						$edu = $row['education'];
						$occ = $row['occupation'];
						$religion = $row['religion'];
						$caste = $row['caste'];
						$mother_tongue = $row['mother_tongue'];
						$u_code = $row['unique_code'];
						$dobx = $row['dob'];
						$profileid = $row['id'];
						$gender = $row['gender'];
						$height = $row['height'];
						$salary = $row['income'];
						$fam_type = $row['fam_type'];
						
						$search_data = 'Profile-'.$u_code;
						$pro_url = $WebSiteBasePath.'/users/'. $search_data.".html";
						$search_user_fname=$row['firstname'];
						$search_user_name = $row['firstname']." ".$row['lastname'];
						$prefer_user_id =$row['idx'];
						$photo =$row['photo'];
						$today = date('Y-m-d');
						$diff = date_diff(date_create($dobx), date_create($today));
						$age_search_user = $diff->format('%y');
						//$user_occ = user_occ($occ);
						if($i==0 || ($i>1 && $i%2==0))
						{
							echo "<div class='row cover_rows' >";
						}
						?>
						<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6 cover_cols' >
							<div class="profile_div">
								<a href="<?php echo $pro_url; ?>" style='text-decoration: none;'>
								
									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<center>
										<div class="profile-sidebar">
											<div class="profile-usertitle">
												<div class="profile-usertitle-name">
													<?php
														echo "<strong>";
														echo ucwords($search_user_fname)." From ".getUserCityName($city);
														echo "</strong>";
													?>
												</div>
											</div>
											<!-- SIDEBAR USERPIC -->
											<div class="profile-userpic">
												<?php
													if($photo=='' || $photo==null)
													{
														echo "<img src='../images/no_profile_pic.png'  class='profile_img' alt='$search_user_fname' title='$search_user_fname'/>";
													}
													else
													{
														echo "<img src='uploads/$prefer_user_id/profile/$photo'  class='profile_img' alt='$search_user_fname' title='$search_user_fname'/>";
													}
												?>
											</div>
											<!-- END SIDEBAR USERPIC -->
											<!-- SIDEBAR USER TITLE -->
											
										</div>
										</center>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 profile_info_data">
										</center>
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile">
												<strong class="">Age: </strong> 
											</div>
											<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data">
											<?php 
												echo "&nbsp";
													
												echo $age_search_user." Years.";
											?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile">
												<strong class="">Height: </strong> 
											</div>
											<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data">
											<?php 
												echo "&nbsp";
													
												if($height!='' || $height!=null)
												{
													$search_user_height_meter = round(($height)/100,2);
								                	$search_user_height_foot = round(($height)/30.48,2);
								                	echo $height.'-cms/ '.$search_user_height_foot.'-fts/ '.$search_user_height_meter.'-mts';
												}
												else
												{
													echo "-NA-";
												}
											?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile">
												<strong>Religion: </strong> 
											</div>
											<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data">
											<?php 
												echo "&nbsp";
								                	
												if($religion!='' || $religion!=null)
												{
													echo getUserReligionName($religion);
												}
												else
												{
													echo "-NA-";
												}
											?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile">
												<strong>Caste: </strong> 
											</div>
											<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data">
											<?php 
												echo "&nbsp";
								                	
												if($caste!='' || $caste!=null)
												{
													echo getUserCastName($caste);
												}
												else
												{
													echo "-NA-";
												}
											?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile">
												<strong>Education: </strong> 
											</div>
											<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data">
											<?php 
												echo "&nbsp";
								                	
												if($edu!='' || $edu!=null)
												{
													echo getUserEducationName($edu);
												}
												else
												{
													echo "-NA-";
												}
											?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile">
												<strong>Occupation: </strong> 
											</div>
											<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data">
											<?php 
												echo "&nbsp";
								                	
												if($occ!='' || $occ!=null)
												{
													echo user_occ($occ);
												}
												else
												{
													echo "-NA-";
												}
											?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile">
												<strong>Salary: </strong> 
											</div>
											<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data">
											<?php 
												echo "&nbsp";
								                	
												if($salary!='' || $salary!=null)
												{
													echo $salary." Monthly";
												}
												else
												{
													echo "-NA-";
												}
											?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile">
												<strong>Mother Tongue: </strong> 
											</div>
											<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data">
											<?php 
												echo "&nbsp";
								                	
												if($mother_tongue!='' || $mother_tongue!=null)
												{
													echo getUserMotherTongueName($mother_tongue);
												}
												else
												{
													echo "-NA-";
												}
											?>
											</div>
										</div>
										</center>
									</div>

								</a>
							</div>
						</div>
						<?php
						$i=$i+1;
						
						if($i%2==0)
						{
							echo "</div>";
						}
					}
					echo "</div>";
				}
				else
				{
					echo "<h3 class='error'>Oops! No profile found with your preference.</h3>";
				}

			?>
			</div>
		</div>

	</section>

</body>
<?php
	include("templates/footer.php");
?>
</html>