<?php
	if(!isset($_SESSION))
	{
		session_start();
	}

	if(!isset($_SESSION['logged_in']) || ($_SESSION['client_user']=='' || $_SESSION['client_user']==null))
	{
		header('Location:../login.php');
		exit;
	}
	
	include("templates/header.php");

?>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>
<body style="background-color:#fff;">

	<section role="main" class="content-body main-section-start">

		<!-- start: page -->
		<div class='row start_section_my_photos'>
			<div class='advanced-search-result-header'>
				<h1>Shortlisted Profiles.</h1>
			</div>
		</div>
		<?php
			if(isset($_GET['shortlist']))
			{
				@$shortlist = $_GET['shortlist'];
			}
			else
			{
				@$shortlist = null;
			}
		?>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 my-preference-result">
				<div class="row">
					<?php
						if($shortlist==null || $shortlist=='by_me')
						{
							$sql_search_shortlisted = "SELECT * FROM shortlisted WHERE userid='$user_id' AND short_id IN(SELECT id FROM clients C WHERE C.status='1')";
						}
						else
						if($shortlist=='by_others')
						{
							$sql_search_shortlisted = "SELECT * FROM shortlisted WHERE short_id='$user_id' AND short_id IN(SELECT id FROM clients C WHERE C.status='1')";
						}
						else
						{
							$sql_search_shortlisted = "SELECT * FROM shortlisted WHERE userid='$user_id' AND short_id IN(SELECT id FROM clients C WHERE C.status='1')";
						}

						$stmt   = $link->prepare($sql_search_shortlisted);
				        $stmt->execute();
				        $sqlTot = $stmt->rowCount();
				        $result = $stmt->fetchAll();
				        if($sqlTot>0)
				        {
				        	echo "<div class='matched-profile-heading'>
									<h2>$sqlTot profiles shortlisted.</h2>
								</div>";
				        	$i=0;

							foreach( $result as $row )
							{
								$short_id = $row['short_id'];
								$sql_shortlist_info = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
						        	clients as A 
						        	LEFT OUTER JOIN
									profilebasic AS B ON A.id = B.userid
									LEFT OUTER JOIN
									address AS C ON A.id = C.userid
									LEFT OUTER JOIN
									profilereligion AS D ON A.id = D.userid
									LEFT OUTER JOIN
									eduocc AS E ON A.id = E.userid
									LEFT OUTER JOIN
									profilepic AS F ON A.id = F.userid
									LEFT OUTER JOIN
									family AS G ON A.id = G.userid
									WHERE A.id='$short_id'";

								$stmt_search   = $link->prepare($sql_shortlist_info);
								$stmt_search->execute();
								$result_search = $stmt_search->fetch();

								$city = $result_search['city'];
								$city_Name = getUserCityName($city);
								$state = $result_search['state'];
								$state_Name = getUserStateName($state);
								$country = $result_search['country'];
								$country_Name = getUserCountryName($country);
								$edu = $result_search['education'];
								$occ = $result_search['occupation'];
								$religion = $result_search['religion'];
								$caste = $result_search['caste'];
								$mother_tongue = $result_search['mother_tongue'];
								$u_code = $result_search['unique_code'];
								$dobx = $result_search['dob'];
								$dispaly_user_dob = date('d-M-Y',strtotime($dobx));
								$profileid = $result_search['id'];
								$gender = $result_search['gender'];
								$height = $result_search['height'];
								$weight = $result_search['weight'];
								if($gender=='1')
						        {
						        	$meta_gender = "Male";
						        }
						        else
						        if($gender=='2')
						        {
						        	$meta_gender = "Female";
						        }
						        else
						        if($gender=='3')
						        {
						        	$meta_gender = "T-Gender";
						        }
						        $occupation = $result_search['occupation'];
								$occupation_name = getUserEmploymentName($occupation);
								$salary = $result_search['income'];
								$currency = getUserIncomeCurrencyCode($result_search['income_currency']);
								$fam_type = $result_search['fam_type'];
								$fam_status = $result_search['fam_stat'];
								$fam_value = $result_search['fam_val'];

								$search_data = 'Profile-'.$u_code;
								$pro_url = $search_data.".html";
								$search_user_fname=$result_search['firstname'];
								$search_user_name = $result_search['firstname']." ".$result_search['lastname'];
								$prefer_user_id =$result_search['idx'];
								$photo =$result_search['photo'];
								$today = date('Y-m-d');
								$diff = date_diff(date_create($dobx), date_create($today));
								$age_search_user = $diff->format('%y');
								$user_occ = user_occ($occ);
								$site_url = getWebsiteBasePath();
								
								$fam_type_name = getUserFamilyTypeUser($prefer_user_id);
								$fam_status_name = getUserFamilyStatusUser($prefer_user_id);
								$fam_value_name = getUserFamilyValueUser($prefer_user_id);

								if($i==0 || ($i%3==0))
								{
									echo "<div class='row'>";
								}

							?>
								<a href="<?php echo $pro_url; ?>" style="text-decoration:none; color:#000000;">
									<div class="card-my-matches col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<span itemscope itemtype='http://schema.org/Person'>
											<meta itemprop='name' content="<?= $search_user_fname; ?>"/>
											<link itemprop='url' href="<?= $site_url.'/users/'.$pro_url; ?>;"/>
											<meta itemprop='description' content="Matrimonial Profile of <?php echo $search_user_fname; ?> From <?php echo $city_Name;?>, <?php echo $state_Name;?>, <?php echo $country_Name;?>. <?php echo $search_user_fname.'\'s '.$siteName;?>  Profile ID is <?php echo $u_code;?>. <?php echo $search_user_fname;?>'s Date Of Birth is <?php echo $dispaly_user_dob;?>. <?php echo $search_user_fname;?>'s mother tongue is <?php echo getUserMotherTongueName($mother_tongue);?>. <?php echo $search_user_fname;?> lives in a <?php echo $fam_type_name;?>. <?php echo $search_user_fname;?> belongs to <?php echo $fam_status_name;?> family. <?php echo $search_user_fname;?> follows <?php echo $fam_value_name;?> Family Values."/>
											<meta itemprop='address' content="<?= $city_Name.','.$state_Name.','.$country_Name; ?>"/>
											<meta itemprop='birthDate' content="<?= $dispaly_user_dob; ?>"/>
											<meta itemprop='gender' content="<?= $meta_gender; ?>"/>
											<meta itemprop='givenName' content="<?= $search_user_fname; ?>"/>
											<meta itemprop='height' content="<?= $height; ?>"/>
											<meta itemprop='homeLocation' content="<?= $city_Name; ?>"/>
											<meta itemprop='jobTitle' content="<?= $occupation_name; ?>"/>
											<meta itemprop='knowsLanguage' content="<?= getUserMotherTongueName($search_user_mother_tongue); ?>"/>
											<meta itemprop='nationality' content="<?= $country_Name; ?>"/>
											<meta itemprop='netWorth' content="<?= $salary; ?>"/>
											<meta itemprop='weight' content="<?= $weight; ?>"/>
										</span>

										<?php
											if($photo=='' || $photo==null)
											{
												echo "<img src='$site_url/images/no_profile_pic.png'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
											}
											else
											{
												echo "<img src='$site_url/users/uploads/$prefer_user_id/profile/$photo'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
											}
										?>
										<br><br>
										<h4>
											<b>
												<?php
													echo "<strong>";
													echo ucwords($search_user_fname);
													echo "</strong>";
												?>	
											</b>
										</h4>
										<p class="title">
											Profile ID: <?php echo $u_code; ?><br>
											From <?php echo substr(getUserCityName($city),0,11); ?>
										</p>

										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong class="">Age: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
													
												echo $age_search_user." Years.";
											?>	
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong class="">Height: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
													
												if($height!='' || $height!=null)
												{
													$search_user_height_meter = round(($height)/100,2);
								                	$search_user_height_foot = round(($height)/30.48,2);
								                	echo $height.'-cms/ '.$search_user_height_foot.'-fts/ '.$search_user_height_meter.'-mts';
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Religion: </strong>
												</p> 
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($religion!='' || $religion!=null)
												{
													echo getUserReligionName($religion);
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Caste: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($caste!='' || $caste!=null)
												{
													echo getUserCastName($caste);
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row" style="height:54px;">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Education: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($edu!='' || $edu!=null)
												{
													echo getUserEducationName($edu);
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row" style="height:54px;">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Occupation: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($occ!='' || $occ!=null)
												{
													echo user_occ($occ);
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Salary: </strong> 
												</p>
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($salary!='' || $salary!=null)
												{
													echo $currency." ".$salary." Monthly";
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
										<div class="row member-profile-info-row">
											<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
												<p align="justify">
													<strong>Mother Tongue: </strong>
												</p> 
											</div>
											<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
												<p align="justify">
											<?php 
												echo "&nbsp;";
								                	
												if($mother_tongue!='' || $mother_tongue!=null)
												{
													echo getUserMotherTongueName($mother_tongue);
												}
												else
												{
													echo "-NA-";
												}
											?>
												</p>
											</div>
										</div>
									</div>
								</a>
							
							
					<?php
								$i++;

								if($i%3==0)
								{
									echo "</div>";
								}

								
							}
						}
				    	else
				    	{
				    		echo "<div class='row'>
				        		<div class='advanced-search-result-header'>
				        			<h2>No record found.</h2>
								</div>
							</div>";
				    	}

					?>	
				</div>			
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
	          	<?php
            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
	            	if($display_skyscrapper_ad=='1')
	            	{
	            		echo "<div class='skyscrapper-ad'>";
	              			echo $skyscrapper_ad = getRandomSkyScrapperAdData();
	              		echo "</div>";
	            	}
	          	?>
			</div>
		</div>
	<!-- end: page -->
	</section>
</div>
</body>
<?php
	include("templates/footer.php");
?>
<script>
	$(document).ready(function(){
		var dataTable_Member_list = $('.tbl-profile').DataTable({
			"ordering": false,
			"bInfo" : false,
			"oLanguage": {
		      "sLengthMenu": "Showing _MENU_ rows",
		    }
		});
	});
</script>
</html>