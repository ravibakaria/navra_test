<?php
	if(isset($_SESSION['last_page_accessed']))
	{
		unset($_SESSION['last_page_accessed']);
	}
	include("templates/header.php");
	$loggedIn = CheckUserLoggedIn();
?>
	<title>
		<?php
			$search_code = $_GET['search'];
			$search_code_val = explode('-',$search_code);
			$search_type = $search_code_val[0];
			$search_value = $search_code_val[1];

			$sql_search_user_info = "SELECT * FROM clients WHERE unique_code='$search_value' AND status=1";
			
			$stmt   = $link->prepare($sql_search_user_info);
	        $stmt->execute();
	        $search_userRow = $stmt->fetch();
        	$search_user_id = $search_userRow['id'];
        	$search_user_first_name = getUserFirstName($search_user_id);
        	$search_user_last_name = $search_userRow['lastname'];
		    $search_user_lastName = substr($search_user_last_name,0,1);
		    $search_user_lastName = $search_user_lastName.'XXXXXXXXX';
		    $profile_link = getWebsiteBasePath()."/users/Profile-$search_value.html";
        	$search_user_dob = getUserDOB($search_user_id);
        	$dispaly_user_dob = date('d-M-Y',strtotime($search_user_dob));
        	$search_user_city = getUserCity($search_user_id);
        	$search_user_state = getUserState($search_user_id);
        	$search_user_country = getUserCountry($search_user_id);
        	$search_user_gender = getUserGender($search_user_id);
        	$search_user_mother_tongue = getUserMotherTongueName($search_user_id);
        	$search_user_family_type = getUserFamilyTypeUser($search_user_id);
        	$search_user_family_status = getUserFamilyStatusUser($search_user_id);
        	$search_user_family_value = getUserFamilyValueUser($search_user_id);
        	if($search_user_gender=='1')
	        {
	        	$display_gender = "His";
	        	$meta_gender = "Male";
	        }
	        else
	        if($search_user_gender=='2')
	        {
	        	$display_gender = "Her";
	        	$meta_gender = "Female";
	        }
	        else
	        if($search_user_gender=='3')
	        {
	        	$display_gender = "His/Her";
	        	$meta_gender = "T-Gender";
	        }

	        $WebsiteBasePath = getWebsiteBasePath();
        	$siteName = getWebsiteTitle();
			echo $search_user_first_name." From ".$search_user_city.", ".$search_user_state.", ".$search_user_country." - ".$siteName." Profile Id:".$search_value;
			$siteTagline = getWebSiteTagline();
			$sql_search_user_profile = "SELECT photo FROM profilepic WHERE userid='$search_user_id'";
			$stmt1   = $link->prepare($sql_search_user_profile);
	        $stmt1->execute();
	        $count_user_pic = $stmt1->rowCount();
	        if($count_user_pic>0)
	        {
	        	$search_user_profile = $stmt1->fetch();
		        $search_user_profile_pic_file_name = $search_user_profile['photo'];
		        $search_user_profile_pic = getWebsiteBasePath().'/users/uploads/'.$search_user_id.'/profile/'.$search_user_profile['photo'];
	        }
	        else
	        {
	        	$search_user_profile_pic = getWebsiteBasePath().'/images/no_profile_pic.png';
	        }

	        $sql_search_user_basic = "SELECT * FROM profilebasic WHERE userid='$search_user_id'";
			$stmt   = $link->prepare($sql_search_user_basic);
	        $stmt->execute();
	        $count_userbasic = $stmt->rowCount();
	        $search_userbasic = $stmt->fetch();
	        $search_user_height = $search_userbasic['height'];
	        $search_user_weight = $search_userbasic['weight'];
	        $search_user_eat_habbit = $search_userbasic['eat_habbit'];
	        $search_user_smoke_habbit = $search_userbasic['smoke_habbit'];
	        $search_user_body_type = $search_userbasic['body_type'];
	        $search_user_complexion = $search_userbasic['complexion'];
	        $search_user_marital_status = $search_userbasic['marital_status'];
	        $search_user_special_case = $search_userbasic['special_case'];
	        $search_user_drink_habbit = $search_userbasic['drink_habbit'];

	        $sql_search_user_education = "SELECT education,occupation,income,income_currency FROM eduocc WHERE userid='$search_user_id'";
			$stmt   = $link->prepare($sql_search_user_education);
	        $stmt->execute();
	        $count_user_education = $stmt->rowCount();
	        $search_usereducation = $stmt->fetch();
	        $search_user_edu_name = $search_usereducation['education'];
	        $search_user_occ_name = $search_usereducation['occupation'];
	        $search_user_income = $search_usereducation['income'];
	        $search_user_curr_name = $search_usereducation['income_currency'];

	        $search_user_occ_name_meta = getUserEmploymentName($search_user_occ_name);
		?>
	</title>

	<meta name="title" content="<?= $siteName.' - '.$siteTagline; ?>" />

	<meta name="description" content="Matrimonial Profile of <?php echo $search_user_first_name; ?> From <?php echo $search_user_city;?>, <?php echo $search_user_state;?>, <?php echo $search_user_country;?>. <?php echo $search_user_first_name.'\'s '.$siteName;?>  Profile ID is <?php echo $search_value;?>. <?php echo $search_user_first_name;?>'s Date Of Birth is <?php echo $dispaly_user_dob;?>. <?php echo $search_user_first_name;?>'s mother tongue is <?php echo $search_user_mother_tongue;?>. <?php echo $search_user_first_name;?> lives in a <?php echo $search_user_family_type;?>. <?php echo $search_user_first_name;?> belongs to <?php echo $search_user_family_status;?> family. <?php echo $search_user_first_name;?> follows <?php echo $search_user_family_value;?> Family Values."/>

	<meta name="keywords" content="<?php echo $search_value;?>, <?php echo $search_user_first_name; ?>, <?php echo $search_user_city;?>, <?php echo $search_user_state;?>, <?php echo $search_user_country;?>">

	<meta property="og:title" content="Matrimonial Profile of <?php echo $search_user_first_name; ?> - <?= $siteName; ?>" />
	<meta property="og:type" content="matimonial.<?php echo $search_user_first_name; ?>" />
  	<meta property="og:url" content="<?= $profile_link; ?>" />
  	<meta property="og:description" content="Matrimonial Profile of <?php echo $search_user_first_name; ?> From <?php echo $search_user_city;?>, <?php echo $search_user_state;?>, <?php echo $search_user_country;?>. <?php echo $search_user_first_name.'\'s '.$siteName;?>  Profile ID is <?php echo $search_value;?>. <?php echo $search_user_first_name;?>'s Date Of Birth is <?php echo $dispaly_user_dob;?>. <?php echo $search_user_first_name;?>'s mother tongue is <?php echo $search_user_mother_tongue;?>. <?php echo $search_user_first_name;?> lives in a <?php echo $search_user_family_type;?>. <?php echo $search_user_first_name;?> belongs to <?php echo $search_user_family_status;?> family. <?php echo $search_user_first_name;?> follows <?php echo $search_user_family_value;?> Family Values." />
  	<meta property="og:image" content="<?= $search_user_profile_pic;?>" />
  	<meta property="twitter:title" content="Matrimonial Profile of <?php echo $search_user_first_name; ?> - <?= $siteName; ?>" />
  	<meta property="twitter:url" content="<?= $profile_link; ?>" />
  	<meta property="twitter:description" content="Matrimonial Profile of <?php echo $search_user_first_name; ?> From <?php echo $search_user_city;?>, <?php echo $search_user_state;?>, <?php echo $search_user_country;?>. <?php echo $search_user_first_name.'\'s '.$siteName;?>  Profile ID is <?php echo $search_value;?>. <?php echo $search_user_first_name;?>'s Date Of Birth is <?php echo $dispaly_user_dob;?>. <?php echo $search_user_first_name;?>'s mother tongue is <?php echo $search_user_mother_tongue;?>. <?php echo $search_user_first_name;?> lives in a <?php echo $search_user_family_type;?>. <?php echo $search_user_first_name;?> belongs to <?php echo $search_user_family_status;?> family. <?php echo $search_user_first_name;?> follows <?php echo $search_user_family_value;?> Family Values." />
  	<meta property="twitter:image" content="<?= $search_user_profile_pic;?>" />

	<style>
		.carousel-indicators{border-color:#53636f;top:100px; margin-top: 95px;}
		.carousel-inner{margin-top:30px;}
		.carousel-control{position:relative;top:50px;}
		a.left{float:left;text-align:left;font-size:2.3rem;}
		a.right{float:right;text-align:right;font-size:2.3rem;}

	</style>
	
<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>	

	<section role="main" class="content-body main-section-start">

		<?php
			$search_code = $_GET['search'];
			$search_code_val = explode('-',$search_code);
			$search_type = $search_code_val[0];
			$search_value = $search_code_val[1];

			$pro_url = $WebSiteBasePath.'/users/';
			if($search_type=='Profile' || $search_type=='profile')
			{
				$sql_search_user_info = "SELECT * FROM clients WHERE unique_code='$search_value' AND status=1";
			
				$stmt   = $link->prepare($sql_search_user_info);
		        $stmt->execute();
		        $count = $stmt->rowCount();
		        if($count>0)
		        {
		        	$base_path = $WebSiteBasePath.'/';
		        	$today = date('Y-m-d');
		        	$photo = null;
		        	$search_userRow = $stmt->fetch();
		        	$search_user_id = $search_userRow['id'];

		        	/*********    Updating profile view start   ***********/
		        		updateProfileViewCount($search_user_id,$search_value);
		        	/*********    Updating profile view end   ***********/

		        	$search_user_first_name = $search_userRow['firstname'];
		        	$search_user_last_name = $search_userRow['lastname'];
		        	$search_user_lastName = substr($search_user_last_name,0,1);
		        	$search_user_name = $search_userRow['firstname']." ".$search_userRow['lastname'];
			        $search_user_dob = $search_userRow['dob'];
			        $search_user_gender = $search_userRow['gender'];
			        if($search_user_gender=='1')
			        {
			        	$display_gender = "His";
			        }
			        else
			        if($search_user_gender=='2')
			        {
			        	$display_gender = "Her";
			        }
			        else
			        if($search_user_gender=='3')
			        {
			        	$display_gender = "His/Her";
			        }

			        $today = date('Y-m-d');
			        $search_user_age_calculate = date_diff(date_create($search_user_dob), date_create($today));
			        $search_user_age_year = $search_user_age_calculate->format('%y');
			        $search_user_age_month = $search_user_age_calculate->format('%m');

			        $sql_search_user_profile = "SELECT photo FROM profilepic WHERE userid='$search_user_id'";
					$stmt1   = $link->prepare($sql_search_user_profile);
			        $stmt1->execute();
			        //$count_user_pic = $stmt1->rowCount();
			        $search_user_profile = $stmt1->fetch();
			        $search_user_profile_pic_file_name = $search_user_profile['photo'];

			        if($search_user_profile_pic_file_name!='' || $search_user_profile_pic_file_name!=null)
			        {
			        	$search_user_profile_pic = 'uploads/'.$search_user_id.'/profile/'.$search_user_profile['photo'];
			        }
			        else
			        { 
			        	$search_user_profile_pic = null;
			        }


			        $sql_search_user_pic = "SELECT photo FROM usergallery WHERE userid='$search_user_id' AND status <>'2'";
					$stmt   = $link->prepare($sql_search_user_pic);
			        $stmt->execute();
			        $count_user_pic = $stmt->rowCount();
			        $search_userpic = $stmt->fetchAll();
			        foreach($search_userpic as $row )
					{
						$photo[] = $row['photo']; 
					}
					//print_r($photo);
			        /*
			        $search_user_pic = $search_userpic[0]['photo'];   //all photos
					*/

			        $sql_search_user_basic = "SELECT * FROM profilebasic WHERE userid='$search_user_id'";
					$stmt   = $link->prepare($sql_search_user_basic);
			        $stmt->execute();
			        $count_userbasic = $stmt->rowCount();
			        $search_userbasic = $stmt->fetch();
			        $search_user_height = $search_userbasic['height'];
			        $search_user_weight = $search_userbasic['weight'];
			        $search_user_eat_habbit = $search_userbasic['eat_habbit'];
			        $search_user_smoke_habbit = $search_userbasic['smoke_habbit'];
			        $search_user_body_type = $search_userbasic['body_type'];
			        $search_user_complexion = $search_userbasic['complexion'];
			        $search_user_marital_status = $search_userbasic['marital_status'];
			        $search_user_special_case = $search_userbasic['special_case'];
			        $search_user_drink_habbit = $search_userbasic['drink_habbit'];

			        $sql_search_user_religious = "SELECT religion,caste,mother_tongue FROM profilereligion WHERE userid='$search_user_id'";
					$stmt   = $link->prepare($sql_search_user_religious);
			        $stmt->execute();
			        $count_user_religious = $stmt->rowCount();
			        $search_userreligious = $stmt->fetch();
			        $search_user_religion = $search_userreligious['religion'];
			        $search_user_caste = $search_userreligious['caste'];
			        $search_user_mother_tongue = $search_userreligious['mother_tongue'];

			       	$sql_search_user_location = "SELECT city,state,country FROM address  WHERE userid='$search_user_id'";
					$stmt   = $link->prepare($sql_search_user_location);
			        $stmt->execute();
			        $count_user_location = $stmt->rowCount();
			        $search_userlocation = $stmt->fetch();
			        $search_user_city = $search_userlocation['city'];
			        $search_user_state = $search_userlocation['state'];
			        $search_user_country = $search_userlocation['country'];
			       	

			        $sql_search_user_education = "SELECT education,occupation,income,income_currency FROM eduocc WHERE userid='$search_user_id'";
					$stmt   = $link->prepare($sql_search_user_education);
			        $stmt->execute();
			        $count_user_education = $stmt->rowCount();
			        $search_usereducation = $stmt->fetch();
			        $search_user_edu_name = $search_usereducation['education'];
			        $search_user_occ_name = $search_usereducation['occupation'];
			        $search_user_income = $search_usereducation['income'];
			        $search_user_curr_name = $search_usereducation['income_currency'];

			        $sql_search_user_family = "SELECT fam_val,fam_type,fam_stat FROM family WHERE userid='$search_user_id'";
					$stmt   = $link->prepare($sql_search_user_family);
			        $stmt->execute();
			        $count_user_family = $stmt->rowCount();
			        $search_userfamily = $stmt->fetch();
			        $search_user_fam_val = $search_userfamily['fam_val'];
			        $search_user_fam_type = $search_userfamily['fam_type'];
			        $search_user_fam_stat = $search_userfamily['fam_stat'];

			        $sql_search_user_desc = "SELECT short_desc,short_desc_family,short_desc_interest FROM profiledesc WHERE userid='$search_user_id'";
					$stmt   = $link->prepare($sql_search_user_desc);
			        $stmt->execute();
			        $count_user_user_desc = $stmt->rowCount();
			        $sql_search_userdesc = $stmt->fetch();
			        $search_user_short_desc = $sql_search_userdesc['short_desc'];
			        $search_user_short_desc_family = $sql_search_userdesc['short_desc_family'];
			        $search_user_short_desc_interest = $sql_search_userdesc['short_desc_interest'];
			        
			        
			    ?>
			    <span itemscope itemtype='http://schema.org/Person'>
			    	<meta itemprop='name' content="<?= $search_user_first_name; ?>"/>
					<link itemprop='url' href="<?= $profile_link; ?>"/>
					<meta itemprop='description' content="Matrimonial Profile of <?php echo $search_user_first_name; ?> From <?php echo $search_user_city;?>, <?php echo $search_user_state;?>, <?php echo $search_user_country;?>. <?php echo $search_user_first_name.'\'s '.$siteName;?>  Profile ID is <?php echo $search_value;?>. <?php echo $search_user_first_name;?>'s Date Of Birth is <?php echo $dispaly_user_dob;?>. <?php echo $search_user_first_name;?>'s mother tongue is <?php echo $search_user_mother_tongue;?>. <?php echo $search_user_first_name;?> lives in a <?php echo $search_user_family_type;?>. <?php echo $search_user_first_name;?> belongs to <?php echo $search_user_family_status;?> family. <?php echo $search_user_first_name;?> follows <?php echo $search_user_family_value;?> Family Values."/>
					<meta itemprop='address' content="<?= getUserCityName($search_user_city).','.getUserStateName($search_user_state).','.getUserCountryName($search_user_country); ?>"/>
					<meta itemprop='birthDate' content="<?= $dispaly_user_dob; ?>"/>
					<meta itemprop='gender' content="<?= $meta_gender; ?>"/>
					<meta itemprop='givenName' content="<?= $search_user_first_name; ?>"/>
					<meta itemprop='height' content="<?= $search_user_height; ?>"/>
					<meta itemprop='homeLocation' content="<?= getUserCityName($search_user_city); ?>"/>
					<meta itemprop='jobTitle' content="<?= $search_user_occ_name_meta; ?>"/>
					<meta itemprop='knowsLanguage' content="<?= getUserMotherTongueName($search_user_mother_tongue); ?>"/>
					<meta itemprop='nationality' content="<?= getUserCountryName($search_user_country); ?>"/>
					<meta itemprop='netWorth' content="<?= $search_user_income; ?>"/>
					<meta itemprop='weight' content="<?= $search_user_weight; ?>"/>
				</span>
					<?php
						if($loggedIn=='1')
						{
							$user_id = $_SESSION['user_id'];

							$social_sharing_display = getSocialSharingDisplayOrNot();
							if($social_sharing_display=='1' && $search_user_id==$user_id)
							{
						
						
					?>
					<div class="row start_section start-section-show-profile">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
							<b class='error'>Please share your profile to increase visibility</b>
						</div>
					</div>
					<?php
							}

						}
					?>
			    <div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row vertical-divider">
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 searched-profile-desc">
								<div class="row centered-data">
									<h1 class="profile_info_data">Profile ID : <?php echo $search_value;?></h1>
								</div>
								<div class="row">
									
									<?php
										if($count_user_pic>0)
										{
									?>
											<div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
												<ol class='carousel-indicators'>
													<?php 

														echo "<li data-target='#myCarousel' data-slide-to='0' class='active'></li>";

														for($i=1;$i<$count_user_pic+1;$i++)
														{
															echo "<li data-target='#myCarousel' data-slide-to='$i' ></li>";
														}
														
													?>
												</ol>

												<div class="carousel-inner" role="listbox">
												<?php
													//Profile Picture
													if($search_user_profile_pic_file_name!='' || $search_user_profile_pic_file_name!=null)
													{
														
														echo "<div class='item active popup-gallery centered-data'> <a class='thumb-image' href='$search_user_profile_pic'><img itemprop='image' src='$WebSiteBasePath/users/$search_user_profile_pic' class=' profile-img-slide' alt='$search_user_first_name'></a> </div>";
													}
													else
													{
														echo "<div class='item active popup-gallery centered-data'> <a class='thumb-image' href='$search_user_profile_pic'><img itemprop='image' src='$WebSiteBasePath/users/$search_user_profile_pic' class=' profile-img-slide' alt='$search_user_first_name'></a> </div>";
													}
												?>


												<?php 
													//Personal photos
													for($i=0;$i<$count_user_pic;$i++)
													{
														$photo_img = getWebsiteBasePath().'/users/uploads/'.$search_user_id.'/photo/'.$photo[$i];
														
														if($i==0)
														{
															echo "<div class='item popup-gallery centered-data'> <a class='thumb-image' href='$photo_img'><img itemprop='image' src='$photo_img' class=' profile-img-slide' alt='$search_user_first_name'></a> </div>";
														}
														else
														{
															echo "<div class='item popup-gallery centered-data'> <a class='thumb-image' href='$photo_img'><img itemprop='image' src='$photo_img' class='profile-img-slide' alt='$search_user_first_name'> </a></div>";
														}
													}
													
												?>
												</div>

												<a href="" class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
													<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
													<span class="sr-only">&#171;</span>
												</a>
												<a href="" class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
													<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
													<span class="sr-only">&#187;</span>
												</a>
											</div><!-- #myCarousel -->
									<?php 
										}
			    	
										else
										{ 
											if($search_user_profile_pic_file_name!='' || $search_user_profile_pic_file_name!=null)
											{
												echo "<div class='item active thumb-preview centered-data'> <a class='thumb-image' href='$search_user_profile_pic'><img src='$WebSiteBasePath/users/$search_user_profile_pic' class='profile-img-slide' alt='$search_user_first_name'> </a></div>";
											}
											else
											{
												echo "<div class='thumb-preview centered-data'><a class='thumb-image' href='$WebSiteBasePath/images/no_profile_pic.png'><img itemprop='image' src='$WebSiteBasePath/images/no_profile_pic.png'  class='img-responsive profile-img-slide' alt='$search_user_first_name' title='$search_user_first_name' style='height:150px;'/></a></div>";
											}
											
										}
									?>
								</div>
								<div class='row clearfix'>&nbsp;</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<p>Age : </p>
									</div>
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
										<p class="profile_search_description">
										<?php
											 
											echo $search_user_age_year." Years,  ".$search_user_age_month." Months";
										?>
										</p>
									</div>
								</div>
								
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<p>Location :</p> 
									</div>
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
										<p class="profile_search_description">
											<?php 
												if($count_user_location>0)
												{
													$city_search_name = getUserCityName($search_user_city);
													$state_search_name = getUserStateName($search_user_state);
													$country_search_name = getUserCountryName($search_user_country);

													$city_search_name_url = str_replace(' ','_',$city_search_name);
													$state_search_name_url = str_replace(' ','_',$state_search_name);
													$country_search_name_url = str_replace(' ','_',$country_search_name);

													$city_search = 'Search-City-'.$city_search_name_url.'.html';
													$state_search ='Search-State-'.$state_search_name_url.'.html';
													$country_search ='Search-Country-'.$country_search_name_url.'.html';
													echo "<a href='$city_search'>".$city_search_name."</a>, <a href='$state_search'>".$state_search_name."</a>, <a href='$country_search'>".$country_search_name."</a>";
												}
												else
												{
													echo "-";
												}
											?>
										</p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<p>Religion/Caste</p> 
									</div>
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
										<p class="profile_search_description">
											<?php 
												if($count_user_religious>0)
												{
													$religion_search_name = getUserReligionName($search_user_religion);

													$religion_search_name_url = str_replace(' ','_',$religion_search_name);

													$religion_search = 'Search-Religion-'.$religion_search_name_url.'.html';
													echo "<a href='$religion_search'>".$religion_search_name."</a>";
												}
												else
												{
													echo "-";
												}

												echo " / ";
												if($count_user_religious>0)
					                            {
					                            	$caste_search_name = getUserCastName($search_user_caste);

					                            	$caste_search_name_url = str_replace(' ','_',$caste_search_name);

					                            	$caste_search ='Search-Caste-'.$caste_search_name_url.'.html';
													echo "<a href='$caste_search'>".$caste_search_name."</a>";
					                            }
					                            else
					                            {
					                              echo "-";
					                            }
											?>
										</p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<p>Education :</p> 
									</div>
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
										<p class="profile_search_description">
											<?php 
												if($count_user_education>0)
												{
													$education_search_name = getUserEducationName($search_user_edu_name);

													$education_search_url = str_replace(' ','_',$education_search_name);

													$education_search ='Search-Education-'.$education_search_url.'.html';
													echo "<a href='$education_search'>".$education_search_name."</a>";
												}
												else
												{
													echo "-";
												}
											?>
										</p>
									</div>
								</div>
								
								<form>
									<?php
										if(!isset($user_id) || $user_id=='' || $user_id==null)
										{
											$user_id = '0';
										}
									?>
									<input type="hidden" class="user_id" value="<?php echo $user_id; ?>">
									<input type="hidden" class="search_user_id" value="<?php echo $search_user_id; ?>">

									<div class="row centered-data">
										<img src="../images/loader/loader.gif" class='img-responsive loading_img centered-loading-image' id='loading_img' alt='loading' style='width:80px; height:80px; display: none;'/>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 show_profile_status">
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 show_profile_shortlist_status">
										</div>
									</div>
									<div class='row clearfix'>&nbsp;</div>
									<div class="row">
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<a href="message.php?id=<?php echo $search_user_id;?>" class="btn btn-primary send_message_button profile-utility-button website-button">Send Message</a>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<?php
												if($loggedIn=='1')
												{
													$send_interest = sendinterestbtn($user_id, $search_user_id);
													if($send_interest>0)
													{
														echo "<button class='btn btn-primary profile-utility-button website-button' disabled><span class='fa fa-check'></span> Interested</button>";
													}
													else
													{
														echo "<button class='btn btn-primary profile_interest profile-utility-button website-button'>Send Interest</button>";
													}
												}
												else
												{
													echo "<button class='btn btn-primary profile_interest profile-utility-button website-button'>Send Interest</button>";
												}
											?>
										</div>
									</div>
									<div class='row clearfix'>&nbsp;</div>
									<div class="row">
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<?php
												if($loggedIn=='1')
												{
													$shortlisted = shortlistbtn($user_id, $search_user_id);
													if($shortlisted>0)
													{
														echo "<button class='btn btn-primary profile-utility-button website-button' disabled><span class='fa fa-check'></span> Shortlisted</button>";
													}
													else
													{
														echo "<button class='btn btn-primary shortlist_profile profile-utility-button website-button'>Shortlist Profile</button>";
													}
												}
												else
												{
													echo "<button class='btn btn-primary shortlist_profile profile-utility-button website-button'>Shortlist Profile</button>";
												}
											?>
											
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<?php
												if($loggedIn=='1')
												{
													if($search_user_profile_pic=='' || $search_user_profile_pic==null)
													{
														$requested_photo = request_photo_btn($user_id, $search_user_id);
														if($requested_photo>0)
														{
															echo "<button class='btn btn-primary profile-utility-button request_photo_btn website-button' disabled><span class='fa fa-check'></span> Requested</button>";
														}
														else
														{
															echo "<button class='btn btn-primary request_photo_btn profile-utility-button website-button'>Request Photo</button>";
														}
													}
													else
													{
														echo "<button class='btn btn-primary profile-utility-button website-button' disabled>Request Photo</button>";
													}
												}
												else
												{
													if($search_user_profile_pic=='' || $search_user_profile_pic==null)
													{
														echo "<button class='btn btn-primary request_photo_btn profile-utility-button website-button'>Request Photo</button>";
													}
													else
													{
														echo "<button class='btn btn-primary profile-utility-button website-button' disabled>Request Photo</button>";
													}
												}
											?>
										</div>
									</div>
									<div class='row clearfix'>&nbsp;</div>
									
									<?php
									if($user_id!='0')
									{
										$userLikedProfileOrNot = getuserLikedProfileOrNot($user_id,$search_user_id);
									?>
										<!--  Like profile section  -->
										<div class="row" id="profile-Like">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<button class='btn btn-primary like-profile website-button' style="width:100%;" <?php if($userLikedProfileOrNot=='1') { echo 'disabled'; }?>> <i class='ace-icon fa fa-thumbs-o-up'></i> Like+  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= getLikeProfileCount($search_user_id)?> Likes </button>
											</div>
											<input type="hidden" class="userid" value="<?php echo $_SESSION['user_id'];?>">
											<input type="hidden" class="liked_userid" value="<?php echo $search_user_id;?>">
											<input type="hidden" class="unique_code" value="<?php echo $search_value;?>">
										</div>
										<div class='row clearfix'>&nbsp;</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 centered-data">
												<img src="../images/loader/loader.gif" class='img-responsive loading_img_like_profile centered-loading-image' id='loading_img_like_profile' alt='loading' style='width:50px; height:50px; display: none;'/>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 like_profile_status">
												<?php
													if($userLikedProfileOrNot!='0')
													{
														echo "<p class='profile-liked-user'><span class='fa fa-check'></span> You already liked this profile.</p>";
													}
												?>
												
											</div>
										</div>
										<br/>
									<?php
									}
									else
									{
										$profile_likes = getLikeProfileCount($search_user_id);
										echo "<button class='btn btn-primary like-profile website-button' style='width:100%;'> <i class='ace-icon fa fa-thumbs-o-up'></i> Like+  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$profile_likes  Likes </button><br/><br/>";
									}
									?>
								</form>
							

								<div class="row" id="profile-Like">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 centered-data">
										<p class='profile-view-user'> <i class="fa fa-eye"></i> &nbsp;&nbsp;<?= getProfileViewCount($search_user_id,$search_value)?> Profile Views</p>
									</div>
								</div>
								<br/>

								
									<?php
							            $display_square_popup_ad = getRandomSquarePopUpAdDisplayOrNot();
							            if($display_square_popup_ad=='1')
							            {
							                echo "<div class='squarepopup-ad'>";
							              echo $squarepopup_ad = getRandomSquarePopUPAdData();
							              echo "</div>";
							            }
							        ?>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
									<div class="row">
										<h2 class="profile_info_data">About 
											<?php
												$hideLastNameOfMember = gethideLastNameOfMember();
												if($hideLastNameOfMember=='1')
												{
													echo ucwords($search_user_first_name)." ".ucwords($search_user_lastName)."XXXXXXXXX ";
												} 
												else
												{
													echo ucwords($search_user_first_name)." ".ucwords(getUserLastName($search_user_id));
												}
											
											?>
										</h2>
									</div>
								</div>
								<br/>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<h3>
											Basic Information
										</h3>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 profile_info_data">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Name 	:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
															$hideLastNameOfMember = gethideLastNameOfMember();
															if($hideLastNameOfMember=='1')
															{
																echo ucwords($search_user_first_name)." ".ucwords($search_user_lastName)."XXXXXXXXX ";
															} 
															else
															{
																echo ucwords($search_user_first_name)." ".ucwords(getUserLastName($search_user_id));
															}
														?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p class="profile_search_description">Birth Date :</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
															echo date('d-M-Y',strtotime($search_user_dob));
														?>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Height (cm/mtr/foot):</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
															if($count_userbasic>0)
															{
																$search_user_height_meter = round(($search_user_height)/100,2);
											                	$search_user_height_foot = round(($search_user_height)/30.48,2);
											                	echo $search_user_height.'-cms / '.$search_user_height_foot.'-fts / '.$search_user_height_meter.'-mts';
															}
															else
															{
																echo "-";
															}
														?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Body Type:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
															if($count_userbasic>0)
															{
																$body_type = getUserBodyTypeName($search_user_body_type);

																$body_type_url = str_replace(' ','_',$body_type);
																

												                $body_type_search = 'Search-BodyType-'.$body_type_url.'.html';

																echo "<a href='$body_type_search'>".$body_type."</a>";
															}
															else
															{
																echo "-";
															}
														?>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Weight (kg):</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
															if($count_userbasic>0)
															{
																echo $search_user_weight;
															}
															else
															{
																echo "-";
															}
														?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Complexion:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_userbasic>0)
								                            {
								                            	$complexion = getUserComplexionName($search_user_complexion);

								                            	$complexion_url = str_replace(' ','_',$complexion);

												              	$complexion_search = 'Search-Complexion-'.$complexion_url.'.html';

																echo "<a href='$complexion_search'>".$complexion."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                          ?>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Eating Habit:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_userbasic>0)
								                            {
								                            	$eat_habbit = getUserEatHabbitName($search_user_eat_habbit);

								                            	$eat_habbit_url = str_replace(' ','_',$eat_habbit);

												                $eat_search = 'Search-EatingHabit-'.$eat_habbit_url.'.html';
																echo "<a href='$eat_search'>".$eat_habbit."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                          ?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Marital Status:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<?php 
									                	
									                ?>
													<p class="profile_search_description">
														<?php 
								                            if($count_userbasic>0)
								                            {
								                            	$marital_status = getUserMaritalStatusName($search_user_marital_status);

								                            	$marital_status_url = str_replace(' ','_',$marital_status);

												                $marital_search = 'Search-MaritalStatus-'.$marital_status_url.'.html';
																echo "<a href='$marital_search'>".$marital_status."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Smoking Habit:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_userbasic>0)
								                            {
								                            	$smoke_habbit = getUserSmockingHabbitName($search_user_smoke_habbit);
								                            	
								                            	$smoke_habbit_url = str_replace(' ','_',$smoke_habbit);

												                $smoke_search = 'Search-SmokingHabit-'.$smoke_habbit_url.'.html';
												                echo "<a href='$smoke_search'>".$smoke_habbit."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Special Case:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_userbasic>0)
								                            {
								                            	$special_case = getUserSpecialCaseName($search_user_special_case);

								                              echo $special_case;
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Drinking Habit:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_userbasic>0)
								                            {
								                            	$drink_habbit = getUserDrinkHabbitName($search_user_drink_habbit);

								                            	$drink_habbit_url = str_replace(' ','_',$drink_habbit);

												                $drink_search = 'Search-DrinkingHabit-'.$drink_habbit_url.'.html';
												                echo "<a href='$drink_search'>".$drink_habbit."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<!--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Date Of Birth:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php echo $search_user_dob;?>
													</p>
												</div>-->
											</div>
										</div>
									</div>
								</div>
								<hr/>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<h3>
											Religious & Location Information
										</h3>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Religion :</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_religious>0)
								                            {
								                            	$search_user_religion = getUserReligionName($search_user_religion);

								                            	$religion_search_url = str_replace(' ','_',$search_user_religion);

								                            	$religion_search = 'Search-Caste-'.$religion_search_url.'.html';

																echo "<a href='$religion_search'>".$search_user_religion."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>City :</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_location>0)
								                            {
								                            	$city_search_name = getUserCityName($search_user_city);

								                            	$city_search_name_url = str_replace(' ','_',$city_search_name);

								                            	$city_search = 'Search-City-'.$city_search_name_url.'.html';

								                              echo "<a href='$city_search'>".$city_search_name."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Caste :</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_religious>0)
								                            {
								                            	$search_user_caste = getUserCastName($search_user_caste);

								                            	$caste_search_url = str_replace(' ', '_', $search_user_caste);
								                            	
								                            	$caste_search = 'Search-Caste-'.$caste_search_url.'.html';

																echo "<a href='$caste_search'>".$search_user_caste."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>State :</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_location>0)
								                            {
								                            	$state_search_name = getUserStateName($search_user_state);

								                            	$state_search_name_url = str_replace(' ','_',$state_search_name);

								                            	$state_search ='Search-State-'.$state_search_name_url.'.html';
								                            	echo "<a href='$state_search'>".$state_search_name."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Mother Tongue:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_religious>0)
								                            {
								                            	$search_user_mother_tongue = getUserMotherTongueName($search_user_mother_tongue);

								                            	$mother_tongue_search_url = str_replace(' ','_',$search_user_mother_tongue);

								                            	$mother_tongue_search = 'Search-MotherTongue-'.$mother_tongue_search_url.'.html';

																echo "<a href='$mother_tongue_search'>".$search_user_mother_tongue."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Country :</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_location>0)
								                            {
								                            	$country_search_name = getUserCountryName($search_user_country);

								                            	$country_search_name_url = str_replace(' ','_',$country_search_name);

								                            	$country_search ='Search-Country-'.$country_search_name_url.'.html';
								                            	echo "<a href='$country_search'>".$country_search_name."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr/>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<h3>
											Education, Career & Family Information
										</h3>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Education:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_education>0)
								                            {
								                            	$education_search_name = getUserEducationName($search_user_edu_name);

								                            	$education_search_url = str_replace(' ','_', $education_search_name);

								                              	$education_search = 'Search-Education-'.$education_search_url.'.html';
																echo "<a href='$education_search'>".$education_search_name."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Family Value:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_family>0)
								                            {
								                            	$fam_val = getUserFamilyValueName($search_user_fam_val);

								                            	$fam_val_url = str_replace(' ','_',$fam_val);


												                $fam_val_search = 'Search-FamilyValue-'.$fam_val_url.'.html';
																echo "<a href='$fam_val_search'>".$fam_val."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Occupation Detail:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_education>0)
								                            {
								                            	$occ_search_name = getUserEmploymentName($search_user_occ_name);

								                            	$occ_search_url = str_replace(' ','_',$occ_search_name);

								                            	$occ_search = 'Search-Occupation-'.$occ_search_url.'.html';
																echo "<a href='$occ_search'>".$occ_search_name."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Family Type:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_family>0)
								                            {
								                            	$fam_type = getUserFamilyTypeName($search_user_fam_type);

								                            	$fam_type_url = str_replace(' ','_',$fam_type);

												                $fam_type_search = 'Search-FamilyType-'.$fam_type_url.'.html';
																echo "<a href='$fam_type_search'>".$fam_type."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Monthly Income:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_education>0)
								                            {
								                            	$search_user_curr_name = getUserIncomeCurrencyCode($search_user_curr_name);

								                              echo $search_user_curr_name.' '.$search_user_income;
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>Family Status:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<?php
										                
										            ?>
													<p class="profile_search_description">
														<?php 
								                            if($count_userbasic>0)
								                            {
								                            	$fam_stat = getUserFamilyStateName($search_user_fam_stat);

								                            	$fam_stat_url = str_replace(' ','_',$fam_stat);

												                $fam_stat_search = 'Search-FamilyStatus-'.$fam_stat_url.'.html';
																echo "<a href='$fam_stat_search'>".$fam_stat."</a>";
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if($loggedIn=='1') { ?>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<h3>
											Short Description About <?php echo $search_user_first_name;?> & <?php echo $display_gender;?> Interested Profile
										</h3>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>About <?php echo $search_user_first_name;?>:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_user_desc>0)
								                            {
								                              echo $search_user_short_desc;
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p>About <?php echo $search_user_first_name;?>'s Family:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_user_desc>0)
								                            {
								                              echo $search_user_short_desc_family;
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
													<p><?php echo $search_user_first_name;?> is Looking For:</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
													<p class="profile_search_description">
														<?php 
								                            if($count_user_user_desc>0)
								                            {
								                              echo $search_user_short_desc_interest;
								                            }
								                            else
								                            {
								                              echo "-";
								                            }
								                        ?>
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<h3>
											Identity Verification
										</h3>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<?php
												$sql_user_doc_status = "SELECT * FROM documents WHERE userid='$search_user_id' AND status <> '2'";
												$stmt_user_doc_status=$link->prepare($sql_user_doc_status);
												$stmt_user_doc_status->execute();
												$count_user_doc_status = $stmt_user_doc_status->rowCount();
												$result_user_doc_status = $stmt_user_doc_status->fetchAll();
												//print_r($result_user_doc_status);
												if($count_user_doc_status==0)
												{
													echo "<div class='row'>
													<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
														<p>Member not uploaded any identity documents.</p>
													</div>
												</div>";
												}
												else
												if($count_user_doc_status>0)
												{
													echo "<div class='row'>";
													echo "<table class='table table-responsive table-bordered'>";
														echo "<tr>";
															echo "<th>Document Type</th>";
															echo "<th>Document Status</th>";
														echo "</tr>";
													foreach ($result_user_doc_status as $row_user_doc_status) 
													{
														$document_type = $row_user_doc_status['photo_type'];
														$document_status = $row_user_doc_status['status'];

														if($document_status=='0')
														{
															$document_status="<b class='label label-warning'><span class='fa fa-exclamation-circle'></span> Pending Verification</b>";;
														}
														else
														if($document_status=='1')
														{
															$document_status="<b class='label label-success'><span class='fa fa-check'></span> Approved</b>";
														}

														echo "<tr>";
															echo "<td>$document_type</td>";
															echo "<td>$document_status</td>";
														echo "</tr>";
													}
													
													echo "</table>";
													echo "</div>";
												}
											?>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				
				<br/>
				<hr class="solid">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
								
								<?php
									$sql = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
							        	clients as A 
							        	LEFT OUTER JOIN
										profilebasic AS B ON A.id = B.userid
										LEFT OUTER JOIN
										address AS C ON A.id = C.userid
										LEFT OUTER JOIN
										profilereligion AS D ON A.id = D.userid
										LEFT OUTER JOIN
										eduocc AS E ON A.id = E.userid
										LEFT OUTER JOIN
										profilepic AS F ON A.id = F.userid
										LEFT OUTER JOIN
										family AS G ON A.id = G.userid
										WHERE A.gender='$search_user_gender' AND A.status=1 AND A.id <> '$search_user_id' ORDER BY rand()";
									$stmt_search   = $link->prepare($sql);
									$stmt_search->execute();
									$sqlTot = $stmt_search->rowCount();
									$result_search = $stmt_search->fetchAll();

								echo "<h2>$sqlTot similar profile found</h2>";	
									if($sqlTot>0)
									{	
										$i=0;
										foreach( $result_search as $row )
										{
											$city = $row['city'];
											$city_Name = getUserCityName($city);
											$state = $row['state'];
											$state_Name = getUserStateName($state);
											$country = $row['country'];
											$country_Name = getUserCountryName($country);
											$edu = $row['education'];
											$occ = $row['occupation'];
											$religion = $row['religion'];
											$caste = $row['caste'];
											$mother_tongue = $row['mother_tongue'];
											$u_code = $row['unique_code'];
											$dobx = $row['dob'];
											$dispaly_user_dob = date('d-M-Y',strtotime($dobx));
											$profileid = $row['id'];
											$gender = $row['gender'];
											$height = $row['height'];
											$weight = $row['weight'];
											if($gender=='1')
									        {
									        	$meta_gender = "Male";
									        }
									        else
									        if($gender=='2')
									        {
									        	$meta_gender = "Female";
									        }
									        else
									        if($gender=='3')
									        {
									        	$meta_gender = "T-Gender";
									        }
									        $occupation = $row['occupation'];
		        							$occupation_name = getUserEmploymentName($occupation);
											$salary = $row['income'];
											$currency = getUserIncomeCurrencyCode($row['income_currency']);
											$fam_type = $row['fam_type'];
											$fam_status = $row['fam_stat'];
											$fam_value = $row['fam_val'];

											$search_data = 'Profile-'.$u_code;
											$pro_url = $search_data.".html";
											$search_user_fname=$row['firstname'];
											$search_user_name = $row['firstname']." ".$row['lastname'];
											$prefer_user_id =$row['idx'];
											$photo =$row['photo'];
											$today = date('Y-m-d');
											$diff = date_diff(date_create($dobx), date_create($today));
											$age_search_user = $diff->format('%y');
											$user_occ = user_occ($occ);
											$site_url = getWebsiteBasePath();
											
											$fam_type_name = getUserFamilyTypeUser($prefer_user_id);
											$fam_status_name = getUserFamilyStatusUser($prefer_user_id);
											$fam_value_name = getUserFamilyValueUser($prefer_user_id);
											
									if($i==0 || ($i%3==0))
									{
										echo "<div class='row'>";
									}		
								?>
									<a href="<?php echo $pro_url; ?>" style="text-decoration:none; color:#000000;">
										<div class="card-my-matches col-xs-12 col-sm-12 col-md-4 col-lg-4">
											<span itemscope itemtype='http://schema.org/Person'>
												<meta itemprop='name' content="<?= $search_user_fname; ?>"/>
												<link itemprop='url' href="<?= $site_url.'/users/'.$pro_url; ?>;"/>
												<meta itemprop='description' content="Matrimonial Profile of <?php echo $search_user_fname; ?> From <?php echo $city_Name;?>, <?php echo $state_Name;?>, <?php echo $country_Name;?>. <?php echo $search_user_fname.'\'s '.$siteName;?>  Profile ID is <?php echo $u_code;?>. <?php echo $search_user_fname;?>'s Date Of Birth is <?php echo $dispaly_user_dob;?>. <?php echo $search_user_fname;?>'s mother tongue is <?php echo getUserMotherTongueName($mother_tongue);?>. <?php echo $search_user_fname;?> lives in a <?php echo $fam_type_name;?>. <?php echo $search_user_fname;?> belongs to <?php echo $fam_status_name;?> family. <?php echo $search_user_fname;?> follows <?php echo $fam_value_name;?> Family Values."/>
												<meta itemprop='address' content="<?= $city_Name.','.$state_Name.','.$country_Name; ?>"/>
												<meta itemprop='birthDate' content="<?= $dispaly_user_dob; ?>"/>
												<meta itemprop='gender' content="<?= $meta_gender; ?>"/>
												<meta itemprop='givenName' content="<?= $search_user_fname; ?>"/>
												<meta itemprop='height' content="<?= $height; ?>"/>
												<meta itemprop='homeLocation' content="<?= $city_Name; ?>"/>
												<meta itemprop='jobTitle' content="<?= $occupation_name; ?>"/>
												<meta itemprop='knowsLanguage' content="<?= getUserMotherTongueName($search_user_mother_tongue); ?>"/>
												<meta itemprop='nationality' content="<?= $country_Name; ?>"/>
												<meta itemprop='netWorth' content="<?= $salary; ?>"/>
												<meta itemprop='weight' content="<?= $weight; ?>"/>
											</span>

											<?php
												if($photo=='' || $photo==null)
												{
													echo "<img src='$site_url/images/no_profile_pic.png'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
												}
												else
												{
													echo "<img src='$site_url/users/uploads/$prefer_user_id/profile/$photo'  class='profile_img member-profile-image' alt='$search_user_fname' title='$search_user_fname' style='width:100%'/>";
												}
											?>
											<br><br>
											<h4>
												<b>
													<?php
														echo "<strong>";
														echo ucwords($search_user_fname);
														echo "</strong>";
													?>	
												</b>
											</h4>
											<p class="title">
												Profile ID: <?php echo $u_code; ?><br>
												From <?php echo substr(getUserCityName($city),0,11); ?>
											</p>

											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong class="">Age: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
														
													echo $age_search_user." Years.";
												?>	
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong class="">Height: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
														
													if($height!='' || $height!=null)
													{
														$search_user_height_meter = round(($height)/100,2);
									                	$search_user_height_foot = round(($height)/30.48,2);
									                	echo $height.'-cms/ '.$search_user_height_foot.'-fts/ '.$search_user_height_meter.'-mts';
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Religion: </strong>
													</p> 
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($religion!='' || $religion!=null)
													{
														echo getUserReligionName($religion);
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Caste: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($caste!='' || $caste!=null)
													{
														echo getUserCastName($caste);
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Education: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($edu!='' || $edu!=null)
													{
														echo getUserEducationName($edu);
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Occupation: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($occ!='' || $occ!=null)
													{
														echo user_occ($occ);
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Salary: </strong> 
													</p>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($salary!='' || $salary!=null)
													{
														echo $currency." ".$salary." Monthly";
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
											<div class="row member-profile-info-row">
												<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 h3_heading_profile">
													<p align="justify">
														<strong>Mother Tongue: </strong>
													</p> 
												</div>
												<div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 adv_profile_data">
													<p align="justify">
												<?php 
													echo "&nbsp;";
									                	
													if($mother_tongue!='' || $mother_tongue!=null)
													{
														echo getUserMotherTongueName($mother_tongue);
													}
													else
													{
														echo "-NA-";
													}
												?>
													</p>
												</div>
											</div>
										</div>
									</a>
								<?php
										$i++;

										if($i%3==0)
										{
											echo "</div>";
										}

										}	
									}
									else
									if($sqlTot==0)
									{
										echo "<h3 class='error'> No profile found with your preference.</h3>";
									}
								?>
					</div>
				</div>
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<?php
		            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
			            	if($display_skyscrapper_ad=='1')
			            	{
			            		echo "<div class='skyscrapper-ad'>";
			              			echo $skyscrapper_ad = getRandomSkyScrapperAdData();
			              		echo "</div>";
			            	}
			          	?>
					</div>
				</div>

				
				<?php
				}
		        else
		        {
		        	echo "<h3>Profile Id does not found in records.</h3>";
		        }
		    }
			else
			{

				echo "<h3>Invalid parameters. Try again.</h3>";
			}
			
		?>

	</section>
</div>
</section>


<?php
	include("templates/footer.php");
?>

<?php
    $sql_AddThisScript = "SELECT social_sharing_display,social_sharing_data FROM `social_sharing`";
    $stmt= $link->prepare($sql_AddThisScript); 
    $stmt->execute();
    $count=$stmt->rowCount();
    $result = $stmt->fetch();
    $social_sharing_display = $result['social_sharing_display'];
    $social_sharing_data = $result['social_sharing_data'];

    if($social_sharing_display=='1')
    {
        if($social_sharing_data!='' || $social_sharing_data!=null)
        {
            $social_sharing_data = file_get_contents($websiteBasePath.'/'.$social_sharing_data);
            echo "$social_sharing_data";
        }
    }
?>
<script>
	$(document).ready(function(){

		$('.carousel-control').click(function(e){
			e.preventDefault();
			$('#myCarousel').carousel( $(this).data() );
		});

		var dataTable_Member_list = $('#similar_profiles_list').DataTable({
			"ordering": false,
			"bInfo" : false,
			"oLanguage": {
		      "sLengthMenu": "Showing _MENU_ rows",
		    }
		});

		/******   Send Message check user   ********/
		$('.send_message_button ').click(function(){
			var user_id_x = $('.user_id').val();
			var search_user_id = $('.search_user_id').val();

			if(user_id_x=='0' || user_id_x=='' || user_id_x==null)
			{
				window.location.assign('../login.php');
				return false;
			}

			if(user_id_x==search_user_id)
			{
				alert('You are not allowed to send messages to your own profile.');
				return false;
			}
		});


		/******     Shortlist profile start  *********/
		$('.shortlist_profile').click(function(){
			var user_id_x = $('.user_id').val();
			var search_user_id = $('.search_user_id').val();
			var task = "Shortlist_Profile";

			if(user_id_x=='0' || user_id_x=='' || user_id_x==null)
			{
				window.location.assign('../login.php');
				return false;
			}

			if(user_id_x==search_user_id)
			{
				alert('You are not allowed to shortlist your own profile.');
				return false;
			}

			var data='user_id_x='+user_id_x+'&search_user_id='+search_user_id+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/show_profile_helper.php',
            	success:function(res)
            	{
            		if(res=='success')
            		{
            			$('.shortlist_profile').attr('disabled',true);
            			$('.show_profile_shortlist_status').html("<div class='alert alert-success show_profile_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span> <strong> Success! </strong> Profile shortlisted successfully.</div>");
            			return false;
            		}
            		else
            		{
            			$('.show_profile_shortlist_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}
            	}
            });

		});
		/******     Shortlist profile end  *********/

		/******     Interested profile start  *********/
		$('.profile_interest').click(function(){
			var user_id_x = $('.user_id').val();
			var search_user_id = $('.search_user_id').val();
			var task = "Interest_Profile";

			if(user_id_x=='0' || user_id_x=='' || user_id_x==null)
			{
				window.location.assign('../login.php');
				return false;
			}

			if(user_id_x==search_user_id)
			{
				alert('You are not allowed to interest your own profile.');
				return false;
			}

			$('.profile_interest').attr('disabled',true);
			$('.loading_img').show();
			var data='user_id_x='+user_id_x+'&search_user_id='+search_user_id+'&task='+task;
			
			$.ajax({
				type:'post',
            	data:data,
            	url:'query/show_profile_helper.php',
            	success:function(res)
            	{
            		$('.loading_img').hide();
            		
            		if(res=='success')
            		{
            			$('.profile_interest').attr('disabled',true);
            			$('.show_profile_status').html("<div class='alert alert-success show_profile_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span> <strong> Success! </strong> Interest sent successfully.</div>");
            			$('.show_profile_success_status').fadeTo(1000, 500).slideUp(500, function(){
            				location.reload();
                        });
            		}
            		else
            		{
            			$('.profile_interest').attr('disabled',false);
            			$('.show_profile_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}
            	}
            });

		});
		/******     Interested profile end  *********/

		/******     Request photo start  *********/
		$('.request_photo_btn').click(function(){
			var user_id_x = $('.user_id').val();
			var search_user_id = $('.search_user_id').val();
			var task = "Request_Photo";

			if(user_id_x=='0' || user_id_x=='' || user_id_x==null)
			{
				window.location.assign('../login.php');
				return false;
			}

			if(user_id_x==search_user_id)
			{
				alert('You are not allowed to request photo for your own profile.');
				return false;
			}

			var data='user_id_x='+user_id_x+'&search_user_id='+search_user_id+'&task='+task;

			$('.request_photo_btn').attr('disabled',true);
			$('.loading_img').show();
			$.ajax({
				type:'post',
            	data:data,
            	url:'query/show_profile_helper.php',
            	success:function(res)
            	{
            		//alert(res);return false;
            		$('.loading_img').hide();
            		if(res=='success')
            		{
            			$('.request_photo_btn').attr('disabled',true);
            			$('.show_profile_status').html("<div class='alert alert-success show_profile_success_status' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-check'></span> <strong> Success! </strong> Request for photo sent successfully.</div>");
            			$('.show_profile_success_status').fadeTo(1000, 500).slideUp(500, function(){
            				location.reload();
                        });
            		}
            		else
            		{
            			$('.request_photo_btn').attr('disabled',false);
            			$('.show_profile_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong>Error!</strong> "+res+"</div>");
						return false;
            		}
            	}
            });

		});
		/******     Request photo  end  *********/


		/***********     Like Profile Start    ***********/
		$('.like-profile').click(function(){
			var userid = $('.userid').val();
			var liked_userid = $('.liked_userid').val();
			var task = "Like_Profile";
			
			if(userid==liked_userid)
			{
				window.location.assign("../login.php");
				return false;
			}

			var data = 'userid='+userid+'&liked_userid='+liked_userid+'&task='+task;
			$('.loading_img_like_profile').show();

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/show_profile_helper.php',
            	success:function(res)
            	{
            		$('.loading_img_like_profile').hide();
            		if(res=='success')
            		{
            			$('.like-profile').attr('disabled',true);
            			$('.like_profile_status').show();
            			$('.like_profile_status').html("<div class='alert alert-success'><strong><i class='fa fa-check'></i></strong> You liked this profile.</div>");
            			return false;
            		}
            		else
            		{
            			$('.like_profile_status').html("<div class='alert alert-danger'><strong><i class='fa fa-times'></i>Error </strong> "+res+"</div>");
            			return false;
            		}
            	}
            });
		});
		/***********     Like Profile End    ***********/
	});
</script>

</body>
</html>