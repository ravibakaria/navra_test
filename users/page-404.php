<?php
	include("templates/header.php");
?>
<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> |  <?php echo getWebsiteTitle(); ?>
	</title>
	<!-- Profile Slider CSS -->
	<link rel="stylesheet" href="../assets/stylesheets/profile_silder.css" />
</head>
<body style="background-color:#fff;">

	<section role="main" class="content-body">
		<header class="page-header">
			<h2>404 - Page Not Found</h2>
		
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="index.php">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>404 - Page Not Found</span></li>
				</ol>
		
				<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
			</div>
		</header>
	<!-- start: page -->
		<section class="body-error error-inside">
			<div class="center-error">

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="main-error mb-xlg">
							<h2 class="error-code text-dark text-center text-semibold m-none">404 <i class="fa fa-file"></i></h2>
							<p class="error-explanation text-center error_404_text">We're sorry, but the page you were looking for doesn't exist.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	<!-- end: page -->
	</section>

</body>
<?php
	include("templates/footer.php");
?>
</html>