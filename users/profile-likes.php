<?php
	include("templates/header.php");
?>
<html>
<head>
	<title>
		<?php
		$a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
		$string = str_replace("-", " ", $a);
		echo $title = ucwords($string);
		?> -  <?php echo getWebsiteTitle(); ?>
	</title>
	<meta name="description" content="<?php echo getWebsiteTitle();?>"/>

<?php
	include("templates/styles_header.php");
	include("templates/sidebar.php");
?>

	<section role="main" class="content-body main-section-start">

		<div class="row start-section-interested-profiles">
			<div class='row'>
				<div class='advanced-search-result-header'>
					<h1>Profile Likes.</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
						<div class="form-group">
							<input class="form-check-input liked_profile" type="radio" name="liked_profile" id="liked_profile1" value="liked_By_Me" checked>

							<label for="liked_profile1" class="control-label profile_info_data">Liked By Me</label>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<input class="form-check-input liked_profile" type="radio" name="liked_profile" id="liked_profile2" value="liked_By_Others">
							<label for="liked_profile2" class="control-label profile_info_data">Liked By Others</label>
						</div>
					</div>
					<input type="hidden" class="user_id_x" value="<?php echo $user_id;?>" />
				</div>
			</div>
			<hr/>
			<div class="row liked-profile-result">
				<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
					<div class="row">
						<div class="result_data">
							
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" style="margin-left: -25px;">
					<div class='skyscrapper-ad'>
			          	<?php
		            		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
			            	if($display_skyscrapper_ad=='1')
			            	{
			              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
			            	}
			          	?>
			        </div>
				</div>
			</div>
			<div class='leaderboard-ad'>
	          	<?php
	            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
	            	if($display_leaderboard_ad=='1')
	            	{
	              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
	            	}
	          	?>
	        </div>
		<!-- end: page -->

		</div>
	</section>
</div>
</body>
<?php
	include("templates/footer.php");
?>

<script>
	$(document).ready(function(){
		var value = $('input[type=radio][name=liked_profile]').val();
		var user_id_x = $('.user_id_x').val();
		var task = "Get_liked_Profile";
		
		var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

		$.ajax({
			type:'post',
        	data:data,
        	url:'query/liked_profiles_helper.php',
        	success:function(res)
        	{
        		$('.result_data').html(res);
        	}
        });

        $('input[type=radio][name=liked_profile]').change(function() {
			var value = $(this).val();
			var user_id_x = $('.user_id_x').val();
			var task = "Get_liked_Profile";
			
			var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'query/liked_profiles_helper.php',
            	success:function(res)
            	{
            		$('.result_data').html(res);
            	}
            });
		});
    });
</script>