<?php
	session_start();
	if(isset($_SESSION['last_page_accessed']))
	{
		unset($_SESSION['last_page_accessed']);
	    unset($_SESSION['membership_plan_id']);
	    unset($_SESSION['featured_listing']);
	    unset($_SESSION['validity']);
	    unset($_SESSION['secret_key']);
	}
	include("layout/header.php"); 
	$siteName = getWebsiteTitle();
	$siteTagline = getWebSiteTagline();
	$site_url = getWebsiteBasePath();
	$site_email = getWebSiteEmail();

	$logo=array();
	$logoURL = getLogoURL();
	if($logoURL!='' || $logoURL!=null)
	{
		$logoURL = explode('/',$logoURL);

		for($i=1;$i<count($logoURL);$i++)
		{
			$logo[] = $logoURL[$i];
		}

		$logo = implode('/',$logo);
	}
?>


<!-- meta info  -->
<title>Search Profiles - <?php echo getWebsiteTitle();?></title>

<meta name="description" content="Matrimonial Profiles of <?= $siteName; ?> members living in desired location. If you are looking for Bride or Groom then we have a perfect match for you. Search matrimonial profiles of people living in desired location having your preferred religion, caste, mother tongue and various education or occupation backgrounds."/>

<meta name="keywords" content="<?php echo $siteName;?>, <?php echo $siteTagline; ?>, Matrimonial profile search by location">

<meta property="og:title" content="<?php echo $siteName.' - '.$siteTagline;?>" />
<meta property="og:url" content="<?= $site_url; ?>" />
<meta property="og:description" content="<?php echo $siteName;?> Profile Search by location helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
<meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
<meta property="twitter:title" content="<?= $siteName.' - '.$siteTagline; ?>" />
<meta property="twitter:url" content="<?= $site_url; ?>" />
<meta property="twitter:description" content="<?php echo $siteName;?> Profile Search by location helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
<meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

<?php
  include("layout/styles.php"); 
  include("layout/menu.php");
?>
<br/><br/><br/>

<?php
	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
	if($display_leaderboard_ad=='1')
	{
?>
		<section class="about clearfix page-header-search">
		    <div class="row">
		    	<div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
		    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
		    			
		    		</div>
		    	</div>
		    	<div class='col-xs-12 col-sm-12 col-md-8 col-lg-8'>
		    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
		    			<div class='leaderboard-ad'>
				          	<?php echo $leaderboard_ad = getRandomLeaderBoardAdData();?>
				        </div>
		    		</div>
		    	</div>
		    	<div class='col-xs-12 col-sm-12 col-md-2 col-lg-2' style="margin-left: -35px;">
		    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
		    			
		    		</div>
		    	</div>
		    </div>
		</section>
<?php
  		
	}
?>


<div class="row">
	<div class="clearfix"><br/></div>
</div>

<section class="about clearfix">
	<div class="row">
    	<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
    			<h1>Search Profiles By Location</h1>
    		</div>
    		<div>
	    		<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
	    			<label>Country</label>:
	    			<select class="form-control form-control-sm country">
	    				<option value="0">--Select Country--</option>
	    				<?php
	    					$sql_country = "SELECT * FROM countries WHERE status='1' ORDER BY name";
	    					$stmt = $link->prepare($sql_country);
	    					$stmt->execute();
	    					$result = $stmt->fetchAll();

	    					foreach ($result as $row) 
	    					{
	    						$id = $row['id'];
	    						$name = $row['name'];

	    						echo "<option value='".$id."'>".$name."</option>";
	    					}
	    				?>
	    			</select>
	    		</div>

	    		<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
	    			<label>State</label>:
	    			<select class="form-control form-control-sm states">
	    				<option value="0">--Select State--</option>
	    			</select>
	    		</div>

	    		<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
	    			<label>City</label>:
	    			<select class="form-control form-control-sm city">
	    				<option value="0">--Select City--</option>
	    			</select>
	    		</div>
	    	</div>
	    	<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
	    		<hr/>
	    	</div>
	    	<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
    			<div class='row'>
    				<div class='col-xs-12 col-sm-12 col-md-10 col-lg-10'>
    					<div class="result_profiles">        <!-- Resultant Profiles  -->
		    				<img src="images/loader/loader.gif" class='img-responsive loading_img centered-loading-image' id='loading_img' alt='loading...' style='width:80px; height:80px; display:none;'/>
		    			</div>
    				</div>
	    			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<div class='skyscrapper-ad'>
				          	<?php
				        		$display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
				            	if($display_skyscrapper_ad=='1')
				            	{
				              		echo $skyscrapper_ad = getRandomSkyScrapperAdData();
				            	}
				          	?>
				        </div>
					</div>      
	    		</div>
	    	</div>
    	</div>
    </div>
	
</section>
 <br/>
<section class="about clearfix">
    <div class="row">
    	<div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
    			
    		</div>
    	</div>
    	<div class='col-xs-12 col-sm-12 col-md-8 col-lg-8'>
    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
    			<div class='leaderboard-ad'>
	    			<?php
		            	$display_leaderboard_ad = getRandomLeaderBoardAdDisplayOrNot();
		            	if($display_leaderboard_ad=='1')
		            	{
		              		echo $leaderboard_ad = getRandomLeaderBoardAdData();
		            	}
		          	?>
		        </div>
    		</div>
    	</div>
    	<div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
    		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
    			
    		</div>
    	</div>
    </div>
</section>
</section>
<?php
  include("layout/footer.php");
?>


<script>
	$(document).ready(function(){
		$('.country').change(function(){
			var country = $(this).val();
			var states = $('.states').val();
			var city = $('.city').val();

			var task="fetch-states-from-country-value";
			var task2="fetch-profiles-result";
			$('.loading_img').show();
			
			$.ajax({
				type:'post',
            	data:'country='+country+'&task='+task,
            	url:'query/sort-location-helper.php',
            	success:function(res)
            	{
            		$('.states').html(res);
            		return false;
            	}
			});

			
			$.ajax({
				type:'post',
            	data:'country='+country+'&states='+states+'&city='+city+'&task2='+task2,
            	url:'query/sort-location-result-helper.php',
            	success:function(result)
            	{
            		$('.loading_img').hide();
            		$('.result_profiles').html(result);
            		return false;
            	}
			});
			
		});

		$('.states').change(function(){
			var states = $(this).val();
			var country = $('.country').val();
			var city = $('.city').val();
			
			var task="fetch-cities-from-states-value";
			var task2="fetch-profiles-result";
			$('.loading_img').show();

			$.ajax({
				type:'post',
            	data:'states='+states+'&task='+task,
            	url:'query/sort-location-helper.php',
            	success:function(res)
            	{
            		$('.city').html(res);
            		return false;
            	}
			});
			
			$.ajax({
				type:'post',
            	data:'country='+country+'&states='+states+'&city='+city+'&task2='+task2,
            	url:'query/sort-location-result-helper.php',
            	success:function(result)
            	{
            		$('.loading_img').hide();
            		$('.result_profiles').html(result);
            		return false;
            	}
			});
			
		});

		$('.city').change(function(){
			var city = $(this).val();
			var country = $('.country').val();
			var states = $('.states').val();

			var task2="fetch-profiles-result";

			$('.loading_img').show();

			$.ajax({
				type:'post',
            	data:'country='+country+'&states='+states+'&city='+city+'&task2='+task2,
            	url:'query/sort-location-result-helper.php',
            	success:function(result)
            	{
            		$('.loading_img').hide();
            		$('.result_profiles').html(result);
            		return false;
            	}
			});
			
		});
	});
</script>

</body>

</html>