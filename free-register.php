<?php
    session_start();
    if(isset($_SESSION['logged_in']) && $_SESSION['client_user']!='')
    {
        header('Location: users/');
        exit;
    }
    else
    if(isset($_SESSION['logged_in']) && $_SESSION['vendor_user']!='')
    {
        header('Location: directory/dashboard.php');
        exit;
    }
    
    if(isset($_SESSION['last_page_accessed']))
    {
        unset($_SESSION['last_page_accessed']);
        unset($_SESSION['membership_plan_id']);
        unset($_SESSION['featured_listing']);
        unset($_SESSION['validity']);
        unset($_SESSION['secret_key']);
    }
    include("layout/header.php");
    include('config/setup-values.php');   //get master setup values
    include('config/email/email_style.php');   //get master setup values
    include('config/email/email_templates.php');   //get master setup values
    include('config/email/email_process.php');

    $today_datetime = date('Y-m-d H:i:s');

    $WebSiteBasePath = getWebsiteBasePath();

    $MinPassLength = getMinimumUserPasswordLength();
    $MaxPassLength = getMaximumUserPasswordLength();

    $terms_of_service_display = getTermsOfServiceSetting();
    $privacy_policy_display = getPrivacyPolicySetting();


    /**********   Form submit status   ***********/
    $recaptchaAllowed = getreCaptchaAllowed();
    if($recaptchaAllowed == "1")
    {
        $recaptchaSiteKey=getreCaptchaSiteKey();
        $recaptchaSecretKey=getreCaptchaSecretKey();
    }

    $errorMessage = null;
    $successMessage = null;

    if($recaptchaAllowed == "1")
    {
        $recaptchaSiteKey=getreCaptchaSiteKey();
        $recaptchaSecretKey=getreCaptchaSecretKey();

        if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
        {
            $secret = $recaptchaSecretKey;
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            
            if($responseData->success)
            {
                if (isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['fname']) && $_POST['fname'] != '' && isset($_POST['lname']) && $_POST['lname'] != '' && isset($_POST['dob']) && $_POST['dob'] != '' && isset($_POST['gender']) && $_POST['gender'] != '' && isset($_POST['password']) && $_POST['password'] != '')
                {
                    $firstname = quote_smart($_POST['fname']);
                    $lname = quote_smart($_POST['lname']);
                    $email = quote_smart($_POST['email']);
                    $country = quote_smart($_POST['country']);
                    $state = quote_smart($_POST['state']);
                    $city = quote_smart($_POST['city']);
                    $phonecode = quote_smart($_POST['country_phone_code']);
                    $dob = quote_smart($_POST['dob']);
                    $gender = quote_smart($_POST['gender']);
                    $phonenumber = quote_smart($_POST['phonenumber']);
                    $password = quote_smart($_POST['password']);
                    $confirm_password = quote_smart($_POST['confirm_password']);
                    
                    $uniquecode = generateRandomString();

                    $MinimumUserPasswordLength = getMinimumUserPasswordLength();
                    $MaximumUserPasswordLength = getMaximumUserPasswordLength();

                    if($MinimumUserPasswordLength=='0' || $MinimumUserPasswordLength=='' || $MinimumUserPasswordLength==null)
                    {
                        $MinimumUserPasswordLength='10';
                    }

                    if($MaximumUserPasswordLength=='0' || $MaximumUserPasswordLength=='' || $MaximumUserPasswordLength==null)
                    {
                        $MaximumUserPasswordLength='100';
                    }

                    $sql_chk_unique_code = $link->prepare("SELECT `unique_code` FROM `clients` WHERE unique_code='$uniquecode'"); 
                    $sql_chk_unique_code->execute();
                    $count_unique_code=$sql_chk_unique_code->rowCount();

                    if($count_unique_code>0)
                    {
                        $uniquecode = generateRandomString();
                    }

                    $passwordx = md5(trim($_POST['password']));

                    $subject = null;
                    $message = null;
                    $mailto = null;
                    $mailtoname = null;
                    $sql_chk = $link->prepare("SELECT `email` FROM `clients` WHERE email='$email' OR  phonenumber='$phonenumber'"); 
                    $sql_chk->execute();
                    $count=$sql_chk->rowCount();
                    //echo "<script>alert(".$count.");</script>";
                    if($count == '0')
                    {
                        
                        $age = (date('Y') - date('Y',strtotime($dob)));

                        if($age<18)
                        {
                            $errorMessage = "Oops? Its seems that you are below 18 year. You cannot proceeed further.";
                        }
                        else
                        {
                            if($password == $confirm_password)
                            {
                                $haveuppercase = preg_match('/[A-Z]/', $password);
                                $havenumeric = preg_match('/[0-9]/', $password);
                                $havespecial = preg_match('/[!@#$%^&)*_(+=}{|:;,.<>}]/', $password);

                                if (!$haveuppercase)
                                {
                                    $errorMessage = 'Password must have atleast one upper case character.';
                                }
                                else if (!$havenumeric)
                                {
                                    $errorMessage = 'Password must have atleast one digit.';
                                }
                                else if (!$havespecial)
                                {
                                    $errorMessage = 'Password must have atleast one of the special characters [!@#$%^&)*_(+=}{|:;,.<>}]';
                                }
                                else if (strlen($password) < $MinimumUserPasswordLength)
                                {
                                    $errorMessage = "Password must be of minimum ".$MinimumUserPasswordLength." characters long.";
                                }
                                else if (strlen($password) > $MaximumUserPasswordLength)
                                {
                                    $errorMessage = "Password must be of maximum ".$MaximumUserPasswordLength." characters long.";
                                }
                                else
                                {
                                    $sql_insert = "INSERT INTO `clients`(`firstname`, `lastname`, `dob`, `gender`,`unique_code`, `email`, `phonecode`, `phonenumber`, `password`, `status`, `created_at`) VALUES ('$firstname','$lname','$dob','$gender','$uniquecode','$email','$phonecode','$phonenumber','$passwordx','1','$today_datetime')";
                                    //$insert_query = $link->exec($sql_insert);
                                    
                                    $sql = "SELECT MAX(id) as id FROM clients";
                                    $stmt   = $link->prepare($sql);
                                    $stmt->execute();
                                    $sql_id = $stmt->fetch();

                                    $passcode = encrypt_decrypt('encrypt', $email);
                                    $verification_link = "<a href='$WebSiteBasePath/verify-email.php?passcode=$passcode'><button>Verify</button></a>";

                                    $verification_text_link = "<a href='$WebSiteBasePath/verify-email.php?passcode=$passcode'>$WebSiteBasePath/verify-email.php?passcode=$passcode</a>";

                                    $id = $sql_id['id']+1;

                                    $sql_insert_location = "INSERT INTO address(userid,city,state,country,created_at) VALUES('$id','$city','$state','$country','$today_datetime')";

                                    $sitetitle = getWebsiteTitle();
                                    $logo_array=array();
                                    $logoURL = getLogoURL();
                                    if($logoURL!='' || $logoURL!=null)
                                    {
                                        $logoURL = explode('/',$logoURL);

                                        for($i=1;$i<count($logoURL);$i++)
                                        {
                                            $logo_array[] = $logoURL[$i];
                                        }

                                        $logo_path = implode('/',$logo_array);
                                    }

                                    if($logoURL!='' || $logoURL!=null)
                                    {
                                        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
                                    }
                                    else
                                    {
                                        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
                                    }

                                    if($link->exec($sql_insert) && $link->exec($sql_insert_location))
                                    {
                                        $SocialSharing = getSocialSharingLinks();   // social sharing links
                                        $primary_color = getPrimaryColor();
                                        $primary_font_color = getPrimaryFontColor();

                                        $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

                                        $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$sitetitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

                                        $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$sitetitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

                                        $EmailVerificationMessage = str_replace(array('$first_name','$site_name','$verification_link','$verification_text_link','$signature'),array($firstname,$sitetitle,$verification_link,$verification_text_link,$GlobalEmailSignature),$EmailVerificationMessage);  //replace footer variables with value

                                        $subject = $EmailVerificationSubject;
                                        $message  = '<!DOCTYPE html>';
                                        $message .= '<html lang="en">
                                            <head>
                                            <meta charset="utf-8">
                                            <meta name="viewport" content="width=device-width">
                                            <title></title>
                                            <style type="text/css">'.$EmailCSS.'</style>
                                            </head>
                                            <body style="margin: 0; padding: 0;">';
                                        //echo $message;exit;
                                        $message .= $EmailGlobalHeader;

                                        $message .= $EmailVerificationMessage;

                                        $message .= $EmailGlobalFooter;

                                        $mailto = $email;
                                        $mailtoname = $firstname;

                                        $registserUserId = getUserIdFromEmail($email);
                                        $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

                                        /*************      Activity log      ***************/
                                        $sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$registserUserId','registred','to system','$IP_Address',now())";
                                        $link->exec($sql_member_log);

                                        /*************      Email log      ***************/
                                        $sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$registserUserId','new registration','email',now())";

                                        if($emailResponse == 'success')
                                        {
                                            $link->exec($sql_member_email_log);
                                            $successMessage = "success";
                                        }
                                        else
                                        {
                                            $errorMessage = "Error in sending verification link";
                                            //$errorMessage = $emailResponse;
                                        }
                                    }
                                    else
                                    {
                                        $errorMessage = "Query execution error. Please try after some time.";
                                    }
                                }
                            }
                        }
                        
                    }
                    else
                    {
                        $errorMessage = "Email id or Mobile number already exists. Please use another email id & mobile number.";
                    }
                }
            }
        }
    }    
    else
    {
        if (isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['fname']) && $_POST['fname'] != '' && isset($_POST['lname']) && $_POST['lname'] != '' && isset($_POST['dob']) && $_POST['dob'] != '' && isset($_POST['gender']) && $_POST['gender'] != '' && isset($_POST['password']) && $_POST['password'] != '')
        {
            $firstname = quote_smart($_POST['fname']);
            $lname = quote_smart($_POST['lname']);
            $email = quote_smart($_POST['email']);
            $country = quote_smart($_POST['country']);
            $state = quote_smart($_POST['state']);
            $city = quote_smart($_POST['city']);
            $phonecode = quote_smart($_POST['country_phone_code']);
            $dob = quote_smart($_POST['dob']);
            $gender = quote_smart($_POST['gender']);
            $phonenumber = quote_smart($_POST['phonenumber']);
            $password = quote_smart($_POST['password']);
            $confirm_password = quote_smart($_POST['confirm_password']);
            
            $uniquecode = generateRandomString();

            $MinimumUserPasswordLength = getMinimumUserPasswordLength();
            $MaximumUserPasswordLength = getMaximumUserPasswordLength();

            if($MinimumUserPasswordLength=='0' || $MinimumUserPasswordLength=='' || $MinimumUserPasswordLength==null)
            {
                $MinimumUserPasswordLength='10';
            }

            if($MaximumUserPasswordLength=='0' || $MaximumUserPasswordLength=='' || $MaximumUserPasswordLength==null)
            {
                $MaximumUserPasswordLength='100';
            }

            $sql_chk_unique_code = $link->prepare("SELECT `unique_code` FROM `clients` WHERE unique_code='$uniquecode'"); 
            $sql_chk_unique_code->execute();
            $count_unique_code=$sql_chk_unique_code->rowCount();

            if($count_unique_code>0)
            {
                $uniquecode = generateRandomString();
            }

            $passwordx = md5(trim($_POST['password']));

            $subject = null;
            $message = null;
            $mailto = null;
            $mailtoname = null;
            $sql_chk = $link->prepare("SELECT `email` FROM `clients` WHERE email='$email' OR  phonenumber='$phonenumber'"); 
            $sql_chk->execute();
            $count=$sql_chk->rowCount();
            //echo "<script>alert(".$count.");</script>";
            if($count == '0')
            {
                
                $age = (date('Y') - date('Y',strtotime($dob)));

                if($age<18)
                {
                    $errorMessage = "Oops? Its seems that you are below 18 year. You cannot proceeed further.";
                }
                else
                {
                    if($password == $confirm_password)
                    {
                        $haveuppercase = preg_match('/[A-Z]/', $password);
                        $havenumeric = preg_match('/[0-9]/', $password);
                        $havespecial = preg_match('/[!@#$%^&)*_(+=}{|:;,.<>}]/', $password);

                        if (!$haveuppercase)
                        {
                            $errorMessage = 'Password must have atleast one upper case character.';
                        }
                        else if (!$havenumeric)
                        {
                            $errorMessage = 'Password must have atleast one digit.';
                        }
                        else if (!$havespecial)
                        {
                            $errorMessage = 'Password must have atleast one of the special characters [!@#$%^&)*_(+=}{|:;,.<>}]';
                        }
                        else if (strlen($password) < $MinimumUserPasswordLength)
                        {
                            $errorMessage = "Password must be of minimum ".$MinimumUserPasswordLength." characters long.";
                        }
                        else if (strlen($password) > $MaximumUserPasswordLength)
                        {
                            $errorMessage = "Password must be of maximum ".$MaximumUserPasswordLength." characters long.";
                        }
                        else
                        {
                            $sql_insert = "INSERT INTO `clients`(`firstname`, `lastname`, `dob`, `gender`,`unique_code`, `email`, `phonecode`, `phonenumber`, `password`, `status`, `created_at`) VALUES ('$firstname','$lname','$dob','$gender','$uniquecode','$email','$phonecode','$phonenumber','$passwordx','1','$today_datetime')";
                            //$insert_query = $link->exec($sql_insert);
                            
                            $sql = "SELECT MAX(id) as id FROM clients";
                            $stmt   = $link->prepare($sql);
                            $stmt->execute();
                            $sql_id = $stmt->fetch();

                            $passcode = encrypt_decrypt('encrypt', $email);
                            $verification_link = "<a href='$WebSiteBasePath/verify-email.php?passcode=$passcode'><button>Verify</button></a>";

                            $verification_text_link = "<a href='$WebSiteBasePath/verify-email.php?passcode=$passcode'>$WebSiteBasePath/verify-email.php?passcode=$passcode</a>";

                            $id = $sql_id['id']+1;

                            $sql_insert_location = "INSERT INTO address(userid,city,state,country,created_at) VALUES('$id','$city','$state','$country','$today_datetime')";

                            $sitetitle = getWebsiteTitle();
                            $logo_array=array();
                            $logoURL = getLogoURL();
                            if($logoURL!='' || $logoURL!=null)
                            {
                                $logoURL = explode('/',$logoURL);

                                for($i=1;$i<count($logoURL);$i++)
                                {
                                    $logo_array[] = $logoURL[$i];
                                }

                                $logo_path = implode('/',$logo_array);
                            }

                            if($logoURL!='' || $logoURL!=null)
                            {
                                $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
                            }
                            else
                            {
                                $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
                            }

                            if($link->exec($sql_insert) && $link->exec($sql_insert_location))
                            {
                                $SocialSharing = getSocialSharingLinks();   // social sharing links
                                $primary_color = getPrimaryColor();
                                $primary_font_color = getPrimaryFontColor();

                                $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

                                $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$sitetitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

                                $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$sitetitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

                                $EmailVerificationMessage = str_replace(array('$first_name','$site_name','$verification_link','$verification_text_link','$signature'),array($firstname,$sitetitle,$verification_link,$verification_text_link,$GlobalEmailSignature),$EmailVerificationMessage);  //replace footer variables with value

                                $subject = $EmailVerificationSubject;
                                $message  = '<!DOCTYPE html>';
                                $message .= '<html lang="en">
                                    <head>
                                    <meta charset="utf-8">
                                    <meta name="viewport" content="width=device-width">
                                    <title></title>
                                    <style type="text/css">'.$EmailCSS.'</style>
                                    </head>
                                    <body style="margin: 0; padding: 0;">';
                                //echo $message;exit;
                                $message .= $EmailGlobalHeader;

                                $message .= $EmailVerificationMessage;

                                $message .= $EmailGlobalFooter;

                                $mailto = $email;
                                $mailtoname = $firstname;

                                $registserUserId = getUserIdFromEmail($email);
                                $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

                                /*************      Activity log      ***************/
                                $sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$registserUserId','registred','to system','$IP_Address',now())";
                                $link->exec($sql_member_log);

                                /*************      Email log      ***************/
                                $sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$registserUserId','new registration','email',now())";

                                if($emailResponse == 'success')
                                {
                                    $link->exec($sql_member_email_log);
                                    $successMessage = "success";
                                }
                                else
                                {
                                    $errorMessage = "Error in sending verification link";
                                    //$errorMessage = $emailResponse;
                                }
                            }
                            else
                            {
                                $errorMessage = "Query execution error. Please try after some time.";
                            }
                        }
                    }
                }
                
            }
            else
            {
                $errorMessage = "Email id or Mobile number already exists. Please use another email id & mobile number.";
            }
        }
    }
?>

    <!-- meta info  -->

    <title>
        <?php
        $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
        $string = str_replace("-", " ", $a);
        echo $title = ucwords($string);
        ?>  -  <?php echo getWebsiteTitle(); ?>
    </title>

    <meta name="title" content="<?= $site_title.' - Registration'; ?>" />

    <meta name="description" content="To find Verified Profiles, Register Free! If you are Looking For Groom or Bride – we have a perfect match for you."/>

    <meta name="keywords" content="<?php echo $site_title;?>, <?php echo $site_tagline;?>, matrimonials, matrimony, marriage, marriage sites, matchmaking" />

    <meta property="og:title" content="<?php echo $site_title.' - Registration'; ?>" />
    <meta property="og:url" content="<?= $site_url; ?>" />
    <meta property="og:description" content="<?php echo $site_tagline;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="og:image" content="<?= $site_url.'/'.$logo;?>" />
    <meta property="twitter:title" content="<?= $site_title.' - Registration'; ?>" />
    <meta property="twitter:url" content="<?= $site_url; ?>" />
    <meta property="twitter:description" content="<?php echo $site_title;?> Profile Search helps to find your desired life partner. You are Looking For Groom or Bride – we have a perfect match for you. Search matrimonial profiles of members. To find Verified Profiles, Register Free!" />
    <meta property="twitter:image" content="<?= $site_url.'/'.$logo;?>" />

    <script src="js/jquery.js"></script>
    
    <script>
        function onSubmit(token)
        {
            document.getElementById("frmLogin").submit();
        }

        function validate(event) 
        {
            event.preventDefault();
            var fname = $('.fname').val();
            var lname = $('.lname').val();
            var email = $('.email').val();
            var country = $('.country').val();
            var state = $('.state').val();
            var city = $('.city').val();
            var dob = $('.dob').val();
            var gender = $('.gender').val();
            var country_phone_code = $('.country_phone_code').val();
            var phonenumber = $('.phonenumber').val();
            var password = $('.password').val();
            var confirm_password = $('.confirm_password').val();
            var agree_check = $('#agree_check').is(':checked');
            var MinPassLength = $('.MinPassLength').val();
            var MaxPassLength = $('.MaxPassLength').val();
            var recaptchaAllowed = $('.recaptchaAllowed').val();
            var empty_success=0;            

            if(MinPassLength=='0' || MinPassLength=='' || MinPassLength==null)
            {
                MinPassLength='10';
            }

            if(MaxPassLength=='0' || MaxPassLength=='' || MaxPassLength==null)
            {
                MaxPassLength='100';
            }

            /* First name Validation */
            if(fname=='' || fname==null)
            {
                $('.fname').addClass('danger_error');
                empty_success=empty_success+1;
            }


            /* Last name Validation */
            if(lname=='' || lname==null)
            {
                $('.lname').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Date of Birth validation */
            if(dob=='' || dob==null)
            {
                $('.dob').addClass('danger_error');
                empty_success=empty_success+1;
            }
            /* Country validation */
            if(country=='0' || country=='' || country==null)
            {
                $('.country').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* State validation */
            if(state=='0' || state=='' || state==null)
            {
                $('.state').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* city validation */
            if(city=='0' || city=='' || city==null)
            {
                $('.city').addClass('danger_error');
                empty_success=empty_success+1;
            }


            /* Email Validation */
            if(email=='' || email==null)
            {
                $('.email').addClass('danger_error');
                empty_success=empty_success+1;
            }

            function validateEmail($email) {
                var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return emailReg.test( $email );
            }

            if(email!='' && !validateEmail(email)) 
            { 
                $('.email').focus();
                $('.email_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Enter valid Email.</div>");
                $('.email').addClass('danger_error');
                empty_success=empty_success+1;
            }


            /* Gender validation */
            if(gender=='0')
            {
                $('.gender').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Phone number validation */
            if(phonenumber=='' || phonenumber==null)
            {
                $('.phonenumber').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(phonenumber!='' && phonenumber.length<4)
            {
                $('.phonenumber').focus();
                $('.phonenumber_error').show();
                $('.phonenumber_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Mobile number should be of minimum 4 digits!</div>");
                $('.phonenumber').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Password validation */
            if(password=='' || password==null)
            {
                $('.password').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(password!='' && password.length < MinPassLength)
            {
                $('.password').focus();
                $('.password_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Password should be of minimum "+MinPassLength+" characters long!</div>");
                $('.password').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(password!='' && password.length > MaxPassLength)
            {
                $('.password').focus();
                $('.password_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Password length not exceed more than "+MaxPassLength+" !</div>");
                $('.password').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Confirm Password validation */
            if(confirm_password=='' || confirm_password==null)
            {
                $('.confirm_password').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(confirm_password!='' && confirm_password != password)
            {
                $('.confirm_password').focus();
                $('.confirm_password_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Password & Confirm password not same!</div>");
                $('.confirm_password').addClass('danger_error');
                empty_success=empty_success+1;
            }

            /* Agreement check validation */
            if(agree_check==false)
            {
                $('.agree_check_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Emty: </strong> Please check the terms & condtions.</div>");
                $('#agree_check').addClass('danger_error');
                empty_success=empty_success+1;
            }

            if(empty_success>0)
            {
                $('.server_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong> Empty! </strong> Please enter all required field & then click on register button.</div>");
                return false;
            }

            //recaptcha validation 
            if(recaptchaAllowed=='1')
            {
                grecaptcha.execute();
            }
            else
            {
                document.getElementById("frmLogin").submit();
            }

            $('.loading_img').show();
            $('.server_data_status').html("");
            $('.error_status').html("");
            $('.agree_check_error').html("");
        }

        function onload() 
        {
            var element = document.getElementById('recaptcha-submit');
            element.onclick = validate;
        }
    </script>

<?php
  include("layout/styles.php"); 
  include("layout/menu.php"); 
?>

<!-- Google recaptcha -->
<?php
    if($recaptchaAllowed == "1")
    {
        echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
    }
?>

<section>
  <br/><br/><br/><br/><br/><br/>
</section>

<div class="row free-register-panel common-row-div">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="row">
                <section class="panel register-panel">
                    <header class="panel-heading register-panel-heading">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 centered-data">
                                    <h1>Register Now!</h1>
                                    <h2>*All fields are manadatory</h2>
                            </div>
                        </div>
                    </header>
                    <form name="frmLogin" id="frmLogin" autocomplete="off" method="post"  action="<?php echo $_SERVER['PHP_SELF'];?>">
                        <div class="panel-body register-panel-body">
                            <input type="hidden" class="MinPassLength" value="<?php echo $MinPassLength;?>">
                            <input type="hidden" class="MaxPassLength" value="<?php echo $MaxPassLength;?>">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">First Name</label>
                                        <input type="text" name="fname" id="fname" class="form-control fname" placeholder="Ex: John" value="<?php echo @$firstname;?>">
                                    </div>
                                    <div class="fname_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" name="lname" id="lname" class="form-control lname" placeholder="Ex: Smith" value="<?php echo @$lname;?>">
                                    </div>
                                    <div class="lname_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Date of Birth</label>
                                        <input type="text" name="dob" id="dob" class="form-control dob" placeholder="YYYY-MM-DD" value="<?php echo @$dob;?>">
                                    </div>
                                    <div class="dob_error"></div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Country</label>
                                        <select name="country" class="form-control country">
                                            <option value="0">Select Country</option>
                                            <?php
                                                $query  = "SELECT * FROM `countries` ORDER BY `name` ASC";
                                                $stmt   = $link->prepare($query);
                                                $stmt->execute();
                                                $result = $stmt->fetchAll();
                                                foreach( $result as $row )
                                                {
                                                    $country_name =  $row['name'];
                                                    $country_id = $row['id']; 

                                                    if(isset($country))
                                                    {
                                                        if($country==$country_id)
                                                        {
                                                            echo "<option value=".$country_id." selected>".$country_name."</option>";
                                                        }
                                                        else
                                                        {
                                                            echo "<option value=".$country_id.">".$country_name."</option>";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        echo "<option value=".$country_id.">".$country_name."</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="country_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">State</label>
                                        <select name="state" class="form-control state">
                                            <?php
                                                if(isset($state))
                                                {
                                                    $sql = "SELECT * FROM states WHERE country_id='$country'";
                                                    $stmt = $link->prepare($sql);
                                                    $stmt->execute();
                                                    $result = $stmt->fetchAll();

                                                    foreach ($result as $row) 
                                                    {
                                                        $state_id = $row['id'];
                                                        $state_name = $row['name'];
                                                        if($state==$state_id)
                                                        {
                                                            echo "<option value='".$state_id."' selected>".$state_name."</option>";
                                                        }
                                                        else
                                                        {
                                                            echo "<option value='".$state_id."'>".$state_name."</option>";
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="state_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <select name="city" class="form-control city">
                                            <?php
                                                if(isset($city))
                                                {
                                                    $sql = "SELECT * FROM cities WHERE state_id='$state'";
                                                    $stmt = $link->prepare($sql);
                                                    $stmt->execute();
                                                    $result = $stmt->fetchAll();

                                                    foreach ($result as $row) 
                                                    {
                                                        $city_id = $row['id'];
                                                        $city_name = $row['name'];
                                                        if($city==$city_id)
                                                        {
                                                            echo "<option value='".$city_id."' selected>".$city_name."</option>";
                                                        }
                                                        else
                                                        {
                                                            echo "<option value='".$city_id."'>".$city_name."</option>";
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="city_error"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Email Id</label>
                                        <input type="text" name="email" id="email" class="form-control email" placeholder="Ex: John@gmail.com" value="<?php echo @$email;?>">
                                    </div>
                                    <div class="email_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Gender</label>
                                        <select class="form-control gender" id="gender" name="gender">
                                            <option value="0">Select Gender</option>
                                            <?php
                                                $sql = "SELECT * FROM gender WHERE status='1'";
                                                $stmt = $link->prepare($sql);
                                                $stmt->execute();
                                                $result = $stmt->fetchAll();
                                                foreach ($result as $row) 
                                                {
                                                    $gender_id = $row['id'];
                                                    $gender_name = $row['name'];

                                                    if(isset($gender))
                                                    {
                                                        if($gender==$gender_id)
                                                        {
                                                            echo "<option value=".$gender_id." selected>".$gender_name."</option>";
                                                        }
                                                        else
                                                        {
                                                            echo "<option value=".$gender_id.">".$gender_name."</option>";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        echo "<option value=".$gender_id.">".$gender_name."</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="gender_error"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Mobile Number</label>
                                        <div class="input-group">
                                            <span class="input-group-addon country_code">
                                                <?php
                                                    echo @$phonecode;
                                                ?>
                                            </span>
                                            <input type="text" name="phonenumber" class="form-control phonenumber" placeholder="Mobile Number"  value="<?php echo @$phonenumber;?>" maxlength="20">
                                        </div>
                                        <input type="hidden"  name="country_phone_code" class="country_phone_code"  value="<?php echo @$phonecode;?>"/>
                                        <div class="phonenumber_error"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <p class="password-notification">
                                            <b>Instructions to set password:</b><br/>
                                            Password should contain atleast one uppercase character.<br/>
                                            Password should contain atleast one digit.<br/>
                                            Password should contain atleast one special character from !@#$%^*()_=+{}|;:,&lt;.><br/>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label">Password</label>
                                                <input type="password" name="password" class="form-control password" id="inputPassword" placeholder="Password" value="<?php echo @$password;?>">
                                            </div>
                                            <div class="password_error"></div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label">Confirm Password</label>
                                                <input type="password" class="form-control confirm_password" name="confirm_password" id="inputPasswordConfirm" placeholder="Confirm Password" value="<?php echo @$password;?>">
                                            </div>
                                            <div class="confirm_password_error"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <span class="button-checkbox">
                                            <input type="checkbox" class="form-inline agree_check" name="agree_check" id="agree_check" <?php if(isset($agree_check) && $agree_check=='1') { echo 'checked'; } ?>> &nbsp;&nbsp;<label for="agree_check" class="control-label">I agree</label>
                                        </span>
                                
                                        to the 

                                        <?php
                                            if($terms_of_service_display=='1')
                                            {
                                                echo "<a href='$websiteBasePath/terms-of-service.html' target='_blank'>Terms Of Services</a>";
                                            }
                                            else
                                            {
                                                echo "Terms Of Services";
                                            }

                                            echo " & ";

                                            if($privacy_policy_display=='1')
                                            {
                                                echo "<a href='$websiteBasePath/privacy-policy.html' target='_blank'>Privacy Policy.</a>";
                                            }
                                            else
                                            {
                                                echo "Privacy Policy.";
                                            }
                                        ?>
                                    </div>
                                    <div class="agree_check_error"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php
                                    if ($errorMessage != '' || $errorMessage != null)
                                    {
                                        echo "<div class='alert alert-danger col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
                                        echo "<strong>Error!</strong> $errorMessage";
                                        echo "</div>";
                                    }

                                    if ($successMessage != '' || $successMessage != null)
                                    {
                                        echo "<div class='alert alert-success col-sm-12 success_status' style='padding: 10px; margin-bottom: 10px;'>";
                                        echo "<strong><i class='fa fa-check'></i> Success! </strong>Registration completed successfully. Please check Your email inbox/spam folder for verification email.";
                                        echo "</div>";
                                        
                                        echo "<script>
                                                $('.success_status').fadeTo(2000, 500).slideUp(500, function(){
                                                $('.success_status').slideUp(200);
                                                window.location.assign('login.php');
                                            });
                                        </script>";
                                    }
                                ?>
                            </div>

                            <input type="hidden" class="recaptchaAllowed" value="<?php echo $recaptchaAllowed;?>">

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 centered-data">
                                            <img src="images/loader/loader.gif" class='img-responsive loading_img centered-loading-image' id='loading_img' alt='loader' style='width:60px; height:60px; display:none;'/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 server_data_status">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer register-panel-footer centered-data">
                            <input type="submit" name="login btn"  class="btn btn-primary btn_register website-button" id="recaptcha-submit" value="Register">
                        </footer>
                        <?php
                            if($recaptchaAllowed == "1")
                            {
                                echo "<div class='form-group'>
                                    <div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
                                </div>";
                            }
                        ?>
                        
                        <script>onload();</script>

                    </form>
                </section>
            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 free-registration-ad">
            <div class='skyscrapper-ad'>
                <?php
                    $display_skyscrapper_ad = getRandomSkyscrapperAdDisplayOrNot();
                    if($display_skyscrapper_ad=='1')
                    {
                        echo $skyscrapper_ad = getRandomSkyScrapperAdData();
                    }
                ?>
            </div>
        </div>
    </div>
</div>

</section>
<?php
    include("layout/footer.php");
?>

<?php
    $sql_AddThisScript = "SELECT social_sharing_display,social_sharing_data FROM `social_sharing`";
    $stmt= $link->prepare($sql_AddThisScript); 
    $stmt->execute();
    $count=$stmt->rowCount();
    $result = $stmt->fetch();
    $social_sharing_display = $result['social_sharing_display'];
    $social_sharing_data = $result['social_sharing_data'];

    if($social_sharing_display=='1')
    {
        if($social_sharing_data!='' || $social_sharing_data!=null)
        {
            $social_sharing_data = file_get_contents($websiteBasePath.'/'.$social_sharing_data);
            echo "$social_sharing_data";
        }
    }
?>

<script>
    $(document).ready(function(){

        /*  Datepicker setting  */
        var date_input=$('input[name="dob"]');
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        
        date_input.datepicker({
          format: 'yyyy-mm-dd',
          container: container,
          todayHighlight: true,
          autoclose: true,
          orientation: "auto top",
        });

        /* empty error message  */
        $('.fname, .lname, .email, .dob, .gender, .phonenumber, .password, .confirm_password, #agree_check, .country, .state, .city').keypress(function(){
            $('.fname_error, .lname_error, .email_error, .dob_error, .gender_error, .phonenumber_error, .password_error, .confirm_password_error, .agree_check_error, .country_error, .state_error, .city_error').html("");
        });

        /* empty error message  */
        $('.fname, .lname, .email, .dob, .gender, .phonenumber, .password, .confirm_password, #agree_check, .country, .state, .city').click(function(){
            $('.fname_error, .lname_error, .email_error, .dob_error, .gender_error, .phonenumber_error, .password_error, .confirm_password_error, .agree_check_error, .country_error, .state_error, .city_error').html("");
        });

        /* empty danger class message  */
        $('.fname').click(function(){
            $('.fname').removeClass("danger_error");
        });

        $('.lname').click(function(){
            $('.lname').removeClass("danger_error");
        });

        $('.email').click(function(){
            $('.email').removeClass("danger_error");
        });

        $('.dob').click(function(){
            $('.dob').removeClass("danger_error");
        });

        $('.gender').click(function(){
            $('.gender').removeClass("danger_error");
        });

        $('.phonenumber').click(function(){
            $('.phonenumber').removeClass("danger_error");
        });

        $('.password').click(function(){
            $('.password').removeClass("danger_error");
        });

        $('.confirm_password').click(function(){
            $('.confirm_password').removeClass("danger_error");
        });

        $('#agree_check').click(function(){
            $('#agree_check').removeClass("danger_error");
        });

        $('.country').click(function(){
            $('.country').removeClass("danger_error");
        });

        $('.state').click(function(){
            $('.state').removeClass("danger_error");
        });

        $('.city').click(function(){
            $('.city').removeClass("danger_error");
        });

        /*   Prevent entering charaters in mobile & phone number   */
        $(".phonenumber").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
            {
                //display error message
                $('.phonenumber_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Albhabets not allowed. Enter Digits only.</div>").show().fadeOut(3000);
                return false;
            }
        })


        $('.country').change(function(){         //Fetch states
            var country_id = $(this).val();
            var task = "Fetch_state_data";  

            $.ajax({
                type:'post',
                data:'country_id='+country_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    var result = $.parseJSON(res);
                    $('.state').html(result[0]);
                    $('.country_code').html("+"+result[1]);
                    $('.country_phone_code').val(result[1]);
                }
            });         
        });

        $('.state').change(function(){         //Fetch cities
            var state_id = $(this).val();
            var task = "Fetch_city_data";  
            $.ajax({
                type:'post',
                data:'state_id='+state_id+'&task='+task,
                url:'query/fetch-info-helper.php',
                success:function(res)
                {
                    $('.city').html(res);
                }
            });         
        });
    });
</script>

</body>
</html>