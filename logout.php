<?php
	session_start();
	require_once 'config/config.php'; 
	include('config/dbconnect.php');    //database connection
	include('config/functions.php');   //strip query string
    include('config/setup-values.php');   //get master setup values
	
	$userid = $_SESSION['user_id'];
	$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$userid','logged','out','$IP_Address',now())";
	$link->exec($sql_member_log);
	
	if(isset($_SESSION['logged_in']))
	{
		unset($_SESSION['logged_in']);
	}

	if(isset($_SESSION['client_user']))
	{
		unset($_SESSION['client_user']);
	}

	if(isset($_SESSION['user_id']))
	{
		unset($_SESSION['user_id']);
	}

	if(isset($_SESSION['user_email']))
	{
		unset($_SESSION['user_email']);
	}

	if(isset($_SESSION['search_data']))
	{
		unset($_SESSION['search_data']);
	}

	if(isset($_SESSION['CHANNEL_ID']))
	{
		unset($_SESSION['CHANNEL_ID']);
	}

	session_destroy();
	header('Location:index.php');
	exit;

?>