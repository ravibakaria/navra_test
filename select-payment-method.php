<?php
  session_start();

  include("layout/header.php");      // Header

  $WebsiteBasePath = getWebsiteBasePath();
  $DefaultCurrency = getDefaultCurrency();
  $DefaultCurrencyCode = getDefaultCurrencyCode($DefaultCurrency);

  $user_name = $_POST['user_name'];
  $email = $_POST['email'];
  $phone = $_POST['phone'];
  $product_name = strtoupper($_POST['product_name']);
  $amount = $_POST['amount'];
?>

<title>
    <?php
    $a = pathinfo(basename($_SERVER['SCRIPT_NAME']), PATHINFO_FILENAME);
    $string = str_replace("-", " ", $a);
    echo $title = ucwords($string);
    ?>  -  <?php echo getWebsiteTitle(); ?>
</title>
<style>
  

</style>
<?php
  include("layout/styles.php"); 
  include("layout/menu.php"); 
?>
<br><br><br><br><br><br>

<div class="container">
  
    <h1 style="background-color:#f0f0f0;padding:10px;">Select your payment method.</h1>

    <p class="select-payment-header-para"><b>Total Amount Payable: <?php echo $DefaultCurrencyCode.":".$amount; ?></b></p>
  
<br>
  <form method="post" action="payment-method-redirect.php">
    <div class="row">

      <?php

        /************   Offline    ***************/
        $is_offline_enable = getIsOfflineEnable();

        if($is_offline_enable=='1')
        {
          $offline_display_name = getOfflineDisplayName();

          echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3 payment-card' id='1'>";
            echo "<div class='payment-method-card' style='padding-left:10px;padding-right:10px;'>
                    <br>
                    <center>
                    <img src='$WebsiteBasePath/images/payment-gateways/offline-payment.png' alt='Offline Payment' class='img img-responsive payment-logo' style='height: 100px;'>
                    
                      <h4><b>$offline_display_name</b></h4>
                      <input type='radio' name='payment_method' class='payment_method_1' value='1'>
                    </center>
                    <br>
                  </div>";
          echo "</div>";
        }

        /************   Bank Transfer    ***************/
        $is_bank_transfer_enable = getIsBankTransferEnable();

        if($is_bank_transfer_enable=='1')
        {
          $bank_transfer_display_name = getBankTransferDisplayName();

          echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3 payment-card' id='2'>";
            echo "<div class='payment-method-card' style='padding-left:10px;padding-right:10px;'>
                    <br>
                    <center>
                    <img src='$WebsiteBasePath/images/payment-gateways/bank-transfer.png' alt='Offline Payment' class='img img-responsive payment-logo' style='height: 100px;'>
                    
                      <h4><b>$bank_transfer_display_name</b></h4>
                      <input type='radio' name='payment_method' class='payment_method_2' value='2'>
                    </center>
                    <br>
                  </div>";
          echo "</div>";
        }


        /************   Instamojo    ***************/
        $is_instamojo_enable = getIsInstamojoEnable();

        if($is_instamojo_enable=='1')
        {
          $instamojo_display_name = getIsInstamojoDisplayName();

          echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3 payment-card' id='3'>";
            echo "<div class='payment-method-card' style='padding-left:10px;padding-right:10px;'>
                    <br>
                    <center>
                    <img src='$WebsiteBasePath/images/payment-gateways/instamojo.png' alt='Pay Using Instamojo' class='img img-responsive payment-logo' style='height: 100px;'>
                    
                      <h4><b>$instamojo_display_name</b></h4>
                      <input type='radio' name='payment_method' class='payment_method_3' value='3'>
                    </center>
                    <br>
                  </div>";
          echo "</div>";
        }

        /************   Paytm    ***************/
        $is_paytm_enable = getIsPaytmEnable();

        if($is_paytm_enable=='1')
        {
          $paytm_display_name = getPaytmDisplayName();

          echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3 payment-card' id='4'>";
            echo "<div class='payment-method-card' style='padding-left:10px;padding-right:10px;'>
                    <br>
                    <center>
                    <img src='$WebsiteBasePath/images/payment-gateways/paytm.png' alt='Pay Using Paytm' class='img img-responsive payment-logo' style='height: 100px;'>
                    
                      <h4><b>$paytm_display_name</b></h4>
                      <input type='radio' name='payment_method' class='payment_method_4' value='4'>
                    </center>
                    <br>
                  </div>";
          echo "</div>";
        }

        /************   Pay U Money    ***************/
        $is_payumoney_enable = getIsPayUmoneyEnable();

        if($is_payumoney_enable=='1')
        {
          $payumoney_display_name = getPayUmoneyDisplayName();

          echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3 payment-card' id='5'>";
            echo "<div class='payment-method-card' style='padding-left:10px;padding-right:10px;'>
                    <br>
                    <center>
                    <img src='$WebsiteBasePath/images/payment-gateways/payumoney.png' alt='Pay Using PayU Money' class='img img-responsive payment-logo' style='height: 100px;'>
                    
                      <h4><b>$payumoney_display_name</b></h4>
                      <input type='radio' name='payment_method' class='payment_method_5' value='5'>
                    </center>
                    <br>
                  </div>";
          echo "</div>";
        }

        /************   Razorpay    ***************/
        $is_razorpay_enable = getIsRazorpayEnable();

        if($is_razorpay_enable=='1')
        {
          $razorpay_display_name = getRazorpayDisplayName();

          echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3 payment-card' id='6'>";
            echo "<div class='payment-method-card' style='padding-left:10px;padding-right:10px;'>
                    <br>
                    <center>
                    <img src='$WebsiteBasePath/images/payment-gateways/razorpay.png' alt='Pay Using Razorpay' class='img img-responsive payment-logo' style='height: 100px;'>
                    
                      <h4><b>$razorpay_display_name</b></h4>
                      <input type='radio' name='payment_method' class='payment_method_6' value='6'>
                    </center>
                    <br>
                  </div>";
          echo "</div>";
        }

        /************   Paypal    ***************/
        $is_paypal_enable = getIsPaypalEnable();

        if($is_paypal_enable=='1')
        {
          $paypal_display_name = getPaypalDisplayName();

          echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3 payment-card' id='7'>";
            echo "<div class='payment-method-card' style='padding-left:10px;padding-right:10px;'>
                    <br>
                    <center>
                    <img src='$WebsiteBasePath/images/payment-gateways/paypal.png' alt='Pay Using Paypal' class='img img-responsive payment-logo' style='height: 100px;'>
                    
                      <h4><b>$paypal_display_name</b></h4>
                      <input type='radio' name='payment_method' class='payment_method_7' value='7'>
                    </center>
                    <br>
                  </div>";
          echo "</div>";
        }

        /************   2Checkout    ***************/
        $co_is_enable = getIs2CheckoutEnable();

        if($co_is_enable=='1')
        {
          $co_display_name = get2CheckoutDisplayName();

          echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3 payment-card' id='8'>";
            echo "<div class='payment-method-card' style='padding-left:10px;padding-right:10px;'>
                    <br>
                    <center>
                    <img src='$WebsiteBasePath/images/payment-gateways/2checkout.png' alt='Pay Using 2Checkout' class='img img-responsive payment-logo' style='height: 100px;'>
                    
                      <h4><b>$co_display_name</b></h4>
                      <input type='radio' name='payment_method' class='payment_method_8' value='8'>
                    </center>
                    <br>
                  </div>";
          echo "</div>";
        }

        /************   Stripe    ***************/
        $sp_is_enable = getIsStripeEnable();

        if($sp_is_enable=='1' && $amount>=100)
        {
          $sp_display_name = getStripeDisplayName();

          echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3 payment-card' id='9'>";
            echo "<div class='payment-method-card' style='padding-left:10px;padding-right:10px;'>
                    <br>
                    <center>
                    <img src='$WebsiteBasePath/images/payment-gateways/Stripe.jpg' alt='Pay Using Stripe' class='img img-responsive payment-logo' style='height: 100px;'>
                    
                      <h4><b>$sp_display_name</b></h4>
                      <input type='radio' name='payment_method' class='payment_method_9' value='9'>
                    </center>
                    <br>
                  </div>";
          echo "</div>";
        }

        /************   CCAvenue    ***************/
        $cc_is_enable = getIsCCAvenueEnable();

        if($cc_is_enable=='1')
        {
          $cc_display_name = getCCAvenueDisplayName();

          echo "<div class='col-xs-12 col-sm-12 col-md-3 col-lg-3 payment-card' id='10'>";
            echo "<div class='payment-method-card' style='padding-left:10px;padding-right:10px;'>
                    <br>
                    <center>
                    <img src='$WebsiteBasePath/images/payment-gateways/CCAvenue.jpg' alt='Pay Using CCAvenue' class='img img-responsive payment-logo' style='height: 100px;'>
                    
                      <h4><b>$cc_display_name</b></h4>
                      <input type='radio' name='payment_method' class='payment_method_10' value='10'>
                    </center>
                    <br>
                  </div>";
          echo "</div>";
        }
      ?>
    </div>
    <br>
    <input type="hidden" name="user_name" value="<?php echo $user_name;?>">
    <input type="hidden" name="email" value="<?php echo $email;?>">
    <input type="hidden" name="phone" value="<?php echo $phone;?>">
    <input type="hidden" name="product_name" value="<?php echo $product_name;?>">
    <input type="hidden" name="amount" value="<?php echo $amount;?>">

    <div class="row">
      <center>
        <img src="images/loader/loader.gif" class='img-responsive loading_img loading_img_checkout' id='loading_img' style='width:80px; height:80px; display:none;'/>
      </center>
      <div class="checkout_status"></div>
      <button type="submit" class="btn btn-lg pull-right btn_checkout website-button">Checkout</button>
    </div>
  </form>

</div>

  <br/><br/>
</section>
<?php
  include "layout/footer.php";
?>

<script>
  $(document).ready(function(){
    
    $('.payment-card').click(function(){
      var radio_id = $(this).attr('id');
      if($('.'+radio_id).prop('checked', false))
      {
        $('.payment_method_'+radio_id).prop('checked', true);
      }
    });


    $('.btn_checkout').click(function(){
      var payment_method = $("input[name='payment_method']:checked").val();
      
      if(payment_method=='' || payment_method==null || payment_method=='0' || payment_method==undefined)
      {
          $('.checkout_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 15px;'><span class='fa fa-times'></span><strong> Error! </strong> Please select payment method.</div>");
          return false;
      }
    });
    return false;
  });
</script>