<?php
	require_once 'config/config.php'; 
  	include("config/dbconnect.php"); 
  	require_once 'config/functions.php'; 
  	require_once 'config/setup-values.php';

  	$adminEmail = getCompanyEmail();

  	$maintainance_mode = getMaintainanceModeActiveOrNot();

	if($maintainance_mode!='1')
	{
	    echo "<script>window.location.assign('index.php');</script>";
	    exit;
	}
?>
<!DOCTYPE html>
<html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">

<head>
  	<title>Website Under Maintainance </title>
  	<meta charset="UTF-8">
  <!-- Mobile Metas -->
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  	<link rel="stylesheet" href="css/bootstrap.css" />
 	<style>
 		html, body{
		  margin: 0 auto;
		  font-family: 'Roboto', sans-serif;
		  height: 100%;
		  background: #EEEEF4;
		  font-weight: 100;
		  user-select: none;
		}

		main{
		  height: 100%;
		  display: flex;
		  margin: 0 20px; // To look nice on mobile
		  text-align: center;
		  flex-direction: column;
		  align-items: center;
		  justify-content: center;
		  h1{
		    font-size: 3em;
		    font-weight: 100;
		    color: #F44;
		    margin: 0;
		  }
		  h2{
		    font-size: 1.5em;
		    font-weight: 100;
		    margin-bottom: 0;
		  }
		  h3{
		    font-size: 1.5em;
		    font-weight: 100;
		    margin-top: 0;
		  }
		  a{
		    font-size: 1.5em;
		    font-weight: 300;
		    color: #F44;
		    text-decoration: none;
		  }
		}

		footer{
		  position: absolute;
		  bottom: 0;
		  margin: 10px;
		  font-weight: 300;
		}
	</style>
</head>

<body>
	<main>

	  	<img src="images/maintainace-loader/loader.gif" alt="website-maintainance">
	    
	  <h1>Website Under Maintanance</h1>
	  <h2>Sorry for the inconvenience.</h2>
	  <h3>For any queries please contact at: <b><?php echo $adminEmail; ?></b></h3>
	</main>
	<footer>Copyright &copy; <?php echo date('Y'); ?> <?php echo getWebsiteTitle(); ?></footer>
</body>
</html>