<?php
	session_start();
	require_once '../config/config.php'; 
	include('../config/dbconnect.php');    //database connection
	include('../config/functions.php');   //strip query string
	include('../config/setup-values.php');   //get master setup values
	include('../config/email/email_style.php');   //get master setup values
	include('../config/email/email_templates.php');   //get master setup values
	include('../config/email/email_process.php');
	
	$today_datetime = date('Y-m-d H:i:s');

  	$basepath = $WebSiteBasePath.'/';

    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }

    $recaptchaAllowed = getreCaptchaAllowed();
	if($recaptchaAllowed == "1")
	{
		$recaptchaSiteKey=getreCaptchaSiteKey();
		$recaptchaSecretKey=getreCaptchaSecretKey();
	}

	$task = quote_smart($_POST['task']);

	if($task == "contact-us-request")
	{ 
		$fullname = quote_smart($_POST['fullname']);
		$email = quote_smart($_POST['email']);
		$mobile = quote_smart($_POST['mobile']);
		$subject = quote_smart($_POST['subject']);
		$message = quote_smart($_POST['message']);
		$user = quote_smart($_POST['user']);
		$recaptchaAllowed = quote_smart($_POST['recaptchaAllowed']);
		$response = quote_smart($_POST['response']);

		if($recaptchaAllowed=='1' && !$response){
		    echo json_encode('Please check the the captcha form.');
		    exit;
		}

		if($recaptchaAllowed == "1")
		{
			$secret = $recaptchaSecretKey;
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response);
			$responseData = json_decode($verifyResponse);
			if($responseData->success)
			{
				$recaptcha_verified = "1";
			}
			else
			{
				echo json_encode('Captcha verification failed! Try after some time.');
		    	exit;
			}
		}
		

		$full_name = $fullname;
		$email_address = $email;
		$enquiry_subject = $subject;

		$enquiry_email = getEnquiryEmail();

		if($user!='' || $user!=null)
		{
			echo json_encode("You are not allowed to access this page.");
			exit;
		}

		$sql_insert="INSERT INTO enquiry(fullName,email,mobile,subject,message,enquiryOn) VALUES('$fullname','$email','$mobile','$subject','$message','$today_datetime')";

		if($link->exec($sql_insert))
		{
			$SocialSharing = getSocialSharingLinks();   // social sharing links

			$EnquiryEmailSubject = str_replace('$enquiry_subject',$enquiry_subject,$EnquiryEmailSubject);   //replace subject variables with value

	        $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo'),array($WebSiteBasePath,$WebSiteTitle,$logo),$EmailGlobalHeader);  //replace header variables with value

	        $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle),$EmailGlobalFooter,$SocialSharing);  //replace footer variables with value

	        $EnquiryEmailMessage = str_replace(array('$site_name','$full_name','$email_address','$mobile','$enquiry_subject','$message','$signature'),array($WebSiteTitle,$full_name,$email_address,$mobile,$enquiry_subject,$message,$GlobalEmailSignature),$EnquiryEmailMessage);  //replace footer variables with value

	        $subject = $EnquiryEmailSubject;
            
	        $message  = '<!DOCTYPE html>';
	        $message .= '<html lang="en">
				<head>
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width">
				<title></title>
				<style type="text/css">'.$EmailCSS.'</style>
				</head>
				<body style="margin: 0; padding: 0;">';
	        //echo $message;exit;
	        $message .= $EmailGlobalHeader;

	        $message .= $EnquiryEmailMessage;

	        $message .= $EmailGlobalFooter;
	        
	        //$mailto = $enquiry_email;
	        $mailto = $email_address;
	        $mailtoname = 'user';

	        $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

	        if($emailResponse == 'success')
	        {
	        	echo json_encode("success");
	        	exit;
	        }
	        else
	        {
	            echo json_encode("Error in sending email. Please try after some time.<br/> Thank You.");
	            exit;
	        }
		}
	}
?>