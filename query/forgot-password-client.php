<?php
	session_start();
	require_once '../config/config.php'; 
	include('../config/dbconnect.php');    //database connection
	include('../config/functions.php');   //strip query string
    include('../config/setup-values.php');   //get master setup values
    include('../config/email/email_style.php');   //get master setup values
    include('../config/email/email_templates.php');   //get master setup values
    include('../config/email/email_process.php');
	
    $WebSiteBasePath = getWebsiteBasePath();
    $sitetitle = getWebsiteTitle();
    $logo_array=array();
    $logoURL = getLogoURL();
    if($logoURL!='' || $logoURL!=null)
    {
        $logoURL = explode('/',$logoURL);

        for($i=1;$i<count($logoURL);$i++)
        {
            $logo_array[] = $logoURL[$i];
        }

        $logo_path = implode('/',$logo_array);
    }

    if($logoURL!='' || $logoURL!=null)
    {
        $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
    }
    else
    {
        $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
    }
    
    $task = quote_smart($_POST['task']);

	if($task == "Forgot-Password-User")
	{
    	$email = quote_smart($_POST['email']);
        $user_name = quote_smart($_POST['user_name']);

        if($user_name!='' || $user_name!=null)
        {
            echo "You are not human. You cannot access this page.";
            exit;
        }
        
    	$sql_chk = $link->prepare("SELECT * FROM `clients` WHERE `email`='$email'"); 
        $sql_chk->execute();

        $count=$sql_chk->rowCount();

        if($count>0)
        {
        	$userData = $sql_chk->fetch(PDO::FETCH_OBJ);
            $id = $userData->id;
            $firstname = $userData->firstname;
            $email = $userData->email;
            $status = $userData->status;

            if($status=='0')
            {
                echo "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email address.<br/>
                        <a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
                exit;
            }
            else
            if($status=='2')
            {
                echo "Your account is disabled.<br/>Contact administrator to activate your account.";
                exit;
            }
            else
            if($status=='3')
            {
                echo "Your account is suspended.<br/>Contact administrator to activate your account.";
                exit;
            }

            $Special_char_string = '[!@#$%^)*_(+=}{|:;,.<>}]'; 
            $pos1 = rand(0,(strlen($Special_char_string)-1));
            $pass1 = $Special_char_string[$pos1];

            $Capital_char_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
            $pos2 = rand(0,(strlen($Capital_char_string)-1));
            $pass2 = $Capital_char_string[$pos2];

            $Number_char_string = '1234567890'; 
            $pos3 = rand(0,(strlen($Number_char_string)-1));
            $pass3 = $Number_char_string[$pos3];

            $salt = "abchefghjkmnpqrstuvwxyz0123456789";
            srand((double)microtime()*1000000);

            $i = 0;
            while ($i <= 5) 
            {
                $num = rand() % 33;
                $tmp = substr($salt, $num, 1);
                $pass1 = $pass1 . $tmp;
                $i++;
            }
                
            $new_pass = $pass2.$pass3.$pass1;    //
            $tmp_password = md5($new_pass);     // random 

            $sql_update = "UPDATE `clients` SET `password`='$tmp_password' WHERE `id`='$id' AND `email`='$email'";

            $login_link = "<a href='$WebSiteBasePath/login.php' target='_blank'>$WebSiteBasePath/admin/login.php</a>";

            $sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$id','forgot password','reset & sent on $email','$IP_Address',now())";

            $sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$id','forgot password','reset & sent on $email',now())";

            if($link->exec($sql_update))
            {
                $link->exec($sql_member_log);
                $link->exec($sql_member_email_log);

                $SocialSharing = getSocialSharingLinks();   // social sharing links
                
                $EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

                $ForgotPasswordSubject = str_replace('$site_name',$WebSiteTitle,$ForgotPasswordSubject);   //replace subject variables with value

                $EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

                $EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

                $ForgotPasswordMessage = str_replace(array('$first_name','$site_name','$login_link','$email_address','$new_password','$signature'),array($firstname,$WebSiteTitle,$login_link,$email,$new_pass,$GlobalEmailSignature),$ForgotPasswordMessage);  //replace footer variables with value


                $subject = $ForgotPasswordSubject;
                
                $message  = '<!DOCTYPE html>';
                $message .= '<html lang="en">
                    <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width">
                    <title></title>
                    <style type="text/css">'.$EmailCSS.'</style>
                    </head>
                    <body style="margin: 0; padding: 0;">';
                //echo $message;exit;
                $message .= $EmailGlobalHeader;

                $message .= $ForgotPasswordMessage;                                               
                $message .= $GlobalEmailSignature;

                $message .= $EmailGlobalFooter;
                
                $mailto = $email;
                $mailtoname = $firstname;
                
                $emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);
                if($emailResponse == 'success')
                {
                        echo "success";
                }
                else
                {
                        echo "Error in sending login details to user.";
                }
            }
            else
            {
                    echo "Something went wrong. Try after some time.";
            }
        }
        else
        {
            echo "invalid_user";
        }
    }
?>