<?php
	session_start();
	require_once '../config/config.php'; 
	include('../config/dbconnect.php');    //database connection
	include('../config/functions.php');   //strip query string
	include('../config/setup-values.php');   //get master setup values
	include('../config/email/email_style.php');   //get master setup values
	include('../config/email/email_templates.php');   //get master setup values
	include('../config/email/email_process.php');
	
	$today_datetime = date('Y-m-d H:i:s');

  	$WebSiteBasePath = getWebsiteBasePath();
	$task = quote_smart($_POST['task']);

	if($task == "register_new_user")
	{ 
		$firstname = quote_smart($_POST['fname']);
		$lname = quote_smart($_POST['lname']);
		$email = quote_smart($_POST['email']);
		$country = quote_smart($_POST['country']);
		$state = quote_smart($_POST['state']);
		$city = quote_smart($_POST['city']);
		$phonecode = quote_smart($_POST['country_phone_code']);
		$dob = quote_smart($_POST['dob']);
		$gender = quote_smart($_POST['gender']);
		$phonenumber = quote_smart($_POST['phonenumber']);
		$password = quote_smart($_POST['password']);
		$confirm_password = quote_smart($_POST['confirm_password']);
		
		$uniquecode = generateRandomString();

		$MinimumUserPasswordLength = getMinimumUserPasswordLength();
		$MaximumUserPasswordLength = getMaximumUserPasswordLength();

		if($MinimumUserPasswordLength=='0' || $MinimumUserPasswordLength=='' || $MinimumUserPasswordLength==null)
		{
			$MinimumUserPasswordLength='8';
		}

		if($MaximumUserPasswordLength=='0' || $MaximumUserPasswordLength=='' || $MaximumUserPasswordLength==null)
		{
			$MaximumUserPasswordLength='40';
		}

		if($robot!='' || $robot!=null)
		{
			echo "You are not human. You cannot access this page.";
			exit;
		}

		$sql_chk_unique_code = $link->prepare("SELECT `unique_code` FROM `clients` WHERE unique_code='$uniquecode'"); 
        $sql_chk_unique_code->execute();
        $count_unique_code=$sql_chk_unique_code->rowCount();
        if($count_unique_code>0)
        {
        	$uniquecode = generateRandomString();
        }

		$passwordx = md5(trim($_POST['password']));

		$errorMessage=null;
		$subject = null;
		$message = null;
		$mailto = null;
		$mailtoname = null;
		$sql_chk = $link->prepare("SELECT `email` FROM `clients` WHERE email='$email' AND  phonenumber='$phonenumber'"); 
        $sql_chk->execute();
        $count=$sql_chk->rowCount();

        if($count == '0')
		{
			
			$age = (date('Y') - date('Y',strtotime($dob)));

			if($age<18)
			{
				echo "Oops? Its seems that you are below 18 year. You cannot proceeed further.";
				exit;
			}

			if($password == $confirm_password)
			{
				$haveuppercase = preg_match('/[A-Z]/', $password);
				$havenumeric = preg_match('/[0-9]/', $password);
				$havespecial = preg_match('/[!@#$%^&)*_(+=}{|:;,.<>}]/', $password);

				if (!$haveuppercase)
				{
					$errorMessage = 'Password must have atleast one upper case character.';
				}
				else if (!$havenumeric)
				{
					$errorMessage = 'Password must have atleast one digit.';
				}
				else if (!$havespecial)
				{
					$errorMessage = 'Password must have atleast one of the special characters [!@#$%^&)*_(+=}{|:;,.<>}]';
				}
				else if (strlen($password) < $MinimumUserPasswordLength)
				{
					$errorMessage = "Password must be of minimum ".$MinimumUserPasswordLength." characters long.";
				}
				else if (strlen($password) > $MaximumUserPasswordLength)
				{
					$errorMessage = "Password must be of maximum ".$MaximumUserPasswordLength." characters long.";
				}
				else
				{
					$sql_insert = "INSERT INTO `clients`(`firstname`, `lastname`, `dob`, `gender`,`unique_code`, `email`, `phonecode`, `phonenumber`, `password`, `status`, `created_at`) VALUES ('$firstname','$lname','$dob','$gender','$uniquecode','$email','$phonecode','$phonenumber','$passwordx','1','$today_datetime')";
					//$insert_query = $link->exec($sql_insert);
					
					$sql = "SELECT MAX(id) as id FROM clients";
					$stmt   = $link->prepare($sql);
			        $stmt->execute();
			        $sql_id = $stmt->fetch();

			        $passcode = encrypt_decrypt('encrypt', $email);
			        $verification_link = "<a href='$WebSiteBasePath/verify-email.php?passcode=$passcode'><button>Verify</button></a>";

		        	$verification_text_link = "<a href='$WebSiteBasePath/verify-email.php?passcode=$passcode'>$WebSiteBasePath/verify-email.php?passcode=$passcode</a>";

			        $id = $sql_id['id']+1;

					$sql_insert_location = "INSERT INTO address(userid,city,state,country,created_at) VALUES('$id','$city','$state','$country','$today_datetime')";

					$sitetitle = getWebsiteTitle();
					$logo_array=array();
					$logoURL = getLogoURL();
					if($logoURL!='' || $logoURL!=null)
					{
						$logoURL = explode('/',$logoURL);

						for($i=1;$i<count($logoURL);$i++)
						{
							$logo_array[] = $logoURL[$i];
						}

						$logo_path = implode('/',$logo_array);
					}

					if($logoURL!='' || $logoURL!=null)
		            {
		                $logo = "<img src='$WebSiteBasePath/$logo_path' class='img-responsive logo-img' />";
		            }
		            else
		            {
		                $logo =  "<strong class='default-logo-text'>$sitetitle</strong>";
		            }

					if($link->exec($sql_insert) && $link->exec($sql_insert_location))
					{
						$SocialSharing = getSocialSharingLinks();   // social sharing links
						$primary_color = getPrimaryColor();
						$primary_font_color = getPrimaryFontColor();

						$EmailCSS = str_replace(array('$WebSiteBasePath'),array($WebSiteBasePath),$EmailCSS);  //replace css variables with value

						$EmailGlobalHeader = str_replace(array('$WebSiteBasePath','$site_name','$logo','$primary_color','$primary_font_color'),array($WebSiteBasePath,$WebSiteTitle,$logo,$primary_color,$primary_font_color),$EmailGlobalHeader);  //replace header variables with value

						$EmailGlobalFooter = str_replace(array('$emailFooterText','$WebSiteBasePath','$site_name','$SocialSharing','$primary_color','$primary_font_color'),array($emailFooterText,$WebSiteBasePath,$WebSiteTitle,$SocialSharing,$primary_color,$primary_font_color),$EmailGlobalFooter);  //replace footer variables with value

						$EmailVerificationMessage = str_replace(array('$first_name','$site_name','$verification_link','$verification_text_link','$signature'),array($firstname,$WebSiteTitle,$verification_link,$verification_text_link,$GlobalEmailSignature),$EmailVerificationMessage);  //replace footer variables with value

						$subject = $EmailVerificationSubject;
						$message  = '<!DOCTYPE html>';
						$message .= '<html lang="en">
							<head>
							<meta charset="utf-8">
							<meta name="viewport" content="width=device-width">
							<title></title>
							<style type="text/css">'.$EmailCSS.'</style>
							</head>
							<body style="margin: 0; padding: 0;">';
						//echo $message;exit;
						$message .= $EmailGlobalHeader;

						$message .= $EmailVerificationMessage;

						$message .= $EmailGlobalFooter;

						$mailto = $email;
						$mailtoname = $firstname;

						$registserUserId = getUserIdFromEmail($email);
						$emailResponse = EmailProcess($subject,$message,$mailto,$mailtoname);

						/*************      Activity log      ***************/
						$sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$registserUserId','registred','to system','$IP_Address',now())";
						$link->exec($sql_member_log);

						/*************      Email log      ***************/
						$sql_member_email_log = "INSERT INTO member_email_logs(userid,task,activity,sent_On) VALUES('$registserUserId','new registration','email',now())";

						if($emailResponse == 'success')
						{
							$link->exec($sql_member_email_log);
							$errorMessage = "success";
						}
						else
						{
							$errorMessage = "Error in sending verification link";
							//$errorMessage = $emailResponse;
						}
					}
					else
					{
						$errorMessage = "Query execution error. Please try after some time.";
					}
				}
			}
		}
		else
		{
			$errorMessage = "Email id & Mobile number already exists. Please use another email id & mobile number.";
		}

		echo $errorMessage;
		exit;
	}
?>