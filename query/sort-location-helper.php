<?php
	session_start();
	require_once '../config/config.php'; 
	include('../config/dbconnect.php');    //database connection
	include('../config/functions.php');   //strip query string
	include('../config/setup-values.php');   //get master setup values

	$task = $_POST['task'];

	if($task == 'fetch-states-from-country-value')
	{
		$country = $_POST['country'];
		$output = null;
		$sql_states = "SELECT * FROM states WHERE country_id='$country' ORDER BY name";
		$stmt = $link->prepare($sql_states);
		$stmt->execute();
		$result = $stmt->fetchAll();

		$output = "<option value='0'>--Select State--</option>";
		foreach ($result as $row) 
		{
			$id = $row['id'];
			$name = $row['name'];

			$output .= "<option value='".$id."'>".$name."</option>";
		}
		echo $output;
		exit;
	}

	if($task == 'fetch-cities-from-states-value')
	{
		$states = $_POST['states'];
		$output = null;
		$sql_states = "SELECT * FROM cities WHERE state_id='$states' ORDER BY name";
		$stmt = $link->prepare($sql_states);
		$stmt->execute();
		$result = $stmt->fetchAll();

		$output = "<option value='0'>--Select City--</option>";
		foreach ($result as $row) 
		{
			$id = $row['id'];
			$name = $row['name'];

			$output .= "<option value='".$id."'>".$name."</option>";
		}
		echo $output;
		exit;
	}
?>