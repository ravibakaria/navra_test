<?php
	if(!isset($_SESSION))
	{
	    ob_start();
	    session_start();
	}

	require_once "../config/config.php";
	require_once "../config/dbconnect.php";
	include('../config/functions.php');       //strip query string

	$task = quote_smart($_POST['task']);

	/*     Fetch states W.R.T. $country_id     */
	if($task == "Fetch_state_data")
	{
		$response = null;
		$output_data = null;
    	$country_id = quote_smart($_POST['country_id']);

    	$query  = "SELECT * FROM `states` WHERE `country_id`='$country_id' ORDER BY `name` ASC";
		$stmt   = $link->prepare($query);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$response = "<option value='0' selected>Select State</option>";
		foreach( $result as $row )
		{
			$state_name =  $row['name'];
			$state_id = $row['id']; 

			$response .= "<option value='".$state_id."'>".$state_name."</option>";
		}

		$stmt1 = $link->prepare("SELECT * FROM countries WHERE id='$country_id'");
        $stmt1->execute();
        $row1 = $stmt1->fetch(PDO::FETCH_ASSOC);

        $phonecode = $row1['phonecode'];

		$output_data = json_encode(array($response,$phonecode));
		echo $output_data;
		exit;
    }

    /*     Fetch cities W.R.T. $state_id     */
    if($task == "Fetch_city_data")
	{
		$response = null;
    	$state_id = quote_smart($_POST['state_id']);

    	$query  = "SELECT * FROM `cities` WHERE `state_id`='$state_id' ORDER BY `name` ASC";
		$stmt   = $link->prepare($query);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$response = "<option value='0' selected>Select City</option>";
		foreach( $result as $row )
		{
			$city_name =  $row['name'];
			$city_id = $row['id']; 

			$response .= "<option value='".$city_id."'>".$city_name."</option>";
		}

		echo $response;
		exit;
    }


    /*     Fetch states W.R.T. $country_id for advanced search    */
	if($task == "Fetch_state_data_Advanced_Search")
	{
		$response = null;
		$output_data = null;
    	$country_id = quote_smart($_POST['country_id']);

    	$query  = "SELECT * FROM `states` WHERE `country_id`='$country_id' ORDER BY `name` ASC";
		$stmt   = $link->prepare($query);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$response = "<option value='0' selected>Select State</option>";
		foreach( $result as $row )
		{
			$state_name =  $row['name'];
			$state_id = $row['id']; 

			$response .= "<option value='".$state_id."'>".$state_name."</option>";
		}

		echo $response;
		exit;
    }

    /*     Fetch age range upto W.R.T. start_age      */
    if($task == "Fetch_age_data")
	{
		$response = null;
    	$from_date = quote_smart($_POST['from_date']);

    	for($i=$from_date;$i<61;$i++)
    	{
    		$response .= "<option value='".$i."'>".$i."</option>";
    	}

    	$response .= "<option value='61'>60+</option>";

		echo $response;
		exit;
    }

    /*     Fetch to_age values      */
    if($task == "Fetch_to_age_values")
	{
		$response = null;
    	$from_date = quote_smart($_POST['from_date']);

    	for($i=$from_date;$i<100;$i++)
    	{
    		$response .= "<option  class='home-search-filter' value='".$i."'>".$i."</option>";
    	}

		echo $response;
		exit;
    }
?>