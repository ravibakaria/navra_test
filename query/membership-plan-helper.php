<?php
	session_start();
	require_once '../config/config.php'; 
	include('../config/dbconnect.php');    //database connection
	include('../config/functions.php');   //strip query string
	include('../config/setup-values.php');   //get master setup values

	$task = quote_smart($_POST['task']);

	/***************       fetch plan price      *************/
	if($task == "get_total_plan_price")
	{ 
		$total_price = null;
		$id = quote_smart($_POST['id']);
		$membership_price = quote_smart($_POST['membership_price']);

		$membership_price_arr = explode(',',$membership_price);
		$membership_price_value = $membership_price_arr[1];
		$featured_listing_price = $membership_price_arr[0];

		$number_of_duration_months = getMembershipPlanDuaration($id);
		$tax_applied_or_not = getTaxAppliedOrNot();

		$total_price = $membership_price_value+($featured_listing_price*$number_of_duration_months);

		$total_price = $total_price.".00";
		if($tax_applied_or_not=='1')
		{
			echo "Total: <h3>INR ".$total_price." +Tax</h3>";
		}
		else
		{
			echo "Total: <h3>INR ".$total_price."</h3>";
		}
		exit;
	}

	/***************       generate secret code premium order      *************/
	if($task == "generate_secret_code_premium_order")
	{
		$secret = null;
		$id = quote_smart($_POST['id']);
		$featured_listing = quote_smart($_POST['featured_listing']);
		$number_of_duration_months = getMembershipPlanDuaration($id);

		$secret = $id."*+".$number_of_duration_months."+*".$featured_listing;
		$secret = md5($secret);

		echo $secret;
		exit;
	}

	/***************       generate secret code featured listing      *************/
	if($task == "generate_secret_code_featured_order")
	{
		$secret = null;
		$month = quote_smart($_POST['month']);

		$secret = "0*+".$month."+*1";
		$secret = md5($secret);

		echo $secret;
		exit;
	}
?>