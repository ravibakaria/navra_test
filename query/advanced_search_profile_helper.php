<?php
	ob_start();
	session_start();
	
	require_once "../config/config.php";
	require_once "../config/dbconnect.php";
	include('../config/functions.php');       //strip query string

	$task = quote_smart($_POST['task']);

	if($task=="Get_Advanced_Search_Result") 
	{
		$where = $sqlTot = $sqlRec =  "";
		$data = null;
		$output = null;
		$today = date('Y-m-d');
		if(isset($_POST['profile_id']))
		{
			$profile_id = $_POST['profile_id'];
		}
		else
		{
			$profile_id=null;
		}
		
		
		$gender = $_POST['gender'];
		$marital_status = $_POST['marital_status'];
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$religion = $_POST['religion'];
		$caste = $_POST['caste'];
		$mother_tongue = $_POST['mother_tongue'];
		$education = $_POST['education'];
		$occupation = $_POST['occupation'];
		$currency = $_POST['currency'];
		$monthly_income_from = $_POST['monthly_income_from'];
		$monthly_income_to = $_POST['monthly_income_to'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$city = $_POST['city'];

		$data = 'profile_id='.$profile_id.'&gender='.$gender.'&marital_status='.$marital_status.'&from_date='.$from_date.'&to_date='.$to_date.'&religion='.$religion.'&caste='.$caste.'&mother_tongue='.$mother_tongue.'&education='.$education.'&occupation='.$occupation.'&currency='.$currency.'&monthly_income_from='.$monthly_income_from.'&monthly_income_to='.$monthly_income_to.'&country='.$country.'&state='.$state.'&city='.$city ;

		$data = str_replace("&",'-',$data);
		$sql = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
        	clients as A 
        	LEFT OUTER JOIN
			profilebasic AS B ON A.id = B.userid
			LEFT OUTER JOIN
			address AS C ON A.id = C.userid
			LEFT OUTER JOIN
			profilereligion AS D ON A.id = D.userid
			LEFT OUTER JOIN
			eduocc AS E ON A.id = E.userid
			LEFT OUTER JOIN
			profilepic AS F ON A.id = F.userid
			LEFT OUTER JOIN
			family AS G ON A.id = G.userid
			WHERE A.gender='$gender' ";

	/******  Profile Client Start *****/
		if($from_date=='0')      // Age form
		{
			$where .="";
		}
		else
		{
			$age_from = date('Y-m-d', strtotime('-'.$from_date.' years'));
			$where .=" AND A.dob >='$age_from' AND A.dob <='$today'";
		}

		if($to_date=='0')      // Age to
		{
			$where .="";
		}
		else
		{
			//$age_from_chk = strtotime($age_from);
			$age_to = date('Y-m-d',strtotime("-".$to_date." years"));
			$where .=" OR A.dob <='$age_to' AND A.dob <='$today'";
		}
	/******  Profile Client End *****/

	/******  Profile Basic Start *****/
		if($marital_status=='' || $marital_status=='0')        // Marital status
		{
			$where .="";
		}
		else
		{
			$where .=" AND B.marital_status IN($marital_status)";
		}

	/******  Profile Basic End *****/

	/******  Profile location Start *****/
		if($city=='0')    // City
		{
			$where .="";
		}
		else
		{
			$where .=" AND C.city ='$city'";
		}
		
		if($state=='0')     //state
		{
			$where .="";
		}
		else
		{
			$where .=" AND C.state ='$state'";
		}

		if($country=='0')     //country
		{
			$where .="";
		}
		else
		{
			$where .=" AND C.country ='$country'";
		}
	/******  Profile location End *****/


	/******  Profile Education Start *****/
		if($education=='0')     //country
		{
			$where .="";
		}
		else
		{
			$where .=" AND E.education ='$education'";
		}

		if($occupation=='0')     //occupation
		{
			$where .="";
		}
		else
		{
			$where .=" AND E.occupation ='$occupation'";
		}

		if($currency=='0')     //currency
		{
			$where .="";
		}
		else
		{
			$where .=" AND E.income_currency ='$currency'";
		}

		if($monthly_income_from=='' || $monthly_income_from==null || $monthly_income_from=='0' )     //monthly_income_from
		{
			$where .="";
		}
		else
		{
			if(($monthly_income_from!='' || $monthly_income_from!=null || $monthly_income_from!='0' )  && $monthly_income_from=='1000001')
			{
				$where .=" AND E.income >$monthly_income_from";
			}
			else
			if(($monthly_income_from!='' || $monthly_income_from!=null || $monthly_income_from!='0' )  && $monthly_income_from!='1000001')
			{
				$where .=" AND E.income >=$monthly_income_from";
			}
		}

		if($monthly_income_to=='' || $monthly_income_to==null || $monthly_income_to=='0')     //monthly_income_to
		{
			$where .="";
		}
		else
		{
			if(($monthly_income_to!='' || $monthly_income_to!=null || $monthly_income_to!='0') && $monthly_income_to=='1000001')
			{
				$where .=" AND E.income >$monthly_income_to";
			}
			else
			if(($monthly_income_to!='' || $monthly_income_to!=null || $monthly_income_to!='0') && $monthly_income_to!='1000001')
			{
				$where .=" AND E.income <=$monthly_income_to";
			}
		}
	/******  Profile Education  *****/


	/******  Profile Relegious Start *****/
		if($religion=='0')     //religion
		{
			$where .="";
		}
		else
		{
			$where .=" AND D.religion ='$religion'";
		}

		if($mother_tongue=='0')     //mother_tongue
		{
			$where .="";
		}
		else
		{
			$where .=" AND D.mother_tongue ='$mother_tongue'";
		}

		if($caste=='0')     //caste
		{
			$where .="";
		}
		else
		{
			$where .=" AND D.caste ='$caste'";
		}
	/******  Profile Relegious End *****/

		/*****  Limit  *****/
		$sqlRec .=  $sql.$where;

		if($profile_id!='' || $profile_id!=null)
		{
			$sql = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
        	clients as A 
        	LEFT OUTER JOIN
			profilebasic AS B ON A.id = B.userid
			LEFT OUTER JOIN
			address AS C ON A.id = C.userid
			LEFT OUTER JOIN
			profilereligion AS D ON A.id = D.userid
			LEFT OUTER JOIN
			eduocc AS E ON A.id = E.userid
			LEFT OUTER JOIN
			profilepic AS F ON A.id = F.userid
			LEFT OUTER JOIN
			family AS G ON A.id = G.userid
			WHERE A.unique_code='$profile_id' ";

			$sqlRec =  $sql;
		}

		

		$stmt_search   = $link->prepare($sqlRec);
		$stmt_search->execute();
		$sqlTot = $stmt_search->rowCount();
		$result_search = $stmt_search->fetchAll();

		if($sqlTot>0)
		{	
			$output .= "<div>";
				$output .= "<h3 class='heading_page'>Profiles which matches your search criteria.</h3>";
			$output .= "</div>";
			$i=0;
			$output .= "<div class='row row-eq-height'>";
			foreach( $result_search as $row )
			{

				$city = $row['city'];
				$state = $row['state'];
				$country = $row['country'];
				$edu = $row['education'];
				$occ = $row['occupation'];
				$religion = $row['religion'];
				$caste = $row['caste'];
				$mother_tongue = $row['mother_tongue'];
				$u_code = $row['unique_code'];
				$dobx = $row['dob'];
				$profileid = $row['id'];
				$gender = $row['gender'];
				$height = $row['height'];
				$salary = $row['income'];
				$salary_currency = $row['income_currency'];
				$fam_type = $row['fam_type'];
				
				$search_data = 'Profile-'.$u_code;
				$pro_url = $search_data.".html";
				$search_user_fname=$row['firstname'];
				$search_user_name = $row['firstname']." ".$row['lastname'];
				$prefer_user_id =$row['idx'];
				$photo =$row['photo'];
				$today = date('Y-m-d');
				$diff = date_diff(date_create($dobx), date_create($today));
				$age_search_user = $diff->format('%y');
				//$user_occ = user_occ($occ);
				if($i==0 || ($i>1 && $i%2==0))
				{
					$output .= "<div class='row cover_rows' >";
				}
				
				$output .= "<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6 cover_cols' >
					<div class='home-advanced-profile-div'>
						<a href='$pro_url' style='text-decoration: none;'>
						
							<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
								<div class='row'>
									<center>
									<div class='profile-sidebar'>
										<div class='profile-usertitle'>
											<div class='profile-usertitle-name'>
												<strong>
													".ucwords($search_user_fname)."<br/> From ".substr(getUserCityName($city),0,11);
												$output .= "</strong>
												
											</div>
										</div>
										<!-- SIDEBAR USERPIC -->
										<div class='profile-userpic'>";
											
												if($photo=='' || $photo==null)
												{
													$output .= "<img src='images/no_profile_pic.png'  class='home-advanced-profile-img' alt='$search_user_fname' title='$search_user_fname'/>";
												}
												else
												{
													$output .= "<img src='user/uploads/$prefer_user_id/profile/$photo'  class='home-advanced-profile-img' alt='$search_user_fname' title='$search_user_fname'/>";
												}
											
										$output .="</div>
										
									</div>
									</center>
								</div>
							</div>
							<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 profile_info_data'>
								<div class='row'>
									</center>
									<div class='row'>
										<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
											<strong >Age: </strong> 
										</div>
										<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>
										&nbsp ".$age_search_user." Years.
									
										</div>
									</div>";
							$output .="<div class='row'>
										<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
											<strong>Height: </strong> 
										</div>
										<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>&nbsp";
												
											if($height!='' || $height!=null)
											{
												$search_user_height_meter = round(($height)/100,2);
							                	$search_user_height_foot = round(($height)/30.48,2);
							                	$output .= $height.'-cms/ '.$search_user_height_foot.'-fts/ '.$search_user_height_meter.'-mts';
											}
											else
											{
												$output .= "-NA-";
											}
										
										$output .="</div>
									</div>";
							$output .="<div class='row'>
										<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
											<strong>Religion: </strong> 
										</div>
										<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>&nbsp";
							                	
											if($religion!='' || $religion!=null)
											{
												$output .= getUserReligionName($religion);
											}
											else
											{
												$output .= "-NA-";
											}
										
										$output .="</div>
									</div>";
							$output .="<div class='row'>
										<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
											<strong>Caste: </strong> 
										</div>
										<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>&nbsp";
							                	
											if($caste!='' || $caste!=null)
											{
												$output .= getUserCastName($caste);
											}
											else
											{
												$output .= "-NA-";
											}
										$output .="</div>
									</div>";
							$output .="<div class='row'>
										<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
											<strong>Education: </strong> 
										</div>
										<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>&nbsp";
							                	
											if($edu!='' || $edu!=null)
											{
												$output .= getUserEducationName($edu);
											}
											else
											{
												$output .= "-NA-";
											}
										
										$output .="</div>
									</div>";
							$output .="<div class='row'>
										<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
											<strong>Occupation: </strong> 
										</div>
										<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>&nbsp";
							                	
											if($occ!='' || $occ!=null)
											{
												$output .= user_occ($occ);
											}
											else
											{
												$output .= "-NA-";
											}
									
										$output .="</div>
									</div>";
							$output .="<div class='row'>
										<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
											<strong>Salary: </strong> 
										</div>
										<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>&nbsp";
							                	
											if($salary!='' || $salary!=null)
											{
												$output .= getUserIncomeCurrencyCode($salary_currency)." ".$salary." Monthly";
											}
											else
											{
												$output .= "-NA-";
											}
									
										$output .="</div>
									</div>";
							$output .="<div class='row'>
										<div class='col-xs-6 col-sm-6 col-md-4 col-lg-4 h3_heading_profile'>
											<strong>Mother Tongue: </strong> 
										</div>
										<div class='col-xs-6 col-sm-6 col-md-8 col-lg-8 adv_profile_data'>&nbsp";
							                	
											if($mother_tongue!='' || $mother_tongue!=null)
											{
												$output .= getUserMotherTongueName($mother_tongue);
											}
											else
											{
												$output .= "-NA-";
											}
										
										$output .="</div>
									</div>
									</center>
								</div>
							</div>

						</a>
					</div>
				</div>";
				
				$i=$i+1;
				
				if($i%2==0)
				{
					$output .= "</div>";
				}

				if($i==10)
				{
					break;
				}
			}
			$output .= "</div>";
			$output .= "<div class='row'>
				<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
					//$url=$WebSiteBasePath.'/user/';
					$link_url = 'User-Adv-'.$data.'.html';
			if($sqlTot>10)
			{
				$output .= "<p align='center'><a href='$link_url' alt='View all'>View All</a></p>";
			}
			
			$output .= "</div>
			</div>";
		}
		else
		{
			$output .= "<h3 class='error'>Oops! No profile found with your preference.</h3>";
		}
		echo $output;
	}
?>