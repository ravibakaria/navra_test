<?php
	session_start();
	require_once '../config/config.php'; 
	include('../config/dbconnect.php');    //database connection
	include('../config/functions.php');   //strip query string
	include "../config/setup-values.php";

        $today_datetime = date('Y-m-d H:i:s');
        
        $basepath = $WebSiteBasePath.'/';
        
        $task = quote_smart($_POST['task']);

	if($task == "login_client_user")
	{
        	$email = quote_smart($_POST['email']);
        	$password = md5(quote_smart($_POST['password']));

        	$sql_chk = $link->prepare("SELECT * FROM `clients` WHERE email='$email'"); 
                $sql_chk->execute();
                $count=$sql_chk->rowCount();

                if($count>0)
                {
                	$userData = $sql_chk->fetch(PDO::FETCH_OBJ);
                	$user_id = $userData->id;
                        $user_firstname = $userData->firstname;
                	$user_password = $userData->password;
                	$user_status = $userData->status;
                	$user_isEmailVerified = $userData->isEmailVerified;
                        $IP_Address = $_SERVER['REMOTE_ADDR'];
                        
                	if($user_status=='1' || $user_status=='2')
                	{
                		if($user_isEmailVerified=='1')
                		{
                			if($user_password==$password)
                			{
                                                $sql_insert_log = "INSERT INTO `userlogs`(`userid`,`user_firstname`,`user_type`,`IP_Address`,`loggedOn`) VALUES('$user_id','$user_firstname','Client','$IP_Address','$today_datetime')";

                                                $sql_member_log = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$user_id','login','to system','$IP_Address',now())";

                                                $link->exec($sql_insert_log);   //Inserting logs for login

                                                $link->exec($sql_member_log);   //Inserting activity logs

                                                $_SESSION['logged_in'] = true;
                				$_SESSION['client_user'] = true;
                				$_SESSION['user_id'] = $user_id;
                				$_SESSION['user_email'] = $email;

                				$errorMessage = "success";
                			}
                			else
                			{
                                                $errorMessage = "Please enter valid email id & password.<br/>";
                			}
                		}
                		else
                		if($user_isEmailVerified=='0')
                		{       
                                        $errorMessage = "email_not_verified";
                		}
                	}
                	else
                	if($user_status=='0')
                	{
                		$errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email address.<br/>
                                        <a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
                	}
                        else
                        if($user_status=='3')
                        {
                                $errorMessage = "Your account is suspended.<br/>Contact administrator to activate your account.";
                        }
                }
                else
                {
                	$errorMessage = "Please enter valid email id & password.";
                }

		echo $errorMessage;
		exit;
	}
?>